<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file customer payments API related controller functions
 */

namespace Invoice\Controller;

use Invoice\Form\CustomerPaymentsForm;
use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Model\Invoice;
use Zend\View\Model\JsonModel;
use Invoice\Model\Customer;
use Invoice\Model\Payments;
use Invoice\Model\CreditNote;
use Invoice\Model\InvoicePayments;
use Invoice\Model\Reference;
use Invoice\Model\PaymentMethodsNumbers;
use Invoice\Model\IncomingPaymentMethodCash;
use Invoice\Model\IncomingPaymentMethodCheque;
use Invoice\Model\IncomingPaymentMethodCreditCard;
use Invoice\Model\IncomingPaymentMethodUniformVoucher;
use Invoice\Model\IncomingPaymentMethodBankTransfer;
use Invoice\Model\IncomingPaymentMethodGiftCard;
use Invoice\Model\IncomingPaymentMethod;
use Invoice\Model\IncomingPaymentMethodLC;
use Invoice\Model\IncomingPaymentMethodTT;
use Invoice\Model\InvoiceEditLog;
use Invoice\Model\InvoiceEditLogDetials;
use Zend\Session\Container;
use Settings\Model\IncomingPaymentMethodLoyaltyCard;

/**
 * This is the Customer Payments controller class
 */
class CustomerPaymentsAPIController extends CoreController
{

    /**
     * The CustomerPaymentsform is creat by this indexAction
     * @return \Zend\View\Model\ViewModel
     */
    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;
    protected $useAccounting;

    const CUSTOMER_PAYMENT_DOCUMENT_TYPE = 'Customer Payment';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function getPaymentNoForLocationAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $refNumber = NULL;
            $locationRefID = NULL;
            $refData = $this->getReferenceNoForLocation(4, $locationID);
            $locationRefID = $refData["locRefID"];
            $refNumber = $refData['refNo'];
            if ($refNumber) {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $refNumber,
                    'locRefID' => $locationRefID,
                );
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_NOREFNUM');
                $this->data = FALSE;
                return $this->JSONRespond();
            }
        }
    }

    /** get payment term from the database* */
    public function paymentTermAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paymentterm = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
            while ($t = $paymentterm->current()) {
                $payterm[$t->id] = $t->method;
            }
            $this->setLogMessage('Payment term list accessed ');
            return new JsonModel($payterm);
        } else {
            $this->setLogMessage("payment term accessed request is not a post request");
            return new JsonModel(array(false));
        }
    }

    public function addAllPaymentAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $this->beginTransaction();
            $res = $this->makePayment($request->getPost());
            if ($res['state']) {
                $this->commit();
            } else {
                $this->rollback();
            }

            return new JsonModel($res);
        }
    }

    public function makePayment($data_set, $restFlag = false, $posUserID = false)
    {
        if ($restFlag) {
            $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
            $customCurrencyId = $displaySettings->currencyID;
        } else {
            $customCurrencyId = (empty($data_set['customCurrencyId'])) ? $this->companyCurrencyId : $data_set['customCurrencyId'];
        }
        
        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = $customCurrencyData->currencyRate;
        $customCurrencyRate = ($data_set['customCurrencyRate'] != 0)? $data_set['customCurrencyRate'] : $customCurrencyRate;

        $customCurrencySymbol = $customCurrencyData->currencySymbol;

        $customerID = $data_set['customerID'];
        $invData = json_decode($data_set['invData']);
        $paymentCode = $data_set['paymentID'];
        $date = $this->convertDateToStandardFormat($data_set['date']);
        $paymentTerm = $data_set['paymentTerm'];
        $amount = $data_set['amount'] * $customCurrencyRate;
        $discount = ($data_set['discount'] != '') ? $data_set['discount'] * $customCurrencyRate : 0.00;
        $memo = $data_set['memo'];
        $paymentType = $data_set['paymentType'];
        $customerCredit = $data_set['customerCredit'];
        $customerBalance = $data_set['customerBalance'];
        $locationID = $data_set['locationID'];
        $creditAmount = ($data_set['creditAmount'] != '') ? $data_set['creditAmount'] * $customCurrencyRate : 0.00;
        $paymentMethods = $data_set['paymentMethods'];
        $pos = isset($data_set['pos']) ? $data_set['pos'] : false;
        $balance = isset($data_set['balance']) ? $data_set['balance'] * $customCurrencyRate : null;
        $totalPaidAmount = isset($data_set['totalPaidAmount']) ? $data_set['totalPaidAmount'] * $customCurrencyRate : null;
        $loyaltyData = $data_set['loyaltyData'];
        $restBalance = $data_set['restBalance'] * $customCurrencyRate;
        $dimensionData = isset($data_set['dimensionData']) ? $data_set['dimensionData'] : [];
        $autoDimenison = $data_set['autoDiemsnion'];
        $ignoreBudgetLimit = $data_set['ignoreBudgetLimit'];

        $refData = $this->getReferenceNoForLocation(4, $locationID);
        $locationReferenceID = $refData["locRefID"];
        // check if a payment from the same paymet Code exists if exist add next number to payment code
        while ($paymentCode) {
            if ($this->CommonTable('Invoice\Model\PaymentsTable')->checkPaymentByCode($paymentCode)->current()) {
                if ($locationReferenceID) {
                    $newPaymentID = $this->getReferenceNumber($locationReferenceID);
                    if ($newPaymentID == $paymentCode) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $paymentCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $paymentCode = $newPaymentID;
                    }
                } else {
                    return array(
                        'state' => false,
                        'msg' => $this->getMessage('ERR_CUSTPAY_API_CODE_EXIST'),
                    );
                }
            } else {
                break;
            }
        }

        $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        $cCurrentbalancess = $customer->customerCurrentBalance;
        $cCurrentCredit = $customer->customerCurrentCredit;

        $accountProduct = array();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountData = $glAccountExist->current();

        if ($restFlag) {
            $this->useAccounting = 1;
        }

        if ($customerID) {
            if ($customerCredit != $cCurrentCredit) {
                return array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_CUSTPAY_API_CUS_CRDT'),
                    'customerID' => $customerID
                );
            } else if ($customerBalance != $cCurrentbalancess) {
                return array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_CUSTPAY_API_CUS_OUTSTAND'),
                    'customerID' => $customerID
                );
            }

            $customerOverPaymentAccountIsSet = true;
            if($this->useAccounting == 1){
                if(empty($customer->customerReceviableAccountID)){
                    return array(
                        'state' => false,
                        'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customer->customerName."-".$customer->customerCode)),
                    );
                } else if($discount > 0 && empty($customer->customerSalesDiscountAccountID)){
                    return array(
                        'state' => false,
                        'msg' => $this->getMessage('ERR_CUSTOMER_SALES_DISCOUNT_ACCOUNT_NOT_SET',array($customer->customerName."-".$customer->customerCode)),
                    );
                }else if($creditAmount > 0 && empty($customer->customerAdvancePaymentAccountID)){
                    $customerOverPaymentAccountIsSet = false;
                }
            }
        }

        if ($restFlag) {
            $entityID = $this->createEntityForRestApi($posUserID);
        } else {
            $entityID = $this->createEntity();
        }
        $paymentdata = array(
            'incomingPaymentCode' => $paymentCode,
            'incomingPaymentDate' => $date,
            'paymentTermID' => $paymentTerm,
            'incomingPaymentAmount' => $amount,
            'customerID' => $customerID,
            'incomingPaymentDiscount' => $discount,
            'incomingPaymentMemo' => $memo,
            'incomingPaymentType' => $paymentType,
            'locationID' => $locationID,
            'entityID' => $entityID,
            'incomingPaymentCreditAmount' => $creditAmount,
            'pos' => $pos,
            'incomingPaymentBalance' => $balance,
            'incomingPaymentPaidAmount' => $totalPaidAmount,
            'customCurrencyId' => $customCurrencyId,
            'incomingPaymentCustomCurrencyRate' => $customCurrencyRate,
        );
        $payment = new Payments;
        $payment->exchangeArray($paymentdata);

        $paymentID = $this->CommonTable('Invoice\Model\PaymentsTable')->savePayments($payment);
        if (!$paymentID) {
            return array(
                'state' => false,
                'msg' => $this->getMessage('ERR_CUSTPAY_API_SAVE'),
                'customerID' => $customerID
            );
        }

        $invTotalAmout = 0.00;
        $invoiceCodesForPayment = "";
        foreach ($invData as $key => $value) {
            $id = $key;
            $dimensionInvoiceId = $key;
            $invoiceValue = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($id);

            //before do anything need to check invoice location and payment location
            if($invoiceValue->locationID != $locationID){
                return array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_INV_LOCA_NOT_SET_PAY_LOC'),
                
                );       
            }
            
            if($invoiceValue->statusID == 10){
                return array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_STATUS_CLOSED',array($invoiceValue->salesInvoiceCode)),
                
                );       
            }
            $invoiceCodesForPayment = (empty($invoiceCodesForPayment)) ? $invoiceValue->salesInvoiceCode : $invoiceCodesForPayment.','.$invoiceValue->salesInvoiceCode;
            $invoiceValue->salesInvoiceCustomCurrencyRate = ($invoiceValue->salesInvoiceCustomCurrencyRate == 0) ? 1 : $invoiceValue->salesInvoiceCustomCurrencyRate;
            $setallocation = $value * $invoiceValue->salesInvoiceCustomCurrencyRate;
            $currencyGainAndLoss = $value * $customCurrencyRate - $value * $invoiceValue->salesInvoiceCustomCurrencyRate;

            $invTotalAmout+=$setallocation;
            $creditValue = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceID($id);
            $creditInvoiceAmount = 0;
            foreach ($creditValue as $val) {
                $datta = (object) $val;
                $creditInvoiceAmount+=$datta->creditNoteTotal;
            }

            $oldLeftToPayamount = $invoiceValue->salesinvoiceTotalAmount - $creditInvoiceAmount - $invoiceValue->salesInvoicePayedAmount;

            $invoicePayments = array(
                'incomingPaymentID' => $paymentID,
                'salesInvoiceID' => $id,
                'incomingInvoicePaymentAmount' => $setallocation,
                'incomingInvoicePaymentCurrencyGainOrLoss' => $currencyGainAndLoss,
            );

            $invPayment = new InvoicePayments;
            $invPayment->exchangeArray($invoicePayments);
            $invoicePaymentId = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->saveInvoicePayment($invPayment);
            if (!$invoicePaymentId) {
                return array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_CUSTPAY_API_INVOICE_PAYMENT_SAVE'),
                    'customerID' => $customerID
                );
            }
            if (round($oldLeftToPayamount, 2) > round($setallocation, 2) ){
                $state = 3;
            } else {
                $state = 4;
            }

            $currentStatusID = $invoiceValue->statusID;
            $totalPayedAmount = $invoiceValue->salesInvoicePayedAmount + $setallocation;
            $invoice = array(
                'salesInvoiceID' => $id,
                'salesInvoicePayedAmount' => $totalPayedAmount,
                'statusID' => $state,
                'customerID' => $customerID,
            );

            $invoiceData = new Invoice;
            $invoiceData->exchangeArray($invoice);
            $this->CommonTable('Invoice\Model\InvoiceTable')->updatePaymentOfInvoice($invoiceData);

            if ($state == 4) {
                $invoiceEditLogModel = new InvoiceEditLog();
                $invoiceEditLogModel->exchangeArray([
                    'userID' => $this->userID,
                    'salesInvoiceID' => $id,
                    'dateAndTime' => $this->getGMTDateTime()
                ]);
                $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);

                $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                $newItemsData = [
                    'invoiceEditLogDetailsOldState' => $currentStatusID,
                    'invoiceEditLogDetailsNewState' => 'Invoice closed by making a payment '.$paymentCode,
                    'invoiceEditLogDetailsCategory' => 'salesInvoiceStatusClosed',
                    'invoiceEditLogID' => $invoiceEditLastInsertID
                ];
                $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
            }
        }

        if($pos){
            $restBalance = $totalPaidAmount - $invTotalAmout;
        }

        $newCurrentBalance = $cCurrentbalancess - $invTotalAmout;
        // when come from pos, rest balance mark as a credit balance. that's why comment bellow line
        // $newCreditBalance = $cCurrentCredit - $creditAmount + $restBalance;

        $newCreditBalance = ($totalPaidAmount + $creditAmount) - $invTotalAmout;
        if ($newCreditBalance > 0) {
            $newCreditBalance = $cCurrentCredit + $newCreditBalance - $creditAmount;
        } else {
            $newCreditBalance = $cCurrentCredit - $creditAmount;
        }

        $customerupdate = array(
            'customerID' => $customerID,
            'customerCurrentBalance' => $newCurrentBalance,
            'customerCurrentCredit' => $newCreditBalance,
        );
        $customerData = new Customer;
        $customerData->exchangeArray($customerupdate);
        $customerUpdateResult = $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCurrentAndCreditBalance($customerData);
        if ($customerUpdateResult != TRUE) {
            return array(
                'state' => false,
                'msg' => $this->getMessage('ERR_CUSTPAY_API_CUST_UPDATE'),
                'customerID' => $customerID
            );
        }


        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

        // if ($displaySettings->smsServiceStatus) {
        //     if ($customer->customerTelephoneNumber != "") {
        //         $message = "Dear ".$customer->customerName.", Thank you for your payment of ".$customCurrencyData->currencySymbol." ".number_format($amount,2)." for the invoice(s) ".$invoiceCodesForPayment.". Remaining outstanding balance: ".$customCurrencyData->currencySymbol." ".number_format($newCurrentBalance,2);
        //         $sendRes = $this->sendSms($customer->customerTelephoneNumber, $message);
        //     }
        // }
        if ($displaySettings->smsServiceStatus) {
            if ($customer->customerTelephoneNumber != "") {
                $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("After Receipt");
                
                if($smsTypeDetails->isEnable && (int)$smsTypeDetails->smsSendType == 1){
                    $message = $smsTypeDetails->smsTypeMessage ."\n";

                    $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);
                    $salesPerson = '';
                    if( sizeof($salesPersonIDArr) > 0){
                        $salesPersonData =  $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByID($salesPersonIDArr[0])->current();
                        $salesPerson = $salesPersonData["salesPersonSortName"];
                    }
                    
                    if($smsTypeDetails->isIncludeInvoiceDetails){
                        while ($row = $smsIncludedDetailsByType->current()) {
                            if((bool) $row['isSelected']){
                                switch ($row['smsIncludedName']) {
                                    case "Payment Date":
                                        $message .=$row['smsIncludedName'] .' : '.$date.' ,';
                                        break;
                                    case "Paid Amount":
                                        $message .=$row['smsIncludedName'] .' : '.number_format($amount , 2)."\n";
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    $sendRes = $this->sendSms($customer->customerTelephoneNumber, $message);
                }
            }
        }

        //update credit note status and settled amount
        if($data_set['payedCreditNoteID'] && $creditAmount > 0 )
        {
            $this->updateCreditNoteByPayments($creditAmount,$data_set['payedCreditNoteID'],$data_set['creditValueExceed']);
        }

        $totalCRAmounts = 0;
        $accountProduct = array();
        $loyaltyDataValues = $this->CommonTable('CustomerLoyaltyTable')->getLoyaltyCardByCustID($customerID);

        foreach ($paymentMethods as $methodValue) {
            if($methodValue['paidAmount']>0){
                $cashPaymentId = '';
                $chequePaymentId = '';
                $creditCardPaymentId = '';
                $bankTransferId = '';
                $giftCardPaymentId = '';
                $lCPaymentId = '';
                $tTPaymentId = '';
                if ($methodValue['methodID'] == 1) {
                    $cashPayment = array(
                        'incomingPaymentMethodCashId' => '',
                        'cashAccountID' => $methodValue['cashAccountID'],
                        );
                    $cashPaymentData = new IncomingPaymentMethodCash;
                    $cashPaymentData->exchangeArray($cashPayment);
                    $cashPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodCashTable')->saveCashPayment($cashPaymentData);
                    if (!$cashPaymentId) {
                        return array(
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            );
                    }
//                update the cash in hand to current locations
                    $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($locationID)->current();
                    $newlocationCashInHand = $currentLocationDetails['locationCashInHand'] + $amount - $discount;
                    $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($locationID, $newlocationCashInHand);
                } else if ($methodValue['methodID'] == 2) {
                    $chequePayment = array(
                        'incomingPaymentMethodChequeNumber' => $methodValue['checkNumber'],
                        'incomingPaymentMethodChequeBankName' => $methodValue['bank'],
                        'postdatedChequeStatus' => $methodValue['postdatedStatus'],
                        'postdatedChequeDate' => ($methodValue['postdatedStatus'] == 1) ? $methodValue['postdatedDate'] : null,
                        'incomingPaymentMethodChequeAccNo' => $methodValue['chequeAccountID']
                        );
                    $chequePaymentData = new IncomingPaymentMethodCheque;
                    $chequePaymentData->exchangeArray($chequePayment);
                    $chequePaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->saveChequePayment($chequePaymentData);
                    if (!$chequePaymentId) {
                        return array(
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            );
                    }
                } else if ($methodValue['methodID'] == 3) {

                // update card payment account
                    if ($data_set['cardType'] && $data_set['cardType'] != '') {
                        $cardPaymentAccountID = $this->CommonTable('Expenses\Model\CardTypeTable')->getAccountIDByID($data_set['cardType'])->current();
                        $cardTypeAccount = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($cardPaymentAccountID['accountID']);
                        $newAccountBalance = $cardTypeAccount['accountBalance'] + ($methodValue['paidAmount'] * $customCurrencyRate - $discount) / $cardTypeAccount['currencyRate'];
                        $accountUpdated = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount(['accountBalance' => $newAccountBalance], $cardPaymentAccountID['accountID']);
                        if (!$accountUpdated) {
                            return array(
                                'state' => false,
                                'msg' => $this->getMessage('ERR_CUSPAY_NOT_UPDATE_CARD_PAYMENT_ACCOUNT'),
                                'customerID' => $customerID
                                );
                        }
                    }

                    $creditCardPayment = array(
                        'incomingPaymentMethodCreditCardNumber' => $methodValue['cardnumber'],
                        'incomingPaymentMethodCreditReceiptNumber' => $methodValue['reciptnumber'],
                        'cardAccountID' => ($cardPaymentAccountID['accountID']) ? $cardPaymentAccountID['accountID'] : null,
                        'cardTypeID' => $data_set['cardType'] ? $data_set['cardType'] : NULL
                        );
                    $creditCardPaymentData = new IncomingPaymentMethodCreditCard;
                    $creditCardPaymentData->exchangeArray($creditCardPayment);
                    $creditCardPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodCreditCardTable')->saveCreditCardPayment($creditCardPaymentData);
                    if (!$creditCardPaymentId) {
                        return array(
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            );
                    }
                } else if ($methodValue['methodID'] == 5) {
                    $bankTransferPayment = array(
                        'incomingPaymentMethodBankTransferBankId' => $methodValue['bankID'],
                        'incomingPaymentMethodBankTransferAccountId' => $methodValue['accountID'],
                        'incomingPaymentMethodBankTransferCustomerBankName' => $methodValue['customerBank'],
                        'incomingPaymentMethodBankTransferCustomerAccountNumber' => $methodValue['customerAccountNumber'],
                        'incomingPaymentMethodBankTransferRef' => $methodValue['bankTransferRef'],
                        );

                    $bankTransferPaymentData = new IncomingPaymentMethodBankTransfer;
                    $bankTransferPaymentData->exchangeArray($bankTransferPayment);
                    $bankTransferId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->saveBankTransferPayment($bankTransferPaymentData);
                    if (!$bankTransferId) {
                        return array(
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            );
                    } else {
                        if ($methodValue['accountID'] != '') {
                            $accountData = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($methodValue['accountID']);
                            $newAccountBalance = $accountData['accountBalance'] + (($methodValue['paidAmount'] * $customCurrencyRate) / $accountData['currencyRate']);
                            $accountUpdate = array(
                                'accountBalance' => $newAccountBalance,
                                );
                            $update = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount($accountUpdate, $methodValue['accountID']);
                            if (!$update) {
                                return array(
                                    'state' => false,
                                    'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                                    'customerID' => $customerID
                                    );
                            }
                        }
                    }
                } else if ($methodValue['methodID'] == 6) {
                    $giftCardId = $methodValue['giftCardID'];
                    $giftCardPayment = array(
                        'giftCardId' => $giftCardId,
                        );
                    $giftCardPaymentData = new IncomingPaymentMethodGiftCard;
                    $giftCardPaymentData->exchangeArray($giftCardPayment);
                    $giftCardPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodGiftCardTable')->saveGiftCardPayment($giftCardPaymentData);
                    if (!$giftCardPaymentId) {
                        return array(
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            );
                    } else {
                        $giftData = array(
                            'giftCardStatus' => 4,
                            );
                        $this->CommonTable('Inventory\Model\GiftCardTable')->updateGiftCardDetailsByGiftCardId($giftData, $giftCardId);
                    }
                } else if ($methodValue['methodID'] == 7) {
                    $LCPaymentReference = $methodValue['lcPaymentReference'];
                    $lCPayment = array(
                        'incomingPaymentMethodLCReference' => $LCPaymentReference,
                        );
                    $lCPaymentData = new IncomingPaymentMethodLC;
                    $lCPaymentData->exchangeArray($lCPayment);
                    $lCPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodLCTable')->saveLCPaymentMethod($lCPaymentData);
                    if (!$lCPaymentId) {
                        return array(
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            );
                    }
                } else if ($methodValue['methodID'] == 8) {
                    $TTPaymentReference = $methodValue['ttPaymentReference'];
                    $tTPayment = array(
                        'incomingPaymentMethodTTReference' => $TTPaymentReference,
                        );
                    $tTPaymentData = new IncomingPaymentMethodTT;
                    $tTPaymentData->exchangeArray($tTPayment);
                    $tTPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTTTable')->saveTTPaymentMethod($tTPaymentData);
                    if (!$tTPaymentId) {
                        return array(
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            );
                    }
                } else if ($methodValue['methodID'] == 9) {
                    $uniformVoucherPayment = array(
                        'voucherNumber' => (!empty($methodValue['voucherNumber'])) ? $methodValue['voucherNumber'] : null ,
                        'voucherAccountID' => (!empty($methodValue['voucherAccountID'])) ? $methodValue['voucherAccountID'] : null
                        );
                    $uniformVoucherPaymentData = new IncomingPaymentMethodUniformVoucher;
                    $uniformVoucherPaymentData->exchangeArray($uniformVoucherPayment);
                    $uniformVoucherPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodUniformVoucherTable')->saveUniformVoucherPayment($uniformVoucherPaymentData);
                    if (!$uniformVoucherPaymentId) {
                        return array(
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            );
                    }
                }

                $paymentMethodAmount = $methodValue['paidAmount'] * $customCurrencyRate;
                if($pos && ($methodValue['methodID'] == 1 || $methodValue['methodID'] == 3) && $restBalance > 0){
                    $paymentMethodAmount = $paymentMethodAmount - $restBalance;
                }
                $totalCRAmounts += $paymentMethodAmount;

                $paymentMethodData = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethodById($methodValue['methodID']);
                if($this->useAccounting == 1){
                    $selectedAccountID = null;
                    if(!($methodValue['methodID'] == 4 || $methodValue['methodID'] == 6)){
                        // then payment type is cash and user change Gl account by manualy.
                        if($methodValue['methodID'] == 1 && $methodValue['cashAccountID'] != ''){
                            $selectedAccountID = $methodValue['cashAccountID'];

                        } else if($methodValue['methodID'] == 2 && $methodValue['chequeAccountID'] != ''){
                            $selectedAccountID = $methodValue['chequeAccountID'];

                        } else if($methodValue['methodID'] == 3 && $methodValue['creditAccountID'] != ''){
                            $selectedAccountID = $methodValue['creditAccountID'];

                        } else if($methodValue['methodID'] == 5 && $methodValue['bankTransferAccountID'] != ''){
                            $selectedAccountID = $methodValue['bankTransferAccountID'];

                        } else if($methodValue['methodID'] == 7 && $methodValue['lcAccountID'] != ''){
                            $selectedAccountID = $methodValue['lcAccountID'];

                        } else if($methodValue['methodID'] == 8 && $methodValue['ttAccountID'] != ''){
                            $selectedAccountID = $methodValue['ttAccountID'];

                        } else if($methodValue['methodID'] == 9 && $methodValue['voucherAccountID'] != ''){
                            $selectedAccountID = $methodValue['voucherAccountID'];

                        } else {

                            if(empty($paymentMethodData['paymentMethodSalesFinanceAccountID'])){
                                return array(
                                    'state' => false,
                                    'msg' => $this->getMessage('ERR_PAY_PAYMETHOD_SALES_ACCOUNT_NOT_SET',array($paymentMethodData['paymentMethodName'])),
                                    );
                            } else {
                               $selectedAccountID =  $paymentMethodData['paymentMethodSalesFinanceAccountID'];
                            }

                        }
                        if(isset($accountProduct[$selectedAccountID]['debitTotal'])){
                            $accountProduct[$selectedAccountID]['debitTotal'] += $paymentMethodAmount;
                        }else{
                            $accountProduct[$selectedAccountID]['debitTotal'] = $paymentMethodAmount;
                            $accountProduct[$selectedAccountID]['creditTotal'] = 0.00;
                            $accountProduct[$selectedAccountID]['accountID'] = $selectedAccountID;
                        }

                    }else{
                        if($methodValue['methodID'] == 4){
                            if(empty($loyaltyDataValues->loyaltyGlAccountID)){
                                return array(
                                    'state' => false,
                                    'msg' => $this->getMessage('ERR_PAY_LOYALTY_CARD_GL_ACCOUNT_NOT_SET',array($loyaltyDataValues->customerLoyaltyCode)),
                                    );
                            }

                            if(isset($accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['debitTotal'])){
                                $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['debitTotal'] += $paymentMethodAmount;
                            }else{
                                $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['debitTotal'] = $paymentMethodAmount;
                                $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['creditTotal'] = 0.00;
                                $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['accountID'] = $loyaltyDataValues->loyaltyGlAccountID;
                            }

                        } else if($methodValue['methodID'] == 6){
                            $giftCardId = $methodValue['giftCardID'];
                            $giftCardData = $this->CommonTable('Inventory\Model\GiftCardTable')->getGiftCardDetailsByGiftCardID($giftCardId);
                            if(empty($giftCardData->giftCardGlAccountID)){
                                return array(
                                    'state' => false,
                                    'msg' => $this->getMessage('ERR_PAY_GIFTCARD_GL_ACCOUNT_NOT_SET',array($giftCardData->giftCardCode)),
                                    );
                            }

                            if(isset($accountProduct[$giftCardData->giftCardGlAccountID]['debitTotal'])){
                                $accountProduct[$giftCardData->giftCardGlAccountID]['debitTotal'] += $paymentMethodAmount;
                            }else{
                                $accountProduct[$giftCardData->giftCardGlAccountID]['debitTotal'] = $paymentMethodAmount;
                                $accountProduct[$giftCardData->giftCardGlAccountID]['creditTotal'] = 0.00;
                                $accountProduct[$giftCardData->giftCardGlAccountID]['accountID'] = $giftCardData->giftCardGlAccountID;
                            }
                        }
                    }
                }

                $paymentMethod = array(
                    'incomingPaymentId' => $paymentID,
                    'incomingPaymentMethodAmount' => $paymentMethodAmount,
                    'incomingPaymentMethodCashId' => $cashPaymentId,
                    'incomingPaymentMethodChequeId' => $chequePaymentId,
                    'incomingPaymentMethodCreditCardId' => $creditCardPaymentId,
                    'incomingPaymentMethodBankTransferId' => $bankTransferId,
                    'incomingPaymentMethodLoyaltyCardId' => $loyaltyCardId,
                    'incomingPaymentMethodGiftCardId' => $giftCardPaymentId,
                    'incomingPaymentMethodLCId' => $lCPaymentId,
                    'incomingPaymentMethodTTId' => $tTPaymentId,
                    'incomingPaymentMethodUniformVoucherId' => $uniformVoucherPaymentId,
                    'entityId' => $this->createEntity()
                    );

                $PaymentMethodData = new IncomingPaymentMethod;
                $PaymentMethodData->exchangeArray($paymentMethod);
                $paymentMethodID = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->savePaymentMethod($PaymentMethodData);
                if (!$paymentMethodID) {
                    return array(
                        'state' => false,
                        'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                        'customerID' => $customerID
                        );
                }
            }
        }

        // If customer has loyalty, then update customer's loyalty points
        if (isset($loyaltyData['cust_loyalty_id'])) {
            $data = [
                'customerLoyaltyID' => $loyaltyData['cust_loyalty_id'],
                'incomingPaymentID' => $paymentID,
                'collectedPoints' => $loyaltyData['earned_points'],
                'redeemedPoints' => $loyaltyData['redeemed_points'],
                'expired' => 0
            ];

            $history = new IncomingPaymentMethodLoyaltyCard();
            $history->exchangeArray($data);

            $loyaltyCardId = $this->CommonTable('IncomingPaymentMethodLoyaltyCardTable')->save($history);
            if (!$loyaltyCardId) {
                return array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                    'customerID' => $customerID
                    );
            }

                // Update total loyalty points of the customer
            $Lpoints = ((float) $loyaltyData['current_points'] + (float) $loyaltyData['earned_points']) - (float) $loyaltyData['redeemed_points'];
            $this->CommonTable('CustomerLoyaltyTable')->updateLoyaltyPoints($loyaltyData['cust_loyalty_id'], $Lpoints);

            if($this->useAccounting == 1 && $loyaltyDataValues->loyaltyID != '' && isset($loyaltyData['earned_points'])){
                if(empty($loyaltyDataValues->loyaltyGlAccountID)){
                    return array(
                        'state' => false,
                        'msg' => $this->getMessage('ERR_PAY_LOYALTY_CARD_GL_ACCOUNT_NOT_SET',array($loyaltyDataValues->customerLoyaltyCode)),
                    );
                }
                if(empty($glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID)){
                    return array(
                        'state' => false,
                        'msg' => $this->getMessage('ERR_PAY_LOYALTY_EXPENSE_GL_ACCOUNT_NOT_SET'),
                    );
                }

                if(isset($accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['creditTotal'])){
                    $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['creditTotal'] += $loyaltyData['earned_points'];
                }else{
                    $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['creditTotal'] = $loyaltyData['earned_points'];
                    $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['debitTotal'] = 0.00;
                    $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['accountID'] = $loyaltyDataValues->loyaltyGlAccountID;
                }

                if(isset($accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['debitTotal'])){
                    $accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['debitTotal'] += $loyaltyData['earned_points'];
                }else{
                    $accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['debitTotal'] = $loyaltyData['earned_points'];
                    $accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['creditTotal'] = 0.00;
                    $accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['accountID'] = $glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID;
                }
            }
        }

        if($this->useAccounting == 1){
            
            if ($currencyGainAndLoss > 0) {
                $overAmount = ($totalCRAmounts + $creditAmount) - ($invTotalAmout - $discount) - ($currencyGainAndLoss);
            } else {
                $overAmount = ($totalCRAmounts + $creditAmount) - ($invTotalAmout - $discount);

            }

            if($overAmount > 0 ){
                if(empty($customer->customerAdvancePaymentAccountID)){
                   return array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_CUSTOMER_ADVANCE_ACCOUNT_NOT_SET',array($customer->customerName."-".$customer->customerCode)),
                    );
                }

                if(isset($accountProduct[$customer->customerAdvancePaymentAccountID]['creditTotal'])){
                    $accountProduct[$customer->customerAdvancePaymentAccountID]['creditTotal'] += $overAmount;
                }else{
                    $accountProduct[$customer->customerAdvancePaymentAccountID]['creditTotal'] = $overAmount;
                    $accountProduct[$customer->customerAdvancePaymentAccountID]['debitTotal'] = 0.00;
                    $accountProduct[$customer->customerAdvancePaymentAccountID]['accountID'] = $customer->customerAdvancePaymentAccountID;
                }
            }

            if($creditAmount > 0 ){
                if($customerOverPaymentAccountIsSet){
                    if(isset($accountProduct[$customer->customerAdvancePaymentAccountID]['debitTotal'])){
                        $accountProduct[$customer->customerAdvancePaymentAccountID]['debitTotal'] += $creditAmount;
                    }else{
                        $accountProduct[$customer->customerAdvancePaymentAccountID]['debitTotal'] = $creditAmount;
                        $accountProduct[$customer->customerAdvancePaymentAccountID]['creditTotal'] = 0.00;
                        $accountProduct[$customer->customerAdvancePaymentAccountID]['accountID'] = $customer->customerAdvancePaymentAccountID;
                    }
                }else{
                    if(isset($accountProduct[$customer->customerReceviableAccountID]['debitTotal'])){
                        $accountProduct[$customer->customerReceviableAccountID]['debitTotal'] += $creditAmount;
                    }else{
                        $accountProduct[$customer->customerReceviableAccountID]['debitTotal'] = $creditAmount;
                        $accountProduct[$customer->customerReceviableAccountID]['creditTotal'] = 0.00;
                        $accountProduct[$customer->customerReceviableAccountID]['accountID'] = $customer->customerReceviableAccountID;
                    }
                }
            }

            
            if($discount > 0){
                if(isset($accountProduct[$customer->customerSalesDiscountAccountID]['debitTotal'])){
                    $accountProduct[$customer->customerSalesDiscountAccountID]['debitTotal'] += $discount;
                }else{
                    $accountProduct[$customer->customerSalesDiscountAccountID]['debitTotal'] = $discount;
                    $accountProduct[$customer->customerSalesDiscountAccountID]['creditTotal'] = 0.00;
                    $accountProduct[$customer->customerSalesDiscountAccountID]['accountID'] = $customer->customerSalesDiscountAccountID;
                }
                $totalCRAmounts += $discount;
            }
            if(isset($accountProduct[$customer->customerReceviableAccountID]['creditTotal'])){
                $accountProduct[$customer->customerReceviableAccountID]['creditTotal'] += $invTotalAmout;
            }else{
                $accountProduct[$customer->customerReceviableAccountID]['creditTotal'] = $invTotalAmout;
                $accountProduct[$customer->customerReceviableAccountID]['debitTotal'] = 0.00;
                $accountProduct[$customer->customerReceviableAccountID]['accountID'] = $customer->customerReceviableAccountID;
            }

            //if any gain or loss by currency, journal entry handle
            if ($currencyGainAndLoss < 0) {
                
                if(empty($glAccountData->glAccountSetupGeneralExchangeVarianceAccountID)){
                   return array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_GL_ACCOUNT_EXCHANGE_ACC_NOT_SET'),
                    );
                }
                
                if(isset($accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['debitTotal'])){
                    $accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['debitTotal'] += ($currencyGainAndLoss)* (-1);
                
                }else{
                    $accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['creditTotal'] = 0.00;
                    $accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['debitTotal'] = ($currencyGainAndLoss)* (-1);
                    $accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['accountID'] = $glAccountData->glAccountSetupGeneralExchangeVarianceAccountID;
                }
            } else if ($currencyGainAndLoss > 0) {
                if(empty($glAccountData->glAccountSetupGeneralExchangeVarianceAccountID)){
                   return array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_GL_ACCOUNT_EXCHANGE_ACC_NOT_SET'),
                    );
                }
                
                if(isset($accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['creditTotal'])){
                    $accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['creditTotal'] += ($currencyGainAndLoss);
                
                }else{
                    $accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['debitTotal'] = 0.00;
                    $accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['creditTotal'] = ($currencyGainAndLoss);
                    $accountProduct[$glAccountData->glAccountSetupGeneralExchangeVarianceAccountID]['accountID'] = $glAccountData->glAccountSetupGeneralExchangeVarianceAccountID;
                }
            } 
            
            $i=0;
            $journalEntryAccounts = array();


            foreach ($accountProduct as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Sales Payment '.$paymentCode.'.';
                $i++;
            }

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);
            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $date,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Sales Payment '.$paymentCode.'.',
                'documentTypeID' => 7,
                'journalEntryDocumentID' => $paymentID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
                );

            $resultData = $this->saveJournalEntry($journalEntryData, null,null, $restFlag, $locationID);
            if(!$resultData['status']){
                return array(
                    'state'=> false,
                    'msg'=>$resultData['msg'],
                    'data'=>$resultData['data'],
                );
            }

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($paymentID,7, 4);
            if(!$jEDocStatusUpdate['status']){
                return array(
                    'status'=> false,
                    'msg'=>$jEDocStatusUpdate['msg'],
                    );
            }

            if ($autoDimenison == "true") {
                $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(1,$dimensionInvoiceId);
                $dimensionData = [];
                foreach ($jEDimensionData as $value) {
                    if (!is_null($value['journalEntryID'])) {
                        $temp = [];
                        $temp['dimensionTypeId'] = $value['dimensionType'];
                        $temp['dimensionValueId'] = $value['dimensionValueID'];
                        $dimensionData[$paymentCode][] = $temp;
                    }
                }
            }

            if (!empty($dimensionData)) {
                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$paymentCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                if(!$saveRes['status']){
                    return array(
                        'status'=> false,
                        'msg'=>$saveRes['msg'],
                        'data'=>$saveRes['data'],
                        );
                }   
            }
        }

        $this->setLogMessage($customCurrencySymbol . $amount . ' payment added successfully - ' . $paymentCode);

        if ($locationReferenceID) {
            $this->updateReferenceNumber($locationReferenceID);
        }

        return array(
            'state' => true,
            'value' => $amount,
            'id' => $paymentID,
            'code' => $paymentCode,
        );
    }

    /** get customer Invoices from the database * */
    public function invoiceAction()
    {
        $id = $_GET['id'];
        $cCurrencyId = (int) $_GET['cid'];
        $customCurrencyId = ($cCurrencyId == 0 || $cCurrencyId == $this->companyCurrencyId) ? null : $cCurrencyId;

        $arr1 = str_split($id, 8);
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $dateFormat = $this->getUserDateFormat(); //user selcted dtae format to pass to the view
        if ($arr1[0] == "custome=") {
            $value = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByCustomerID($arr1[1], $locationID, $customCurrencyId);
            $creditValue = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCustomerIDAndLocationID($arr1[1], $locationID);
            foreach ($creditValue as $val) {
                $datta = (object) $val;
                $creditInvoiceAmount[$datta->invoiceID]+=$datta->creditNoteTotal;
            }
        } else if ($arr1[0] == "invoice=") {
            $value = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByInvoiceID($arr1[1]);
            $creditValue = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceID($arr1[1]);
            foreach ($creditValue as $val) {
                $datta = (object) $val;
                $creditInvoiceAmount[$datta->invoiceID]+=$datta->creditNoteTotal;
            }
        }
        $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentMethos as $t) {
            if($t['paymentMethodInSales'] == 1){
                $payMethod[$t['paymentMethodID']] = $t['paymentMethodName'];
            }
        }
        
        $paymenteview = new ViewModel(array(
            'payments' => $value,
            'creditInvoice' => $creditInvoiceAmount,
            'paymentMethod' => $payMethod,
            'dateFormat' => $dateFormat
                )
        );

        $paymenteview->setTerminal(true);
        $paymenteview->setTemplate('invoice/customer-payments-api/addPaymentList');
        if ($arr1[0] == "custome=") {
            $this->setLogMessage('Customer Invoice list accessed ');
        } else if ($arr1[0] == "invoice=") {
            $this->setLogMessage('Invoice accessed ');
        }
        return $paymenteview;
    }

    /** get Customer ADMINCurrent Balance from the database * */
    public function checkCreditAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $id = $request->getPost('customerID');
            $arr1 = str_split($id, 8);
            if ($arr1[0] == "custome=") {
                $custid = $arr1[1];
            } else if ($arr1[0] == "invoice=") {
                $value = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($arr1[1]);
                $custid = $value->customerID;
                $paymentTerm = $value->paymentTermID;
            }
            $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($custid);
            if ($customer == null) {
                $this->setLogMessage("Customer not fount by this id " . $custid, $custid);
                return new JsonModel(array(null));
            } else {
                if($customer->customerStatus == 2){
                    return new JsonModel(array("CustomerInactive" => true));
                }
                $cbalance = $customer->customerCurrentBalance;
                $ccredit = $customer->customerCurrentCredit;
                $customerName = $customer->customerName . ' - ' . $customer->customerShortName;
                if ($arr1[0] == "custome=") {
                    $paymentTerm = $customer->customerPaymentTerm;
                }
                $this->setLogMessage('Customer details accessed successfully  ' . $customerName);
                return new JsonModel(array($ccredit, $customerName, $paymentTerm, $cbalance));
            }
        } else {
            $this->setLogMessage("Credit check request is not a post request ");
            return new JsonModel(array(null));
        }
    }

    /** get payment detatils by Id* */
    public function getPaymentsAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\PaymentsTable')->fetchAll();
        while ($row = $re->current()) {
            $a[] = $row;
        }
        $this->setLogMessage('Payment list accessed ');
        return new JsonModel($a);
    }

    public function RetriveCustomerPaymentsAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $cust_id = trim($request->getPost('customerID'));
            $payType = $request->getPost('payType');
            if ($payType == 'cusPay' || $payType == 'POS') {
                $getpayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByCustomerID($cust_id, $locationID, $payType);
            } else {
                $getpayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByCustomerID($cust_id, $locationID);
            }
            $paymenteview = new ViewModel(array(
                'payments' => $getpayment,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                    )
            );
            $paymenteview->setTerminal(true);
            $paymenteview->setTemplate('invoice/customer-payments-api/payment-list');
            $this->setLogMessage('Customer payments list accessed');
            return $paymenteview;
        }
        $this->setLogMessage("Customer payments request is not post request");
        return false;
    }

    public function RetrivePaymentsAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
            $Payment = trim($request->getPost('PaymentID'));
            $getpayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByPaymentID($Payment);

            $paymenteview = new ViewModel(array(
                'payments' => $getpayment,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                    )
            );
            $paymenteview->setTerminal(true);
            $paymenteview->setTemplate('invoice/customer-payments-api/payment-list');
            $this->setLogMessage('payment accessed');
            return $paymenteview;
        }
        $this->setLogMessage("Customer payments request is not post request");
        return false;
    }

    public function deletePaymentAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $Paymentno = $request->getPost('payno');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $username = trim($request->getPost('username'));
            $password = trim($request->getPost('password'));
            $incomingPaymentCancelMessage = $request->getPost('incomingPaymentCancelMessage');
            $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
            if ($user) {
                if ($user->roleID == '1') {
                    $passwordData = explode(':', $user->userPassword);
                    $storedPassword = $passwordData[0];
                    $checkPassword = md5($password . $passwordData[1]);

                    if ($storedPassword == $checkPassword) {
                        $payment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentByID($Paymentno);
                        if ($payment->incomingPaymentStatus == 5) {
                            $this->rollback();
                            return new JsonModel(array('PAYMENTSTATUSERR',$this->getMessage('ERR_PAYMENT_ALREADY_CANCEL')));
                        }

                        $customerID = $payment->customerID;
                        $paymentType = $payment->incomingPaymentType;
                        $paymentTotal = $payment->incomingPaymentAmount;
                        $creditPayment = $payment->incomingPaymentCreditAmount;
                        $entityID = $payment->entityID;
                        $locationID = $payment->locationID;

                        $customerCredit = 0.00;
                        $customerOutStanding = 0.00;
                        $this->beginTransaction();

                        $accountProduct = array();
                        if ($this->useAccounting == 1) {
                            if($paymentType == 'invoice'){
                                $memo =  'Created By Sales Payment Delete '.$payment->incomingPaymentCode.'.';
                                $comment =  'Journal Entry is posted when delete Sales Payment '.$payment->incomingPaymentCode.'.';
                            }else if($paymentType == 'advance'){
                                $memo =  'Created By Sales Advance Payment Delete '.$payment->incomingPaymentCode;
                                $comment =  'Journal Entry is posted when delete Advance Sales Payment '.$payment->incomingPaymentCode.'.';
                            }
                            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('7', $Paymentno);
                            $journalEntryID = $journalEntryData['journalEntryID'];
                            $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);

                            $i=0;
                            $journalEntryAccounts = array();
                            foreach ($jEAccounts as $key => $value) {
                                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                                $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
                                $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
                                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
                                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
                                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = $memo;
                                $i++;
                            }

                    //get journal entry reference number.
                            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                            $jelocationReferenceID = $jeresult['locRefID'];
                            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                            $journalEntryData = array(
                                'journalEntryAccounts' => $journalEntryAccounts,
                                'journalEntryDate' => $this->convertDateToStandardFormat($journalEntryData['journalEntryDate']),
                                'journalEntryCode' => $JournalEntryCode,
                                'journalEntryTypeID' => '',
                                'journalEntryIsReverse' => 0,
                                'journalEntryComment' => $comment,
                                'documentTypeID' => 7,
                                'journalEntryDocumentID' => $Paymentno,
                                'ignoreBudgetLimit' => $ignoreBudgetLimit,
                                );

                            $resultData = $this->saveJournalEntry($journalEntryData, '7', $Paymentno);

                            if(!$resultData['status']){
                                $this->rollback();
                                return new JsonModel(array('JEERROR',$resultData['msg'],$resultData['data']));
                            }

                            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($Paymentno,7, 5);
                            if(!$jEDocStatusUpdate['status']){
                                $this->rollback();
                                return new JsonModel(array('JEERROR',$resultData['msg']));

                            }

                            $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(7,$Paymentno);
                            $dimensionData = [];
                            foreach ($jEDimensionData as $value) {
                                if (!is_null($value['journalEntryID'])) {
                                    $temp = [];
                                    $temp['dimensionTypeId'] = $value['dimensionType'];
                                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                                    $dimensionData[$payment->incomingPaymentCode][] = $temp;
                                }
                            }
                            if (!empty($dimensionData)) {
                                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$payment->incomingPaymentCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                if(!$saveRes['status']){
                                    return new JsonModel(array('JEERROR',$saveRes['msg']));
                                }   
                            }
                        }

                        if ($paymentType == 'invoice') {
                            $invoicepayment = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getInvoiceByPaymentID($Paymentno);
                            $creditValueInThisPayment = 0;
                            while ($results = $invoicepayment->current()) {
                                $result = (object) $results;
                                $invoiceID = $result->salesInvoiceID;
                                $totalInvoicePaymentamount = $result->incomingInvoicePaymentAmount;
                                $totalInvPaymentamount += $result->incomingInvoicePaymentAmount;
                                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                                $incomingPaymentPaidAmount = $result->incomingPaymentPaidAmount;
                                $invoiceOldPayAmount = $invoice->salesInvoicePayedAmount;
                                $invoiceNewPayAmount = $invoiceOldPayAmount - $totalInvoicePaymentamount;
                                $creditValueInThisPayment = $result->incomingPaymentCreditAmount;
                                
                                $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceID($invoiceID);
                                $creditNoteTotal = 0;
                                foreach ($creditNotes as $creditNoteValues) {
                                    $creditNoteValues = (object) $creditNoteValues;
                                    if($creditNoteValues->creditNotePaymentEligible==1){
                                        $creditNoteTotal+=$creditNoteValues->creditNotePaymentAmount;
                                    }
                                }
                                
                                $customerOutCreResults = ($totalInvoicePaymentamount - $creditNoteTotal );

                                if ($customerOutCreResults <= 0) {
                                    $status = 4;
                                    $customerCredit+=$totalInvoicePaymentamount;

                                } else {
                                    $status = 3;
                                    $customerOutStanding+= $customerOutCreResults;
                                    $customerCredit+=$creditNoteTotal;
                                }

                                $currentStatusID = $invoice->statusID;
                                $invoicedata = array(
                                    'salesInvoiceID' => $invoiceID,
                                    'salesInvoicePayedAmount' => $invoiceNewPayAmount,
                                    'statusID' => $status,
                                );

                                $inv = new Invoice();
                                $inv->exchangeArray($invoicedata);
                                $this->CommonTable('Invoice\Model\InvoiceTable')->updatePaymentOfInvoice($inv);

                                if ($status == 4) {
                                    $invoiceEditLogModel = new InvoiceEditLog();
                                    $invoiceEditLogModel->exchangeArray([
                                        'userID' => $this->userID,
                                        'salesInvoiceID' => $invoiceID,
                                        'dateAndTime' => $this->getGMTDateTime()
                                    ]);
                                    $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);

                                    $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                                    $newItemsData = [
                                        'invoiceEditLogDetailsOldState' => $currentStatusID,
                                        'invoiceEditLogDetailsNewState' => 'Invoice closed by deleting payment details',
                                        'invoiceEditLogDetailsCategory' => 'salesInvoiceStatusClosed',
                                        'invoiceEditLogID' => $invoiceEditLastInsertID
                                    ];
                                    $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                                    $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                                }
                            }

                            if ($customerID != null) {
                                $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                                $customerOldBalance = $customer->customerCurrentBalance;
                                $customerCurrentCredit = $customer->customerCurrentCredit;
                                $customerNewBalance = $customerOldBalance + $customerOutStanding;

                                $paidCreditAmount = 0;
                                if ($incomingPaymentPaidAmount > 0) {
                                    $paidCreditAmount = $incomingPaymentPaidAmount - $totalInvPaymentamount;
                                }

                                if (is_null($creditValueInThisPayment)) {
                                    $customerNewCreditBalance = floatval($customerCurrentCredit - $customerCredit -$paidCreditAmount) + floatval($creditValueInThisPayment);
                                } else {
                                    $customerNewCreditBalance = floatval($customerCurrentCredit - $customerCredit) + floatval($creditValueInThisPayment);
                                }
                               
                                if ($customerNewCreditBalance < 0) {
                                    $customerNewCreditBalance = 0.00;
                                }
                                $data = array(
                                    'customerCurrentBalance' => $customerNewBalance,
                                    'customerCurrentCredit' => $customerNewCreditBalance,
                                    'customerID' => $customerID,
                                );

                                $cust = new Customer();
                                $cust->exchangeArray($data);
                                $result = $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCurrentAndCreditBalance($cust);
                            }

                            $paymentMethod = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getPaymentMethodByPaymentId($Paymentno);

                            foreach ($paymentMethod as $method) {
                                $method = (object) $method;
                                if ($method->incomingPaymentMethodBankTransferId != '') {
                                    $payAmount = $method->incomingPaymentMethodAmount;
                                    $bankTransfer = (object) $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->getBankTransferDataByBankTransferId($method->incomingPaymentMethodBankTransferId)->current();
                                    $accountID = $bankTransfer->incomingPaymentMethodBankTransferAccountId;
                                    $accountData = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountID);

                                    $newAccountBalance = $accountData['accountBalance'] - $payAmount;
                                    $accountUpdate = array(
                                        'accountBalance' => $newAccountBalance,
                                    );
                                    $update = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount($accountUpdate, $accountID);
                                }
                                if ($method->incomingPaymentMethodCashId != '' && $method->incomingPaymentMethodCashId != 0) {
//                                    if cash payment then decrease the amount
                                    $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($payment->locationID)->current();
                                    $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] - $method->incomingPaymentMethodAmount;
                                    $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($payment->locationID, $newLocationCashInHand);
                                }
                                if ($method->incomingPaymentMethodCreditCardId != '' && $method->incomingPaymentMethodCreditCardId != 0) {
                                    // get card type ID from incomingPaymentMethodCreditCard table
                                    $creditCardDetails = $this->CommonTable('Invoice\Model\IncomingPaymentMethodCreditCardTable')->getByID($method->incomingPaymentMethodCreditCardId)->current();
                                    if (isset($creditCardDetails->cardAccountID)) {
                                        $cardTypeAccount = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($creditCardDetails->cardAccountID);
                                        $newAccountBalance = $cardTypeAccount['accountBalance'] - $method->incomingPaymentMethodAmount;
                                        $accountUpdated = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount(['accountBalance' => $newAccountBalance], $creditCardDetails->cardAccountID);
                                        if (!$accountUpdated) {
                                            $this->rollback();
                                            return new JsonModel(array('Card Account not update'));
                                        }
                                    }
                                }
                            }

                            $loyaltyData = $this->CommonTable('IncomingPaymentMethodLoyaltyCardTable')->getIncomingPaymentMethodLoyaltyCardByPaymentID($Paymentno);
                            
                            if ($loyaltyData) {
                                $customerLoyalty = $this->CommonTable('CustomerLoyaltyTable')->getLoyaltyCardByCustomerLoyaltyID($loyaltyData->customerLoyaltyID);
                                $newLoayltyPoints = floatval($customerLoyalty['customerLoyaltyPoints']) + floatval($loyaltyData->redeemedPoints) - floatval($loyaltyData->collectedPoints);
                                if ($newLoayltyPoints < 0) {
                                    $this->rollback();
                                    return new JsonModel(array('maxRedeem'));
                                }
                                $this->CommonTable('CustomerLoyaltyTable')->updateLoyaltyPoints($loyaltyData->customerLoyaltyID, $newLoayltyPoints);
                            }

                            $this->updateDeleteInfoEntity($entityID);
                            //update incomingPayment table 'status' column as cancelled
                            $this->_updatePaymentStatus($Paymentno, $incomingPaymentCancelMessage);
                            $this->setLogMessage('Payment '.$payment->incomingPaymentCode.' is deleted.');
                            $this->commit();
                            return new JsonModel(array(true));
                        } else if ($paymentType == 'advance') {
                            $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                            $customerOldBalance = $customer->customerCurrentCredit;
                            $customerNewBalance = $customerOldBalance - $paymentTotal;
                            if ($customerNewBalance < 0) {
                                $this->rollback();
                                return new JsonModel(array('chengeCredit'));
                            } else {
                                $data = array(
                                    'customerCurrentCredit' => $customerNewBalance,
                                    'customerID' => $customerID,
                                );
                                $cust = new Customer();
                                $cust->exchangeArray($data);
                                $result = $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCredit($cust);

                                $paymentMethod = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getPaymentMethodByPaymentId($Paymentno);

                                foreach ($paymentMethod as $method) {
                                    $method = (object) $method;
                                    if ($method->incomingPaymentMethodBankTransferId != '') {
                                        $payAmount = $method->incomingPaymentMethodAmount;
                                        $bankTransfer = (object) $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->getBankTransferDataByBankTransferId($method->incomingPaymentMethodBankTransferId)->current();
                                        $accountID = $bankTransfer->incomingPaymentMethodBankTransferAccountId;
                                        $accountData = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountID);

                                        $newAccountBalance = $accountData['accountBalance'] - $payAmount;
                                        $accountUpdate = array(
                                            'accountBalance' => $newAccountBalance,
                                        );
                                        $update = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount($accountUpdate, $accountID);
                                    } else if ($method->incomingPaymentMethodCashId != '' && $method->incomingPaymentMethodCashId != 0) {
//                                    if cash payment then decrease the amount
                                        $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($payment->locationID)->current();
                                        $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] - $method->incomingPaymentMethodAmount;
                                        $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($payment->locationID, $newLocationCashInHand);
                                    }
                                    if ($method->incomingPaymentMethodCreditCardId != '' && $method->incomingPaymentMethodCreditCardId != 0) {
                                        // get card type ID from incomingPaymentMethodCreditCard table
                                        $creditCardDetails = $this->CommonTable('Invoice\Model\IncomingPaymentMethodCreditCardTable')->getByID($method->incomingPaymentMethodCreditCardId)->current();
                                        $cardTypeAccount = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($creditCardDetails->cardAccountID);
                                        $newAccountBalance = $cardTypeAccount['accountBalance'] - $method->incomingPaymentMethodAmount;
                                        $accountUpdated = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount(['accountBalance' => $newAccountBalance], $creditCardDetails->cardAccountID);
                                        if (!$accountUpdated) {
                                            $this->rollback();
                                            return new JsonModel(array('Card Account not update'));
                                        }
                                    }
                                }
                                $this->updateDeleteInfoEntity($entityID);
                                $this->_updatePaymentStatus($Paymentno, $incomingPaymentCancelMessage);
                                $this->setLogMessage('Advance Payment '.$payment->incomingPaymentCode.' is deleted.');
                                $this->commit();
                                return new JsonModel(array(true));
                            }
                        }
                        $this->setLogMessage("Fail to delete payment details");
                        return new JsonModel(array(false));
                    } else {
                        return new JsonModel(array('pass'));
                    }
                } else {
                    return new JsonModel(array('admin'));
                }
            } else {
                return new JsonModel(array('user'));
            }
            $this->setLogMessage("Fail to delete payment details");
            return new JsonModel(array(false));
        }

        $this->setLogMessage("Payment delete request is not a post request");
        return new JsonModel(array(false));
    }

    private function _updatePaymentStatus($paymentID, $incomingPaymentCancelMessage)
    {
        $cancelStatusID = $this->getStatusID('cancelled');
        $data = array(
            'incomingPaymentStatus' => $cancelStatusID,
            'incomingPaymentCancelMessage' => $incomingPaymentCancelMessage
        );
        $this->CommonTable('Invoice\Model\PaymentsTable')->update($data, $paymentID);
    }

    public function getPaymentsByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($invrequest->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'
            ];
            $fromdate = $this->convertDateToStandardFormat($invrequest->getPost('fromdate'));
            $todate = $this->convertDateToStandardFormat($invrequest->getPost('todate'));
            $customerID = $invrequest->getPost('customerID');
            $payType = $invrequest->getPost('paymentType');
            if ($payType == 'cusPay' || $payType == 'POS') {
                $getpayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByDate($fromdate, $todate, $customerID, $locationID, $payType);
            } else {
                $getpayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByDate($fromdate, $todate, $customerID, $locationID);
            }

            $paymenteview = new ViewModel(array(
                'payments' => $getpayment,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                    )
            );
            $paymenteview->setTerminal(true);
            $paymenteview->setTemplate('invoice/customer-payments-api/payment-list');
            $this->setLogMessage('Date filter Payment list accsessed ');
            return $paymenteview;
        }
    }

    public function RetrivePaymentsForViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $payment = $request->getPost('payno');
            $value = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentByID($payment);
            $this->setLogMessage('Payment accsessed ');
            if ($value != NULL) {
                $in = array();
                $pvalue = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getInvoiceByPaymentID($payment);
                $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($value->customerID);
                $paymentmethods = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
                while ($t = $paymentmethods->current()) {
                    $t = (object) $t;
                    $paymethod[] = $t;
                }
                $curBal = 0;
                if ($cust != NULL) {
                    $curBal = $cust->currentBalance;
                    $cusAdd = $cust->address;
                    $custp = $cust->telephoneNumber;
                    $cusmail = $cust->email;
                } else {
                    $curBal = 0;
                    $cusAdd = NULL;
                    $custp = NULL;
                    $cusmail = NULL;
                }

                $companyres = $this->CommonTable('Invoice\Model\CompanyTable')->fetchAll();
                $com = NULL;
                if ($companyres != NULL) {
                    $com = $companyres->current();
                }

                $payterm = $this->CommonTable('Invoice\Model\PaymenttermsTable')->getPaymentterm($value->paymentTerm);
                if ($payterm != NULL) {
                    $paytermname = $payterm->term;
                } else {
                    $paytermname = 'Not specified';
                }

                $data = array();
                $pdata = array();
                $data['customerName'] = $cust->customerName;
                $data['customerID'] = $value->customerID;
                $data['customerAddress'] = ($cusAdd != NULL ) ? $cusAdd : NULL;
                $data['customertelephone'] = ($custp != NULL) ? $custp : NULL;
                $data['customerEmail'] = ($cusmail != NULL) ? $cusmail : NULL;
                $data['companyName'] = ($com != NULL) ? $com->companyName : NULL;
                $data['companyAddress'] = ($com != NULL) ? $com->companyAddress : NULL;
                $data['companyTelephone'] = ($com != NULL) ? $com->telephoneNumber : NULL;
                $data['companyEmail'] = ($com != NULL) ? $com->email : NULL;
                $data['companyReg'] = ($com != NULL) ? $com->trNumber : NULL;
                $data['issuedDate'] = $value->date;
                $data['payementTerm'] = $paytermname;
                if ($value->discount == null) {
                    $discount = 0.00;
                } else {
                    $discount = $value->discount;
                }
                $data['totalDiscount'] = $discount;
                $data['total'] = $value->amount;
                $data['comment'] = $value->memo;
                $data['paymentID'] = $value->paymentID;
                $data['paymentType'] = $value->paymentType;
                $data['currentBalance'] = $curBal;
                $InvoiceData[] = 0;
                while ($t = $pvalue->current()) {
                    $pdata[] = $t;
                    $payterm = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($t->invoiceID);
                    if ($payterm != '') {
                        $inv_data = array(
                            'totalAmount' => $payterm->totalAmount,
                            'dueDate' => $payterm->overDueDate,
                            'payedAmount' => $payterm->payedAmount,
                        );
                        $InvoiceData[$t->invoiceID] = $inv_data;
                    }
                }
                $data['InvoicePayment'] = $pdata;
                $data['Invoice'] = $InvoiceData;
                $data['paymethod'] = $paymethod;
                $data['cdnUrl'] = $this->cdnUrl;
                $this->setLogMessage('Payment related data accsessed for view ');
                return new JsonModel($data);
            }
            $this->setLogMessage("Wrong payment " . $payment);
            $in[0] = 'wrongquono';
            return new JsonModel($in);
        }
    }

    /** Update customer Advance Payment and Reference * */
    public function advancePaymentAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $result = $this->makeAdvancePayment($request->getPost());
            return new JsonModel(array(
                'state' => $result['state'],
                'msg' => $result['msg'],
                'data' => $result['data'],
                'id' => $result['id'],
                'customerID' => $result['customerID']
            ));
        } else {
            $this->setLogMessage("Advance payment add request is not a post request");
            return new JsonModel(array(
                'state' => false,
                'msg' => $payId . " failed to save payment"
            ));
        }
    }

    /*
    * This function is used to save advance payment
    * @param $advance Payment details
    */
    public function makeAdvancePayment($advDetails, $openingCreditFlag = false)
    {
        $refNumber = '';
        $payId = $advDetails['paymentID'];
        $locationID = $advDetails['locationID'];
        $ignoreBudgetLimit = $advDetails['ignoreBudgetLimit'];
        $referenceValues = $this->getLocationReferenceDetails('5', $locationID);
        if (!$referenceValues) {
            $referenceValues = $this->getLocationReferenceDetails('5', NULL);
        }
        if ($referenceValues) {
            $locationReferenceID = $referenceValues->locationReferenceID;
        }
        
        // check if a advance paymet from the same advance paymet Code exists if exist add next number to advance payment code
        while ($payId) {
            if ($this->CommonTable('Invoice\Model\PaymentsTable')->checkPaymentByCode($payId)->current()) {
                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                    $payId = $this->getReferenceNumber($locationReferenceID);
                } else {
                    return ['state'=> false, 'msg' => 'A Advance Payment Code already exists', 'data' => null];
                }
            } else {
                break;
            }
        }

        $this->beginTransaction();

        $currencyId = $advDetails['customCurrencyId'];
        $ccRate = $advDetails['customCurrencyRate'];
        $customCurrencyId = (empty($currencyId)) ? $this->companyCurrencyId : $currencyId;

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = $customCurrencyData->currencyRate;
        $customCurrencyRate = ($ccRate != 0 )? $ccRate : $customCurrencyRate;
        $customCurrencySymbol = $customCurrencyData->currencySymbol;

        $entityID = $this->createEntity();
        $date = $advDetails['date'];
        $amount = ((float) $advDetails['amount']) * $customCurrencyRate;
        $memo = $advDetails['memo'];
        $paymentType = $advDetails['paymentType'];
        $customerID = $advDetails['customerID'];
        $paymentMethods = $advDetails['paymentMethods'];
        $dimensionData = $advDetails['dimensionData'];


        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        $customerCredit = $cust->customerCurrentCredit;
        $totalCredit = $customerCredit + $amount;

        $paymentdata = array(
            'incomingPaymentCode' => $payId,
            'incomingPaymentDate' => $this->convertDateToStandardFormat($date),
            'paymentTermID' => 1,
            'incomingPaymentAmount' => $amount,
            'customerID' => $customerID,
            'incomingPaymentDiscount' => "0.00",
            'incomingPaymentMemo' => $memo,
            'incomingPaymentType' => $paymentType,
            'locationID' => $locationID,
            'pos' => 0,
            'entityID' => $entityID,
            'incomingPaymentStatus' => 4,
            'incomingPaymentPaidAmount' => $amount,
            'customCurrencyId' => $customCurrencyId,
            'incomingPaymentCustomCurrencyRate' => $customCurrencyRate,
        );


        $payment = new Payments;
        $payment->exchangeArray($paymentdata);
        $paymentID = $this->CommonTable('Invoice\Model\PaymentsTable')->savePayments($payment);
        if (!$paymentID) {
            $this->rollback();
            return ['state' => false, 'msg' => $this->getMessage('ERR_CUSTPAY_API_SAVE'), 'customerID' => $customerID];
        } else {
            $customerupdate = array(
                'customerID' => $customerID,
                'customerCurrentCredit' => $totalCredit,
            );
            $customerData = new Customer;
            $customerData->exchangeArray($customerupdate);
            if ($openingCreditFlag == false) {
                $customerUpdateResult = $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCredit($customerData);
                if ($customerUpdateResult != TRUE) {
                    $this->rollback();
                    return ['state' => false, 'msg' => $this->getMessage('ERR_CUSTPAY_API_CUST_UPDATE'), 'customerID' => $customerID];
                }
            }

            $accountProduct = array();
            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountData = $glAccountExist->current();

            if($customerID && $this->useAccounting == 1){
                if(empty($cust->customerAdvancePaymentAccountID)){
                    return [
                        'state' => false,
                        'msg' => $this->getMessage('ERR_CUSTOMER_ADVANCE_ACCOUNT_NOT_SET',array($cust->customerName."-".$cust->customerCode)),
                        ];
                }
            }

            $totalCRAmounts = 0;
            $accountProduct = array();
            foreach ($paymentMethods as $methodValue) {
                $cashPaymentId = false;
                $chequePaymentId = false;
                $creditCardPaymentId = false;
                if ($methodValue['methodID'] == 1) {
                    $cashPayment = array(
                        'incomingPaymentMethodCashId' => '',
                    );
                    $cashPaymentData = new IncomingPaymentMethodCash;
                    $cashPaymentData->exchangeArray($cashPayment);
                    $cashPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodCashTable')->saveCashPayment($cashPaymentData);
                    if (!$cashPaymentId) {
                        $this->rollback();
                        return [
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            ];
                    }
//                      update the cash in hand to current locations
                    $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($locationID)->current();
                    $newlocationCashInHand = $currentLocationDetails['locationCashInHand'] + $amount;
                    $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($locationID, $newlocationCashInHand);
                } else if ($methodValue['methodID'] == 2) {
                    $chequePayment = array(
                        'incomingPaymentMethodChequeNumber' => $methodValue['checkNumber'],
                        'incomingPaymentMethodChequeBankName' => $methodValue['bank'],
                        'postdatedChequeStatus' => $methodValue['postdatedStatus'],
                        'postdatedChequeDate' => ($methodValue['postdatedStatus'] == 1) ? $methodValue['postdatedDate'] : null
                    );
                    $chequePaymentData = new IncomingPaymentMethodCheque;
                    $chequePaymentData->exchangeArray($chequePayment);
                    $chequePaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->saveChequePayment($chequePaymentData);
                    if (!$chequePaymentId) {
                        $this->rollback();
                        return [
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                            ];
                    }
                } else if ($methodValue['methodID'] == 3) {

                    // update card payment account
                    if ($advDetails['cardType'] && $advDetails['cardType'] != '') {
                        $cardPaymentAccountID = $this->CommonTable('Expenses\Model\CardTypeTable')->getAccountIDByID($advDetails['cardType'])->current();
                        $cardTypeAccount = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($cardPaymentAccountID['accountID']);
                        $newAccountBalance = $cardTypeAccount['accountBalance'] + $methodValue['paidAmount'];
                        $accountUpdated = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount(['accountBalance' => $newAccountBalance], $cardPaymentAccountID['accountID']);
                        if (!$accountUpdated) {
                            $this->rollback();
                            return [
                                'state' => false,
                                'msg' => $this->getMessage('ERR_CUSPAY_NOT_UPDATE_CARD_PAYMENT_ACCOUNT'),
                                'customerID' => $customerID
                                ];
                        }
                    }

                    $creditCardPayment = array(
                        'incomingPaymentMethodCreditCardNumber' => $methodValue['cardnumber'],
                        'incomingPaymentMethodCreditReceiptNumber' => $methodValue['reciptnumber'],
                        'cardAccountID' => ($cardPaymentAccountID['accountID']) ? $cardPaymentAccountID['accountID'] : null,
                        'cardTypeID' => $advDetails['cardType'] ? $advDetails['cardType'] : NULL
                    );
                    $creditCardPaymentData = new IncomingPaymentMethodCreditCard;
                    $creditCardPaymentData->exchangeArray($creditCardPayment);
                    $creditCardPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodCreditCardTable')->saveCreditCardPayment($creditCardPaymentData);
                    if (!$creditCardPaymentId) {
                        $this->rollback();
                        return [
                                'state' => false,
                                'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                                'customerID' => $customerID
                                ];
                    }
                } else if($methodValue['methodID'] == 4){
                     $this->rollback();
                    return [
                        'state' => false,
                        'msg' => $this->getMessage('ERR_ADPAY_CANT_USE_LOYALTY_CARD'),
                        'customerID' => $customerID
                    ];
                } else if ($methodValue['methodID'] == 5) {
                    $bankTransferPayment = array(
                        'incomingPaymentMethodBankTransferBankId' => $methodValue['bankID'],
                        'incomingPaymentMethodBankTransferAccountId' => $methodValue['accountID'],
                        'incomingPaymentMethodBankTransferCustomerBankName' => $methodValue['customerBank'],
                        'incomingPaymentMethodBankTransferCustomerAccountNumber' => $methodValue['customerAccountNumber'],
                        'incomingPaymentMethodBankTransferRef' => $methodValue['bankTransferRef'],
                    );
                    $bankTransferPaymentData = new IncomingPaymentMethodBankTransfer;
                    $bankTransferPaymentData->exchangeArray($bankTransferPayment);
                    $bankTransferId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->saveBankTransferPayment($bankTransferPaymentData);
                    if (!$bankTransferId) {
                        $this->rollback();
                        return [
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                        ];
                    } else {
                        $accountData = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($methodValue['accountID']);

                        $newAccountBalance = $accountData['accountBalance'] + ((float) $methodValue['paidAmount'] * $customCurrencyRate);
                        $accountUpdate = array(
                            'accountBalance' => $newAccountBalance,
                        );
                        $update = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount($accountUpdate, $methodValue['accountID']);
                        if (!$update) {
                            $this->rollback();
                            return [
                                'state' => false,
                                'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                                'customerID' => $customerID
                            ];
                        }
                    }
                } else if ($methodValue['methodID'] == 6) {
                    $giftCardId = $methodValue['giftCardID'];
                    $giftCardPayment = array(
                        'giftCardId' => $giftCardId,
                    );
                    $giftCardPaymentData = new IncomingPaymentMethodGiftCard;
                    $giftCardPaymentData->exchangeArray($giftCardPayment);
                    $giftCardPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodGiftCardTable')->saveGiftCardPayment($giftCardPaymentData);
                    if (!$giftCardPaymentId) {
                        $this->rollback();
                        return [
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                        ];
                    } else {
                        $giftData = array(
                            'giftCardStatus' => 4,
                        );
                        $this->CommonTable('Inventory\Model\GiftCardTable')->updateGiftCardDetailsByGiftCardId($giftData, $giftCardId);
                    }
                } else if ($methodValue['methodID'] == 7) {
                    $LCPaymentReference = $methodValue['lcPaymentReference'];
                    $lCPayment = array(
                        'incomingPaymentMethodLCReference' => $LCPaymentReference,
                    );
                    $lCPaymentData = new IncomingPaymentMethodLC;
                    $lCPaymentData->exchangeArray($lCPayment);
                    $lCPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodLCTable')->saveLCPaymentMethod($lCPaymentData);
                    if (!$lCPaymentId) {
                        $this->rollback();
                       return [
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                        ];
                    }
                } else if ($methodValue['methodID'] == 8) {
                    $TTPaymentReference = $methodValue['ttPaymentReference'];
                    $tTPayment = array(
                        'incomingPaymentMethodTTReference' => $TTPaymentReference,
                    );
                    $tTPaymentData = new IncomingPaymentMethodTT;
                    $tTPaymentData->exchangeArray($tTPayment);
                    $tTPaymentId = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTTTable')->saveTTPaymentMethod($tTPaymentData);
                    if (!$tTPaymentId) {
                        $this->rollback();
                        return [
                            'state' => false,
                            'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
                            'customerID' => $customerID
                        ];
                    }
                }

                $paymentMethodAmount = $methodValue['paidAmount'] * $customCurrencyRate;
                $totalCRAmounts += $paymentMethodAmount;

                $paymentMethodData = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethodById($methodValue['methodID']);

                if($this->useAccounting == 1){

                    if(!($methodValue['methodID'] == 6 || $methodValue['methodID'] == 4)){

                             // then payment type is cash and user change Gl account by manualy.
                        if($methodValue['methodID'] == 1 && $methodValue['cashAccountID'] != ''){
                            $selectedAccountID = $methodValue['cashAccountID'];

                        } else if($methodValue['methodID'] == 2 && $methodValue['chequeAccountID'] != ''){
                            $selectedAccountID = $methodValue['chequeAccountID'];

                        } else if($methodValue['methodID'] == 3 && $methodValue['creditAccountID'] != ''){
                            $selectedAccountID = $methodValue['creditAccountID'];

                        } else if($methodValue['methodID'] == 5 && $methodValue['bankTransferAccountID'] != ''){
                            $selectedAccountID = $methodValue['bankTransferAccountID'];

                        } else if($methodValue['methodID'] == 7 && $methodValue['lcAccountID'] != ''){
                            $selectedAccountID = $methodValue['lcAccountID'];

                        } else if($methodValue['methodID'] == 8 && $methodValue['ttAccountID'] != ''){
                            $selectedAccountID = $methodValue['ttAccountID'];

                        } else {
                            if(empty($paymentMethodData['paymentMethodSalesFinanceAccountID'])){
                                $this->rollback();
                                return [
                                    'state' => false,
                                    'msg' => $this->getMessage('ERR_PAY_PAYMETHOD_SALES_ACCOUNT_NOT_SET',array($paymentMethodData['paymentMethodName'])),
                                ];
                            } else {
                               $selectedAccountID =  $paymentMethodData['paymentMethodSalesFinanceAccountID'];
                            }

                        }
                        if(isset($accountProduct[$selectedAccountID]['debitTotal'])){
                            $accountProduct[$selectedAccountID]['debitTotal'] += $paymentMethodAmount;
                        }else{
                            $accountProduct[$selectedAccountID]['debitTotal'] = $paymentMethodAmount;
                            $accountProduct[$selectedAccountID]['creditTotal'] = 0.00;
                            $accountProduct[$selectedAccountID]['accountID'] = $selectedAccountID;
                        }

                    }else{
                       if($methodValue['methodID'] == 6){
                            $giftCardId = $methodValue['giftCardID'];
                            $giftCardData = $this->CommonTable('Inventory\Model\GiftCardTable')->getGiftCardDetailsByGiftCardID($giftCardId);
                            if(empty($giftCardData->giftCardGlAccountID)){
                                $this->rollback();
                                return [
                                    'state' => false,
                                    'msg' => $this->getMessage('ERR_PAY_GIFTCARD_GL_ACCOUNT_NOT_SET',array($giftCardData->giftCardCode)),
                                ];
                            }

                            if(isset($accountProduct[$giftCardData->giftCardGlAccountID]['debitTotal'])){
                                $accountProduct[$giftCardData->giftCardGlAccountID]['debitTotal'] += $paymentMethodAmount;
                            }else{
                                $accountProduct[$giftCardData->giftCardGlAccountID]['debitTotal'] = $paymentMethodAmount;
                                $accountProduct[$giftCardData->giftCardGlAccountID]['creditTotal'] = 0.00;
                                $accountProduct[$giftCardData->giftCardGlAccountID]['accountID'] = $giftCardData->giftCardGlAccountID;
                            }
                        } else if($methodValue['methodID'] == 4){
                            if(empty($paymentMethodData['paymentMethodSalesFinanceAccountID'])){
                                $this->rollback();
                                return [
                                    'state' => false,
                                    'msg' => $this->getMessage('ERR_PAY_PAYMETHOD_SALES_ACCOUNT_NOT_SET',array($paymentMethodData['paymentMethodName'])),
                                ];
                            }
                            if(isset($accountProduct[$paymentMethodData['paymentMethodSalesFinanceAccountID']]['debitTotal'])){
                                $accountProduct[$paymentMethodData['paymentMethodSalesFinanceAccountID']]['debitTotal'] += $paymentMethodAmount;
                            }else{
                                $accountProduct[$paymentMethodData['paymentMethodSalesFinanceAccountID']]['debitTotal'] = $paymentMethodAmount;
                                $accountProduct[$paymentMethodData['paymentMethodSalesFinanceAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$paymentMethodData['paymentMethodSalesFinanceAccountID']]['accountID'] = $paymentMethodData['paymentMethodSalesFinanceAccountID'];
                            }
                        }
                    }
                }

                $paymentMethod = array(
                    'incomingPaymentId' => $paymentID,
                    'incomingPaymentMethodAmount' => ((float) $methodValue['paidAmount'] * $customCurrencyRate),
                    'incomingPaymentMethodCashId' => $cashPaymentId,
                    'incomingPaymentMethodChequeId' => $chequePaymentId,
                    'incomingPaymentMethodCreditCardId' => $creditCardPaymentId,
                    'incomingPaymentMethodBankTransferId' => $bankTransferId,
                    'incomingPaymentMethodGiftCardId' => $giftCardPaymentId,
                    'incomingPaymentMethodLCId' => $lCPaymentId,
                    'incomingPaymentMethodTTId' => $tTPaymentId,
                    'entityId' => $this->createEntity()
                );
                $PaymentMethodData = new IncomingPaymentMethod;
                $PaymentMethodData->exchangeArray($paymentMethod);
                $paymentMethodID = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->savePaymentMethod($PaymentMethodData);
                if (!$paymentMethodID) {
                    $this->rollback();
                    return [
                        'state' => false, 
                        'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'), 
                        'customerID' => $customerID
                    ];
                }
            }

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }

            if($this->useAccounting == 1){

                if(isset($accountProduct[$cust->customerAdvancePaymentAccountID]['creditTotal'])){
                    $accountProduct[$cust->customerAdvancePaymentAccountID]['creditTotal'] += $totalCRAmounts;
                }else{
                    $accountProduct[$cust->customerAdvancePaymentAccountID]['creditTotal'] = $totalCRAmounts;
                    $accountProduct[$cust->customerAdvancePaymentAccountID]['debitTotal'] = 0.00;
                    $accountProduct[$cust->customerAdvancePaymentAccountID]['accountID'] = $cust->customerAdvancePaymentAccountID;
                }

                $i=0;
                $journalEntryAccounts = array();

                foreach ($accountProduct as $key => $value) {
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Sales Advance Payment '.$payId.'.';
                    $i++;
                }

        //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $this->convertDateToStandardFormat($date),
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create Sales Advance Payment '.$payId.'.',
                    'documentTypeID' => 7,
                    'journalEntryDocumentID' => $paymentID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                    );

                $resultData = $this->saveJournalEntry($journalEntryData);
                if(!$resultData['status']){
                    $this->rollback();
                    return [
                        'state'=> false,
                        'msg'=>$resultData['msg'],
                        'data'=>$resultData['data'],
                    ];
                }


                if (!empty($dimensionData)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$payId], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);

                    if(!$saveRes['status']){
                        return array('status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']);
                    }   
                }

                $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($paymentID,7, 4);
                if(!$jEDocStatusUpdate['status']){
                    return array(
                        'status'=> false,
                        'msg'=>$jEDocStatusUpdate['msg'],
                        );
                }
            }
            $this->commit();
            $this->setLogMessage('Advance payment '. $payId.' is succsessfully saved');
            return [
                'state' => true,
                'value' => $payId,
                'id' => $paymentID,
                'msg' => $this->$payId . " payment succsessfully saved"//$this->getMessage('SUCC_CUSTPAY_API_PAYMENT_SAVE',array($payId ))
            ];
        }
    }

    public function addPaymentMethodsNumbersAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paymentID = $request->getPost('paymentID');
            $paymentdetails = json_decode($request->getPost('PaymentMethodDetails'));
            $paymentdetails[3];
            if ($paymentdetails[1] == '3') {
                $data = array(
                    'paymentID' => $paymentID,
                    'paymentMethodID' => $paymentdetails[1],
                    'paymentMethodReferenceNumber' => $paymentdetails[2],
                    'paymentMethodBank' => '',
                    'paymentMethodCardID' => $paymentdetails[3],
                );
            }
            if ($paymentdetails[4] == '2') {
                $data = array(
                    'paymentID' => $paymentID,
                    'paymentMethodID' => $paymentdetails[4],
                    'paymentMethodReferenceNumber' => $paymentdetails[5],
                    'paymentMethodBank' => $paymentdetails[6],
                    'paymentMethodCardID' => '',
                );
            }
            $PaymentMethodsNumbers = new PaymentMethodsNumbers();
            $PaymentMethodsNumbers->exchangeArray($data);
            $valuess = $this->CommonTable('Invoice\Model\PaymentMethodsNumbersTable')->savePaymentMethodsNumbers($PaymentMethodsNumbers);
            if ($valuess) {
                return new JsonModel(array(true));
                if ($paymentdetails[1]) {
                    $this->setLogMessage('Payment method numbers detais sucssesfully saved ');
                } else if ($paymentdetails[5]) {
                    $this->setLogMessage('Payment method numbers detais sucssesfully saved ');
                }
            } else {
                return new JsonModel(array(false));
            }
        } else {
            $this->setLogMessage("Add payment method numbers request is not a post request");
            return new JsonModel(array(false));
        }
    }

    public function showUpdateAction()
    {
        $value = $_POST['value'];
        $viewmodel = new ViewModel(array(
            'value' => $value
        ));
        $viewmodel->setTerminal(true);
        return $viewmodel;
    }

    public function getCreditNoteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerID = $request->getPost('customerID');
            $a = array();
            $re = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCustomerID($customerID);
            $this->setLogMessage('Credit note list accessed ');
            if ($re) {
                while ($row = $re->current()) {
                    if ($row->state == 'New') {
                        $a[] = $row;
                    }
                }
                if ($a != null) {
                    return new JsonModel($a);
                } else {
                    $per[0] = 'error';
                    return new JsonModel($per);
                }
            } else {
                $per[0] = 'error';
                return new JsonModel($per);
            }
        }
    }

    public function getCreditNoteByIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $creditnote = $request->getPost('creditNote');
            $result = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByID($creditnote);
            $this->setLogMessage('Credit note id accessed sucssesfully ');
            return new JsonModel(array($result));
        } else {
            $this->setLogMessage("Credit note id access request is not a post request");
            return new JsonModel(array(""));
        }
    }

    public function checkInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invID = $request->getPost('invoiceID');
            $check = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invID);


            if ($check != '') {
                $value = 'true';
                $custid = $check->customerID;
                $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($custid);
                if($customer->customerStatus == 2){
                    return new JsonModel(array(
                        'state' => false,
                        'value' => 'inactiveCustomer',
                        'msg' => $this->getMessage('ERR_CUSTPAY_CUSTOMER_INACTIVE')
                    ));
                }
            } else {
                $value = 'false';
            }
            return new JsonModel(array(
                'state' => $value,
                'value' => $check
            ));
        }
    }

    public function getCustomerDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerShortName = trim($request->getPost('shortname'));
            $data = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByShortname($customerShortName);
            return new JsonModel(array(
                'value' => $data
            ));
        }
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = self::CUSTOMER_PAYMENT_DOCUMENT_TYPE;

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_CUSTPAY_API_EMAIL_SENT');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_CUSTPAY_API_EMAIL_SENT');
        return $this->JSONRespond();
    }

    /**
     * search customer Payments for dropdown
     * @return type
     */
    public function searchCustomerPaymentsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $posOrCusPay = $searchrequest->getPost('addFlag');
            if ($posOrCusPay == 'POS' || $posOrCusPay == 'cusPay') {
                $customerPayments = $this->CommonTable('Invoice\Model\PaymentsTable')->searchCustomerPaymentsForDropDown($locationID, $searchKey, $posOrCusPay);
            } else {
                $customerPayments = $this->CommonTable('Invoice\Model\PaymentsTable')->searchCustomerPaymentsForDropDown($locationID, $searchKey);
            }
            $customerPaymentsList = array();
            foreach ($customerPayments as $customerPayment) {
                $temp['value'] = $customerPayment['incomingPaymentID'];
                $temp['text'] = $customerPayment['incomingPaymentCode'];
                $customerPaymentsList[] = $temp;
            }

            $this->data = array('list' => $customerPaymentsList);
            return $this->JSONRespond();
        }
    }

    /**
     * search Payments for document dropdown
     * @return type
     */
    public function searchPaymentsForDocumentDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            $customerPayments = $this->CommonTable('Invoice\Model\PaymentsTable')->searchCustomerPaymentsForDropDown($locationID, $searchKey);
            $customerPaymentsList = array();
            foreach ($customerPayments as $customerPayment) {
                $temp['value'] = $customerPayment['incomingPaymentID'];
                $temp['text'] = $customerPayment['incomingPaymentCode'];
                $customerPaymentsList[] = $temp;
            }

            $this->setLogMessage("Retrive customer payment list for dropdown.");
            $this->data = array('list' => $customerPaymentsList);
            return $this->JSONRespond();
        }
    }

    /**
     * get all customer cheques
     * @return JsonModel
     */
    public function getCustomerChequePaymentsAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->data = array();

        if ($this->request->isPost()) {
            $searchKey = $this->getRequest()->getPost('searchKey');
            $cheques = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')
                    ->searchCustomerChequePayments($searchKey);
            if ($cheques->count() > 0) {
                $this->status = true;
                $this->msg = $cheques->count() . ' cheque payment(s) found';
                foreach ($cheques as $cheque) {
                    $list[] = array(
                        'value' => $cheque['incomingPaymentID'],
                        'text' => $cheque['incomingPaymentCode']
                    );
                }
                $this->data['list'] = $list;
            } else {
                $this->msg = 'cheque payments not found';
            }
        }
        return $this->JSONRespond();
    }

    /**
     * get customers cheques by given filter
     * @return ViewModel
     */
    public function getCustomersChequesByFilterAction()
    {
        if ($this->request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $cheques = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')
                    ->getCustomerCheques($postData['fromdate'], $postData['todate'], $postData['sortValue']);

            $chequeView = new ViewModel(array(
                'cheques' => $cheques,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $chequeView->setTerminal(true);
            $chequeView->setTemplate('invoice/customer-payments-api/cheque-list');
            return $chequeView;
        }
    }

    /**
     * get customers cheques by customerId
     * @return ViewModel
     */
    public function getCustomersChequesByCustomerIDAction()
    {
        if ($this->request->isPost()) {
            $customerId = $this->getRequest()->getPost('customerId');
            $cheques = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')
                    ->getCustomerChequesByCustomerId($customerId);

            $chequeView = new ViewModel(array(
                'cheques' => $cheques,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $chequeView->setTerminal(true);
            $chequeView->setTemplate('invoice/customer-payments-api/cheque-list');
            return $chequeView;
        }
    }

    /**
     * get customers cheques by paymentId
     * @return ViewModel
     */
    public function getCustomersChequesByPaymentIDAction()
    {
        if ($this->request->isPost()) {
            $paymentId = $this->getRequest()->getPost('paymentId');
            $cheques = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')
                    ->getCustomerChequesByPaymentId($paymentId);

            $chequeView = new ViewModel(array(
                'cheques' => $cheques,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $chequeView->setTerminal(true);
            $chequeView->setTemplate('invoice/customer-payments-api/cheque-list');
            return $chequeView;
        }
    }

    /**
     * @author User <prathap@thinkcube.com>
     */
    public function getCustomerLoyaltyCardAction()
    {
        if ($this->request->isPost()) {

            $loyaltyTable = $this->CommonTable('CustomerLoyaltyTable');
            $loyaltyCard = $loyaltyTable->getLoyaltyCardByCustomerID($this->request->getPost('customerID'));

            if ($loyaltyCard) {

                $res = array(
                    'cust_loyal_id' => $loyaltyCard['customerLoyaltyID'],
                    'code' => $loyaltyCard['customerLoyaltyCode'],
                    'available_points' => $loyaltyCard['customerLoyaltyPoints'],
                    'card_name' => $loyaltyCard['loyaltyName'],
                    'min' => $loyaltyCard['loyaltyActiveMinVal'],
                    'max' => $loyaltyCard['loyaltyActiveMaxVal'],
                    'earning_ratio' => $loyaltyCard['loyaltyEarningPerPoint'],
                    'redeem_ratio' => $loyaltyCard['loyaltyRedeemPerPoint'],
                );
                $this->data = $res;
                $this->status = true;
                return $this->JSONRespond();
            }
        }

        $this->status = false;
        return $this->JSONRespond();
    }

    //edit payment Methods details action
    public function editPaymentMethodsAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $paymentMethods = $request->getPost('paymentMethods');
            $paymentId = $request->getPost('paymentId');
            $paymentComment = $request->getPost('paymentComment');

            $dataPayment = array(
                'incomingPaymentMemo' => $paymentComment,
            );
            $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByPaymentID($paymentId)->current();

            $this->CommonTable('Invoice\Model\PaymentsTable')->update($dataPayment, $paymentId);

            foreach ($paymentMethods as $value) {
                $value = (object) $value;
                $id = $value->methodTypeID;
                if ($value->methodID == 2) {
                    $data = array(
                        'incomingPaymentMethodChequeNumber' => $value->checkNumber,
                        'incomingPaymentMethodChequeBankName' => $value->bank
                    );
                    $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->updateIncomingPaymentMethodCheque($data, $id);
                } else if ($value->methodID == 3) {
                    $data = array(
                        'incomingPaymentMethodCreditCardNumber' => $value->cardnumber,
                        'incomingPaymentMethodCreditReceiptNumber' => $value->reciptnumber
                    );
                    $this->CommonTable('Invoice\Model\IncomingPaymentMethodCreditCardTable')->updateCreditCardDetails($data, $id);
                } else if ($value->methodID == 5) {
                    $data = array(
                        'incomingPaymentMethodBankTransferBankId' => $value->bankID,
                        'incomingPaymentMethodBankTransferAccountId' => $value->accountID,
                        'incomingPaymentMethodBankTransferCustomerBankName' => $value->customerBank,
                        'incomingPaymentMethodBankTransferCustomerAccountNumber' => $value->customerAccountNumber,
                    );
                    $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->updateBankTransfer($data, $id);
                } else if ($value->methodID == 7) {
                    $data = array(
                        'incomingPaymentMethodLCReference' => $value->lcPaymentReference,
                    );
                    $this->CommonTable('Invoice\Model\IncomingPaymentMethodLCTable')->updateLCPaymentMethod($data, $id);
                } else if ($value->methodID == 8) {
                    $data = array(
                        'incomingPaymentMethodTTReference' => $value->ttPaymentReference,
                    );
                    $this->CommonTable('Invoice\Model\IncomingPaymentMethodTTTable')->updateTTPaymentMethod($data, $id);
                }
            }
            $this->setLogMessage('Payment '.$paymentData['incomingPaymentCode'].' is updated.');
            $this->status = true;
            $this->msg = $this->getMessage('SUC_PAY_METHOD_UPDATE');
            return $this->JSONRespond();
        }
    }

    /**
    * use to update credit note status
    */
    function updateCreditNoteByPayments($creditNoteSettledAmount,$creditNoteID,$cnValueExceed = false)
    {
        if($cnValueExceed){
            //calculate credit note settled amount
            $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNoteID)->current();
            $creditNoteUpdateData = array(
            'creditNoteSettledAmount' => $creditNoteSettledAmount + $creditNoteData['creditNoteSettledAmount'],
            'statusID' => 3,
            'creditNoteID' => $creditNoteID,
            );

        }else{
            $creditNoteUpdateData = array(
            'creditNoteSettledAmount' => $creditNoteSettledAmount,
            'statusID' => 4,
            'creditNoteID' => $creditNoteID,
            );
        }
        $creditNoteUpdate = new CreditNote;
        $creditNoteUpdate->exchangeArray($creditNoteUpdateData);
        $update = $this->CommonTable('Invoice\Model\CreditNoteTable')->updateCreditNoteSettleAmountAndStatusID($creditNoteUpdate);
        return $update;
    }

    public function getPaymentReceiptPathAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespond();
        }

        $documentType = self::CUSTOMER_PAYMENT_DOCUMENT_TYPE;
        $paymentId = $request->getPost('paymentId');
        $templateId = $request->getPost('templateId');
        $isInitialView = $request->getPost('isInitial');

        if (!$isInitialView) {
            return $this->JSONRespond();
        }

        try {
            $sm = $this->getServiceLocator();
            $tpl = $sm->get('Template');

            // If template ID is not passed, get default template
            if (!$templateId) {
                $defaultTpl = $tpl->getDefaultTemplate($documentType);
                $templateId = $defaultTpl['templateID'];
            }

            $companyName = $this->getSubdomain();
            $filename = $this->getDocumentFileName($documentType, $paymentId, $templateId);
            $file = $filename;
            $arr = str_split($file, 2);

            $paymentFolderPath = "/userfiles/" . md5($companyName) . "/documents/" . strtolower(preg_replace('/([^a-zA-Z0-9])*/', '', $documentType));
            $filePath = $paymentFolderPath . "/" . $arr[0] . "/" . $arr[1] . "/" . substr($file, 4) . '-original.html';

            $this->status = true;
            $this->data = $filePath;
            $this->msg = $this->getMessage('SUCC_ORIGINAL_PAY_RECEIPT');

            return $this->JSONRespondHtml();

        } catch (Exception $ex) {
            $this->msg = $this->getMessage('ERR_ORIGINAL_PAY_RECEIPT');
            return $this->JSONRespondHtml();
        }
    }

}

/////////////////// END OF Customer Payements API CONTROLLER \\\\\\\\\\\\\\\\\\\\




