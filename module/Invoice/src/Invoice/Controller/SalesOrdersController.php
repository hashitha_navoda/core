<?php

/**
 * @author Prathap <prathap@thinkcube.com>
 * This file contains Invoice related controller functions
 */

namespace Invoice\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Invoice\Form\SalesOrdersForm;
use Invoice\Form\ProductForm;
use Invoice\Form\RecurrentSalesOrderForm;
use Invoice\Form\AddCustomerForm;
use Invoice\Model\Reference;
use Core\Controller\CoreController;
use Core\Controller\Interfaces\DocumentInterface;
use Zend\I18n\Translator\Translator;

class SalesOrdersController extends CoreController implements DocumentInterface
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'salesorder_upper_menu';
    protected $userID;
    protected $user_session;
    protected $paginator;
    protected $company;
    private $_salesOrderViewData;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
    }

    public function confirmDeleteSalesorderAction()
    {
        $viewmodel = new ViewModel();
        $viewmodel->setTerminal(true);
        return $viewmodel;
    }

    public function createAction()
    {
        $this->setLogMessage("Add Sales Order Form Accessed");
        $this->getSideAndUpperMenus('Sales Orders', 'Create Sales Order', 'SALES');
        if ($this->params()->fromRoute('param1') == 'rec') {
            $salesOrderID = $this->params()->fromRoute('param2');
        } else {
            $quotationID = $this->params()->fromRoute('param1');
        }

        $locationID = $this->user_session->userActiveLocation["locationID"];
        $refData = $this->getReferenceNoForLocation(2, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }

// Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        //get custom currency list
        $currency = $this->getCurrencyListWithRates();
        //get price list
        $priceList = $this->getActivePriceList();

        $displaySetup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll()->current();

        $so_form = new SalesOrdersForm(array(
            'name' => 'sales_orders_form',
            'id' => 'so_no',
            'terms' => $terms,
            'salesPersons' => $salesPersons,
            'customCurrency' => $currency,
            'priceList' => $priceList,
        ));
        $so_form->get('so_no')->setValue($rid);
        $so_form->get('customCurrencyId')->setValue($displaySetup->currencyID);
        $so_form->get('so_no')->setAttribute('Disabled', TRUE);

//        $pro = $this->getProductJson();
        $userdateFormat = $this->getUserDateFormat(); //taking the user selected date format from the database

        $sales_orders = new ViewModel(array(
            'so_form' => $so_form,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'quotationID' => $quotationID,
            'salesOrderID' => $salesOrderID,
            'activeCusList' => $this->getActiveCustomerList(TRUE),
            'userDateFormat' => $userdateFormat, //passing the date format to the view
            'locationID' => $locationID
        ));


        $documenTypes = $this->getDocumentTypes();
        $dtypes = array();
        foreach ($documenTypes as $key => $val) {
            if ($key == 46) {
                $dtypes[$key] = $val;
            }
        }

        $documentReference = new ViewModel(array(
            'title' => 'Select Related Customer Order',
            'documentType' => $dtypes,
        ));

        $documentReference->setTemplate('/core/modal/document-reference.phtml');
        $sales_orders->addChild($documentReference, 'documentReference');



        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Sales Order Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Sales Order Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $sales_orders->addChild($refNotSet, 'refNotSet');
        }

        return $sales_orders;
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Sales Orders', 'View Sales Orders', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $salesOrderID = $this->params()->fromRoute('param1');
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }

// Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        //get custom currency list
        $currency = $this->getCurrencyListWithRates();
        //get price list
        $priceList = $this->getActivePriceList();

        $so_form = new SalesOrdersForm(array(
            'name' => 'salesOrder',
            'id' => 'so_no',
            'terms' => $terms,
            'salesPersons' => $salesPersons,
            'customCurrency' => $currency,
            'priceList' => $priceList
        ));
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);

        $userdateFormat = $this->getUserDateFormat(); /////////////

        $salesOrder = new ViewModel(array(
            'so_form' => $so_form,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'edit_ID' => $salesOrderID,
            'userdateFormat' => $userdateFormat,
            'currentLocationID' => $locationID
        ));

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $salesOrder->addChild($attachmentsView, 'attachmentsView');

        $documenTypes = $this->getDocumentTypes();
        $dtypes = array();
        foreach ($documenTypes as $key => $val) {
            if ($key == 46) {
                $dtypes[$key] = $val;
            }
        }

        $documentReference = new ViewModel(array(
            'title' => 'Select Related Customer Order',
            'documentType' => $dtypes,
        ));

        $documentReference->setTemplate('/core/modal/document-reference.phtml');
        $salesOrder->addChild($documentReference, 'documentReference');


        return $salesOrder;
    }

    public function listAction()
    {
        $this->setLogMessage("Sales Order View List accessed");
        $this->getSideAndUpperMenus('Sales Orders', 'View Sales Orders', 'SALES');

        $this->getPaginatedSalesOrders();
        $globaldata = $this->getServiceLocator()->get('config');
        $statuses = $globaldata['statuses'];

        $userdateFormat = $this->getUserDateFormat();
        $invoiceView = new ViewModel(array(
            'salesOrders' => $this->paginator,
            'statuses' => $statuses,
            'userdateFormat' => $userdateFormat,
            'companyCurrencySymbol' => $this->companyCurrencySymbol
                )
        );
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('invoice/sales-orders/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/sales-orders/document-view');
        $invoiceView->addChild($docHistoryView, 'docHistoryView');
        $invoiceView->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $invoiceView->addChild($attachmentsView, 'attachmentsView');

        $customerOrderView = new ViewModel();
        $customerOrderView->setTemplate('invoice/sales-orders/linked-customer-order-view');
        $invoiceView->addChild($customerOrderView, 'customerOrderView');

        return $invoiceView;
    }

    private function getPaginatedSalesOrders()
    {
        $location = $this->user_session->userActiveLocation["locationID"];
        $this->paginator = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll($location, true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function documentAction()
    {
        $documentType = 'Sales Order';
        $salesOrderID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        echo $this->generateDocument($salesOrderID, $documentType, $templateID);
        exit;
    }

    public function getDataForDocument($salesOrderID)
    {

        if (!empty($this->_salesOrderViewData)) {
            return $this->_salesOrderViewData;
        }

        $itemAttrDetails = $this->CommonTable('Settings\Model\ItemAttributeTable')->fetchAll();
        $itemAttrDetailArray = [];
        foreach ($itemAttrDetails as $value) {
            $itemAttrDetailArray[$value['itemAttributeID']] = $value['itemAttributeName'];
        }

        $data = $this->getDataForDocumentView();

        $salesOrderDetails = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderBySoID($salesOrderID, true)->current();
        $salesOrderDetails['salesOrderIssuedDate'] = $this->convertDateToUserFormat($salesOrderDetails['issuedDate']); // $salesOrderDetails['issuedDate'];
        $salesOrderDetails['salesOrderExpireDate'] = $this->convertDateToUserFormat($salesOrderDetails['expireDate']);
        $salesOrderDetails['salesOrderComment'] = $salesOrderDetails['comment'];
        $salesOrderDetails['salesOrderCode'] = $salesOrderDetails['soCode'];
        $salesOrderDetails['createdTimeStamp'] = $this->getUserDateTime($salesOrderDetails['createdTimeStamp']);

        $customCurrencyId = $salesOrderDetails['customCurrencyId'];

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId == "" || $customCurrencyId == 0) ? 1 : $salesOrderDetails['salesOrdersCustomCurrencyRate'];
        $customCurrencySymbol = ($customCurrencyId == "" || $customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->currencySymbol;
        $salesOrderDetails['customCurrencySymbol'] = $customCurrencySymbol;

        $salesOrderDetails = (object) $salesOrderDetails;
        $products = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductWithUomBySalesOrderID($salesOrderID);
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerAndPrimaryProfileByID($salesOrderDetails->customerID);

        //        Add customer Primary Profile address to customer address and email
        $customer->customerAddress = (($customer->customerProfileLocationNo) ? $customer->customerProfileLocationNo . ', ' : '') .
                (($customer->customerProfileLocationRoadName1) ? $customer->customerProfileLocationRoadName1 . ', ' : '') .
                (($customer->customerProfileLocationRoadName2) ? $customer->customerProfileLocationRoadName2 . ', ' : '') .
                (($customer->customerProfileLocationRoadName3) ? $customer->customerProfileLocationRoadName3 . ', ' : '') .
                (($customer->customerProfileLocationSubTown) ? $customer->customerProfileLocationSubTown . ', ' : '') .
                (($customer->customerProfileLocationTown) ? $customer->customerProfileLocationTown . ', ' : '') .
                (($customer->customerProfileLocationPostalCode) ? $customer->customerProfileLocationPostalCode . ', ' : '') .
                (($customer->customerProfileLocationPostalCode) ? $customer->customerProfileLocationPostalCode . ', ' : '') .
                ($customer->customerProfileLocationCountry) . '.';
        $customer->customerEmail = $customer->customerProfileEmail;


        // if customer is available, salesorder.customerName gets replaced
        $data = array_merge($data, (array) $salesOrderDetails, (array) $customer);

        // get template specific data for the document
        $templateDefaultOptions = array('product_table' => $this->getServiceLocator()->get('config')['template-data-table']['sales_order']);

        // get template options from database and override the default options
        $templateOptions = array_replace_recursive($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);

        $defaultFooterRows = [];
        foreach ($templateDefaultOptions['product_table']['rows'] as $row) {
            $defaultFooterRows[$row['name']] = $row;
        }

        $templateFooterRows = [];
        foreach ((array) $this->templateDetails['templateOptions']['product_table']['rows'] as $row) {
            $templateFooterRows[$row['name']] = $row;
        }

        $templateOptions['product_table']['rows'] = $templateFooterRows + $defaultFooterRows;

        $data['templateOptions'] = $templateOptions;

        // reorder columns
        $productsTableColumns = array_filter($templateOptions['product_table']['columns'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableColumns, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        // reorder rows
        $productsTableRows = array_filter($templateOptions['product_table']['rows'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableRows, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        $templateOptions['product_table']['columns'] = $productsTableColumns;
        $templateOptions['product_table']['rows'] = $productsTableRows;

        $allowedColumnsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['columns']);

        $allowedRowsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['rows']);


        // to prevent loading categories from the database in the following foreach loop
        $allCategories = array();
        if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
            $allCategories = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        }

        $subTotalWithoutDiscount = 0;
        $$totalDiscount = 0;
        foreach ($products as $productIndex => $product) {
            $product = (object) $product;
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID, $product->uomID);

            $qty = ($product->quantity == 0) ? 1 : $product->quantity; // as a precaution
            $item_tot_tax = ($product->taxTotal > 0) ? $this->getProductUnitPriceViaDisplayUom((($product->taxTotal / $customCurrencyRate) / $product->quantity), $productUom) : 0.00;
            $item_tax_string = ($item_tot_tax > 0) ? "Tax: " . $data['currencySymbol'] . number_format($item_tot_tax, 2) : "";
            if ($product->discountType == 'vl') {
                $item_dic_string = ($product->discount > 0) ? "Disc: " . $this->getProductUnitPriceViaDisplayUom(($product->discount / $customCurrencyRate), $productUom) . " " . $data['currencySymbol'] : "";
                $product->totalDiscount = $qty * ($product->discount / $customCurrencyRate);
            } else {
                $item_dic_string = ($product->discount > 0) ? "Disc: " . number_format($product->discount,2) . "%" : "";
                $product->totalDiscount = (($product->unitPrice / $customCurrencyRate) * $qty) / 100 * $product->discount;
            }


            $totalDiscount += floatval($product->totalDiscount);

            $productDescLine = "";

            //Product description line
            if ($product->soProductDescription != '' && $product->soProductDescription != NULL) {
                $productDescLine = "<br /><small class=\"description\"><span>(</span>" . $product->soProductDescription . "<span>)</span></small>";
                $productDiscription = $product->soProductDescription;

            } else {
                $productDiscription = '';
            }

            if ($salesOrderDetails->showTax == "1") {    // this must be rebuld by adding a column to the database.
                $sub_line_vals = array_filter(array($item_tax_string, $item_dic_string));
                if (!empty($sub_line_vals)) {
                    $sub_line = "<br /><small class=\"taxes\">(" . implode(",&nbsp;", $sub_line_vals) . ")</small>";
                }
                $product->unitPrice = ($product->unitPrice / $customCurrencyRate);

            } else {
                $product->unitPrice = (floatval($product->total / $customCurrencyRate) + floatval($product->totalDiscount)) / floatval($qty);
                $sub_line = ($product->discount > 0) ? "<br><small>(" . $item_dic_string . ")</small>" : "";
            }

            // get template options from database and override the default options
            $templateDefaultOptions = array(
                'categorize_products' => false
            );


            //use to set discount and unite price when product tax exists.
            $totalTaxValuePerItem = 0;

            if ($product->productTaxEligible == '1') {
                if ($product->discountType == 'pr' && $salesOrderDetails->showTax != "1") {
                    $totalTaxValuePerItem = ((floatval($product->taxTotal / $customCurrencyRate) * 100) / (100 - floatval($product->discount))) / floatval($product->quantity);
                    $product->unitPrice = floatval($totalTaxValuePerItem) + floatval($product->unitPrice / $customCurrencyRate);
                    $product->totalDiscount = ($product->unitPrice - (floatval($product->total / $customCurrencyRate) / floatval($product->quantity))) * floatval($product->quantity);
                }
            }

            $unitPrice = $this->getProductUnitPriceViaDisplayUom(($product->unitPrice), $productUom);
            $thisqty = $this->getProductQuantityViaDisplayUom($product->quantity, $productUom);
            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

            $lineItemDiscount = ($product->discount > 0) ? (($product->quotationProductDiscountType == 'vl') ? number_format($product->totalDiscount, 2) : $product->discount . '%') : '-';
            $itemTotalWithoutDiscount = (($product->discount * $product->quantity) + $product->taxTotal) / $customCurrencyRate;

            //calculate tax value per item
            $itemUnitTax = floatval($product->taxTotal / $customCurrencyRate)/ floatval($product->quantity);

            //calculate discount value per item
            $itemUnitDisc = floatval($product->totalDiscount / $customCurrencyRate)/ floatval($product->quantity);

            //calculate unit tax for real item price
            $realUnitTax = $this->calculateProductUnitTax($product->unitPrice, $product->productID);

            $attribute_01 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeOneID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_02 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeTwoID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_03 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeThreeID,$product->productID)->current()['itemAttributeValueDescription'];

            $product_record = array(
                'product_index' => $productIndex + 1,
                'product_code' => $product->productCode,
                'product_name' => $product->productName . $productDescLine . $sub_line,
                'product_description' => $productDiscription,
                'product_code_and_name' => $product->productCode . ' - ' . $product->productName . $productDescLine . $sub_line,
                'quantity' => $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'],
                'price' => number_format($unitPrice, 2),
                'price_w_tax' => number_format((floatval($product->unitPrice) + floatval($realUnitTax)),2 ),
                'discount' => number_format($product->totalDiscount, 2),
                'discount_per_or_val' => $lineItemDiscount,
                'total' => number_format(($product->total / $customCurrencyRate), 2),
                'total_wo_tx' => number_format(($product->total - $product->taxTotal) / $customCurrencyRate, 2),
                'total_wo_discounts' => number_format($itemTotalWithoutDiscount, 2),
                'attribute_01' => (!is_null($attribute_01)) ? $attribute_01 : '-',
                'attribute_02' => (!is_null($attribute_02)) ? $attribute_02 : '-',
                'attribute_03' => (!is_null($attribute_03)) ? $attribute_03 : '-',
            );

            $subTotalWithoutDiscount = $subTotalWithoutDiscount + floatval($itemTotalWithoutDiscount);

            $templateOptions = array_merge($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);
            // filter only the columns that need to be displayed
            $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

            // nest products under category if they should be categorized
            if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
                $categoryId = $product->categoryID;
                $records[$categoryId]['products'][] = $product_record;

                // add category name to the list
                if (!isset($records[$categoryId]['category'])) {
                    $records[$categoryId]['category'] = (isset($allCategories[$categoryId])) ? $allCategories[$categoryId] : ''; // change this to category name
                }
            } else {
                $records[0]['products'][] = $product_record;
            }

            $sub_total+= ($product->total / $customCurrencyRate);
            $product->unitPrice = $unitPrice;
            $product->itemQuntity = $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'];
            $data['productsList'][] = (array) $product;
        }

        //get all tax details
        $taxDetails = $this->CommonTable('Settings\Model\TaxCompoundTable')->fetchAll();
        $compoundTaxes = [];
        foreach ($taxDetails as $key => $taxValue) {
            $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;
        }
        
        $txTypes = array();
        $totalTax = 0;
        $normalTaxTotal = 0;
        $compoundTaxTotal = 0;
        $normalTaxesSet = array();
        $compoundTaxesSet = array();
        $cmTaxDetails = [];
        $nmlTaxDetails = [];
        $productsTax = $this->CommonTable('Invoice\Model\SalesOrderProductTaxTable')->getSalesOrderTax($salesOrderID);
        foreach ($productsTax as $productTax) {
            $thisTax = $productTax['soTaxAmount'] / $customCurrencyRate;
            if (!isset($txTypes[$productTax['taxName']])) {
                $txTypes[$productTax['taxName']] = $thisTax;
            } else {
                $txTypes[$productTax['taxName']] += $thisTax;
            }
            
            if ($productTax['taxType'] == 'v') {
                $normalTaxTotal += floatval($thisTax);
                if (!array_key_exists($productTax['id'], $nmlTaxDetails)) {
                    $normalTaxesSet[] = $productTax['taxName'];
                    $nmlTaxDetails[$productTax['id']] =  $productTax['id'];
                }
                
            } else if ($productTax['taxType'] == 'c') {
                $compoundTaxTotal += floatval($thisTax);
                if (!array_key_exists($productTax['id'], $cmTaxDetails)) {
                    $compoundTaxesSet[] = $productTax['taxName'];
                    $cmTaxDetails[$productTax['id']] =  $productTax['id'];   
                }
            }
            $totalTax += $thisTax;
        }

        $data['calculatableTaxesTotal'] = $calculatableTaxesTotal;
        $data['suspendedTaxesTotal'] = $suspendedTaxesTotal;
        $data['normalTaxTotal'] = number_format($normalTaxTotal, 2);
        $data['normalTaxesSet'] = (!empty($normalTaxesSet)) ? implode(', ', array_unique($normalTaxesSet)) : '';
        $data['compoundTaxesSet'] = (!empty($compoundTaxesSet)) ? implode(', ', array_unique($compoundTaxesSet)) : '';
        $data['compoundTaxTotal'] = number_format($compoundTaxTotal, 2);
        $data['calculatableTaxes'] = (!empty($calculatableTaxes)) ? implode(', ', array_unique($calculatableTaxes)) : '';
        $data['suspendableTaxes'] = (!empty($suspendableTaxes)) ? implode(', ', array_unique($suspendableTaxes)) : '';

        $tx_line = "";
        foreach ($txTypes as $t_name => $t_val) {
            $tx_line.=($tx_line == null) ? $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2) :
                    ",&nbsp;" . $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2);
        }

        $tx_line = trim($tx_line, "&nbsp;");

        $headers = array(
            'product_index' => array('label' => _(""), 'align' => 'left'),
            'product_code' => array('label' => _("Item Code"), 'align' => 'left'),
            'product_name' => array('label' => _("Item Name"), 'align' => 'left'),
            'product_code_and_name' => array('label' => _("Item Code & Name"), 'align' => 'left'),
            'product_description' => array('label' => _("Product Description"), 'align' => 'left'),
            'quantity' => array('label' => _("Qty"), 'align' => 'left'),
            'price' => array('label' => _("Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'price_w_tax' => array('label' => _("Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'discount' => array('label' => _("Total Discount (-)"), 'align' => 'right'),
            'discount_per_or_val' => array('label' => _("Discount (-)"), 'align' => 'right'),
            'total' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_tx' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_discounts' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'attribute_01' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeOneID]), 'align' => 'left'),
            'attribute_02' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeTwoID]), 'align' => 'left'),
            'attribute_03' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeThreeID]), 'align' => 'left'),
        );

        $footer_rows = array(
            'sub_total' => true,
            'discount' => true,
            'vat' => true,
            'total' => true
        );

                // filter only the headers that need to be displayed
        $headers = array_splice(array_replace(array_flip($allowedColumnsList), $headers), 0, count($allowedColumnsList));

        // filter only the footer rows that need to be displayed
        $footer_rows = array_splice(array_replace(array_flip($allowedRowsList), $footer_rows), 0, count($allowedRowsList));

        // set column width
        if (isset($templateOptions['product_table']['columns'])) {
            foreach ($templateOptions['product_table']['columns'] as $column) {
                $columnName = $column['name'];
                $columnWidth = $column['width'];

                // if column is in allowed list
                if (isset($headers[$columnName])) {
                    $headers[$columnName]['width'] = $columnWidth;
                }
            }
        }

        $product_record = count($product_record) ? $product_record : [];
        $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

        $data['table'] = array(
            'hide_table_header' => ($templateOptions['product_table']['hide_table_header'] === true || $templateOptions['product_table']['hide_table_header'] == 'true'),
            'hide_table_borders' => ($templateOptions['product_table']['hide_table_borders'] === true || $templateOptions['product_table']['hide_table_borders'] == 'true'),
            'headers' => $headers,
            'footers' => $footer_rows,
            'records' => $records,
        );

        $itemWiseDiscount = $totalDiscount;
        $totalDiscount += ($salesOrderDetails->totalDiscount / $customCurrencyRate);

        $data['comment'] = $salesOrderDetails->comment;
        $data['sub_total'] = $sub_total;
        $data['sub_total_wo_tax'] = $sub_total - $totalTax;
        $data['sub_total_wo_discounts'] = $subTotalWithoutDiscount;
        $data['total'] = $salesOrderDetails->totalAmount / $customCurrencyRate;
        $data['totalTax'] = number_format($totalTax, 2);
        $data['discount'] = number_format($totalDiscount, 2); // total discount received
        $data['discountStatement'] = _("Total discount received:");

        $data['show_tax'] = $salesOrderDetails->showTax == "1" ? true : false;
        $data['tax_record'] = "<strong>" . _("Tax:") . "</strong>&nbsp;";
        $data['tax_record_line'] = $tx_line;

        if ($data['customerShortName'] == 'DefaultCustomer') {
            $data['customerName'] = '';
        } else {
            $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];
        }


        $data["email"] = array(
            "to" => $customer->email,
            "subject" => "Sales Order from a company.",
            "body" => "Dear " . $customer->customerName . ",<br><br>Thank you for your Inquiry.<br><br>A Sales Order has been generated for you from " . $this->company->companyName . " and attached herewith.<br><br>Sales Order Number: " . $salesOrderDetails->soCode . "<br>Sales Order Amount: " . $salesOrderDetails->totalAmount / $customCurrencyRate . "<br><br>Looking forward to hearing from you soon.<br><br>Best Regards<br>ThinkCube" . $this->company->companyName
        );
        $data['cdnUrl'] = $this->cdnUrl;

        $tempTotalValue = 0;
        //use to create total discount and itemwise discount
        $item_count = sizeof($data['productsList']);
        for ($i = 0; $i < $item_count; $i++) {
            $tempTotalValue+=$data['productsList'][$i]['totalDiscount'];
        }

        return $this->_salesOrderViewData = $data;
    }

    public function getDocumentDataTable($salesOrderID, $documentSize = 'A4')
    {

        $data_table_vars = $this->getDataForDocument($salesOrderID);

        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/sales-orders/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    // Displays the overlay which loads an iframe inside (calls documentAction)
    public function salesOrdersViewAction()
    {
        return $this->documentPreviewAction();
    }

    public function documentPreviewAction()
    {
        $salesOrderID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($salesOrderID);
        $salesOrderCode = $data['soCode'];
        $data['salesOrderCode'] = $salesOrderCode;
        $data['total'] = number_format($data['total'], 2);
        $path = "/salesOrders/document/"; //.$salesOrderCode;
        $translator = new Translator();
        $createNew = $translator->translate('New Sales Order');
        $createPath = "/salesOrders/create";

        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Sales Order from " . $data['companyName'],
            "body" => <<<EMAILBODY

Dear {$data['customerName']}, <br /><br />

A new Sales Order has been generated and attached herewith. <br /><br />

<strong>Sales Order No:</strong> {$data['salesOrderCode']} <br />
<strong>Sales Order Amount:</strong> {$data['customCurrencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $documentType = 'Sales Order';

        $commonView = $this->getCommonPreview($data, $path, $createNew, $documentType, $salesOrderID, $createPath);
        $commonView->setTerminal(true);
        return $commonView;
    }

    public function recurrentSalesOrderAction()
    {
        $this->setLogMessage("Add Recurrent Sales Order form Accessed");
        $this->getSideAndUpperMenus('Sales Orders', 'Recurrent Sales Order', 'SALES');
        $recurrentSOID = $this->params()->fromRoute('param1');
        $salesOrder_form = new RecurrentSalesOrderForm(array(
            'name' => 'salesOrder',
            'id' => 'sales_order_no'));
        $salesOrder_form->get('state')->setValue("Active");
        $salesOrder = new ViewModel(array(
            'salesOrderForm' => $salesOrder_form,
            'salesOrderID' => $recurrentSOID,
            'sales_orders_list' => $this->getSalesOrdersIdsJson(),
        ));
        return $salesOrder;
    }

    public function recurentSalesOrderViewAction()
    {
        $this->setLogMessage("View Recurrent Sales Order List Accessed");
        $this->getSideAndUpperMenus('Sales Orders', 'View Recurrent Sales Orders', 'SALES');

        $this->getPaginatedRecurrentSalesOrder();
        $sales_order_form = new RecurrentSalesOrderForm(array(
            'name' => 'salesOrder',
            'id' => 'sales_order_no'));
        $recurrentSalesOrderView = new ViewModel(array(
            'salesOrederForm' => $sales_order_form,
            'rec_so_list' => $this->getRecurrentIDsJson("SalesOrder"),
            'cust_list' => $this->getCustomerNameJson(),
        ));

        $recurrentSalesOrderlist = new ViewModel(array(
            'recurrentSalesOrder' => $this->paginator
                )
        );
        $delete_recurrent_sales_order = new viewModel(array(
        ));
        $delete_recurrent_sales_order->setTemplate('/invoice/sales-orders-api/confirm-delete-recurrent-sales-order');
        $recurrentSalesOrderView->addChild($delete_recurrent_sales_order, 'delete_so');

        $recurrentSalesOrderlist->setTemplate('invoice/sales-orders-api/recurrent-sales-order-list');
        $recurrentSalesOrderView->addChild($recurrentSalesOrderlist, 'recurrent_so_list');
        return $recurrentSalesOrderView;
    }

    private function getPaginatedRecurrentSalesOrder()
    {
        $this->paginator = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->fetchAllSalesOrder(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function getCustomerNameJson()
    {
        $cust_obj = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerNameList();
        while ($row1 = $cust_obj->current()) {
            $cust_list[] = $row1["cust_name"];
        }
        return json_encode($cust_list);
    }

    //get all quotation id list for typerhead
    public function getQuotationIdsJson()
    {
        $qot_obj = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotationIdList();
        while ($row = $qot_obj->current()) {
            $quot_list[] = $row['id'];
        }
        return json_encode($quot_list);
    }

    //get all recurrent sales orders list for typerheads
    public function getRecurrentIDsJson($type)
    {
        $rec_so = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceList($type);
        while ($row = $rec_so->current()) {
            $recurrent_so_list[] = $row['id'];
        }
        return json_encode($recurrent_so_list);
    }

    //get product list and make two json arrays for typerheads.
    public function getProductJson()
    {
        $pr_boject = $this->CommonTable('Invoice\Model\ProductTable')->getProductNames();
        while ($row = $pr_boject->current()) {
            $pro_ids[] = $row['id'];
            $pro_ids[] = $row['id'];
            $pro_name[] = $row['productName'];
            $pro_details[$row['id']] = $row;
            $pro_name_map[$row['productName']] = $row['id'];
        }
        return array(
            "pro_ids" => json_encode($pro_ids),
            "pro_name" => json_encode($pro_name),
            "pro_details" => json_encode($pro_details),
            "pro_name_map" => json_encode($pro_name_map)
        );
    }

    //get sales Orders ids in json format for typer head.
    public function getSalesOrdersIdsJson()
    {
        $so_id_obj = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderIds();
        while ($row = $so_id_obj->current()) {
            $sales_orders_list[] = $row['id'];
        }
        return json_encode($sales_orders_list);
    }

    public function documentPdfAction()
    {
        $salesOrderID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Sales Order';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/invoice/sales-orders/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($salesOrderID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($salesOrderID, $documentType, $documentData, $templateID);

        return;
    }

}

/////////////////// END OF IMVOICE CONTROLLER \\\\\\\\\\\\\\\\\\\\