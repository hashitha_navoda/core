<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file customer payments related controller functions
 */

namespace Invoice\Controller;

use Invoice\Form\CustomerPaymentsForm;
use Invoice\Form\AddCustomerForm;
use Invoice\Form\AdvancePaymentsForm;
use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Model\Reference;
use Zend\Session\Container;
use Zend\I18n\Translator\Translator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;

/**
 * This is the Customer Payments controller class
 */
class CustomerPaymentsController extends CoreController
{

    /**
     * The CustomerPaymentsform is creat by this indexAction
     * @return \Zend\View\Model\ViewModel
     */
    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'customerpayments_upper_menu';
    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;
    protected $company;
    private $_paymentViewData;
    private $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }
    }

    /** customer payments form load * */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Payments', 'Create Payment', 'SALES');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(4, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $invoiceID = $this->params()->fromRoute('param1');
        $idInvoice = '';
        $customerID = '';
        if ($invoiceID == 'cust') {
            $invoiceID = '';
            $customerID = $this->params()->fromRoute('param2');
        } else {
            $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID, $locationID);
            if ($invoice) {
                $idInvoice = $invoice->salesInvoiceID;
            } else {
                $idInvoice = '';
                $invoiceID = '';
            }
        }
        $paymentterm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        while ($t = $paymentterm->current()) {

            switch ($t->paymentTermName) {
                case 'Cash Only':
                    $paymentTerm = 'Cash';
                    break;
                case 'Cheque Only':
                    $paymentTerm = 'Cheque';
                    break;
                case 'Card Only':
                    $paymentTerm = 'Card';
                    break;
                case 'Loyalty Card Only':
                    $paymentTerm = 'Loyalty Card';
                    break;
                case 'Bank Transfer Only':
                    $paymentTerm = 'Bank Transfer';
                    break;
                case 'Gift Card Only':
                    $paymentTerm = 'Gift Card';
                    break;
                case 'LC Only':
                    $paymentTerm = 'LC';
                    break;
                case 'TT Only':
                    $paymentTerm = 'TT';
                    break;
                case 'Uniform Voucher Only':
                    $paymentTerm = 'Uniform Voucher';
                    break;
                default:
                    $paymentTerm = null;
                    break;
            }

            if (!empty($paymentTerm)) {
                $ptData = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethodByName($paymentTerm);

                if ($ptData->paymentMethodInSales) {
                    $payterm[$t->paymentTermID] = $t->paymentTermName;
                }
            } else {
                if ($t->paymentTermName != "90Days LC") {
                    $payterm[$t->paymentTermID] = $t->paymentTermName;
                }

            }

        }


        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $payMethod = array();
        $payMethodAcc = array();
        $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentMethos as $t) {
            if($t['paymentMethodInSales'] == 1){
                $payMethod[$t['paymentMethodID']] = $t['paymentMethodName'];
                $payMethodAcc[$t['paymentMethodID']] = $t;
            }
        }

        $customer = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        while ($t = $customer->current()) {
            $t = (object) $t;
            $cust[$t->customerID] = $t->customerName;
        }

        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(false, array('bank.bankName' => 'ASC'));
        foreach ($banks as $t) {
            $t = (object) $t;
            $bank[$t->bankId] = $t->bankName;
        }

        $gCard = $this->CommonTable('Inventory\Model\GiftCardTable')->getAllOpenGiftCards();
        foreach ($gCard as $g) {
            $g = (object) $g;
            $giftCards[$g->giftCardId] = $g->giftCardCode;
        }

        $cardTypes = $this->CommonTable('Expenses\Model\CardTypeTable')->getAssignedCardTypes();
        foreach ($cardTypes as $cardType) {
            $cardTypeList[$cardType['cardTypeID']] = $cardType['cardTypeName'];
        }

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();

        $form2 = new CustomerPaymentsForm(array(
            'customer' => $cust,
            'paymentTerm' => $payterm,
            'banks' => $bank,
            'giftCards' => $giftCards,
            'paymentMethod' => $payMethod,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'companyCurrencyId' => $this->companyCurrencyId,
            'customCurrency' => $currency,
        ));

        $form2->get('save')->setValue('Add Payment');
        $form2->get('customCurrencyId')->setValue($this->companyCurrencyId);
        $form2->get('customCurrencyId')->setAttribute('data-baseid', $this->companyCurrencyId);
        /** create a viewModel with data
          customerPaymentForm,AdvancePaymentsForm,paymentReference number and advanceReference Number* */
        $dateFormat = $this->getUserDateFormat();
        $todayDateTime = $this->getUserDateTime();
        $todayDate = date('Y-m-d', strtotime($todayDateTime));

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');

        $index = new ViewModel(array(
            'form2' => $form2,
            'invoiceID' => $invoiceID,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'idInvoice' => $idInvoice,
            'selectedInvoiceCode' => $invoice->salesInvoiceCode,
            'customerID' => $customerID,
            'referenceNumber' => $rid,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'todayDate' => $todayDate,
            'dateFormat' => $dateFormat,
            'cardTypeList' => ($cardTypeList) ? $cardTypeList : [],
            'payMethodAcc' =>$payMethodAcc,
            'useAccounting' => $this->useAccounting
        ));

        $index->addChild($dimensionAddView, 'dimensionAddView');
        $check = 0;
        if ($rid == '' || $rid == NULL) {
            $check = 1;
            if ($lrefID == null) {
                $title = 'Payment Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Payment Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
        }
        if ($check == 1) {
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $index->addChild($refNotSet, 'refNotSet');
        }
        $this->setLogMessage('Payment view succsessfully accessed');
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/payments/viewPayments.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'getDisplaySetup',
            'getGiftCard'
        ]);
        return $index;
    }

    /** customer advance payments form load * */
    public function advancePaymentAction()
    {
        $this->getSideAndUpperMenus('Payments', 'Create Payment', 'SALES');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];

        $adRefData = $this->getReferenceNoForLocation(5, $locationID);
        $adRid = $adRefData["refNo"];
        $adLrefID = $adRefData["locRefID"];
        $dateFormat = $this->getUserDateFormat();

        $paymethod = array();
        $payMethodAcc = array();
        $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentMethos as $t) {
            if($t['paymentMethodInSales'] == 1){
                $paymethod[$t['paymentMethodID']] = $t['paymentMethodName'];
                $payMethodAcc[$t['paymentMethodID']] = $t;
            }
        }

        $customer = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        while ($t = $customer->current()) {
            $t = (object) $t;
            $cust[$t->customerID] = $t->customerName;
        }

        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(false, array('bank.bankName' => 'ASC'));
        foreach ($banks as $t) {
            $t = (object) $t;
            $bank[$t->bankId] = $t->bankName;
        }

        $gCard = $this->CommonTable('Inventory\Model\GiftCardTable')->getAllOpenGiftCards();
        foreach ($gCard as $g) {
            $g = (object) $g;
            $giftCards[$g->giftCardId] = $g->giftCardCode;
        }

        //get custom currecy list
        $customCurrency = $this->getCurrencyListWithRates();

        // card account
        $cardTypes = $this->CommonTable('Expenses\Model\CardTypeTable')->getAssignedCardTypes();
        foreach ($cardTypes as $cardType) {
            $cardTypeList[$cardType['cardTypeID']] = $cardType['cardTypeName'];
        }

        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";


        $form1 = new AdvancePaymentsForm(array(
            'customer' => $cust,
            'paymentMethod' => $paymethod,
            'banks' => $bank,
            'giftCards' => $giftCards,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'customCurrency' => $customCurrency,
        ));

        $form1->get('customCurrencyId')->setValue($this->companyCurrencyId);
        $form1->get('customCurrencyId')->setAttribute('data-baseid', $this->companyCurrencyId);

        /** create a viewModel with data
          AdvancePaymentsForm,paymentReference number and advanceReference Number* */
        $index = new ViewModel(array(
            'form1' => $form1,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'adReferenceNumber' => $adRid,
            'cusActiveList' => $this->getActiveCustomerList(TRUE),
            'cust_list' => $this->getCustomerNameJson(),
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'dateFormat' => $dateFormat,
            'cardTypeList' => ($cardTypeList) ? $cardTypeList : [],
            'payMethodAcc' => $payMethodAcc,
            'useAccounting' => $this->useAccounting
        ));
        $check = 0;
        if ($adRid == '' || $adRid == NULL) {
            $check = 1;
            if ($adLrefID == null) {
                $title = 'Advance Payment Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Advance Payment Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
        }
        if ($check == 1) {
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $index->addChild($refNotSet, 'refNotSet');
        }

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $index->addChild($dimensionAddView, 'dimensionAddView');

        $this->setLogMessage('Advence Payment view succsessfully accessed');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'getGiftCard',
            'location'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/payments/viewPayments.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/payments/advancePayments.js');
        return $index;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Payments', 'View Payments', 'SALES');
        $this->getPaginatedPayments();
        $dateFormat = $this->getUserDateFormat();

        $payments = new ViewModel(array(
            'payments' => $this->paginator,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat
        ));

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $payments->addChild($attachmentsView, 'attachmentsView');

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/payments/viewPayments.js');
        return $payments;
    }

    public function viewPosAction()
    {
        $this->getSideAndUpperMenus('Payments', 'View POS Payments', 'SALES');
        $this->getPaginatedPOSPayments();
        $dateFormat = $this->getUserDateFormat();

        $payments = new ViewModel(array(
            'payments' => $this->POSpaginator,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/payments/viewPayments.js');
        return $payments;
    }

    public function getPaginatedPayments()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Invoice\Model\PaymentsTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function getPaginatedPOSPayments()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->POSpaginator = $this->CommonTable('Invoice\Model\PaymentsTable')->fetchAll(true, $locationID, true);
        $this->POSpaginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->POSpaginator->setItemCountPerPage(10);
    }

    public function documentAction()
    {
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

        $documentType = 'Customer Payment';
        $paymentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }
        
        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $paymentID, $templateID);

        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForPaymentPreview($paymentID);

        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);

        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
                $data['cheque_table'] = $this->_getDocumentChequeTable($paymentID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])).'-cheque'));
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($paymentID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                break;
        }

        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    //payment method edit action
    public function editAction()
    {
        if( $this->params()->fromRoute('param2') == 'pos'){
            $this->getSideAndUpperMenus('Payments', 'View POS Payments', 'SALES');
        }else{
            $this->getSideAndUpperMenus('Payments', 'View Payments', 'SALES');
        }
        $paymentID = $this->params()->fromRoute('param1');
        $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByPaymentID($paymentID)->current();
        $paymentMethods = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getPaymentMethodAllDetailsByPaymentId($paymentID);

        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(false, array('bank.bankName' => 'ASC'));
        foreach ($banks as $t) {
            $t = (object) $t;
            $bank[$t->bankId] = $t->bankName;
        }

        $gCard = $this->CommonTable('Inventory\Model\GiftCardTable')->getAllOpenGiftCards();
        foreach ($gCard as $g) {
            $g = (object) $g;
            $giftCards[$g->giftCardId] = $g->giftCardCode;
        }

        $currencyRate = 1;
        if ($paymentData['incomingPaymentCustomCurrencyRate'] > 0) {
            $currencyRate = $paymentData['incomingPaymentCustomCurrencyRate'];
        }
        $currencySymbol = $this->companyCurrencySymbol;
        if ($paymentData['customCurrencyId'] != '') {
            $currencySymbol = $paymentData['currencySymbol'];
        }
        $paymentEdit = new ViewModel(array(
            'paymentMethods' => $paymentMethods,
            'banks' => $bank,
            'giftCards' => $giftCards,
            'paymentCode' => $paymentData['incomingPaymentCode'],
            'paymentId' => $paymentID,
            'comment' => $paymentData['incomingPaymentMemo'],
            'currencyRate' => $currencyRate,
            'currencySymbol' => $currencySymbol,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/payments/editPayments.js');
        return $paymentEdit;
    }

    private function _getDataForPaymentPreview($paymentID)
    {

        if (!empty($this->_paymentViewData)) {
            return $this->_paymentViewData;
        }

        // Get company details
        $data = $this->getDataForDocumentView();

        $paymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getAllPaymentDetailsByID($paymentID)->current();

//        $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($paymentDetails['customerID']);
        $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerAndPrimaryProfileByID($paymentDetails['customerID']);
        $customCurrencyId = $paymentDetails['customCurrencyId'];
//get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "") ? $paymentDetails['incomingPaymentCustomCurrencyRate'] : 1;
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $data['currencySymbol'];
        $data['currencySymbol'] = $customCurrencySymbol;

//        Add Customer Primary data to customer address and email
        if ($paymentDetails['customerID'] != '') {
            $customerDetails->customerAddress = (($customerDetails->customerProfileLocationNo) ? $customerDetails->customerProfileLocationNo . ', ' : '') .
                    (($customerDetails->customerProfileLocationRoadName1) ? $customerDetails->customerProfileLocationRoadName1 . ', ' : '') .
                    (($customerDetails->customerProfileLocationRoadName2) ? $customerDetails->customerProfileLocationRoadName2 . ', ' : '') .
                    (($customerDetails->customerProfileLocationRoadName3) ? $customerDetails->customerProfileLocationRoadName3 . ', ' : '') .
                    (($customerDetails->customerProfileLocationSubTown) ? $customerDetails->customerProfileLocationSubTown . ', ' : '') .
                    (($customerDetails->customerProfileLocationTown) ? $customerDetails->customerProfileLocationTown . ', ' : '') .
                    (($customerDetails->customerProfileLocationPostalCode) ? $customerDetails->customerProfileLocationPostalCode . ', ' : '') .
                    (($customerDetails->customerProfileLocationPostalCode) ? $customerDetails->customerProfileLocationPostalCode . ', ' : '') .
                    ($customerDetails->customerProfileLocationCountry) . '.';
            $customerDetails->customerEmail = $customerDetails->customerProfileEmail;
        }
        $paymentDetails['incomingPaymentDate'] = $this->convertDateToUserFormat($paymentDetails['incomingPaymentDate']);
        $paymentDetails['createdTimeStamp'] = $this->getUserDateTime($paymentDetails['createdTimeStamp']);

        $data = array_merge($data, $paymentDetails, (array) $customerDetails);

        // to be used by below functions - convert to object
        $paymentDetails = (object) $paymentDetails;

        // Use core date function
        $data['today_date'] = gmdate('Y-m-d');
        $invoiceDetails = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getAllInvoiceDetailsByPaymentID($paymentID);
        $paymentmethods = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();

        while ($row = $paymentTerms->current()) {
            $id = $row->id;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->term;
        }

        $cr_payment = 0;

        while ($t = $paymentmethods->current()) {
            $t = (object) $t;
            $paymethod[] = $t;
        }

        $inv_no_list = null;
        while ($row = $invoiceDetails->current()) {
            if (sizeof($invoiceDetails) == 1) {
                if (!empty($row['jobID'])) {
                    $jobDetails = (array) $this->CommonTable('Jobs\Model\JobTable')->getServiceJobById($row['jobID']);
                    $data['jobCode'] = $jobDetails['jobReferenceNumber'];
                    $data['vehicleRegNo'] = $jobDetails['serviceVehicleRegNo'];
                    $data['vehicleType'] = $jobDetails['vehicleTypeName'];
                    $data['mileage'] = $jobDetails['jobVehicleKMs'];
                    $data['jobInTime'] = $this->getUserDateTime($jobDetails['createdTimeStamp']);
                    $data['jobOutTime'] = $this->getUserDateTime($jobDetails['completed_at']);
                }
            }

            $data['invoices'][] = $row;
            $ccrate = ($row['salesInvoiceCustomCurrencyRate'] != 0) ? $row['salesInvoiceCustomCurrencyRate'] : 1;
            if ($paymentDetails->incomingPaymentType == "invoice") {
                $rec[] = array(
                    $row["salesInvoiceCode"],
                    number_format(abs($row["salesinvoiceTotalAmount"] / $ccrate), 2),
                    number_format(abs($row["salesInvoicePayedAmount"] / $ccrate - $row["incomingInvoicePaymentAmount"] / $ccrate), 2),
                    number_format(abs($row["salesinvoiceTotalAmount"] - $row["salesInvoicePayedAmount"]), 2),
                    number_format(abs($row["incomingInvoicePaymentAmount"] / $ccrate), 2)
                );
                $inv_no_list .=$row["invoiceID"] . "&nbsp;";
            }
        }

        if ($paymentDetails->incomingPaymentType == "advance") {
            $rec[] = array(
                $customerDetails->customerName . '-' . $customerDetails->customerShortName,
                number_format($paymentDetails->incomingPaymentAmount / $customCurrencyRate, 2)
            );
        }


        $data['type'] = "receipt";
        $data['cust_name'] = $customerDetails->customerName;
        $data['doc_data'] = array(
            "Date" => $paymentDetails->incomingPaymentDate,
            "payment terms" => $terms[$paymentDetails->paymentTermName],
            "payment No." => $paymentDetails->incomingPaymentCode
        );
        $data['cust_data'] = array(
            "address" => $customerDetails->address,
        );
        if ($paymentDetails->incomingPaymentType == "invoice") {
            $data['table'] = array(
                'col_size' => array("5", "10", "10", "10", "10", "10", "10", "10"),
                'headers' => array(
                    "invoice No " => 2,
                    "Invoice amount </br>" . "(" . $customCurrencySymbol . ")" => 1,
                    "Previously paid amount </br>" . "(" . $customCurrencySymbol . ")" => 1,
                    "Left to pay </br>" . "(" . $customCurrencySymbol. ")" => 1,
                    "Paid Amount </br>" . "(" . $customCurrencySymbol . ")" => 1,
                ),
                'records' => $rec,
            );
        } else {
            $data['table'] = array(
                'col_size' => array("5", "20", "20", "10"),
                'headers' => array(
                    "Customer" => 2,
                    "Advance amount" => 1,
                ),
                'records' => $rec,
            );
        }

        $data['memo'] = $paymentDetails->incomingPaymentMemo;
        $data['sub_total'] = $paymentDetails->incomingPaymentAmount / $customCurrencyRate;
        $data['cr_payment'] = $paymentDetails->incomingPaymentCreditAmount / $customCurrencyRate;
        $data['cr_discount'] = $paymentDetails->incomingPaymentDiscount / $customCurrencyRate;
//        $data['total'] = $paymentDetails->incomingPaymentAmount - $paymentDetails->incomingPaymentDiscount;
        $data['total'] = ($paymentDetails->incomingPaymentPaidAmount / $customCurrencyRate);
        $data['totalWithCredit'] = ($paymentDetails->incomingPaymentPaidAmount / $customCurrencyRate) + ($paymentDetails->incomingPaymentCreditAmount / $customCurrencyRate);
        $data['balance'] = $paymentDetails->incomingPaymentBalance / $customCurrencyRate;
        $data['discount'] = null;

        $data['cdnUrl'] = $this->cdnUrl;
        if ($data['customerID']) {
            $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];
        } else {
            $data['customerName'] = '';
        }

        $data['customerAge'] = "";

        if (!empty($customerDetails->customerDateOfBirth)) {
            $birthday_timestamp = strtotime($customerDetails->customerDateOfBirth);  
            $age = date('md', $birthday_timestamp) > date('md') ? date('Y') - date('Y', $birthday_timestamp) - 1 : date('Y') - date('Y', $birthday_timestamp);
            $data['customerAge'] = $age;
        }

        return $this->_paymentViewData = $data;
    }

    private function _getDocumentDataTable($paymentID, $documentSize = 'A4')
    {

        $data_table_vars = $this->_getDataForPaymentPreview($paymentID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/customer-payments/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    private function _getDocumentChequeTable($paymentID, $documentSize = 'A4')
    {
        $data_table_vars = $this->_getDataForChequePreview($paymentID);
        $view = new ViewModel(
            array(
                'paymentMethodData' => $data_table_vars
            ));
        $view->setTemplate('/invoice/customer-payments/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    protected function _getDataForChequePreview($paymentID) {

        $data = array();
        
        $incomingPaymentChequeDetails = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getCustomerChequesByPaymentId($paymentID);

        foreach ($incomingPaymentChequeDetails as $value) {
            $data['cheque'][] = array('chequeNumber' => $value['incomingPaymentMethodChequeNumber'],
                            'chequeDate' => $value['postdatedChequeDate'],
                            'chequeAmount' => $value['incomingPaymentMethodAmount']);
        }

        $paymentMethod = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getPaymentMethodByPaymentId($paymentID);

        foreach ($paymentMethod as $value) {
            if($value['incomingPaymentMethodCashId'] != "0") {
                $data['cash'] = $value['incomingPaymentMethodAmount'];                
            } else if ($value['incomingPaymentMethodCreditCardId'] != "0") {
                $data['card'] = $value['incomingPaymentMethodAmount'];                
            } else if ($value['incomingPaymentMethodLCId'] != "0") {
                $data['loyalty'] = $value['incomingPaymentMethodAmount'];                
            }  
        }

        return $data;
    }

    public function viewReceiptAction()
    {
        
        $paymentID = $this->params()->fromRoute('param1');
        $isInitialView = ($this->params()->fromRoute('param2') == 'initial') ? true : false;
        
        $data = $this->_getDataForPaymentPreview($paymentID);
        $data['paymentID'] = $paymentID;
        $discount = number_format($data['cr_discount'], 2);
        $path = "/customerPayments/document/"; //.$paymentID;
        $translator = new Translator();
        $createNew = $translator->translate('New Payment');
        $createPath = "/customerPayments";

        if (!$data['customerName']) {
            $customerSalutaion = "";
        } else {
            $customerSalutaion = "Dear " . $data['customerName'] . ",";
        }


        $invoices = '';

        if (isset($data['invoices'])) {
            foreach ($data['invoices'] as $inv) {
                $invCCRate = ($inv["salesInvoiceCustomCurrencyRate"] != 0) ? $inv["salesInvoiceCustomCurrencyRate"] : 1;
                $invPaidAmount = number_format(abs($inv["incomingInvoicePaymentAmount"] / $invCCRate), 2);
                $invoices .= <<<HTML
Invoice No: {$inv['salesInvoiceCode']} <br />
Invoice Date: {$inv['salesInvoiceIssuedDate']} <br />
Amount Paid: {$data['currencySymbol']}{$invPaidAmount} <br /><br />
HTML;
            }
        }
        $invoices .= '</ol>';

        $data['totalWithCredit'] = number_format($data['totalWithCredit'], 2);
        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Receipt for your payment to " . $data['companyName'],
            "body" => <<<EMAILBODY
{$customerSalutaion}<br />

Thank you for the payment. <br /><br />

A payment receipt has been generated for you from {$data['companyName']} based on your payment and attached herewith. <br /><br />

<strong>Payment No.</strong>: {$data['incomingPaymentCode']} <br />
<strong>Date Paid</strong>: {$data['incomingPaymentDate']} <br />
<strong>Discount</strong>: {$data['currencySymbol']}{$discount} <br />
<strong>Total Paid Amount</strong>: {$data['currencySymbol']}{$data['totalWithCredit']} <br /><br />

<strong>Invoices:</strong> <br /><br />

{$invoices}


Looking forward to working with you again. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );
        
        if($this->hasSmsData("After Receipt") && $data['customerTelephoneNumber'] != ""){
            $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("After Receipt");
            $message = $smsTypeDetails->smsTypeMessage ."\n";

            $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);

            if($smsTypeDetails->isIncludeInvoiceDetails){
                while ($row = $smsIncludedDetailsByType->current()) {
                    if((bool) $row['isSelected']){
                        switch ($row['smsIncludedName']) {
                            case "Payment Date":
                                $message .=$row['smsIncludedName'] .' : '.$data['incomingPaymentDate'].' ,';
                                break;
                            case "Paid Amount":
                                $message .=$row['smsIncludedName'] .' : '.$data['currencySymbol'].$data['totalWithCredit']."\n";
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            
            $data["sms"] = array(
                "customerTelephoneNumber" => $data["customerTelephoneNumber"],
                "message" => $message
            );
        }else{
            $data["sms"] = null;
        }

        $journalEntryValues = [];
        if($this->useAccounting == 1){
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('7',$paymentID);
            $journalEntryID = $journalEntryData['journalEntryID'];
            $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryValues = $jeResult['JEDATA']; 
        }

        $JEntry = new ViewModel(
            array(
             'journalEntry' => $journalEntryValues,
             'closeHidden' => true 
             )
            );
        $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $documentType = 'Customer Payment';
        $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paymentID, $createPath, false, $isInitialView);
        $preview->addChild($JEntry, 'JEntry');
        
        $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
        $additionalButton->setTemplate('additionalButton');
        $preview->addChild($additionalButton, 'additionalButton');

        return $preview;
    }

    public function hasSmsData($smsType){
         $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

        if ($displaySettings->smsServiceStatus) {
                $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName($smsType);
                
                if($smsTypeDetails->isEnable && (int)$smsTypeDetails->smsSendType == 2){
                    return true;
                   
                }
        }
        return false;
    }

    public function getCustomerNameJson()
    {
        $cust_obj = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerNameList();
        while ($row1 = $cust_obj->current()) {
            $cust_list[] = $row1["cust_name"];
        }
        return json_encode($cust_list);
    }

    public function getInvoiceIdsJson()
    {
        $ids_obj = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceIds();
        while ($row = $ids_obj->current()) {
            $ids[] = $row[
                    'salesInvoiceID'];
        }
        return json_encode($ids);
    }

    public function getPaymentIdsJson()
    {
        $ids_obj = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentIdsList();
        while ($row = $ids_obj->current()) {
            $ids[] = $row['incomingPaymentID'];
        }
        return json_encode($ids);
    }

    public function viewChequesAction()
    {
        $page = $this->params('param1', 1);

        $this->getSideAndUpperMenus('Payments', 'View Cheques', 'SALES');

        $cheques = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')
                ->getCustomerCheques();

        $checkList = array();
        foreach ($cheques as $cheque) {
            $checkList[] = $cheque;
        }
        $items = new ArrayAdapter($checkList);
        $paginator = new Paginator($items);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/payments/viewCheques.js');

        return new ViewModel(array(
            'cheques' => $paginator,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
    }

    public function documentPdfAction()
    {
        $paymentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Customer Payment';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/invoice/customer-payments/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->_getDataForPaymentPreview($paymentID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($paymentID, $documentType, $documentData, $templateID);

        return;
    }

}

/////////////////// END OF Product CONTROLLER \\\\\\\\\\\\\\\\\\\\
