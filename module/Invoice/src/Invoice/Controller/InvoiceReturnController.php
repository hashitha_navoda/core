<?php

/**
 * @author ashan madushka <ashan@thinkcube.com>
 * This file contains Credit note Process related controller functions
 */

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Invoice\Form\InvoiceReturnForm;

class InvoiceReturnController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'credit_note_upper_menu';
    protected $userID;
    protected $user_session;
    protected $company;
    private $_creditNoteViewData;
    private $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Credit Note', 'Create Invoice Return', 'SALES');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(49, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $paymentterm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        while ($t = $paymentterm->current()) {
            $payterm[$t->paymentTermID] = $t->paymentTermName;
        }

        $today = date('Y-m-d');
        $promotions = $this->CommonTable('Settings\Model\PromotionTable')->getAllPromotion();
        $promotionData = array();
        foreach ($promotions as $pro) {
            $pro = (object) $pro;
            $promotionData[$pro->promotionID] = $pro;
        }

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();

        $invoiceReturnForm = new InvoiceReturnForm(
                array(
            'customCurrency' => $currency,
            'promotions' => $promotionData
                )
        );

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/invoice-return.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/calculations.js');
        $invoiceReturnForm->get('customer')->setAttribute('disabled', true);

        $checkIsSalesInvoiceApprovalActive = $this->CommonTable('Settings/Model/ApprovalDocumentDetailsTable')->checkDocIsActiveForApprovalWF(48);
        $approvalWorkflows = [];

        if ($checkIsSalesInvoiceApprovalActive['isActive']) {
            $getApprovalWorkflows = $this->CommonTable('Settings/Model/ApprovalWorkflowsTable')->getApprovalWorkflowsByDocID(48);

            foreach ($getApprovalWorkflows as $key => $value) {
                $approvalWorkflows[] = $value;
            }

        }

        $invoiceReturnAddView = new ViewModel(
                array(
            'paymentTerm' => $payterm,
            'invoiceReturnForm' => $invoiceReturnForm,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'referenceNumber' => $rid,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'isActiveApproval' => $checkIsSalesInvoiceApprovalActive['isActive'],
            'approvalWorkflows' => $approvalWorkflows
                )
        );
        $invoiceReturnProductsView = new ViewModel();
        $invoiceReturnProductsView->setTemplate('invoice/invoice-return/invoice-return-add-products');
        $invoiceReturnAddView->addChild($invoiceReturnProductsView, 'creditNoteAddProducts');
        $creditNotesubProductsView = new ViewModel();
        $creditNotesubProductsView->setTemplate('invoice/return/return-add-sub-products');
        $invoiceReturnAddView->addChild($creditNotesubProductsView, 'creditNoteAddSubProducts');
        $invoiceReturnAddView->addChild($dimensionAddView, 'dimensionAddView');
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Invoice Return Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Invoice Return Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $invoiceReturnAddView->addChild($refNotSet, 'refNotSet');
        }
        return $invoiceReturnAddView;
    }

    public function viewAction()
    {

        $this->getSideAndUpperMenus('Credit Note', 'View Invoice Return', 'SALES');
        $this->getPaginatedInvoiceReturns();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $creditNoteView = new ViewModel(array(
            'creditNote' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'locationID' => $locationID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
                )
        );
        // $docHistoryView = new ViewModel();
        // $docHistoryView->setTemplate('invoice/credit-note/doc-history');
        // $documentView = new ViewModel();
        // $documentView->setTemplate('invoice/credit-note/document-view');
        // // $creditNoteView->addChild($docHistoryView, 'docHistoryView');
        // // $creditNoteView->addChild($documentView, 'documentView');

        // // $attachmentsView = new ViewModel();
        // // $attachmentsView->setTemplate('core/modal/attachmentView');
        // // $creditNoteView->addChild($attachmentsView, 'attachmentsView');
        
        return $creditNoteView;
    }

    public function viewDraftAction()
    {

        $this->getSideAndUpperMenus('Credit Note', 'View Draft Invoice Return', 'SALES');
        $this->getPaginatedDraftInvoiceReturns();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $creditNoteView = new ViewModel(array(
            'creditNote' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'locationID' => $locationID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
                )
        );
       
        
        return $creditNoteView;
    }

    private function getPaginatedInvoiceReturns()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }


    private function getPaginatedDraftInvoiceReturns()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Invoice\Model\DraftInvoiceReturnTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function viewInvoiceReturnReceiptAction()
    {
        return $this->documentPreviewAction();
    }

    public function viewDraftInvoiceReturnReceiptAction()
    {
        return $this->draftDocumentPreviewAction();
    }

    public function documentPreviewAction()
    {
        $invoiceReturnID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($invoiceReturnID);
        $data['invoiceReturnID'] = $data['invoiceReturnID'];
        $data['total'] = number_format($data['total'], 2);
        $path = "/invoice-return/document/"; //.$returnID;
        $createNew = "New Invoice Return";
        $createPath = "/invoice-return/create";

        if (!$data['customerName']) {
            $customerSalutaion = "";
        } else {
            $customerSalutaion = "Dear " . $data['customerName'] . ",";
        }


        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Invoice Return from " . $data['companyName'],
            "body" => <<<EMAILBODY

{$customerSalutaion}<br />

Thank you for your inquiry. <br /><br />

A Invoice Return has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>Invoice Return Number:</strong> {$data['invoiceReturnCode']} <br />
<strong>Invoice Return Amount:</strong> {$data['currencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $documentType = 'Sales Invoice Return';
        $preview =  $this->getCommonPreview($data, $path, $createNew, $documentType, $invoiceReturnID, $createPath);

        return $preview;
    }


    public function draftDocumentPreviewAction()
    {
        $invoiceReturnID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($invoiceReturnID);
        $data['invoiceReturnID'] = $data['invoiceReturnID'];
        $data['total'] = number_format($data['total'], 2);
        $path = "/invoice-return/draftDocument/"; //.$returnID;
        $createNew = "Draft Invoice Return";
        $createPath = "/invoice-return/create";

        if (!$data['customerName']) {
            $customerSalutaion = "";
        } else {
            $customerSalutaion = "Dear " . $data['customerName'] . ",";
        }

        $documentType = 'Sales Invoice Return';
        $preview =  $this->getCommonPreview($data, $path, $createNew, $documentType, $invoiceReturnID, $createPath);

        return $preview;
    }

    /**
     * this functoion will echo the print view of a grn (ID should be passed as a param1)
     * @return viewModel Grn View ViewModel
     */
    public function draftDocumentAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = 'Sales Invoice Return';
        $invReturnID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName('Draft Invoice Return', $invReturnID, $templateID);

        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->getDataForDraftDocument($invReturnID);
        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);
        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->getDocumentDataTable($grnID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    public function getDataForDocument($invoiceReturnID)
    {

        if (!empty($this->_creditNoteViewData)) {
            return $this->_creditNoteViewData;
        }

        $data = $this->getDataForDocumentView();

        $invoiceReturnDetails = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->getInvoiceReturnByInvoiceReturnID($invoiceReturnID)->current();

        $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceReturnDetails['invoiceID']);

        $salesInvoiceSuspendedTaxFlag = false;
        if ($invoiceDetails->salesInvoiceSuspendedTax == "1") {
            $salesInvoiceSuspendedTaxFlag = true;
        }

        $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($invoiceReturnDetails['customerID']);

        $customCurrencyId = $invoiceReturnDetails['customCurrencyId'];
        $invoiceReturnDetails['createdTimeStamp'] = $this->getUserDateTime($invoiceReturnDetails['createdTimeStamp']);

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId == "" || $customCurrencyId == 0) ? 1 : $invoiceReturnDetails['invoiceReturnCustomCurrencyRate'];
        $customCurrencySymbol = ($customCurrencyId == "" || $customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->currencySymbol;

        $data = array_merge($data, $invoiceReturnDetails, (array) $customerDetails);


        $addressArray = array(
            trim($invoiceReturnDetails['customerProfileLocationNo'], ','),
            trim($invoiceReturnDetails['customerProfileLocationRoadName1'], ','),
            trim($invoiceReturnDetails['customerProfileLocationRoadName2'], ','),
            trim($invoiceReturnDetails['customerProfileLocationRoadName3'], ','),
            trim($invoiceReturnDetails['customerProfileLocationSubTown'], ','),
            trim($invoiceReturnDetails['customerProfileLocationTown'], ','),
            trim($invoiceReturnDetails['customerProfileLocationPostalCode'], ','),
            trim($invoiceReturnDetails['customerProfileLocationCountry'], ','),
        );

        $address = implode(', ', array_filter($addressArray));
        $data['customerAddress'] = ($address != '') ? $address . '.' : $address;


        $data['currencySymbol'] = $customCurrencySymbol;

        // to be used by below functions - convert to object
        $invoiceReturnDetails = (object) $invoiceReturnDetails;

        $invoiceReturnProductDetails = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->getAllInvoiceReturnProductDetailsByInvoiceReturnID($invoiceReturnID);

        $txTypes = array();
        $sub_total = 0;
        $suspendedTaxAmount = 0;
        foreach ($invoiceReturnProductDetails as $product) {

            $product = (object) $product;

            if ($salesInvoiceSuspendedTaxFlag) {
                $invoiceProductTax = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductTaxDetailsByInvoiceProductId($product->invoiceProductID);        
                foreach ($invoiceProductTax as $value) {
                    if ($value['taxSuspendable'] == "1") {
                        $susTaxAmount = (floatval($value['salesInvoiceProductTaxAmount']) / floatval($value['salesInvoiceProductQuantity'])) * floatval($product->invoiceReturnProductQuantity);
                        $suspendedTaxAmount += $susTaxAmount;
                    }
                }
            }

            $total_itm_tx = 0;
            $productsTax = $this->CommonTable('Invoice\Model\InvoiceReturnProductTaxTable')->getInvoiceReturnProductTaxByInvoiceReturnProductID($product->invoiceReturnProductID);
            foreach ($productsTax as $productTax) {
                if (!isset($txTypes[$productTax['taxName']])) {
                    $txTypes[$productTax['taxName']] = $productTax['invoiceReturnProductTaxAmount'] / $customCurrencyRate;
                } else {
                    $txTypes[$productTax['taxName']] += $productTax['invoiceReturnProductTaxAmount'] / $customCurrencyRate;
                }
                $total_itm_tx += $productTax['invoiceReturnProductTaxAmount'] / $customCurrencyRate;
            }

            $itemDiscount = ($product->invoiceReturnProductDiscountType == 'precentage') ? ($product->invoiceReturnProductPrice * $product->invoiceReturnProductDiscount / 100) / $customCurrencyRate : $product->invoiceReturnProductDiscount / $customCurrencyRate;
            $item_tax_string = ($total_itm_tx > 0) ? "Tax " . number_format($total_itm_tx, 2) : "";
            $item_disc_string = "";

            if ($itemDiscount > 0) {
                if ($product->invoiceReturnProductDiscountType == 'precentage') {
                    $item_disc_string = "Disc: " . number_format($product->invoiceReturnProductDiscount, 2) . "%";
                } else {
                    $item_disc_string = "Disc: " . $data['currencySymbol'] . number_format($itemDiscount, 2) . "";
                }
            }
            $sub_line = "";
            if ($total_itm_tx > 0) {
                if ($item_tax_string != "" && $item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                } else if ($item_tax_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ")</small>";
                } else if ($item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                }
                $unit = $product->invoiceReturnProductPrice / $customCurrencyRate;
            } else {
                $unit = ($product->invoiceReturnProductPrice / $customCurrencyRate) + ($total_itm_tx / $product->invoiceReturnProductQuantity);
                $sub_line = ($product->invoiceReturnProductDiscount > 0) ? "<br><small>(" . $item_disc_string . ")</small>" : "";
            }

            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID);

            $unitPrice = $this->getProductUnitPriceViaDisplayUom($product->invoiceReturnProductPrice, $productUom) / $customCurrencyRate;
            $productQtyDetails = $this->getProductQuantityViaDisplayUom($product->invoiceReturnProductQuantity, $productUom);
            // Add dash for non inventory product
            $productQtyDetails['quantity'] = ($productQtyDetails['quantity'] == 0) ? '-' : $productQtyDetails['quantity'];

            $records[] = array(
                $product->productCode,
                $product->productName . $sub_line,
                $productQtyDetails['quantity'] . ' ' . $productQtyDetails['uomAbbr'],
                number_format($unitPrice, 2),
                number_format(($product->invoiceReturnProductTotal / $customCurrencyRate), 2),
            );
            $sub_total+= $product->invoiceReturnProductTotal / $customCurrencyRate;
        }
        // var_dump($sub_total).die();

        foreach ($txTypes as $t_name => $t_val) {
            $tx_line.=($tx_line == null) ? $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2) :
                    ",&nbsp;" . $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2);
        }

        if ($suspendedTaxAmount > 0) {
            $tx_line.=($tx_line == null) ? "Suspended Tax(VAT):&nbsp; " . $customCurrencySymbol . " " . number_format($suspendedTaxAmount, 2) :
                    ",&nbsp;Suspended Tax(VAT):&nbsp; " . $customCurrencySymbol . " " . number_format($suspendedTaxAmount, 2);            
        }


         //get all tax details
        $taxDetails = $this->CommonTable('Settings\Model\TaxCompoundTable')->fetchAll();
        $compoundTaxes = [];

        foreach ($taxDetails as $key => $taxValue) {
            $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;

        }
        $txTypes = array();
        $totalTax = 0;
        $calculatableTaxesTotal = 0;
        $suspendedTaxesTotal = 0;
        $normalTaxTotal = 0;
        $compoundTaxTotal = 0;
        $calculatableTaxes = array();
        $suspendableTaxes = array();
        $normalTaxesSet = array();
        $compoundTaxesSet = array();
        $creditNoteTax = $this->CommonTable('Invoice\Model\InvoiceReturnProductTaxTable')->getInvoiceReturnTax($invoiceReturnID);

        foreach ($creditNoteTax as $productTax) {

            $thisTax = $productTax['invoiceReturnProductTaxAmount'];
            if(array_key_exists($productTax['taxID'], $compoundTaxes)){
                    $compoundTaxTotal += floatval($thisTax);
                    $compoundTaxesSet[] = $productTax['taxName'];
            } else {
                    $normalTaxTotal += floatval($thisTax);
                    $normalTaxesSet[] = $productTax['taxName'];
            }


            // $txTypes[$productTax['taxName']] = (!isset($txTypes[$productTax['taxName']])) ? $thisTax : $txTypes[$productTax['taxName']] + $thisTax;
            // $calculatableTaxesTotal += ($productTax['taxSuspendable'] != 1) ? $thisTax : 0;
            // $suspendedTaxesTotal += ($productTax['taxSuspendable'] == 1) ? $thisTax : 0;
            // $totalTax += $thisTax;

            // if ($productTax['taxSuspendable'] == 1) {
            //     if ($invoiceDetails['salesInvoiceSuspendedTax'] == 1) {
            //         $suspendableTaxes[] = $productTax['taxName'];                        // }
            //     }else{
            //         $calculatableTaxes[] = $productTax['taxName'];
            //     }
            // } else {
            //     $calculatableTaxes[] = $productTax['taxName'];
            // }
        }
        $data['normalTaxTotal'] = $normalTaxTotal;
        $data['compoundTaxTotal'] = $compoundTaxTotal;
        $data['type'] = _("Invoice Return");
        $data['cust_name'] = '';
        $data['state'] = $invoiceReturnDetails->statusID;
        $data['doc_data'] = array(
            _("Date") => $invoiceReturnDetails->invoiceReturnDate,
            _("Valid till") => '',
            _("Payment terms") => '',
            _("Delivery Note No.") => '',
            _("Invoice Return Code") => $invoiceReturnDetails->invoiceReturnCode,
        );
        $data['cust_data'] = array(
            _("address") => '',
        );
        $data['table'] = array(
            'col_size' => array(10, 90, 290, 50, 100, 100),
            'col_allign' => array("left", "left", "left", "right", "right"),
            'headers' => array(
                _("product code") => 2,
                _("product") => 1,
                _("return qty") => 1,
                _("price") => 1,
                _("total") => 1
            ),
            'records' => $records,
        );

        if ($invoiceReturnID == 'template') {
            $data['template'] = true;
            $data['Invoice Return Code'] = '[invoiceReturnCode]';
            $data['Invoice Return Date'] = '[invoiceReturnDate]';
            $data['current_time'] = '[current_time]';
            $data['userName'] ='[userName]';
            
        }

        $data['comment'] = $invoiceReturnDetails->invoiceReturnComment;
        $data['Invoice Return Code'] = $invoiceReturnDetails->invoiceReturnCode;
        $data['sub_total'] = $sub_total;
        $data['total'] = $invoiceReturnDetails->invoiceReturnTotal / $customCurrencyRate;
        $data['discount'] = '';
        $data['discountStatement'] = '';
        $data['show_tax'] = true;
        $data['tax_record'] = $tx_line;
        if ($data['customerID']) {
            $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];
        } else {
            $data['customerName'] = '';
        }
        return $this->_creditNoteViewData = $data;
    }

     public function getDataForDraftDocument($invoiceReturnID)
    {

        if (!empty($this->_creditNoteViewData)) {
            return $this->_creditNoteViewData;
        }

        $data = $this->getDataForDocumentView();

        $invoiceReturnDetails = $this->CommonTable('Invoice\Model\DraftInvoiceReturnTable')->getInvoiceReturnByInvoiceReturnID($invoiceReturnID)->current();

        $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceReturnDetails['invoiceID']);

        $salesInvoiceSuspendedTaxFlag = false;
        if ($invoiceDetails->salesInvoiceSuspendedTax == "1") {
            $salesInvoiceSuspendedTaxFlag = true;
        }

        $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($invoiceReturnDetails['customerID']);

        $customCurrencyId = $invoiceReturnDetails['customCurrencyId'];
        $invoiceReturnDetails['createdTimeStamp'] = $this->getUserDateTime($invoiceReturnDetails['createdTimeStamp']);

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId == "" || $customCurrencyId == 0) ? 1 : $invoiceReturnDetails['invoiceReturnCustomCurrencyRate'];
        $customCurrencySymbol = ($customCurrencyId == "" || $customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->currencySymbol;

        $data = array_merge($data, $invoiceReturnDetails, (array) $customerDetails);


        $addressArray = array(
            trim($invoiceReturnDetails['customerProfileLocationNo'], ','),
            trim($invoiceReturnDetails['customerProfileLocationRoadName1'], ','),
            trim($invoiceReturnDetails['customerProfileLocationRoadName2'], ','),
            trim($invoiceReturnDetails['customerProfileLocationRoadName3'], ','),
            trim($invoiceReturnDetails['customerProfileLocationSubTown'], ','),
            trim($invoiceReturnDetails['customerProfileLocationTown'], ','),
            trim($invoiceReturnDetails['customerProfileLocationPostalCode'], ','),
            trim($invoiceReturnDetails['customerProfileLocationCountry'], ','),
        );

        $address = implode(', ', array_filter($addressArray));
        $data['customerAddress'] = ($address != '') ? $address . '.' : $address;


        $data['currencySymbol'] = $customCurrencySymbol;

        // to be used by below functions - convert to object
        $invoiceReturnDetails = (object) $invoiceReturnDetails;

        $invoiceReturnProductDetails = $this->CommonTable('Invoice\Model\DraftInvoiceReturnProductTable')->getAllInvoiceReturnProductDetailsByInvoiceReturnID($invoiceReturnID);

        $txTypes = array();
        $sub_total = 0;
        $suspendedTaxAmount = 0;
        foreach ($invoiceReturnProductDetails as $product) {

            $product = (object) $product;

            if ($salesInvoiceSuspendedTaxFlag) {
                $invoiceProductTax = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductTaxDetailsByInvoiceProductId($product->invoiceProductID);        
                foreach ($invoiceProductTax as $value) {
                    if ($value['taxSuspendable'] == "1") {
                        $susTaxAmount = (floatval($value['salesInvoiceProductTaxAmount']) / floatval($value['salesInvoiceProductQuantity'])) * floatval($product->invoiceReturnProductQuantity);
                        $suspendedTaxAmount += $susTaxAmount;
                    }
                }
            }

            $total_itm_tx = 0;
            $productsTax = $this->CommonTable('Invoice\Model\DraftInvoiceReturnProductTaxTable')->getInvoiceReturnProductTaxByInvoiceReturnProductID($product->draftInvoiceReturnProductID);
            foreach ($productsTax as $productTax) {
                if (!isset($txTypes[$productTax['taxName']])) {
                    $txTypes[$productTax['taxName']] = $productTax['invoiceReturnProductTaxAmount'] / $customCurrencyRate;
                } else {
                    $txTypes[$productTax['taxName']] += $productTax['invoiceReturnProductTaxAmount'] / $customCurrencyRate;
                }
                $total_itm_tx += $productTax['invoiceReturnProductTaxAmount'] / $customCurrencyRate;
            }

            $itemDiscount = ($product->invoiceReturnProductDiscountType == 'precentage') ? ($product->invoiceReturnProductPrice * $product->invoiceReturnProductDiscount / 100) / $customCurrencyRate : $product->invoiceReturnProductDiscount / $customCurrencyRate;
            $item_tax_string = ($total_itm_tx > 0) ? "Tax " . number_format($total_itm_tx, 2) : "";
            $item_disc_string = "";

            if ($itemDiscount > 0) {
                if ($product->invoiceReturnProductDiscountType == 'precentage') {
                    $item_disc_string = "Disc: " . number_format($product->invoiceReturnProductDiscount, 2) . "%";
                } else {
                    $item_disc_string = "Disc: " . $data['currencySymbol'] . number_format($itemDiscount, 2) . "";
                }
            }
            $sub_line = "";
            if ($total_itm_tx > 0) {
                if ($item_tax_string != "" && $item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                } else if ($item_tax_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ")</small>";
                } else if ($item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                }
                $unit = $product->invoiceReturnProductPrice / $customCurrencyRate;
            } else {
                $unit = ($product->invoiceReturnProductPrice / $customCurrencyRate) + ($total_itm_tx / $product->invoiceReturnProductQuantity);
                $sub_line = ($product->invoiceReturnProductDiscount > 0) ? "<br><small>(" . $item_disc_string . ")</small>" : "";
            }

            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID);

            $unitPrice = $this->getProductUnitPriceViaDisplayUom($product->invoiceReturnProductPrice, $productUom) / $customCurrencyRate;
            $productQtyDetails = $this->getProductQuantityViaDisplayUom($product->invoiceReturnProductQuantity, $productUom);
            // Add dash for non inventory product
            $productQtyDetails['quantity'] = ($productQtyDetails['quantity'] == 0) ? '-' : $productQtyDetails['quantity'];

            $records[] = array(
                $product->productCode,
                $product->productName . $sub_line,
                $productQtyDetails['quantity'] . ' ' . $productQtyDetails['uomAbbr'],
                number_format($unitPrice, 2),
                number_format(($product->invoiceReturnProductTotal / $customCurrencyRate), 2),
            );
            $sub_total+= $product->invoiceReturnProductTotal / $customCurrencyRate;
        }

        foreach ($txTypes as $t_name => $t_val) {
            $tx_line.=($tx_line == null) ? $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2) :
                    ",&nbsp;" . $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2);
        }

        if ($suspendedTaxAmount > 0) {
            $tx_line.=($tx_line == null) ? "Suspended Tax(VAT):&nbsp; " . $customCurrencySymbol . " " . number_format($suspendedTaxAmount, 2) :
                    ",&nbsp;Suspended Tax(VAT):&nbsp; " . $customCurrencySymbol . " " . number_format($suspendedTaxAmount, 2);            
        }


         //get all tax details
        $taxDetails = $this->CommonTable('Settings\Model\TaxCompoundTable')->fetchAll();
        $compoundTaxes = [];

        foreach ($taxDetails as $key => $taxValue) {
            $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;

        }
        $txTypes = array();
        $totalTax = 0;
        $calculatableTaxesTotal = 0;
        $suspendedTaxesTotal = 0;
        $normalTaxTotal = 0;
        $compoundTaxTotal = 0;
        $calculatableTaxes = array();
        $suspendableTaxes = array();
        $normalTaxesSet = array();
        $compoundTaxesSet = array();
        $creditNoteTax = $this->CommonTable('Invoice\Model\DraftInvoiceReturnProductTaxTable')->getInvoiceReturnTax($invoiceReturnID);

        foreach ($creditNoteTax as $productTax) {

            $thisTax = $productTax['invoiceReturnProductTaxAmount'];
            if(array_key_exists($productTax['taxID'], $compoundTaxes)){
                    $compoundTaxTotal += floatval($thisTax);
                    $compoundTaxesSet[] = $productTax['taxName'];
            } else {
                    $normalTaxTotal += floatval($thisTax);
                    $normalTaxesSet[] = $productTax['taxName'];
            }
        }
        $data['normalTaxTotal'] = $normalTaxTotal;
        $data['compoundTaxTotal'] = $compoundTaxTotal;
        $data['type'] = _("Invoice Return");
        $data['cust_name'] = '';
        $data['state'] = $invoiceReturnDetails->statusID;
        $data['doc_data'] = array(
            _("Date") => $invoiceReturnDetails->invoiceReturnDate,
            _("Valid till") => '',
            _("Payment terms") => '',
            _("Delivery Note No.") => '',
            _("Invoice Return Code") => $invoiceReturnDetails->invoiceReturnCode,
        );
        $data['cust_data'] = array(
            _("address") => '',
        );
        $data['table'] = array(
            'col_size' => array(10, 90, 290, 50, 100, 100),
            'col_allign' => array("left", "left", "left", "right", "right"),
            'headers' => array(
                _("product code") => 2,
                _("product") => 1,
                _("return qty") => 1,
                _("price") => 1,
                _("total") => 1
            ),
            'records' => $records,
        );

        if ($invoiceReturnID == 'template') {
            $data['template'] = true;
            $data['Invoice Return Code'] = '[invoiceReturnCode]';
            $data['Invoice Return Date'] = '[invoiceReturnDate]';
            $data['current_time'] = '[current_time]';
            $data['userName'] ='[userName]';
            
        }

        $data['comment'] = $invoiceReturnDetails->invoiceReturnComment;
        $data['Invoice Return Code'] = $invoiceReturnDetails->invoiceReturnCode;
        $data['sub_total'] = $sub_total;
        $data['total'] = $invoiceReturnDetails->invoiceReturnTotal / $customCurrencyRate;
        $data['discount'] = '';
        $data['discountStatement'] = '';
        $data['show_tax'] = true;
        $data['tax_record'] = $tx_line;
        if ($data['customerID']) {
            $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];
        } else {
            $data['customerName'] = '';
        }
        return $this->_creditNoteViewData = $data;
    }


    public function documentAction()
    {
        $documentType = 'Sales Invoice Return';
        $creditNoteID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        echo $this->generateDocument($creditNoteID, $documentType, $templateID);
        exit;
    }

    public function getDocumentDataTable($invoiceReturnID, $documentSize = 'A4')
    {

        $data_table_vars = $this->getDataForDocument($invoiceReturnID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/invoice-return/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('ViewRenderer')
                        ->render($view);
    }

    public function documentPdfAction()
    {
        $invoiceReturnID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Sales Invoice Return';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/invoice-return/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($invoiceReturnID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($invoiceReturnID, $documentType, $documentData, $templateID);

        return;
    }

}

/////////////////// END OF RETURN CONTROLLER \\\\\\\\\\\\\\\\\\\\







