<?php

namespace Invoice\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class MrpSettingsController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'settings_products_upper_menu';

    public function indexAction()
    {

        $this->getSideAndUpperMenus('Product Setup', 'MRP Settings');
        $this->useAccounting = $this->user_session->useAccounting;
        $displaySetup = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();

        $allLocations = $this->allLocations;

        foreach ($allLocations as $key => $value) {
            $locIDs[] = $key;
            $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
        }

        $catgry = [];
        $catgrys = $this->CommonTable('Inventory/Model/CategoryTable')->actionFetchAll();
        foreach ($catgrys as $c) {
            $catgry[$c['categoryID']] = $c['categoryName'];
        }
        $locationID = $this->user_session->userActiveLocation['locationID'];

        $view = new ViewModel(array(
            'useMrpSettings' => $displaySetup['useMrpSettings'],
            'locNames' => $locNames,
            'catgryList' => $catgry,
            'currentLoc' => $locationID
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/mrp-settings.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return $view;
    }


    public function listAction()
    {

        $this->getSideAndUpperMenus('Product Setup', 'View MRP Details');
        $this->useAccounting = $this->user_session->useAccounting;
        $displaySetup = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();
        $catgry = [];
        $catgrys = $this->CommonTable('Inventory/Model/CategoryTable')->actionFetchAll();
        foreach ($catgrys as $c) {
            $catgry[$c['categoryID']] = $c['categoryName'];
        }
        
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $itemList = $this->getPaginatedProducts($locationID);
        $view = new ViewModel(array(
            'itemList' => $this->paginator,
            'paginated' => true,
            'catgryList' => $catgry,
            'currentLoc' => $locationID
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/mrp-item-list.js');

        return $view;
    }

    public function getPaginatedProducts($locationID, $fixedAssetFlag = false)
    {
        $this->paginator = $this->CommonTable('Inventory/Model/ProductTable')->fetchAll(true, $locationID, $fixedAssetFlag);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

}
