<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains Customer related controller functions
 */

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Form\AddCustomerForm;
use Invoice\Form\DataimportForm;
use Zend\Session\Container;
use Invoice\Form\CustomerProfileForm;
use Zend\I18n\Translator\Translator;

class CustomerController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'customer_upper_menu';
    protected $paginator;
    protected $userID;
    protected $username;
    protected $user_session;
    protected $sidemenu;
    protected $uppermenu;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;

    }

//this function return paginated customer list
    public function indexAction()
    {
        $this->setLogMessage('Customer list accessed');
        $this->getSideAndUpperMenus('Customers', 'View Customers', 'CRM');

        return $this->customerView();
    }

//this function for add new customer
    public function addAction()
    {
        $this->setLogMessage("Customer Add Page accessed");

        $this->getSideAndUpperMenus('Customers', 'Add Customer', 'CRM');

        return $this->customerAdd();
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Customers', 'Add Customer', 'CRM');
        return $this->customerEdit();
    }

//this function returns payment terms array
    private function _getPaymentTerms()
    {
//get payment terms list
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        foreach ($paymentTerms as $row) {
            $id = (int) $row->paymentTermID;
            $terms[$id] = $row->paymentTermName;
        }
        return $terms;
    }

//this function returns currency array
    private function _getCurrencys()
    {
//get currencys list
        $allCurrency = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currency = array();
        $currency['0'] = '';
        foreach ($allCurrency as $row) {


            $currency[$row->currencyID] = $row->currencyName;
        }

        return $currency;
    }

//this function return paginated customer list
    private function getPaginatedCustomers()
    {
        $this->paginator = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll(true, TRUE, TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

//this funtion for customer data import
    public function importAction()
    {
        $this->setLogMessage("Customer import page accessed");
        $this->getSideAndUpperMenus('Customers', 'Customer Import', 'CRM');

        $form = new DataimportForm();
        $form->get('submit')->setValue(_('Next'));

        $request = $this->getRequest();

        if ($request->isPost()) {

            if ($_FILES["fileupload"]["tmp_name"] == null) {
                return array('form' => $form, 'fileerror' => 'nofile');
            } else if (substr($_FILES["fileupload"]["name"], -3) != 'csv') {
                return array('form' => $form, 'fileerror' => 'utferror');
            }
            if ($request->getPost('delimiter') == 0) {
                $delim = ',';
            } elseif ($request->getPost('delimiter') == 1) {
                $delim = ';';
            } elseif ($request->getPost('delimiter') == 2) {
                $delim = '.';
            } else {
                $delim = ',';
            }

            if ($request->getPost('header') == 1) {
                $headerchecked = TRUE;
            } else {
                $headerchecked = FALSE;
            }
            $content = fopen($_FILES["fileupload"]["tmp_name"], "r");
            $encodingtype = $request->getPost('characterencoding');
            $fileenc = file($_FILES["fileupload"]["tmp_name"]);
            $string = $fileenc[0];
            if ($encodingtype == 0) {
                $enres = mb_check_encoding($string, 'UTF-8');
                if ($enres == FALSE) {
                    return array('form' => $form, 'fileerror' => 'utferror');
                }
            } elseif ($encodingtype == 1) {
                $enres = mb_check_encoding($string, 'ISO-8859-1');
                if ($enres == FALSE) {
                    return array('form' => $form, 'fileerror' => 'isoerror');
                }
            }
            move_uploaded_file($_FILES["fileupload"]["tmp_name"], '/tmp/ezBiz.customerImportData.csv');
            chmod('/tmp/ezBiz.customerImportData.csv', 0777);

            $columncount = 0;

            if ($headerchecked == TRUE) {
                $headerlist = fgetcsv($content, 1000, $delim);
                $firstdataline = fgetcsv($content, 1000, $delim);
                for ($headers = 0; $headers < sizeof($headerlist); $headers++) {
                    $da[$headers]['header'] = $headerlist[$headers];
                    $da[$headers]['row'] = $firstdataline[$headers];
                }
                $columncount = sizeof($headerlist);
            } elseif ($headerchecked == FALSE) {
                $firstdataline = fgetcsv($content, 1000, $delim);
                for ($columns = 0; $columns < sizeof($firstdataline); $columns++) {
                    $da[$columns]['header'] = "Column" . ($columns + 1);
                    $da[$columns]['row'] = $firstdataline[$columns];
                }
                $columncount = sizeof($firstdataline);
            }
            $fileData = $this->getFileData($request->getPost()->toArray(),$request->getFiles()->toArray());
            $view = new ViewModel(array(
                'data' => $da,
                'form' => $form,
                'header' => TRUE,
                'delim' => $delim,
                'columns' => $columncount,
                'fData' => $fileData['data']
            ));
            $view->setTemplate("invoice/customer/importmapping");
            return $view;
        }

        return array('form' => $form);
    }

//this function  for customer massedit
    public function masseditAction()
    {
        $this->setLogMessage("Customer Mass Edit page accessed");
        $this->getSideAndUpperMenus('Customers', 'Customer Mass Edit', 'CRM');

//get customer list
        $customers = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAllWithFullDetails();
        $index = 0;

//get payment term list
        $pt = $this->_getPaymentTerms();

//get currency list
        $cu = $this->_getCurrencys();

//        get customer Categories
        $customerCategories[''] = '';
        $customerCategories += $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();

// get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];

        $config = $this->getServiceLocator()->get('config');
        $countries = ['' => ''] + $config['all_countries'];

//        set customer Address into an array
        $customersAddress = array();
//set each customer data to two dimensional array
        $custa = array();
        foreach ($customers as $cust) {
            $cust = (object) $cust;
//            $paymentfull = $this->CommonTable('Core\Model\PaymentTermTable')->getPaymentTerm($cust->customerPaymentTerm);
//            $Currencyfull = $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($cust->customerCurrency);
            $custa[$index]['customerID'] = $cust->customerID;
            $custa[$index]['customerName'] = $cust->customerName;
            $custa[$index]['customerCode'] = $cust->customerCode;
            $custa[$index]['customerCurrentTitle'] = $cust->customerTitle;
            $custa[$index]['customerShortName'] = $cust->customerShortName;
            $custa[$index]['currency'] = isset($cust->currencyName) ? $cust->currencyName : NULL;
            $custa[$index]['telephoneNumber'] = $cust->customerTelephoneNumber;
            $custa[$index]['email'] = $cust->customerProfileEmail;
            $custa[$index]['customerCategory'] = $cust->customerCategory;
            $custa[$index]['customerDateOfBirth'] = $cust->customerDateOfBirth;
            $custa[$index]['customerIdentityNumber'] = $cust->customerIdentityNumber;
            $custa[$index]['vatNumber'] = $cust->customerVatNumber;
            $custa[$index]['svatNumber'] = $cust->customerSVatNumber;
            $custa[$index]['paymentTerm'] = isset($cust->paymentTermName) ? $cust->paymentTermName : '';
            $custa[$index]['creditLimit'] = $cust->customerCreditLimit;
            $custa[$index]['currentBalance'] = $cust->customerCurrentBalance;
            $custa[$index]['currentCredit'] = $cust->customerCurrentCredit;
            $custa[$index]['discount'] = $cust->customerDiscount;
//            set customer Address
            $customersAddress[$cust->customerID]['cMEA_locationNo'] = $cust->customerProfileLocationNo;
            $customersAddress[$cust->customerID]['cMEA_roadName1'] = $cust->customerProfileLocationRoadName1;
            $customersAddress[$cust->customerID]['cMEA_roadName2'] = $cust->customerProfileLocationRoadName2;
            $customersAddress[$cust->customerID]['cMEA_roadName3'] = $cust->customerProfileLocationRoadName3;
            $customersAddress[$cust->customerID]['cMEA_subTown'] = $cust->customerProfileLocationSubTown;
            $customersAddress[$cust->customerID]['cMEA_town'] = $cust->customerProfileLocationTown;
            $customersAddress[$cust->customerID]['cMEA_postalCode'] = $cust->customerProfileLocationPostalCode;
            $customersAddress[$cust->customerID]['cMEA_country'] = $cust->customerProfileLocationCountry;
            $index = $index + 1;
        }

        $view = new ViewModel(array(
            'pt' => $pt,
            'cu' => $cu,
            'customerTitle' => $customerTitle,
            'cust' => $custa,
            'customerCategories' => $customerCategories,
            'countries' => $countries,
            'customersAddress' => json_encode($customersAddress),
        ));
        return $view;
    }

    /**
     * This will return customer list view for sales tab customers index
     * @return type
     */
    public function salesCustomerIndexAction()
    {
        $this->sideMenus = 'invoice_side_menu';
        $this->upperMenus = 'sales_customer_upper_menu';

        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }

        $this->setLogMessage('Customer list accessed');
        $this->getSideAndUpperMenus('Customers', 'View Customers', 'SALES');

        return $this->customerView(true);
    }

    /**
     * This will Return Customer List view with out side and upper menu.
     * @return ViewModel
     */
    private function customerView($isSales = false)
    {
        $this->getPaginatedCustomers();
        if ($isSales) {
            $view = new ViewModel([
                'customers' => $this->paginator,
                'editAction' => 'sales-customer-edit',
                'indexAction' => 'sales-customer-index',
                    ]
            );
        } else {
            $view = new ViewModel([
                'customers' => $this->paginator,
                'editAction' => 'edit',
                'indexAction' => 'index',
                    ]
            );
        }

        $view->setTemplate('/invoice/customer/index');

//get customer delete confirm box
        $customerDeleteBox = new ViewModel();
        $customerDeleteBox->setTemplate('/invoice/customer-api/get-cusdelete-confirm-box');
        $view->addChild($customerDeleteBox, 'delete_customer');

//set java script files for customer list
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/customer.js');


//if request is post request and  post variable search is true  then send index view to customer list
        $request = $this->getRequest();
        if ($request->isPost() && $request->getPost('search') == TRUE) {
            $view->setTerminal(TRUE);
            return $view;
        } else {
//set customer search header for customer list
            $searchHeader = new ViewModel();
            $searchHeader->setTemplate('invoice/customer-api/search-header');
            $view->addChild($searchHeader, 'searchHeader');
            return $view;
        }
    }

    public function salesCustomerAddAction()
    {
        $this->sideMenus = 'invoice_side_menu';
        $this->upperMenus = 'sales_customer_upper_menu';

        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }

        $this->setLogMessage("Customer Add Page accessed");
        $this->getSideAndUpperMenus('Customers', 'Add Customer', 'SALES');
        return $this->customerAdd();
    }

    /**
     * This will return customer add view model without side and upper menu items
     * @return ViewModel
     */
    private function customerAdd()
    {
//get payment terms list
        $terms = $this->_getPaymentTerms();

//get currencys list
        $currency = $this->_getCurrencys();
//get customer list
//       get customer code
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(20, $locationID);
        $rid = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        $customerCode = $this->getReferenceNumber($locationReferenceID);

        $data = array();
        $result = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll(false, true);
        foreach ($result as $row) {
            $data[$row["customerID"]] = $row["customerName"] . '-' . $row["customerCode"];
        }

// get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];
        $countries = $config['all_countries'];

        $currencyValue = $this->getDefaultCurrency();
        $userdateFormat = $this->getUserDateFormat();

//        Customer Categories
        $customerCategories[''] = '';
        $customerCategories['addNewCustomerCategory'] = 'Add New';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

        //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

//        Get Loyalty Card list
        $loyalty = $this->CommonTable("LoyaltyTable")->getLoyaltyList();
        $translator = new Translator();
        $loyaltyCardList[0] = $translator->translate("-- Select Loyalty Card --");
        while ($l = $loyalty->current()) {
            $loyaltyCardList[$l->id] = $l->name;
        }

//      get Active Customer Evnt for customer creation form
        $customerEvents = $this->CommonTable('Settings\Model\CustomerEventTable')->getActiveCutomerEvents();
        $customerEvenList = [];
        $customerEvenList[''] = '';
        foreach ($customerEvents as $cEvent) {
            $customerEvenList[$cEvent['customerEventID']] = $cEvent['customerEventName'];
        }

//      get Active Customer rating types
        $ratingTypes = $this->CommonTable('Invoice\Model\RatingTypesTable')->getActiveRatingTypes();
        $ratingTypesList = [];
        foreach ($ratingTypes as $rtTypes) {
            $rtTypes = (object) $rtTypes;
            $ratingTypesList[$rtTypes->ratingTypesId] = $rtTypes;
        }


        $form = new AddCustomerForm(NULL, $terms, $currency, $customerTitle, $currencyValue, $customerCategories, $userdateFormat, $loyaltyCardList, $customerEvenList, $customerPriceLists);
        $form->get('customerFormsubmit')->setValue(_("Add New Customer"));
//        set panel tile
        $translator = new Translator();
        $panelTitle = $translator->translate('Add new customer');

        $customerProfileForm = new CustomerProfileForm($countries);
        $form->get('customerCode')->setAttribute('value', $customerCode)->setAttribute('data-cuscode', $customerCode);

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $form->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $form->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $form->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $form->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $form->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $form->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $form->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $form->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

        $view = new ViewModel(array(
            'form' => $form,
            'customerProfileForm' => $customerProfileForm,
            'customers' => $data,
            'countries' => $countries,
            'panelTitle' => $panelTitle,
            'ratingTypesList' => $ratingTypesList,
            'useAccounting' => $this->useAccounting,
                )
        );
//        if reference not set
        if ($rid == '' || $rid == NULL) {
            if ($locationReferenceID == null) {
                $title = 'Customer Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Customer Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $view->addChild($refNotSet, 'refNotSet');
        }
//      Customer selector by dropdown
        $customerSelecterHeader = new ViewModel(array('customers' => $data));
        $customerSelecterHeader->setTemplate("invoice/customer/customer-selector-header");
        $view->addChild($customerSelecterHeader, 'customerSelectorHeader');

        $view->setTemplate('invoice/customer/add');

        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jquery-bar-rating/themes/fontawesome-stars.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jquery-bar-rating/themes/bars-square.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/jquery.barrating.min.js');

        return $view;
    }

    /**
     * Customer Edit page
     * @return ViewModel
     */
    private function customerEdit()
    {
        $this->setLogMessage("Customer Edit Page accessed");
        $customerID = $this->params()->fromRoute('param1', NULL);

        $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

        $customerProfileData = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerID($customerID);
        $selectedCustomerProfileData = array();
        foreach ($customerProfileData as $data) {
            $selectedCustomerProfileData[$data['customerProfileID']] = $data;
        }

        $customerRatingTypes = [];
        $customerProfileRatingTypes = [];
        $customerRatings = $this->CommonTable('Invoice\Model\CustomerRatingsTable')->getCustomerRatingsByCustomerId($customerID);
        foreach ($customerRatings as $val) {
            $val = (object) $val;
            if ($val->customerProfileId == 0) {
                $customerRatingTypes[$val->ratingTypesId] = $val;
            } else {
                $customerProfileRatingTypes[$val->customerProfileId][$val->ratingTypesId] = $val;
            }
        }



//get payment terms list
        $terms = $this->_getPaymentTerms();

//get currencys list
        $currency = $this->_getCurrencys();
//get customer list

        $data = array();
        $result = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        foreach ($result as $row) {
            $data[$row["customerID"]] = $row["customerName"] . '-' . $row["customerShortName"];
        }

// get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];

        $countries = $config['all_countries'];
        $currencyValue = $customerData->customerCurrency;
        $userdateFormat = $this->getUserDateFormat();

//        Customer Categories
        $customerCategories[''] = '';
        $customerCategories['addNewCustomerCategory'] = 'Add New';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

        //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

//        Get Loyalty Card list
        $loyalty = $this->CommonTable("LoyaltyTable")->getLoyaltyList();
        $loyaltyCardList[0] = "-- No Loyalty Card Assigned --";
        while ($l = $loyalty->current()) {
            $loyaltyCardList[$l->id] = $l->name;
        }

//      get Active Customer rating types
        $ratingTypes = $this->CommonTable('Invoice\Model\RatingTypesTable')->getActiveRatingTypes();
        $ratingTypesList = [];
        foreach ($ratingTypes as $rtTypes) {
            $rtTypes = (object) $rtTypes;
            $ratingTypesList[$rtTypes->ratingTypesId] = $rtTypes;
        }

//      get Active Customer Evnt for customer creation form
        $customerEvents = $this->CommonTable('Settings\Model\CustomerEventTable')->getActiveCutomerEvents();
        $customerEvenList = [];
        $customerEvenList[''] = '';
        foreach ($customerEvents as $cEvent) {
            $customerEvenList[$cEvent['customerEventID']] = $cEvent['customerEventName'];
        }

        $financeAccountsArray = $this->getFinanceAccounts();

        $form = new AddCustomerForm(NULL, $terms, $currency, $customerTitle, $currencyValue, $customerCategories, $userdateFormat, $loyaltyCardList, $customerEvenList, $customerPriceLists);
        $form->get('customerFormsubmit')->setValue(_("Add New Customer"));

        $form->get('customerTitle')->setValue($customerData->customerTitle);
        $form->get('customerCode')->setValue($customerData->customerCode);
        $form->get('customerName')->setValue($customerData->customerName);
        $form->get('customerShortName')->setValue($customerData->customerShortName);
        $form->get('customerTelephoneNumber')->setValue($customerData->customerTelephoneNumber);
        $form->get('customerCurrency')->setValue($customerData->customerCurrency);
        $form->get('customerDateOfBirth')->setValue($customerData->customerDateOfBirth);
        $form->get('customerIdentityNumber')->setValue($customerData->customerIdentityNumber);
        $form->get('customerCategory')->setValue($customerData->customerCategory);
        $form->get('customerPriceList')->setValue($customerData->customerPriceList);
        $form->get('customerVatNumber')->setValue($customerData->customerVatNumber);
        $form->get('customerSVatNumber')->setValue($customerData->customerSVatNumber);
        $form->get('customerCreditLimit')->setValue(number_format($customerData->customerCreditLimit, 2, '.', ''));
        $form->get('customerPaymentTerm')->setValue($customerData->customerPaymentTerm);
        $form->get('customerCurrentBalance')->setValue(number_format($customerData->customerCurrentBalance, 2));
        $form->get('customerCurrentCredit')->setValue(number_format($customerData->customerCurrentCredit, 2));
        $form->get('customerDiscount')->setValue($customerData->customerDiscount);
        $form->get('customerLoyaltyNo')->setValue($customerData->loyaltyID);
        $form->get('customerLoyaltyCode')->setValue($customerData->customerLoyaltyCode);
        $form->get('customerLoyaltyNo')->setAttribute('data-id','editCus');
        $form->get('customerLoyaltyNo')->setAttribute('data-code',$customerData->customerLoyaltyCode);
        $form->get('customerOther')->setValue($customerData->customerOther);
        $form->get('customerEvent')->setValue($customerData->customerEvent);

        if($this->useAccounting == 1){
            $customerReceviableAccountID = $customerData->customerReceviableAccountID;
            $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
            $form->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
            $form->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);

            $customerSalesAccountID = $customerData->customerSalesAccountID;
            $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
            $form->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
            $form->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);

            $customerSalesDiscountAccountID = $customerData->customerSalesDiscountAccountID;
            $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
            $form->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
            $form->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);

            $customerAdvancePaymentAccountID = $customerData->customerAdvancePaymentAccountID;
            $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
            $form->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
            $form->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
        }


//        set panel tile
        $translator = new Translator();
        $panelTitle = $translator->translate('Update the Customer');

        $customerProfileForm = new CustomerProfileForm($countries);

        $view = new ViewModel(array(
            'form' => $form,
            'customers' => $data,
            'countries' => $countries,
            'customerGender' => $customerData->customerGender,
            'customerProfileForm' => $customerProfileForm,
            'ratingTypesList' => $ratingTypesList,
            'selectedCustomerID' => $customerID,
            'panelTitle' => $panelTitle,
            'selectedCustomerProfileData' => json_encode($selectedCustomerProfileData),
            'customerRatingTypes' => json_encode($customerRatingTypes),
            'customerProfileRatingTypes' => json_encode($customerProfileRatingTypes),
            'useAccounting' => $this->useAccounting,
                )
        );

        $view->setTemplate("invoice/customer/add");
//set javascript file for add customer page
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jquery-bar-rating/themes/fontawesome-stars.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jquery-bar-rating/themes/bars-square.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/jquery.barrating.min.js');

        return $view;
    }

    public function salesCustomerEditAction()
    {
        $this->sideMenus = 'invoice_side_menu';
        $this->upperMenus = 'sales_customer_update_upper_menu';

        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }
        
        $this->setLogMessage("Customer Add Page accessed");
        $this->getSideAndUpperMenus('Customers', 'Update Customer', 'SALES');
        return $this->customerEdit();
    }

    public function enumerateModalAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespondHtml();
        }
        $postData = $request->getPost()->toArray();
        $fileData = unserialize($postData['fileData']);
        $itemList = [];
        //for separate headers and product data
        if ($postData['header'] == 1) {
            $itemList = array_slice($fileData, 1);
        } else {
            $itemList = $fileData;
        }
        
        $columnData = [];
        foreach ($itemList as $item) {
            foreach ($item as $key => $value) {
                foreach (explode(',', $value) as $ke => $val) {
                    $val = trim($val);
                    if (!(empty($val) && $val != '0') && empty($columnData[$ke])) {
                        $columnData[$ke][] = $val;
                    } else if (!(empty($val) && $val != '0') && !in_array($val, $columnData[$ke])) {
                        $columnData[$ke][] = $val;
                    }
                }
            }
        }

        $config = $this->getServiceLocator()->get('config')['fields'];
        $fieldData = [];

        if ($postData['field'] == 'customerCategory') {
            $categories = $this->CommonTable('Invoice/Model/CustomerCategoryTable')->fetchAll();
            foreach ($categories as $category) {
                $fieldData[$category['customerCategoryID']] = $category['customerCategoryName'];
            }
            $translator = new Translator();
            $fieldData['0'] = $translator->translate('ADD NEW CATEGORY');
        }
        
        $view = new ViewModel(array(
            'enumerators' => $columnData[$postData['columnId']],
            'fieldData' => $fieldData
        ));
        $view->setTemplate("invoice/customer/enumerator-modal");

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_ENUM_RETRIEVE');
        $this->html = $view;
        $this->data = count($columnData[$postData['columnId']]);

        return $this->JSONRespondHtml();
    }

    private function getFileData($data, $file)
    {
        $fileName = '/tmp/ezBiz.customerImportData.csv';
        move_uploaded_file( $file['fileupload']["tmp_name"], $fileName);
        chmod( $fileName, 0777);

        $fileenc = file($fileName);
        $encodingStatus = mb_check_encoding($fileenc[0], ($data['characterencoding'] == 0) ? 'UTF-8' : 'ISO-8859-1');

        if(!$encodingStatus){
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_ENCODING'),
                'data' => []
            ];
        }

        if (($handle = fopen($fileName, "r")) == FALSE) {
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_READ'),
                'data' => []
            ];
        }

        $dataArr = [];
        while (($fileData = fgetcsv($handle, 1000, $data['delimiter'])) !== FALSE) {
            $rowElementStatusArr = [];
            $rowElementStatusArr = array_map(function($element){
                $element = trim($element);
                return (empty($element)) ? false : true;
            }, $fileData);
            if(in_array(true, $rowElementStatusArr)){//for ignore empty lines
                $dataArr[] = $fileData;
            }
        }
        fclose($handle);
        return [
            'status' => true,
            'msg' => $this->getMessage('SUCC_UPLOADED_FILE'),
            'data' => $dataArr
        ];
    }

}

/////////////////// END OF CUSTOMER CONTROLLER \\\\\\\\\\\\\\\\\\\\






