<?php

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Core\Controller\Interfaces\DocumentInterface;
use Core\Service\ReferenceService;
use Invoice\Form\AddCustomerForm;
use Inventory\Form\ProductForm;
use Inventory\Form\CategoryForm;
use Inventory\Form\ProductHandelingForm;
use Invoice\Form\QuotationForm;
use Invoice\Model\Reference;
use Zend\I18n\Translator\Translator;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class QuotationController extends CoreController implements DocumentInterface
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'quotation_upper_menu';
    private $_quotationViewData;
    private $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID("Inventory\Controller\Product", "index", $this->user_session->userID);

        $isCanCreateItem = $isEnable;
        $this->getSideAndUpperMenus('Quotations', 'Create Quotation', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];

        $tax = array();
        $ctax = array();

        //get reference number for quotation from reference service
        $referenceService = $this->getService('ReferenceService');
        // document type 1 = quotation
        $reference = $referenceService->getReferenceNumber(1, $locationID);
        $refId = $reference['data']['referenceNo'];
        $locRefId = $reference['data']['locationReferenceID'];

        //get payment terms list
        $paymentTermService = $this->getService('PaymentTermService');
        $paymentTerms = $paymentTermService->getPaymentTerms()['data']['paymentTerms'];

        $tax_resource = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
        foreach ($tax_resource as $t) {
            $tax[$t->id] = $t->taxName;
            if ($t->taxType == 'c') {
                $ctax[$t->id] = $t->taxName;
            }
        }

        $currency = $this->getCurrencyListWithRates();
        $priceList = $this->getActivePriceList();

        // Get sales persons list
        $salesPersonService = $this->getService('SalesPersonService');
        $salesPersons = $salesPersonService->getSalesPersons()['data']['salesPersons'];

        //get job card model
        $config = $this->getService('config');
        $jobCardModel = $config['jobCardModel'];

        $currencyValue = $this->getDefaultCurrency();
        $quot_form = QuotationForm::getQuotationForm(
                                        $paymentTerms,
                                        $salesPersons, 
                                        $jobCardModel, 
                                        $currency, 
                                        $priceList,
                                        $refId,
                                        $currencyValue
                                    );
      
        $this->getService('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);

        //taking the user selected date format from the database
        $userdateFormat = $this->getUserDateFormat(); 

         // Get sales persons list
        $currencyService = $this->getService('CurrencyService');
        $currencies = $currencyService->getAllCurrency()['data']['currencies'];

        // get person Title (Mr, Miss ..) from personTitle.config file
        $customerTitle = $config['personTitle'];

        //get countries
        $countries = ['' => ''] + $config['all_countries'];

        //get customer Code
        $custReference = $referenceService->getReferenceNumber(20, $locationID);
        $customerCode = $custReference['data']['referenceNo'];

        //get Active Customer Evnt for customer creation form
        $customerService = $this->getService('CustomerService');
        $customerEventList = $customerService->getActiveCustomerEvents()['data']['customerEventList'];
      

        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

         //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $customerForm = new AddCustomerForm(NULL, $paymentTerms, $currencies, $customerTitle, $currencyValue,$customerCategories, NULL, NULL, $customerEventList, $customerPriceLists);
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);
        
        $quotation = new ViewModel(array(
            'quot_form' => $quot_form,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'userdateformat' => $userdateFormat,
            'isCanCreateItem' => $isCanCreateItem,
            'locationID' => $locationID
        ));

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' => $this->useAccounting,
        ));

        $customerAddView->setTemplate('invoice/customer/customer-add-modal');

        $productForm = new ProductForm(array(
            'supplier' => $sup,
            'tax' => $tax,
            'productType' => $productType,
            'location' => $location,
        ));

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $productSalesAccountName = $financeAccountsArray[$productSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productSalesAccountID]['financeAccountsName'];
                $productForm->get('productSalesAccountID')->setAttribute('data-id',$productSalesAccountID);
                $productForm->get('productSalesAccountID')->setAttribute('data-value',$productSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $productInventoryAccountName = $financeAccountsArray[$productInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productInventoryAccountID]['financeAccountsName'];
                $productForm->get('productInventoryAccountID')->setAttribute('data-id',$productInventoryAccountID);
                $productForm->get('productInventoryAccountID')->setAttribute('data-value',$productInventoryAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $productCOGSAccountName = $financeAccountsArray[$productCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productCOGSAccountID]['financeAccountsName'];
                $productForm->get('productCOGSAccountID')->setAttribute('data-id',$productCOGSAccountID);
                $productForm->get('productCOGSAccountID')->setAttribute('data-value',$productCOGSAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $productAdjusmentAccountName = $financeAccountsArray[$productAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productAdjusmentAccountID]['financeAccountsName'];
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-id',$productAdjusmentAccountID);
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-value',$productAdjusmentAccountName);
            }
        }

        $productForm->get('submit')->setValue('Save');
        $productForm->get('productState')->setValue('Active');
        $productForm->setAttribute('id', 'create-product-form');

        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $defaultCategoryID = array_search('General', $allCategoryList);
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        $defaultUomID = $this->CommonTable('Settings\Model\UomTable')->getUomByName('Unit')->uomID;
        $categoryForm = new CategoryForm($allCategoryList);
        $categoryForm->get('categoryParentID')->setAttribute('data-id', $defaultCategoryID);
        $productHandelingForm = new ProductHandelingForm();
        $productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", true);
        $createProductView = new ViewModel(array(
            'productForm' => $productForm,
            'productHandelingForm' => $productHandelingForm,
            'categoryForm' => $categoryForm,
            'uomList' => $uoms,
            'tax' => $tax,
            'ctax' => $ctax,
            'location' => $location,
            'defaultUom' => $defaultUomID,
            'useAccounting' => $this->useAccounting
        ));
        $createProductView->setTemplate('invoice/invoice-api/create-product');
        $quotation->addChild($createProductView, 'createProductView');

        
        $barCodeAddModal = new ViewModel();
        $barCodeAddModal->setTemplate('/inventory/product/add-bar-codes-modal');
        $quotation->addChild($barCodeAddModal, 'barCodeAddModal');


        $this->getViewHelper('HeadScript')->prependFile('/js/product.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/category.js');

        $quotation->addChild($customerAddView, 'customerAddView');
        
        //if reference id is not set, then load a modal with message to add reference to that document.
        if ($refId == null) {

            $title = 'Quotation Reference Number not set';
            $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            
            if ($locRefId != null) {
                $title = 'Quotation Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference',
            ));
            
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $quotation->addChild($refNotSet, 'refNotSet');
        }

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories', 'uomByID', 'taxes'
        ]);
        
        return $quotation;
    }

    public function listAction()
    {

        $this->getSideAndUpperMenus('Quotations', 'View Quotations', 'SALES');
        $this->getPaginatedQuotations();
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();

        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }

        $quot_form = new QuotationForm(array(
            'name' => 'quatation',
            'id' => 'qot_no',
            'terms' => $terms
        ));


        $location = $this->user_session->userActiveLocation["locationID"];
        $sales_order = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll($location, false);
        $salesorder = array();
        foreach ($sales_order as $sa) {
            if ($sa['quotationID'] != '') {
                $salesorder[$sa['quotationID']] = $sa['quotationID'];
            }
        }
        // get quotationIDs of copyied product
        $copiedQuotations = $this->CommonTable('Invoice\Model\QuotationTable')->getCopiedQuotationProductDetails($location);
        foreach ($copiedQuotations as $copiedQuotation) {
            if ($copiedQuotation['quotationProductCopied'] == '1') {
                $copiedQuoationList[$copiedQuotation['quotationID']] = $copiedQuotation['quotationID'];
            }
        }

        $userdateFormat = $this->getUserDateFormat();
        $quotation = new ViewModel(array(
            'quot_form' => $quot_form,
            'quotation' => $this->paginator,
            'salesOrder' => $salesorder,
            'copiedQuotations' => $copiedQuoationList,
            'statuses' => $this->getStatusesList(),
            'cur' => $this->companyCurrencySymbol,
            'userdateFormat' => $userdateFormat,
            'locationID' => $location
        ));

        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('invoice/quotation/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/quotation/document-view');
        $quotation->addChild($docHistoryView, 'docHistoryView');
        $quotation->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $quotation->addChild($attachmentsView, 'attachmentsView');

        return $quotation;
    }

    public function documentAction()
    {
        $documentType = 'Sales Quotation';
        $quotationID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        echo $this->generateDocument($quotationID, $documentType, $templateID);
        exit;
    }

    public function getDataForDocument($quotationID)
    {
        if (!empty($this->_quotationViewData)) {
            return $this->_quotationViewData;
        }

        $itemAttrDetails = $this->CommonTable('Settings\Model\ItemAttributeTable')->fetchAll();
        $itemAttrDetailArray = [];
        foreach ($itemAttrDetails as $value) {
            $itemAttrDetailArray[$value['itemAttributeID']] = $value['itemAttributeName'];
        }

        $data = $this->getDataForDocumentView();

        $quotationDetails = (array) $this->CommonTable('Invoice\Model\QuotationTable')->getAllQuotationDetailsByQuotationID($quotationID)->current();
        $quotationDetails['quotationIssuedDate'] = $this->convertDateToUserFormat($quotationDetails['quotationIssuedDate']); //$quotationDetails['quotationIssuedDate'];
        $quotationDetails['quotationExpireDate'] = $this->convertDateToUserFormat($quotationDetails['quotationExpireDate']); //$quotationDetails['quotationExpireDate'];

        $quotationDetails['quotationComment'] = $quotationDetails['quotationComment'];
        $quotationDetails['quotationCode'] = $quotationDetails['quotationCode'];

        $customCurrencyId = $quotationDetails['customCurrencyId'];

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId == "" || $customCurrencyId == 0) ? 1 : $quotationDetails['quotationCustomCurrencyRate'];
        $customCurrencySymbol = ($customCurrencyId == "" || $customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->currencySymbol;
        $quotationDetails['customCurrencySymbol'] = $customCurrencySymbol;
        $quotationDetails['createdTimeStamp'] = $this->getUserDateTime($quotationDetails['createdTimeStamp']);
        $quotationDetails['salesPerson'] = ($quotationDetails['salesPersonID'] != 0) ?  
                $quotationDetails['salesPersonTitle'] . ' ' . $quotationDetails['salesPersonFirstName'] . ' ' . $quotationDetails['salesPersonLastName'] : '-';
        
// to be used by below functions - convert to object
        $quotationDetails = (object) $quotationDetails;
        $products = $this->CommonTable('Invoice\Model\QuotationProductTable')->getProductsByQuotationID($quotationID);
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerAndPrimaryProfileByID($quotationDetails->customerID);

//        Add customer Primary Profile address to customer address and email
        $customer->customerAddress = (($customer->customerProfileLocationNo) ? $customer->customerProfileLocationNo . ', ' : '') .
                (($customer->customerProfileLocationRoadName1) ? $customer->customerProfileLocationRoadName1 . ', ' : '') .
                (($customer->customerProfileLocationRoadName2) ? $customer->customerProfileLocationRoadName2 . ', ' : '') .
                (($customer->customerProfileLocationRoadName3) ? $customer->customerProfileLocationRoadName3 . ', ' : '') .
                (($customer->customerProfileLocationSubTown) ? $customer->customerProfileLocationSubTown . ', ' : '') .
                (($customer->customerProfileLocationTown) ? $customer->customerProfileLocationTown . ', ' : '') .
                (($customer->customerProfileLocationPostalCode) ? $customer->customerProfileLocationPostalCode . ', ' : '') .
                (($customer->customerProfileLocationPostalCode) ? $customer->customerProfileLocationPostalCode . ', ' : '') .
                ($customer->customerProfileLocationCountry) . '.';
        $customer->customerEmail = $customer->customerProfileEmail;
        
// if customer is available, quotataion.customerName gets replaced
        $data = array_merge($data, (array) $quotationDetails, (array) $customer);

        // get template specific data for the document
        $templateDefaultOptions = array('product_table' => $this->getServiceLocator()->get('config')['template-data-table']['quotation']);

        // get template options from database and override the default options
        $templateOptions = array_replace_recursive($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);
        $defaultFooterRows = [];
        foreach ($templateDefaultOptions['product_table']['rows'] as $row) {
            $defaultFooterRows[$row['name']] = $row;
        }

        $templateFooterRows = [];
        foreach ((array) $this->templateDetails['templateOptions']['product_table']['rows'] as $row) {
            $templateFooterRows[$row['name']] = $row;
        }

        $templateOptions['product_table']['rows'] = $templateFooterRows + $defaultFooterRows;

        $data['templateOptions'] = $templateOptions;

        // reorder columns
        $productsTableColumns = array_filter($templateOptions['product_table']['columns'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableColumns, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        // reorder rows
        $productsTableRows = array_filter($templateOptions['product_table']['rows'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableRows, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        $templateOptions['product_table']['columns'] = $productsTableColumns;
        $templateOptions['product_table']['rows'] = $productsTableRows;

        $allowedColumnsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['columns']);

        $allowedRowsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['rows']);


        // to prevent loading categories from the database in the following foreach loop
        $allCategories = array();
        if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
            $allCategories = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        }

        $subTotalWithoutDiscount = 0;


        foreach ($products as $productIndex => $product) {

            $product = (object) $product;
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID, $product->uomID);

            $qty = ($product->quotationProductQuantity == 0) ? 1 : $product->quotationProductQuantity; // as a precaution
            $item_tot_tax = ($product->taxTotal > 0) ? $this->getProductUnitPriceViaDisplayUom((($product->taxTotal / $customCurrencyRate) / $product->quotationProductQuantity), $productUom) : 0.00;
            $item_tax_string = ($item_tot_tax > 0) ? "Tax: " . $data['currencySymbol'] . number_format($item_tot_tax, 2) : "";
            if ($product->quotationProductDiscountType == 'vl') {
                $item_dic_string = ($product->quotationProductDiscount > 0) ? "Disc: " . $this->getProductUnitPriceViaDisplayUom(($product->quotationProductDiscount / $customCurrencyRate), $productUom) . " " . $data['currencySymbol'] : "";
                $product->totalDiscount = $qty * ($product->quotationProductDiscount / $customCurrencyRate);
            } else {
                $item_dic_string = ($product->quotationProductDiscount > 0) ? "Disc: " . $product->quotationProductDiscount . "%" : "";
                $product->totalDiscount = (($product->quotationProductUnitPrice / $customCurrencyRate) * $qty) / 100 * $product->quotationProductDiscount;
            }


            $totalDiscount += floatval($product->totalDiscount);

            $productDescLine = "";

            //Product description line
            if ($product->quotationProductDescription != '' && $product->quotationProductDescription != NULL) {
                $productDescLine = "<br /><small class=\"description\"><span>(</span>" . $product->quotationProductDescription . "<span>)</span></small>";
                $productDiscription = $product->quotationProductDescription;

            } else {
                $productDiscription = '';
            }

            if ($quotationDetails->quotationShowTax == "1") {    // this must be rebuld by adding a column to the database.
                $sub_line_vals = array_filter(array($item_tax_string, $item_dic_string));
                if (!empty($sub_line_vals)) {
                    $sub_line = "<br /><small class=\"taxes\">(" . implode(",&nbsp;", $sub_line_vals) . ")</small>";
                }
                $product->unitPrice = ($product->quotationProductUnitPrice / $customCurrencyRate);

            } else {
                $product->unitPrice = (floatval($product->quotationProductTotal / $customCurrencyRate) + floatval($product->totalDiscount)) / floatval($qty);
                $sub_line = ($product->quotationProductDiscount > 0) ? "<br><small>(" . $item_dic_string . ")</small>" : "";
            }

            // get template options from database and override the default options
            $templateDefaultOptions = array(
                'categorize_products' => false
            );


            //use to set discount and unite price when product tax exists.
            $totalTaxValuePerItem = 0;

            if ($product->productTaxEligible == '1') {
                if ($product->quotationProductDiscountType == 'pr' && $quotationDetails->quotationShowTax != "1") {
                    $totalTaxValuePerItem = ((floatval($product->taxTotal / $customCurrencyRate) * 100) / (100 - floatval($product->quotationProductDiscount))) / floatval($product->quotationProductQuantity);
                    $product->unitPrice = floatval($totalTaxValuePerItem) + floatval($product->quotationProductUnitPrice / $customCurrencyRate);
                    $product->totalDiscount = ($product->unitPrice - (floatval($product->quotationProductTotal / $customCurrencyRate) / floatval($product->quotationProductQuantity))) * floatval($product->quotationProductQuantity);
                }
            }

            $unitPrice = $this->getProductUnitPriceViaDisplayUom(($product->unitPrice), $productUom);
            $thisqty = $this->getProductQuantityViaDisplayUom($product->quotationProductQuantity, $productUom);
            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

            $lineItemDiscount = ($product->quotationProductDiscount > 0) ? (($product->quotationProductDiscountType == 'vl') ? number_format($product->totalDiscount, 2) : $product->quotationProductDiscount . '%') : '-';
            $itemTotalWithoutDiscount = (($product->quotationProductUnitPrice * $product->quotationProductQuantity) + $product->taxTotal) / $customCurrencyRate;

            //calculate tax value per item
            $itemUnitTax = floatval($product->taxTotal / $customCurrencyRate)/ floatval($product->quotationProductQuantity);

            //calculate discount value per item
            $itemUnitDisc = floatval($product->totalDiscount / $customCurrencyRate)/ floatval($product->quotationProductQuantity);

            //calculate unit tax for real item price
            $realUnitTax = $this->calculateProductUnitTax($product->quotationProductUnitPrice, $product->productID);

            $attribute_01 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeOneID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_02 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeTwoID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_03 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeThreeID,$product->productID)->current()['itemAttributeValueDescription'];

            $product_record = array(
                'product_index' => $productIndex + 1,
                'product_code' => $product->productCode,
                'product_name' => $product->productName . $productDescLine . $sub_line,
                'product_description' => $productDiscription,
                'product_code_and_name' => $product->productCode . ' - ' . $product->productName . $productDescLine . $sub_line,
                'quantity' => $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'],
                'price' => number_format($unitPrice, 2),
                'price_w_tax' => number_format((floatval($product->quotationProductUnitPrice) + floatval($realUnitTax)),2 ),
                'discount' => number_format($product->totalDiscount, 2),
                'discount_per_or_val' => $lineItemDiscount,
                'total' => number_format(($product->quotationProductTotal / $customCurrencyRate), 2),
                'total_wo_tx' => number_format(($product->quotationProductTotal - $product->taxTotal) / $customCurrencyRate, 2),
                'total_wo_discounts' => number_format($itemTotalWithoutDiscount, 2),
                'attribute_01' => (!is_null($attribute_01)) ? $attribute_01 : '-',
                'attribute_02' => (!is_null($attribute_02)) ? $attribute_02 : '-',
                'attribute_03' => (!is_null($attribute_03)) ? $attribute_03 : '-',
            );

            $subTotalWithoutDiscount = $subTotalWithoutDiscount + floatval($itemTotalWithoutDiscount);

            $templateOptions = array_merge($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);
            // filter only the columns that need to be displayed
            $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

            // nest products under category if they should be categorized
            if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
                $categoryId = $product->categoryID;
                $records[$categoryId]['products'][] = $product_record;

                // add category name to the list
                if (!isset($records[$categoryId]['category'])) {
                    $records[$categoryId]['category'] = (isset($allCategories[$categoryId])) ? $allCategories[$categoryId] : ''; // change this to category name
                }
            } else {
                $records[0]['products'][] = $product_record;
            }

            $sub_total+= ($product->quotationProductTotal / $customCurrencyRate);
            $product->unitPrice = $unitPrice;
            $product->itemQuntity = $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'];
            $data['productsList'][] = (array) $product;
        }

        $compoundTaxes = [];
        foreach ($taxDetails as $key => $taxValue) {
            $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;
        }
        
        $txTypes = array();
        $totalTax = 0;
        $normalTaxTotal = 0;
        $compoundTaxTotal = 0;
        $calculatableTaxesTotal = 0;
        $normalTaxesSet = array();
        $compoundTaxesSet = array();
        $cmTaxDetails = [];
        $nmlTaxDetails = [];
        $productsTax = $this->CommonTable('Invoice\Model\QuotationProductTaxTable')->getQuotationTax($quotationID);
        foreach ($productsTax as $productTax) {

            $thisTax = $productTax['quotTaxAmount'] / $customCurrencyRate;
            if (!isset($txTypes[$productTax['taxName']])) {
                $txTypes[$productTax['taxName']] = $thisTax;
            } else {
                $txTypes[$productTax['taxName']] += $thisTax;
            }
            
            $calculatableTaxesTotal += ($productTax['taxSuspendable'] != 1) ? $thisTax : 0;
            if ($productTax['taxType'] == 'v') {
                $normalTaxTotal += floatval($thisTax);
                if (!array_key_exists($productTax['id'], $nmlTaxDetails)) {
                    $normalTaxesSet[] = $productTax['taxName'];
                    $nmlTaxDetails[$productTax['id']] =  $productTax['id'];
                }
                
            } else if ($productTax['taxType'] == 'c') {
                $compoundTaxTotal += floatval($thisTax);
                if (!array_key_exists($productTax['id'], $cmTaxDetails)) {
                    $compoundTaxesSet[] = $productTax['taxName'];
                    $cmTaxDetails[$productTax['id']] =  $productTax['id'];   
                }
            }
            $totalTax += $thisTax;
        }

        $data['calculatableTaxesTotal'] = $calculatableTaxesTotal;
        $data['suspendedTaxesTotal'] = $suspendedTaxesTotal;
        $data['normalTaxTotal'] = number_format($normalTaxTotal, 2);
        $data['normalTaxesSet'] = (!empty($normalTaxesSet)) ? implode(', ', array_unique($normalTaxesSet)) : '';
        $data['compoundTaxesSet'] = (!empty($compoundTaxesSet)) ? implode(', ', array_unique($compoundTaxesSet)) : '';
        $data['compoundTaxTotal'] = number_format($compoundTaxTotal, 2);
        $data['calculatableTaxes'] = (!empty($calculatableTaxes)) ? implode(', ', array_unique($calculatableTaxes)) : '';
        $data['suspendableTaxes'] = (!empty($suspendableTaxes)) ? implode(', ', array_unique($suspendableTaxes)) : '';

        $tx_line = "";
        foreach ($txTypes as $t_name => $t_val) {
            $tx_line.=($tx_line == null) ? $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2) :
                    ",&nbsp;" . $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2);
        }

        $tx_line = trim($tx_line, "&nbsp;");

        $headers = array(
            'product_index' => array('label' => _(""), 'align' => 'left'),
            'product_code' => array('label' => _("Item Code"), 'align' => 'left'),
            'product_name' => array('label' => _("Item Name"), 'align' => 'left'),
            'product_code_and_name' => array('label' => _("Item Code & Name"), 'align' => 'left'),
            'product_description' => array('label' => _("Product Description"), 'align' => 'left'),
            'quantity' => array('label' => _("Quantity"), 'align' => 'left'),
            'price' => array('label' => _("Unit Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'price_w_tax' => array('label' => _("Unit Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'discount' => array('label' => _("Total Discount (-)"), 'align' => 'right'),
            'discount_per_or_val' => array('label' => _("Discount (-)"), 'align' => 'right'),
            'total' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_tx' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_discounts' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'attribute_01' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeOneID]), 'align' => 'left'),
            'attribute_02' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeTwoID]), 'align' => 'left'),
            'attribute_03' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeThreeID]), 'align' => 'left'),
        );


        $footer_rows = array(
            'sub_total' => true,
            'discount' => true,
            'vat' => true,
            'total' => true
        );

        // filter only the headers that need to be displayed
        $headers = array_splice(array_replace(array_flip($allowedColumnsList), $headers), 0, count($allowedColumnsList));

        // filter only the footer rows that need to be displayed
        $footer_rows = array_splice(array_replace(array_flip($allowedRowsList), $footer_rows), 0, count($allowedRowsList));

        // set column width
        if (isset($templateOptions['product_table']['columns'])) {
            foreach ($templateOptions['product_table']['columns'] as $column) {
                $columnName = $column['name'];
                $columnWidth = $column['width'];

                // if column is in allowed list
                if (isset($headers[$columnName])) {
                    $headers[$columnName]['width'] = $columnWidth;
                }
            }
        }

        $product_record = count($product_record) ? $product_record : [];
        $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

        $data['table'] = array(
            'hide_table_header' => ($templateOptions['product_table']['hide_table_header'] === true || $templateOptions['product_table']['hide_table_header'] == 'true'),
            'hide_table_borders' => ($templateOptions['product_table']['hide_table_borders'] === true || $templateOptions['product_table']['hide_table_borders'] == 'true'),
            'headers' => $headers,
            'footers' => $footer_rows,
            'records' => $records,
        );

        $itemWiseDiscount = $totalDiscount;
        $totalDiscount += ($invoiceDetails->salesInvoiceTotalDiscount / $customCurrencyRate);

        $data['comment'] = $quotationDetails->quotationComment;
        $data['sub_total'] = $sub_total;
        $data['sub_total_wo_tax'] = $sub_total - $totalTax;
        $data['sub_total_wo_discounts'] = $subTotalWithoutDiscount;
        $data['total'] = $quotationDetails->quotationTotalAmount / $customCurrencyRate;
        $data['totalTax'] = number_format($totalTax, 2);
        $data['discount'] = number_format($totalDiscount, 2); // total discount received
        $data['discountStatement'] = _("Total discount received:");

        $data['show_tax'] = $quotationDetails->quotationShowTax == "1" ? true : false;
        $data['tax_record'] = "<strong>" . _("Tax:") . "</strong>&nbsp;";
        $data['tax_record_line'] = $tx_line;

        if ($data['customerShortName'] == 'DefaultCustomer') {
            $data['customerName'] = '';
        } else {
            $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];
        }


        $data["email"] = array(
            "to" => $customer->email,
            "subject" => "Quotation from a company.",
            "body" => "Dear " . $customer->customerName . ",<br><br>Thank you for your Inquiry.<br><br>A quotation has been generated for you from " . $this->company->companyName . " and attached herewith.<br><br>Quotation Number: " . $quotationDetails->quotation_no . "<br>Quotation Amount: " . $quotationDetails->total / $customCurrencyRate . "<br><br>Looking forward to hearing from you soon.<br><br>Best Regards<br>ThinkCube" . $this->company->companyName
        );
        $data['cdnUrl'] = $this->cdnUrl;

        $tempTotalValue = 0;
        //use to create total discount and itemwise discount
        $item_count = sizeof($data['productsList']);
        for ($i = 0; $i < $item_count; $i++) {
            $tempTotalValue+=$data['productsList'][$i]['totalDiscount'];
        }

        return $this->_quotationViewData = $data;
    }

    public function getDocumentDataTable($quotationCode, $documentSize = 'A4')
    {
        $data_table_vars = $this->getDataForDocument($quotationCode);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/quotation/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);
        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    public function quotationViewAction()
    {
        return $this->documentPreviewAction();
    }

// Displays the overlay which loads an iframe inside (calls documentAction)
    public function documentPreviewAction()
    {

        $quotationCode = $this->params()->fromRoute('param2');
        $quotationID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($quotationID);
        $data['quotationCode'] = $data['quotationCode'];
        $data['total'] = number_format($data['total'], 2);
        $path = "/quotation/document/"; //.$quotationCode; //////////
        $translator = new Translator();
        $createNew = $translator->translate('New Quotation');
        $createPath = '/quotation/create';
        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Quotation from " . $data['companyName'],
            "body" => <<<EMAILBODY

Dear {$data['customerName']}, <br /><br />

Thank you for your inquiry. <br /><br />

A quotation has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>Quotation No:</strong> {$data['quotationCode']} <br />
<strong>Quotation Amount:</strong> {$data['customCurrencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );
        $view = new ViewModel($data);
        $view->setTerminal(TRUE);
        $documentType = 'Sales Quotation';

        $commonView = $this->getCommonPreview($data, $path, $createNew, $documentType, $quotationID, $createPath);
        $commonView->setTerminal(true);
        return $commonView;
    }

    public function EditAction()
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID("Inventory\Controller\Product", "index", $this->user_session->userID);

        $isCanCreateItem = $isEnable;
        $this->getSideAndUpperMenus('Quotations', 'View Quotations', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $tax = array();
        $ctax = array();
        $quotationID = $this->params()->fromRoute('param1');
        $refData = $this->getReferenceNoForLocation(1, $locationID);
        $rid = $refData["refNo"];
        $quatationData = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByID($quotationID);
        $cusProfDetails = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerProfileID($quatationData->customerProfileID)->current();
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }

        $tax_resource = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
        foreach ($tax_resource as $t) {
            $tax[$t->id] = $t->taxName;
            if ($t->taxType == 'c') {
                $ctax[$t->id] = $t->taxName;
            }
        }

        // Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }
        $currency = $this->getCurrencyListWithRates();
        $priceList = $this->getActivePriceList();

        $quot_form = new QuotationForm(array(
            'name' => 'quatation',
            'id' => 'qot_no',
            'terms' => $terms,
            'salesPersons' => $salesPersons,
            'customCurrency' => $currency,
            'priceList' => $priceList,
        ));
        $quot_form->get('qot_no')->setValue($rid);

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);

        $userdateFormat = $this->getUserDateFormat();
        $quotation = new ViewModel(array(
            'quot_form' => $quot_form,
            'cur' => $this->companyCurrencySymbol,
            'edit_ID' => $quotationID,
            'currentLocationID' => $locationID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'cusID' => $quatationData->customerID,
            'cusProfID' => $quatationData->customerProfileID,
            'isCanCreateItem' => $isCanCreateItem,
            'cusProfName' => $cusProfDetails['customerProfileName'],
            'activeCusList' => $this->getActiveCustomerList(),
            'userdateformat' => $userdateFormat  /////user date format for edit action
        ));

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }
        $financeAccountsArray = $this->getFinanceAccounts();

        $productForm = new ProductForm(array(
            'supplier' => $sup,
            'tax' => $tax,
            'productType' => $productType,
            'location' => $location,
        ));

         $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $productSalesAccountName = $financeAccountsArray[$productSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productSalesAccountID]['financeAccountsName'];
                $productForm->get('productSalesAccountID')->setAttribute('data-id',$productSalesAccountID);
                $productForm->get('productSalesAccountID')->setAttribute('data-value',$productSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $productInventoryAccountName = $financeAccountsArray[$productInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productInventoryAccountID]['financeAccountsName'];
                $productForm->get('productInventoryAccountID')->setAttribute('data-id',$productInventoryAccountID);
                $productForm->get('productInventoryAccountID')->setAttribute('data-value',$productInventoryAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $productCOGSAccountName = $financeAccountsArray[$productCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productCOGSAccountID]['financeAccountsName'];
                $productForm->get('productCOGSAccountID')->setAttribute('data-id',$productCOGSAccountID);
                $productForm->get('productCOGSAccountID')->setAttribute('data-value',$productCOGSAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $productAdjusmentAccountName = $financeAccountsArray[$productAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productAdjusmentAccountID]['financeAccountsName'];
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-id',$productAdjusmentAccountID);
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-value',$productAdjusmentAccountName);
            }
        }


        $productForm->get('submit')->setValue('Save');
        $productForm->get('productState')->setValue('Active');
        $productForm->setAttribute('id', 'create-product-form');

        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $defaultCategoryID = array_search('General', $allCategoryList);
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        $defaultUomID = $this->CommonTable('Settings\Model\UomTable')->getUomByName('Unit')->uomID;
        $categoryForm = new CategoryForm($allCategoryList);
        $categoryForm->get('categoryParentID')->setAttribute('data-id', $defaultCategoryID);
        $productHandelingForm = new ProductHandelingForm();
        $productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", true);
        $createProductView = new ViewModel(array(
            'productForm' => $productForm,
            'productHandelingForm' => $productHandelingForm,
            'categoryForm' => $categoryForm,
            'uomList' => $uoms,
            'tax' => $tax,
            'ctax' => $ctax,
            'location' => $location,
            'defaultUom' => $defaultUomID,
            'useAccounting' => $this->useAccounting
        ));
        $createProductView->setTemplate('invoice/invoice-api/create-product');
        $quotation->addChild($createProductView, 'createProductView');

        $barCodeAddModal = new ViewModel();
        $barCodeAddModal->setTemplate('/inventory/product/add-bar-codes-modal');
        $quotation->addChild($barCodeAddModal, 'barCodeAddModal');

        $this->getViewHelper('HeadScript')->prependFile('/js/product.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/category.js');

        $cust_form = new AddCustomerForm('add_new_customer', $terms);
        $cust_form->setAttribute('id', 'add_customer_form');
        $cust_form->setAttribute('name', 'add_customer_form');
        $cust_form->get('customerDiscount')->setAttribute("id", "discnt");
        $add_cust = new ViewModel(array(
            'form' => $cust_form,
            'action' => 'add',
        ));
        $add_cust->setTemplate('/invoice/customer-api/get-customer-edit-box');
        $quotation->addChild($add_cust, 'cust_formv');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $quotation->addChild($attachmentsView, 'attachmentsView');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories', 'uomByID', 'taxes'
        ]);

        return $quotation;
    }

    private function getPaginatedQuotations()
    {
        $location = $this->user_session->userActiveLocation["locationID"];
        $this->paginator = $this->CommonTable('Invoice\Model\QuotationTable')->fetchAllByLocation($location, $paginated = true);
        $this->paginator->setCurrentPageNumber($this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    //get product list and make two json arrays for typerheads.
    public function getProductJson()
    {
        $pr_boject = $this->CommonTable('Invoice\Model\ProductTable')->getProductNames();

        while ($row = $pr_boject->current()) {
            $pro_ids[] = $row['id'];
            $pro_ids[] = $row['id'];
            $pro_name[] = $row['productName'];
            $pro_details[$row['id']] = $row;
            $pro_name_map[$row['productName']] = $row['id'];
        }
        return array(
            "pro_ids" => json_encode($pro_ids),
            "pro_name" => json_encode($pro_name),
            "pro_details" => json_encode($pro_details),
            "pro_name_map" => json_encode($pro_name_map)
        );
    }

    public function getCustomerNameJson()
    {
        $cust_obj = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerNameList();
        while ($row1 = $cust_obj->current()) {
            $cust_list[] = $row1["cust_name"];
        }
        return json_encode($cust_list);
    }

    //get all quotation id list for typerhead
    public function getQuotationIdsJson()
    {
        $qot_obj = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotationIdList();
        while ($row = $qot_obj->current()) {
            $quot_list[] = $row['id'];
        }
        return json_encode($quot_list);
    }

    public function documentPdfAction()
    {
        $quotationID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Sales Quotation';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/invoice/quotation/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($quotationID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($quotationID, $documentType, $documentData, $templateID);

        return;
    }

}

/////////////////// END OF IMVOICE CONTROLLER \\\\\\\\\\\\\\\\\\\\
