<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains ReturnAPI related controller functions
 */

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Invoice\Model\Returns;
use Invoice\Model\ReturnsProduct;
use Invoice\Model\ReturnsProductTax;
use Invoice\Model\ReturnsSubProduct;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Invoice\Model\Reference;
use Zend\Session\Container;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemOut;
use Inventory\Model\ProductSerial;
use Inventory\Model\ProductBatch;
use Inventory\Model\PurchaseInvoiceProduct;
use Inventory\Model\PurchaseInvoice;
use Inventory\Model\PurchaseInvoiceProductTax;

class ReturnAPIController extends CoreController
{

    protected $userID;
    protected $user_session;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function saveReturnDetailsAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $multipleDlnCopyFlag = $request->getPost('multipleDlnCopyFlag');
            $customerID = $request->getPost('customerID');
            $customerName = $request->getPost('customerName');
            $dimensionData = $request->getPost('dimensionData');
            $customCurrencyId = $request->getPost('customCurrencyId');
            $crossLocationFlag = ($request->getPost('crossLocationFlag') === 'false') ? false : true;
            $delBatchDetails = $request->getPost('delBatchDetails');
            $delSerialDetails = $request->getPost('delSerialDetails');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $salesReturnCustomCurrencyRate = (float) $request->getPost('salesReturnCustomCurrencyRate');
            //get customCurrency data by customer CurrencyId
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;
            
            $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
            if ($cust->customerName . '-' . $cust->customerCode == $customerName) {
                $products = $request->getPost('products');
                $subProducts = $request->getPost('subProducts');
                $returnCode = $request->getPost('returnCode');
                $locationID = $request->getPost('locationID');
                $date = $this->convertDateToStandardFormat($request->getPost('date'));
                $totalPrice = (float) trim($request->getPost('returnTotalPrice')) * $salesReturnCustomCurrencyRate;
                $comment = $request->getPost('returnComment');

                $this->beginTransaction();

                $result = $this->getReferenceNoForLocation('7', $locationID);
                $locationReferenceID = $result['locRefID'];

                //check return numer already exist in return table if exist give next number for return code
                while ($returnCode) {
                    if ($this->CommonTable('SalesReturnsTable')->checkReturnByCode($returnCode)->current()) {
                        if ($locationReferenceID) {
                            $newReturnCode = $this->getReferenceNumber($locationReferenceID);
                            if ($newReturnCode == $returnCode) {
                                $this->updateReferenceNumber($locationReferenceID);
                                $returnCode = $this->getReferenceNumber($locationReferenceID);
                            } else {
                                $returnCode = $newReturnCode;
                            }
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_RETURNAPI_CODE_EXIST');
                            return $this->JSONRespond();
                        }
                    } else {
                        break;
                    }
                }

                $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
                $displayData = $result->current();
                $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
                $averageCostingFlag = $displayData['averageCostingFlag'];

                if(empty($FIFOCostingFlag) && empty($averageCostingFlag)){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE');
                    return $this->JSONRespond();
                }
                
                $deliveryNoteID = $request->getPost('deliveryNoteID');
                $deliveryNoteOriginalLocationID = $locationID;
                if ($multipleDlnCopyFlag == "false") {
                    $deliveryNoteStatus = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteStatusByDeliveryNoteID($deliveryNoteID)->current();
                    if ($deliveryNoteStatus['deliveryNoteStatus'] == "10") {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_INVOICE_DLN');
                        return $this->JSONRespond();
                    }

                    $deliveryNoteOriginalLocationID = $deliveryNoteStatus['deliveryNoteLocationID'];
                    

                    $checkProduct = $this->_checkDeliveryNoteProductsByDeliveryNoteID($deliveryNoteID);
                    if ($checkProduct == "0") {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_RETURNAPI_RETURN_ALL_PRODUCTS');
                        return $this->JSONRespond();
                    }
                } else {
                    foreach ($deliveryNoteID as $key => $value) {
                        $deliveryNoteStatus = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteStatusByDeliveryNoteID($value)->current();
                        if ($deliveryNoteStatus['deliveryNoteStatus'] == "10") {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_INVOICE_DLN');
                            return $this->JSONRespond();
                        }

                        $checkProduct = $this->_checkDeliveryNoteProductsByDeliveryNoteID($value);
                        if ($checkProduct == "0") {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_RETURNAPI_RETURN_ALL_PRODUCTS');
                            return $this->JSONRespond();
                        }
                    }
                }




                $entityID = $this->createEntity();
                $returnData = array(
                    'salesReturnCode' => $returnCode,
                    'customerID' => $customerID,
                    'locationID' => $locationID,
                    'salesReturnDate' => $date,
                    'salesReturnTotal' => $totalPrice,
                    'salesReturnComment' => $comment,
                    'statusID' => 4,
                    'customCurrencyId' => $customCurrencyId,
                    'salesReturnCustomCurrencyRate' => $salesReturnCustomCurrencyRate,
                    'entityID' => $entityID
                );

                $return = new Returns;
                $return->exchangeArray($returnData);

                $returnID = $this->CommonTable('SalesReturnsTable')->saveSalesReturn($return);

                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }

                $accountProduct = array();
                
                foreach ($products as $key => $p) {
                    $batchProducts = $subProducts[$p['productID']];
                    
                    if ($multipleDlnCopyFlag == "false") {
                        $productID = $p['productID'];
                    } else {
                        $productID = $p['productID'];
                        $productID = explode("_", $productID)[1];
                    }
                    $deliveryNoteProductID = $p['deliveryNoteproductID'];
                    $loreturnQty = $returnQty = $p['returnQuantity']['qty'];
                    $productType = $p['productType'];
                    if ($productType == 2) {
                        $loreturnQty = 0;
                    }

                    $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

                    if($this->useAccounting == 1){
                        if(empty($pData['productInventoryAccountID'])){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                            return $this->JSONRespond();
                        }
                         //check whether Product Cost of Goods Sold Gl account id set or not
                        if(empty($pData['productCOGSAccountID'])){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName']));
                            return $this->JSONRespond();
                        }
                    }

                    $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationID);
                    $locationProductID = $locationProduct->locationProductID;
                    //get location Product Data for set average costing Price
                    $locationProductAverageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
                    $locationProductQuantity = $locationProduct->locationProductQuantity;
                    $locationProductLastItemInID = $locationProduct->locationProductLastItemInID;

                    $locationProductData = array(
                        'locationProductQuantity' => $locationProduct->locationProductQuantity + $loreturnQty,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductData, $productID, $locationID);

                    // get original deliveryNote location Product Details
                    $origLocationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $deliveryNoteOriginalLocationID);
                    $origLocationProductID = $origLocationProduct->locationProductID;

                    //calculate discount value
                    $discountValue = 0;
                    if ($p['productDiscountType'] == "precentage") {
                        $discountValue = ($p['productPrice']  * $p['productDiscount']/ 100) * $salesReturnCustomCurrencyRate;
                    } else if ($p['productDiscountType'] == "value") {
                        $discountValue = $p['productDiscount'] * $salesReturnCustomCurrencyRate;
                        $p['productDiscount'] = $p['productDiscount'] * $salesReturnCustomCurrencyRate;
                    }
                    
                    $returnProductData = array(
                        'salesReturnID' => $returnID,
                        'deliveryNoteProductID' => $deliveryNoteProductID,
                        'productID' => $productID,
                        'salesReturnProductPrice' => $p['productPrice'] * $salesReturnCustomCurrencyRate,
                        'salesReturnProductDiscount' => $p['productDiscount'],
                        'salesReturnProductDiscountType' => $p['productDiscountType'],
                        'salesReturnProductTotal' => $p['productTotal'] * $salesReturnCustomCurrencyRate,
                        'salesReturnProductQuantity' => $returnQty
                    );
                    $returnProdut = new ReturnsProduct;
                    $returnProdut->exchangeArray($returnProductData);
                    $returnProductID = $this->CommonTable('SalesReturnsProductTable')->saveReturnProducts($returnProdut);

                    $deliveryNoteProductQuantity = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductQuantityByDeliveryNoteProductID($deliveryNoteProductID)->current();

                    $deliveryNoteRemainQty = $deliveryNoteProductQuantity['deliveryNoteProductQuantity'] - $deliveryNoteProductQuantity['deliveryNoteProductCopiedQuantity'];

                    $dNProductCopiedQuantity = $deliveryNoteProductQuantity['deliveryNoteProductCopiedQuantity'] + $returnQty;

                    if ($dNProductCopiedQuantity < $deliveryNoteProductQuantity['deliveryNoteProductQuantity']) {
                        $deliveryNoteProductQuantityUpdateData = array(
                            'deliveryNoteProductCopiedQuantity' => $dNProductCopiedQuantity
                        );
                    } else {
                        $deliveryNoteProductQuantityUpdateData = array(
                            'deliveryNoteProductCopiedQuantity' => $dNProductCopiedQuantity,
                            'copied' => 1
                        );
                    }

                    $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateDeliveryNoteProductReturnQuantity($deliveryNoteProductID, $deliveryNoteProductQuantityUpdateData);

                    if ($p['pTax']) {
                        foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                            $returnProductTaxData = array(
                                'salesReturnProductID' => $returnProductID,
                                'taxID' => $taxKey,
                                'salesReturnProductTaxPrecentage' => $productTax['tP'],
                                'salesReturnProductTaxAmount' => $productTax['tA'] * $salesReturnCustomCurrencyRate
                            );
                            $returnProductTax = new ReturnsProductTax();
                            $returnProductTax->exchangeArray($returnProductTaxData);
                            $this->CommonTable('SalesReturnsProductTaxTable')->saveReturnsProductTax($returnProductTax);
                        }
                    }

                    if (!count($batchProducts) > 0) {
                        $itemReturnQty = $returnQty;
                        //add item in details for non serial and batch products
                        if ($multipleDlnCopyFlag == "false") {
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($origLocationProductID, 'Delivery Note', $request->getPost('deliveryNoteID'));
                        } else {
                            $getDeliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductID)->current();

                            // need to get original locationProductID
                            if ($locationID != $getDeliveryNoteID['deliveryNoteLocationID']) {
                                 // get original deliveryNote location Product Details
                                $origLocationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $getDeliveryNoteID['deliveryNoteLocationID']);
                                $origLocationProductID = $origLocationProduct->locationProductID;
                            }
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($origLocationProductID, 'Delivery Note', $getDeliveryNoteID['deliveryNoteID']);
                        }
                        foreach (array_reverse($itemOutDetails) as $itemOutData) {
                            if ($itemReturnQty != 0) {
                                if ($itemOutData->itemOutQty > $itemOutData->itemOutReturnQty) {
                                    $itemInPreDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetails($itemOutData->itemOutItemInID);
                                    
                                    $unitPrice = $itemInPreDetails['itemInPrice'];
                                    $unitDiscount = $itemInPreDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutData->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }
                                    
                                    $leftQty = floatval($itemOutData->itemOutQty) - floatval($itemOutData->itemOutReturnQty);
                                    $itemInInsertQty = 0;
                                    $itemOutUpdateReturnQty = 0;
                                    if ($leftQty >= $itemReturnQty) {
                                        $itemInInsertQty = $itemReturnQty;
                                        $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) + floatval($itemReturnQty);
                                        $itemReturnQty = 0;
                                    } else {
                                        $itemInInsertQty = $leftQty;
                                        $itemOutUpdateReturnQty = $itemOutData->itemOutQty;
                                        $itemReturnQty = floatval($itemReturnQty) - floatval($leftQty);                                    
                                    }
                                    //hit data to the item in table with left qty
                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInPreDetails['itemInIndexID']);
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Sales Returns',
                                        'itemInDocumentID' => $returnID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => NULL,
                                        'itemInSerialID' => NULL,
                                        'itemInQty' => $itemInInsertQty,
                                        'itemInPrice' => $unitPrice,
                                        'itemInDiscount' => $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                    //update item out return qty
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                                
                                    if($this->useAccounting == 1){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $itemInInsertQty * $itemOutData->itemOutAverageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = $itemInInsertQty * ($itemInPreDetails['itemInPrice'] - $itemInPreDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                }
                            } else {
                                break;
                            }
                        }
                        if ($itemReturnQty != 0) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_DLN_NOT_ENOUGH_QTY',array($pData['productCode'].' - '.$pData['productName']));
                            return $this->JSONRespond();
                        }
                    }

                    if (count($batchProducts) > 0) {
                        foreach ($batchProducts as $batchKey => $batchValue) {

                            if ($batchValue['serialID']) {
                                if ((float)$deliveryNoteRemainQty < count($batchProducts)) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_DLN_NOT_ENOUGH_SERIAL_ITEM_QTY',array($pData['productCode'].' - '.$pData['productName']));
                                    return $this->JSONRespond();
                                }
                            }
                            
                            $result = $this->saveReturnSubProductData($batchValue, $returnProductID);
                            if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                                if ((float)$deliveryNoteRemainQty < (float)$batchValue['qtyByBase']) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_DLN_NOT_ENOUGH_QTY',array($pData['productCode'].' - '.$pData['productName']));
                                    return $this->JSONRespond();
                                }

                                if ($crossLocationFlag) {
                                    
                                    // insert new batch record with new locationProduct ID
                                    $bPData = array(
                                        'locationProductID' => $locationProductID,
                                        'productBatchCode' => $delBatchDetails[$batchValue['batchID']]['PBC'],
                                        'productBatchExpiryDate' => ($delBatchDetails[$batchValue['batchID']]['PBExpD'] != '') ? $this->convertDateToStandardFormat($delBatchDetails[$batchValue['batchID']]['PBExpD']) : null,
                                        'productBatchWarrantyPeriod' => $delBatchDetails[$batchValue['batchID']]['PBWoD'],
                                        'productBatchManufactureDate' => ($delBatchDetails[$batchValue['batchID']]['PBManD'] != '') ? $this->convertDateToStandardFormat($delBatchDetails[$batchValue['batchID']]['PBManD']) : null,
                                        'productBatchQuantity' => $batchValue['qtyByBase'],
                                        'productBatchPrice' => $delBatchDetails[$batchValue['batchID']]['BtPrice'],
                                    );
                                    $batchProduct = new ProductBatch();
                                    $batchProduct->exchangeArray($bPData);

                                    $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                                } else {
                          
                                    $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                                    $productBatchQuentity = $batchProduct->productBatchQuantity + $batchValue['qtyByBase'];
                                    $productBatchQty = array(
                                        'productBatchQuantity' => $productBatchQuentity,
                                    );
                                    $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                                    
                                }
                                if ($batchValue['serialID']) {
                                    //Add details to item in table for batch and serial products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchSerialProductDetails($origLocationProductID, $batchValue['batchID'], $batchValue['serialID']);

                                    if ($multipleDlnCopyFlag == "false") {
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'],$batchValue['batchID'], 'Delivery Note',$request->getPost('deliveryNoteID'));
                                    } else {
                                        $getDeliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductID)->current();
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'],$batchValue['batchID'], 'Delivery Note',$getDeliveryNoteID['deliveryNoteID']);
                                    }
                                    
                                    $unitPrice = $itemInDetails['itemInPrice'];
                                    $unitDiscount = $itemInDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }

                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                    if ($crossLocationFlag) {
                                        $newItemInIndex = null;
                                    }
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Sales Returns',
                                        'itemInDocumentID' => $returnID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => $batchValue['batchID'],
                                        'itemInSerialID' => $batchValue['serialID'],
                                        'itemInQty' => 1,
                                        'itemInPrice' => $unitPrice,
                                        'itemInDiscount' => $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                    if ($crossLocationFlag) { 
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID,'1');
                                    }
                                    
                                    if($this->useAccounting == 1){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = 1 * $itemOutDetails->itemOutAverageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }

                                    ///////////////////
                                } else {
                                    //Add details to item in table for batch products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchProductDetails($origLocationProductID, $batchValue['batchID']);
                                    if ($multipleDlnCopyFlag == "false") {
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetails($batchValue['batchID'], 'Delivery Note',$request->getPost('deliveryNoteID'));
                                    } else {
                                        $getDeliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductID)->current();
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetails($batchValue['batchID'], 'Delivery Note',$getDeliveryNoteID['deliveryNoteID']);
                                    }
                                    
                                    $unitPrice = $itemInDetails['itemInPrice'];
                                    $unitDiscount = $itemInDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }

                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                    if ($crossLocationFlag) {
                                        $newItemInIndex = null;
                                    }
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Sales Returns',
                                        'itemInDocumentID' => $returnID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => $batchValue['batchID'],
                                        'itemInSerialID' => NULL,
                                        'itemInQty' => $batchValue['qtyByBase'],
                                        'itemInPrice' => $unitPrice,
                                        'itemInDiscount' =>  $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                    if ($crossLocationFlag) {
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID,$batchValue['qtyByBase']);
                                    }

                                    if($this->useAccounting == 1){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $batchValue['qtyByBase'] * $itemOutDetails->itemOutAverageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }

                                    ///////////////////
                                }
                            } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                //Add details to item in table for serial products
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getSerialProductDetails($origLocationProductID, $batchValue['serialID']);
                                if ($multipleDlnCopyFlag == "false") {
                                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Delivery Note',$request->getPost('deliveryNoteID'));
                                } else {
                                    $getDeliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductID)->current();
                                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Delivery Note',$getDeliveryNoteID['deliveryNoteID']);
                                }
                                
                                $unitPrice = $itemInDetails['itemInPrice'];
                                $unitDiscount = $itemInDetails['itemInDiscount'];
                                if($averageCostingFlag == 1){
                                    $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                    $unitDiscount = 0;
                                }

                                $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                if ($crossLocationFlag) {
                                    $newItemInIndex = null;
                                }
                                $itemInData = array(
                                    'itemInIndex' => $newItemInIndex,
                                    'itemInDocumentType' => 'Sales Returns',
                                    'itemInDocumentID' => $returnID,
                                    'itemInLocationProductID' => $locationProductID,
                                    'itemInBatchID' => NULL,
                                    'itemInSerialID' => $batchValue['serialID'],
                                    'itemInQty' => 1,
                                    'itemInPrice' => $unitPrice,
                                    'itemInDiscount' => $unitDiscount,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                if (!$crossLocationFlag) {
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID,'1');
                                }
                                
                                if($this->useAccounting == 1){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = 1 * $itemOutDetails->itemOutAverageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = 1* ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }
                                ///////////////////////////////////
                            }
                            if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                if ($crossLocationFlag) {
                                    // Update product Serial table locationProductID
                                    $serialProductData = array(
                                        'locationProductID' => $locationProductID,
                                        'productSerialSold' => '0',
                                    );

                                } else {
                                    $serialProductData = array(
                                        'productSerialSold' => '0',
                                    );
                                    
                                }
                                $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                            }
                        }
                    } else {
//                    $this->saveTransferProductData(array('batchID' => 0, 'serialID' => 0, 'qtyByBase' => $p['transferQuantity']['qtyByBase']), $trasferID, $productID);
                    }

                    $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductID);
                    $locationPrTotalQty = 0;
                    $locationPrTotalPrice = 0;
                    $newItemInIds = array();
                    if(count($itemInData) > 0){
                        foreach ($itemInData as $key => $value) {
                            if($value->itemInID > $locationProductLastItemInID){
                                $newItemInIds[] = $value->itemInID; 
                                $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                if($remainQty > 0){
                                    $itemInPrice = $remainQty*$value->itemInPrice;
                                    $itemInDiscount = $remainQty*$value->itemInDiscount;
                                    $locationPrTotalQty += $remainQty;
                                    $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                                }
                                $locationProductLastItemInID = $value->itemInID; 
                            }
                        }
                    }
                    $locationPrTotalQty += $locationProductQuantity;
                    $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                    if($locationPrTotalQty > 0){
                        $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                        $lpdata = array(
                            'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                            'locationProductLastItemInID' => $locationProductLastItemInID,
                        );
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationProductID);
                        
                        foreach ($newItemInIds as $inID) {
                            $intemInData = array(
                                'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                );
                            $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                        }
                    }
                }

                if ($multipleDlnCopyFlag == "false") {
    //                if all products are returned change the deliverynote status
                    if (trim($request->getPost('deliveryNoteID')) != "") {
                        $this->_updateDeliveryNoteStatusByDeliveryNoteID($request->getPost('deliveryNoteID'));
                    }
                } else {
                    foreach ($request->getPost('deliveryNoteID') as $key => $value) {
                        $this->_updateDeliveryNoteStatusByDeliveryNoteID($value);
                    }
                }
      
                //      call product updated event
                $productIDs = array_map(function($element) {
                    if ($multipleDlnCopyFlag == "false") {
                        return $element['productID'];
                    } else {
                        return explode("_", $element['productID'])[1];
                    }
                }, $products);

                $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
                $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

                if ($this->useAccounting == 1) {
                    //create data array for the journal entry save.
                    $i=0;
                    $journalEntryAccounts = array();

                    foreach ($accountProduct as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Sales Return '.$returnCode.'.';
                        $i++;
                    }

                    //get journal entry reference number.
                    $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
                    $jelocationReferenceID = $jeresult['locRefID'];
                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                    $journalEntryData = array(
                        'journalEntryAccounts' => $journalEntryAccounts,
                        'journalEntryDate' => $date,
                        'journalEntryCode' => $JournalEntryCode,
                        'journalEntryTypeID' => '',
                        'journalEntryIsReverse' => 0,
                        'journalEntryComment' => 'Journal Entry is posted when create Sales Return '.$returnCode.'.',
                        'documentTypeID' => 5,
                        'journalEntryDocumentID' => $returnID,
                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                    );

                    $resultData = $this->saveJournalEntry($journalEntryData);

                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($returnID,5, 4);
                    if(!$jEDocStatusUpdate['status']){
                        $this->status = false;
                        $this->msg = $jEDocStatusUpdate['msg'];
                        $this->data = $jEDocStatusUpdate['data'];
                        return $this->JSONRespond();
                    }

                    if($resultData['status']){
                        if ($multipleDlnCopyFlag == "false") {
                            $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(4,$deliveryNoteID);
                            $dimensionData = [];
                            foreach ($jEDimensionData as $value) {
                                if (!is_null($value['journalEntryID'])) {
                                    $temp = [];
                                    $temp['dimensionTypeId'] = $value['dimensionType'];
                                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                                    $dimensionData[$creditNoteCode][] = $temp;
                                }
                            }
                            if (!empty($dimensionData)) {
                                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$creditNoteCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                if(!$saveRes['status']){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $saveRes['msg'];
                                    $this->data = $saveRes['data'];
                                    return $this->JSONRespond();
                                }   
                            }
                        } else {
                            if (!empty($dimensionData)) {
                                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$returnCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                if(!$saveRes['status']){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $saveRes['msg'];
                                    $this->data = $saveRes['data'];
                                    return $this->JSONRespond();
                                }   
                            }
                        }
                        $this->commit();
                        $this->status = true;
                        $this->data = array('returnID' => $returnID);
                        $this->msg = $this->getMessage('SUCC_RETURNAPI_PRODUCTDETAIL_ADD', array($returnCode));
                        $this->setLogMessage($customCurrencySymbol." ". $totalPrice. ' amount return Product details were successfully added - ' . $returnCode);
                        $this->flashMessenger()->addMessage($this->msg);
                    }else{
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $resultData['msg'];
                        $this->data = $resultData['data'];
                    }
                }else{
                    $this->commit();
                    $this->status = true;
                    $this->data = array('returnID' => $returnID);
                    $this->msg = $this->getMessage('SUCC_RETURNAPI_PRODUCTDETAIL_ADD', array($returnCode));
                    $this->setLogMessage($customCurrencySymbol." ". $totalPrice. ' amount return Product details were successfully added - ' . $returnCode);
                    $this->flashMessenger()->addMessage($this->msg);
                }
                return $this->JSONRespond();

            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DELINOTEAPI_VALID_CUST');
                return $this->JSONRespond();
            }
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DELINOTEAPI_PRODUCT_SAVE');
            return $this->JSONRespond();
        }
    }


    /*
     * This function is used to save direct return data
     * @param JSON Request
     * @return JSON Respond
     */
    public function saveDirectReturnDetailsAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerID = $request->getPost('customerID');
            $customCurrencyId = $request->getPost('customCurrencyId');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');

            //get customCurrency data by customer CurrencyId
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;
            $salesReturnCustomCurrencyRate = (float) $customCurrencyData->currencyRate;

            $products = $request->getPost('products');
            $returnCode = $request->getPost('returnCode');
            $locationID = $request->getPost('locationID');
            $directReturnFlag = $request->getPost('directReturnFlag');
            $date = $this->convertDateToStandardFormat($request->getPost('date'));
            $totalPrice = (float) trim($request->getPost('returnTotalPrice'));
            $comment = $request->getPost('returnComment');
            $dimensionData = $request->getPost('dimensionData');

            $this->beginTransaction();

            $result = $this->getReferenceNoForLocation('7', $locationID);
            $locationReferenceID = $result['locRefID'];

            //check return numer already exist in return table if exist give next number for return code
            while ($returnCode) {
                if ($this->CommonTable('SalesReturnsTable')->checkReturnByCode($returnCode)->current()) {
                    if ($locationReferenceID) {
                        $newReturnCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newReturnCode == $returnCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $returnCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $returnCode = $newReturnCode;
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_RETURNAPI_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }

            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            if(empty($FIFOCostingFlag) && empty($averageCostingFlag)){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE');
                return $this->JSONRespond();
            }
                
            $entityID = $this->createEntity();
            $returnData = array(
                'salesReturnCode' => $returnCode,
                'customerID' => $customerID,
                'locationID' => $locationID,
                'salesReturnDate' => $date,
                'salesReturnTotal' => $totalPrice,
                'salesReturnComment' => $comment,
                'statusID' => 4,
                'customCurrencyId' => $customCurrencyId,
                'salesReturnCustomCurrencyRate' => $salesReturnCustomCurrencyRate,
                'entityID' => $entityID,
                'directReturnFlag' => 1
            );

            $return = new Returns;
            $return->exchangeArray($returnData);

            $returnID = $this->CommonTable('SalesReturnsTable')->saveSalesReturn($return);

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }
            $allProductTotal = 0;
            foreach ($products as $product) {
                $locationPID = $product['locationPID'];
                //get location product data for update average costing.
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;
             
                $productBaseQty = $product['pQuantity'];
                $productType = $product['productType'];
                $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);
                $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                $allProductTotal+=$productTotal;

                //If product is a batch product
                if (array_key_exists('bProducts', $product)) {
                    $batchProductCount = 0;
                    $batchSerialProductCount = 0;
                    foreach ($product['bProducts'] as $bProduct) {

                        $batchProductCount++;
                        $bPData = array(
                            'locationProductID' => $locationPID,
                            'productBatchCode' => $bProduct['bCode'],
                            'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                            'productBatchWarrantyPeriod' => $bProduct['warnty'],
                            'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                            'productBatchQuantity' => $bProduct['bQty'] * $productConversionRate
                        );

                        $batchProduct = new ProductBatch();
                        $batchProduct->exchangeArray($bPData);
                        $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                        if(!$insertedBID){
                            $this->rollBack();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                            return $this->JSONRespond();
                        }
                        //if product is a batch and serial product
                        if (array_key_exists('sProducts', $product)) {
                            foreach ($product['sProducts'] as $sProduct) {
                                $batchSerialProductCount++;
                                if (isset($sProduct['sBCode'])) {

                                    if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                        $sPData = array(
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialCode' => $sProduct['sCode'],
                                            'productSerialExpireDate' => $this->convertDateToStandardFormat($sProduct['sEdate']),
                                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                            'productSerialWarrantyPeriodType' => $sProduct['sWarrantyType'],
                                        );
                                        $serialPr = new ProductSerial();
                                        $serialPr->exchangeArray($sPData);
                                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                        if(!$insertedSID){
                                            $this->rollBack();
                                            $this->status = false;
                                            $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                                            return $this->JSONRespond();
                                        }
                                        
                                        if ($batchSerialProductCount == 1) {
                                            $returnProductData = array(
                                                'salesReturnID' => $returnID,
                                                'deliveryNoteProductID' => 0,
                                                'productID' => $product['pID'],
                                                'salesReturnProductPrice' => $product['pUnitPrice'],
                                                'salesReturnProductDiscount' => $product['pDiscount'],
                                                'salesReturnProductDiscountType' => $product['discountType'],
                                                'salesReturnProductTotal' => $product['pTotal'],
                                                'salesReturnProductQuantity' => $product['pQuantity']
                                            );
                                            $returnProdut = new ReturnsProduct;
                                            $returnProdut->exchangeArray($returnProductData);
                                            $returnProductID = $this->CommonTable('SalesReturnsProductTable')->saveReturnProducts($returnProdut);
                                            if(!$returnProductID){
                                                $this->rollBack();
                                                $this->status = false;
                                                $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                                                return $this->JSONRespond();
                                            }
                                        }
                                        //save item in details
                                        $itemInData = array(
                                            'itemInDocumentType' => 'Direct Return',
                                            'itemInDocumentID' => $returnID,
                                            'itemInLocationProductID' => $locationPID,
                                            'itemInBatchID' => $insertedBID,
                                            'itemInSerialID' => $insertedSID,
                                            'itemInQty' => 1,
                                            'itemInPrice' => $product['pUnitPrice'],
                                            'itemInDiscount' => $discountValue,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                        if(!$itemInID){
                                            $this->rollBack();
                                            $this->status = false;
                                            $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                                            return $this->JSONRespond();
                                        }

                                        $data = array(
                                            'salesReturnProductID' => $returnProductID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialID' => $insertedSID,
                                            'salesReturnSubProductQuantity' => 1,
                                        );
                                        $returnSubProduct = new ReturnsSubProduct;
                                        $returnSubProduct->exchangeArray($data);
                                        $returnSubProductID = $this->CommonTable('SalesReturnsSubProductTable')->saveReturnSubProduct($returnSubProduct);

                                        if (array_key_exists('pTax', $product)) {
                                            if (array_key_exists('tL', $product['pTax'])) {
                                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                    $returnProductTaxData = array(
                                                        'salesReturnProductID' => $returnProductID,
                                                        'taxID' => $taxKey,
                                                        'salesReturnProductTaxPrecentage' => $productTax['tP'],
                                                        'salesReturnProductTaxAmount' => $productTax['tA']
                                                    );
                                                    $returnProductTax = new ReturnsProductTax();
                                                    $returnProductTax->exchangeArray($returnProductTaxData);
                                                    $this->CommonTable('SalesReturnsProductTaxTable')->saveReturnsProductTax($returnProductTax);
                                                }
                                            }
                                        }
                                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                        $newPQtyData = array(
                                            'locationProductQuantity' => $newPQty
                                        );
                                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                        if(!$status){
                                            $this->rollBack();
                                            $this->status = false;
                                            $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                                            return $this->JSONRespond();
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($batchProductCount == 1) {
                                $returnProductData = array(
                                    'salesReturnID' => $returnID,
                                    'deliveryNoteProductID' => 0,
                                    'productID' => $product['pID'],
                                    'salesReturnProductPrice' => $product['pUnitPrice'],
                                    'salesReturnProductDiscount' => $product['pDiscount'],
                                    'salesReturnProductDiscountType' => $product['discountType'],
                                    'salesReturnProductTotal' => $product['pTotal'],
                                    'salesReturnProductQuantity' => $product['pQuantity']
                                );
                                $returnProdut = new ReturnsProduct;
                                $returnProdut->exchangeArray($returnProductData);
                                $returnProductID = $this->CommonTable('SalesReturnsProductTable')->saveReturnProducts($returnProdut);
                                if(!$returnProductID){
                                    $this->rollBack();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                                    return $this->JSONRespond();
                                }
                            }
                            //save item in details
                            $itemInData = array(
                                'itemInDocumentType' => 'Direct Return',
                                'itemInDocumentID' => $returnID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => $insertedBID,
                                'itemInSerialID' => NULL,
                                'itemInQty' => $bProduct['bQty'] * $productConversionRate,
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if(!$itemInID){
                                $this->rollBack();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                                return $this->JSONRespond();
                            }

                            $data = array(
                                'salesReturnProductID' => $returnProductID,
                                'productBatchID' => $insertedBID,
                                'productSerialID' => NULL,
                                'salesReturnSubProductQuantity' => $bProduct['bQty'],
                            );
                            $returnSubProduct = new ReturnsSubProduct;
                            $returnSubProduct->exchangeArray($data);
                            $returnSubProductID = $this->CommonTable('SalesReturnsSubProductTable')->saveReturnSubProduct($returnSubProduct);
                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $returnProductTaxData = array(
                                            'salesReturnProductID' => $returnProductID,
                                            'taxID' => $taxKey,
                                            'salesReturnProductTaxPrecentage' => $productTax['tP'],
                                            'salesReturnProductTaxAmount' => $productTax['tA']
                                        );
                                        $returnProductTax = new ReturnsProductTax();
                                        $returnProductTax->exchangeArray($returnProductTaxData);
                                        $this->CommonTable('SalesReturnsProductTaxTable')->saveReturnsProductTax($returnProductTax);
                                    }
                                }
                            }
                            if ($batchProductCount == 1) {
                                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                                $newPQtyData = array(
                                    'locationProductQuantity' => $newPQty
                                );
                                $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                if(!$status){
                                    $this->rollBack();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                                    return $this->JSONRespond();
                                }
                            }
                        }
                    }
                } elseif (array_key_exists('sProducts', $product)) {
                    $serialProductCount = 0;
                    //If the product is a serial product
                    foreach ($product['sProducts'] as $sProduct) {
                        $serialProductCount++;
                        $sPData = array('locationProductID' => $locationPID,
                            'productSerialCode' => $sProduct['sCode'],
                            'productSerialExpireDate' => $sProduct['sEdate'],
                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                            'productSerialWarrantyPeriodType' => $sProduct['sWarrantyType'],
                        );
                        $serialPr = new ProductSerial();
                        $serialPr->exchangeArray($sPData);
                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                        if(!$insertedSID){
                            $this->rollBack();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                            return $this->JSONRespond();
                        }
                        if ($serialProductCount == 1) {
                            $returnProductData = array(
                                'salesReturnID' => $returnID,
                                'deliveryNoteProductID' => 0,
                                'productID' => $product['pID'],
                                'salesReturnProductPrice' => $product['pUnitPrice'],
                                'salesReturnProductDiscount' => $product['pDiscount'],
                                'salesReturnProductDiscountType' => $product['discountType'],
                                'salesReturnProductTotal' => $product['pTotal'],
                                'salesReturnProductQuantity' => $product['pQuantity']
                            );
                            $returnProdut = new ReturnsProduct;
                            $returnProdut->exchangeArray($returnProductData);
                            $returnProductID = $this->CommonTable('SalesReturnsProductTable')->saveReturnProducts($returnProdut);
                            if(!$returnProductID){
                                $this->rollBack();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                                return $this->JSONRespond();
                            }
                        }
                        //save item in details
                        $itemInData = array(
                            'itemInDocumentType' => 'Direct Return',
                            'itemInDocumentID' => $returnID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => $insertedSID,
                            'itemInQty' => 1,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            $this->rollBack();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                            return $this->JSONRespond();
                        }

                        $data = array(
                            'salesReturnProductID' => $returnProductID,
                            'productBatchID' => NULL,
                            'productSerialID' => $insertedSID,
                            'salesReturnSubProductQuantity' => 1,
                        );
                        $returnSubProduct = new ReturnsSubProduct;
                        $returnSubProduct->exchangeArray($data);
                        $returnSubProductID = $this->CommonTable('SalesReturnsSubProductTable')->saveReturnSubProduct($returnSubProduct);
                        if ($serialProductCount == 1) {
                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $returnProductTaxData = array(
                                            'salesReturnProductID' => $returnProductID,
                                            'taxID' => $taxKey,
                                            'salesReturnProductTaxPrecentage' => $productTax['tP'],
                                            'salesReturnProductTaxAmount' => $productTax['tA']
                                        );
                                        $returnProductTax = new ReturnsProductTax();
                                        $returnProductTax->exchangeArray($returnProductTaxData);
                                        $this->CommonTable('SalesReturnsProductTaxTable')->saveReturnsProductTax($returnProductTax);
                                    }
                                }
                            }
                        }
                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                        );
                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        if(!$status){
                            $this->rollBack();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                            return $this->JSONRespond();
                        }
                    }
                } else {
                    // if the product is non serial and non batch
                    $returnProductData = array(
                        'salesReturnID' => $returnID,
                        'deliveryNoteProductID' => 0,
                        'productID' => $product['pID'],
                        'salesReturnProductPrice' => $product['pUnitPrice'],
                        'salesReturnProductDiscount' => $product['pDiscount'],
                        'salesReturnProductDiscountType' => $product['discountType'],
                        'salesReturnProductTotal' => $product['pTotal'],
                        'salesReturnProductQuantity' => $product['pQuantity']
                    );
                    $returnProdut = new ReturnsProduct;
                    $returnProdut->exchangeArray($returnProductData);
                    $returnProductID = $this->CommonTable('SalesReturnsProductTable')->saveReturnProducts($returnProdut);
                    if(!$returnProductID){
                        $this->rollBack();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                        return $this->JSONRespond();
                    }

                    if ($productType != 2) {
                        $itemInData = array(
                            'itemInDocumentType' => 'Direct Return',
                            'itemInDocumentID' => $returnID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => NULL,
                            'itemInQty' => $productBaseQty,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            $this->rollBack();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                            return $this->JSONRespond();
                        }
                    }
                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $returnProductTaxData = array(
                                    'salesReturnProductID' => $returnProductID,
                                    'taxID' => $taxKey,
                                    'salesReturnProductTaxPrecentage' => $productTax['tP'],
                                    'salesReturnProductTaxAmount' => $productTax['tA']
                                );
                                $returnProductTax = new ReturnsProductTax();
                                $returnProductTax->exchangeArray($returnProductTaxData);
                                $this->CommonTable('SalesReturnsProductTaxTable')->saveReturnsProductTax($returnProductTax);
                            }
                        }
                    }
                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    if ($productType == 2) {
                        $productBaseQty = 0;
                    }

                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                    if(!$status){
                        $this->rollBack();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PURINV_CREATE');
                        return $this->JSONRespond();
                    }
                }
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
                $locationPrTotalQty = 0;
                $locationPrTotalPrice = 0;
                $newItemInIds = array();
                if(count($itemInData) > 0){
                    foreach ($itemInData as $key => $value) {
                        if($value->itemInID > $locationProductLastItemInID){
                            $newItemInIds[] = $value->itemInID;
                            $remainQty = $value->itemInQty - $value->itemInSoldQty;
                            if($remainQty > 0){
                                $itemInPrice = $remainQty*$value->itemInPrice;
                                $itemInDiscount = $remainQty*$value->itemInDiscount;
                                $locationPrTotalQty += $remainQty;
                                $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                            }
                            $locationProductLastItemInID = $value->itemInID;
                        }
                    }
                }
                $locationPrTotalQty += $locationProductQuantity;
                $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                if($locationPrTotalQty > 0){
                    $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                    $lpdata = array(
                        'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                        'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                    foreach ($newItemInIds as $inID) {
                        $intemInData = array(
                            'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                            );
                        $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                    }
                }
            }
            //call product updated event
            $productIDs = array_map(function($element) {
                return $element['pID'];
            }, $products);

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            $jeResult = $this->saveJournalEntryDataForReturn($returnID, $returnCode, $customerID, $products, $allProductTotal, $date, $averageCostingFlag, $salesReturnCustomCurrencyRate, $dimensionData, $ignoreBudgetLimit);

            if($jeResult['status']){
                $this->commit();
                $this->setLogMessage($customCurrencySymbol ."".$totalPrice. ' amount direct return Product details were successfully added - ' . $returnCode);
                $this->flashMessenger()->addMessage($jeResult['msg']);
            }else{
                $this->rollback();
            }

            $this->status = $jeResult['status'];
            $this->data = $jeResult['data'];
            $this->msg = $jeResult['msg'];
            return $this->JSONRespond();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DELINOTEAPI_PRODUCT_SAVE');
            return $this->JSONRespond();
        }
    }

    /**
    * This fuction is used to save journal entry for the return
    **/
    public function saveJournalEntryDataForReturn($returnID, $returnCode, $customerID, $productsData,$allProductTotal, $date, $averageCostingFlag, $salesReturnCustomCurrencyRate, $dimensionData, $ignoreBudgetLimit)
    {
        $accountProduct = array();
        if ($this->useAccounting == 1) {
            foreach ($productsData as $key => $product) {
                $locationPID = $product['locationPID'];
                $productID = $product['pID'];
                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);
                if(empty($pData['productInventoryAccountID'])){
                    return array('status' => false, 'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])));
                }
                 //check whether Product Cost of Goods Sold Gl account id set or not
                if(empty($pData['productCOGSAccountID'])){
                    return array('status' => false, 'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName'])));
                }

                //calculate discount value
                $discountValue = 0;
                if ($product['discountType'] == "precentage") {
                    $discountValue = ($product['pUnitPrice']  * $product['pDiscount']/ 100);
                } else if ($p['discountType'] == "value") {
                    $discountValue = $product['pDiscount'];
                }


                if($averageCostingFlag == 1){
                    $locationProDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);

                    //set gl accounts for the journal entry
                    $productTotal = $product['pQuantity'] * $locationProDetails->locationProductAverageCostingPrice;
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                    }

                    if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                    }
                }else{
                    $productTotal = $product['pQuantity'] * ($product['pUnitPrice'] - $product['discountValue']);
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                    }

                    if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                    }
                }

            }


            $i=0;
            $journalEntryAccounts = array();

            foreach ($accountProduct as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Direct Sales Return '.$returnCode.'.';
                $i++;
            }

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $date,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Direct Sales Return '.$returnCode.'.',
                'documentTypeID' => 5,
                'journalEntryDocumentID' => $returnID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

            $resultData = $this->saveJournalEntry($journalEntryData);

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($returnID,5, 4);
            if(!$jEDocStatusUpdate['status']){
                return array(
                    'status'=> false,
                    'msg'=>$jEDocStatusUpdate['msg'],
                    'data'=>$jEDocStatusUpdate['data'],
                    );
            } 

            if($resultData['status']){
                if (!empty($dimensionData)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$returnCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                    if(!$saveRes['status']){
                        return array(
                            'status'=> false,
                            'msg'=>$saveRes['msg'],
                            'data'=>$saveRes['data'],
                            );
                    }   
                }

                $status = true;
                $data = array('returnID' => $returnID);
                $msg = $this->getMessage('SUCC_RETURNAPI_PRODUCTDETAIL_ADD', array($returnCode));
            }else{
                $status = false;
                $msg = $resultData['msg'];
            }
        } else {
            $status = true;
            $data = array('returnID' => $returnID);
            $msg = $this->getMessage('SUCC_RETURNAPI_PRODUCTDETAIL_ADD', array($returnCode));
        }
        return array('status' => $status, 'msg' => $msg, 'data' => $data);
    }
    function saveReturnSubProductData($batchValue, $returnProductID)
    {
        $data = array(
            'salesReturnProductID' => $returnProductID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'salesReturnSubProductQuantity' => $batchValue['qtyByBase'],
        );
        $returnSubProduct = new ReturnsSubProduct;
        $returnSubProduct->exchangeArray($data);
        $returnSubProductID = $this->CommonTable('SalesReturnsSubProductTable')->saveReturnSubProduct($returnSubProduct);
        return $returnSubProductID;
    }

    public function getReturnsFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($searchrequest->isPost()) {
            $returnID = $searchrequest->getPost('ReturnID');
            $returns = $this->CommonTable('SalesReturnsTable')->getReturnforSearch($returnID);
            foreach ($returns as $t) {
                $returnArray[$t['salesReturnID']] = (object) $t;
            }
            if ($returns->count() == 0) {
                return new JsonModel(array(FALSE));
            } else {
                $return = new ViewModel(
                        array('return' => $returnArray,
                    'statuses' => $this->getStatusesList(),
                    'dateFormat' => $dateFormat,
                    'companyCurrencySymbol' => $this->companyCurrencySymbol));
                $return->setTerminal(TRUE);
                $return->setTemplate('invoice/return/returnList');
                return $return;
            }
        }
    }

    public function retriveCustomerReturnAction()
    {
        $er = array();
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $cust_id = $request->getPost('customerID');
            $returns = $this->CommonTable('SalesReturnsTable')->getReturnsByCustomerID($cust_id, $locationID);

            foreach ($returns as $t) {
                $returnArray[$t['salesReturnID']] = (object) $t;
            }
            $customerReturn = new ViewModel(
                    array('return' => $returnArray,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,));
            $customerReturn->setTerminal(TRUE);
            $customerReturn->setTemplate('invoice/return/returnList');
            return $customerReturn;
        }
    }

    public function getReturnsByDatefilterAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
            $fromdate = $this->convertDateToStandardFormat($request->getPost('fromdate'));
            $todate = $this->convertDateToStandardFormat($request->getPost('todate'));
            $customerID = $request->getPost('customerID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $filteredReturns = $this->CommonTable('SalesReturnsTable')->getReturnsByDate($fromdate, $todate, $customerID, $locationID);

            foreach ($filteredReturns as $t) {
                $returnsArray[$t['salesReturnID']] = (object) $t;
            }
            $DateFilteredReturn = new ViewModel(
                    array('return' => $returnsArray,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,));
            $DateFilteredReturn->setTerminal(TRUE);
            $DateFilteredReturn->setTemplate('invoice/return/returnList');
            return $DateFilteredReturn;
        }
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Sales Returns';
        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_RETURNAPI_SENT_EMAIL'); //TODO : create messages according to module
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_RETURNAPI_SENT_EMAIL');
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search Sales Returns For Dropdown
     * @return viewModel
     */
    public function searchSalesReturnsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $salesReturnSearchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $salesReturns = $this->CommonTable('SalesReturnsTable')->searchSalesReturnForDropdown($locationID, $salesReturnSearchKey);

            $salesReturnList = array();
            foreach ($salesReturns as $salesReturn) {
                $temp['value'] = $salesReturn['salesReturnID'];
                $temp['text'] = $salesReturn['salesReturnCode'];
                $salesReturnList[] = $temp;
            }

            $this->data = array('list' => $salesReturnList);
            return $this->JSONRespond();
        }
    }

    private function _updateDeliveryNoteStatusByDeliveryNoteID($deliveryNoteID)
    {
        $checkProduct = $this->_checkDeliveryNoteProductsByDeliveryNoteID($deliveryNoteID);
        if ($checkProduct == "0") {
//            update status
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNoteStatus(
                    $deliveryNoteID, $closeStatusID);
        }
    }

    private function _checkDeliveryNoteProductsByDeliveryNoteID($deliveryNoteID){

        $deliveryProductsArray = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductByDeliveryNoteID($deliveryNoteID, true);

        $dparray;
        $dparrayQty;
        $iparray;
        $iparrayQty;
        $checkProduct = 0;

        foreach ($deliveryProductsArray as $dp) {
            $dp = (object) $dp;
            $dparray[$dp->productID] = $dp->productID;
            $dparrayQty[$dp->productID] = $dp->deliveryNoteProductQuantity;
        }
        $data = $this->CommonTable('SalesReturnsProductTable')->getSalesReturnProductsByDelvieryNoteID($deliveryNoteID);

        foreach ($data as $ip) {
            $ip = (object) $ip;
            $iparray[$ip->productID] = $ip->productID;
            $iparrayQty[$ip->productID]+=$ip->salesReturnProductQuantity;
        }

        foreach ($dparrayQty as $key => $Pdata) {
            if ($iparrayQty[$key] != $Pdata) {
                $checkProduct = 1;
            }
        }

        return $checkProduct;
    }

    public function getAllRelatedDocumentDetailsByReturnIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $returnID = $request->getPost('returnID');
            $salesReturnData = $this->CommonTable('SalesReturnsTable')->getReturnBasicDetailsByReturnId($returnID);
            $deliveryNoteData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getReturnRelatedDeliveryNoteDataByReturnId($returnID);
            $dlnRelatedSalesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getDlnRelatedSalesOrderDataBydlnId(null, null, $returnID);
            $soRelatedQuotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getSalesOrderRelatedQuotationDataBySOId(null, null, $returnID);
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getDeliveryNoteRelatedSalesInvoiceDataByQuotationId(null, null ,null, $returnID);
            $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getDeliveryNoteRelatedInvoicePaymentsDataByQuotationId(null, null, null, null, $returnID);
            $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDeliveryNoteRelatedCreditNoteDataByQuotationId(null, null, null, null, $returnID);
            $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getDeliveryNoteRelatedCreditNotePaymentDataByQuotationId(null, null, null, null, $returnID);
            

            $dataExistsFlag = false;
            if ($salesReturnData) {
                $salesReturnData = array(
                    'type' => 'SalesReturn',
                    'documentID' => $salesReturnData['salesReturnID'],
                    'code' => $salesReturnData['salesReturnCode'],
                    'amount' => number_format($salesReturnData['salesReturnTotal'], 2),
                    'issuedDate' => $salesReturnData['salesReturnDate'],
                    'created' => $salesReturnData['createdTimeStamp'],
                );
                $returnDetails[] = $salesReturnData;
                $dataExistsFlag = true;
            }
            
            if (isset($deliveryNoteData)) {
                foreach ($deliveryNoteData as $dlnDta) {
                    $dlnData = array(
                        'type' => 'DeliveryNote',
                        'documentID' => $dlnDta['deliveryNoteID'],
                        'code' => $dlnDta['deliveryNoteCode'],
                        'amount' => number_format($dlnDta['deliveryNotePriceTotal'], 2),
                        'issuedDate' => $dlnDta['deliveryNoteDeliveryDate'],
                        'created' => $dlnDta['createdTimeStamp'],
                    );
                    $returnDetails[] = $dlnData;
                    if (isset($dlnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedSalesOrderData)) {
                foreach ($dlnRelatedSalesOrderData as $soDta) {
                    $soData = array(
                        'type' => 'SalesOrder',
                        'documentID' => $soDta['soID'],
                        'code' => $soDta['soCode'],
                        'amount' => number_format($soDta['totalAmount'], 2),
                        'issuedDate' => $soDta['issuedDate'],
                        'created' => $soDta['createdTimeStamp'],
                    );
                    $returnDetails[] = $soData;
                    if (isset($soDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($soRelatedQuotaionData)) {
                foreach ($soRelatedQuotaionData as $qtnDta) {
                    $qtnDeta = array(
                        'type' => 'Quotation',
                        'documentID' => $qtnDta['quotationID'],
                        'code' => $qtnDta['quotationCode'],
                        'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                        'issuedDate' => $qtnDta['quotationIssuedDate'],
                        'created' => $qtnDta['createdTimeStamp'],
                    );
                    $returnDetails[] = $qtnDeta;
                    if (isset($qtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($invoiceData)) {
                foreach ($invoiceData as $invDta) {
                    $invDeta = array(
                        'type' => 'SalesInvoice',
                        'documentID' => $invDta['salesInvoiceID'],
                        'code' => $invDta['salesInvoiceCode'],
                        'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                        'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                        'created' => $invDta['createdTimeStamp'],
                    );
                    $returnDetails[] = $invDeta;
                    if (isset($invDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
             
            if (isset($paymentData)) {
                foreach ($paymentData as $payDta) {
                    $paymentData = array(
                        'type' => 'CustomerPayment',
                        'documentID' => $payDta['incomingPaymentID'],
                        'code' => $payDta['incomingPaymentCode'],
                        'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                        'issuedDate' => $payDta['incomingPaymentDate'],
                        'created' => $payDta['createdTimeStamp'],
                    );
                    $returnDetails[] = $paymentData;
                    if (isset($payDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($creditNoteData)) {
                foreach ($creditNoteData as $cNDta) {
                    $creditNoteData = array(
                        'type' => 'CreditNote',
                        'documentID' => $cNDta['creditNoteID'],
                        'code' => $cNDta['creditNoteCode'],
                        'amount' => number_format($cNDta['creditNoteTotal'], 2),
                        'issuedDate' => $cNDta['creditNoteDate'],
                        'created' => $cNDta['createdTimeStamp'],
                    );
                    $returnDetails[] = $creditNoteData;
                    if (isset($cNDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($creditNotePaymentData)) {
                foreach ($creditNotePaymentData as $cNPDta) {
                    $creditNotePaymentData = array(
                        'type' => 'CreditNotePayment',
                        'documentID' => $cNPDta['creditNotePaymentID'],
                        'code' => $cNPDta['creditNotePaymentCode'],
                        'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                        'issuedDate' => $cNPDta['creditNotePaymentDate'],
                        'created' => $cNPDta['createdTimeStamp'],
                    );
                    $returnDetails[] = $creditNotePaymentData;
                    if (isset($cNPDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            $sortData = Array();
            foreach ($returnDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $returnDetails);

            $documentDetails = array(
                'returnDetails' => $returnDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }
}

/////////////////// END OF CUSTOMER API CONTROLLER \\\\\\\\\\\\\\\\\\\\