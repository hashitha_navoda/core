<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * this contains company api controller unit testing codes
 */

namespace InvoiceTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\Container;

class CompanyAPIControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
                array_merge(
                        include DOC_ROOT . 'config/test.application.config.php', include DOC_ROOT . 'config/autoload/local.test.php'
                )
        );
        parent::setUp();
    }

    public function mockUserLogin()
    {
        $userSessions = new Container('ezBizUser');
        $userSessions->userID = 1;
        $userSessions->roleID = 1;
        $userSessions->timeZone = 'Asia/Colombo';
        $locationArray = array(
            'locationName' => "kirulapana",
            'roleID' => 1,
            'locationID' => 53
        );
        $userSessions->userActiveLocation = $locationArray;
    }

    //test whether company index action can be accessed
    public function testIndexActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $companyTableMock = $this->getMockBuilder('Invoice\Model\CompanyTable')
                ->disableOriginalConstructor()
                ->getMock();

        $companyTableMock->expects($this->once())
                ->method('saveCompany')
                ->will($this->returnValue(1));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CompanyTable', $companyTableMock);


        $postData = array(
            'id' => 1,
            'companyName' => 'ThinkCube Systems Pvt Ltd',
            'companyAddress' => 'No 19 rajagiriya gardens Nawala road Rajagiriya',
            'postalCode' => '43434',
            'telephoneNumber' => '0112908347',
            'email' => 'info@thinkcube.com',
            'country' => 'Sri Lanka',
            'website' => 'www.thinkcube.com',
            'brNumber' => '3334'
        );

        $this->dispatch('/companyAPI/index', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CompanyAPI');
        $this->assertControllerClass('CompanyAPIController');
        $this->assertMatchedRouteName('companyAPI');
    }

    //test whether company display add action can be accessed
    public function testDisplayAddActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $displaySetupTableMock = $this->getMockBuilder('Core\Model\DisplaySetupTable')
                ->disableOriginalConstructor()
                ->getMock();

        $displaySetupTableMock->expects($this->once())
                ->method('saveDisplaySetup')
                ->will($this->returnValue(1));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Core\Model\DisplaySetupTable', $displaySetupTableMock);

        $resultSetMock = $this->getMockBuilder('Zend\Db\Adapter\Driver\Pdo\Result')
                ->disableOriginalConstructor()
                ->getMock();

        $resultSetMock->expects($this->once())
                ->method('current')
                ->will($this->returnValue(array('currencySymbol' => '$', 'timeZone' => 'Asia/Colombo')));

        $serviceManager->setService('Zend\Db\Adapter\Driver\Pdo\Result', $resultSetMock);

        $displaySetupTableMock->expects($this->once())
                ->method('fetchAllDetails')
                ->will($this->returnValue($resultSetMock));
        $postData = array(
            'displaySetupID' => 1,
            'currencyID' => 1,
            'timeZone' => 'Asia/Colombo',
            //'dateFormat' => 'DD-MM-YYYY',
            'timeFormat' => 12
        );

        $this->dispatch('/companyAPI/displayAdd', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CompanyAPI');
        $this->assertControllerClass('CompanyAPIController');
        $this->assertMatchedRouteName('companyAPI');
    }

    //test whether prefixUpdate action can be accessed with updated data
    public function testPrefixUpdateActionCanBeAccessedWithUpdatedData()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $data = array(
            "id" => "5",
            "locationID" => 1,
            "referenceName" => "Advance Payment",
            "type" => "ADPAYN",
            "digits" => "4",
            "nextNumber" => "3"
        );

        $dataobject = (object) array('locationReferenceID' => 1, 'referenceID' => 1);
        $datastringfy = json_encode($data);
        $dataArray = array('1' => $datastringfy);
        $postData = array(
            'prefixData' => $dataArray
        );
        $referenceTableMock = $this->getMockBuilder('Settings\Model\ReferenceTable')
                ->disableOriginalConstructor()
                ->getMock();

        $referenceTableMock->expects($this->once())
                ->method('getReference')
                ->with()
                ->will($this->returnValue($dataobject));

        $referenceTableMock->expects($this->once())
                ->method('updateReferenceByID')
                ->with()
                ->will($this->returnValue(true));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Settings\Model\ReferenceTable', $referenceTableMock);

        $this->dispatch('/companyAPI/prefixUpdate', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CompanyAPI');
        $this->assertControllerClass('CompanyAPIController');
        $this->assertMatchedRouteName('companyAPI');
    }

    //test whether prefixUpdate action can be accessed with save data
    public function testPrefixUpdateActionCanBeAccessedWithSaveData()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $data = array(
            "id" => "5",
            "locationID" => 1,
            "referenceName" => "Advance Payment",
            "type" => "ADPAYN",
            "digits" => "4",
            "nextNumber" => "3"
        );

        $dataobject = null;
        $datastringfy = json_encode($data);
        $dataArray = array('1' => $datastringfy);
        $postData = array(
            'prefixData' => $dataArray
        );
        $referenceTableMock = $this->getMockBuilder('Settings\Model\ReferenceTable')
                ->disableOriginalConstructor()
                ->getMock();

        $referenceTableMock->expects($this->once())
                ->method('getReference')
                ->with()
                ->will($this->returnValue($dataobject));

        $referenceTableMock->expects($this->once())
                ->method('saveReference')
                ->with()
                ->will($this->returnValue(true));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Settings\Model\ReferenceTable', $referenceTableMock);

        $this->dispatch('/companyAPI/prefixUpdate', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CompanyAPI');
        $this->assertControllerClass('CompanyAPIController');
        $this->assertMatchedRouteName('companyAPI');
    }

    //test whether getLocationPrefixData action can be accessed
    public function testGetLocationPrefixDataActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $postData = array(
            'referenceID' => 1
        );

        $this->dispatch('/companyAPI/getLocationPrefixData', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CompanyAPI');
        $this->assertControllerClass('CompanyAPIController');
        $this->assertMatchedRouteName('companyAPI');
    }

    //test whether locationRelatedPrefix action can be accessed
    public function testLocationRelatedPrefixActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $postData = array(
            'locationID' => 53
        );

        $this->dispatch('/companyAPI/locationRelatedPrefix', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CompanyAPI');
        $this->assertControllerClass('CompanyAPIController');
        $this->assertMatchedRouteName('companyAPI');
    }

//    //test whether Preview action can be accessed
//    public function testPreviewActionCanBeAccessed()
//    {
//        //set usserSession variables
//        $this->mockUserLogin();
//
//        $postData = array(
//            'companyID' => 1
//        );
//        $_POST['companyID'] = 1;
//
//        $_SERVER['HTTP_HOST'] = $this->getApplicationConfig()['servername'];
//        $this->dispatch('/companyAPI/preview', 'POST', $postData);
//        $this->assertResponseStatusCode(200);
//        $this->assertModuleName('Invoice');
//        $this->assertControllerName('Invoice\Controller\CompanyAPI');
//        $this->assertControllerClass('CompanyAPIController');
//        $this->assertMatchedRouteName('companyAPI');
//    }
}
