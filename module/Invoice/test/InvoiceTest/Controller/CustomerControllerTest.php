<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * this contains customer controller unit testing codes
 */

namespace InvoiceTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\Container;

class CustomerControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;


    public function setUp()
    {
        $this->setApplicationConfig(
                include DOC_ROOT . 'config/test.application.config.php'
        );

        parent::setUp();

    }


    public function mockUserLogin()
    {
        $userSessions = new Container('ezBizUser');
        $userSessions->userID = 1;
        $userSessions->roleID = 1;
        $userSessions->timeZone = 'Asia/Colombo';
        $locationArray = array(
            'locationName' => "kirulapana",
            'roleID' => 1,
            'locationID' => 53
        );
        $userSessions->userActiveLocation = $locationArray;

    }


    //test wheather customer index action can be accessed with variable search true
    public function testIndexActionCanBeAccessedWithVariableSearchTrue()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $postData = array(
            'search' => true,
        );
        $this->dispatch('/customer/index', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\Customer');
        $this->assertControllerClass('CustomerController');
        $this->assertMatchedRouteName('customer');

    }


    //test wheather customer index action can be accessed with variable search false
    public function testIndexActionCanBeAccessedWithVariableSearchFalse()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $postData = array(
            'search' => false,
        );
        $this->dispatch('/customer/index', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\Customer');
        $this->assertControllerClass('CustomerController');
        $this->assertMatchedRouteName('customer');

    }


//    test wheather customer add action can be accessed
    // test wheather customer add action can be accessed
    public function testAddActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $this->dispatch('/customer/add');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\Customer');
        $this->assertControllerClass('CustomerController');
        $this->assertMatchedRouteName('customer');

    }


    // test wheather customer import action can be accessed
    public function testImportActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $this->dispatch('/customer/import');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\Customer');
        $this->assertControllerClass('CustomerController');
        $this->assertMatchedRouteName('customer');

    }


    // test wheather customer import action can be accessed with post header true
//    public function testImportActionCanBeAccessedWithPostHeaderTrue()
//    {
//        //set usserSession variables
//        $this->mockUserLogin();
//        $postData = array(
//            'delimiter' => 0,
//            'header' => 1,
//            'characterencoding' => 0
//        );
////        $_FILES["fileupload"]["tmp_name"] = DOC_ROOT . 'data/testFiles/customer.csv';
//        $this->dispatch('/customer/import', 'POST', $postData);
//        $this->assertResponseStatusCode(200);
//        $this->assertModuleName('Invoice');
//        $this->assertControllerName('Invoice\Controller\Customer');
//        $this->assertControllerClass('CustomerController');
//        $this->assertMatchedRouteName('customer');
//    }
    // test wheather customer import action can be accessed with post header False
//    public function testImportActionCanBeAccessedWithPostHeaderFalse()
//    {
//        //set usserSession variables
//        $this->mockUserLogin();
//        $postData = array(
//            'delimiter' => 0,
//            'header' => 0,
//            'characterencoding' => 0
//        );
//        $_FILES["fileupload"]["tmp_name"] = DOC_ROOT . 'data/testFiles/customer.csv';
//
//        $this->dispatch('/customer/import', 'POST', $postData);
//        $this->assertResponseStatusCode(200);
//        $this->assertModuleName('Invoice');
//        $this->assertControllerName('Invoice\Controller\Customer');
//        $this->assertControllerClass('CustomerController');
//        $this->assertMatchedRouteName('customer');
//    }
    // test wheather customer mass edit action can be accessed
    public function testMassEditActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $this->dispatch('/customer/massedit');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\Customer');
        $this->assertControllerClass('CustomerController');
        $this->assertMatchedRouteName('customer');

    }


}
