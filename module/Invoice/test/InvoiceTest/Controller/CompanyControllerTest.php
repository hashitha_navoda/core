<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * this contains company controller unit testing codes
 */

namespace InvoiceTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\Container;

class CompanyControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
                array_merge(
                        include DOC_ROOT . 'config/test.application.config.php', include DOC_ROOT . 'config/autoload/local.test.php'
                )
        );
        parent::setUp();
    }

    public function mockUserLogin()
    {
        $userSessions = new Container('ezBizUser');
        $userSessions->userID = 1;
        $userSessions->roleID = 1;
        $userSessions->timeZone = 'Asia/Colombo';
        $locationArray = array(
            'locationName' => "kirulapana",
            'roleID' => 1,
            'locationID' => 53
        );
        $userSessions->userActiveLocation = $locationArray;
    }

    //test whether company index action can be accessed
    public function testIndexActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $_SERVER['HTTP_HOST'] = $this->getApplicationConfig()['servername'];

        $this->dispatch('/company/index');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\Company');
        $this->assertControllerClass('CompanyController');
        $this->assertMatchedRouteName('company');
    }

    //test whether company dispalys action can be accessed
    public function testDisplayActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $this->dispatch('/company/display');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\Company');
        $this->assertControllerClass('CompanyController');
        $this->assertMatchedRouteName('company');
    }

    //test whether reference action can be accessed
    public function testReferenceActionCanBeAccessed()
    {
        //set usserSession variables
        $this->mockUserLogin();

        $this->dispatch('/company/reference');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\Company');
        $this->assertControllerClass('CompanyController');
        $this->assertMatchedRouteName('company');
    }

}
