<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * this contains customer  Api controller unit testing codes
 */

namespace InvoiceTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\Container;

class CustomerAPIControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
                include DOC_ROOT . 'config/test.application.config.php'
        );

        parent::setUp();
    }

    public function mockUserLogin()
    {
        $userSessions = new Container('ezBizUser');
        $userSessions->userID = 1;
        $userSessions->roleID = 1;
        $userSessions->timeZone = 'Asia/Colombo';
        $locationArray = array(
            'locationName' => "kirulapana",
            'roleID' => 1,
            'locationID' => 53
        );
        $userSessions->userActiveLocation = $locationArray;
    }

    //test wheather GetCustomerByID action can be accessed with real database customer
    public function testGetCustomerByIDActionCanBeAccessedWithRealCustomer()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'customerID' => 1,
        );
        $this->dispatch('/customerAPI/getcustomerByID', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather GetCustomerByID action can be accessed with virtual database customer
    public function testGetCustomerByIDActionCanBeAccessedWithVirtualCustomer()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'customerID' => 1000,
        );
        $this->dispatch('/customerAPI/getcustomerByID', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather add action can be accessed with real data base customer
    public function testAddActionCanBeAccessedWithRealCustomer()
    {
        //set userSession variables
        $this->mockUserLogin();

        $data = (object) array('customerID' => 1);

        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('getCustomerByShortname')
                ->will($this->returnValue($data));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        $postData = array(
            'customerShortName' => 'CustomerShortName',
            'customerTitle' => 'Mr.',
            'customerName' => 'CustomerName',
        );
        $this->dispatch('/customerAPI/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather add action can be accessed with virtual data base customer
    public function testAddActionCanBeAccessedWithVirtualCustomer()
    {
        //set userSession variables
        $this->mockUserLogin();
        $data = (object) array('');

        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('getCustomerByShortname')
                ->will($this->returnValue($data));

        $customerTableMock->expects($this->once())
                ->method('saveCustomer')
                ->will($this->returnValue(true));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        $postData = array(
            'customerShortName' => 'Silva',
            'customerTitle' => 'Mr.',
            'customerName' => 'Nimal',
        );
        $this->dispatch('/customerAPI/add', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather getCustomer action can be accessed with real customer
    public function testGetCustomerActionCanBeAccessedWithRealCustomer()
    {
        //set userSession variables
        $this->mockUserLogin();

        $data = (object) array('customerID' => 1, 'customerCurrency' => 1, 'currencyName' => 'RS');

        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('getCustomerByID')
                ->will($this->returnValue($data));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        $postData = array(
            'customerShortName' => 'ashan',
        );
        $this->dispatch('/customerAPI/getcustomer', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather getCustomer action can be accessed with virtual customer
    public function testGetCustomerActionCanBeAccessedWithVirtualCustomer()
    {
        //set userSession variables
        $this->mockUserLogin();

        $data = (object) array('');

        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('getCustomerByID')
                ->will($this->returnValue($data));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        $postData = array(
            'customerShortName' => 'ashan',
        );
        $this->dispatch('/customerAPI/getcustomer', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather updatecustomer action can be accessed
    public function testUpdateCustomerActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();

        $customerResult = (object) array('customerID' => 1, 'entityID' => 1, 'customerShortName' => 'customerShortName');
        $customerShortNameResult = (object) array();

        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('getCustomerByID')
                ->will($this->returnValue($customerResult));

        $customerTableMock->expects($this->once())
                ->method('updateCustomer')
                ->will($this->returnValue(true));

        $customerTableMock->expects($this->once())
                ->method('getCustomerByShortName')
                ->will($this->returnValue($customerShortNameResult));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        $entityTableMock = $this->getMockBuilder('Core\Model\EntityTable')
                ->disableOriginalConstructor()
                ->getMock();

        $entityTableMock->expects($this->once())
                ->method('updateEntity');

        $serviceManagerEntity = $this->getApplicationServiceLocator();
        $serviceManagerEntity->setAllowOverride(true);
        $serviceManagerEntity->setService('Core\Model\EntityTable', $entityTableMock);

        $postData = array(
            'customerID' => 1,
        );
        $this->dispatch('/customerAPI/updatecustomer', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather getCustomerFromSearchByCustomerName action can be accessed
    public function testGetCustomerFromSearchByCustomerNameActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'customerName' => 'CustomerName',
        );
        $this->dispatch('/customerAPI/getCustomerFromSearchByCustomerName', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather getCustomerFromSearchByCustomerShortName action can be accessed
    public function testGetCustomerFromSearchByCustomerShortNameActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'customerName' => 'CustomerShortName',
        );
        $this->dispatch('/customerAPI/getCustomerFromSearchByCustomerShortName', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather getCusdeleteConfirmBox action can be accessed
    public function testGetCusDeleteCofirmBoxActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();

        $this->dispatch('/customerAPI/getCusdeleteConfirmBox');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather deleteCustomer action can be accessed
    public function testDeleteCustomerActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'customerID' => 1,
        );

        $custDataObject = (object) array('customerShortName' => 'CustomerShortName', 'entityID' => 4);

        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('getCustomerByID')
                ->will($this->returnValue($custDataObject));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        $entityTableMock = $this->getMockBuilder('Core\Model\EntityTable')
                ->disableOriginalConstructor()
                ->getMock();

        $entityTableMock->expects($this->once())
                ->method('updateEntity')
                ->will($this->returnValue(true));

        $serviceManager->setService('Core\Model\EntityTable', $entityTableMock);

        $this->dispatch('/customerAPI/deleteCustomer', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather getImportConfirm action can be accessed
    public function testGetImportConfirmActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();

        $this->dispatch('/customerAPI/getImportConfirm');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather getCustomerEditBox action can be accessed
    public function testGetCustomerEditBoxActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'customerID' => 1,
        );

        $this->dispatch('/customerAPI/getCustomerEditBox', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather importcustomer action can be accessed with discard method
    public function testImportCustomerActionCanBeAccessedWithDiscardMethod()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'method' => 'discard',
            'header' => 'dd',
            'delim' => ',',
            'columncount' => 4,
            'col' => array(
                0 => 'Customer Title',
                1 => 'Customer Name',
                2 => 'Customer Short Name',
                3 => 'Currency',
                4 => 'Telephone',
                5 => 'Email',
                6 => 'Address',
                7 => 'Vat Number',
                8 => 'Outstanding Balance',
                9 => 'Credit Balance',
                10 => 'Credit Limit',
                11 => 'Discount',
                12 => 'Other',
                13 => 'Customer Title',
            ),
        );
        $data = (object) array();
        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('getCustomerByShortname')
                ->will($this->returnValue($data));

        $customerTableMock->expects($this->once())
                ->method('saveCustomer')
                ->will($this->returnValue(true));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        $entityTableMock = $this->getMockBuilder('Core\Model\EntityTable')
                ->disableOriginalConstructor()
                ->getMock();

        $entityTableMock->expects($this->once())
                ->method('saveEntity')
                ->will($this->returnValue(4));
        $serviceManager->setService('Core\Model\EntityTable', $entityTableMock);

        //copy customer.csv file to /tmp/ezBiz.customerImportData.csv for customer import testing
        copy(DOC_ROOT . '/data/testFiles/customer.csv', '/tmp/ezBiz.customerImportData.csv');

        $this->dispatch('/customerAPI/importcustomer', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather importcustomer action can be accessed with replace method customerID alredy set
    public function testImportCustomerActionCanBeAccessedWithReplaceMethodCusomerIDAlredySet()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'method' => 'replace',
            'header' => 'dd',
            'delim' => ',',
            'columncount' => 4,
            'col' => array(
                0 => 'Customer Title',
                1 => 'Customer Name',
                2 => 'Customer Short Name',
                3 => 'Currency',
                4 => 'Telephone',
                5 => 'Email',
                6 => 'Address',
                7 => 'Vat Number',
                8 => 'Outstanding Balance',
                9 => 'Credit Balance',
                10 => 'Credit Limit',
                11 => 'Discount',
                12 => 'Other',
                13 => 'Customer Title',
            ),
        );
        $data = (object) array('customerID' => 1, 'entityID' => 4);
        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('getCustomerByShortname')
                ->will($this->returnValue($data));

        $customerTableMock->expects($this->once())
                ->method('importreplaceCustomer')
                ->will($this->returnValue(true));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        //copy customer.csv file to /tmp/ezBiz.customerImportData.csv for customer import testing
        copy(DOC_ROOT . '/data/testFiles/customer.csv', '/tmp/ezBiz.customerImportData.csv');

        $this->dispatch('/customerAPI/importcustomer', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather importcustomer action can be accessed with replace method customerID Not set
    public function testImportCustomerActionCanBeAccessedWithReplaceMethodCustomerIDNoteSet()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'method' => 'replace',
            'header' => 'dd',
            'delim' => ',',
            'columncount' => 4,
            'col' => array(
                0 => 'Customer Title',
                1 => 'Customer Name',
                2 => 'Customer Short Name',
                3 => 'Currency',
                4 => 'Telephone',
                5 => 'Email',
                6 => 'Address',
                7 => 'Vat Number',
                8 => 'Outstanding Balance',
                9 => 'Credit Balance',
                10 => 'Credit Limit',
                11 => 'Discount',
                12 => 'Other',
                13 => 'Customer Title',
            ),
        );
        $data = (object) array();
        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('getCustomerByShortname')
                ->will($this->returnValue($data));

        $customerTableMock->expects($this->once())
                ->method('saveCustomer')
                ->will($this->returnValue(true));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        $entityTableMock = $this->getMockBuilder('Core\Model\EntityTable')
                ->disableOriginalConstructor()
                ->getMock();

        $entityTableMock->expects($this->once())
                ->method('saveEntity')
                ->will($this->returnValue(4));
        $serviceManager->setService('Core\Model\EntityTable', $entityTableMock);

        //copy customer.csv file to /tmp/ezBiz.customerImportData.csv for customer import testing
        copy(DOC_ROOT . '/data/testFiles/customer.csv', '/tmp/ezBiz.customerImportData.csv');

        $this->dispatch('/customerAPI/importcustomer', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

    //test wheather massupdate action can be accessed
    public function testMassUpdateActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'cupd' => array(
                0 => array(
                    0 => '206',
                    1 => 'Mr.',
                    2 => 'ashan',
                    3 => 'Rs.',
                    4 => 'SLR',
                    5 => '0718554612',
                    6 => 'asasd@sadad.com',
                    7 => 'Kiribathgoda',
                    8 => '0',
                    9 => 'Cash Only',
                    10 => '50179.40',
                    11 => '54840.90',
                    12 => '53703.00',
                    13 => '10',
                )
            ),
        );

        $customerTableMock = $this->getMockBuilder('Invoice\Model\CustomerTable')
                ->disableOriginalConstructor()
                ->getMock();

        $customerTableMock->expects($this->once())
                ->method('massupdateCustomer')
                ->will($this->returnValue(true));

        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('Invoice\Model\CustomerTable', $customerTableMock);

        $this->dispatch('/customerAPI/massupdate', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\CustomerAPI');
        $this->assertControllerClass('CustomerAPIController');
        $this->assertMatchedRouteName('customerAPI');
    }

}
