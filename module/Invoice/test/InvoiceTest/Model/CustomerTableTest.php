<?php

namespace InvoiceTest\Model;

use Invoice\Model\CustomerTable;
use Invoice\Model\Customer;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;
use Zend\Db\Adapter\AdapterInterface;

class CustomerTableTest extends PHPUnit_Framework_TestCase
{

    public function testCanRetrieveAnCustomerByItsName()
    {
        $customer = new Customer();
        $customer->exchangeArray(array('customerID' => 1,
            'customerTitle' => 'Mr',
            'customerName' => 'ashan',
            'customerShortName' => 'madushka',
            'customerTelephoneNumber' => '0718273445',
            'customerEmail' => 'ashan@thinkcube.com',
            'customerAddress' => '316/b old kandy road dalugama kelaniya',
            'customerVatNumber' => 12,
            'customerPaymentTerm' => 1,
            'customerCreditLimit' => 1000.50,
            'customerCurrentBalance' => 1000.50,
            'customerCurrentCredit' => 1000.50,
            'customerDiscount' => 10,
            'customerOther' => 'nothing',
            'customerCurrency' => 3,
            'entityID' => 1245,
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Customer());
        $resultSet->initialize(array($customer));

        $mockTableGateway = $this->getMock(
                'Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false
        );
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('customerName' => 'ashan'))
                ->will($this->returnValue($resultSet));

        $customerTable = new CustomerTable($mockTableGateway);

        $this->assertSame($customer, $customerTable->getCustomerByName('ashan'));
    }

//    public function testCanRetrieveAnCustomerByItsShortName()
//    {
//        $customer = new Customer();
//        $customer->exchangeArray(array('customerID' => 1,
//            'customerTitle' => 'Mr',
//            'customerName' => 'ashan',
//            'customerShortName' => 'madushka',
//            'customerTelephoneNumber' => '0718273445',
//            'customerEmail' => 'ashan@thinkcube.com',
//            'customerAddress' => '316/b old kandy road dalugama kelaniya',
//            'customerVatNumber' => 12,
//            'customerPaymentTerm' => 1,
//            'customerCreditLimit' => 1000.50,
//            'customerCurrentBalance' => 1000.50,
//            'customerCurrentCredit' => 1000.50,
//            'customerDiscount' => 10,
//            'customerOther' => 'nothing',
//            'customerCurrency' => 3,
//            'entityID' => 1245,
//        ));
//
//        $resultSet = new ResultSet();
//        $resultSet->setArrayObjectPrototype(new Customer());
//        $resultSet->initialize(array($customer));
//
//        $mockTableGateway = $this->getMock(
//                'Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false
//        );
//        $mockTableGateway->expects($this->once())
//                ->method('select')
//                ->with(array('customerShortName' => 'madushka'))
//                ->will($this->returnValue($resultSet));
//
//        $customerTable = new CustomerTable($mockTableGateway);
//
//        $this->assertSame($customer, $customerTable->getCustomerByShortname('madushka'));
//    }
}
