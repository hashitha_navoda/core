<?php

namespace InvoiceTest\Model;

use Invoice\Model\Customer;
use PHPUnit_Framework_TestCase;

class CustomerTest extends PHPUnit_Framework_TestCase
{

    //test customer  properties initially set to NULL
    public function testCustomerInitialState()
    {
        $customer = new Customer();

        $this->assertNull(
                $customer->customerID, '"customerID" should initially be null'
        );
        $this->assertNull(
                $customer->customerTitle, '"customerTitle" should initially be null'
        );
        $this->assertNull(
                $customer->customerName, '"customerName" should initially be null'
        );
        $this->assertNull(
                $customer->customerShortName, '"customerShortName" should initially be null'
        );
        $this->assertNull(
                $customer->customerTelephoneNumber, '"customerTelephoneNumber" should initially be null'
        );
        $this->assertNull(
                $customer->customerEmail, '"customerEmail" should initially be null'
        );
        $this->assertNull(
                $customer->customerAddress, '"customerAddress" should initially be null'
        );
        $this->assertNull(
                $customer->customerVatNumber, '"customerVatNumber" should initially be null'
        );
        $this->assertNull(
                $customer->customerPaymentTerm, '"customerPaymentTerm" should initially be null'
        );
        $this->assertNull(
                $customer->customerCreditLimit, '"customerCreditLimit" should initially be null'
        );
        $this->assertNull(
                $customer->customerCurrentBalance, '"customerCurrentBalance" should initially be null'
        );
        $this->assertNull(
                $customer->customerCurrentCredit, '"customerCurrentCredit" should initially be null'
        );
        $this->assertNull(
                $customer->customerDiscount, '"customerDiscount" should initially be null'
        );
        $this->assertNull(
                $customer->customerOther, '"customerOther" should initially be null'
        );
        $this->assertNull(
                $customer->customerCurrency, '"customerCurrency" should initially be null'
        );
        $this->assertNull(
                $customer->entityID, '"entityID" should initially be null'
        );
    }

    //test customer’s properties be set correctly when we call exchangeArray()
    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $customer = new Customer();

        $data = array('customerID' => 1,
            'customerTitle' => 'Mr',
            'customerName' => 'ashan',
            'customerShortName' => 'madushka',
            'customerTelephoneNumber' => '0718273445',
            'customerEmail' => 'ashan@thinkcube.com',
            'customerAddress' => '316/b old kandy road dalugama kelaniya',
            'customerVatNumber' => 12,
            'customerPaymentTerm' => 1,
            'customerCreditLimit' => 1000.50,
            'customerCurrentBalance' => 1000.50,
            'customerCurrentCredit' => 1000.50,
            'customerDiscount' => 10,
            'customerOther' => 'nothing',
            'customerCurrency' => 3,
            'entityID' => 1245,
        );

        $customer->exchangeArray($data);

        $this->assertSame(
                $data['customerID'], $customer->customerID, '"customerID" was not set correctly'
        );
        $this->assertSame(
                $data['customerTitle'], $customer->customerTitle, '"customerTitle" was not set correctly'
        );
        $this->assertSame(
                $data['customerName'], $customer->customerName, '"customerName" was not set correctly'
        );
        $this->assertSame(
                $data['customerShortName'], $customer->customerShortName, '"customerShortName" was not set correctly'
        );
        $this->assertSame(
                $data['customerTelephoneNumber'], $customer->customerTelephoneNumber, '"customerTelephoneNumber" was not set correctly'
        );
        $this->assertSame(
                $data['customerEmail'], $customer->customerEmail, '"customerEmail" was not set correctly'
        );
        $this->assertSame(
                $data['customerAddress'], $customer->customerAddress, '"customerAddress" was not set correctly'
        );
        $this->assertSame(
                $data['customerVatNumber'], $customer->customerVatNumber, '"customerVatNumber" was not set correctly'
        );
        $this->assertSame(
                $data['customerPaymentTerm'], $customer->customerPaymentTerm, '"customerPaymentTerm" was not set correctly'
        );
        $this->assertSame(
                $data['customerCreditLimit'], $customer->customerCreditLimit, '"customerCreditLimit" was not set correctly'
        );
        $this->assertSame(
                $data['customerCurrentBalance'], $customer->customerCurrentBalance, '"customerCurrentBalance" was not set correctly'
        );
        $this->assertSame(
                $data['customerCurrentCredit'], $customer->customerCurrentCredit, '"customerCurrentCredit" was not set correctly'
        );
        $this->assertSame(
                $data['customerDiscount'], $customer->customerDiscount, '"customerDiscount" was not set correctly'
        );
        $this->assertSame(
                $data['customerOther'], $customer->customerOther, '"customerOther" was not set correctly'
        );
        $this->assertSame(
                $data['customerCurrency'], $customer->customerCurrency, '"customerCurrency" was not set correctly'
        );
        $this->assertSame(
                $data['entityID'], $customer->entityID, '"entityID" was not set correctly'
        );
    }

    //test default value be used for customer properties
    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $customer = new Customer();

        $customer->exchangeArray(array('customerID' => 1,
            'customerTitle' => 'Mr',
            'customerName' => 'ashan',
            'customerShortName' => 'madushka',
            'customerTelephoneNumber' => '0718273445',
            'customerEmail' => 'ashan@thinkcube.com',
            'customerAddress' => '316/b old kandy road dalugama kelaniya',
            'customerVatNumber' => 12,
            'customerPaymentTerm' => 1,
            'customerCreditLimit' => 1000.50,
            'customerCurrentBalance' => 1000.50,
            'customerCurrentCredit' => 1000.50,
            'customerDiscount' => 10,
            'customerOther' => 'nothing',
            'customerCurrency' => 3,
            'entityID' => 1245,
        ));
        $customer->exchangeArray(array());

        $this->assertNull(
                $customer->customerID, '"customerID" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerTitle, '"customerTitle" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerName, '"customerName" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerShortName, '"customerShortName" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerTelephoneNumber, '"customerTelephoneNumber" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerEmail, '"customerEmail" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerAddress, '"customerAddress" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerVatNumber, '"customerVatNumber" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerPaymentTerm, '"customerPaymentTerm" should have defaulted to null'
        );
        $this->assertEquals(
                0, $customer->customerCreditLimit, '"customerCreditLimit" should have defaulted to null'
        );
        $this->assertEquals(
                0, $customer->customerCurrentBalance, '"customerCurrentBalance" should have defaulted to null'
        );
        $this->assertEquals(
                0, $customer->customerCurrentCredit, '"customerCurrentCredit" should have defaulted to null'
        );
        $this->assertEquals(
                0, $customer->customerDiscount, '"customerDiscount" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerOther, '"customerOther" should have defaulted to null'
        );
        $this->assertNull(
                $customer->customerCurrency, '"customerCurrency" should have defaulted to null'
        );
        $this->assertNull(
                $customer->entityID, '"entityID" should have defaulted to null'
        );
    }

    //test whether we can get an array copy of our model
    public function testGetArrayCopyReturnsAnArrayWithPropertyValues()
    {
        $customer = new Customer();
        $data = array('customerID' => 1,
            'customerTitle' => 'Mr',
            'customerName' => 'ashan',
            'customerShortName' => 'madushka',
            'customerTelephoneNumber' => '0718273445',
            'customerEmail' => 'ashan@thinkcube.com',
            'customerAddress' => '316/b old kandy road dalugama kelaniya',
            'customerVatNumber' => 12,
            'customerPaymentTerm' => 1,
            'customerCreditLimit' => 1000.50,
            'customerCurrentBalance' => 1000.50,
            'customerCurrentCredit' => 1000.50,
            'customerDiscount' => 10,
            'customerOther' => 'nothing',
            'customerCurrency' => 3,
            'entityID' => 1245,
        );

        $customer->exchangeArray($data);
        $copyArray = $customer->getArrayCopy();

        $this->assertSame(
                $data['customerID'], $copyArray['customerID'], '"customerID" was not set correctly'
        );
        $this->assertSame(
                $data['customerTitle'], $copyArray['customerTitle'], '"customerTitle" was not set correctly'
        );
        $this->assertSame(
                $data['customerName'], $copyArray['customerName'], '"customerName" was not set correctly'
        );
        $this->assertSame(
                $data['customerShortName'], $copyArray['customerShortName'], '"customerShortName" was not set correctly'
        );
        $this->assertSame(
                $data['customerTelephoneNumber'], $copyArray['customerTelephoneNumber'], '"customerTelephoneNumber" was not set correctly'
        );
        $this->assertSame(
                $data['customerEmail'], $copyArray['customerEmail'], '"customerEmail" was not set correctly'
        );
        $this->assertSame(
                $data['customerAddress'], $copyArray['customerAddress'], '"customerAddress" was not set correctly'
        );
        $this->assertSame(
                $data['customerVatNumber'], $copyArray['customerVatNumber'], '"customerVatNumber" was not set correctly'
        );
        $this->assertSame(
                $data['customerPaymentTerm'], $copyArray['customerPaymentTerm'], '"customerPaymentTerm" was not set correctly'
        );
        $this->assertSame(
                $data['customerCreditLimit'], $copyArray['customerCreditLimit'], '"customerCreditLimit" was not set correctly'
        );
        $this->assertSame(
                $data['customerCurrentBalance'], $copyArray['customerCurrentBalance'], '"customerCurrentBalance" was not set correctly'
        );
        $this->assertSame(
                $data['customerCurrentCredit'], $copyArray['customerCurrentCredit'], '"customerCurrentCredit" was not set correctly'
        );
        $this->assertSame(
                $data['customerDiscount'], $copyArray['customerDiscount'], '"customerDiscount" was not set correctly'
        );
        $this->assertSame(
                $data['customerOther'], $copyArray['customerOther'], '"customerOther" was not set correctly'
        );
        $this->assertSame(
                $data['customerCurrency'], $copyArray['customerCurrency'], '"customerCurrency" was not set correctly'
        );
        $this->assertSame(
                $data['entityID'], $copyArray['entityID'], '"entityID" was not set correctly'
        );
    }

    //test input fileter set for customer
    public function testInputFiltersAreSetCorrectly()
    {
        $customer = new Customer();

        $inputFilter = $customer->getInputFilter();

        $this->assertSame(5, $inputFilter->count());
        $this->assertTrue($inputFilter->has('customerName'));
        $this->assertTrue($inputFilter->has('customerShortName'));
        $this->assertTrue($inputFilter->has('customerTelephoneNumber'));
        $this->assertTrue($inputFilter->has('customerAddress'));
        $this->assertTrue($inputFilter->has('customerOther'));
    }

}
