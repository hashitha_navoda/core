<?php

namespace Invoice;

use Settings\Model\Company;
use Settings\Model\CompanyTable;
use Invoice\Model\InvoiceTable;
use Invoice\Model\Invoice;
use Invoice\Model\InvoiceProductTable;
use Invoice\Model\InvoiceProduct;
use Invoice\Model\InvoiceSubProductTable;
use Invoice\Model\InvoiceSubProduct;
use Invoice\Model\InvoiceProductTaxTable;
use Invoice\Model\InvoiceProductTax;
use Invoice\Model\InvoiceReceipt;
use Invoice\Model\InvoiceReceiptTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Invoice\Model\Customer;
use Invoice\Model\CustomerTable;
use Core\Model\CurrencyTable;
use Core\Model\Currency;
use Invoice\Model\SalesOrder;
use Invoice\Model\SalesOrderTable;
use Invoice\Model\SalesOrderProduct;
use Invoice\Model\SalesOrderProductTable;
use Invoice\Model\SalesOrderProductTax;
use Invoice\Model\SalesOrderProductTaxTable;
use Invoice\Model\Quotation;
use Invoice\Model\QuotationTable;
use Invoice\Model\QuotationProduct;
use Invoice\Model\QuotationProductTable;
use Invoice\Model\QuotationProductTax;
use Invoice\Model\QuotationProductTaxTable;
use Invoice\Model\Payments;
use Invoice\Model\PaymentsTable;
use Invoice\Model\InvoicePayments;
use Invoice\Model\InvoicePaymentsTable;
use Invoice\Model\PaymentMethods;
use Invoice\Model\PaymentMethodsTable;
use Invoice\Model\RecurrentInvoice;
use Invoice\Model\RecurrentInvoiceTable;
use Invoice\Model\CreditNote;
use Invoice\Model\CreditNoteTable;
use Invoice\Model\CreditNoteProduct;
use Invoice\Model\CreditNoteProductTable;
use Invoice\Model\CreditNoteProductTax;
use Invoice\Model\CreditNoteProductTaxTable;
use Invoice\Model\CreditNoteSubProduct;
use Invoice\Model\CreditNoteSubProductTable;
use Invoice\Model\DeliveryNote;
use Invoice\Model\DeliveryNoteTable;
use Invoice\Model\DeliveryNoteProduct;
use Invoice\Model\DeliveryNoteProductTable;
use Invoice\Model\DeliveryNoteSubProduct;
use Invoice\Model\DeliveryNoteSubProductTable;
use Invoice\Model\DeliveryNoteProductTax;
use Invoice\Model\DeliveryNoteProductTaxTable;
use Invoice\Model\Returns;
use Invoice\Model\ReturnsTable;
use Invoice\Model\ReturnsProduct;
use Invoice\Model\ReturnsProductTable;
use Invoice\Model\ReturnsSubProduct;
use Invoice\Model\ReturnsSubProductTable;
use Invoice\Model\ReturnsProductTax;
use Invoice\Model\ReturnsProductTaxTable;
use Invoice\Model\CreditNotePayment;
use Invoice\Model\CreditNotePaymentTable;
use Invoice\Model\CreditNotePaymentDetails;
use Invoice\Model\CreditNotePaymentDetailsTable;
use Invoice\Model\CreditNotePaymentMethodNumbers;
use Invoice\Model\CreditNotePaymentMethodNumbersTable;
use Invoice\Model\CustomerProfile;
use Invoice\Model\CustomerProfileTable;
use Invoice\Model\CustomerCategory;
use Invoice\Model\CustomerCategoryTable;
use Invoice\Model\IncomingPaymentMethod;
use Invoice\Model\IncomingPaymentMethodTable;
use Invoice\Model\IncomingPaymentMethodCash;
use Invoice\Model\IncomingPaymentMethodCashTable;
use Invoice\Model\IncomingPaymentMethodCheque;
use Invoice\Model\IncomingPaymentMethodChequeTable;
use Invoice\Model\IncomingPaymentMethodCreditCard;
use Invoice\Model\IncomingPaymentMethodCreditCardTable;
use Invoice\Model\IncomingPaymentMethodBankTransfer;
use Invoice\Model\IncomingPaymentMethodBankTransferTable;
use Invoice\Model\IncomingPaymentMethodGiftCard;
use Invoice\Model\IncomingPaymentMethodGiftCardTable;
use Invoice\Model\IncomingPaymentMethodLC;
use Invoice\Model\IncomingPaymentMethodLCTable;
use Invoice\Model\IncomingPaymentMethodTT;
use Invoice\Model\IncomingPaymentMethodTTTable;
use Invoice\Model\IncomingPaymentMethodUniformVoucher;
use Invoice\Model\IncomingPaymentMethodUniformVoucherTable;
use Invoice\Model\RatingTypes;
use Invoice\Model\RatingTypesTable;
use Invoice\Model\CustomerRatings;
use Invoice\Model\CustomerRatingsTable;
use Invoice\Model\InvoiceEditLog;
use Invoice\Model\InvoiceEditLogTable;
use Invoice\Model\DeliveryNoteEditLog;
use Invoice\Model\DeliveryNoteEditLogTable;
use Invoice\Model\DeliveryNoteEditLogDetails;
use Invoice\Model\DeliveryNoteEditLogDetailsTable;
use Invoice\Model\InvoiceProductDeliveryNoteProduct;
use Invoice\Model\InvoiceProductDeliveryNoteProductTable;
use Invoice\Model\Membership;
use Invoice\Model\MembershipTable;
use Invoice\Model\DraftInvoice;
use Invoice\Model\DraftInvoiceTable;
use Invoice\Model\InvoicedBomSubItems;
use Invoice\Model\InvoicedBomSubItemsTable;
use Invoice\Model\InvoiceSalesPersons;
use Invoice\Model\InvoiceSalesPersonsTable;
use Invoice\Model\Contacts;
use Invoice\Model\ContactsTable;
use Invoice\Model\ContactMobileNumbers;
use Invoice\Model\ContactMobileNumbersTable;
use Invoice\Model\ContactEmails;
use Invoice\Model\ContactEmailsTable;
use Invoice\Model\CustomerOrder;
use Invoice\Model\CustomerOrderTable;
use Invoice\Model\ServiceChargeType;
use Invoice\Model\ServiceChargeTypeTable;
use Invoice\Model\InvoiceServiceCharge;
use Invoice\Model\InvoiceServiceChargeTable;
use Invoice\Model\InvoiceFreeProduct;
use Invoice\Model\InvoiceFreeProductTable;
use Invoice\Model\DraftInvWfTable;
use Invoice\Model\DraftInvWf;
use Invoice\Model\DraftInvProductTable;
use Invoice\Model\DraftInvProduct;
use Invoice\Model\DraftInvSubProductTable;
use Invoice\Model\DraftInvSubProduct;
use Invoice\Model\DraftInvProductTaxTable;
use Invoice\Model\DraftInvProductTax;
use Invoice\Model\DraftInvServiceCharge;
use Invoice\Model\DraftInvServiceChargeTable;
use Invoice\Model\DraftInvFreeIssueItems;
use Invoice\Model\DraftInvFreeIssueItemsTable;
use Invoice\Model\DraftInvSalesPersons;
use Invoice\Model\DraftInvSalesPersonsTable;
use Invoice\Model\InvoiceReturn;
use Invoice\Model\InvoiceReturnTable;
use Invoice\Model\InvoiceReturnProduct;
use Invoice\Model\InvoiceReturnProductTable;
use Invoice\Model\InvoiceReturnProductTax;
use Invoice\Model\InvoiceReturnProductTaxTable;
use Invoice\Model\InvoiceReturnSubProduct;
use Invoice\Model\InvoiceReturnSubProductTable;
use Invoice\Model\ContactLists;
use Invoice\Model\ContactListsTable;
use Invoice\Model\ContactListContacts;
use Invoice\Model\ContactListContactsTable;
use Invoice\Model\CreditNoteInvoiceReturn;
use Invoice\Model\CreditNoteInvoiceReturnTable;
use Invoice\Model\DraftInvoiceReturn;
use Invoice\Model\DraftInvoiceReturnTable;
use Invoice\Model\DraftInvoiceReturnProduct;
use Invoice\Model\DraftInvoiceReturnProductTable;
use Invoice\Model\DraftInvoiceReturnProductTax;
use Invoice\Model\DraftInvoiceReturnProductTaxTable;
use Invoice\Model\DraftInvoiceReturnSubProduct;
use Invoice\Model\DraftInvoiceReturnSubProductTable;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $this->getConfig();
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
        return include __DIR__ . '/config/msglist.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'PaymentsAPI' => 'Invoice\Controller\CustomerPaymentsAPIController',
                'InvoiceController' => 'Invoice\Controller\API\InvoiceController',
                'QuotationService' => 'Invoice\Service\QuotationService',
                'DeliveryNoteService' => 'Invoice\Service\DeliveryNoteService',
                'SalesPersonService' => 'Invoice\Service\SalesPersonService',
                'CustomerService' => 'Invoice\Service\CustomerService',
                'QuotationProductService' => 'Invoice\Service\QuotationProductService',
                'CustomerOrderService' => 'Invoice\Service\CustomerOrderService',
                'ServiceChargeTypeService' => 'Invoice\Service\ServiceChargeTypeService',
                'QuotationProductTaxService' => 'Invoice\Service\QuotationProductTaxService',
                'CreditNoteController' => 'Invoice\Controller\API\CreditNoteAPIController',
                'CreditNotePaymentController' => 'Invoice\Controller\API\CreditNotePaymentsAPIController',
                'CustomerPaymentsAPIController' => 'Invoice\Controller\CustomerPaymentsAPIController',
                'DeliveryNoteAPI' => 'Invoice\Controller\DeliveryNoteAPIController',
            ),
            'shared' => array(
                'QuotationService' => true,
                'DeliveryNoteService' => true,
                'SalesPersonService' => true,
                'CustomerService' => true,
                'CustomerService' => true,
                'CustomerOrderService' => true,
                'ServiceChargeTypeService' => true
            ),
            'aliases' => array(
                'QuotationTable' => 'Invoice\Model\QuotationTable',
                'QuotationProductTable' => 'Invoice\Model\QuotationProductTable',
                'QuotationProductTaxTable' => 'Invoice\Model\QuotationProductTaxTable',
                'CustomerTable' => 'Invoice\Model\CustomerTable',
                'CustomerRatingsTable' => 'Invoice\Model\CustomerRatingsTable',
                'InvoiceProductTable' => 'Invoice\Model\InvoiceProductTable',
                'DraftInvProductTable' => 'Invoice\Model\DraftInvProductTable',
                'CreditNoteTable' => 'Invoice\Model\CreditNoteTable',
                'InvoiceTable' => 'Invoice\Model\InvoiceTable',
                'DraftInvWfTable' => 'Invoice\Model\DraftInvWfTable',
                'InvoiceProductTaxTable' => 'Invoice\Model\InvoiceProductTaxTable',
                'DraftInvProductTaxTable' => 'Invoice\Model\DraftInvProductTaxTable',
                'DeliveryNoteTable' => 'Invoice\Model\DeliveryNoteTable',
                'DeliveryNoteProductTable' => 'Invoice\Model\DeliveryNoteProductTable',
                'DeliveryNoteProductTaxTable' => 'Invoice\Model\DeliveryNoteProductTaxTable',
                'PaymentsTable' => 'Invoice\Model\PaymentsTable',
                'CreditNotePaymentTable' => 'Invoice\Model\CreditNotePaymentTable',
                'CreditNoteProductTaxTable' => 'Invoice\Model\CreditNoteProductTaxTable',
                'CustomerCategoryTable' => 'Invoice\Model\CustomerCategoryTable',
                'CustomerProfileTable' => 'Invoice\Model\CustomerProfileTable',
                'InvoiceProductDeliveryNoteProductTable' => 'Invoice\Model\InvoiceProductDeliveryNoteProductTable',
                'SalesOrderProductTable' => 'Invoice\Model\SalesOrderProductTable',
                'IncomingPaymentMethodUniformVoucherTable' => 'Invoice\Model\IncomingPaymentMethodUniformVoucherTable',
                'InvoicePaymentsTable' => 'Invoice\Model\InvoicePaymentsTable',
                'IncomingPaymentMethodCashTable' => 'Invoice\Model\IncomingPaymentMethodCashTable',
                'IncomingPaymentMethodChequeTable' => 'Invoice\Model\IncomingPaymentMethodChequeTable',
                'IncomingPaymentMethodCreditCardTable' => 'Invoice\Model\IncomingPaymentMethodCreditCardTable',
                'IncomingPaymentMethodBankTransferTable' => 'Invoice\Model\IncomingPaymentMethodBankTransferTable',
                'IncomingPaymentMethodGiftCardTable' => 'Invoice\Model\IncomingPaymentMethodGiftCardTable',
                'IncomingPaymentMethodLCTable' => 'Invoice\Model\IncomingPaymentMethodLCTable',
                'IncomingPaymentMethodTTTable' => 'Invoice\Model\IncomingPaymentMethodTTTable',
                'IncomingPaymentMethodTable' => 'Invoice\Model\IncomingPaymentMethodTable',
                'InvoicedBomSubItemsTable' => 'Invoice\Model\InvoicedBomSubItemsTable',
                'InvoiceSalesPersonsTable' => 'Invoice\Model\InvoiceSalesPersonsTable',
                'DraftInvSalesPersonsTable' => 'Invoice\Model\DraftInvSalesPersonsTable',
                'ContactsTable' => 'Invoice\Model\ContactsTable',
                'CustomerOrderTable' => 'Invoice\Model\CustomerOrderTable',
                'ServiceChargeTypeTable' => 'Invoice\Model\ServiceChargeTypeTable',
                'InvoiceServiceChargeTable' => 'Invoice\Model\InvoiceServiceChargeTable',
                'DraftInvServiceChargeTable' => 'Invoice\Model\DraftInvServiceChargeTable',
                'InvoiceFreeProductTable' => 'Invoice\Model\InvoiceFreeProductTable',
                'DraftInvFreeIssueItemsTable' => 'Invoice\Model\DraftInvFreeIssueItemsTable',
                'ContactMobileNumbersTable' => 'Invoice\Model\ContactMobileNumbersTable',
                'ContactEmailsTable' => 'Invoice\Model\ContactEmailsTable',
                'InvoiceReturnTable' => 'Invoice\Model\InvoiceReturnTable',
                'InvoiceReturnProductTable' => 'Invoice\Model\InvoiceReturnProductTable',
                'InvoiceReturnProductTaxTable' => 'Invoice\Model\InvoiceReturnProductTaxTable',
                'InvoiceReturnSubProductTable' => 'Invoice\Model\InvoiceReturnSubProductTable',
                'CreditNoteInvoiceReturnTable' => 'Invoice\Model\CreditNoteInvoiceReturnTable',
                'ContactListsTable' => 'Invoice\Model\ContactListsTable',
                'ContactListContactsTable' => 'Invoice\Model\ContactListContactsTable',
                'DraftInvoiceReturnTable' => 'Invoice\Model\DraftInvoiceReturnTable',
                'DraftInvoiceReturnProductTable' => 'Invoice\Model\DraftInvoiceReturnProductTable',
                'DraftInvoiceReturnProductTaxTable' => 'Invoice\Model\DraftInvoiceReturnProductTaxTable',
                'DraftInvoiceReturnSubProductTable' => 'Invoice\Model\DraftInvoiceReturnSubProductTable',
            ),

            'factories' => array(
                'Invoice\Model\InvoiceProductTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceProductTableGateway');
            $table = new InvoiceProductTable($tableGateway);
            return $table;
        },
                'InvoiceProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceProduct());
            return new TableGateway('salesInvoiceProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvProductTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvProductTableGateway');
            $table = new DraftInvProductTable($tableGateway);
            return $table;
        },
                'DraftInvProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvProduct());
            return new TableGateway('draftInvProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceSubProductTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceSubProductTableGateway');
            $table = new InvoiceSubProductTable($tableGateway);
            return $table;
        },
                'InvoiceSubProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceSubProduct());
            return new TableGateway('salesInvoiceSubProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvSubProductTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvSubProductTableGateway');
            $table = new DraftInvSubProductTable($tableGateway);
            return $table;
        },
                'DraftInvSubProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvSubProduct());
            return new TableGateway('draftInvSubProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceProductTaxTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceProductTaxTableGateway');
            $table = new InvoiceProductTaxTable($tableGateway);
            return $table;
        },
                'InvoiceProductTaxTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceProductTax());
            return new TableGateway('salesInvoiceProductTax', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvProductTaxTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvProductTaxTableGateway');
            $table = new DraftInvProductTaxTable($tableGateway);
            return $table;
        },
                'DraftInvProductTaxTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvProductTax());
            return new TableGateway('draftInvProductTax', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DeliveryNoteProductTaxTable' => function($sm) {
            $tableGateway = $sm->get('DeliveryNoteProductTaxTableGateway');
            $table = new DeliveryNoteProductTaxTable($tableGateway);
            return $table;
        },
                'DeliveryNoteProductTaxTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DeliveryNoteProductTax());
            return new TableGateway('deliveryNoteProductTax', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\PaymentMethodTable' => function($sm) {
            $tableGateway = $sm->get('PaymentMethodGateway');
            $table = new PaymentMethodsNumbersTable($tableGateway);
            return $table;
        },
                'PaymentMethodGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new PaymentMethods());
            return new TableGateway('paymentMethod', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CustomerTable' => function($sm) {
            $tableGateway = $sm->get('CustomerTableGateway');
            $table = new CustomerTable($tableGateway);
            return $table;
        },
                'CustomerTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Customer());
            return new TableGateway('customer', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CreditNoteTable' => function($sm) {
            $tableGateway = $sm->get('CreditNoteTableGateway');
            $table = new CreditNoteTable($tableGateway);
            return $table;
        },
                'CreditNoteTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CreditNote());
            return new TableGateway('creditNote', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CreditNoteProductTable' => function($sm) {
            $tableGateway = $sm->get('CreditNoteProductTableGateway');
            $table = new CreditNoteProductTable($tableGateway);
            return $table;
        },
                'CreditNoteProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CreditNoteProduct());
            return new TableGateway('creditNoteProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CreditNoteProductTaxTable' => function($sm) {
            $tableGateway = $sm->get('CreditNoteProductTaxTableGateway');
            $table = new CreditNoteProductTaxTable($tableGateway);
            return $table;
        },
                'CreditNoteProductTaxTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CreditNoteProductTax());
            return new TableGateway('creditNoteProductTax', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CreditNoteSubProductTable' => function($sm) {
            $tableGateway = $sm->get('CreditNoteSubProductTableGateway');
            $table = new CreditNoteSubProductTable($tableGateway);
            return $table;
        },
                'CreditNoteSubProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CreditNoteSubProduct());
            return new TableGateway('creditNoteSubProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\CurrencyTable' => function($sm) {
            $tableGateway = $sm->get('CurrencyTableGateway');
            $table = new CurrencyTable($tableGateway);
            return $table;
        },
                'CurrencyTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Currency());
            return new TableGateway('currency', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\QuotationTable' => function($sm) {
            $tableGateway = $sm->get('QuotationTableGateway');
            $table = new QuotationTable($tableGateway);
            return $table;
        },
                'QuotationTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Quotation());
            return new TableGateway('quotation', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceSalesPersonsTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceSalesPersonsTableGateway');
            $table = new InvoiceSalesPersonsTable($tableGateway);
            return $table;
        },
                'InvoiceSalesPersonsTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceSalesPersons());
            return new TableGateway('invoiceSalesPersons', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvSalesPersonsTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvSalesPersonsTableGateway');
            $table = new DraftInvSalesPersonsTable($tableGateway);
            return $table;
        },
                'DraftInvSalesPersonsTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvSalesPersons());
            return new TableGateway('draftInvSalesPersons', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceReturnTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceReturnTableGateway');
            $table = new InvoiceReturnTable($tableGateway);
            return $table;
        },
                'InvoiceReturnTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceReturn());
            return new TableGateway('invoiceReturns', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceReturnProductTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceReturnProductTableGateway');
            $table = new InvoiceReturnProductTable($tableGateway);
            return $table;
        },
                'InvoiceReturnProductTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceReturnProduct());
            return new TableGateway('invoiceReturnProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceReturnProductTaxTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceReturnProductTaxTableGateway');
            $table = new InvoiceReturnProductTaxTable($tableGateway);
            return $table;
        },
                'InvoiceReturnProductTaxTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceReturnProductTax());
            return new TableGateway('invoiceReturnProductTax', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceReturnSubProductTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceReturnSubProductTableGateway');
            $table = new InvoiceReturnSubProductTable($tableGateway);
            return $table;
        },
                'InvoiceReturnSubProductTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceReturnSubProduct());
            return new TableGateway('invoiceReturnSubProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CreditNoteInvoiceReturnTable' => function($sm) {
            $tableGateway = $sm->get('CreditNoteInvoiceReturnTableGateway');
            $table = new CreditNoteInvoiceReturnTable($tableGateway);
            return $table;
        },
                'CreditNoteInvoiceReturnTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CreditNoteInvoiceReturn());
            return new TableGateway('creditNoteInvoiceReturns', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\QuotationProductTable' => function($sm) {
            $tableGateway = $sm->get('QuotationProductTableGateway');
            $table = new QuotationProductTable($tableGateway);
            return $table;
        },
                'QuotationProductTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new QuotationProduct());
            return new TableGateway('quotationProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceReceiptTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceReceiptTableGateway');
            $table = new InvoiceReceiptTable($tableGateway);
            return $table;
        },
                'InvoiceReceiptTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceReceipt());
            return new TableGateway('invoiceReceipt', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\SalesOrderTable' => function($sm) {
            $tableGateway = $sm->get('SalesOrderTableGateway');
            $table = new SalesOrderTable($tableGateway);
            return $table;
        },
                'SalesOrderTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new SalesOrder());
            return new TableGateway('salesOrders', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\SalesOrderProductTable' => function($sm) {
            $tableGateway = $sm->get('SalesOrderProductTableGateway');
            $table = new SalesOrderProductTable($tableGateway);
            return $table;
        },
                'SalesOrderProductTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new SalesOrderProduct());
            return new TableGateway('salesOrdersProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceTableGateway');
            $table = new InvoiceTable($tableGateway);
            return $table;
        },
                'InvoiceTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Invoice());
            return new TableGateway('salesInvoice', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvWfTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvWfTableGateway');
            $table = new DraftInvWfTable($tableGateway);
            return $table;
        },
                'DraftInvWfTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvWf());
            return new TableGateway('draftInvWf', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\PaymentsTable' => function($sm) {
            $tableGateway = $sm->get('PaymentsTableGateway');
            $table = new PaymentsTable($tableGateway);
            return $table;
        },
                'PaymentsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Payments());
            return new TableGateway('incomingPayment', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoicePaymentsTable' => function($sm) {
            $tableGateway = $sm->get('InvoicePaymentsTableGateway');
            $table = new InvoicePaymentsTable($tableGateway);
            return $table;
        },
                'InvoicePaymentsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoicePayments());
            return new TableGateway('incomingInvoicePayment', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\PaymentMethodsTable' => function($sm) {
            $tableGateway = $sm->get('PaymentMethodsTableGateway');
            $table = new PaymentMethodsTable($tableGateway);
            return $table;
        },
                'PaymentMethodsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new PaymentMethods());
            return new TableGateway('paymentMethods', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\RecurrentInvoiceTable' => function($sm) {
            $tableGateway = $sm->get('RecurrentInvoiceTableGateway');
            $table = new RecurrentInvoiceTable($tableGateway);
            return $table;
        },
                'RecurrentInvoiceTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new RecurrentInvoice());
            return new TableGateway('recurrentInvoice', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DeliveryNoteTable' => function($sm) {
            $tableGateway = $sm->get('DeliveryNoteTableGateway');
            $table = new DeliveryNoteTable($tableGateway);
            return $table;
        },
                'DeliveryNoteTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DeliveryNote());
            return new TableGateway('deliveryNote', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DeliveryNoteProductTable' => function($sm) {
            $tableGateway = $sm->get('DeliveryNoteProductTableGateway');
            $table = new DeliveryNoteProductTable($tableGateway);
            return $table;
        },
                'DeliveryNoteProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DeliveryNoteProduct());
            return new TableGateway('deliveryNoteProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DeliveryNoteSubProductTable' => function($sm) {
            $tableGateway = $sm->get('DeliveryNoteSubProductTableGateway');
            $table = new DeliveryNoteSubProductTable($tableGateway);
            return $table;
        },
                'DeliveryNoteSubProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DeliveryNoteSubProduct());
            return new TableGateway('deliveryNoteSubProducts', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\QuotationProductTaxTable' => function($sm) {
            $tableGateway = $sm->get('QuotationProductTaxTableGateway');
            $table = new QuotationProductTaxTable($tableGateway);
            return $table;
        },
                'QuotationProductTaxTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new QuotationProductTax());
            return new TableGateway('quotationProductTax', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\SalesOrderProductTaxTable' => function($sm) {
            $tableGateway = $sm->get('SalesOrderProductTaxTableGateway');
            $table = new SalesOrderProductTaxTable($tableGateway);
            return $table;
        },
                'SalesOrderProductTaxTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new SalesOrderProductTax());
            return new TableGateway('salesOrderProductTax', $dbAdapter, null, $resultSetPrototype);
        },
                'SalesReturnsTable' => function($sm) {
            $tableGateway = $sm->get('SalesReturnsTableGateway');
            $table = new ReturnsTable($tableGateway);
            return $table;
        },
                'SalesReturnsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Returns());
            return new TableGateway('salesReturn', $dbAdapter, null, $resultSetPrototype);
        },
                'SalesReturnsProductTable' => function($sm) {
            $tableGateway = $sm->get('SalesReturnsProductTableGateway');
            $table = new ReturnsProductTable($tableGateway);
            return $table;
        },
                'SalesReturnsProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ReturnsProduct());
            return new TableGateway('salesReturnProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'SalesReturnsSubProductTable' => function($sm) {
            $tableGateway = $sm->get('SalesReturnsSubProductTableGateway');
            $table = new ReturnsSubProductTable($tableGateway);
            return $table;
        },
                'SalesReturnsSubProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ReturnsSubProduct());
            return new TableGateway('salesReturnSubProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'SalesReturnsProductTaxTable' => function($sm) {
            $tableGateway = $sm->get('SalesReturnsProductTaxTableGateway');
            $table = new ReturnsProductTaxTable($tableGateway);
            return $table;
        },
                'SalesReturnsProductTaxTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ReturnsProductTax());
            return new TableGateway('salesReturnProductTax', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CompanyTable' => function($sm) {
            $tableGateway = $sm->get('CompanyTableGateway');
            $table = new CompanyTable($tableGateway);
            return $table;
        },
                'CompanyTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CompanyTable());
            return new TableGateway('companyTable', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CreditNotePaymentTable' => function($sm) {
            $tableGateway = $sm->get('CreditNotePaymentTableGateway');
            $table = new CreditNotePaymentTable($tableGateway);
            return $table;
        },
                'CreditNotePaymentTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CreditNotePayment());
            return new TableGateway('creditNotePayment', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CreditNotePaymentDetailsTable' => function($sm) {
            $tableGateway = $sm->get('CreditNotePaymentDetailsTableGateway');
            $table = new CreditNotePaymentDetailsTable($tableGateway);
            return $table;
        },
                'CreditNotePaymentDetailsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CreditNotePaymentDetails());
            return new TableGateway('creditNotePaymentDetails', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CreditNotePaymentMethodNumbers' => function($sm) {
            $tableGateway = $sm->get('CreditNotePaymentMethodNumbersTableGateway');
            $table = new CreditNotePaymentMethodNumbersTable($tableGateway);
            return $table;
        },
                'CreditNotePaymentMethodNumbersTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CreditNotePaymentMethodNumbers());
            return new TableGateway('creditNotePaymentMethodNumbers', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CustomerProfileTable' => function($sm) {
            $tableGateway = $sm->get('CustomerProfileTableGateway');
            $table = new CustomerProfileTable($tableGateway);
            return $table;
        },
                'CustomerProfileTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CustomerProfile());
            return new TableGateway('customerProfile', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CustomerCategoryTable' => function($sm) {
            $tableGateway = $sm->get('CustomerCategoryTableGateway');
            $table = new CustomerCategoryTable($tableGateway);
            return $table;
        },
                'CustomerCategoryTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CustomerCategory());
            return new TableGateway('customerCategory', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\IncomingPaymentMethodTable' => function($sm) {
            $tableGateway = $sm->get('IncomingPaymentMethodTableGateway');
            $table = new IncomingPaymentMethodTable($tableGateway);
            return $table;
        },
                'IncomingPaymentMethodTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethod());
            return new TableGateway('incomingPaymentMethod', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\IncomingPaymentMethodCashTable' => function($sm) {
            $tableGateway = $sm->get('IncomingPaymentMethodCashTableGateway');
            $table = new IncomingPaymentMethodCashTable($tableGateway);
            return $table;
        },
                'IncomingPaymentMethodCashTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethodCash());
            return new TableGateway('incomingPaymentMethodCash', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\IncomingPaymentMethodChequeTable' => function($sm) {
            $tableGateway = $sm->get('IncomingPaymentMethodChequeTableGateway');
            $table = new IncomingPaymentMethodChequeTable($tableGateway);
            return $table;
        },
                'IncomingPaymentMethodChequeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethodCheque());
            return new TableGateway('incomingPaymentMethodCheque', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\IncomingPaymentMethodCreditCardTable' => function($sm) {
            $tableGateway = $sm->get('IncomingPaymentMethodCreditCardTableGateway');
            $table = new IncomingPaymentMethodCreditCardTable($tableGateway);
            return $table;
        },
                'IncomingPaymentMethodCreditCardTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethodCreditCard());
            return new TableGateway('incomingPaymentMethodCreditCard', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\IncomingPaymentMethodBankTransferTable' => function($sm) {
            $tableGateway = $sm->get('IncomingPaymentMethodBankTransferTableGateway');
            $table = new IncomingPaymentMethodBankTransferTable($tableGateway);
            return $table;
        },
                'IncomingPaymentMethodBankTransferTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethodBankTransfer());
            return new TableGateway('incomingPaymentMethodBankTransfer', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\IncomingPaymentMethodGiftCardTable' => function($sm) {
            $tableGateway = $sm->get('IncomingPaymentMethodGiftCardTableGateway');
            $table = new IncomingPaymentMethodGiftCardTable($tableGateway);
            return $table;
        },
                'IncomingPaymentMethodGiftCardTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethodGiftCard());
            return new TableGateway('incomingPaymentMethodGiftCard', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\IncomingPaymentMethodLCTable' => function($sm) {
            $tableGateway = $sm->get('IncomingPaymentMethodLCTableGateway');
            $table = new IncomingPaymentMethodLCTable($tableGateway);
            return $table;
        },
                'IncomingPaymentMethodLCTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethodLC());
            return new TableGateway('incomingPaymentMethodLC', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\IncomingPaymentMethodTTTable' => function($sm) {
            $tableGateway = $sm->get('IncomingPaymentMethodTTTableGateway');
            $table = new IncomingPaymentMethodTTTable($tableGateway);
            return $table;
        },
                'IncomingPaymentMethodTTTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethodTT());
            return new TableGateway('incomingPaymentMethodTT', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DispatchNoteTable' => function($sm) {
            $tableGateway = $sm->get('DispatchNoteTableGateway');
            $table = new Model\DispatchNoteTable($tableGateway);
            return $table;
        },
                'DispatchNoteTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Model\DispatchNote());
            return new TableGateway('dispatchNote', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DispatchNoteProductTable' => function($sm) {
            $tableGateway = $sm->get('DispatchNoteProductTableGateway');
            $table = new Model\DispatchNoteProductTable($tableGateway);
            return $table;
        },
                'DispatchNoteProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Model\DispatchNoteProduct());
            return new TableGateway('dispatchNoteProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DispatchNoteSubProductTable' => function($sm) {
            $tableGateway = $sm->get('DispatchNoteSubProductTableGateway');
            $table = new Model\DispatchNoteSubProductTable($tableGateway);
            return $table;
        },
                'DispatchNoteSubProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Model\DispatchNoteSubProduct());
            return new TableGateway('dispatchNoteSubProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\RatingTypesTable' => function($sm) {
            $tableGateway = $sm->get('RatingTypesTableGateway');
            $table = new RatingTypesTable($tableGateway);
            return $table;
        },
                'RatingTypesTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new RatingTypes());
            return new TableGateway('ratingTypes', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CustomerRatingsTable' => function($sm) {
            $tableGateway = $sm->get('CustomerRatingsTableGateway');
            $table = new CustomerRatingsTable($tableGateway);
            return $table;
        },
                'CustomerRatingsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CustomerRatings());
            return new TableGateway('customerRatings', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceEditLogTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceEditLogTableGateway');
            $table = new Model\InvoiceEditLogTable($tableGateway);
            return $table;
        },
                'InvoiceEditLogTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Model\InvoiceEditLog());
            return new TableGateway('invoiceEditLog', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceEditLogDetailsTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceEditLogDetailsTableGateway');
            $table = new Model\InvoiceEditLogDetailsTable($tableGateway);
            return $table;
        },
                'InvoiceEditLogDetailsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Model\InvoiceEditLogDetials());
            return new TableGateway('invoiceEditLogDetails', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DeliveryNoteEditLogTable' => function($sm) {
            $tableGateway = $sm->get('DeliveryNoteEditLogTableGateway');
            $table = new DeliveryNoteEditLogTable($tableGateway);
            return $table;
        },
                'DeliveryNoteEditLogTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DeliveryNoteEditLog());
            return new TableGateway('deliveryNoteEditLog', $dbAdapter, null, $resultSetPrototype);
        },
              'Invoice\Model\DeliveryNoteEditLogDetailsTable' => function($sm) {
            $tableGateway = $sm->get('DeliveryNoteEditLogDetailsTableGateway');
            $table = new DeliveryNoteEditLogDetailsTable($tableGateway);
            return $table;
        },
                'DeliveryNoteEditLogDetailsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DeliveryNoteEditLogDetails());
            return new TableGateway('deliveryNoteEditLogDetails', $dbAdapter, null, $resultSetPrototype);
        },
            'Invoice\Model\InvoiceProductDeliveryNoteProductTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceProductDeliveryNoteProductTableGateway');
            $table = new InvoiceProductDeliveryNoteProductTable($tableGateway);
            return $table;
        },
                'InvoiceProductDeliveryNoteProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceProductDeliveryNoteProduct());
            return new TableGateway('invoiceProductDeliveryNoteProduct', $dbAdapter, null, $resultSetPrototype);
        },'Invoice\Model\MembershipTable' => function($sm) {
            $tableGateway = $sm->get('MembershipTableGateway');
            $table = new MembershipTable($tableGateway);
            return $table;
        },
                'MembershipTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Membership());
            return new TableGateway('membership', $dbAdapter, null, $resultSetPrototype);
        },
        'Invoice\Model\DraftInvoiceTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvoiceTableGateway');
            $table = new DraftInvoiceTable($tableGateway);
            return $table;
        },
                'DraftInvoiceTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvoice());
            return new TableGateway('draftInvoice', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\IncomingPaymentMethodUniformVoucherTable' => function($sm) {
            $tableGateway = $sm->get('IncomingPaymentMethodUniformVoucherTableGateway');
            $table = new IncomingPaymentMethodUniformVoucherTable($tableGateway);
            return $table;
        },
                'IncomingPaymentMethodUniformVoucherTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethodUniformVoucher());
            return new TableGateway('incomingPaymentMethodUniformVoucher', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\ContactsTable' => function($sm) {
            $tableGateway = $sm->get('ContactsTableGateway');
            $table = new ContactsTable($tableGateway);
            return $table;
        },
                'ContactsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Contacts());
            return new TableGateway('contacts', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\CustomerOrderTable' => function($sm) {
            $tableGateway = $sm->get('CustomerOrderTableGateway');
            $table = new CustomerOrderTable($tableGateway);
            return $table;
        },
                'CustomerOrderTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new CustomerOrder());
            return new TableGateway('customerOrders', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\ServiceChargeTypeTable' => function($sm) {
            $tableGateway = $sm->get('ServiceChargeTypeTableGateway');
            $table = new ServiceChargeTypeTable($tableGateway);
            return $table;
        },
                'ServiceChargeTypeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ServiceChargeType());
            return new TableGateway('serviceChargeType', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceServiceChargeTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceServiceChargeTableGateway');
            $table = new InvoiceServiceChargeTable($tableGateway);
            return $table;
        },
                'InvoiceServiceChargeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceServiceCharge());
            return new TableGateway('invoiceServiceCharge', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvServiceChargeTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvServiceChargeTableGateway');
            $table = new DraftInvServiceChargeTable($tableGateway);
            return $table;
        },
                'DraftInvServiceChargeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvServiceCharge());
            return new TableGateway('draftInvServiceCharge', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\InvoiceFreeProductTable' => function($sm) {
            $tableGateway = $sm->get('InvoiceFreeProductTableGateway');
            $table = new InvoiceFreeProductTable($tableGateway);
            return $table;
        },
                'InvoiceFreeProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoiceFreeProduct());
            return new TableGateway('salesInvoiceFreeIssueItems', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvFreeIssueItemsTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvFreeIssueItemsTableGateway');
            $table = new DraftInvFreeIssueItemsTable($tableGateway);
            return $table;
        },
                'DraftInvFreeIssueItemsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvFreeIssueItems());
            return new TableGateway('draftInvFreeIssueItems', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\ContactMobileNumbersTable' => function($sm) {
            $tableGateway = $sm->get('ContactMobileNumbersTableGateway');
            $table = new ContactMobileNumbersTable($tableGateway);
            return $table;
        },
                'ContactMobileNumbersTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ContactMobileNumbers());
            return new TableGateway('contactMobileNumbers', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\ContactEmailsTable' => function($sm) {
            $tableGateway = $sm->get('ContactEmailsTableGateway');
            $table = new ContactEmailsTable($tableGateway);
            return $table;
        },
                'ContactEmailsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ContactEmails());
            return new TableGateway('contactEmails', $dbAdapter, null, $resultSetPrototype);
        },
        'Invoice\Model\InvoicedBomSubItemsTable' => function($sm) {
            $tableGateway = $sm->get('InvoicedBomSubItemsTableGateway');
            $table = new InvoicedBomSubItemsTable($tableGateway);
            return $table;
        },
        'InvoicedBomSubItemsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InvoicedBomSubItems());
            return new TableGateway('invoicedBomSubItems', $dbAdapter, null, $resultSetPrototype);
        },
        'Invoice\Model\ContactListsTable' => function($sm) {
            $tableGateway = $sm->get('ContactListsTableGateway');
            $table = new ContactListsTable($tableGateway);
            return $table;
        },
        'ContactListsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ContactLists());
            return new TableGateway('contactLists', $dbAdapter, null, $resultSetPrototype);
        },
        'Invoice\Model\ContactListContactsTable' => function($sm) {
            $tableGateway = $sm->get('ContactListContactsTableGateway');
            $table = new ContactListContactsTable($tableGateway);
            return $table;
        },
        'ContactListContactsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ContactListContacts());
            return new TableGateway('contactListContacts', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvoiceReturnTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvoiceReturnTableGateway');
            $table = new DraftInvoiceReturnTable($tableGateway);
            return $table;
        },
                'DraftInvoiceReturnTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvoiceReturn());
            return new TableGateway('draftInvoiceReturns', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvoiceReturnProductTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvoiceReturnProductTableGateway');
            $table = new DraftInvoiceReturnProductTable($tableGateway);
            return $table;
        },
                'DraftInvoiceReturnProductTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvoiceReturnProduct());
            return new TableGateway('draftInvoiceReturnProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvoiceReturnProductTaxTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvoiceReturnProductTaxTableGateway');
            $table = new DraftInvoiceReturnProductTaxTable($tableGateway);
            return $table;
        },
                'DraftInvoiceReturnProductTaxTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvoiceReturnProductTax());
            return new TableGateway('draftInvoiceReturnProductTax', $dbAdapter, null, $resultSetPrototype);
        },
                'Invoice\Model\DraftInvoiceReturnSubProductTable' => function($sm) {
            $tableGateway = $sm->get('DraftInvoiceReturnSubProductTableGateway');
            $table = new DraftInvoiceReturnSubProductTable($tableGateway);
            return $table;
        },
                'DraftInvoiceReturnSubProductTableGateway' => function($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DraftInvoiceReturnSubProduct());
            return new TableGateway('draftInvoiceReturnSubProduct', $dbAdapter, null, $resultSetPrototype);
        }
            ),
        );
    }

}
