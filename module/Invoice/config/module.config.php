<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Invoice\Controller\SalesOrders' => 'Invoice\Controller\SalesOrdersController',
            'Invoice\Controller\API\SalesOrders' => 'Invoice\Controller\API\SalesOrdersController',
            'Invoice\Controller\QuotationController' => 'Invoice\Controller\QuotationController',
            'Invoice\Controller\API\Quotation' => 'Invoice\Controller\API\QuotationController',
            'Invoice\Controller\CustomerOrder' => 'Invoice\Controller\CustomerOrderController',
            'Invoice\Controller\API\CustomerOrder' => 'Invoice\Controller\API\CustomerOrderController',
            'Invoice\Controller\Customer' => 'Invoice\Controller\CustomerController',
            'Invoice\Controller\RestAPI\Customer' => 'Invoice\Controller\RestAPI\CustomerRestfullController',
            'Invoice\Controller\CustomerAPI' => 'Invoice\Controller\CustomerAPIController',
            'Invoice\Controller\Return' => 'Invoice\Controller\ReturnController',
            'Invoice\Controller\ReturnAPI' => 'Invoice\Controller\ReturnAPIController',
            'Invoice\Controller\CustomerPayments' => 'Invoice\Controller\CustomerPaymentsController',
            'Invoice\Controller\CustomerPaymentsAPI' => 'Invoice\Controller\CustomerPaymentsAPIController',
            'Invoice\Controller\Invoice' => 'Invoice\Controller\InvoiceController',
            'Invoice\Controller\SalesDashboard' => 'Invoice\Controller\SalesDashboardController',
            'Invoice\Controller\CrmDashboard' => 'Invoice\Controller\CrmDashboardController',
            'Invoice\Controller\API\Invoice' => 'Invoice\Controller\API\InvoiceController',
            'Invoice\Controller\API\SalesDashboard' => 'Invoice\Controller\API\SalesDashboardController',
            'Invoice\Controller\API\CrmDashboard' => 'Invoice\Controller\API\CrmDashboardController',
            'Invoice\Controller\DeliveryNote' => 'Invoice\Controller\DeliveryNoteController',
            'Invoice\Controller\DeliveryNoteAPI' => 'Invoice\Controller\DeliveryNoteAPIController',
            'Invoice\Controller\CreditNote' => 'Invoice\Controller\CreditNoteController',
            'Invoice\Controller\API\CreditNote' => 'Invoice\Controller\API\CreditNoteAPIController',
            'Invoice\Controller\CreditNotePayments' => 'Invoice\Controller\CreditNotePaymentsController',
            'Invoice\Controller\API\CreditNotePayments' => 'Invoice\Controller\API\CreditNotePaymentsAPIController',
            'Invoice\Controller\CustomerCategory' => 'Invoice\Controller\CustomerCategoryController',
            'Invoice\Controller\DispatchNote' => 'Invoice\Controller\DispatchNoteController',
            'Invoice\Controller\API\DispatchNote' => 'Invoice\Controller\API\DispatchNoteController',
            'Invoice\Controller\RatingTypes' => 'Invoice\Controller\RatingTypesController',
            'Invoice\Controller\API\RatingTypes' => 'Invoice\Controller\API\RatingTypesController',
            'Invoice\Controller\Contacts' => 'Invoice\Controller\ContactsController',
            'Invoice\Controller\API\Contacts' => 'Invoice\Controller\API\ContactsController',
            'Invoice\Controller\ServiceChargeType' => 'Invoice\Controller\ServiceChargeTypeController',
            'Invoice\Controller\API\ServiceChargeType' => 'Invoice\Controller\API\ServiceChargeTypeController',
            'Invoice\Controller\MrpSettings' => 'Invoice\Controller\MrpSettingsController',
            'Invoice\Controller\API\MrpSettings' => 'Invoice\Controller\API\MrpSettingsController',
            'Invoice\Controller\InvoiceReturn' => 'Invoice\Controller\InvoiceReturnController',
            'Invoice\Controller\API\InvoiceReturn' => 'Invoice\Controller\API\InvoiceReturnController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'invoice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/invoice[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\Invoice',
                        'action' => 'index',
                    ),
                ),
            ),
            'sales-dashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sales-dashboard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\SalesDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'crm-dashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-dashboard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\CrmDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'invoice-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/invoice-api[/:action][/:param1][/:paran2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\Invoice',
                        'action' => 'index',
                    ),
                ),
            ),
            'sales-dashboard-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sales-dashboard-api[/:action][/:param1][/:paran2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\SalesDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'crm-dashboard-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-dashboard-api[/:action][/:param1][/:paran2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\CrmDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'template' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/template[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\Template',
                        'action' => 'index',
                    ),
                ),
            ),
            'template-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/template[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller\API',
                        'controller' => 'Template',
                        'action' => 'index',
                    ),
                ),
            ),
            'salesOrders-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/salesOrders[/][:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller\API',
                        'controller' => 'SalesOrders',
                        'action' => 'create',
                    ),
                ),
            ),
            'quotation-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quotation-api[/:action]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller\API',
                        'controller' => 'Quotation',
                        'action' => 'index',
                    ),
                ),
            ),
            'salesOrders' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/salesOrders[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\SalesOrders',
                        'action' => 'create',
                    ),
                ),
            ),
            'quotation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quotation[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\QuotationController',
                        'action' => 'create',
                    ),
                ),
            ),
            'customerOrder' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/customerOrder[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\CustomerOrder',
                        'action' => 'create',
                    ),
                ),
            ),
            'customerOrder-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/customerOrder-api[/:action]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller\API',
                        'controller' => 'CustomerOrder',
                        'action' => 'index',
                    ),
                ),
            ),
            'customerCategory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/customer-category[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\CustomerCategory',
                        'action' => 'index',
                    ),
                ),
            ),
            'invoice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/invoice[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\Invoice',
                        'action' => 'index',
                    ),
                ),
            ),
            'invoiceAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/invoiceAPI[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller',
                        'controller' => 'InvoiceAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'customer' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/customer[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\Customer',
                        'action' => 'index',
                    ),
                ),
            ),
            'customerAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/customerAPI[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller',
                        'controller' => 'CustomerAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'customer-rest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/customer[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\RestAPI\Customer'
                    ),
                ),
            ),
            'contacts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contacts[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\Contacts',
                        'action' => 'create',
                    ),
                ),
            ),
            'contactsAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contactsAPI[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\Contacts',
                        'action' => 'save',
                    ),
                ),
            ),
            'service-charge-type' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-charge-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\ServiceChargeType',
                        'action' => 'index',
                    ),
                ),
            ),
            'service-charge-type-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-charge-type-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\ServiceChargeType',
                        'action' => 'save',
                    ),
                ),
            ),
            'mrp-setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/mrp-setting[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\MrpSettings',
                        'action' => 'index',
                    ),
                ),
            ),
            'mrp-setting-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/mrp-setting-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\MrpSettings',
                        'action' => 'save',
                    ),
                ),
            ),
            'invoice-return' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/invoice-return[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\InvoiceReturn',
                        'action' => 'index',
                    ),
                ),
            ),
            'return' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/return[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\Return',
                        'action' => 'create',
                    ),
                ),
            ),
            'returnAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/returnAPI[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller',
                        'controller' => 'ReturnAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'customerPayments' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/customerPayments[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\CustomerPayments',
                        'action' => 'index',
                    ),
                ),
            ),
            'customerPaymentsAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/customerPaymentsAPI[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller',
                        'controller' => 'CustomerPaymentsAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'delivery-note' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delivery-note[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller',
                        'controller' => 'DeliveryNote',
                        'action' => 'create',
                    ),
                ),
            ),
            'delivery-note-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delivery-note-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Invoice\Controller',
                        'controller' => 'DeliveryNoteAPI',
                        'action' => 'getDeliveryNoteNoForLocation',
                    ),
                ),
            ),
            'creditNote' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/credit-note[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\CreditNote',
                        'action' => 'create',
                    ),
                ),
            ),
            'creditNoteAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/credit-note-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\CreditNote',
                        'action' => 'saveCreditNoteDetails',
                    ),
                ),
            ),
            'creditNotePayments' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/credit-note-payments[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\CreditNotePayments',
                        'action' => 'create',
                    ),
                ),
            ),
            'creditNotePaymentsApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/credit-note-payments-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\CreditNotePayments',
                        'action' => 'index',
                    ),
                ),
            ),
            'dispatch-note' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dispatch-note[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\DispatchNote',
                        'action' => 'index',
                    ),
                ),
            ),
            'dispatch-note-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dispatch-note-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\DispatchNote',
                        'action' => 'index',
                    ),
                ),
            ),
            'ratingTypes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rating-types[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\RatingTypes',
                        'action' => 'index',
                    ),
                ),
            ),
            'ratingTypesApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rating-types-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Invoice\Controller\API\RatingTypes',
                        'action' => 'save',
                    ),
                ),
            ),
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'company' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
