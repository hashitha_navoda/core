<?php

namespace Inventory;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Inventory\Model\Product;
use Inventory\Model\ProductTable;
use Inventory\Model\ProductTax;
use Inventory\Model\ProductTaxTable;
use Inventory\Model\ProductSupplier;
use Inventory\Model\ProductSupplierTable;
use Inventory\Model\LocationProduct;
use Inventory\Model\LocationProductTable;
use Inventory\Model\ProductHandeling;
use Inventory\Model\ProductHandelingTable;
use Inventory\Model\Transfer;
use Inventory\Model\TransferTable;
use Inventory\Model\TransferProduct;
use Inventory\Model\TransferProductTable;
use Inventory\Model\Grn;
use Inventory\Model\GrnTable;
use Inventory\Model\Category;
use Inventory\Model\CategoryTable;
use Inventory\Model\ProductSerial;
use Inventory\Model\ProductSerialTable;
use Inventory\Model\ProductBatch;
use Inventory\Model\ProductBatchTable;
use Inventory\Model\ProductUom;
use Inventory\Model\ProductUomTable;
use Inventory\Model\GoodsIssueType;
use Inventory\Model\GoodsIssueTypeTable;
use Inventory\Model\GoodsIssue;
use Inventory\Model\GoodsIssueTable;
use Inventory\Model\GoodsIssueProduct;
use Inventory\Model\GoodsIssueProductTable;
use Inventory\Model\GrnProduct;
use Inventory\Model\GrnProductTable;
use Inventory\Model\GrnProductTax;
use Inventory\Model\GrnProductTaxTable;
use Inventory\Model\PurchaseOrder;
use Inventory\Model\PurchaseOrderTable;
use Inventory\Model\PurchaseOrderProduct;
use Inventory\Model\PurchaseOrderProductTable;
use Inventory\Model\PurchaseOrderProductTax;
use Inventory\Model\PurchaseOrderProductTaxTable;
use Inventory\Model\PurchaseInvoice;
use Inventory\Model\PurchaseInvoiceTable;
use Inventory\Model\PurchaseInvoiceProduct;
use Inventory\Model\PurchaseInvoiceProductTable;
use Inventory\Model\PurchaseInvoiceProductTax;
use Inventory\Model\PurchaseInvoiceProductTaxTable;
use Inventory\Model\SupplierPayments;
use Inventory\Model\SupplierPaymentsTable;
use Inventory\Model\SupplierInvoicePayments;
use Inventory\Model\SupplierInvoicePaymentsTable;
use Inventory\Model\SupplierPaymentMethodNumbers;
use Inventory\Model\SupplierPaymentMethodNumbersTable;
use Inventory\Model\PurchaseReturn;
use Inventory\Model\PurchaseReturnTable;
use Inventory\Model\PurchaseReturnProduct;
use Inventory\Model\PurchaseReturnProductTable;
use Inventory\Model\PurchaseReturnProductTax;
use Inventory\Model\PurchaseReturnProductTaxTable;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemInTable;
use Inventory\Model\ItemOut;
use Inventory\Model\ItemOutTable;
use Inventory\Model\ProductImage;
use Inventory\Model\ProductImageTable;
use Inventory\Model\DebitNote;
use Inventory\Model\DebitNoteTable;
use Inventory\Model\DebitNoteProduct;
use Inventory\Model\DebitNoteProductTable;
use Inventory\Model\DebitNoteSubProduct;
use Inventory\Model\DebitNoteSubProductTable;
use Inventory\Model\DebitNoteProductTax;
use Inventory\Model\DebitNoteProductTaxTable;
use Inventory\Model\DebitNotePayment;
use Inventory\Model\DebitNotePaymentTable;
use Inventory\Model\DebitNotePaymentDetails;
use Inventory\Model\DebitNotePaymentDetailsTable;
use Inventory\Model\DebitNotePaymentMethodNumbers;
use Inventory\Model\DebitNotePaymentMethodNumbersTable;
use Inventory\Model\GiftCard;
use Inventory\Model\GiftCardTable;
use Inventory\Model\PriceList;
use Inventory\Model\PriceListTable;
use Inventory\Model\PriceListLocations;
use Inventory\Model\PriceListLocationsTable;
use Inventory\Model\PriceListItems;
use Inventory\Model\PriceListItemsTable;
use Inventory\Model\Bom;
use Inventory\Model\BomTable;
use Inventory\Model\BomSubItem;
use Inventory\Model\BomSubItemTable;
use Inventory\Model\BomAssemblyDisassembly;
use Inventory\Model\BomAssemblyDisassemblyTable;
use Inventory\Model\ExpiryAlertNotification;
use Inventory\Model\ExpiryAlertNotificationTable;
use Inventory\Model\PoDraft;
use Inventory\Model\PoDraftTable;
use Inventory\Model\CompoundCostType;
use Inventory\Model\CompoundCostTypeTable;
use Inventory\Model\CompoundCostItem;
use Inventory\Model\CompoundCostItemTable;
use Inventory\Model\CompoundCostTemplate;
use Inventory\Model\CompoundCostTemplateTable;
use Inventory\Model\CompoundCostValue;
use Inventory\Model\CompoundCostValueTable;
use Inventory\Model\CompoundCostGrn;
use Inventory\Model\CompoundCostGrnTable;
use Inventory\Model\CompoundCostPurchaseInvoice;
use Inventory\Model\CompoundCostPurchaseInvoiceTable;
use Inventory\Model\PurchaseRequisition;
use Inventory\Model\PurchaseRequisitionTable;
use Inventory\Model\PurchaseRequisitionProduct;
use Inventory\Model\PurchaseRequisitionProductTable;
use Inventory\Model\PurchaseRequisitionProductTax;
use Inventory\Model\PurchaseRequisitionProductTaxTable;
use Inventory\Model\Depreciation;
use Inventory\Model\DepreciationTable;
use Inventory\Model\ItemBarcode;
use Inventory\Model\ItemBarcodeTable;
use Inventory\Model\DraftGrn;
use Inventory\Model\DraftGrnTable;
use Inventory\Model\DraftGrnProduct;
use Inventory\Model\DraftGrnProductTable;
use Inventory\Model\DraftGrnProductTax;
use Inventory\Model\DraftGrnProductTaxTable;
use Inventory\Model\DraftProductBatch;
use Inventory\Model\DraftProductBatchTable;
use Inventory\Model\DraftProductSerial;
use Inventory\Model\DraftProductSerialTable;
use Inventory\Model\FreeIssueDetails;
use Inventory\Model\FreeIssueDetailsTable;
use Inventory\Model\FreeIssueItems;
use Inventory\Model\FreeIssueItemsTable;
use Zend\Session\Container;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $this->getConfig();
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'Inventory\Controller\API\InventoryAdjustment' => 'Inventory\Controller\API\InventoryAdjustmentController',
                'PurchaseInvoiceController' => 'Inventory\Controller\API\PurchaseInvoiceController',
                'OutGoingPaymentController' => 'Inventory\Controller\API\OutGoingPaymentController',
                'ProductAPIController' => 'Inventory\Controller\ProductAPIController',
                'SupplierPaymentService' => 'Inventory\Service\SupplierPaymentService',
                'InventoryMaterialRequisitionService' => 'Inventory\Service\InventoryMaterialRequisitionService',
            ),
            'aliases' => array(
                'ProductTable' => 'Inventory\Model\ProductTable',
                'LocationProductTable' => 'Inventory\Model\LocationProductTable',
                'ItemInTable' => 'Inventory\Model\ItemInTable',
                'ItemOutTable' => 'Inventory\Model\ItemOutTable',
                'ProductUomTable' => 'Inventory\Model\ProductUomTable',
                'GiftCardTable' => 'Inventory\Model\GiftCardTable',
                'GrnTable' => 'Inventory\Model\GrnTable',
                'TransferTable' => 'Inventory\Model\TransferTable',
                'GoodsIssueTable' => 'Inventory\Model\GoodsIssueTable',
                'PurchaseInvoiceTable' => 'Inventory\Model\PurchaseInvoiceTable',
                'PurchaseReturnTable' => 'Inventory\Model\PurchaseReturnTable',
                'DebitNoteTable' => 'Inventory\Model\DebitNoteTable',
                'DebitNotePaymentTable' => 'Inventory\Model\DebitNotePaymentTable',
                'CategoryTable' => 'Inventory\Model\CategoryTable',
                'GrnProductTaxTable' => 'Inventory\Model\GrnProductTaxTable',
                'PurchaseInvoiceProductTaxTable' => 'Inventory\Model\PurchaseInvoiceProductTaxTable',
                'CompoundCostTypeTable' => 'Inventory\Model\CompoundCostTypeTable',
                'ProductTaxTable' => 'Inventory\Model\ProductTaxTable',
                'PurchaseInvoiceTable' => 'Inventory\ModelPurchaseInvoiceTable',
                'SupplierTable' => 'Inventory\Model\SupplierTable',
                'DepreciationTable' => 'Inventory\Model\DepreciationTable',
                'PurchaseOrderProductTable' => 'Inventory\Model\PurchaseOrderProductTable',
                'ProductSerialTable' => 'Inventory\Model\ProductSerialTable',
                'ProductBatchTable' => 'Inventory\Model\ProductBatchTable',
                'ItemBarcodeTable' => 'Inventory\Model\ItemBarcodeTable',
                'ProductSupplierTable' => 'Inventory\Model\ProductSupplierTable',
                'DraftGrnTable' => 'Inventory\Model\DraftGrnTable',
                'DraftGrnProductTable' => 'Inventory\Model\DraftGrnProductTable',
                'DraftGrnProductTaxTable' => 'Inventory\Model\DraftGrnProductTaxTable',
                'DraftProductBatchTable' => 'Inventory\Model\DraftProductBatchTable',
                'DraftProductSerialTable' => 'Inventory\Model\DraftProductSerialTable',
                'BomTable' => 'Inventory\Model\BomTable',
                'FreeIssueItemsTable' => 'Inventory\Model\FreeIssueItemsTable',
                'FreeIssueDetailsTable' => 'Inventory\Model\FreeIssueDetailsTable',
            ),
            
            'factories' => array(
                'Inventory\Model\ProductCategoryTable' => function($sm) {
                    $tableGateway = $sm->get('ProductCategoryTableGateway');
                    $table = new Model\ProductCategoryTable($tableGateway);
                    return $table;
                },
                'ProductCategoryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ProductCategory());
                    return new TableGateway('category', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ProductTable' => function($sm) {
                    $tableGateway = $sm->get('ProductTableGateway');
                    $table = new ProductTable($tableGateway);
                    return $table;
                },
                'ProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Product());
                    return new TableGateway('product', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\CategoryTable' => function($sm) {
                    $tableGateway = $sm->get('Inventory\Model\CategoryTableGateway');
                    $table = new CategoryTable($tableGateway);
                    return $table;
                },
                'Inventory\Model\CategoryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Category());
                    return new TableGateway('category', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('ProductTaxTableGateway');
                    $table = new ProductTaxTable($tableGateway);
                    return $table;
                },
                'ProductTaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductTax());
                    return new TableGateway('productTax', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ProductSupplierTable' => function($sm) {
                    $tableGateway = $sm->get('ProductSupplierTableGateway');
                    $table = new ProductSupplierTable($tableGateway);
                    return $table;
                },
                'ProductSupplierTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductSupplier());
                    return new TableGateway('productSupplier', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\LocationProductTable' => function($sm) {
                    $tableGateway = $sm->get('LocationProductTableGateway');
                    $table = new LocationProductTable($tableGateway);
                    return $table;
                },
                'LocationProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new LocationProduct());
                    return new TableGateway('locationProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ProductHandelingTable' => function($sm) {
                    $tableGateway = $sm->get('ProductHandelingTableGateway');
                    $table = new ProductHandelingTable($tableGateway);
                    return $table;
                },
                'ProductHandelingTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductHandeling());
                    return new TableGateway('productHandeling', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\GrnTable' => function($sm) {
                    $tableGateway = $sm->get('GrnTableGateway');
                    $table = new GrnTable($tableGateway);
                    return $table;
                },
                'GrnTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Grn());
                    return new TableGateway('grn', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\GrnProductTable' => function($sm) {
                    $tableGateway = $sm->get('GrnProductTableGateway');
                    $table = new GrnProductTable($tableGateway);
                    return $table;
                },
                'GrnProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GrnProduct());
                    return new TableGateway('grnProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\GrnProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('GrnProductTaxTableGateway');
                    $table = new GrnProductTaxTable($tableGateway);
                    return $table;
                },
                'GrnProductTaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GrnProductTax());
                    return new TableGateway('grnProductTax', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseOrderTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseOrderTableGateway');
                    $table = new PurchaseOrderTable($tableGateway);
                    return $table;
                },
                'PurchaseOrderTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseOrder());
                    return new TableGateway('purchaseOrder', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseOrderProductTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseOrderProductTableGateway');
                    $table = new PurchaseOrderProductTable($tableGateway);
                    return $table;
                },
                'PurchaseOrderProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseOrderProduct());
                    return new TableGateway('purchaseOrderProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseOrderProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseOrderProductTaxTableGateway');
                    $table = new PurchaseOrderProductTaxTable($tableGateway);
                    return $table;
                },
                'PurchaseOrderProductTaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseOrderProductTax());
                    return new TableGateway('purchaseOrderProductTax', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseInvoiceTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseInvoiceTableGateway');
                    $table = new PurchaseInvoiceTable($tableGateway);
                    return $table;
                },
                'PurchaseInvoiceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseInvoice());
                    return new TableGateway('purchaseInvoice', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseInvoiceProductTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseInvoiceProductTableGateway');
                    $table = new PurchaseInvoiceProductTable($tableGateway);
                    return $table;
                },
                'PurchaseInvoiceProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseInvoiceProduct());
                    return new TableGateway('purchaseInvoiceProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseInvoiceProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseInvoiceProductTaxTableGateway');
                    $table = new PurchaseInvoiceProductTaxTable($tableGateway);
                    return $table;
                },
                'PurchaseInvoiceProductTaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseInvoiceProductTax());
                    return new TableGateway('purchaseInvoiceProductTax', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ProductSerialTable' => function($sm) {
                    $tableGateway = $sm->get('ProductSerialTableGateway');
                    $table = new ProductSerialTable($tableGateway);
                    return $table;
                },
                'ProductSerialTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductSerial());
                    return new TableGateway('productSerial', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ProductBatchTable' => function($sm) {
                    $tableGateway = $sm->get('ProductBatchTableGateway');
                    $table = new ProductBatchTable($tableGateway);
                    return $table;
                },
                'ProductBatchTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductBatch());
                    return new TableGateway('productBatch', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ProductUomTable' => function($sm) {
                    $tableGateway = $sm->get('ProductUomTableGateway');
                    $table = new ProductUomTable($tableGateway);
                    return $table;
                },
                'ProductUomTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductUom());
                    return new TableGateway('productUom', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\GoodsIssueTypeTable' => function($sm) {
                    $tableGateway = $sm->get('GoodsIssueTypeTableGateway');
                    $table = new GoodsIssueTypeTable($tableGateway);
                    return $table;
                },
                'GoodsIssueTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GoodsIssueType());
                    return new TableGateway('goodsIssueType', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\GoodsIssueTable' => function($sm) {
                    $tableGateway = $sm->get('GoodsIssueTableGateway');
                    $table = new GoodsIssueTable($tableGateway);
                    return $table;
                },
                'GoodsIssueTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GoodsIssue());
                    return new TableGateway('goodsIssue', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\GoodsIssueProductTable' => function($sm) {
                    $tableGateway = $sm->get('GoodsIssueProductTableGateway');
                    $table = new GoodsIssueProductTable($tableGateway);
                    return $table;
                },
                'GoodsIssueProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GoodsIssueProduct());
                    return new TableGateway('goodsIssueProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\TransferTable' => function($sm) {
                    $tableGateway = $sm->get('TransferTableGateway');
                    $table = new TransferTable($tableGateway);
                    return $table;
                },
                'TransferTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Transfer());
                    return new TableGateway('transfer', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\TransferProductTable' => function($sm) {
                    $tableGateway = $sm->get('TransferProductTableGateway');
                    $table = new TransferProductTable($tableGateway);
                    return $table;
                },
                'TransferProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TransferProduct());
                    return new TableGateway('transferProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'SupplierPaymentsTable' => function($sm) {
                    $tableGateway = $sm->get('SupplierPaymentsTableGateway');
                    $table = new SupplierPaymentsTable($tableGateway);
                    return $table;
                },
                'SupplierPaymentsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SupplierPayments());
                    return new TableGateway('outgoingPayment', $dbAdapter, null, $resultSetPrototype);
                },
                'SupplierInvoicePaymentsTable' => function($sm) {
                    $tableGateway = $sm->get('SupplierInvoicePaymentsTableGateway');
                    $table = new SupplierInvoicePaymentsTable($tableGateway);
                    return $table;
                },
                'SupplierInvoicePaymentsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SupplierInvoicePayments());
                    return new TableGateway('outgoingPaymentInvoice', $dbAdapter, null, $resultSetPrototype);
                },
                'SupplierPaymentMethodNumbersTable' => function($sm) {
                    $tableGateway = $sm->get('SupplierPaymentMethodNumbersTableGateway');
                    $table = new SupplierPaymentMethodNumbersTable($tableGateway);
                    return $table;
                },
                'SupplierPaymentMethodNumbersTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SupplierPaymentMethodNumbers());
                    return new TableGateway('outGoingPaymentMethodsNumbers', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseReturnTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseReturnTableGateway');
                    $table = new PurchaseReturnTable($tableGateway);
                    return $table;
                },
                'PurchaseReturnTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseReturn());
                    return new TableGateway('purchaseReturn', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseReturnProductTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseReturnProductTableGateway');
                    $table = new PurchaseReturnProductTable($tableGateway);
                    return $table;
                },
                'PurchaseReturnProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseReturnProduct());
                    return new TableGateway('purchaseReturnProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseReturnProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseReturnProductTaxTableGateway');
                    $table = new PurchaseReturnProductTaxTable($tableGateway);
                    return $table;
                },
                'PurchaseReturnProductTaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseReturnProductTax());
                    return new TableGateway('purchaseReturnProductTax', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ItemInTable' => function($sm) {
                    $tableGateway = $sm->get('ItemInTableGateway');
                    $table = new ItemInTable($tableGateway);
                    return $table;
                },
                'ItemInTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ItemIn());
                    return new TableGateway('itemIn', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ItemOutTable' => function($sm) {
                    $tableGateway = $sm->get('ItemOutTableGateway');
                    $table = new ItemOutTable($tableGateway);
                    return $table;
                },
                'ItemOutTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ItemOut());
                    return new TableGateway('itemOut', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ProductImageTable' => function($sm) {
                    $tableGateway = $sm->get('ProductImageTableGateway');
                    $table = new ProductImageTable($tableGateway);
                    return $table;
                },
                'ProductImageTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductImage());
                    return new TableGateway('productImage', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\DebitNoteTable' => function($sm) {
                    $tableGateway = $sm->get('DebitNoteTableGateway');
                    $table = new DebitNoteTable($tableGateway);
                    return $table;
                },
                'DebitNoteTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DebitNote());
                    return new TableGateway('debitNote', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\DebitNoteProductTable' => function($sm) {
                    $tableGateway = $sm->get('DebitNoteProductTableGateway');
                    $table = new DebitNoteProductTable($tableGateway);
                    return $table;
                },
                'DebitNoteProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DebitNoteProduct());
                    return new TableGateway('debitNoteProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\DebitNoteSubProductTable' => function($sm) {
                    $tableGateway = $sm->get('DebitNoteSubProductTableGateway');
                    $table = new DebitNoteSubProductTable($tableGateway);
                    return $table;
                },
                'DebitNoteSubProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DebitNoteSubProduct());
                    return new TableGateway('debitNoteSubProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\DebitNoteProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('DebitNoteProductTaxTableGateway');
                    $table = new DebitNoteProductTaxTable($tableGateway);
                    return $table;
                },
                'DebitNoteProductTaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DebitNoteProductTax());
                    return new TableGateway('debitNoteProductTax', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\DebitNotePaymentTable' => function($sm) {
                    $tableGateway = $sm->get('DebitNotePaymentTableGateway');
                    $table = new DebitNotePaymentTable($tableGateway);
                    return $table;
                },
                'DebitNotePaymentTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DebitNotePayment());
                    return new TableGateway('debitNotePayment', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\DebitNotePaymentDetailsTable' => function($sm) {
                    $tableGateway = $sm->get('DebitNotePaymentDetailsTableGateway');
                    $table = new DebitNotePaymentDetailsTable($tableGateway);
                    return $table;
                },
                'DebitNotePaymentDetailsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DebitNotePaymentDetails());
                    return new TableGateway('debitNotePaymentDetails', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\DebitNotePaymentMethodNumbersTable' => function($sm) {
                    $tableGateway = $sm->get('DebitNotePaymentMethodNumbersTableGateway');
                    $table = new DebitNotePaymentMethodNumbersTable($tableGateway);
                    return $table;
                },
                'DebitNotePaymentMethodNumbersTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DebitNotePaymentMethodNumbers());
                    return new TableGateway('debitNotePaymentMethodNumbers', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\GiftCardTable' => function($sm) {
                    $tableGateway = $sm->get('GiftCardTableGateway');
                    $table = new GiftCardTable($tableGateway);
                    return $table;
                },
                'GiftCardTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GiftCard());
                    return new TableGateway('giftCard', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PriceListTable' => function($sm) {
                    $tableGateway = $sm->get('PriceListTableGateway');
                    $table = new PriceListTable($tableGateway);
                    return $table;
                },
                'PriceListTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PriceList());
                    return new TableGateway('priceList', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PriceListLocationsTable' => function($sm) {
                    $tableGateway = $sm->get('PriceListLocationsTableGateway');
                    $table = new PriceListLocationsTable($tableGateway);
                    return $table;
                },
                'PriceListLocationsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PriceListLocations());
                    return new TableGateway('priceListLocations', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PriceListItemsTable' => function($sm) {
                    $tableGateway = $sm->get('PriceListItemsTableGateway');
                    $table = new PriceListItemsTable($tableGateway);
                    return $table;
                },
                'PriceListItemsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PriceListItems());
                    return new TableGateway('priceListItems', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\BomTable' => function($sm) {
                    $tableGateway = $sm->get('BomTableGateway');
                    $table = new BomTable($tableGateway);
                    return $table;
                },
                'BomTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Bom());
                    return new TableGateway('bom', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\BomSubItemTable' => function($sm) {
                    $tableGateway = $sm->get('BomSubItemTableGateway');
                    $table = new BomSubItemTable($tableGateway);
                    return $table;
                },
                'BomSubItemTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new BomSubItem());
                    return new TableGateway('bomSubItem', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\BomAssemblyDisassemblyTable' => function($sm) {
                    $tableGateway = $sm->get('BomAssemblyDisassemblyTableGateway');
                    $table = new BomAssemblyDisassemblyTable($tableGateway);
                    return $table;
                },
                'BomAssemblyDisassemblyTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new BomAssemblyDisassembly());
                    return new TableGateway('bomAssemblyDisassembly', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\ExpiryAlertNotificationTable' => function($sm) {
                    $tableGateway = $sm->get('ExpiryAlertNotificationTableGateway');
                    $table = new ExpiryAlertNotificationTable($tableGateway);
                    return $table;
                },
                'ExpiryAlertNotificationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ExpiryAlertNotification());
                    return new TableGateway('expiryAlertNotification', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PoDraftTable' => function($sm) {
                    $tableGateway = $sm->get('PoDraftTableGateway');
                    $table = new PoDraftTable($tableGateway);
                    return $table;
                },
                'PoDraftTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PoDraft());
                    return new TableGateway('poDraft', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\CompoundCostTypeTable' => function($sm) {
                    $tableGateway = $sm->get('CompoundCostTypeTableGateway');
                    $table = new CompoundCostTypeTable($tableGateway);
                    return $table;
                },
                'CompoundCostTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CompoundCostItem());
                    return new TableGateway('compoundCostType', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\CompoundCostItemTable' => function($sm) {
                    $tableGateway = $sm->get('CompoundCostItemTableGateway');
                    $table = new CompoundCostItemTable($tableGateway);
                    return $table;
                },
                'CompoundCostItemTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CompoundCostTemplate());
                    return new TableGateway('compoundCostItem', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\CompoundCostTemplateTable' => function($sm) {
                    $tableGateway = $sm->get('CompoundCostTemplateTableGateway');
                    $table = new CompoundCostTemplateTable($tableGateway);
                    return $table;
                },
                'CompoundCostTemplateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CompoundCostTemplate());
                    return new TableGateway('compoundCostTemplate', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\CompoundCostGrnTable' => function($sm) {
                    $tableGateway = $sm->get('CompoundCostGrnTableGateway');
                    $table = new CompoundCostGrnTable($tableGateway);
                    return $table;
                },
                'CompoundCostGrnTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CompoundCostGrn());
                    return new TableGateway('compoundCostGrn', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\CompoundCostValueTable' => function($sm) {
                    $tableGateway = $sm->get('CompoundCostValueTableGateway');
                    $table = new CompoundCostValueTable($tableGateway);
                    return $table;
                },
                'CompoundCostValueTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CompoundCostValue());
                    return new TableGateway('compoundCostValue', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\CompoundCostPurchaseInvoiceTable' => function($sm) {
                    $tableGateway = $sm->get('CompoundCostPurchaseInvoiceTableGateway');
                    $table = new CompoundCostPurchaseInvoiceTable($tableGateway);
                    return $table;
                },
                'CompoundCostPurchaseInvoiceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CompoundCostPurchaseInvoice());
                    return new TableGateway('compoundCostPurchaseInvoice', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseRequisitionTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseRequisitionTableGateway');
                    $table = new PurchaseRequisitionTable($tableGateway);
                    return $table;
                },
                'PurchaseRequisitionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseRequisition());
                    return new TableGateway('purchaseRequisition', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseRequisitionProductTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseRequisitionProductTableGateway');
                    $table = new PurchaseRequisitionProductTable($tableGateway);
                    return $table;
                },
                'PurchaseRequisitionProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseRequisitionProduct());
                    return new TableGateway('purchaseRequisitionProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Inventory\Model\PurchaseRequisitionProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseRequisitionProductTaxTableGateway');
                    $table = new PurchaseRequisitionProductTaxTable($tableGateway);
                    return $table;
                },
                'PurchaseRequisitionProductTaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseRequisitionProductTax());
                    return new TableGateway('purchaseRequisitionProductTax', $dbAdapter, null, $resultSetPrototype);
                },
                 'Inventory\Model\DepreciationTable' => function($sm) {
                    $tableGateway = $sm->get('DepreciationTableGateway');
                    $table = new DepreciationTable($tableGateway);
                    return $table;
                },
                'DepreciationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Depreciation());
                    return new TableGateway('depreciation', $dbAdapter, null, $resultSetPrototype);
                },
                 'Inventory\Model\ItemBarcodeTable' => function($sm) {
                    $tableGateway = $sm->get('ItemBarcodeTableGateway');
                    $table = new ItemBarcodeTable($tableGateway);
                    return $table;
                },
                'ItemBarcodeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ItemBarcode());
                    return new TableGateway('itemBarcode', $dbAdapter, null, $resultSetPrototype);
                },
                 'Inventory\Model\DraftGrnTable' => function($sm) {
                    $tableGateway = $sm->get('DraftGrnTableGateway');
                    $table = new DraftGrnTable($tableGateway);
                    return $table;
                },
                'DraftGrnTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DraftGrn());
                    return new TableGateway('draftGrn', $dbAdapter, null, $resultSetPrototype);
                },
                 'Inventory\Model\DraftGrnProductTable' => function($sm) {
                    $tableGateway = $sm->get('DraftGrnProductTableGateway');
                    $table = new DraftGrnProductTable($tableGateway);
                    return $table;
                },
                'DraftGrnProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DraftGrnProduct());
                    return new TableGateway('draftGrnProduct', $dbAdapter, null, $resultSetPrototype);
                },
                 'Inventory\Model\DraftGrnProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('DraftGrnProductTaxTableGateway');
                    $table = new DraftGrnProductTaxTable($tableGateway);
                    return $table;
                },
                'DraftGrnProductTaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DraftGrnProductTax());
                    return new TableGateway('draftGrnProductTax', $dbAdapter, null, $resultSetPrototype);
                },
                 'Inventory\Model\DraftProductBatchTable' => function($sm) {
                    $tableGateway = $sm->get('DraftProductBatchTableGateway');
                    $table = new DraftProductBatchTable($tableGateway);
                    return $table;
                },
                'DraftProductBatchTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DraftProductBatch());
                    return new TableGateway('draftProductBatch', $dbAdapter, null, $resultSetPrototype);
                },
                 'Inventory\Model\DraftProductSerialTable' => function($sm) {
                    $tableGateway = $sm->get('DraftProductSerialTableGateway');
                    $table = new DraftProductSerialTable($tableGateway);
                    return $table;
                },
                'DraftProductSerialTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DraftProductSerial());
                    return new TableGateway('draftProductSerial', $dbAdapter, null, $resultSetPrototype);
                },
                 'Inventory\Model\FreeIssueDetailsTable' => function($sm) {
                    $tableGateway = $sm->get('FreeIssueDetailsTableGateway');
                    $table = new FreeIssueDetailsTable($tableGateway);
                    return $table;
                },
                'FreeIssueDetailsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new FreeIssueDetails());
                    return new TableGateway('freeIssueDetails', $dbAdapter, null, $resultSetPrototype);
                },
                 'Inventory\Model\FreeIssueItemsTable' => function($sm) {
                    $tableGateway = $sm->get('FreeIssueItemsTableGateway');
                    $table = new FreeIssueItemsTable($tableGateway);
                    return $table;
                },
                'FreeIssueItemsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new FreeIssueItems());
                    return new TableGateway('freeIssueItems', $dbAdapter, null, $resultSetPrototype);
                }
                
            )
        );
    }

}
