<?php

namespace InventoryTest\Model;

use Inventory\Model\Grn;
use PHPUnit_Framework_TestCase;

class GrnTest extends PHPUnit_Framework_TestCase
{

    //test customer  properties initially set to NULL
    public function testGrnInitialState()
    {
        $grn = new Grn();

        $this->assertNull(
                $grn->grnID, '"grnID" should initially be null'
        );
        $this->assertNull(
                $grn->grnCode, '"grnCode" should initially be null'
        );
        $this->assertNull(
                $grn->grnSupplierID, '"grnSupplierID" should initially be null'
        );
        $this->assertNull(
                $grn->grnSupplierReference, '"grnSupplierReference" should initially be null'
        );
        $this->assertNull(
                $grn->grnRetrieveLocation, '"grnRetrieveLocation" should initially be null'
        );
        $this->assertNull(
                $grn->grnDate, '"grnDate" should initially be null'
        );
        $this->assertNull(
                $grn->grnDeliveryCharge, '"grnDeliveryCharge" should initially be null'
        );
        $this->assertNull(
                $grn->grnTotal, '"grnTotal" should initially be null'
        );
        $this->assertNull(
                $grn->grnComment, '"grnComment" should initially be null'
        );
        $this->assertNull(
                $grn->grnShowTax, '"grnShowTax" should initially be null'
        );
        $this->assertNull(
                $grn->status, '"status" should initially be null'
        );
        $this->assertNull(
                $grn->entityID, '"entityID" should initially be null'
        );
    }

    //test grn’s properties be set correctly when we call exchangeArray()
    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $grn = new Grn();

        $data = array(
            'grnCode' => 'GRN001',
            'grnSupplierID' => 1,
            'grnSupplierReference' => 'supRef',
            'grnRetrieveLocation' => 'Test Location',
            'grnDate' => '2014-09-18',
            'grnDeliveryCharge' => 500.00,
            'grnTotal' => 2000.00,
            'grnComment' => 'Test Comment',
            'grnShowTax' => 1,
            'status' => 3,
            'entityID' => 1
        );

        $grn->exchangeArray($data);

        $this->assertSame(
                $data['grnCode'], $grn->grnCode, '"grnCode" was not set correctly'
        );
        $this->assertSame(
                $data['grnSupplierID'], $grn->grnSupplierID, '"grnSupplierID" was not set correctly'
        );
        $this->assertSame(
                $data['grnSupplierReference'], $grn->grnSupplierReference, '"grnSupplierReference" was not set correctly'
        );
        $this->assertSame(
                $data['grnRetrieveLocation'], $grn->grnRetrieveLocation, '"grnRetrieveLocation" was not set correctly'
        );
        $this->assertSame(
                $data['grnDate'], $grn->grnDate, '"grnDate" was not set correctly'
        );
        $this->assertSame(
                $data['grnDeliveryCharge'], $grn->grnDeliveryCharge, '"grnDeliveryCharge" was not set correctly'
        );
        $this->assertSame(
                $data['grnTotal'], $grn->grnTotal, '"grnTotal" was not set correctly'
        );
        $this->assertSame(
                $data['grnComment'], $grn->grnComment, '"grnComment" was not set correctly'
        );
        $this->assertSame(
                $data['grnShowTax'], $grn->grnShowTax, '"grnShowTax" was not set correctly'
        );
        $this->assertSame(
                $data['status'], $grn->status, '"status" was not set correctly'
        );
        $this->assertSame(
                $data['entityID'], $grn->entityID, '"entityID" was not set correctly'
        );
    }

    //test default value be used for grn properties
    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $grn = new Grn();

        $grn->exchangeArray(array(
            'grnCode' => 'GRN001',
            'grnSupplierID' => 1,
            'grnSupplierReference' => 'supRef',
            'grnRetrieveLocation' => 'Test Location',
            'grnDate' => '2014-09-18',
            'grnDeliveryCharge' => '500.00',
            'grnTotal' => 2000.00,
            'grnComment' => 'Test Comment',
            'grnShowTax' => 1,
            'status' => 3,
            'entityID' => 1
        ));
        $grn->exchangeArray(array());

        $this->assertNull(
                $grn->grnID, '"grnID" should have defaulted to null'
        );
        $this->assertNull(
                $grn->grnCode, '"grnCode" should have defaulted to null'
        );
        $this->assertNull(
                $grn->grnSupplierID, '"grnSupplierID" should have defaulted to null'
        );
        $this->assertNull(
                $grn->grnSupplierReference, '"grnSupplierReference" should have defaulted to null'
        );
        $this->assertNull(
                $grn->grnRetrieveLocation, '"grnRetrieveLocation" should have defaulted to null'
        );
        $this->assertNull(
                $grn->grnDate, '"grnDate" should have defaulted to null'
        );
        $this->assertEquals(
                0, $grn->grnDeliveryCharge, '"grnDeliveryCharge" should have defaulted to null'
        );
        $this->assertEquals(
                0, $grn->grnTotal, '"grnTotal" should have defaulted to null'
        );
        $this->assertNull(
                $grn->grnComment, '"grnComment" should have defaulted to null'
        );
        $this->assertNull(
                $grn->grnShowTax, '"grnShowTax" should have defaulted to null'
        );
        $this->assertEquals(
                3, $grn->status, '"status" should have defaulted to 3 open'
        );
        $this->assertNull(
                $grn->entityID, '"entityID" should have defaulted to null'
        );
    }

    //test whether we can get an array copy of our model
    public function testGetArrayCopyReturnsAnArrayWithPropertyValues()
    {
        $grn = new Grn();
        $data = array(
            'grnCode' => 'GRN001',
            'grnSupplierID' => 1,
            'grnSupplierReference' => 'supRef',
            'grnRetrieveLocation' => 'Test Location',
            'grnDate' => '2014-09-18',
            'grnDeliveryCharge' => 500.00,
            'grnTotal' => 2000.00,
            'grnComment' => 'Test Comment',
            'grnShowTax' => 1,
            'status' => 3,
            'entityID' => 1
        );

        $grn->exchangeArray($data);
        $copyArray = $grn->getArrayCopy();

        $this->assertSame(
                $data['grnCode'], $copyArray['grnCode'], '"grnCode" was not set correctly'
        );
        $this->assertSame(
                $data['grnSupplierID'], $copyArray['grnSupplierID'], '"grnSupplierID" was not set correctly'
        );
        $this->assertSame(
                $data['grnSupplierReference'], $copyArray['grnSupplierReference'], '"grnSupplierReference" was not set correctly'
        );
        $this->assertSame(
                $data['grnRetrieveLocation'], $copyArray['grnRetrieveLocation'], '"grnRetrieveLocation" was not set correctly'
        );
        $this->assertSame(
                $data['grnDate'], $copyArray['grnDate'], '"grnDate" was not set correctly'
        );
        $this->assertSame(
                $data['grnDeliveryCharge'], $copyArray['grnDeliveryCharge'], '"grnDeliveryCharge" was not set correctly'
        );
        $this->assertSame(
                $data['grnTotal'], $copyArray['grnTotal'], '"grnTotal" was not set correctly'
        );
        $this->assertSame(
                $data['grnComment'], $copyArray['grnComment'], '"grnComment" was not set correctly'
        );
        $this->assertSame(
                $data['grnShowTax'], $copyArray['grnShowTax'], '"grnShowTax" was not set correctly'
        );
        $this->assertSame(
                $data['status'], $copyArray['status'], '"status" was not set correctly'
        );
        $this->assertSame(
                $data['entityID'], $copyArray['entityID'], '"entityID" was not set correctly'
        );
    }

    //test input fileter set for Grn
    public function testInputFiltersAreSetCorrectly()
    {
        $grn = new Grn();

        $inputFilter = $grn->getInputFilter();

        $this->assertSame(0, $inputFilter->count());
    }

}
