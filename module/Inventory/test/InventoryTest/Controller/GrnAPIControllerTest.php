<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * this contains Grn  Api controller unit testing functions
 */

namespace Inventory\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\Container;

class GrnAPIControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
                array_merge(include DOC_ROOT . 'config/test.application.config.php', include DOC_ROOT . 'config/autoload/local.test.php')
        );

        parent::setUp();
    }

    public function setServerVariables()
    {

        $_SERVER['HTTP_HOST'] = $this->getApplicationConfig()['servername'];
        $_SERVER['SERVER_NAME'] = $this->getApplicationConfig()['servername'];
        define('ROOT_PATH', DOC_ROOT);
    }

    public function mockUserLogin()
    {
        $userSessions = new Container('ezBizUser');
        $userSessions->userID = 1;
        $userSessions->roleID = 1;
        $userSessions->timeZone = 'Asia/Colombo';
        $userSessions->wizardComplete = TRUE;
        $locationArray = array(
            'locationName' => "Test Location",
            'roleID' => 1,
            'locationID' => 1
        );
        $userSessions->userActiveLocation = $locationArray;
    }

    //test wheather GetCustomerByID action can be accessed
    public function testSaveGrnActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();

        $postData = array(
            'sID' => 1,
            'gC' => 'TLGRN0002',
            'dD' => 2014 - 09 - 23,
            'sR' => 'Test Supllier',
            'rL' => 2,
            'cm' => 'Grn Test',
            'pr' => Array
                (
                '10' => Array
                    (
                    'locationPID' => 10,
                    'pID' => 5,
                    'pCode' => 'batch1',
                    'pName' => 'Batch Product',
                    'pQuantity' => 3,
                    'pDiscount' => 3,
                    'pUnitPrice' => 300,
                    'pUom' => 1,
                    'pTotal' => 1003.95,
                    'pTax' => Array(),
                    'bProducts' => Array
                        (
                        'DFD' => Array
                            (
                            'bCode' => 'DFD',
                            'bQty' => 3,
                            'mDate' => 2014 - 09 - 23,
                            'eDate' => 2014 - 09 - 30,
                            'warnty' => ''
                        )
                    )
                )
            ),
            'dC' => 600,
            'fT' => 4429.75,
            'sT' => 1,
            'lRID' => '',
        );

        $this->dispatch('/api/grn/saveGrn', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Inventory');
        $this->assertControllerName('Inventory\Controller\API\Grn');
        $this->assertControllerClass('grncontroller');
        $this->assertMatchedRouteName('grn-api');
    }

    public function testgetGrnNoForLocationActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();
        $postData = array(
            'locationID' => 2
        );
        $this->dispatch('/api/grn/getGrnNoForLocation', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Inventory');
        $this->assertControllerName('Inventory\Controller\API\Grn');
        $this->assertControllerClass('grncontroller');
        $this->assertMatchedRouteName('grn-api');
    }

    public function testgetgetGrnsForSearchActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();
        $postData = array(
            'searchKey' => 'TLGRN0001'
        );
        $this->dispatch('/api/grn/getGrnsForSearch', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Inventory');
        $this->assertControllerName('Inventory\Controller\API\Grn');
        $this->assertControllerClass('grncontroller');
        $this->assertMatchedRouteName('grn-api');
    }

    public function testgetGrnDetailsActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();
        $postData = array(
            'grnID' => '1'
        );
        $this->dispatch('/api/grn/getGrnDetails', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Inventory');
        $this->assertControllerName('Inventory\Controller\API\Grn');
        $this->assertControllerClass('grncontroller');
        $this->assertMatchedRouteName('grn-api');
    }

    public function testchangeGrnStatusActionCanBeAccessed()
    {
        //set userSession variables
        $this->mockUserLogin();
        $postData = array(
            'grnID' => '1',
            'statusCode' => 'open'
        );
        $this->dispatch('/api/grn/changeGrnStatus', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Inventory');
        $this->assertControllerName('Inventory\Controller\API\Grn');
        $this->assertControllerClass('grncontroller');
        $this->assertMatchedRouteName('grn-api');
    }

}
