<?php

/**
 * @author Damith <damith@thinkcube.com>
 * this file contains GrnController related test functions
 */

namespace InventoryTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\Container;

class GrnControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
                array_merge(include DOC_ROOT . 'config/test.application.config.php', include DOC_ROOT . 'config/autoload/local.test.php')
        );

        parent::setUp();
    }

    public function setServerVariables()
    {

        $_SERVER['HTTP_HOST'] = $this->getApplicationConfig()['servername'];
        $_SERVER['SERVER_NAME'] = $this->getApplicationConfig()['servername'];
    }

    public function mockUserLogin()
    {
        $userSessions = new Container('ezBizUser');
        $userSessions->userID = 1;
        $userSessions->roleID = 1;
        $userSessions->timeZone = 'Asia/Colombo';
        $userSessions->wizardComplete = TRUE;
        $locationArray = array(
            'locationName' => "Test Location",
            'roleID' => 1,
            'locationID' => 1
        );
        $userSessions->userActiveLocation = $locationArray;
    }

    //test whether grn create action can be accessed
    public function testCreateActionCanBeAccessed()
    {
        //set user Session variables
        $this->mockUserLogin();

        $this->dispatch('/grn/create');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Inventory');
        $this->assertControllerName('Inventory\Controller\GrnController');
        $this->assertControllerClass('GrnController');
        $this->assertMatchedRouteName('grn');
    }

    public function testListActionCanBeAccessed()
    {
        //set user Session variables
        $this->mockUserLogin();

        $this->dispatch('/grn/list');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Inventory');
        $this->assertControllerName('Inventory\Controller\GrnController');
        $this->assertControllerClass('GrnController');
        $this->assertMatchedRouteName('grn');
    }

    public function testViewActionCanBeAccessed()
    {

        //set user Session variables
        $this->mockUserLogin();
        //Here 1 is passed as the input param1-which exists in test database
        $this->dispatch('/grn/view/1');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Inventory');
        $this->assertControllerName('Inventory\Controller\GrnController');
        $this->assertControllerClass('GrnController');
        $this->assertMatchedRouteName('grn');
    }

    public function testPreViewActionCanBeAccessed()
    {

        //set user Session variables
        $this->mockUserLogin();
        $this->setServerVariables();
        $this->dispatch('/grn/preview/1');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Inventory');
        $this->assertControllerName('Inventory\Controller\GrnController');
        $this->assertControllerClass('GrnController');
        $this->assertMatchedRouteName('grn');
    }

    /**
     * Test case for the grn document action was not written as it echo a html output
     */
}
