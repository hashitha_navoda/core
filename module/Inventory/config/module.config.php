<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Inventory\Controller\Product' => 'Inventory\Controller\ProductController',
            'Inventory\Controller\InventoryDashboard' => 'Inventory\Controller\InventoryDashboardController',
            'Inventory\Controller\PurchaseDashboard' => 'Inventory\Controller\PurchaseDashboardController',
            'Inventory\Controller\FixedAssetManagement' => 'Inventory\Controller\FixedAssetManagementController',
            'Inventory\Controller\ProductAPI' => 'Inventory\Controller\ProductAPIController',
            'Inventory\Controller\RestAPI\Product' => 'Inventory\Controller\RestAPI\ProductRestfullController',
            'Inventory\Controller\GrnController' => 'Inventory\Controller\GrnController',
            'Inventory\Controller\API\Grn' => 'Inventory\Controller\API\GrnController',
            'Inventory\Controller\API\InventoryDashboard' => 'Inventory\Controller\API\InventoryDashboardController',
            'Inventory\Controller\API\PurchaseDashboard' => 'Inventory\Controller\API\PurchaseDashboardController',
            'Inventory\Controller\PurchaseOrderController' => 'Inventory\Controller\PurchaseOrderController',
            'Inventory\Controller\PurchaseRequistionController' => 'Inventory\Controller\PurchaseRequistionController',
            'Inventory\Controller\API\PurchaseOrderController' => 'Inventory\Controller\API\PurchaseOrderController',
            'Inventory\Controller\API\PurchaseRequistionController' => 'Inventory\Controller\API\PurchaseRequistionController',
            'Inventory\Controller\PurchaseReturnsController' => 'Inventory\Controller\PurchaseReturnsController',
            'Inventory\Controller\API\PurchaseReturnsController' => 'Inventory\Controller\API\PurchaseReturnsController',
            'Inventory\Controller\API\FixedAssetManagementController' => 'Inventory\Controller\API\FixedAssetManagementController',
            'Inventory\Controller\PurchaseInvoiceController' => 'Inventory\Controller\PurchaseInvoiceController',
            'Inventory\Controller\API\PurchaseInvoiceController' => 'Inventory\Controller\API\PurchaseInvoiceController',
            'Inventory\Controller\Category' => 'Inventory\Controller\CategoryController',
            'Inventory\Controller\API\Category' => 'Inventory\Controller\API\CategoryController',
            'Inventory\Controller\InventoryAdjustment' => 'Inventory\Controller\InventoryAdjustmentController',
            'Inventory\Controller\API\InventoryAdjustment' => 'Inventory\Controller\API\InventoryAdjustmentController',
            'Inventory\Controller\Transfer' => 'Inventory\Controller\TransferController',
            'Inventory\Controller\API\Transfer' => 'Inventory\Controller\API\TransferController',
            'Inventory\Controller\OutGoingPayment' => 'Inventory\Controller\OutGoingPaymentController',
            'Inventory\Controller\API\OutGoingPayment' => 'Inventory\Controller\API\OutGoingPaymentController',
            'Inventory\Controller\Supplier' => 'Inventory\Controller\SupplierController',
            'Inventory\Controller\SupplierAPI' => 'Inventory\Controller\SupplierAPIController',
            'Inventory\Controller\DebitNote' => 'Inventory\Controller\DebitNoteController',
            'Inventory\Controller\API\DebitNote' => 'Inventory\Controller\API\DebitNoteAPIController',
            'Inventory\Controller\DebitNotePayments' => 'Inventory\Controller\DebitNotePaymentsController',
            'Inventory\Controller\API\DebitNotePayments' => 'Inventory\Controller\API\DebitNotePaymentsAPIController',
            'Inventory\Controller\GiftCard' => 'Inventory\Controller\GiftCardController',
            'Inventory\Controller\API\GiftCard' => 'Inventory\Controller\API\GiftCardController',
            'Inventory\Controller\RestAPI\GiftCard' => 'Inventory\Controller\RestAPI\GiftCardRestfullController',
            'Inventory\Controller\Bom' => 'Inventory\Controller\BomController',
            'Inventory\Controller\API\Bom' => 'Inventory\Controller\API\BomController',
            'Inventory\Controller\InventoryMaterialRequisition' => 'Inventory\Controller\InventoryMaterialRequisitionController',
            'Inventory\Controller\CompoundCosting' => 'Inventory\Controller\CompoundCostingController',
            'Inventory\Controller\RestAPI\Category' => 'Inventory\Controller\RestAPI\CategoryRestfullController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\Product',
                        'action' => 'index',
                    ),
                ),
            ),
            'inventory-dashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inventory-dashboard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\InventoryDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'inventory-dashboard-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inventory-dashboard-api[/:action][/:param1][/:paran2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\InventoryDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'purchasing-dashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchasing-dashboard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\PurchaseDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'purchasing-dashboard-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchasing-dashboard-api[/:action][/:param1][/:paran2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\PurchaseDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'fixedAssetManagement' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/fixed-asset-management[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\FixedAssetManagement',
                        'action' => 'index',
                    ),
                ),
            ),
            'productAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/productAPI[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Inventory\Controller',
                        'controller' => 'ProductAPI',
//                        'action' => 'index',
                    ),
                ),
            ),
            'product-rest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/product[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\RestAPI\Product'
                    ),
                ),
            ),
            'category-rest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/category[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\RestAPI\Category'
                    ),
                ),
            ),
            'gift-card-rest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/gift-card[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\RestAPI\GiftCard'
                    ),
                ),
            ),
            'grn' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/grn[/][:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\GrnController',
                    ),
                ),
            ),
            'grn-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/grn[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\Grn',
                        'action' => 'create',
                    ),
                ),
            ),
            'purchaseOrder' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/po[/][:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\PurchaseOrderController',
                        'action' => 'create',
                    ),
                ),
            ),
             'purchaseRequistion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchase-requistion[/][:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\PurchaseRequistionController',
                        'action' => 'create',
                    ),
                ),
            ),
            'purchaseOrder-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/po[/][:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\PurchaseOrderController',
                        'action' => 'create',
                    ),
                ),
            ),
            'purchaseRequistion-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/purchase-requistion[/][:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\PurchaseRequistionController',
                        'action' => 'create',
                    ),
                ),
            ),
            'purchaseInvoice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pi[/][:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\PurchaseInvoiceController',
                        'action' => 'create',
                    ),
                ),
            ),
            'purchaseInvoice-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/pi[/][:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\PurchaseInvoiceController',
                        'action' => 'create',
                    ),
                ),
            ),
            'purchaseReturns' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pr[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\PurchaseReturnsController',
                    ),
                ),
            ),
            'pr-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/pr[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\PurchaseReturnsController'
                    ),
                ),
            ),
            'fixedAssetManagementApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/fixed-asset-management-api[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\FixedAssetManagementController'
                    ),
                ),
            ),
            'inventoryAdjustment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inventory-adjustment[/][:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\InventoryAdjustment',
                        'action' => 'index',
                    ),
                ),
            ),
            'inventoryAdjustmentAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/inventory-adjustment[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                        'param3' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\InventoryAdjustment',
                        'action' => 'index',
                    ),
                ),
            ),
            'transfer' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transfer[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\Transfer',
                        'action' => 'create',
                    ),
                ),
            ),
            'transferAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transferAPI[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\Transfer',
                        'action' => 'saveTransferDetails',
                    ),
                ),
            ),
            'outGoingPayment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/supplierPayments[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\OutGoingPayment',
                        'action' => 'create',
                    ),
                ),
            ),
            'outGoingPaymentAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/supplierPaymentsAPI[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\OutGoingPayment',
                        'action' => 'create',
                    ),
                ),
            ),
            'supplier' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/supplier[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\Supplier',
                        'action' => 'add',
                    ),
                ),
            ),
            'supplierAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/supplierAPI[/][:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Inventory\Controller',
                        'controller' => 'SupplierAPI',
                    ),
                ),
            ),
            'debitNote' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/debit-note[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\DebitNote',
                        'action' => 'create',
                    ),
                ),
            ),
            'debitNoteAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/debit-note-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\DebitNote',
                        'action' => 'saveDebitNoteDetails',
                    ),
                ),
            ),
            'debitNotePayments' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/debit-note-payments[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\DebitNotePayments',
                        'action' => 'create',
                    ),
                ),
            ),
            'debitNotePaymentsAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/debit-note-payments-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\DebitNotePayments',
                        'action' => 'getDebitNoteDetailsByDebitNoteID',
                    ),
                ),
            ),
            'inventoryMaterialRequisition' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inventory-material-requisition[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\InventoryMaterialRequisition',
                        'action' => 'create',
                    ),
                ),
            ),
            'compoundCosting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/compound-costing[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\CompoundCosting',
                        'action' => 'create',
                    ),
                ),
            ),
            'giftCard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/gift-card[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\GiftCard',
                        'action' => 'create',
                    ),
                ),
            ),
            'giftCardAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/gift-card-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\GiftCard',
                        'action' => 'save',
                    ),
                ),
            ),
            'bom' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/bom[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\Bom',
                        'action' => 'create',
                    ),
                ),
            ),
            'bom-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/bom-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\Bom',
                        'action' => 'save',
                    ),
                ),
            ),
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'product' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
