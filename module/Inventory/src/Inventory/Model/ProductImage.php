<?php

/**
 * @author Peushani Jayasekara <peushani@thinkcube.com>
 * This file contains product Image Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductImage implements InputFilterAwareInterface
{

    public $productImageID;
    public $productImageName;
    public $productID;

    public function exchangeArray($data)
    {
        $this->productImageID = (!empty($data['productImageID'])) ? $data['productImageID'] : 0;
        $this->productImageName = (!empty($data['productImageName'])) ? $data['productImageName'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
