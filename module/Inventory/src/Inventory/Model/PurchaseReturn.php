<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the Purchase Return model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseReturn implements InputFilterAwareInterface
{

    public $purchaseReturnID;
    public $purchaseReturnCode;
    public $purchaseReturnGrnID;
    public $purchaseReturnDate;
    public $purchaseReturnComment;
    public $purchaseReturnShowTax;
    public $purchaseReturnTotal;
    public $purchaseReturnStatus;
    public $purchaseReturnEntityID;
    public $directReturnFlag;
    public $prSupplierID;
    public $prLocationID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseReturnID = (!empty($data['purchaseReturnID'])) ? $data['purchaseReturnID'] : null;
        $this->purchaseReturnCode = (!empty($data['purchaseReturnCode'])) ? $data['purchaseReturnCode'] : null;
        $this->purchaseReturnGrnID = (!empty($data['purchaseReturnGrnID'])) ? $data['purchaseReturnGrnID'] : null;
        $this->purchaseReturnDate = (!empty($data['purchaseReturnDate'])) ? $data['purchaseReturnDate'] : null;
        $this->purchaseReturnComment = (!empty($data['purchaseReturnComment'])) ? $data['purchaseReturnComment'] : null;
        $this->purchaseReturnShowTax = (!empty($data['purchaseReturnShowTax'])) ? $data['purchaseReturnShowTax'] : 0;
        $this->purchaseReturnTotal = (!empty($data['purchaseReturnTotal'])) ? $data['purchaseReturnTotal'] : 0.00;
        $this->purchaseReturnStatus = (!empty($data['purchaseReturnStatus'])) ? $data['purchaseReturnStatus'] : null;
        $this->purchaseReturnEntityID = (!empty($data['purchaseReturnEntityID'])) ? $data['purchaseReturnEntityID'] : null;
        $this->directReturnFlag = (!empty($data['directReturnFlag'])) ? $data['directReturnFlag'] : 0;
        $this->prSupplierID = (!empty($data['prSupplierID'])) ? $data['prSupplierID'] : 0;
        $this->prLocationID = (!empty($data['prLocationID'])) ? $data['prLocationID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

