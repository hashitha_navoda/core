<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Inventory\Model\ProductUom;

class ProductUomTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {

    }

    public function getAll()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productUom');
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function saveProductUom(ProductUom $ProductUom)
    {
        $productUoms = array(
            'productID' => $ProductUom->productID,
            'uomID' => $ProductUom->uomID,
            'productUomConversion' => $ProductUom->productUomConversion,
            'productUomDisplay' => $ProductUom->productUomDisplay,
            'productUomBase' => $ProductUom->productUomBase,
        );

        try {
            $this->tableGateway->insert($productUoms);
            return $this->tableGateway->lastInsertValue;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function updateProductUom(ProductUom $data)
    {
        $productUom = array(
            'productUomConversion' => $data->productUomConversion,
            'productUomDisplay' => $data->productUomDisplay
        );

        try {
            $this->tableGateway->update($productUom, array('productID' => $data->productID, 'uomID' => $data->uomID));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function deleteProductUom($productID)
    {
        $this->tableGateway->delete(array('productID' => $productID));
        return true;
    }

    public function deleteProductUomByUomID($productID, $uomID)
    {
        $this->tableGateway->delete(array('productID' => $productID, 'uomID' => $uomID));
        return true;
    }

    public function getProductUomByProductID($productID, $selectedUom)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('*'), 'left')
                ->where(array('productUom.productID' => $productID));
        if($selectedUom != null){
            $select->where(array('productUom.uomId' => $selectedUom));
        }

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    /**
     * Get product uom details
     * @param array $productIds
     * @return mixed
     */
    public function getProductUomListByProductIds($productIds = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('*'), 'left');
        if ($productIds) {
            $select->where(array('productUom.productID' => $productIds));
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        $uomList = [];
        foreach ($rowset as $row) {
            $uomList[$row['productID']][] = $row;
        }
        return $uomList;
    }

    public function getProductUomsListByProductID($productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('*'), 'left')
                ->where(array('productUom.productID' => $productID));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        $uomList = array();
        foreach ($rowset as $result) {
            $uomList[$result['uomID']] = array(
                'uA' => $result['uomAbbr'],
                'uN' => $result['uomName'],
                'uDP' => $result['uomDecimalPlace'],
                'uS' => $result['uomState'],
                'uC' => $result['productUomConversion'],
                'pUI' => $result['productUoMID'],
                'uomID' => $result['uomID'],
                'pUDisplay' => $result['productUomDisplay'],
            );
        }
        return $uomList;
    }

    public function getProductUomListByProductID($productID, $selectedUom = null)
    {
        return $this->getProductUomByProductID($productID, $selectedUom)
                        ->getResource()
                        ->fetchAll(\PDO::FETCH_ASSOC) ? : array();
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * retrun in 'productUomConversion' Assending order
     * @param type $productID
     * @return array
     */
    public function getAsendingProductUomByProductID($productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('*'), 'left')
                ->where(array('productUom.productID' => $productID))
                ->order('productUom.productUomConversion', 'ASC');

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * retrun in 'productUomConversion' Desending order
     * @param type $productID
     * @return array
     */
    public function getOrderedProductUomByProductID($productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('*'), 'left')
                ->where(array('productUom.productID' => $productID))
                ->order('productUomConversion DESC');

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param productID, uomID, qty to be converted
     * @return Float qty
     */
    public function convertProductQtyForBaseQty($productID, $uomID, $qty)
    {
        $rowObject = $this->tableGateway->select(array('productID' => $productID, 'uomID' => $uomID));
        $uomCon = $rowObject->current()->productUomConversion;
        $qtyInBaseQty = floatval($uomCon) * floatval($qty);
        return $qtyInBaseQty;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param productID, uomID
     * @return Float conversionRate
     */
    public function getProductUomConversionValue($productID, $uomID)
    {
        $rowObject = $this->tableGateway->select(array('productID' => $productID, 'uomID' => $uomID));
        $uomCon = $rowObject->current()->productUomConversion;
        return $uomCon;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param productID
     * @return int $uomID
     */
    public function getProductBaseUomID($productID)
    {
        $rowObject = $this->tableGateway->select(array('productID' => $productID, 'productUomBase' => 1));
        $uomID = $rowObject->current()->uomID;
        return $uomID;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param productID
     * @return int $uomID
     */
    public function getProductBaseUom($productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('*'), 'left')
                ->where(array('productUom.productUomBase' => 1, 'productID' => $productID));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset->current();
    }

    public function getUomAbbrByProductID()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('*'), 'left')
                ->where(array('productUom.productUomBase' => 1));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    public function getDisplayProductUomByProductID($productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('*'), 'left')
                ->where(array('productUom.productID' => $productID, 'productUom.productUomDisplay' => '1'));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    //retrived all related uom details by location product id
    public function getAllRelatedUomByLocationProductId($locationProductId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->join('locationProduct', 'productUom.productID = locationProduct.productID', array('locationProductID'), 'left')
                ->join('uom', 'productUom.uomID = uom.uomID', array('*'), 'left')
                ->where(array('locationProduct.locationProductID' => $locationProductId, 'productUomDisplay' => 1, 'uom.uomState' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

}
