<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class GoodsIssue
{

    public $goodsIssueID;
    public $goodsIssueCode;
    public $locationID;
    public $goodsIssueDate;
    public $goodsIssueReason;
    public $goodsIssueTypeID;
    public $status;
    public $entityID;
    public $goodsIssueComment;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->goodsIssueID = (!empty($data['goodsIssueID'])) ? $data['goodsIssueID'] : null;
        $this->goodsIssueCode = (!empty($data['goodsIssueCode'])) ? $data['goodsIssueCode'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->goodsIssueDate = (!empty($data['goodsIssueDate'])) ? $data['goodsIssueDate'] : null;
        $this->goodsIssueReason = (!empty($data['goodsIssueReason'])) ? $data['goodsIssueReason'] : null;
        $this->goodsIssueComment = (!empty($data['goodsIssueComment'])) ? $data['goodsIssueComment'] : null;
        $this->goodsIssueTypeID = (!empty($data['goodsIssueTypeID'])) ? $data['goodsIssueTypeID'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
