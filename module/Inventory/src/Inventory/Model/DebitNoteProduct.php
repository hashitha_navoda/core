<?php

/**
 * @author Ashan madushka <ashan@thinkcube.com>
 * This file contains debitNote product model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class DebitNoteProduct
{

    public $debitNoteProductID;
    public $debitNoteID;
    public $purchaseInvoiceProductID;
    public $productID;
    public $debitNoteProductPrice;
    public $debitNoteProductDiscount;
    public $debitNoteProductDiscountType;
    public $debitNoteProductQuantity;
    public $debitNoteProductTotal;
    public $rackID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->debitNoteProductID = (!empty($data['debitNoteProductID'])) ? $data['debitNoteProductID'] : null;
        $this->debitNoteID = (!empty($data['debitNoteID'])) ? $data['debitNoteID'] : null;
        $this->purchaseInvoiceProductID = (!empty($data['purchaseInvoiceProductID'])) ? $data['purchaseInvoiceProductID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->debitNoteProductPrice = (!empty($data['debitNoteProductPrice'])) ? $data['debitNoteProductPrice'] : 0.00;
        $this->debitNoteProductDiscount = (!empty($data['debitNoteProductDiscount'])) ? $data['debitNoteProductDiscount'] : 0.00;
        $this->debitNoteProductDiscountType = (!empty($data['debitNoteProductDiscountType'])) ? $data['debitNoteProductDiscountType'] : null;
        $this->debitNoteProductQuantity = (!empty($data['debitNoteProductQuantity'])) ? $data['debitNoteProductQuantity'] : 0;
        $this->debitNoteProductTotal = (!empty($data['debitNoteProductTotal'])) ? $data['debitNoteProductTotal'] : 0.00;
        $this->rackID = (!empty($data['rackID'])) ? $data['rackID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
