<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Unit of Measure Table Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Transfer
{

    public $transferID;
    public $transferCode;
    public $locationIDIn;
    public $locationIDOut;
    public $transferDate;
    public $transferDescription;
    public $transferStatus;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->transferID = (!empty($data['transferID'])) ? $data['transferID'] : 0;
        $this->transferCode = (!empty($data['transferCode'])) ? $data['transferCode'] : null;
        $this->locationIDIn = (!empty($data['locationIDIn'])) ? $data['locationIDIn'] : 0;
        $this->locationIDOut = (!empty($data['locationIDOut'])) ? $data['locationIDOut'] : 0;
        $this->transferDate = (!empty($data['transferDate'])) ? $data['transferDate'] : null;
        $this->transferDescription = (!empty($data['transferDescription'])) ? $data['transferDescription'] : null;
        $this->transferStatus = (!empty($data['transferStatus'])) ? $data['transferStatus'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'transferID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'transferCode',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 45,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationIDIn',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'transferDate',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'digits'
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationIDOut',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'digits'
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'transferDescription',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'digits'
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
