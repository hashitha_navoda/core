<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains debit note payment Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DebitNotePaymentTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new \Zend\Db\Sql\Select;
            $select->from('debitNotePayment')
                    ->columns(array('*'))
                    ->order(array('debitNotePaymentDate' => 'DESC'))
                    ->JOIN('entity', 'debitNotePayment.entityID= entity.entityID ', array('deleted'), 'left')
                    ->JOIN('supplier', 'supplier.supplierID= debitNotePayment.supplierID ', array('supplierName', 'supplierCode'), 'left')
                    ->where(array('locationID' => $locationID/** , 'deleted' => 0* */));
            return new \Zend\Paginator\Paginator(
                    new \Zend\Paginator\Adapter\DbSelect(
                    $select, $this->tableGateway->getAdapter()
                    )
            );
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveDebitNotePayment($debitNotePayment)
    {
        $data = array(
            'debitNotePaymentID' => $debitNotePayment->debitNotePaymentID,
            'debitNotePaymentCode' => $debitNotePayment->debitNotePaymentCode,
            'debitNotePaymentDate' => $debitNotePayment->debitNotePaymentDate,
            'paymentTermID' => $debitNotePayment->paymentTermID,
            'debitNotePaymentAmount' => $debitNotePayment->debitNotePaymentAmount,
            'supplierID' => $debitNotePayment->supplierID,
            'debitNotePaymentDiscount' => $debitNotePayment->debitNotePaymentDiscount,
            'debitNotePaymentMemo' => $debitNotePayment->debitNotePaymentMemo,
            'locationID' => $debitNotePayment->locationID,
            'statusID' => $debitNotePayment->statusID,
            'entityID' => $debitNotePayment->entityID,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDebitNotePaymentsByLocatioID($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment')
                ->columns(array("*"))
                ->join('entity', 'debitNotePayment.entityID = entity.entityID', array("deleted"), "left")
                ->where(array('debitNotePayment.locationID' => $locationID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getDebitNotePaymentsByLocatioIDWithDeleted($locationID, $searchKey = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment')
                ->columns(array("*"))
                ->where(array('debitNotePayment.locationID' => $locationID));
        if ($searchKey) {
            $select->where->like('debitNotePaymentCode', '%' . $searchKey . '%');
            $select->limit(50);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getDebitNotePaymentsBySupplierIDAndLocationID($supplierID, $locationID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNotePayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNotePayment.entityID = e.entityID")
                    ->join('supplier', "debitNotePayment.supplierID = supplier.supplierID")
//                    ->where(array('e.deleted' => '0'))
                    ->where(array('debitNotePayment.supplierID' => $supplierID, 'debitNotePayment.locationID' => $locationID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getDebitNotePaymentByDebitNotePaymentID($debitNotePaymentID, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNotePayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNotePayment.entityID = e.entityID")
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->join('supplier', "debitNotePayment.supplierID = supplier.supplierID", array('supplierCode', 'supplierName'))
                    ->where(array('debitNotePayment.debitNotePaymentID' => $debitNotePaymentID));
            if ($flag == NULL) {
                $select->where(array('e.deleted' => '0'));
            }
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getDebitNotePaymentByDebitNotePaymentIDForRec($debitNotePaymentID, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNotePayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNotePayment.entityID = e.entityID")
                    ->join('debitNotePaymentMethodNumbers', 'debitNotePayment.debitNotePaymentID = debitNotePaymentMethodNumbers.debitNotePaymentID', ['*'], 'left')
                    ->join('debitNotePaymentDetails', 'debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID', ['*'], 'left')
                    ->where(array('debitNotePayment.debitNotePaymentID' => $debitNotePaymentID));
                $select->where(array('e.deleted' => '0'));
            
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getDebitNotePaymentByDebitNotePaymentIDForJE($debitNotePaymentID, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNotePayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNotePayment.entityID = e.entityID")
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->join('supplier', "debitNotePayment.supplierID = supplier.supplierID", array('supplierCode', 'supplierName'))
                    ->where(array('debitNotePayment.debitNotePaymentID' => $debitNotePaymentID));
            $select->where->notEqualTo('debitNotePayment.statusID', 5);
            $select->where->notEqualTo('debitNotePayment.statusID', 10);
            if ($flag == NULL) {
                $select->where(array('e.deleted' => '0'));
            }
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getDebitNotePaymentByDebitNotePaymentCode($id)
    {
        $rowset = $this->tableGateway->select(array('debitNotePaymentCode' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getDebitNotePaymentsByDate($fromdate, $todate, $supplierID = NULL, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment')
                ->columns(array('*'))
                ->join('entity', 'debitNotePayment.entityID = entity.entityID', array('deleted'), 'left')
                ->join('supplier', 'supplier.supplierID = debitNotePayment.supplierID', array('supplierCode', 'supplierName'), 'left')
                ->where(array('debitNotePaymentDate >= ?' => $fromdate))
                ->where(array('debitNotePaymentDate <= ?' => $todate, 'locationID' => $locationID/** ,'deleted' => 0* */));
        if ($supplierID) {
            $select->where(array('debitNotePayment.supplierID' => $supplierID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function update($data, $paymentID)
    {
        return $this->tableGateway->update($data, array('debitNotePaymentID' => $paymentID));
    }

    /**
     * Get Location wise cash payments
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @return type
     */
    public function getCashDebitNotePaymentsByLocation($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment')
                ->columns(['*'])
                ->join('entity', 'debitNotePayment.entityID = entity.entityID', ['deleted'], "left")
                ->join('debitNotePaymentDetails', 'debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID', ['debitNotePaymentID', 'paymentMethodID', 'debitNotePaymentDetailsID', 'debitNotePaymentDetailsAmount'], 'left')
                ->where(['debitNotePayment.locationID' => $locationID, 'deleted' => 0, 'debitNotePaymentDetails.paymentMethodID' => '1']);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
    * get all debit note payment details by given supplier IDs
    *
    **/
    public function getDebitNotePaymentsBySupplierIDs($supplierID = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNotePayment')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "debitNotePayment.entityID = e.entityID", array('deleted','createdTimeStamp'))
                ->join('supplier', "debitNotePayment.supplierID = supplier.supplierID", array('*'), 'left')
                ->join("status", "debitNotePayment.statusID = status.statusID", array("*"), "left")
                ->join("debitNotePaymentDetails", "debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID", array("debitNoteID"), "left")
                ->where(array('e.deleted' => '0'));
        $select->where->in('debitNotePayment.supplierID', $supplierID);
        $select->where->in('debitNotePayment.statusID', $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('debitNotePayment.debitNotePaymentDate', $fromdate, $todate);
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
        
    }

    public function getDebitNotePaymentDetailsByPoId($poId = null, $grnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment');
        $select->columns(array('*'));
        $select->join('debitNotePaymentDetails','debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID',array('*'),'left');
        $select->join('debitNote','debitNotePaymentDetails.debitNoteID = debitNote.debitNoteID',array('*'),'left');
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('entity','debitNotePayment.entityID = entity.entityID',array('*'),'left');
        if (!is_null($poId)) {
            $select->where(array('purchaseInvoice.purchaseInvoicePoID' => $poId));
        }
        if (!is_null($grnID)) {
            $select->where(array('purchaseInvoice.purchaseInvoiceGrnID' => $grnID));
        }
        $select->group("debitNotePayment.debitNotePaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }
    
    public function getDebitNotePaymentDetailsByCopiedPoId($poId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment');
        $select->columns(array('*'));
        $select->join('debitNotePaymentDetails','debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID',array('*'),'left');
        $select->join('debitNote','debitNotePaymentDetails.debitNoteID = debitNote.debitNoteID',array('*'),'left');
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('grnProduct','purchaseInvoice.purchaseInvoiceGrnID = grnProduct.grnID',array('grnID'),'left');
        $select->join('entity','debitNotePayment.entityID = entity.entityID',array('*'),'left');
        $select->where(array('grnProduct.grnProductDocumentId' => $poId, 'grnProduct.grnProductDocumentTypeId' => 9));
        $select->group("debitNotePayment.debitNotePaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    // get debit note payment details relatde to pi
    public function getDebitNotePaymentDetailsByPiId($piId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment');
        $select->columns(array('*'));
        $select->join('debitNotePaymentDetails','debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID',array('*'),'left');
        $select->join('debitNote','debitNotePaymentDetails.debitNoteID = debitNote.debitNoteID',array('*'),'left');
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('entity','debitNotePayment.entityID = entity.entityID',array('*'),'left');
        $select->where(array('purchaseInvoice.purchaseInvoiceID' => $piId));
        $select->group("debitNotePayment.debitNotePaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getDebitNotePaymentDetailsThroughPOByPrId($prId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment');
        $select->columns(array('*'));
        $select->join('debitNotePaymentDetails','debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID',array('*'),'left');
        $select->join('debitNote','debitNotePaymentDetails.debitNoteID = debitNote.debitNoteID',array('*'),'left');
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('purchaseOrder','purchaseInvoice.purchaseInvoicePoID = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('grnProduct','grnProduct.grnProductDocumentId = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('purchaseReturn','grnProduct.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
        $select->join('entity', 'debitNotePayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseReturn.purchaseReturnID' => $prId,'grnProduct.grnProductDocumentTypeId' => 9));
        $select->group("debitNotePayment.debitNotePaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getDebitNotePaymentDetailsThroughGrnByPrId($prId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment');
        $select->columns(array('*'));
        $select->join('debitNotePaymentDetails','debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID',array('*'),'left');
        $select->join('debitNote','debitNotePaymentDetails.debitNoteID = debitNote.debitNoteID',array('*'),'left');
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('grn','grn.grnID = purchaseInvoice.purchaseInvoiceGrnID',array('*'),'left');
        $select->join('purchaseReturn','grn.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
        $select->join('entity', 'debitNotePayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseReturn.purchaseReturnID' => $prId));
        $select->group("debitNotePayment.debitNotePaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getDebitNotePaymentDetailsByDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePayment');
        $select->columns(array('*'));
        $select->join('debitNotePaymentDetails','debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID',array('*'),'left');
        $select->join('debitNote','debitNotePaymentDetails.debitNoteID = debitNote.debitNoteID',array('*'),'left');
        $select->join('entity', 'debitNotePayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('debitNote.debitNoteID' => $debitNoteID));
        $select->group("debitNotePayment.debitNotePaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getDebitNotePaymentsBySupplierIDsForSupplierBalance($supplierID = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNotePayment')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "debitNotePayment.entityID = e.entityID", array('deleted','createdTimeStamp'))
                ->join('supplier', "debitNotePayment.supplierID = supplier.supplierID", array('*'), 'left')
                ->join("status", "debitNotePayment.statusID = status.statusID", array("*"), "left")
                ->join("debitNotePaymentDetails", "debitNotePayment.debitNotePaymentID = debitNotePaymentDetails.debitNotePaymentID", array("debitNoteID"), "left")
                ->where(array('e.deleted' => '0'));
        $select->where->in('debitNotePayment.supplierID', $supplierID);
        $select->where->in('debitNotePayment.statusID', $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('e.createdTimeStamp', $fromdate, $todate);
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
        
    }
}
