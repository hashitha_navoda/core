<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PurchaseRequisitionProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePurchaseRequisitionProduct(PurchaseRequisitionProduct $poProduct)
    {
        $data = array(
            'purchaseRequisitionID' => $poProduct->purchaseRequisitionID,
            'locationProductID' => $poProduct->locationProductID,
            'purchaseRequisitionProductQuantity' => $poProduct->purchaseRequisitionProductQuantity,
            'purchaseRequisitionProductPrice' => $poProduct->purchaseRequisitionProductPrice,
            'purchaseRequisitionProductDiscount' => $poProduct->purchaseRequisitionProductDiscount,
            'purchaseRequisitionProductTotal' => $poProduct->purchaseRequisitionProductTotal,
            'copied' => $poProduct->copied,
            'purchaseRequisitionProductCopiedQuantity' => $poProduct->purchaseRequisitionProductCopiedQuantity
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function updateCopiedPrProducts($locationPID, $prID)
    {
        $data = array(
            'copied' => 1
        );
        $this->tableGateway->update($data, array('locationProductID' => $locationPID, 'purchaseRequisitionID' => $prID));
    }

    public function updateCopiedPrProductQty($locationPID, $prID, $productQty)
    {
        $data = array(
            'purchaseRequisitionProductCopiedQuantity' => $productQty
        );
        $this->tableGateway->update($data, array('locationProductID' => $locationPID, 'purchaseRequisitionID' => $prID));
    }

    public function getUnCopiedPrProducts($prID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionProduct');
        $select->where(array('purchaseRequisitionID' => $prID));
        $select->where(array('copied' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getPrProduct($prId, $locationProductId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionProduct');
        $select->where(array('purchaseRequisitionID' => $prId));
        $select->where(array('locationProductID' => $locationProductId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    public function CheckPurchaseRequisitionProductByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionProduct')
                ->columns(array('*'))
                ->join('purchaseRequisition', 'purchaseRequisition.purchaseRequisitionID = purchaseRequisitionProduct.purchaseRequisitionID', array('entityID'), 'left')
                ->join('entity', 'entity.entityID = purchaseRequisition.entityID', array('deleted'), 'left')
                ->where(array('locationProductID' => $locationProductID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function getPurchaseRequisitionProductQtyByProductID($fromDate = null, $toDate, $moreRecords = null, $productID = null, $locationID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionProduct')
                ->columns(array('totalQty' => new Expression('SUM(purchaseRequisitionProduct.purchaseRequisitionProductQuantity)'),
                        'totalAssign' => new Expression('SUM(purchaseRequisitionProduct.purchaseRequisitionProductCopiedQuantity)')))
                ->join('locationProduct', 'purchaseRequisitionProduct.locationProductID = locationProduct.locationProductID'
                        , array('locationProductID'), 'left')
                ->join('purchaseRequisition', 'purchaseRequisition.purchaseRequisitionID = purchaseRequisitionProduct.purchaseRequisitionID', 
                        array('purchaseRequisitionRetrieveLocation'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID'), 'left');
        $select->join('entity', 'product.entityID = entity.entityID', array('deleted'));
        $select->group(array('product.productID'));
        $select->where(array('purchaseRequisition.status' => 3, 'purchaseRequisitionProduct.copied' => 0, 'entity.deleted' => 0));
        $select->where('product.productState','=',1);
        if($productID){
            $select->where(array('product.productID' => $productID));
        }
        if($locationID){
            $select->where->in('purchaseRequisition.purchaseRequisitionRetrieveLocation', $locationID);
        }
        if($moreRecords){
            $select->where->greaterThan('purchaseRequisition.purchaseRequisitionExpDelDate', $toDate);
        } else{
            $select->where->between('purchaseRequisition.purchaseRequisitionExpDelDate', $fromDate, $toDate);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function deleteDraftRecordsByPoID($poID)
    {
        try {
            $result = $this->tableGateway->delete(array('purchaseRequisitionID' => $poID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function updatePoCopiedStateAndCopiedQty($updatedDetaSet = [])
    {
        return $this->tableGateway->update($updatedDetaSet, array('purchaseRequisitionProductID' => $updatedDetaSet['purchaseRequisitionProductID']));
    }

}
