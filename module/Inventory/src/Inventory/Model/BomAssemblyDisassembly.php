<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BomAssemblyDisassembly
{

    public $bomAssemblyDisassemblyID;
    public $bomAssemblyDisassemblyType;
    public $bomAssemblyDisassemblyReference;
    public $bomID;
    public $bomAssemblyDisassemblyParentQuantity;
    public $bomAssemblyDisassemblyComment;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->bomAssemblyDisassemblyID = (!empty($data['bomAssemblyDisassemblyID'])) ? $data['bomAssemblyDisassemblyID'] : null;
        $this->bomAssemblyDisassemblyType = (!empty($data['bomAssemblyDisassemblyType'])) ? $data['bomAssemblyDisassemblyType'] : null;
        $this->bomAssemblyDisassemblyReference = (!empty($data['bomAssemblyDisassemblyReference'])) ? $data['bomAssemblyDisassemblyReference'] : null;
        $this->bomID = (!empty($data['bomID'])) ? $data['bomID'] : null;
        $this->bomAssemblyDisassemblyParentQuantity = (!empty($data['bomAssemblyDisassemblyParentQuantity'])) ? $data['bomAssemblyDisassemblyParentQuantity'] : null;
        $this->bomAssemblyDisassemblyComment = (!empty($data['bomAssemblyDisassemblyComment'])) ? $data['bomAssemblyDisassemblyComment'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
