<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Bom
{

    public $bomID;
    public $bomCode;
    public $bomName;
    public $bomTotalCost;
    public $productID;
    public $bomDiscription;
    public $isJEPostSubItemWise;
    public $bomItemType;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->bomID = (!empty($data['bomID'])) ? $data['bomID'] : null;
        $this->bomCode = (!empty($data['bomCode'])) ? $data['bomCode'] : null;
        $this->bomTotalCost = (!empty($data['bomTotalCost'])) ? $data['bomTotalCost'] : 0;
        $this->bomDiscription = (!empty($data['bomDiscription'])) ? $data['bomDiscription'] : null;
        $this->isJEPostSubItemWise = (!empty($data['isJEPostSubItemWise'])) ? $data['isJEPostSubItemWise'] : 0;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->bomItemType = (!empty($data['bomItemType'])) ? $data['bomItemType'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
