<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Invoice Payments Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SupplierInvoicePayments implements InputFilterAwareInterface
{

    public $outgoingPaymentInvoiceID;
    public $paymentID;
    public $invoiceID;
    public $paymentVoucherId;
    public $outgoingInvoiceCashAmount;
    public $outgoingInvoiceCreditAmount;
    public $paymentMethodID;
    public $outgoingPaymentInvoiceCurrencyGainAndLossType;
    public $outgoingPaymentInvoiceGainAndLossAmount;
    protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->outgoingPaymentInvoiceID = (!empty($data['outgoingPaymentInvoiceID'])) ? $data['outgoingPaymentInvoiceID'] : null;
        $this->paymentID = (!empty($data['paymentID'])) ? $data['paymentID'] : null;
        $this->invoiceID = (!empty($data['invoiceID'])) ? $data['invoiceID'] : null;
        $this->paymentVoucherId = (!empty($data['paymentVoucherId'])) ? $data['paymentVoucherId'] : null;
        $this->outgoingInvoiceCashAmount = (!empty($data['outgoingInvoiceCashAmount'])) ? $data['outgoingInvoiceCashAmount'] : 0.00;
        $this->outgoingInvoiceCreditAmount = (!empty($data['outgoingInvoiceCreditAmount'])) ? $data['outgoingInvoiceCreditAmount'] : 0.00;
        $this->paymentMethodID = (!empty($data['paymentMethodID'])) ? $data['paymentMethodID'] : null;
        $this->outgoingPaymentInvoiceCurrencyGainAndLossType = (!empty($data['outgoingPaymentInvoiceCurrencyGainAndLossType'])) ? $data['outgoingPaymentInvoiceCurrencyGainAndLossType'] : null;
        $this->outgoingPaymentInvoiceGainAndLossAmount = (!empty($data['outgoingPaymentInvoiceGainAndLossAmount'])) ? $data['outgoingPaymentInvoiceGainAndLossAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

// Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
