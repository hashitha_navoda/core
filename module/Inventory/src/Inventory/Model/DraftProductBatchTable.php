<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DraftProductBatchTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function getBatchProducts($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('productBatchID' => $id));

        return $rowset->current();
    }

    public function getBatchProductsByCodeAndLocationProductId($cId, $locationProductID, $qty)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productBatch')
                ->where(array('locationProductID' => $locationProductID, 'productBatchCode' => $cId))
                ->where->greaterThanOrEqualTo('productBatchQuantity', $qty);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getBatchProductsByLocIdCode($cId, $locProID)
    {
        $rowset = $this->tableGateway->select(array('productBatchID' => $cId, 'locationProductID' => $locProID));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $cId");
        }
        return $row;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * Batch Product Save Function
     * @return  Inserted BatchProduct ID
     */
    public function saveBatchProduct($productBatch)
    {
        $data = array(
            'locationProductID' => $productBatch->locationProductID,
            'productBatchCode' => $productBatch->productBatchCode,
            'productBatchExpiryDate' => $productBatch->productBatchExpiryDate,
            'productBatchWarrantyPeriod' => $productBatch->productBatchWarrantyPeriod,
            'productBatchManufactureDate' => $productBatch->productBatchManufactureDate,
            'productBatchQuantity' => $productBatch->productBatchQuantity,
            'productBatchPrice' => $productBatch->productBatchPrice
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function addBatchProducts($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('productBatchID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getBatchProductFromLocationProductIdAndProductBatchCode($locationProductID, $productBatchCode)
    {
        $locationProductID = (int) $locationProductID;
        $rowset = $this->tableGateway->select(array('locationProductID' => $locationProductID, 'productBatchCode' => $productBatchCode));
        $row = $rowset->current();
        return $row;
    }

    public function getBatchProductsByLocProductId($pId)
    {

        $pId = (int) $pId;
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productBatch')
                ->where(array('locationProductID' => $pId))
        ->where->greaterThanOrEqualTo('productBatchQuantity', 0);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 0) {
            return 0;
        } else {
            return $results;
        }
    }

    public function updateBatch($bCode, $data)
    {
        $row = $this->tableGateway->update($data, array('productBatchCode' => $bCode));
        return $row;
    }

    public function updateBatchByID($id, $data)
    {
        $row = $this->tableGateway->update($data, array('productBatchID' => $id));
        return $row;
    }

    public function updateProductBatchQuantity($bId, $data)
    {
        return $this->tableGateway->update($data, array('productBatchID' => $bId));
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $locPId
     * @return type
     */
    public function getBatchProductsByLocID($locPId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productBatch')
                ->columns(array('lpID' => new Expression('locationProductID'),
                    'pbID' => new Expression('productBatchID'),
                    'pbCD' => new Expression('productBatchCode'),
                    'bQty' => new Expression('productBatchQuantity')))
                ->where(array('productBatch.locationProductID' => $locPId))
        ->where->greaterThan('productBatchQuantity', 0);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 0) {
            return 0;
        } else {
            foreach ($results as $val) {
                $countArray[] = $val;
            }

            return $countArray;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $locPId
     * @return type
     */
    public function getBatchIdList($locPId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productBatch')
                ->columns(array('lpID' => new Expression('locationProductID'),
                    'pbID' => new Expression('productBatchID'),
                    'pbCD' => new Expression('productBatchCode')))
                ->where(array('productBatch.locationProductID' => $locPId));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
    * get All batch codes that related to the given keywrd
    **/
    public function searchBatchProductsForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productBatch');
        $select->columns(array('productBatchCode'));
        $select->where->like('productBatchCode', '%' . $searchKey . '%');
        $select->limit(10);
        $select->group(array('productBatchCode'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * get batchProduct Details that given BatchCode
    *
    **/
    public function getBatchProductDetailsByCode($cId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productBatch');
        $select->columns(array('*'));
        $select->where(array('productBatchCode' => $cId));
        $select->order(array('productBatch.productBatchID DESC'));
        $select->limit(1);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        return $results;
    }

}
