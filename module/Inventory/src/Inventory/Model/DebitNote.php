<?php

/**
 * @author Ashan madushka <ashan@thinkcube.com>
 * This file contains debitNote model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class DebitNote
{

    public $debitNoteID;
    public $debitNoteCode;
    public $purchaseInvoiceID;
    public $debitNoteComment;
    public $debitNoteDate;
    public $supplierID;
    public $statusID;
    public $debitNoteTotal;
    public $locationID;
    public $paymentTermID;
    public $debitNotePaymentEligible;
    public $debitNotePaymentAmount;
    public $debitNoteSettledAmount;
    public $entityID;
    public $directDebitNoteFlag;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->debitNoteID = (!empty($data['debitNoteID'])) ? $data['debitNoteID'] : null;
        $this->debitNoteCode = (!empty($data['debitNoteCode'])) ? $data['debitNoteCode'] : null;
        $this->purchaseInvoiceID = (!empty($data['purchaseInvoiceID'])) ? $data['purchaseInvoiceID'] : null;
        $this->debitNoteComment = (!empty($data['debitNoteComment'])) ? $data['debitNoteComment'] : null;
        $this->debitNoteDate = (!empty($data['debitNoteDate'])) ? $data['debitNoteDate'] : null;
        $this->supplierID = (!empty($data['supplierID'])) ? $data['supplierID'] : null;
        $this->statusID = (!empty($data['statusID'])) ? $data['statusID'] : null;
        $this->debitNoteTotal = (!empty($data['debitNoteTotal'])) ? $data['debitNoteTotal'] : 0.00;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->paymentTermID = (!empty($data['paymentTermID'])) ? $data['paymentTermID'] : null;
        $this->debitNotePaymentEligible = (!empty($data['debitNotePaymentEligible'])) ? $data['debitNotePaymentEligible'] : null;
        $this->debitNotePaymentAmount = (!empty($data['debitNotePaymentAmount'])) ? $data['debitNotePaymentAmount'] : 0.00;
        $this->debitNoteSettledAmount = (!empty($data['debitNoteSettledAmount'])) ? $data['debitNoteSettledAmount'] : 0.00;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->directDebitNoteFlag = (!empty($data['directDebitNoteFlag'])) ? $data['directDebitNoteFlag'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
