<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Invoice Payments Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class SupplierInvoicePaymentsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveInvoicePayment(invoicepayments $invoicepayment)
    {
        $data = array(
            'incomingPaymentID' => $invoicepayment->paymentID,
            'salesInvoiceID' => $invoicepayment->invoiceID,
            'incomingInvoiceCashAmount' => $invoicepayment->amount,
            'incomingInvoiceCreditAmount' => $invoicepayment->creditAmount,
            'paymentMethodID' => $invoicepayment->paymentMethod,
        );
        $this->tableGateway->insert($data);
    }

    public function getAllSupplierInvoiceDetailsByPaymentID($payId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice');
        $select->join(
                'purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array(
            '*',
            'left_to_pay' => new Expression("`purchaseInvoiceTotal` -  `purchaseInvoicePayedAmount`")
                ), 'left'
        );
        $select->join(
                'paymentVoucher', 'outgoingPaymentInvoice.paymentVoucherId = paymentVoucher.paymentVoucherID', array(
            '*',
            'left_to_pay_pv' => new Expression("`paymentVoucherTotal` -  `paymentVoucherPaidAmount`")
                ), 'left'
        );
        $select->where(array('outgoingPaymentInvoice.paymentID = ?' => $payId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getInvoiceByPaymentID($payId)
    {
//        $rowset = $this->tableGateway->select(array('paymentID' => $payId));

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice');
        $select->join(
                'purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID ', array(
            '*',
            'left_to_pay' => new Expression("`purchaseInvoiceTotal` -  `purchaseInvoicePayedAmount`")
                ), 'left'
        );
        $select->join(
                'paymentVoucher', 'outgoingPaymentInvoice.paymentVoucherId = paymentVoucher.paymentVoucherID', array(
            '*',
            'left_to_pay_pv' => new Expression("`paymentVoucherTotal` -  `paymentVoucherPaidAmount`")
                ), 'left'
        );
        $select->where(array('outgoingPaymentInvoice.paymentID = ?' => $payId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPVDataByPaymentID($payId)
    {
//        $rowset = $this->tableGateway->select(array('paymentID' => $payId));

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice');
        $select->join(
                'paymentVoucher', 'outgoingPaymentInvoice.paymentVoucherId = paymentVoucher.paymentVoucherID', array(
            '*',
            'left_to_pay_pv' => new Expression("`paymentVoucherTotal` -  `paymentVoucherPaidAmount`")
                ), 'left'
        );
        $select->where->notEqualTo('outgoingPaymentInvoice.paymentVoucherId',0);
        $select->where(array('outgoingPaymentInvoice.paymentID = ?' => $payId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPIDataByPaymentID($payId)
    {
//        $rowset = $this->tableGateway->select(array('paymentID' => $payId));

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice');
        $select->join(
                'purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID ', array(
            '*',
            'left_to_pay' => new Expression("`purchaseInvoiceTotal` -  `purchaseInvoicePayedAmount`")
                ), 'left'
        );
        $select->where->notEqualTo('invoiceID',0);
        $select->where(array('outgoingPaymentInvoice.paymentID = ?' => $payId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }




    /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     */
    public function getPaymentsDetailsByPaymentVoucherID($pvID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPaymentInvoice.paymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('*'),'left')
                ->join('account', 'outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID = account.accountId', array('*'))
                ->join('bank', 'account.bankId = bank.bankId', array('*'))
                ->where(array('outgoingPaymentInvoice.paymentVoucherId' => $pvID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

     /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     */
    public function getPaymentsDetailsByPurchaseInvoiceID($piID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPaymentInvoice.paymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('*'),'left')
                ->join('account', 'outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID = account.accountId', array('*'))
                ->join('bank', 'account.bankId = bank.bankId', array('*'))
                ->where(array('outgoingPaymentInvoice.invoiceID' => $piID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }


    /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     */
    public function getPaymentsDetailsByPurchaseInvoiceIDForAging($piID, $endDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice')
                ->columns(array('*','paymentTotal' => new Expression('SUM(outgoingPaymentInvoice.outgoingInvoiceCashAmount)')))
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
                ->where(array('outgoingPaymentInvoice.invoiceID' => $piID, 'entity.deleted' => 0));

        $select->where->lessThanOrEqualTo('outgoingPaymentDate', $endDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

     /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     */
    public function getPaymentsDetailsByPaymentVaoucherIDForAging($pvID, $endDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice')
                ->columns(array('*','paymentTotal' => new Expression('SUM(outgoingPaymentInvoice.outgoingInvoiceCashAmount)')))
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
                ->where(array('outgoingPaymentInvoice.paymentVoucherId' => $pvID, 'entity.deleted' => 0));

        $select->where->lessThanOrEqualTo('outgoingPaymentDate', $endDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentIDByPurchaseInvoiceID($piID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice')
                ->where(array('outgoingPaymentInvoice.invoiceID' => $piID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentIDByPaymentVoucherID($pvID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice')
                ->where(array('outgoingPaymentInvoice.paymentVoucherId' => $pvID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentIDByPaymentVoucherIDASC($pvID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice')
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate'))
                ->order(array('outgoingPayment.outgoingPaymentDate' => 'ASC'))
                ->where(array('outgoingPaymentInvoice.paymentVoucherId' => $pvID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     *
     * Get invoice payments by paymentMethodId.
     * @param string $paymentMethodId
     * @return mixed
     */
    public function getInvoicePaymentByPaymentMethodIdAndAccountId($paymentMethodId, $accountId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPaymentInvoice')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate'))
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outGoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodBankTransferAccountId', 'outGoingPaymentMethodBankTransferSupplierAccountNumber', 'outGoingPaymentMethodReferenceNumber'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPaymentInvoice.paymentMethodID', $paymentMethodId)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodBankTransferAccountId', $accountId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

     /**
     * Update purchase invoice payment by invoice id 
     * @param array $data
     * @param int $invoiceId
     * @return boolean
     */
    public function updateInvoicePaymentByInvoiceId($data, $invoiceId)
    {
        if ($this->tableGateway->update($data, array('invoiceID' => $invoiceId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function updateInvoicePaymentByPaymentVoucherId($data, $paymentVoucherID)
    {
        if ($this->tableGateway->update($data, array('paymentVoucherId' => $paymentVoucherID))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
    * this function use to update outgoingPaymentInvoice table records by its id
    * @param array $data
    * @param int $id
    * return boolean
    **/
    public function updateInvoicePaymentByInvoicePaymentID($data, $id)
    {
        try {
            $this->tableGateway->update($data, array('outgoingPaymentInvoiceID' => $id));
            return true;
        } catch(Exception $e) {
            error_log($e);
            return false;
        }
    }

}
