<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class DraftProductSerialTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function getSerialProducts($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('productSerialID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getProductByCodeAndSerailCode($productCode, $serialCode = null, $bulkFlag = false,$bulkSerialCodes = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial');
        $select->join('locationProduct', 'productSerial.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode'));
        $select->where(array('product.productCode' => $productCode));
        if ($bulkFlag) {
            $select->where->in('productSerial.productSerialCode', $bulkSerialCodes);
        } else { 
            $select->where(array('productSerial.productSerialCode' => $serialCode));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function addSerialProducts($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('productSerialID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveSerialProduct($productSerial)
    {
        $data = array(
            'locationProductID' => $productSerial->locationProductID,
            'productBatchID' => $productSerial->productBatchID,
            'productSerialCode' => $productSerial->productSerialCode,
            'productSerialExpireDate' => $productSerial->productSerialExpireDate,
            'productSerialID' => $productSerial->productSerialID,
            'productSerialWarrantyPeriod' => $productSerial->productSerialWarrantyPeriod,
            'productSerialSold' => $productSerial->productSerialSold,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function getSerialProductsByLocProductId($locProId)
    {
        $locProId = (int) $locProId;
        $rowset = $this->tableGateway->select(array('locationProductID' => $locProId, 'productSerialSold' => 0, 'productSerialReturned' => 0));

        if ($rowset->count() == 0) {
            return 0;
        } else {
            return $rowset;
        }
    }

    public function getSerialProductsBySerialCode($sCode)
    {
        $rowset = $this->tableGateway->select(array('productSerialCode' => $sCode));
        $row = $rowset->current();
        return $row;
    }

    public function getSerialProBySCDLocID($sCode, $locProID)
    {
        $rowset = $this->tableGateway->select(array('productSerialCode' => $sCode, 'locationProductID' => $locProID, 'productSerialSold' => 0));
        $row = $rowset->current();
        return $row;
    }

    public function getSerialProductsByLocIdSerialCode($sCode, $locProID)
    {
        $rowset = $this->tableGateway->select(array('productSerialCode' => $sCode, 'locationProductID' => $locProID, 'productSerialSold' => 0));
        $row = $rowset->current();
        return $row;
    }

    public function getSerialAndBatchProductsByProductId($locProId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productSerial')
                ->columns(array('lpID' => new Expression('productSerial.locationProductID'),
                    'pbID' => new Expression('productSerial.productBatchID'),
                    'psCD' => new Expression('productSerial.productSerialCode')))
                ->join('productBatch', 'productSerial.productBatchID = productBatch.productBatchID', array(
                    'pbCD' => new Expression('productBatchCode')), 'left')
                ->where(array('productSerial.locationProductID' => $locProId,
                    'productBatch.productBatchCode IS NOT NULL',
                    'productSerial.productSerialSold' => 0,
                    'productSerialReturned' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 0) {
            return NULL;
        } else {
            foreach ($results as $val) {
                $countArray[] = $val;
            }

            return $countArray;
        }
    }

    public function getBatchIdList($pId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productSerial')
                ->columns(array('locationProductID', 'productBatchID', 'productSerialID'))
                ->join('productBatch', 'productSerial.productBatchID = productBatch.productBatchID', array('productBatchCode'), 'left')
                ->where(array('productSerial.locationProductID' => $pId,
                    'productBatch.productBatchCode IS NOT NULL',
                    'productSerial.productSerialSold' => 0))
        ->where->greaterThan('productBatchQuantity', 0);


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 0) {
            return 0;
        } else {
            foreach ($results as $val) {
                $countArray[] = $val;
            }

            return $countArray;
        }
    }

    public function updateBatchSerial($sCode, $bId, $data)
    {
        $result = $this->tableGateway->update($data, array('productSerialCode' => $sCode, 'productBatchID' => $bId));
        return $result;
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param type $locProID
     * @param type $sCode
     * @param type $data
     * @return type
     */
    public function updateSerial($proSerialID, $sCode, $data)
    {
        $result = $this->tableGateway->update($data, array('productSerialCode' => $sCode, 'productSerialID' => $proSerialID));
        return $result;
    }

    public function updateLocationProductID($sId, $data)
    {
        $result = $this->tableGateway->update($data, array('productSerialID' => $sId));
        return $result;
    }

    public function updateProductSerialData($data, $sId)
    {
        $result = $this->tableGateway->update($data, array('productSerialID' => $sId));
        return $result;
    }

    public function updateBatchSerialID($sId, $bId, $data)
    {
        try {
            $this->tableGateway->update($data, array('productSerialID' => $sId, 'productBatchID' => $bId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function updateBySerialID($data, $sId)
    {
        try {
            $this->tableGateway->update($data, array('productSerialID' => $sId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get serial product by serial code and locationproductID
    public function getSerialProductsBySerialCodeAndLocationProdutId($sCode, $locationProductID)
    {
        $rowset = $this->tableGateway->select(array('productSerialCode' => $sCode, 'locationProductID' => $locationProductID));
        $row = $rowset->current();
        return $row;
    }

    /**
     * Get serial items to movement report
     * @param string $itemSerialIds
     * @param string $InvoiceIds
     * @param string $itemIds
     * @param int $locationIds
     * @return mixed
     */
    public function getSerialProductsToMovementReport($itemSerialIds = null, $InvoiceIds = null, $itemIds = null, $locationIds = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial')
                ->columns(array('*'))
                ->join('locationProduct', 'productSerial.locationProductID = locationProduct.locationProductID', array('locationID'))
                ->join('location', 'locationProduct.locationID = location.locationID', array('locationName'))
                ->join('product', 'locationProduct.productID = product.productID', array('productName'))
                ->join('salesInvoiceSubProduct', 'productSerial.productSerialID = salesInvoiceSubProduct.productSerialID', array('salesInvocieProductID'))
                ->join('salesInvoiceProduct', 'salesInvoiceSubProduct.salesInvocieProductID = salesInvoiceProduct.salesInvoiceProductID', array('salesInvoiceID'))
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesInvoiceCode', 'salesInvoiceIssuedDate','statusID'))
                ->join('itemIn', 'productSerial.productSerialID = itemIn.itemInSerialID', array('itemInDocumentType', 'itemInDocumentID'))
                ->join('deliveryNote', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('deliveryNoteCode'),'left')
        ->where->equalTo('productSerial.productSerialSold', 1)
        ->where->equalTo('salesInvoice.statusID', 3);
        $select->group(array('productSerial.productSerialID'));
        if ($itemSerialIds) {
            $select->where->in('productSerial.productSerialID', $itemSerialIds);
        } 
        if ($InvoiceIds) {
            $select->where->in('salesInvoice.salesInvoiceID', $InvoiceIds);
        } else {
            $select->where->in("salesInvoice.locationID", $locationIds);
        }
        if ($itemIds) {
            $select->where->in('product.productID', $itemIds);
        } else {
            $select->join('entity', 'product.entityID = entity.entityID', array('*'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('product.serialProduct', 1);
        }
        if ($locationIds) {
            $select->where->in('location.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get Product Serial List
     * @param type $searchKey
     * @return mixed
     */
    public function searchProductSreialsForDropdown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial')
                ->columns(array('*'));
        if ($searchKey != NULL) {
            $select->where(new PredicateSet(array(new Operator('productSerial.productSerialCode', 'like', '%' . $searchKey . '%'))));
            $select->limit(10);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getSerialProduct($serialID = NULL, $batchID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial')
                ->columns(['*'])
                ->where(['productSerialID' => $serialID, 'productBatchID' => $batchID]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getBatchProduct($batchID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productBatch')
                ->columns(['*'])
                ->where(['productBatchID' => $batchID]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getBatchSerialProduct($serilaID, $batchID)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('productSerialID' => $serilaID, 'productBatchID' => $batchID));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $serilaID");
        }
        return $row;
    }
    
    /**
     * Get serial products details
     * @param array $itemSerialIds
     * @param array $itemIds
     * @param array $locationIds
     * @return mixed
     */
    public function getSerialProductsDetails( $itemSerialIds = null, $itemIds = null, $locationIds = null) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial')
                ->columns(array('*'))
                ->join('locationProduct', 'productSerial.locationProductID = locationProduct.locationProductID', array('locationID'))
                ->join('location', 'locationProduct.locationID = location.locationID', array('locationName'))
                ->join('product', 'locationProduct.productID = product.productID', array('productName'))
                ->join('itemIn', 'productSerial.productSerialID = itemIn.itemInSerialID', array('itemInDocumentType', 'itemInDocumentID'))
                ->where->in('itemIn.itemInDocumentType',['Payment Voucher','Goods Received Note','Inventory Positive Adjustment','Inventory Transfer','Credit Note']);
        if ($itemSerialIds) {
            $select->where->in('productSerial.productSerialID', $itemSerialIds);
        }
        if ($itemIds) {
            $select->where->in('product.productID', $itemIds);
        }
        if ($locationIds) {
            $select->where->in('location.locationID', $locationIds);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getSerialProductByBatchIDAndSerialID($serilaID, $batchID)
    {
        $rowset = $this->tableGateway->select(array('productSerialID' => $serilaID, 'productBatchID' => $batchID));
        return $row = $rowset->current();
    }

    public function getSerialProductByProdctLocationIDAndSerialID($serilaID, $locationProductD)
    {
        $rowset = $this->tableGateway->select(array('productSerialID' => $serilaID, 'locationProductID' => $locationProductD));
        return $row = $rowset->current();
    }
    public function getSerialDetailsByProductID($proID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial');
        $select->columns(array('*'));
        $select->join('productBatch', 'productSerial.productBatchID = productBatch.productBatchID', array('productBatchCode'),'left');
        $select->join('locationProduct', 'productSerial.locationProductID = locationProduct.locationProductID', array('productID'),'left');
        $select->where(array('locationProduct.productID' => $proID));
        $select->where(array('productSerial.productSerialSold' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getSerialProductByCodeAndSerailCode($productCode, $serialCode)
     {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial');
        $select->join('locationProduct', 'productSerial.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode'));
        $select->where(array('product.productCode' => $productCode));
        $select->where(array('productSerial.productSerialCode' => $serialCode));
        $select->where(array('productSerial.productSerialReturned' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * get serial product details
     */
    public function getSerialProductDetails($productSerialId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial');
        $select->where->equalTo('productSerial.productSerialID', $productSerialId);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute()->current();
    }

    public function getSerialProductDetailsBySerialCodeAndLocId($sCode, $locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial');
        $select->where->equalTo('productSerial.productSerialCode', $sCode);
        $select->where->equalTo('productSerial.locationproductID', $locationProductID);
        $select->where->notEqualTo('productSerial.productSerialReturned', 1);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute()->current();
    }

    public function getAvailableSerialProductsByLocIdSerialCode($sCode, $locProID)
    {
        $rowset = $this->tableGateway->select(array('productSerialCode' => $sCode, 'locationProductID' => $locProID, 'productSerialSold' => 0, 'productSerialReturned' => 0));
        $row = $rowset->current();
        return $row;
    }

    /**
    * get n number of available product serial
    * @param int $n
    * @param int $locationProductID
    * @return array
    */
    public function getMultiAvailableProductSerialID($n, $locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productSerial');
        $select->where(new PredicateSet(array(new Operator('locationProductID', '=', $locationProductID))));
        $select->where(new PredicateSet(array(new Operator('productSerialSold', '=', 0))));
        $select->where(new PredicateSet(array(new Operator('productSerialReturned', '=', 0))));
        $select->limit($n);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() != $n) {
            return 0;
        } else {
            foreach ($results as $value) {
                $productSerialID[] = $value['productSerialID'];
                $productBatchID[] = $value['productBatchID'];
            }
            $data = array(
                'productSerialIDs' => $productSerialID,
                'productBatchIDs' => $productBatchID
            );
            return $data;
        }
    }

    /**
    * set n number of available product serial as sold
    * @param mixed $data
    * @param int $productSerialID
    * @return 0/1
    */
    public function setMultiProductSerialAsSold($data, $productSerialID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $update = $sql->update();
        $update->table('productSerial');
        $update->set($data);
        $update->where->in('productSerialID', $productSerialID);
        $statement = $sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return $results;
    }
}
