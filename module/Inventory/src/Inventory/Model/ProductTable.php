<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\Product;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class ProductTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID = NULL, $fixedAssetFlag = false, $withoutInactiveItems = false)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('product')
                    ->columns(array('*'))
                    ->join('locationProduct', 'product.productID = locationProduct.productID', array(
                        'locationProductID', 'defaultSellingPrice', 'locationProductQuantity', 'mrpType', 'mrpPercentageValue', 'mrpValue'), 'left')
                    ->join('category', 'product.categoryID = category.categoryID', array('categoryName', 'categoryParentID'), 'left')
                    ->join('productUom', 'productUom.productID = product.productID', array('productUomID', 'productUomConversion'), 'left')
                    ->join('uom', 'uom.uomID = productUom.uomID', array('uomAbbr'), 'left')
                    ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                    ->join('productHandeling', 'product.productID = productHandeling.productID', array('productHandelingGiftCard','productHandelingFixedAssets'), 'left')
                    ->where(array('entity.deleted' => '0', 'productUomDisplay' => 1, 'productHandelingGiftCard' => 0));
            if ($locationID) {
                $select->where(array('locationProduct.locationID' => $locationID));
            }

            if ($fixedAssetFlag) {
                $select->where(array('productHandeling.productHandelingFixedAssets' => 1, 'product.serialProduct' => 1));
            }

            if ($withoutInactiveItems) {
                $select->where(array('product.productState' => 1));
            }

            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );

            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function getProduct($productID)
    {
        $rowset = $this->tableGateway->select(array('productID' => $productID));
        $row = $rowset->current();
        return $row;
    }

    public function getProductsByDiscountSchemeID($schemeID)
    {
        $rowset = $this->tableGateway->select(array('discountSchemID' => $schemeID));
        $row = $rowset;
        return $row;
    }

    public function getProductByLocIdProCode($proCode, $locId)
    {
        $locId = (int) $locId;
        $rowset = $this->tableGateway->select(array('productCode' => $proCode, 'locationID' => $locId));
        $row = $rowset->current();
        return $row;
    }

    public function getProductByCode($productCode,$serial = false, $batch = false, $productStatus = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->where(array('product.productCode' => $productCode, 'entity.deleted' => '0'));
        if($serial){
            $select->where(array('product.serialProduct' => '1'));
        }
        if($batch){
            $select->where(array('product.batchProduct' => '1'));   
        }
        if ($productStatus) {
            $select->where(array('product.productState' => '1'));   
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results->current();
    }

    /**
     *
     * @param type $productBarcode
     * @return type
     */
    public function getProductByBarcode($productBarcode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->where(array(
                    'product.productBarcode' => $productBarcode,
                    'entity.deleted' => '0'
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results->current();
    }

    public function getProductByName($name)
    {
        $rowset = $this->tableGateway->select(array('productName' => $name));
        $row = $rowset->current();
        return $row;
    }

    public function getProductByProductID($id, $category = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('productHandeling', 'product.productID = productHandeling.productID', array('*'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->join('category','product.categoryID = category.categoryID', array('categoryName'), 'left')
                ->where(array('product.productID' => $id, 'entity.deleted' => '0'));
        if($category != ''){
            $select->where->in('product.categoryID',$category);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results) {
            $row = $results->current();
            return $row;
        } else {
            return $results;
        }
    }

    public function getProductByProductCode($pCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->where(array('product.productCode' => $pCode, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results) {
            $row = (object) $results->current();
            return $row;
        } else {
            return $results;
        }
    }

    public function getProductByPCode($pCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->where(array('product.productCode' => $pCode, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function saveProduct(Product $product)
    {
        $data = array(
            'productCode' => $product->productCode,
            'productBarcode' => $product->productBarcode,
            'productName' => $product->productName,
            'productTypeID' => $product->productTypeID,
            'productDescription' => $product->productDescription,
            'productDiscountEligible' => $product->productDiscountEligible,
            'productDiscountPercentage' => $product->productDiscountPercentage,
            'productDiscountValue' => $product->productDiscountValue,
            'productDefaultOpeningQuantity' => $product->productDefaultOpeningQuantity,
            'productDefaultSellingPrice' => $product->productDefaultSellingPrice,
            'productDefaultMinimumInventoryLevel' => $product->productDefaultMinimumInventoryLevel,
            'productDefaultMaximunInventoryLevel' => $product->productDefaultMaximunInventoryLevel,
            'productDefaultReorderLevel' => $product->productDefaultReorderLevel,
            'productTaxEligible' => $product->productTaxEligible,
            'productImageURL' => $product->productImageURL,
            'productState' => $product->productState,
            'categoryID' => $product->categoryID,
            'productGlobalProduct' => $product->productGlobalProduct,
            'batchProduct' => $product->batchProduct,
            'serialProduct' => $product->serialProduct,
            'hsCode' => $product->hsCode,
            'customDutyPercentage' => $product->customDutyPercentage,
            'customDutyValue' => $product->customDutyValue,
            'entityID' => $product->entityID,
            'productDefaultPurchasePrice' => $product->productDefaultPurchasePrice,
            'rackID' => $product->rackID,
            'itemDetail' => $product->itemDetail,
            'productSalesAccountID' => $product->productSalesAccountID,
            'productInventoryAccountID' => $product->productInventoryAccountID,
            'productCOGSAccountID' => $product->productCOGSAccountID,
            'productAdjusmentAccountID' => $product->productAdjusmentAccountID,
            'productDepreciationAccountID' => $product->productDepreciationAccountID,
            'productPurchaseDiscountEligible' => $product->productPurchaseDiscountEligible,
            'productPurchaseDiscountPercentage' => $product->productPurchaseDiscountPercentage,
            'productPurchaseDiscountValue' => $product->productPurchaseDiscountValue,
            'isUseDiscountScheme' => $product->isUseDiscountScheme,
            'discountSchemID' => $product->discountSchemID,
        );
        $result = $this->getProductByCode($product->productCode);
        if ($result == NULL) {
            try {
                $this->tableGateway->insert($data);
                $id = $this->tableGateway->lastInsertValue;
                return $id;
            } catch (\Exception $exc) {
                echo $exc;
                return false;
            }
        } else {
            return false;
        }
    }

    public function updateProduct(Product $product)
    {
        $data = array(
            'productCode' => $product->productCode,
            'productBarcode' => $product->productBarcode,
            'productName' => $product->productName,
            'productTypeID' => $product->productTypeID,
            'productDescription' => $product->productDescription,
            'productDiscountEligible' => $product->productDiscountEligible,
            'productDiscountPercentage' => $product->productDiscountPercentage,
            'productDiscountValue' => $product->productDiscountValue,
            'productDefaultOpeningQuantity' => $product->productDefaultOpeningQuantity,
            'productDefaultSellingPrice' => $product->productDefaultSellingPrice,
            'productDefaultMinimumInventoryLevel' => $product->productDefaultMinimumInventoryLevel,
            'productDefaultMaximunInventoryLevel' => $product->productDefaultMaximunInventoryLevel,
            'productDefaultReorderLevel' => $product->productDefaultReorderLevel,
            'productTaxEligible' => $product->productTaxEligible,
            'productImageURL' => $product->productImageURL,
            'productState' => $product->productState,
            'categoryID' => $product->categoryID,
            'productGlobalProduct' => $product->productGlobalProduct,
            'batchProduct' => $product->batchProduct,
            'serialProduct' => $product->serialProduct,
            'hsCode' => $product->hsCode,
            'customDutyPercentage' => $product->customDutyPercentage,
            'customDutyValue' => $product->customDutyValue,
            'productDefaultPurchasePrice' => $product->productDefaultPurchasePrice,
            'rackID' => $product->rackID,
            'itemDetail' => $product->itemDetail,
             'productSalesAccountID' => $product->productSalesAccountID,
            'productInventoryAccountID' => $product->productInventoryAccountID,
            'productCOGSAccountID' => $product->productCOGSAccountID,
            'productAdjusmentAccountID' => $product->productAdjusmentAccountID,
            'productDepreciationAccountID' => $product->productDepreciationAccountID,
            'productPurchaseDiscountEligible' => $product->productPurchaseDiscountEligible,
            'productPurchaseDiscountPercentage' => $product->productPurchaseDiscountPercentage,
            'productPurchaseDiscountValue' => $product->productPurchaseDiscountValue,
            'isUseDiscountScheme' => $product->isUseDiscountScheme,
            'discountSchemID' => $product->discountSchemID,
        );
        try {
            $this->tableGateway->update($data, array('productID' => $product->productID));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function updateImportProduct($product)
    {
        $data = array(
            'categoryID' => $product->categoryID,
            'productGlobalProduct' => $product->productGlobalProduct,
        );
        try {
            $this->tableGateway->update($data, array('productID' => $product->productID));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function updateImageUrl($data, $productID)
    {
        try {
            $this->tableGateway->update($data, array('productID' => $productID));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function importreplaceProduct(Product $product, $entityID)
    {
        $data = array(
            'productName' => $product->productName,
            'productTypeID' => $product->productTypeID,
            'productDescription' => $product->productDescription,
            'productDiscountEligible' => $product->productDiscountEligible,
            'productDiscountPercentage' => $product->productDiscountPercentage,
            'productDiscountValue' => $product->productDiscountValue,
            'productTaxEligible' => $product->productTaxEligible,
            'productImageURL' => $product->productImageURL,
            'productState' => $product->productState,
            'categoryID' => $product->categoryID,
            'productGlobalProduct' => $product->productGlobalProduct,
            'batchProduct' => $product->batchProduct,
            'serialProduct' => $product->serialProduct,
            'hsCode' => $product->hsCode,
            'customDutyPercentage' => $product->customDutyPercentage,
            'customDutyValue' => $product->customDutyValue,
        );
        try {
            $this->tableGateway->update($data, array('productCode' => $product->productCode, 'entityID' => $entityID));
            return 'upd';
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    public function updateProductState(Product $product)
    {
        $data = array(
            'productState' => $product->productState,
        );
        try {
            $this->tableGateway->update($data, array('productID' => $product->productID));
            return array('state' => true,);
        } catch (\Exception $e) {
            return array('state' => false,);
        }
    }

    public function getProductIdByCode($code)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('productCode' => $code));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getProductsforSearch($productString, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $productString . '\' in product.productName )>0,POSITION(\'' . $productString . '\' in product.productName), 9999),'
                            . 'IF(POSITION(\'' . $productString . '\' in product.productCode )>0,POSITION(\'' . $productString . '\' in product.productCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(product.productName ), CHAR_LENGTH(product.productCode )) '),
                    '*',
                ))
                ->join('productUom', 'productUom.productID = product.productID', array('productUomID', 'productUomConversion'), 'left')
                ->join('uom', 'uom.uomID = productUom.uomID', array('uomAbbr'), 'left')
                ->join('category', 'product.categoryID= category.categoryID', array('categoryName', 'categoryParentID'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->join('locationProduct', 'product.productID =  locationProduct.productID', array('*'), 'left')
                ->join('productHandeling', 'product.productID = productHandeling.productID', array('productHandelingGiftCard'), 'left')
                ->where(array('locationProduct.locationID' => $locationID, 'productUomDisplay' => 1, 'productHandeling.productHandelingGiftCard' => 0))
                ->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))))
                ->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $productString . '%'), new Operator('product.productCode', 'like', '%' . $productString . '%'), new Operator('product.productDescription', 'like', '%' . $productString . '%')), PredicateSet::OP_OR))
                ->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));   
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * @author Sandun<sandun@thinkcube.com>
     * get productlist according to location Id
     * @param type $locationID
     * @return rowset
     */
    public function getProductList($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join('productHandeling', 'product.productID = productHandeling.productID', array('*'), 'left');
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array('*'));
        $select->join('productTax', 'product.productID = productTax.productID', array('taxID'), 'left');
        $select->join('tax', 'productTax.taxID = tax.id', array('*'), 'left');
        $select->join('productSerial', 'locationProduct.locationProductID = productSerial.locationProductID', array('productSerialID', 'serialProductBatchID' => new Expression('productSerial.productBatchID'), 'productSerialCode', 'productSerialSold'), 'left');
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomID'), 'left');
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomDecimalPlace', 'uomAbbr', 'uomState', 'uomName'), 'left');
        $select->join('productBatch', 'locationProduct.locationProductID = productBatch.locationProductID', array('productBatchID', 'productBatchCode', 'productBatchQuantity'), 'left');
        $select->join('entity', 'entity.entityID = product.entityID', array(
            'deleted'), 'left');
        $select->group('product.productCode');
        $select->where(array('locationID' => $locationID));
        $select->where(array('productState' => 1));
        $select->where(array('deleted' => 0));
        $select->where(array('locationProduct.locationID' => $locationID, 'product.productCode IS NOT NULL'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * @author Sandun<sandun@thinkcube.com>
     * get productlist according to location Id
     * @param type $locationID
     * @return rowset
     */
    public function getProductLists($proType = false, $globalPro = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->columns(array('productID', 'productName', 'productCode', 'productTypeID'));
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array(
            'locationProductQuantity', 'locationID', 'locationProductID'), 'left');
        $select->join('entity', 'entity.entityID = product.entityID', array(
            'deleted'), 'left');
        $select->join('location', 'location.locationID = locationProduct.locationID', array('locationCode', 'locationID'), 'left');
        $select->join('grnProduct', 'locationProduct.locationProductID = grnProduct.locationProductID', array('grnProductPrice'), 'left');
        $select->join('productUom', 'productUom.productID = locationProduct.productID', array('uomID', 'pC' => new Expression('productUomConversion')), 'left');
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'), 'left');
        $select->where(array('deleted' => '0'));
        if ($proType) {
            $select->where(array('product.productTypeID' => 1));
        }
        if ($globalPro) {
            $select->where(array('product.productGlobalProduct' => 1));
        }
        $select->order('product.productName');
        $select->group('product.productCode');
        $select->group('locationProduct.locationID');
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * @author Damith Thamara<damith@thinkcube.com>
     * @param INT $locationID
     * @return Resultset
     */
    public function getActiveProductListByLocation($locationID, $productID = false, $type = false, $invalidFlag = false)
    {
        
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array('*'));
        $select->join(array('locEntity' => 'entity'), 'locationProduct.entityID = locEntity.entityID', array('deleted'));
        $select->join('productHandeling', 'product.productID = productHandeling.productID', array('*'), 'left');
        $select->join('productTax', 'product.productID = productTax.productID', array('taxID'), 'left');
        $select->join('tax', 'productTax.taxID = tax.id', array('*'), 'left');
        $select->join('productSerial', 'locationProduct.locationProductID = productSerial.locationProductID', array('productSerialID', 'serialProductBatchID' => new Expression('productSerial.productBatchID'), 'productSerialCode', 'productSerialSold', 'productSerialReturned','productSerialWarrantyPeriod', 'productSerialExpireDate','productSerialWarrantyPeriodType'), 'left');
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomID', 'productUomDisplay', 'productUomBase'), 'left');
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomDecimalPlace', 'uomAbbr', 'uomState', 'uomName'), 'left');
        $select->join('productBatch', 'locationProduct.locationProductID = productBatch.locationProductID', array('productBatchID', 'productBatchCode', 'productBatchQuantity', 'productBatchExpiryDate', 'productBatchWarrantyPeriod','productBatchPrice'), 'left');
        $select->join(array('entity' => 'entity'), 'entity.entityID = product.entityID', array(
            'deleted'), 'left');
        $select->where(array('locationID' => $locationID));
        if(!$invalidFlag){
            $select->where(array('productState' => 1));
        }
        $select->where(array('entity.deleted' => 0));
        $select->where(array('locEntity.deleted' => 0));
        if ($productID) {
            $select->where(array('product.productID' => $productID));
        }
        if ($type == 'activity') {
            $select->where->NEST->equalTo('productHandelingFixedAssets', 1)->OR->equalTo(
                    'productHandelingPurchaseProduct', 1);
        }

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function getProductBatchAndSerial($pCode)
    {
        $rowset = $this->tableGateway->select(array('productCode' => $pCode, 'batchProduct' => 1, 'serialProduct' => 1));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getProductBatchAndSerialByProductID($productID)
    {
        $rowset = $this->tableGateway->select(array('productID' => $productID, 'batchProduct' => 1, 'serialProduct' => 1));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getProductBatch($pCode)
    {
        $rowset = $this->tableGateway->select(array('productCode' => $pCode, 'batchProduct' => 1, 'serialProduct' => 0));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getProductBatchByProductID($productID)
    {
        $rowset = $this->tableGateway->select(array('productID' => $productID, 'batchProduct' => 1, 'serialProduct' => 0));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getProductSerial($pCode)
    {
        $rowset = $this->tableGateway->select(array('productCode' => $pCode, 'batchProduct' => 0, 'serialProduct' => 1));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getProductSerialByProducttID($productID)
    {
        $rowset = $this->tableGateway->select(array('productID' => $productID, 'batchProduct' => 0, 'serialProduct' => 1));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getProductOnlyProduct($pCode)
    {
        $rowset = $this->tableGateway->select(array('productCode' => $pCode));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getProductOnlyProductByProductID($productID)
    {
        $rowset = $this->tableGateway->select(array('productID' => $productID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getProductListByProIds($pIDs)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('pName' => new Expression('productName'),
                    'pID' => new Expression('productID')))
                ->order('productID')
        ->where->in('productID', $pIDs);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getGlobalProducts()
    {
        try {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('product')
                    ->columns(array('*'))
                    ->join('entity', 'entity.entityID = product.entityID', array('deleted'), 'left')
                    ->where(array('productGlobalProduct' => 1, 'deleted' => 0));
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            return $results;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return $rowset
     */
    public function getBatchOrSerialProductList()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->columns(array('productID', 'productName', 'productCode',));
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array(
            'locationProductQuantity', 'locationID', 'locationProductID'), 'left');
        $select->where->notEqualTo('product.productTypeID', 2);
        $select->where(array('product.batchProduct' => 1,
            'product.serialProduct' => 1), \Zend\Db\Sql\Predicate\PredicateSet::OP_OR);
        $select->group('product.productID');
        $select->order('product.productName');
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $sbIds
     * @param type $fromDate
     * @param type $toDate
     */
    public function getExpiredBatchData($sbIds, $fromDate, $toDate, $isAllProducts)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->columns(array(
            'pID' => new Expression('product.productID'),
            'pName' => new Expression('product.productName'),
            'pCD' => new Expression('product.productCode')));
        $select->join('locationProduct', 'product.productID =  locationProduct.productID', array(
            'lpID' => new Expression('locationProduct.locationProductID')), 'left');
        $select->join('productBatch', 'locationProduct.locationProductID =  productBatch.locationProductID', array(
            'bExDate' => new Expression('productBatch.productBatchExpiryDate'),
            'qty' => new Expression('productBatchQuantity'),
            'bCD' => new Expression('productBatchCode'),
            'productBatchID'), 'left');
        $select->join('productSerial', 'productBatch.productBatchID =  productSerial.productBatchID', array(
            'sExDate' => new Expression('productSerial.productSerialExpireDate'),
            'productSerialID',
            'productSerialSold',
            'sCD' => new Expression('productSerialCode')
                ), 'left');
        $select->order('productBatch.productBatchID');
        if (!$isAllProducts) {
            $select->where->in('product.productID', $sbIds);
        }
        $select->where->notEqualTo('product.productTypeID', 2);
        $select->where(new PredicateSet(array(new Between('productBatch.productBatchExpiryDate', $fromDate, $toDate), new Between('productSerial.productSerialExpireDate', $fromDate, $toDate)), PredicateSet::OP_OR));
        $select->where(array('productBatchQuantity > 0'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        foreach ($rowset as $value) {
            $returnValue[] = $value;
        }
        
        return $returnValue;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $sbIds
     * @param type $fromDate
     * @param type $toDate
     * @return $rowset
     */
    public function getExpiredSerialData($sbIds = null, $fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product')
                ->columns(array(
                    'pID' => new Expression('product.productID'),
                    'pName' => new Expression('product.productName'),
                    'pCD' => new Expression('product.productCode')))
                ->join('locationProduct', 'product.productID =  locationProduct.productID', array(
                    'lpID' => new Expression('locationProduct.locationProductID')), 'left')
                ->join('productSerial', 'locationProduct.locationProductID =  productSerial.locationProductID', array(
                    'exDate' => new Expression('productSerial.productSerialExpireDate'),
                    'sCD' => new Expression('productSerialCode'),'productSerialID','productSerialSold'), 'left')
                ->order('productSerial.productSerialExpireDate')
                ->where->notEqualTo('product.productTypeID', 2);
        $select->where(array('product.serialProduct' => 1, 'productSerial.productSerialSold' => 0))
                ->where->between('productSerial.productSerialExpireDate', $fromDate, $toDate);
        if ($sbIds != null) {
            $select->where->in('product.productID', $sbIds);
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        foreach ($rowset as $value) {
            $returnValue[] = $value;
        }
        
        return $returnValue;
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @param type $categoryID
     * @return $boolean
     * this function used for check whether catgorys has use for product
     */
    public function useCategory($categoryID)
    {
        $id = (int) $categoryID;
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryID' => $id, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results->current();
        return(isset($row->categoryID)) ? TRUE : FALSE;
    }

    /**
     * @author Ashan Madushka<ashan@thinkcube.com>
     * @return $rowset
     * get active non inventory items
     */
    public function getActiveNonInventoryItems()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join(array('entity' => 'entity'), 'entity.entityID = product.entityID', array(
            'deleted'), 'left');
        $select->where(array('productState' => 1));
        $select->where(array('entity.deleted' => 0));
        $select->where(array('productTypeID' => 2));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * @author Ashan Madushka<ashan@thinkcube.com>
     * @return $rowset
     * get active non inventory items by search key
     */
    public function getActiveNonInventoryItemsBySearchKey($locationID, $searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('locationProductID'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('entityID'), 'left')
                ->join(array('lcpentity' => 'entity'), 'locationProduct.entityID = lcpentity.entityID', array('entityID'), 'left')
                ->where(array('locationProduct.locationID' => $locationID, 'entity.deleted' => 0, 'productState' => 1, 'productTypeID' => 2))
                ->order(array('product.productName' => 'ASC'));
        $select->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $searchKey . '%'), new Operator('product.productCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * this function gives only raw material items details
     * @param string $searchKey
     * @param boolean $addFlag
     * @return $rowSet
     */
    public function getRawMaterialItems($searchKey = FALSE, $addFlag = FALSE)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->columns(array('*'));
        $select->join('productHandeling', 'productHandeling.productID = product.productID', array('*'), 'left');
        $select->join('entity', 'entity.entityID = product.entityID', array('deleted'), 'left');
        $select->join('productUom', 'productUom.productID = product.productID', array('uomID'), 'left');
        $select->where(array('entity.deleted' => 0, 'productHandelingPurchaseProduct' => 1,
            'productState' => 1, 'productUomConversion' => 1));
        if ($addFlag != FALSE) {
            $select->where(array('batchProduct' => 0, 'serialProduct' => 0));
        }
        if (isset($searchKey)) {
            $select->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $searchKey . '%'), new Operator('product.productCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowSet = $query->execute();
        return $rowSet;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * this function gives only fixed assets items details
     * @return $rowSet
     */
    public function getFixedAssetsItems($searchKey = 'Fix')
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->columns(array('*'));
        $select->join('productHandeling', 'productHandeling.productID = product.productID', array('*'), 'left');
        $select->join('entity', 'entity.entityID = product.entityID', array('deleted'), 'left');
        $select->where(array('entity.deleted' => 0, 'productHandelingFixedAssets' => 1,
            'productState' => 1));
        if (isset($searchKey)) {
            $select->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $searchKey . '%'), new Operator('product.productCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        }

        $query = $sql->prepareStatementForSqlObject($select);
        $rowSet = $query->execute();
        return $rowSet;
    }

    public function giftCardFetchAll($paginated = FALSE, $locationID = NULL)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('product')
                    ->columns(array('*'))
                    ->join('locationProduct', 'product.productID = locationProduct.productID', array(
                        'locationProductID', 'defaultSellingPrice', 'locationProductQuantity'), 'left')
                    ->join('category', 'product.categoryID = category.categoryID', array('categoryName', 'categoryParentID'), 'left')
                    ->join('productUom', 'productUom.productID = product.productID', array('productUomID'), 'left')
                    ->join('uom', 'uom.uomID = productUom.uomID', array('uomAbbr'), 'left')
                    ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                    ->join('productHandeling', 'product.productID = productHandeling.productID', array('productHandelingGiftCard'), 'left')
                    ->where(array('entity.deleted' => '0', 'productUomDisplay' => 1, 'productHandelingGiftCard' => 1));
            if ($locationID) {
                $select->where(array('locationProduct.locationID' => $locationID));
            }
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );

            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function getGiftCardforSearch($giftCardString, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $giftCardString . '\' in product.productName )>0,POSITION(\'' . $giftCardString . '\' in product.productName), 9999),'
                            . 'IF(POSITION(\'' . $giftCardString . '\' in product.productCode )>0,POSITION(\'' . $giftCardString . '\' in product.productCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(product.productName ), CHAR_LENGTH(product.productCode )) '),
                    '*',
                ))
                ->join('category', 'product.categoryID= category.categoryID', array('categoryName', 'categoryParentID'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->join('locationProduct', 'product.productID =  locationProduct.productID', array('*'), 'left')
                ->join('productHandeling', 'product.productID = productHandeling.productID', array('productHandelingGiftCard'), 'left')
                ->where(array('locationProduct.locationID' => $locationID, 'productHandeling.productHandelingGiftCard' => 1))
                ->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))))
                ->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $giftCardString . '%'), new Operator('product.productCode', 'like', '%' . $giftCardString . '%')), PredicateSet::OP_OR))
                ->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * Get active product list by categoryId
     * @param Int $categoryId
     * @return mixed
     */
    public function getProductListByCategoryId($categoryId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('product.categoryId', $categoryId)
        ->where->equalTo('product.productState', 1);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * Get active product list by categoryId
     * @param Int $categoryId
     * @return mixed
     */
    public function getProductListByCategoryIdAndLocationID($categoryId, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'))
                ->join('locationProduct', 'product.productID =  locationProduct.productID', array('*'), 'left')
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('product.categoryId', $categoryId)
        ->where->equalTo('locationProduct.locationID', $locationID)
        ->where->equalTo('product.productState', 1);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * Get serial product list
     * @return mixed
     */
    public function getSerialProductList($searchKey = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('product.serialProduct', 1);
        if ($searchKey) {
            $select->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $searchKey . '%'), new Operator('product.productCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
            $select->limit(10);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getAllProductForExpiryAlert($locationID)
    {
        $sql = "SELECT * FROM (SELECT `product`.`productID`,`locationProduct`.`locationProductID`,`expiryAlertNotification`.`numberOfDays`,`productSerial`.`productSerialCode`, `productSerial`.`productSerialExpireDate`,DATE_ADD(CURDATE(),INTERVAL `expiryAlertNotification`.`NumberOfDays` DAY) AS `testDay`,`productBatch`.`productBatchCode`, `productBatch`.`productBatchExpiryDate`,`productBatch`.`productBatchQuantity`,`uom`.`uomAbbr`,`product`.`productCode`,`product`.`productName`,`productSerial`.`productSerialID`,`productBatch`.`productBatchID`
FROM `product`
LEFT JOIN `locationProduct`
ON `product`.`productID`=`locationProduct`.`productID`
LEFT JOIN `expiryAlertNotification`
ON `product`.`productID`=`expiryAlertNotification`.`productID`
LEFT JOIN `productSerial`
ON `locationProduct`.`locationProductID`=`productSerial`.`locationProductID` AND `product`.`serialProduct` = '1'
LEFT JOIN `productBatch`
ON `locationProduct`.`locationProductID`=`productBatch`.`locationProductID` AND `product`.`serialProduct` = '0' AND `product`.`batchProduct` = '1'
LEFT JOIN `productUom`
ON `product`.`productID`=`productUom`.`productID` AND `productUom`.`productUomDisplay`='1'
LEFT JOIN `uom`
ON `productUom`.`uomID`=`uom`.`uomID`
WHERE `locationProduct`.`locationID`='$locationID' AND  (`locationProduct`.`locationProductQuantity`!='0'OR`locationProduct`.`locationProductQuantity`!='NULL') AND (`productSerial`.`productSerialExpireDate`<= DATE_ADD(CURDATE(),INTERVAL `expiryAlertNotification`.`NumberOfDays` DAY) OR `productBatch`.`productBatchExpiryDate`<= DATE_ADD(CURDATE(),INTERVAL `expiryAlertNotification`.`NumberOfDays` DAY))
ORDER BY `expiryAlertNotification`.`NumberOfDays` ) newTable
GROUP BY `productSerialCode`
;";
        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);

        return $resultSet;
    }

    /**
     * Get location wise product details by product id and location id
     * @param int $productId
     * @param int $locationId
     * @return mixed
     */
    public function getLocationWiseProductDetails($productId, $locationId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('*'), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('productUomBase'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('entityID'), 'left')
                ->where(array('product.productID' => $productId, 'locationProduct.locationID' => $locationId, 'productUom.productUomBase' => '1', 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results) {
            return $results->current();
        } else {
            return null;
        }
    }
    
    /**
     * Update existing product by product id
     * @param array $data
     * @param string $productId
     * @return boolean
     */
    public function updateProductByArray($data, $productId)
    {
        if ($productId) {
            $this->tableGateway->update($data, array('productID' => $productId));
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Get existing product list
     * @return mixed
     */
    public function getActiveProductList() 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;                
    }

    public function getProductForSerachByCategoryID($categoryId, $serchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'));
        $select->where->equalTo('entity.deleted', 0);
        if($categoryId != null){
            $select->where->in('product.categoryId', $categoryId);
        }
        $select->where(new PredicateSet(
                array(
                    new Operator('product.productName', 'like', '%' . $serchKey . '%'), 
                    new Operator('product.productCode', 'like', '%' . $serchKey . '%')
                    ), PredicateSet::OP_OR));
               


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    
    }

    // get batch and serial products by search key
    public function getBatchAndSerialProductListBySearchKey($searchKey = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'))
        ->where->equalTo('entity.deleted', 0);
        $select->where(new PredicateSet(array(new Operator('product.serialProduct','=',1), new Operator('product.batchProduct','=',1)), PredicateSet::OP_OR));
            
        if ($searchKey) {
            $select->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $searchKey . '%'), new Operator('product.productCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
            $select->limit(10);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /*
    * get the product data by gl account id
    */
    public function getProductByGlAccountID($glAccountID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->where(new PredicateSet(array(new Operator('productSalesAccountID', '=',$glAccountID), 
                    new Operator('productInventoryAccountID', '=',$glAccountID),
                    new Operator('productCOGSAccountID', '=',$glAccountID),
                    new Operator('productAdjusmentAccountID', '=',$glAccountID)
                ), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /*
    * check availability of the product code
    */
    public function checkAvailabilityOfProductCode($proCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
                ->where(array('product.productCode' => $proCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if(count($results) <= 0) {
            return true;
        } else {
            return false;
        }
    }
    /*
    * check availability of the product code
    */
    public function getProductsByAttributeID($attributeID, $attributeValueID)
    {
        // var_dump($attributeValueID).die();
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->join('itemAttributeMap', 'product.productID = itemAttributeMap.productID', array('*'), 'left')
                ->join('itemAttributeValueMap', 'itemAttributeMap.itemAttributeMapID = itemAttributeValueMap.itemAttributeMapID', array('*'), 'left')
                ->where(array('entity.deleted' => 0))
                ->where(array('itemAttributeMap.itemAttributeID' => $attributeID, 'itemAttributeValueMap.itemAttributeValueID' => $attributeValueID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $data = [];
        foreach ($results as $key => $value) {

            $data[$value['productID']]=$value['productID'];
        // var_dump($value);
        }
        // die();
        return $data;
    }

}
