<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains price list item Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PriceListItems implements InputFilterAwareInterface
{

    public $priceListItemsId;
    public $priceListId;
    public $productId;
    public $priceListItemsPrice;

    /**
     * 1 - Value
     * 2 - Percentage
     * @var int price list discount type
     */
    public $priceListItemsDiscountType;

    /**
     * @var decimal price list discount
     */
    public $priceListItemsDiscount;

    protected $inputFilter;

    const PRICE_LIST_ITEM_DEFAULT_DISCOUNT_TYPE = 2;

    public function exchangeArray($data)
    {
        $this->priceListItemsId = (!empty($data['priceListItemsId'])) ? $data['priceListItemsId'] : null;
        $this->priceListId = (!empty($data['priceListId'])) ? $data['priceListId'] : null;
        $this->productId = (!empty($data['productId'])) ? $data['productId'] : null;
        $this->priceListItemsPrice = (!empty($data['priceListItemsPrice'])) ? $data['priceListItemsPrice'] : 0.00;
        $this->priceListItemsDiscountType = (!empty($data['priceListItemsDiscountType'])) ? $data['priceListItemsDiscountType'] : self::PRICE_LIST_ITEM_DEFAULT_DISCOUNT_TYPE;
        $this->priceListItemsDiscount = (!empty($data['priceListItemsDiscount'])) ? $data['priceListItemsDiscount'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
