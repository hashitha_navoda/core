<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains supplier payments Table Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class SupplierPayments
{

    public $outgoingPaymentID;
    public $outgoingPaymentCode;
    public $paymentTermID;
    public $outgoingPaymentDate;
    public $paymentMethodID;
    public $outgoingPaymentAmount;
    public $supplierID;
    public $outgoingPaymentDiscount;
    public $outgoingPaymentMemo;
    public $outgoingPaymentType;
    public $debitNoteID;
    public $locationID;
    public $outgoingPaymentCancelMessage;
    public $outgoingPaymentStatus;
    public $entityID;
    public $outgoingPaymentCreditAmount;
    public $outgoingPaymentPaidAmount;
    public $outgoingPaymentBalanceAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->outgoingPaymentID = (!empty($data['outgoingPaymentID'])) ? $data['outgoingPaymentID'] : null;
        $this->outgoingPaymentCode = (!empty($data['outgoingPaymentCode'])) ? $data['outgoingPaymentCode'] : null;
        $this->paymentTermID = (!empty($data['paymentTermID'])) ? $data['paymentTermID'] : null;
        $this->outgoingPaymentDate = (!empty($data['outgoingPaymentDate'])) ? $data['outgoingPaymentDate'] : null;
        $this->paymentMethodID = (!empty($data['paymentMethodID'])) ? $data['paymentMethodID'] : null;
        $this->outgoingPaymentAmount = (!empty($data['outgoingPaymentAmount'])) ? $data['outgoingPaymentAmount'] : 0.00;
        $this->supplierID = (!empty($data['supplierID'])) ? $data['supplierID'] : null;
        $this->outgoingPaymentDiscount = (!empty($data['outgoingPaymentDiscount'])) ? $data['outgoingPaymentDiscount'] : 0.00;
        $this->outgoingPaymentMemo = (!empty($data['outgoingPaymentMemo'])) ? $data['outgoingPaymentMemo'] : null;
        $this->outgoingPaymentType = (!empty($data['outgoingPaymentType'])) ? $data['outgoingPaymentType'] : null;
        $this->debitNoteID = (!empty($data['debitNoteID'])) ? $data['debitNoteID'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->outgoingPaymentStatus = (!empty($data['outgoingPaymentStatus'])) ? $data['outgoingPaymentStatus'] : 0;
        $this->outgoingPaymentCancelMessage = (!empty($data['outgoingPaymentCancelMessage'])) ? $data['outgoingPaymentCancelMessage'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->outgoingPaymentCreditAmount = (!empty($data['outgoingPaymentCreditAmount'])) ? $data['outgoingPaymentCreditAmount'] : null;
        $this->outgoingPaymentPaidAmount = (!empty($data['outgoingPaymentPaidAmount'])) ? $data['outgoingPaymentPaidAmount'] : null;
        $this->outgoingPaymentBalanceAmount = (!empty($data['outgoingPaymentBalanceAmount'])) ? $data['outgoingPaymentBalanceAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
