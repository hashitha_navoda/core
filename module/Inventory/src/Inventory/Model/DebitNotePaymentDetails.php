<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains debit note payment details Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DebitNotePaymentDetails implements InputFilterAwareInterface
{

    public $debitNotePaymentDetailsID;
    public $paymentMethodID;
    public $debitNotePaymentID;
    public $debitNoteID;
    public $debitNotePaymentDetailsAmount;
    protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->debitNotePaymentDetailsID = (!empty($data['debitNotePaymentDetailsID'])) ? $data['debitNotePaymentDetailsID'] : null;
        $this->paymentMethodID = (!empty($data['paymentMethodID'])) ? $data['paymentMethodID'] : null;
        $this->debitNotePaymentID = (!empty($data['debitNotePaymentID'])) ? $data['debitNotePaymentID'] : null;
        $this->debitNoteID = (!empty($data['debitNoteID'])) ? $data['debitNoteID'] : null;
        $this->debitNotePaymentDetailsAmount = (!empty($data['debitNotePaymentDetailsAmount'])) ? $data['debitNotePaymentDetailsAmount'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

// Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
