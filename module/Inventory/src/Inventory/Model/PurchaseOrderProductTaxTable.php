<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PurchaseOrderProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePurchaseOrderProductTax(PurchaseOrderProductTax $poProductTax)
    {
        $data = array(
            'purchaseOrderID' => $poProductTax->purchaseOrderID,
            'purchaseOrderProductID' => $poProductTax->purchaseOrderProductID,
            'purchaseOrderTaxID' => $poProductTax->purchaseOrderTaxID,
            'purchaseOrderTaxPrecentage' => $poProductTax->purchaseOrderTaxPrecentage,
            'purchaseOrderTaxAmount' => $poProductTax->purchaseOrderTaxAmount,
        );
        $this->tableGateway->insert($data);
    }

    
    public function deleteDraftRecordTaxesByPoID($poID)
    {
        try {
            $result = $this->tableGateway->delete(array('purchaseOrderID' => $poID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getInvoiceTax($poID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseOrderProductTax")
                ->columns(array("*"))
                ->join("tax", "purchaseOrderProductTax.purchaseOrderTaxID = tax.id", array("*"), "left")
                ->join("purchaseOrderProduct", "purchaseOrderProductTax.purchaseOrderProductID = purchaseOrderProduct.purchaseOrderProductID", array("purchaseOrderID"), "left")
                ->where(array("purchaseOrderProduct.purchaseOrderID" => $poID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

}

