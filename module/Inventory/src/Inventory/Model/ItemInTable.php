<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ItemInTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveItemIn(ItemIn $itemIn)
    {
        $data = array(
            'itemInIndex' => $itemIn->itemInIndex,
            'itemInDocumentType' => $itemIn->itemInDocumentType,
            'itemInDocumentID' => $itemIn->itemInDocumentID,
            'itemInLocationProductID' => $itemIn->itemInLocationProductID,
            'itemInBatchID' => $itemIn->itemInBatchID,
            'itemInSerialID' => $itemIn->itemInSerialID,
            'itemInQty' => $itemIn->itemInQty,
            'itemInPrice' => $itemIn->itemInPrice,
            'itemInAverageCostingPrice' => $itemIn->itemInAverageCostingPrice,
            'itemInDiscount' => $itemIn->itemInDiscount,
            'itemInSoldQty' => $itemIn->itemInSoldQty,
            'itemInDateAndTime' => $itemIn->itemInDateAndTime,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function getFirstAvailableProductDetails($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInLocationProductID =$locationProductID ORDER BY  `itemInIndexID`;");
        $results = $statement->execute();

        $resultsArray = [];
        foreach ($results as $result) {
            if ($result['itemInRemainingQty'] > 0) {
                $resultsArray[] = $result;
            }
        }
        if (isset($resultsArray[0])) {
            return $resultsArray[0];
        } else {
            return NULL;
        }
    }

    public function getAvailableProductDetails($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInLocationProductID =$locationProductID ORDER BY  `itemInIndexID`;");
        $results = $statement->execute();

        $resultsArray = [];
        foreach ($results as $result) {
            if ($result['itemInRemainingQty'] > 0) {
                $resultsArray[] = $result;
            }
        }
        if ($resultsArray) {
            return $resultsArray;
        } else {
            return NULL;
        }
    }

    public function getNewItemInIndex($startingItemIndexID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT itemInIndex FROM (SELECT coalesce( itemInIndex, itemInID ) AS itemInIndex FROM itemIn WHERE coalesce( itemInIndex, itemInID ) >= $startingItemIndexID  ORDER BY itemInIndex LIMIT 2 ) as dd");
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result['itemInIndex'];
        }
        if (count($resultsArray) == 2) {
            return ($resultsArray[0] + $resultsArray[1]) / 2;
        } else {
            return NULL;
        }
    }

    public function getItemInDetails($itemInID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInID =$itemInID;");
        $results = $statement->execute();
        return $results->current();
    }

    public function getAllItemInDetails()
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn;");
        $results = $statement->execute();
        return $results;
    }

    public function getFirstAvailableProductDetailsByDocument($locationProductID, $documentType, $documentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInLocationProductID =$locationProductID AND   itemInDocumentType='$documentType'  AND     itemInDocumentID=$documentID  ORDER BY  `itemInIndexID`;");
        $results = $statement->execute();

        $resultsArray = [];
        foreach ($results as $result) {
            if ($result['itemInRemainingQty'] > 0) {
                $resultsArray[] = $result;
            }
        }
        if (isset($resultsArray[0])) {
            return $resultsArray[0];
        } else {
            return NULL;
        }
    }

    public function getFirstAvailableBatchProductDetails($locationProductID, $batchID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInLocationProductID =$locationProductID AND itemInBatchID=$batchID ORDER BY  `itemInIndexID`;");
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            if ($result['itemInRemainingQty'] > 0) {

                $resultsArray[] = $result;
            }
        }
        if (isset($resultsArray[0])) {
            return $resultsArray[0];
        } else {
            return NULL;
        }
    }

    public function getBatchProductDetails($locationProductID, $batchID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInLocationProductID =$locationProductID AND itemInBatchID=$batchID ORDER BY  `itemInIndexID`;");
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }

        return $resultsArray[0];
    }

    public function getFirstAvailableSerialProductDetails($locationProductID, $serialID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInLocationProductID=$locationProductID AND itemInSerialID=$serialID ORDER BY  `itemInIndexID`;");
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            if ($result['itemInRemainingQty'] > 0) {

                $resultsArray[] = $result;
            }
        }
        if (isset($resultsArray[0])) {
            return $resultsArray[0];
        } else {
            return NULL;
        }
    }

    public function getSerialProductDetails($locationProductID, $serialID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInLocationProductID=$locationProductID AND itemInSerialID=$serialID ORDER BY  `itemInIndexID`;");
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray[0];
    }

    public function getFirstAvailableBatchSerialProductDetails($locationProductID, $batchID, $serialID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInLocationProductID =$locationProductID AND itemInBatchID=$batchID AND itemInSerialID=$serialID ORDER BY  `itemInIndexID`;");
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            if ($result['itemInRemainingQty'] > 0) {

                $resultsArray[] = $result;
            }
        }
        if (isset($resultsArray[0])) {
            return $resultsArray[0];
        } else {
            return NULL;
        }
    }

    public function getBatchSerialProductDetails($locationProductID, $batchID, $serialID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT *,COALESCE ( `itemInIndex` , `itemInID` ) as `itemInIndexID`, itemInQty  - itemInSoldQty AS itemInRemainingQty FROM itemIn WHERE itemInLocationProductID =$locationProductID AND itemInBatchID=$batchID AND itemInSerialID=$serialID ORDER BY  `itemInIndexID`;");
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray[0];
    }

    public function updateItemInBatchProductSoldQty($locationProductID, $batchID, $soldQty)
    {
        $data = array('itemInSoldQty' => $soldQty);
        $result = $this->tableGateway->update($data, array(
            'itemInLocationProductID' => $locationProductID, 'itemInBatchID' => $batchID));
        return $result;
    }

    public function updateItemInSerialProductSoldQty($locationProductID, $serialID, $soldQty)
    {
        $data = array('itemInSoldQty' => $soldQty);
        $result = $this->tableGateway->update($data, array(
            'itemInLocationProductID' => $locationProductID, 'itemInSerialID' => $serialID));
        return $result;
    }

    public function updateItemInBatchSerialProductSoldQty($locationProductID, $batchID, $serialID, $soldQty)
    {
        $data = array('itemInSoldQty' => $soldQty);
        $result = $this->tableGateway->update($data, array(
            'itemInLocationProductID' => $locationProductID, 'itemInSerialID' => $serialID, 'itemInBatchID' => $batchID));
        return $result;
    }

    public function updateItemInSoldQtyDetails($itemInID, $soldQty)
    {
        $data = array('itemInSoldQty' => $soldQty);
        $result = $this->tableGateway->update($data, array('itemInID' => $itemInID));
        return $result;
    }

    public function getProductDetails($productIds, $locationIds, $categoryIDs = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from('itemIn');
        $select->columns(array('itemInID',
            'itemInDiscount',
            'itemInPrice',
            'qtySubtraction' => new Expression('(itemInQty-itemInSoldQty)'),
            'itemInDocumentID', 'itemInDocumentType', 'itemInLocationProductID'));
        $select->join('locationProduct', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array(
            'productID'));
        $select->join('location', 'location.locationID = locationProduct.locationID', array(
            'locationName', 'locationID', 'locationCode'), 'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productName', 'productCode'), 'left');
        if ($locationIds != NULL) {
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        if ($productIds != NULL) {
            $select->where->in('product.productID', $productIds);
        }
        if($categoryIDs != null){
            $select->where->in('product.categoryID', $categoryIDs);
        }
        $select->order(array('locationProduct.productID', 'locationProduct.locationID'));
        //$select->where->in('locationProduct.productID', $productIds);
        //$select->where->in('locationProduct.locationID', $locationIds);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getItemInProductDetails($locationProductID)
    {
        $rowset = $this->tableGateway->select(array('itemInLocationProductID' => $locationProductID));

        if ($rowset->count() == 0) {
            return NULL;
        } else {
            return $rowset;
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param array $productList
     * @param Date $fromDate
     * @param Date $toDate
     * @return array $resultsArray
     */
    public function getRangeItemInDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
        ->where->between('itemInDateAndTime', $fromDate, $toDate)
        ->where->notEqualTo('itemInDocumentType', 'Invoice Edit')
        ->where->notEqualTo('itemInDocumentType', 'Delivery Note Edit');
        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getTransferDetailsForMovements($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $documentDateFlag = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('transfer','itemIn.itemInDocumentID = transfer.transferID',array('*','documentDate' =>  new Expression('transferDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
        ->where->equalTo('itemInDocumentType', 'Inventory Transfer');
        $select->where(array('product.productState' => 1));
        if ($documentDateFlag) {
            if (!empty($fromDate) && !empty($toDate)) {
                $select->where->between('transferDate', $fromDate, $toDate);
            }
            if (!empty($fromDate) && is_null($toDate)) {
                $select->where->lessThan('transferDate', $fromDate);
            }
        } else {
            $select->where->between('itemInDateAndTime', $fromDate, $toDate);
        }
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
      

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray; 
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param Array $proList
     * @param Date $fromDate
     * @param Array $locationIds
     * @return Array $resultsList
     */
    public function getStockInRange($proList, $fromDate, $locationIds = [],$isNonInventoryProducts = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('availableInQty' => new Expression('SUM(itemInQty)'),'itemInPrice','itemInDocumentType','itemInDateAndTime','itemInDocumentID', 'itemInID'))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode','productTypeID'), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemIn.itemInDocumentType', 'product.productID'))
        ->where->lessThan('itemInDateAndTime', $fromDate)
        ->where->in('locationProduct.productID', $proList)
        ->where->notEqualTo('itemInDocumentType', 'Invoice Edit')
        ->where->notEqualTo('itemInDocumentType', 'Delivery Note Edit');
        $select->where(array('product.productState' => 1));
        if(!empty($locationIds)){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }


    public function getTransferedStockInRange($proList, $fromDate, $transferedLocationIds = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('transferedQuantity' => new Expression('SUM(itemInQty)'),'itemInPrice','itemInDocumentType','itemInDocumentID'))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode'), 'left')
                ->join('transfer','itemIn.itemInDocumentID = transfer.transferID',array('*'), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
        ->where->lessThan('itemInDateAndTime', $fromDate)
        ->where->equalTo('itemInDocumentType', 'Inventory Transfer')
        ->where->in('locationProduct.productID', $proList);
        $select->where(array('product.productState' => 1));
        if(!empty($transferedLocationIds)){
            $select->where->in('locationProduct.locationID', $transferedLocationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function getTransferStockInRange($proList, $fromDate, $transferedLocationIds = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('transferedQuantity' => new Expression('SUM(itemInQty)'),'itemInPrice','itemInDocumentType','itemInDocumentID'))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode'), 'left')
                ->join('transfer','itemIn.itemInDocumentID = transfer.transferID',array('*'), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
        ->where->lessThan('itemInDateAndTime', $fromDate)
        ->where->equalTo('itemInDocumentType', 'Inventory Transfer')
        ->where->in('locationProduct.productID', $proList);
        $select->where(array('product.productState' => 1));
        $select->where->in('transfer.locationIDOut',$transferedLocationIds);
       
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    public function getTransferedlocationIds($locationIds = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('trnasferedLocationId' => new Expression('locationIDIn')))
                ->where->in('transfer.locationIDOut', $locationIds);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * getting data of Credit note between given Date range
     * @param Date $fromDate
     * @param Date $toDate
     * @return Array $resultsList
     */
    public function getCreditNoteDetails($fromDate, $toDate, $cusCategory = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*',
                    'cNoteTotalCost' => new Expression('((itemInQty*itemInPrice))')))
                ->join('creditNote', 'creditNote.creditNoteID = itemIn.itemInDocumentID', array('*',
                    'date' => new Expression('creditNoteDate')), 'left')
                ->join('location', 'location.locationID = creditNote.locationID', array('locationName', 'locationCode'), 'left')
                ->join("customer", "creditNote.customerID = customer.customerID", array('customerName','customerCode'), 'left')
                ->where(array('itemInDocumentType' => 'Credit Note'))
        ->where->between('creditNote.creditNoteDate', $fromDate, $toDate);
        if($cusCategory != ""){
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function truncateTable()
    {
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("TRUNCATE TABLE `itemIn`;");
        $results = $statement->execute();
    }

    public function getItemInDataByDocumentTypeAndID($itemInIndex, $itemInDocType)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
                ->where(array('itemInDateAndTime' => $itemInIndex, '' => $itemInDocType));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function checkProductByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
                ->where(array('itemInLocationProductID' => $locationProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function updateItemInDataByItemInId($itemInData, $itemInID)
    {
        try {
            $result = $this->tableGateway->update($itemInData, array('itemInID' => $itemInID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get stock aging details
     * @param string $date
     * @param array $productIds
     * @return mixed
     */
    public function getStockAgeDetails($date, $productIds = [],$categoryIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(['productID','productCode', 'productName', 'entityID', 'productTypeID'])
                ->join('locationProduct', 'product.productID = locationProduct.productID', ['locationProductID','locationID'],'left')
                ->join('itemIn', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', [
                    'WithIn30StockValue'  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 0 AND 30 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    'WithIn30Qty'  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 0 AND 30 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "WithIn90StockValue"  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 30 AND 90 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end "),
                    "WithIn90Qty"  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 30 AND 90 then (SUM(itemInQty-itemInSoldQty)) end "),
                    "WithIn180StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 90 AND 180 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn180Qty" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 90 AND 180 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "WithIn270StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 180 AND 270 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn270Qty" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 180 AND 270 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "Over270StockValue"   => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) > 270 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "Over270Qty"   => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) > 270 then (SUM(itemInQty-itemInSoldQty)) end")
                    ], 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->where(['productTypeID' => 1, 'deleted' => 0]);
        if ($productIds) {
            $select->where->in('product.productID', $productIds);
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);   
        }
        $select->order(['product.productID']);
        $select->group(['locationProduct.locationProductID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get stock aging details
     * @param string $date
     * @param array $productIds
     * @return mixed
     */
    public function getStockAgeDetailsForAvg($date, $productIds = [],$categoryIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(['productID','productCode', 'productName', 'entityID', 'productTypeID'])
                ->join('locationProduct', 'product.productID = locationProduct.productID', ['locationProductID','locationID', 'locationProductAverageCostingPrice'],'left')
                ->join('itemIn', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', [
                    'WithIn30StockValue'  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 0 AND 30 then (SUM((itemInQty-itemInSoldQty) * locationProductAverageCostingPrice)) end"),
                    'WithIn30Qty'  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 0 AND 30 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "WithIn90StockValue"  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 30 AND 90 then (SUM((itemInQty-itemInSoldQty) * locationProductAverageCostingPrice)) end "),
                    "WithIn90Qty"  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 30 AND 90 then (SUM(itemInQty-itemInSoldQty)) end "),
                    "WithIn180StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 90 AND 180 then (SUM((itemInQty-itemInSoldQty) * locationProductAverageCostingPrice)) end"),
                    "WithIn180Qty" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 90 AND 180 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "WithIn270StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 180 AND 270 then (SUM((itemInQty-itemInSoldQty) * locationProductAverageCostingPrice)) end"),
                    "WithIn270Qty" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 180 AND 270 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "Over270StockValue"   => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) > 270 then (SUM((itemInQty-itemInSoldQty) * locationProductAverageCostingPrice)) end"),
                    "Over270Qty"   => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) > 270 then (SUM(itemInQty-itemInSoldQty)) end")
                    ], 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->where(['productTypeID' => 1, 'deleted' => 0]);
        if ($productIds) {
            $select->where->in('product.productID', $productIds);
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);   
        }
        $select->order(['product.productID']);
        $select->group(['locationProduct.locationProductID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getItemInSoldDetailsByDocumentTypeAndDocumentID($documentID, $documentType, $type = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', ['productID'],'left')
                ->where(array('itemInDocumentType' => $documentType, 'itemInDocumentID' => $documentID));
        if($type){
            $select->where->notEqualTo('itemInSoldQty', 0);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function getSoldQtyByBatchOrSerialID($batchID = NULL, $serialID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('totalQty' => new Expression('SUM(itemInSoldQty)')));
        if($batchID != ''){
            $select->where(array('itemInBatchID' => $batchID));
        }
        if($serialID != ''){
            $select->where(array('itemInSerialID' => $serialID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getItemInDetailsByBatchIDOrSerialID($batchID = NULL, $serialID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'));
        if($batchID != ''){
            $select->where(array('itemInBatchID' => $batchID));
        } else if($serialID != ''){
            $select->join('productSerial','itemIn.itemInSerialID = productSerial.productSerialID',array('locationProductID','productSerialCode'),'left');
            $select->where(array('itemInSerialID' => $serialID));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function getProductDetailsBylocationProductIDDocTypeAndDocumentID($locationProductID, $docType, $documentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
                ->where(array('itemInLocationProductID' => $locationProductID, 'itemInDocumentType' => $docType , 'itemInDocumentID' => $documentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getItemInDetailsBydocumentIDLocationProdcutIDQtyAndAvgCosting($locationProID, $docType, $docID, $itemQty, $price)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
        ->where(array('itemInLocationProductID' => $locationProID, 'itemInDocumentType' => $docType, 'itemInDocumentID' => $docID, 'itemInQty' => $itemQty, 'itemInPrice' => $price));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAllDetailsByLocationProductIDAndDateRange($locationProductID, $setDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
        ->where->greaterThan('itemInDateAndTime', $setDate);
        $select->where(array('itemInLocationProductID' => $locationProductID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getTotalCostByLocationProductIDAndDateRange($locationProductID, $inID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('totalCost' => new Expression('SUM(itemInPrice*itemInQty)'),'totalQty' => new Expression('SUM(itemInQty)')))
        ->where->greaterThan('itemInID', $inID);
        $select->where(array('itemInLocationProductID' => $locationProductID, 'itemInDeletedFlag' => 0));
        $select->group('itemInLocationProductID');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAvgCostingValueForSelectedID($locationProductID, $inID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
        ->where->lessThan('itemInID', $inID);
        $select->where(array('itemInLocationProductID' => $locationProductID, 'itemInDeletedFlag' => 0));
        $select->order('itemInID DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results; 
    }

    public function getVeryNextRecordByIDAndLocationProductID($locationProductID,$inID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
        ->where->greaterThan('itemInID', $inID);
        $select->where(array('itemInLocationProductID' => $locationProductID, 'itemInDeletedFlag' => 0));
        $select->order('itemInID ASC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results; 
    }

    public function getAllNextRecordByInIDAndLocationProductID($locationProductID,$inID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
        ->where->greaterThanOrEqualTo('itemInID', $inID);
        $select->where(array('itemInLocationProductID' => $locationProductID, 'itemInDeletedFlag' => 0));
        $select->order('itemInID ASC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results; 
    }

    /**
     * Get item in document batch item remaining quantity
     */
    public function getBatchItemInDetails($documentType, $documentId, $batchId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('remainingQty' => new Expression('itemInQty - itemInSoldQty')));
        $select->where(array('itemInDocumentType' => $documentType, 'itemInDocumentID' => $documentId, 'itemInBatchID' => $batchId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    /**
     * Check whether item in
     */
    public function hasItemIn()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->limit(1)
                ->where->equalTo('itemInDeletedFlag', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if($results->count() == 0) {
            return false;
        }

        return true;
    }


    public function getItemInDetailsForAvgCosting($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('totalAvilableStockQty' => new Expression('SUM(itemInQty)'),'totalAvilableStockValue' => new Expression('SUM(itemInQty*(itemInPrice-itemInDiscount))')));
        $select->where(array('itemInLocationProductID' => $locationProductID,'itemInIndex' => NULL));
        $select->where->notEqualTo('itemInDeletedFlag',1);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results; 
    }

    public function getCanceledItemInProductDetails($canceledPiId, $locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'));
        $select->where(array('itemInDocumentID' => $canceledPiId,'itemInLocationProductID' => $locationProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results; 
    }

    public function getItemInIdByGrnId($grnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('itemInID'));
        $select->where(array('itemInDocumentID' => $grnID,'itemInDocumentType' => "Goods Received Note"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results; 
    }

    public function getItemInDataByTranferId($transferID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'));
        $select->where(array('itemInDocumentID' => $transferID,'itemInDocumentType' => "Inventory Transfer", "itemInLocationProductID" => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results; 
    }

    public function getAllNextRecordByInIDAndLocationProductIDInDecendingOrder($locationProductID,$inID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
        ->where->greaterThanOrEqualTo('itemInID', $inID);
        $select->where(array('itemInLocationProductID' => $locationProductID, 'itemInDeletedFlag' => 0));
        $select->order('itemInID DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results; 
    }

    public function getVeryPreviousRecordByIDAndLocationProductID($locationProductID,$inID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
        ->where->lessThan('itemInID', $inID);
        $select->where(array('itemInLocationProductID' => $locationProductID, 'itemInDeletedFlag' => 0));
        $select->order('itemInID DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results; 

    }

    public function getTransferOutDataByLocationProductIDAndDate($locationProductID, $date = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('totalQty' => new Expression('SUM(itemInQty)')))
                ->where->greaterThan('itemInDateAndTime', $date);
        $select->where(array('itemInLocationProductID' => $locationProductID, 'itemInDeletedFlag' => 0, 'itemInDocumentType' => "Inventory Transfer"));
        $select->group('itemInLocationProductID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getTransferDetailsByGivenDateRangeAndLOcationProductID($locationProductID,$fromDate,$toDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
        ->where(array('itemInLocationProductID' => $locationProductID, 'itemInDeletedFlag' => 0,'itemInDocumentType' => "Inventory Transfer"));
        if($toDate){
            $select->where->between('itemInDateAndTime', $fromDate,$toDate);
            
        } else {
            $select->where->greaterThan('itemInDateAndTime', $fromDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getSerialItemDetails($itemIds = [], $serialIds = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('itemInID','itemInDocumentType','itemInDocumentID','itemInSoldQty'))
                ->join("itemOut", "itemIn.itemInID = itemOut.itemOutItemInID", array('itemOutID','itemOutDocumentType','itemOutDocumentID'), 'left')
                ->join('locationProduct','itemIn.itemInLocationProductID = locationProduct.locationProductID',array('productID','locationID'),'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productCode','productName'))
                ->join('location','locationProduct.locationID = location.locationID', array('locationCode','locationName'))
                ->join('productSerial','itemIn.itemInSerialID = productSerial.productSerialID', array('productSerialID','productSerialCode'),'left')
                ->join(array('outInvoice' => 'salesInvoice'),'itemOut.itemOutDocumentID = outInvoice.salesInvoiceID', array('outInvId' => new Expression('outInvoice.salesInvoiceID'),'outInvCode'=> new Expression('outInvoice.salesInvoiceCode')), 'left')
                ->join(array('inInvoice' => 'salesInvoice'),'itemIn.itemInDocumentID = inInvoice.salesInvoiceID', array('inInvId' => new Expression('inInvoice.salesInvoiceID'),'inInvCode'=> new Expression('inInvoice.salesInvoiceCode')), 'left')
                ->join(array('outDelNote' => 'deliveryNote'),'itemOut.itemOutDocumentID = outDelNote.deliveryNoteID', array('outDelId' => new Expression('outDelNote.deliveryNoteID'), 'outDelCode' => new Expression('outDelNote.deliveryNoteCode')), 'left')
                ->join(array('inDelNote' => 'deliveryNote'),'itemIn.itemInDocumentID = inDelNote.deliveryNoteID', array('inDelId' => new Expression('inDelNote.deliveryNoteID'), 'inDelCode' => new Expression('inDelNote.deliveryNoteCode')), 'left')
                ->join('salesReturn', 'itemIn.itemInDocumentID = salesReturn.salesReturnID', array('salesReturnID','salesReturnCode'), 'left')
                ->join(array('inCreditNote' => 'creditNote'), 'itemIn.itemInDocumentID = inCreditNote.creditNoteID', array('inCredId' => new Expression('inCreditNote.creditNoteID'), 'inCredCode' => new Expression('inCreditNote.creditNoteCode')), 'left')
                ->join(array('outCreditNote' => 'creditNote'), 'itemOut.itemOutDocumentID = outCreditNote.creditNoteID', array('outCredId' => new Expression('outCreditNote.creditNoteID'), 'outCredCode' => new Expression('outCreditNote.creditNoteCode')), 'left')
                ->join(array('outGrn' => 'grn'), 'itemOut.itemOutDocumentID = outGrn.grnID', array('outGrnID' => new Expression('outGrn.grnID'), 'outGrnCode' => new Expression('outGrn.grnCode')), 'left')
                ->join(array('inGrn' => 'grn'), 'itemIn.itemInDocumentID = inGrn.grnID', array('inGrnID' => new Expression('inGrn.grnID'), 'inGrnCode' => new Expression('inGrn.grnCode')), 'left')
                ->join(array('outAdjustment' => 'goodsIssue'), 'itemOut.itemOutDocumentID = outAdjustment.goodsIssueID', array('outAdjID' => new Expression('outAdjustment.goodsIssueID'), 'outAdjCode' => new Expression('outAdjustment.goodsIssueCode')), 'left')
                ->join(array('inAdjustment' => 'goodsIssue'), 'itemIn.itemInDocumentID = inAdjustment.goodsIssueID', array('inAdjID' => new Expression('inAdjustment.goodsIssueID'), 'inAdjCode' => new Expression('inAdjustment.goodsIssueCode')), 'left')
                ->join(array('inPurchaseInvo' => 'purchaseInvoice'), 'itemIn.itemInDocumentID = inPurchaseInvo.purchaseInvoiceID', array('inPIID' => new Expression('inPurchaseInvo.purchaseInvoiceID'), 'inPICode' => new Expression('inPurchaseInvo.purchaseInvoiceCode')), 'left')
                ->join(array('outPurchaseInvo' => 'purchaseInvoice'), 'itemOut.itemOutDocumentID = outPurchaseInvo.purchaseInvoiceID', array('outPIID' => new Expression('outPurchaseInvo.purchaseInvoiceID'), 'outPICode' => new Expression('outPurchaseInvo.purchaseInvoiceCode')), 'left')
                ->join(array('outPaymentVoucher' => 'paymentVoucher'), 'itemOut.itemOutDocumentID = outPaymentVoucher.paymentVoucherID', array('outPVID' => new Expression('outPaymentVoucher.paymentVoucherID'), 'outPVCode' => new Expression('outPaymentVoucher.paymentVoucherCode')), 'left')
                ->join(array('inPaymentVoucher' => 'paymentVoucher'), 'itemIn.itemInDocumentID = inPaymentVoucher.paymentVoucherID', array('inPVID' => new Expression('inPaymentVoucher.paymentVoucherID'), 'inPVCode' => new Expression('inPaymentVoucher.paymentVoucherCode')), 'left')
                ->join('purchaseReturn', 'itemOut.itemOutDocumentID = purchaseReturn.purchaseReturnID', array('purchaseReturnID','purchaseReturnCode'), 'left')
                ->join('transfer', 'itemIn.itemInDocumentID = transfer.transferID', array('transferID','transferCode','locationIDIn','locationIDOut'), 'left');
        if ($itemIds) {
            $select->where->in('product.productID', $itemIds);
        }
        if ($serialIds) {
            $select->where->in('itemIn.itemInSerialID', $serialIds);
        } else {
            $select->where->equalTo('product.serialProduct',1);
        }
        $select->where(array('product.productState' => 1));  
        $select->order('itemIn.itemInID');  
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * this function use to get itemIn total cost and total qty for given document type
    * @param string $documentType
    * return mix
    **/
    public function getItemInQtyAndCostByItemOutDocumentType($documentType) {
        
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT sum(`itemIn`.`itemInPrice`*`itemOut`.`itemOutQty`) as totalValue, sum(`itemOut`.`itemOutQty`) as totalQt, `itemOut`.`itemOutLocationProductID`,`itemOut`.`itemOutDocumentID` FROM `itemIn` JOIN `itemOut` on `itemIn`.`itemInID` = `itemOut`.`itemOutItemInID` WHERE `itemOut`.`itemOutDocumentType`='$documentType' GROUP BY `itemOut`.`itemOutDocumentID`,`itemOut`.`itemOutLocationProductID`;");
        $results = $statement->execute();
        return $results;
    }

    /**
    * get very next item in record for given date
    * @param date $date
    * @param array $locationIDs
    * @param array $proIDs
    * return array
    **/
    public function getAllItemDetailsAfterSelectedDate($date, $locationIDs, $proIDs) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
                ->join('locationProduct','itemIn.itemInLocationProductID = locationProduct.locationProductID', array('productID'),'left')
                ->where->lessthan('itemIn.itemInDateAndTime', $date);
        $select->where->in('locationProduct.locationID', $locationIDs);
        $select->where->in('locationProduct.productID', $proIDs);
        $select->order('itemIn.itemInID DESC');
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * this function use to get last inserted detals that related to the given document type
    * given location id(s) and given product id(s)
    * @param array $documentType
    * @param array $locationID
    * @param array $productID
    * @param boolean $allProduct
    * return mix
    **/
    public function getLastInsertedItemValuesByDocumentTypeLocationIDAndProductID($documentType = [], $locationID = [], $productID = [], $allProduct = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('*'))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', ['productID', 'locationID'],'left');
        $select->where->in('itemIn.itemInDocumentType', $documentType);
        $select->where->in('locationProduct.locationID', $locationID);
        if (!$allProduct) {
            $select->where->in('locationProduct.productID', $productID);
        }
        $select->order('itemIn.itemInID DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getRangeItemInDetailsOfSalesReturnWithDocumentDate($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();

        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('salesReturn', 'itemIn.itemInDocumentID = salesReturn.salesReturnID', array('documentDate' => new Expression('salesReturn.salesReturnDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->greaterThan('salesReturn.salesReturnDate', $fromDate)
            ->where->lessThan('salesReturn.salesReturnDate', $toDate)
            ->where->in('itemInDocumentType',['Sales Returns','Direct Return']);
        } 


        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('salesReturn', 'itemIn.itemInDocumentID = salesReturn.salesReturnID', array('salesReturnCode','documentDate' => new Expression('salesReturn.salesReturnDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->lessThan('salesReturn.salesReturnDate', $fromDate)
            ->where->in('itemInDocumentType',['Sales Returns','Direct Return']);
        }

        
        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemInDetailsOfSalesInvoiceWithDocumentDate($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('salesInvoice','itemIn.itemInDocumentID = salesInvoice.salesInvoiceID', array('documentDate' => new Expression('salesInvoice.salesInvoiceIssuedDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate)
            ->where->in('itemInDocumentType', ['POS Invoice Cancel','Invoice Cancel']);
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('salesInvoice','itemIn.itemInDocumentID = salesInvoice.salesInvoiceID', array('salesInvoiceCode','documentDate' => new Expression('salesInvoice.salesInvoiceIssuedDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->lessThan('salesInvoice.salesInvoiceIssuedDate', $fromDate)
            ->where->in('itemInDocumentType', ['POS Invoice Cancel','Invoice Cancel']);
        }

        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemInDetailsOfCreditNoteWithDocumentDate($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('creditNote', 'itemIn.itemInDocumentID = creditNote.creditNoteID',array('documentDate' => new Expression('creditNoteDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->between('creditNoteDate', $fromDate, $toDate)
            ->where->in('itemInDocumentType',['Credit Note','Direct Credit Note', 'Buy Back Credit Note']);
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('creditNote', 'itemIn.itemInDocumentID = creditNote.creditNoteID',array('creditNoteCode','documentDate' => new Expression('creditNoteDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->lessThan('creditNoteDate', $fromDate)
            ->where->in('itemInDocumentType',['Credit Note','Direct Credit Note', 'Buy Back Credit Note']);
        }

        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemInDetailsOfGrnWithDocumentDate($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();


        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('grn', 'itemIn.itemInDocumentID = grn.grnID', array('documentDate' => new Expression('grnDate'), 'status'), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->between('grnDate', $fromDate, $toDate)
            ->where->equalTo('itemInDocumentType','Goods Received Note')
            ->where->notEqualTo('grn.status', 10);
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('grn', 'itemIn.itemInDocumentID = grn.grnID', array('grnCode','documentDate' => new Expression('grnDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->lessThan('grnDate', $fromDate)
            ->where->equalTo('itemInDocumentType','Goods Received Note');
        }
        
        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemInDetailsOfPostiveAdjustmentWithDocumentDate($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('goodsIssue', 'itemIn.itemInDocumentID = goodsIssue.goodsIssueID', array('documentDate' => new Expression('goodsIssueDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->between('goodsIssueDate', $fromDate, $toDate)
            ->where->in('itemInDocumentType',['Inventory Positive Adjustment','Delete Negative Adjustment']);
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('goodsIssue', 'itemIn.itemInDocumentID = goodsIssue.goodsIssueID', array('goodsIssueCode','documentDate' => new Expression('goodsIssueDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->lessThan('goodsIssueDate', $fromDate)
            ->where->in('itemInDocumentType',['Inventory Positive Adjustment','Delete Negative Adjustment']);
        }


        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemInDetailsOfPurchaseInvoiceWithDocumentDate($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('purchaseInvoice', 'itemIn.itemInDocumentID = purchaseInvoice.purchaseInvoiceID',  array('documentDate' => new Expression('purchaseInvoiceIssueDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->between('purchaseInvoiceIssueDate', $fromDate, $toDate)
            ->where->equalTo('itemInDocumentType','Payment Voucher');
        }

        if  (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('purchaseInvoice', 'itemIn.itemInDocumentID = purchaseInvoice.purchaseInvoiceID',  array('purchaseInvoiceCode','documentDate' => new Expression('purchaseInvoiceIssueDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->lessThan('purchaseInvoiceIssueDate', $fromDate)
            ->where->equalTo('itemInDocumentType','Payment Voucher');
        }



        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemInDetailsOfExpensePurchaseVoucherWithDocumentDate($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        

        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('paymentVoucher', 'itemIn.itemInDocumentID = paymentVoucher.paymentVoucherID', array('documentDate' => new Expression('paymentVoucherIssuedDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->between('paymentVoucherIssuedDate', $fromDate, $toDate)
            ->where->equalTo('itemInDocumentType','Expense Payment Voucher');
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('paymentVoucher', 'itemIn.itemInDocumentID = paymentVoucher.paymentVoucherID', array('paymentVoucherCode','documentDate' => new Expression('paymentVoucherIssuedDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->lessThan('paymentVoucherIssuedDate', $fromDate)
            ->where->equalTo('itemInDocumentType','Expense Payment Voucher');
        }

        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemInDetailsOfTransferWithDocumentDate($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('transfer', 'itemIn.itemInDocumentID = transfer.transferID', array('documentDate' => new Expression('transferDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->between('transferDate', $fromDate, $toDate)
            ->where->equalTo('itemInDocumentType','Inventory Transfer');
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'totalInQty' => new Expression('SUM(itemInQty)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('transfer', 'itemIn.itemInDocumentID = transfer.transferID', array('documentDate' => new Expression('transferDate')), 'left')
                ->group(array('itemIn.itemInDocumentID', 'itemInDocumentType', 'product.productID'))
            ->where->lessThan('transferDate', $fromDate)
            ->where->equalTo('itemInDocumentType','Inventory Transfer');
        }

        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getItemInDetailsOfTransferWithDocumentDateAndLocID($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false, $isAvoidGrouping = false, $isActualDate = false)
    {

        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        if ($isAvoidGrouping) {
                $select->from('itemIn')
                ->columns(array(
                    '*'
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('transfer', 'itemIn.itemInDocumentID = transfer.transferID', array('transferDate'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        } else {
            $select->from('itemIn')
                ->columns(array(
                    '*',
                    'qtyTransferIn' => new Expression('SUM(itemInQty)'),
                    'inCost' => new Expression('SUM(itemInQty*itemInPrice)')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('transfer', 'itemIn.itemInDocumentID = transfer.transferID', array('transferDate'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        }
        
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }
        if ($isActualDate) {
            $select->where->greaterThan('itemIn.itemInDateAndTime', $fromDate);
            $select->where->lessThan('itemIn.itemInDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('transfer.transferDate', $fromDate);
            $select->where->lessThan('transfer.transferDate', $toDate);
        }
        
        $select->where->equalTo('itemInDocumentType','Inventory Transfer');
        if (!$isAvoidGrouping) {
            $select->group(array('locationProduct.productID'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getStockAgeDetailsForDashbaordByYear($date, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(['productID','productCode', 'productName', 'entityID', 'productTypeID'])
                ->join('locationProduct', 'product.productID = locationProduct.productID', ['locationProductID','locationID'],'left')
                ->join('itemIn', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', [
                    'WithIn30StockValue'  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 0 AND 30 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    'WithIn30Qty'  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 0 AND 30 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "WithIn90StockValue"  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 30 AND 90 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end "),
                    "WithIn90Qty"  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 30 AND 90 then (SUM(itemInQty-itemInSoldQty)) end "),
                    "WithIn180StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 90 AND 180 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn180Qty" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 90 AND 180 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "WithIn270StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 180 AND 270 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn270Qty" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 180 AND 270 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "WithIn360StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 270 AND 360 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn360Qty" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 270 AND 360 then (SUM(itemInQty-itemInSoldQty)) end"),
                    "Over360StockValue"   => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) > 360 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "Over360Qty"   => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) > 360 then (SUM(itemInQty-itemInSoldQty)) end")
                    ], 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->where(['productTypeID' => 1, 'deleted' => 0]);
        $select->order(['product.productID']);
        $select->group(['locationProduct.locationProductID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getStockAgeDetailsForDashbaordByMonth($date, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(['productID','productCode', 'productName', 'entityID', 'productTypeID'])
                ->join('locationProduct', 'product.productID = locationProduct.productID', ['locationProductID','locationID'],'left')
                ->join('itemIn', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', [
                    'WithIn5StockValue'  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 0 AND 5 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn10StockValue"  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 5 AND 10 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end "),
                    "WithIn15StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 10 AND 15 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn20StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 15 AND 20 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn25StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 20 AND 25 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn30StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 25 AND 30 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end")
                    ], 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->where(['productTypeID' => 1, 'deleted' => 0]);
        $select->order(['product.productID']);
        $select->group(['locationProduct.locationProductID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getStockAgeDetailsForDashbaordByWeek($date, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(['productID','productCode', 'productName', 'entityID', 'productTypeID'])
                ->join('locationProduct', 'product.productID = locationProduct.productID', ['locationProductID','locationID'],'left')
                ->join('itemIn', 'locationProduct.locationProductID = itemIn.itemInLocationProductID', [
                    'WithIn2StockValue'  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 0 AND 2 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn4StockValue"  => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 2 AND 4 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end "),
                    "WithIn6StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 4 AND 6 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end"),
                    "WithIn8StockValue" => new Expression("case when DATEDIFF('" . $date . "', DATE_FORMAT(`itemInDateAndTime`, '%Y-%m-%d')) BETWEEN 6 AND 8 then (SUM((itemInQty-itemInSoldQty) * itemInPrice)) end")
                    ], 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                ->where(['productTypeID' => 1, 'deleted' => 0]);
        $select->order(['product.productID']);
        $select->group(['locationProduct.locationProductID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * getting data of Credit note between given Date range
     * @param Date $fromDate
     * @param Date $toDate
     * @return Array $resultsList
     */
    public function getCreditNoteDetailsForDashboard($fromDate, $toDate, $cusCategory = NULL, $locationID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn')
                ->columns(array('itemInID','itemInIndex','itemInDocumentType','itemInDocumentID', 'itemInLocationProductID', 'itemInQty', 'itemInPrice', 'itemInDiscount',
                    'cNoteTotalCost' => new Expression('((itemInQty*itemInPrice))')))
                ->join('creditNote', 'creditNote.creditNoteID = itemIn.itemInDocumentID', array('creditNoteID','creditNoteCode','creditNoteTotal','locationID', 
                    'date' => new Expression('creditNoteDate')), 'left')
                // ->join('location', 'location.locationID = creditNote.locationID', array('locationName', 'locationCode'), 'left')
                // ->join("customer", "creditNote.customerID = customer.customerID", array('customerName','customerCode'), 'left')
                ->where(array('itemInDocumentType' => 'Credit Note', 'locationID' => $locationID))
        ->where->between('creditNote.creditNoteDate', $fromDate, $toDate);
        // if($cusCategory != ""){
        //     $select->where->in('customer.customerCategory', $cusCategory);
        // }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }
}
