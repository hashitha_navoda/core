<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class FreeIssueDetails
{

    public $freeIssueDetailID;
    public $majorProductID;
    public $majorConditionType;
    public $majorQuantity;
    public $selectedMajorUom;
    public $locationID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->freeIssueDetailID = (!empty($data['freeIssueDetailID'])) ? $data['freeIssueDetailID'] : null;
        $this->majorProductID = (!empty($data['majorProductID'])) ? $data['majorProductID'] : null;
        $this->majorConditionType = (!empty($data['majorConditionType'])) ? $data['majorConditionType'] : null;
        $this->majorQuantity = (!empty($data['majorQuantity'])) ? $data['majorQuantity'] : null;
        $this->selectedMajorUom = (!empty($data['selectedMajorUom'])) ? $data['selectedMajorUom'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
