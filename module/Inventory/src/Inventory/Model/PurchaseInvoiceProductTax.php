<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the PIProductTax model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseInvoiceProductTax implements InputFilterAwareInterface
{

    public $purchaseInvoiceProductTaxID;
    public $purchaseInvoiceID;
    public $purchaseInvoiceProductID;
    public $purchaseInvoiceTaxID;
    public $purchaseInvoiceTaxPrecentage;
    public $purchaseInvoiceTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseInvoiceProductTaxID = (!empty($data['purchaseInvoiceProductTaxID'])) ? $data['purchaseInvoiceProductTaxID'] : null;
        $this->purchaseInvoiceID = (!empty($data['purchaseInvoiceID'])) ? $data['purchaseInvoiceID'] : null;
        $this->purchaseInvoiceProductID = (!empty($data['purchaseInvoiceProductID'])) ? $data['purchaseInvoiceProductID'] : null;
        $this->purchaseInvoiceTaxID = (!empty($data['purchaseInvoiceTaxID'])) ? $data['purchaseInvoiceTaxID'] : null;
        $this->purchaseInvoiceTaxPrecentage = (!empty($data['purchaseInvoiceTaxPrecentage'])) ? $data['purchaseInvoiceTaxPrecentage'] : null;
        $this->purchaseInvoiceTaxAmount = (!empty($data['purchaseInvoiceTaxAmount'])) ? $data['purchaseInvoiceTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

