<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains price list locations Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PriceListLocations implements InputFilterAwareInterface
{

    public $priceListLocationsId;
    public $priceListId;
    public $locationId;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->priceListLocationsId = (!empty($data['priceListLocationsId'])) ? $data['priceListLocationsId'] : null;
        $this->priceListId = (!empty($data['priceListId'])) ? $data['priceListId'] : null;
        $this->locationId = (!empty($data['locationId'])) ? $data['locationId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
