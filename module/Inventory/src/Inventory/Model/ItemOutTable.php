<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\Predicate;

class ItemOutTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveItemOut(ItemOut $itemOut)
    {
        $data = array(
            'itemOutDocumentType' => $itemOut->itemOutDocumentType,
            'itemOutDocumentID' => $itemOut->itemOutDocumentID,
            'itemOutLocationProductID' => $itemOut->itemOutLocationProductID,
            'itemOutBatchID' => $itemOut->itemOutBatchID,
            'itemOutSerialID' => $itemOut->itemOutSerialID,
            'itemOutItemInID' => $itemOut->itemOutItemInID,
            'itemOutQty' => $itemOut->itemOutQty,
            'itemOutPrice' => $itemOut->itemOutPrice,
            'itemOutAverageCostingPrice' => $itemOut->itemOutAverageCostingPrice,
            'itemOutDiscount' => $itemOut->itemOutDiscount,
            'itemOutReturnQty' => $itemOut->itemOutReturnQty,
            'itemOutDateAndTime' => $itemOut->itemOutDateAndTime,
            'itemOutDeletedFlag' => $itemOut->itemOutDeletedFlag,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function getItemOutDetailsForProductByDocument($locationProductID, $documentType, $documentID)
    {
        $rowset = $this->tableGateway->select(array('itemOutLocationProductID' => $locationProductID, 'itemOutDocumentType' => $documentType, 'itemOutDocumentID' => $documentID));

        $resultsArray = [];
        foreach ($rowset as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function getSerialItemOutDetails($serialID,$documentType, $documentID)
    {
        $rowset = $this->tableGateway->select(array('itemOutSerialID' => $serialID, 'itemOutDocumentType' => $documentType, 'itemOutDocumentID' => $documentID));
        return $rowset->current();       
    }

    public function getBatchItemOutDetails($batchID,$documentType, $documentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = new select();
        $select->from('itemOut')
                ->columns(array('*'))
                ->where(array('itemOutBatchID' => $batchID, 'itemOutDocumentType' => $documentType, 'itemOutDocumentID' => $documentID))
        ->where('itemOutReturnQty != itemOutQty');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results->current();
    }

    public function getBatchSerialItemOutDetails($serialID,$batchID,$documentType, $documentID)
    {
        $rowset = $this->tableGateway->select(array('itemOutSerialID' => $serialID,'itemOutBatchID' => $batchID, 'itemOutDocumentType' => $documentType, 'itemOutDocumentID' => $documentID));
        return $rowset->current();
    }



    public function updateItemOutReturnQty($itemOutID, $returnQty)
    {
        $data = array('itemOutReturnQty' => $returnQty);
        $result = $this->tableGateway->update($data, array('itemOutID' => $itemOutID));
        return $result;
    }

    public function getRangeItemOutDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('salesInvoice', new Expression('itemOut.itemOutDocumentID = salesInvoice.salesInvoiceID AND itemOut.itemOutDocumentType = "Sales Invoice"'), array('salesInvoiceCode', 'salesInvoiceStatus' => new Expression('salesInvoice.statusID')), 'left')
                ->join('deliveryNote', new Expression('itemOut.itemOutDocumentID = deliveryNote.deliveryNoteID AND itemOut.itemOutDocumentType = "Delivery Note"'), array('deliveryNoteCode', 'deliveryNoteStatus'), 'left')
                ->join('goodsIssue', new Expression('itemOut.itemOutDocumentID = goodsIssue.goodsIssueID AND (itemOut.itemOutDocumentType = "Inventory Negative Adjustment" OR itemOut.itemOutDocumentType = "Delete Positive Adjustment")'), array('goodsIssueCode', 'goodsIssueStatus'), 'left')
                ->join('purchaseReturn', new Expression('itemOut.itemOutDocumentID = purchaseReturn.purchaseReturnID AND (itemOut.itemOutDocumentType = "Purchase Return" OR itemOut.itemOutDocumentType = "Direct Purchase Return")'), array('purchaseReturnCode', 'purchaseReturnStatus'), 'left')
                ->join('debitNote', new Expression('itemOut.itemOutDocumentID = debitNote.debitNoteID AND itemOut.itemOutDocumentType = "Debit Note"'), array('debitNoteCode', 'debitNoteStatus' => new Expression('debitNote.statusID')), 'left')
                ->join('activity', new Expression('itemOut.itemOutDocumentID = activity.activityId AND itemOut.itemOutDocumentType = "Activity"'), array('activityCode'), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
        ->where->between('itemOutDateAndTime', $fromDate, $toDate);
        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            if(!($v['salesInvoiceStatus'] == 10 || $v['salesInvoiceStatus'] == 8 || $v['deliveryNoteStatus'] == 10)){
                $resultsArray[] = $v;
            }
        }
        return $resultsArray;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @return Array $resultsList
     */
    public function getSalesInvoiceDetails($fromDate, $toDate, $cusCategory = NULL, $locationID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('*',
                    'totalInvoiceCost' => new Expression('(itemOutQty*itemInPrice)')))
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('*'), 'left')
                ->join('salesInvoice', 'itemOut.itemOutDocumentID = salesInvoice.salesInvoiceID', array('salesInvoiceID',
                    'salesInvoiceCode', 'salesInvoiceCustomCurrencyRate',
                    'salesInvoiceTotalAmount', 'locationID', 
                    'date' => new Expression('salesInvoiceIssuedDate')), 'left')
                ->join('location', 'location.locationID = salesInvoice.locationID', array('locationName', 'locationCode'), 'left')
                ->join('entity', 'entity.entityID = salesInvoice.entityID', array('deleted'), 'left')
                ->join('customer','salesInvoice.customerID = customer.customerID', array('customerCode','customerName'),'left')
                ->where(array('itemOutDocumentType' => 'Sales Invoice', 'deleted' => 0))
        ->where->notEqualTo('salesInvoice.statusID', 5)
        ->where->notEqualTo('salesInvoice.statusID', 10)
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        if($cusCategory != ""){
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        if (!is_null($locationID)) {
            $select->where->equalTo('salesInvoice.locationID', $locationID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param Array $proList
     * @param Date $fromDate
     * @param Array $locationIds
     * @return Array $resultsList
     */
    public function getStockOutRange($proList, $fromDate, $locationIds = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('availableOutQty' => new Expression('itemOutQty'),'itemOutDateAndTime','itemOutDocumentType'))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('salesInvoice', new Expression('itemOut.itemOutDocumentID = salesInvoice.salesInvoiceID AND itemOut.itemOutDocumentType = "Sales Invoice"'), array('salesInvoiceCode', 'salesInvoiceStatus' => new Expression('salesInvoice.statusID')), 'left')
                ->join('deliveryNote', new Expression('itemOut.itemOutDocumentID = deliveryNote.deliveryNoteID AND itemOut.itemOutDocumentType = "Delivery Note"'), array('deliveryNoteCode', 'deliveryNoteStatus'), 'left')
                ->join('goodsIssue', new Expression('itemOut.itemOutDocumentID = goodsIssue.goodsIssueID AND (itemOut.itemOutDocumentType = "Inventory Negative Adjustment" OR itemOut.itemOutDocumentType = "Delete Positive Adjustment")'), array('goodsIssueCode', 'goodsIssueStatus'), 'left')
                ->join('purchaseReturn', new Expression('itemOut.itemOutDocumentID = purchaseReturn.purchaseReturnID AND itemOut.itemOutDocumentType = "Purchase Return"'), array('purchaseReturnCode', 'purchaseReturnStatus'), 'left')
                ->join('debitNote', new Expression('itemOut.itemOutDocumentID = debitNote.debitNoteID AND itemOut.itemOutDocumentType = "Debit Note"'), array('debitNoteCode', 'debitNoteStatus' => new Expression('debitNote.statusID')), 'left')
                ->join('activity', new Expression('itemOut.itemOutDocumentID = activity.activityId AND itemOut.itemOutDocumentType = "Activity"'), array('activityCode'), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
        ->where->lessThan('itemOutDateAndTime', $fromDate)
        ->where->in('locationProduct.productID', $proList);
        if(!empty($locationIds)){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            if(!($v['salesInvoiceStatus'] == 10 || $v['salesInvoiceStatus'] == 8 || $v['deliveryNoteStatus'] == 10)){
                $resultsList[] = $v;
            }
        }
        return $resultsList;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param int $referenceID
     * @param decimal $unitPrice
     * @param string $itemOutDocumentType
     * @param int $activityId
     * @return boolean
     */
    public function updateJobCardUnitPrice($referenceID, $unitPrice, $itemOutDocumentType, $activityId)
    {

        try {

            $set = array(
                'itemOutPrice' => $unitPrice,
                'itemOutDocumentType' => $itemOutDocumentType,
                'itemOutJobCardReferenceID' => $referenceID
            );

            $this->tableGateway->update($set, array('itemOutDocumentID' => $activityId, 'itemOutDocumentType' => 'Activity'));

            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @return Array $resultsList
     */
    public function getSalesInvoiceProductDetails($fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID', 'salesInvoiceCode', 'salesInvoiceTotalAmount'))
                ->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array(
                    'salesInvoiceProductID', 'salesInvoiceProductQuantity', 'productID'))
                ->join('product', 'salesInvoiceProduct.productID = product.productID', array(
                    'productName', 'productCode'))
                ->join('itemOut', 'itemOut.itemOutDocumentID = salesInvoiceProduct.salesInvoiceID', array(
                    'itemOutID', 'itemOutQty', 'itemOutPrice', 'itemOutDiscount'), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice', 'itemInDiscount'))
                ->join('entity', 'entity.entityID = salesInvoice.entityID', array('deleted'), 'left')
                ->group(['salesInvoiceProductID'])
                ->where(array('itemOutDocumentType' => 'Sales Invoice', 'deleted' => 0))
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function getInvoiceJobCardDetails($jobCardID = NULL, $jobCardType = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->columns(array('*',
            'totalInvoiceCost' => new Expression('(itemOutQty*itemInPrice)'),
        ));
        $select->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('*'), 'left');
        if ($jobCardID != NULL) {
            $select->where->equalTo('itemOutJobCardReferenceID', $jobCardID);
        }
        if ($jobCardType != NULL) {
            $select->where->equalTo('itemOutDocumentType', $jobCardType);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = [];
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function updateDeliveryNoteUnitPrice($data, $locationProductID, $deliveryNoteID)
    {
        try {
            $this->tableGateway->update($data, array('itemOutLocationProductID' => $locationProductID, 'itemOutDocumentID' => $deliveryNoteID, 'itemOutDocumentType' => 'Delivery Note'));

            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    public function getInvoiceDeliveryNoteDetails($documentID = NULL, $documentType = NULL, $locationProID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->columns(array('*'));
        $select->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('*'), 'left');
        $select->group('itemOutID');
        if ($documentID != NULL && $documentType != NULL) {
            $select->where->equalTo('itemOutDocumentID', $documentID);
            $select->where->equalTo('itemOutDocumentType', $documentType);
        } else if($documentType != NULL){
            $select->where->equalTo('itemOutDocumentType', $documentType);
        }
        if ($locationProID != NULL) {
            $select->where->equalTo('itemOutLocationProductID', $locationProID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function deleteActivity($activityID)
    {
        try {
            $result = $this->tableGateway->delete(array('itemOutDocumentType' => "Activity", 'itemOutDocumentID' => $activityID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }
    
    /**
     * Get location product item out details by document type, document type ids and location product id
     * @param string $documentType
     * @param array $documentTypeIds
     * @param array $locationProductId
     * @return mixed
     */
    public function getLocationProductItemOutQuantityDetails( $documentType, $documentTypeIds, $locationProductId) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array(
                    'itemOutLocationProductID',
                    'itemOutQty' => new Expression('SUM(itemOut.itemOutQty)'),
                    'itemReturnQty' => new Expression('SUM(itemOut.itemOutReturnQty)')
                ))
                ->group('itemOut.itemOutLocationProductID')
                ->where->equalTo('itemOut.itemOutDocumentType', $documentType)
                ->where->in('itemOut.itemOutDocumentID', $documentTypeIds)
                ->where->equalTo('itemOut.itemOutLocationProductID', $locationProductId);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute()->current();
    } 

    public function getReturnQtyByBatchOrSerialID($batchID = NULL, $serialID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('totalQty' => new Expression('SUM(itemOutReturnQty)')));
        if($batchID != ''){
            $select->where(array('itemOutBatchID' => $batchID));
        }
        if($serialID != ''){
            $select->where(array('itemOutSerialID' => $serialID));   
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;   
    } 

    public function getOutDataByLocationProductIDAndDate($locationProductID, $date)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('totalQty' => new Expression('SUM(itemOutQty-itemOutReturnQty)')))
        ->where->greaterThan('itemOutDateAndTime', $date);
        $select->where(array('itemOutLocationProductID' => $locationProductID, 'itemOutDeletedFlag' => 0));
        $select->group('itemOutLocationProductID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
     
    public function getItemOutDetailsByGivenDateRangeAndLOcationProductID($locationProductID,$fromDate,$toDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('*'))
        ->where(array('itemOutLocationProductID' => $locationProductID, 'itemOutDeletedFlag' => 0));
        if($toDate){
            $select->where->between('itemOutDateAndTime', $fromDate,$toDate);
            
        } else {
            $select->where->greaterThan('itemOutDateAndTime', $fromDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateItemOutTable($itemOutID, $data)
    {
        try {
            $result = $this->tableGateway->update($data, array('itemOutID' => $itemOutID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getItemOutDetailsByGivenDocumentType($documentType)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('*', 'totalSoldQtty' => new Expression('SUM(itemOutQty)'), 'totalReturn' => new Expression('SUM(itemOutReturnQty)')))
        ->join('locationProduct', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('productID','locationID'), 'left')
        ->where(array('itemOutDocumentType' => $documentType))
        ->group(array('itemOutDocumentID','itemOutLocationProductID'));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateItemOutByArray($itemOutData, $locationPID, $canceledItemInDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $update = $sql->update();
        $update->table('itemOut');
        $update->set($itemOutData);
        $update->where('itemOutLocationProductID = '.$locationPID.'');
        $update->where->greaterThan('itemOutDateAndTime', $canceledItemInDate);
        $update->where->notEqualTo('itemOutDeletedFlag', 1);

        $statement = $sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();

        $flag = $result->getAffectedRows();
        if(!($flag)){
            return false;
        }else{
            return true;
        }
    }

    public function getItemOutDocumentIdsByCancelledDate($locationPID, $canceledItemInDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('itemOutDocumentID','itemOutQty'))
        ->join('journalEntry', 'journalEntry.journalEntryDocumentID = itemOut.itemOutDocumentID', array('journalEntryID'), 'left')
        ->where(array('itemOutLocationProductID' => $locationPID))
        ->where->greaterThan('itemOutDateAndTime', $canceledItemInDate)
        ->where->notEqualTo('itemOutDeletedFlag', 1);
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getItemOutDataByItemOutID($itemOutId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('itemOutDocumentID','itemOutQty'))
        ->join('journalEntry', 'journalEntry.journalEntryDocumentID = itemOut.itemOutDocumentID', array('journalEntryID'), 'left')
        ->where(array('itemOutID' => $itemOutId))
        ->where->notEqualTo('itemOutDeletedFlag', 1);
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getItemOutDetailsForProductByDocumentAndItemOutPrice($locationProductID, $documentType, $documentID,$itemOutPrice)
    {
        $rowset = $this->tableGateway->select(array('itemOutLocationProductID' => $locationProductID, 'itemOutDocumentType' => $documentType, 'itemOutDocumentID' => $documentID, 'itemOutPrice' => $itemOutPrice));

        $resultsArray = [];
        foreach ($rowset as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function getItemOutDetailsForProductByDocumentAndSerialID($locationProductID, $documentType, $documentID,$serialID)
    {
        $rowset = $this->tableGateway->select(array('itemOutLocationProductID' => $locationProductID, 'itemOutDocumentType' => $documentType, 'itemOutDocumentID' => $documentID, 'itemOutSerialID' => $serialID));

        $resultsArray = [];
        foreach ($rowset as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function getItemOutDetailsForProductByDocumentAndBatchID($locationProductID, $documentType, $documentID,$batchID)
    {
        $rowset = $this->tableGateway->select(array('itemOutLocationProductID' => $locationProductID, 'itemOutDocumentType' => $documentType, 'itemOutDocumentID' => $documentID, 'itemOutBatchID' => $batchID));

        $resultsArray = [];
        foreach ($rowset as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }
    /**
    * this function use to get all itemOut records that related to the given doc type, batch id and doc id
    * @param int $batchID
    * @param string $documentType
    * @param int $documentID
    * return array
    **/
    public function getBatchItemOutDetailsForCreditNote($batchID,$documentType, $documentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = new select();
        $select->from('itemOut')
                ->columns(array('*'))
                ->where(array('itemOutBatchID' => $batchID, 'itemOutDocumentType' => $documentType, 'itemOutDocumentID' => $documentID))
        ->where('itemOutReturnQty != itemOutQty');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

    public function getOutDataByLocationProductIDStartDateAndEndDate($locationProductID, $sDate, $eDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('totalQty' => new Expression('SUM(itemOutQty-itemOutReturnQty)')))
        ->where->between('itemOutDateAndTime', $sDate, $eDate);
        $select->where(array('itemOutLocationProductID' => $locationProductID, 'itemOutDeletedFlag' => 0));
        $select->group('itemOutLocationProductID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * get very next itemOut record for given date
    * @param date $date
    * @param array $locationIDs
    * @param array $proIDs
    * return array
    **/
    public function getAllItemDetailsAfterSelectedDate($date, $locationIDs, $proIDs) {
    
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('*'))
                ->join('locationProduct','itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('productID'),'left')
                ->where->greaterThan('itemOut.itemOutDateAndTime', $date);
        $select->where->in('locationProduct.locationID', $locationIDs);
        $select->where->in('locationProduct.productID', $proIDs);
        $select->group('locationProduct.productID');
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;

    }

    /**
     * This function is used to get dln wise sales invoice product data
     * @param Date $fromDate
     * @param Date $toDate
     * @return Array $resultsList
     */
    public function getDlnWiseSalesInvoiceDetails($fromDate, $toDate, $cusCategory = NULL, $locationID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
            ->columns(array('*',
                    'totalInvoiceCost' => new Expression('(itemOutQty*itemInPrice)')))
            ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('*'), 'left')
            ->join('salesInvoiceProduct','salesInvoiceProduct.salesInvoiceProductDocumentID = itemOut.itemOutDocumentID and salesInvoiceProduct.locationProductID = itemOut.itemOutLocationProductID',array('salesInvoiceProductID'))
            ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesInvoiceID',
                    'salesInvoiceCode', 'salesInvoiceCustomCurrencyRate',
                    'salesInvoiceTotalAmount', 'locationID', 
                    'date' => new Expression('salesInvoiceIssuedDate')), 'left')
            ->join('location', 'location.locationID = salesInvoice.locationID', array('locationName', 'locationCode'), 'left')
            ->join('entity', 'entity.entityID = salesInvoice.entityID', array('deleted'), 'left')
            ->join('customer','salesInvoice.customerID = customer.customerID', array('customerCode','customerName'),'left')
            ->where(array('itemOutDocumentType' => 'Delivery Note', 'deleted' => 0,'salesInvoiceProduct.documentTypeID' => 4))
            ->where->notEqualTo('salesInvoice.statusID', 5)
            ->where->notEqualTo('salesInvoice.statusID', 10)
            ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        if($cusCategory != ""){
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        if (!is_null($locationID)) {
            $select->where->equalTo('salesInvoice.locationID', $locationID);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function getRangeItemOutSaleInvoiceDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('salesInvoice','itemOut.itemOutDocumentID = salesInvoice.salesInvoiceID', array('salesInvoiceCode', 'salesInvoiceStatus' => new Expression('salesInvoice.statusID'),'documentOutDate' => new Expression('salesInvoiceIssuedDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate)
            ->where->equalTo('itemOutDocumentType','Sales Invoice');
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('salesInvoice','itemOut.itemOutDocumentID = salesInvoice.salesInvoiceID', array('salesInvoiceCode', 'salesInvoiceStatus' => new Expression('salesInvoice.statusID'),'documentOutDate' => new Expression('salesInvoiceIssuedDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->lessThan('salesInvoiceIssuedDate', $fromDate)
            ->where->equalTo('itemOutDocumentType','Sales Invoice');
        }


        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            if(!($v['salesInvoiceStatus'] == 10 || $v['salesInvoiceStatus'] == 8)){
                $resultsArray[] = $v;
            }
        }
        return $resultsArray;
    }

    public function getRangeItemOutDeliveryNoteDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('deliveryNote','itemOut.itemOutDocumentID = deliveryNote.deliveryNoteID', array('deliveryNoteCode', 'deliveryNoteStatus','documentOutDate' => new Expression('deliveryNoteDeliveryDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->greaterThan('deliveryNoteDeliveryDate', $fromDate)
            ->where->lessThan('deliveryNoteDeliveryDate', $toDate)
            ->where->equalTo('itemOutDocumentType','Delivery Note');
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('deliveryNote','itemOut.itemOutDocumentID = deliveryNote.deliveryNoteID', array('deliveryNoteCode', 'deliveryNoteStatus','documentOutDate' => new Expression('deliveryNoteDeliveryDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->lessThan('deliveryNoteDeliveryDate', $fromDate)
            ->where->equalTo('itemOutDocumentType','Delivery Note');
        }


        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            if(!($v['deliveryNoteStatus'] == 10) && !($v['deliveryNoteStatus'] == 15)){
                $resultsArray[] = $v;
            }
        }
        return $resultsArray;
    }

    public function getRangeItemOutGRNDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
            
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('grn','itemOut.itemOutDocumentID = grn.grnID', array('grnCode', 'grnStatus' => new Expression('grn.status'),'status','documentOutDate' => new Expression('grnDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->between('grnDate', $fromDate, $toDate)
            ->where->equalTo('itemOutDocumentType','Goods Received Note Cancel');
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('grn','itemOut.itemOutDocumentID = grn.grnID', array('grnCode', 'status','documentOutDate' => new Expression('grnDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->lessThan('grnDate', $fromDate)
            ->where->equalTo('itemOutDocumentType','Goods Received Note Cancel');
        }



        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            if(!($v['grnStatus'] == 10)){
                $resultsArray[] = $v;
            }
        }
        return $resultsArray;
    }

    public function getRangeItemOutPurchaseInvoiceCancelDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('purchaseInvoice','itemOut.itemOutDocumentID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode', 'purchaseStatus' => new Expression('purchaseInvoice.status'),'documentOutDate' => new Expression('purchaseInvoiceIssueDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->between('purchaseInvoiceIssueDate', $fromDate, $toDate)
            ->where->equalTo('itemOutDocumentType','purchase Invoice cancel');
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('purchaseInvoice','itemOut.itemOutDocumentID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode', 'status','documentOutDate' => new Expression('purchaseInvoiceIssueDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->lessThan('purchaseInvoiceIssueDate', $fromDate)
            ->where->equalTo('itemOutDocumentType','purchase Invoice cancel');
        }


        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            if(!($v['purchaseStatus'] == 10)){
                $resultsArray[] = $v;
            }
        }
        return $resultsArray;
    }

    public function getRangeItemOutEXPVCancelDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('paymentVoucher','itemOut.itemOutDocumentID = paymentVoucher.paymentVoucherID', array('paymentVoucherCode', 'paymentVoucherStatus','documentOutDate' => new Expression('paymentVoucherIssuedDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->between('paymentVoucherIssuedDate', $fromDate, $toDate)
            ->where->equalTo('itemOutDocumentType','Payment Voucher Cancel');
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('paymentVoucher','itemOut.itemOutDocumentID = paymentVoucher.paymentVoucherID', array('paymentVoucherCode', 'paymentVoucherStatus','documentOutDate' => new Expression('paymentVoucherIssuedDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->lessThan('paymentVoucherIssuedDate', $fromDate)
            ->where->equalTo('itemOutDocumentType','Payment Voucher Cancel');
        } 

        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            if(!($v['deliveryNoteStatus'] == 10)){
                $resultsArray[] = $v;
            }
        }
        return $resultsArray;
    }

    public function getRangeItemOutGoodIssueDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('goodsIssue','itemOut.itemOutDocumentID = goodsIssue.goodsIssueID', array('goodsIssueCode', 'goodsIssueStatus','documentOutDate' => new Expression('goodsIssueDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->between('goodsIssueDate', $fromDate, $toDate)
            ->where->in('itemOutDocumentType',['Delete Positive Adjustment','Inventory Negative Adjustment']);
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('goodsIssue','itemOut.itemOutDocumentID = goodsIssue.goodsIssueID', array('goodsIssueCode', 'goodsIssueStatus','documentOutDate' => new Expression('goodsIssueDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->lessThan('goodsIssueDate', $fromDate)
            ->where->in('itemOutDocumentType',['Delete Positive Adjustment','Inventory Negative Adjustment']);
        }


        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemOutPurchaseReturnDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('purchaseReturn','itemOut.itemOutDocumentID = purchaseReturn.purchaseReturnID', array('purchaseReturnCode', 'purchaseReturnStatus','documentOutDate' => new Expression('purchaseReturnDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->between('purchaseReturnDate', $fromDate, $toDate)
            ->where->in('itemOutDocumentType',['Purchase Return','Direct Purchase Return']);
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('purchaseReturn','itemOut.itemOutDocumentID = purchaseReturn.purchaseReturnID', array('purchaseReturnCode', 'purchaseReturnStatus','documentOutDate' => new Expression('purchaseReturnDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->lessThan('purchaseReturnDate', $fromDate)
            ->where->in('itemOutDocumentType',['Purchase Return','Direct Purchase Return']);
        }

        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemOutDebitNoteDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('debitNote','itemOut.itemOutDocumentID = debitNote.debitNoteID', array('debitNoteCode', 'debitNoteStatus' => new Expression('debitNote.statusID'),'documentOutDate' => new Expression('debitNoteDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->between('debitNoteDate', $fromDate, $toDate)
            ->where->in('itemOutDocumentType',['Debit Note','Direct Debit Note']);
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('debitNote','itemOut.itemOutDocumentID = debitNote.debitNoteID', array('debitNoteCode', 'debitNoteStatus' => new Expression('debitNote.statusID'),'documentOutDate' => new Expression('debitNoteDate')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
            ->where->lessThan('debitNoteDate', $fromDate)
            ->where->in('itemOutDocumentType',['Debit Note','Direct Debit Note']);
        }

        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    public function getRangeItemOutActivityDetails($productList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        
        if (!empty($fromDate) && !empty($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    'documentOutDate' => new Expression('itemOutDateAndTime')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('activity', new Expression('itemOut.itemOutDocumentID = activity.activityId AND itemOut.itemOutDocumentType = "Activity"'), array('activityCode','documentOutDate' => new Expression('activityStartingTime')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
                ->where->between('activityStartingTime', $fromDate, $toDate);
        }

        if (!empty($fromDate) && is_null($toDate)) {
            $select->from('itemOut')
                ->columns(array(
                    '*',
                    'totalOutQty' => new Expression('SUM(itemOutQty)'),
                    'documentOutDate' => new Expression('itemOutDateAndTime')
                    ))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode', 'productTypeID'), 'left')
                ->join('activity', new Expression('itemOut.itemOutDocumentID = activity.activityId AND itemOut.itemOutDocumentType = "Activity"'), array('activityCode','documentOutDate' => new Expression('activityStartingTime')), 'left')
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice'), 'left')
                ->group(array('itemOut.itemOutDocumentID', 'itemOutDocumentType', 'product.productID', 'itemInPrice'))
                ->where->lessThan('activityStartingTime', $fromDate);   
        }

        $select->where(array('product.productState' => 1));
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $productList);
        }
        if ($isNonInventoryProducts) {
            $select->where(['product.productTypeID' => 2]);
        } else {
            $select->where(['product.productTypeID' => 1]);
        }
        if($locationIds != ''){
            $select->where->in('locationProduct.locationID', $locationIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsArray = Array();
        foreach ($results as $v) {
            $resultsArray[] = $v;
        }
        return $resultsArray;
    }

    /*
    this funtion is use to calculate total cost (total item-in price) of deliveryNote
    */
    public function getCostForDelivaryNote($deliveryNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->columns(array("*"));
        $select->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('totalCostOfCreditNote' => new Expression('SUM((itemIn.itemInPrice - itemIn.itemInDiscount) * itemOut.itemOutQty)')), 'left');
        $select->where(array('itemOut.itemOutDocumentType' => 'Delivery Note', 'itemOut.itemOutDocumentID'=> $deliveryNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results->current();
    }


    public function getBestMovingItemByDateRange($fromDate, $toDate, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('movingCount' => new Expression('COUNT(itemOutQty)')))
                ->join('locationProduct', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID', 'productName', 'productCode'), 'left')
                ->where(array('itemOut.itemOutDocumentType' => 'Sales Invoice','itemOutDeletedFlag' => 0))
        ->where->between('itemOutDateAndTime', $fromDate, $toDate." 23:59:00");
        $select->where->equalTo('locationProduct.locationID', $locationID);
        $select->group('itemOut.itemOutLocationProductID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @return Array $resultsList
     */
    public function getSalesInvoiceDetailsForDashboard($fromDate, $toDate, $cusCategory = NULL, $locationID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut')
                ->columns(array('itemOutID','itemOutDocumentType', 'itemOutDocumentID', 'itemOutLocationProductID', 'itemOutQty', 'itemOutPrice', 'itemOutDiscount', 
                    'totalInvoiceCost' => new Expression('(itemOutQty*itemInPrice)')))
                ->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('*'), 'left')
                ->join('salesInvoice', 'itemOut.itemOutDocumentID = salesInvoice.salesInvoiceID', array('salesInvoiceID',
                    'salesInvoiceCode','salesInvoiceTotalAmount', 'locationID', 
                    'date' => new Expression('salesInvoiceIssuedDate')), 'left')
                // ->join('location', 'location.locationID = salesInvoice.locationID', array('locationName', 'locationCode'), 'left')
                ->join('entity', 'entity.entityID = salesInvoice.entityID', array('deleted'), 'left')
                // ->join('customer','salesInvoice.customerID = customer.customerID', array('customerCode','customerName'),'left')
                ->where(array('itemOutDocumentType' => 'Sales Invoice', 'deleted' => 0, 'locationID' => $locationID))
        ->where->notEqualTo('salesInvoice.statusID', 5)
        ->where->notEqualTo('salesInvoice.statusID', 10)
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
    
        if (!is_null($locationID)) {
            $select->where->equalTo('salesInvoice.locationID', $locationID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function getInvoiceDeliveryNoteDetailsForDashboard($documentID = NULL, $documentType = NULL, $locationProID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->columns(array('itemOutID', 'itemOutLocationProductID', 'itemOutDocumentType', 'itemOutQty', 'itemOutDiscount', 'itemOutPrice'));
        $select->join('itemIn', 'itemIn.itemInID = itemOut.itemOutItemInID', array('itemInPrice', 'itemInDiscount', 'itemInDocumentID', 'itemInLocationProductID'), 'left');
        $select->group('itemOutID');
        if ($documentID != NULL && $documentType != NULL) {
            $select->where->equalTo('itemOutDocumentID', $documentID);
            $select->where->equalTo('itemOutDocumentType', $documentType);
        } else if($documentType != NULL){
            $select->where->equalTo('itemOutDocumentType', $documentType);
        }
        if ($locationProID != NULL) {
            $select->where->equalTo('itemOutLocationProductID', $locationProID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }
}
