<?php
namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseRequisitionProduct implements InputFilterAwareInterface
{

    public $purchaseRequisitionProductID;
    public $purchaseRequisitionID;
    public $locationProductID;
    public $purchaseRequisitionProductQuantity;
    public $purchaseRequisitionProductPrice;
    public $purchaseRequisitionProductDiscount;
    public $purchaseRequisitionProductTotal;
    public $copied;
    public $purchaseRequisitionProductCopiedQuantity;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseRequisitionProductID = (!empty($data['purchaseRequisitionProductID'])) ? $data['purchaseRequisitionProductID'] : null;
        $this->purchaseRequisitionID = (!empty($data['purchaseRequisitionID'])) ? $data['purchaseRequisitionID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->purchaseRequisitionProductQuantity = (!empty($data['purchaseRequisitionProductQuantity'])) ? $data['purchaseRequisitionProductQuantity'] : null;
        $this->purchaseRequisitionProductPrice = (!empty($data['purchaseRequisitionProductPrice'])) ? $data['purchaseRequisitionProductPrice'] : null;
        $this->purchaseRequisitionProductDiscount = (!empty($data['purchaseRequisitionProductDiscount'])) ? $data['purchaseRequisitionProductDiscount'] : null;
        $this->purchaseRequisitionProductTotal = (!empty($data['purchaseRequisitionProductTotal'])) ? $data['purchaseRequisitionProductTotal'] : null;
        $this->copied = (!empty($data['copied'])) ? $data['copied'] : 0;
        $this->purchaseRequisitionProductCopiedQuantity = (!empty($data['purchaseRequisitionProductCopiedQuantity'])) ? $data['purchaseRequisitionProductCopiedQuantity'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
