<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains debit note payment Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DebitNotePayment implements InputFilterAwareInterface
{

    public $debitNotePaymentID;
    public $debitNotePaymentCode;
    public $debitNotePaymentDate;
    public $paymentTermID;
    public $debitNotePaymentAmount;
    public $supplierID;
    public $debitNotePaymentDiscount;
    public $debitNotePaymentMemo;
    public $locationID;
    public $statusID;
    public $debitNotePaymentCancelMessage;
    public $entityID;
    protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->debitNotePaymentID = (!empty($data['debitNotePaymentID'])) ? $data['debitNotePaymentID'] : null;
        $this->debitNotePaymentCode = (!empty($data['debitNotePaymentCode'])) ? $data['debitNotePaymentCode'] : null;
        $this->debitNotePaymentDate = (!empty($data['debitNotePaymentDate'])) ? $data['debitNotePaymentDate'] : null;
        $this->paymentTermID = (!empty($data['paymentTermID'])) ? $data['paymentTermID'] : null;
        $this->debitNotePaymentAmount = (!empty($data['debitNotePaymentAmount'])) ? $data['debitNotePaymentAmount'] : 0.00;
        $this->supplierID = (!empty($data['supplierID'])) ? $data['supplierID'] : null;
        $this->debitNotePaymentDiscount = (!empty($data['debitNotePaymentDiscount'])) ? $data['debitNotePaymentDiscount'] : 0.00;
        $this->debitNotePaymentMemo = (!empty($data['debitNotePaymentMemo'])) ? $data['debitNotePaymentMemo'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->statusID = (!empty($data['statusID'])) ? $data['statusID'] : null;
        $this->debitNotePaymentCancelMessage = (!empty($data['debitNotePaymentCancelMessage'])) ? $data['debitNotePaymentCancelMessage'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

// Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
