<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 * This file contains SupplierID and supplierCategoryID
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SupplierCategoryComponent implements InputFilterAwareInterface
{

    public $supplierID;
    public $supplierCategoryID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->supplierID = (!empty($data['supplierID'])) ? $data['supplierID'] : NULL;
        $this->supplierCategoryID = (!empty($data['supplierCategoryID'])) ? $data['supplierCategoryID'] : NULL;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'supplierCategoryID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'supplierCategoryID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

}
