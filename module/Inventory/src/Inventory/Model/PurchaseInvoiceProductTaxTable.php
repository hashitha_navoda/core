<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class PurchaseInvoiceProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePurchaseInvoiceProductTax(PurchaseInvoiceProductTax $piProductTax)
    {
        $data = array(
            'purchaseInvoiceID' => $piProductTax->purchaseInvoiceID,
            'purchaseInvoiceProductID' => $piProductTax->purchaseInvoiceProductID,
            'purchaseInvoiceTaxID' => $piProductTax->purchaseInvoiceTaxID,
            'purchaseInvoiceTaxPrecentage' => $piProductTax->purchaseInvoiceTaxPrecentage,
            'purchaseInvoiceTaxAmount' => $piProductTax->purchaseInvoiceTaxAmount,
        );
        $this->tableGateway->insert($data);
    }

    public function getPurchaseInvoiceProductTax($purchaseInvoiceID = NULL, $locationProductID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoiceProductTax")
                ->columns(array("*"))
                ->join("purchaseInvoiceProduct", "purchaseInvoiceProduct.purchaseInvoiceProductID = purchaseInvoiceProductTax.purchaseInvoiceProductID", array("*"))
                ->where(array(
                    'purchaseInvoiceProduct.purchaseInvoiceID' => $purchaseInvoiceID,
                    'purchaseInvoiceProduct.locationProductID' => $locationProductID))
                ->group('purchaseInvoiceTaxID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $data = [];
        foreach ($results as $result) {
            $data[] = $result;
        }
        return $data;
    }

}
