<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains supplier payments Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\PredicateInterface;

class SupplierPaymentsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new \Zend\Db\Sql\Select;
            $select->from('outgoingPayment')
                    ->columns(array('*'))
                    ->order(array('outgoingPaymentDate' => 'DESC', 'outgoingPaymentCode' => 'DESC',))
                    ->JOIN('entity', 'outgoingPayment.entityID= entity.entityID ', array('deleted'), 'left')
                    ->JOIN('supplier', ' supplier.supplierID= outgoingPayment.supplierID ', array('supplierName', 'supplierCode', 'supplierTitle'), 'left')
                    ->where(array('locationID' => $locationID/** , 'deleted' => 0* */));
            return new \Zend\Paginator\Paginator(
                    new \Zend\Paginator\Adapter\DbSelect(
                    $select, $this->tableGateway->getAdapter()
                    )
            );
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getPaymentsByDate($fromdate, $todate, $supplierID = NULL, $locationID, $whichPayment = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('*'))
                ->join('entity', ' outgoingPayment.entityID = entity.entityID', array('deleted'), 'left')
                ->join('supplier', 'supplier.supplierID =  outgoingPayment.supplierID', array('supplierCode', 'supplierName'), 'left')
                ->join('outgoingPaymentInvoice', ' outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID ', array('invoiceID','paymentVoucherId'), 'left')
                ->group('outgoingPaymentID')
                ->where(array('outgoingPaymentDate >= ?' => $fromdate))
                ->where(array('outgoingPaymentDate <= ?' => $todate, 'locationID' => $locationID/* , 'deleted' => 0 */));
        if ($supplierID) {
            $select->where(array('outgoingPayment.supplierID' => $supplierID));
        }
        if($whichPayment=='PI'){
            $select->where->notEqualTo('invoiceID',0);
        }else if($whichPayment=='PV'){
            $select->where->notEqualTo('paymentVoucherId',0);
            $select->where(array('outgoingPaymentType'=>'invoice'));
        }else if($whichPayment=='PVAdv'){
            $select->where(array('outgoingPaymentType'=>'advance'));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentsBySupplierID($supID, $locationID, $whichPayment = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('*'))
                ->join('entity', ' outgoingPayment.entityID = entity.entityID', array('deleted'), 'left')
                ->join('supplier', 'supplier.supplierID = outgoingPayment.supplierID', array('*'), 'left')
                ->join('outgoingPaymentInvoice', ' outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID ', array('invoiceID','paymentVoucherId'), 'left')
                ->group('outgoingPaymentID')
                ->where(array('outgoingPayment.supplierID' => $supID, 'outgoingPayment.locationID' => $locationID/* , 'deleted' => 0 */));
        if($whichPayment=='PI'){
            $select->where->notEqualTo('invoiceID',0);
        }else if($whichPayment=='PV'){
            $select->where->notEqualTo('paymentVoucherId',0);
            $select->where(array('outgoingPaymentType'=>'invoice'));
        }else if($whichPayment=='PVAdv'){
            $select->where(array('outgoingPaymentType'=>'advance'));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentsByPaymentID($paymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array("*"))
                ->join('entity', ' outgoingPayment.entityID = entity.entityID', array('deleted','createdTimeStamp'), 'left')
                ->join('supplier', 'outgoingPayment.supplierID =  supplier.supplierID', array("*"), "left")
                ->where(array('outgoingPayment.outgoingPaymentID' => $paymentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentsByPaymentIDForJE($paymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array("*"))
                ->join('supplier', 'outgoingPayment.supplierID =  supplier.supplierID', array("*"), "left")
                ->where(array('outgoingPayment.outgoingPaymentID' => $paymentID));
        $select->where->notEqualTo('outgoingPayment.outgoingPaymentStatus', 5);
        $select->where->notEqualTo('outgoingPayment.outgoingPaymentStatus', 10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentsByLocatioID($locationID, $searchKey = false, $whichPayment = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array("*"))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array("deleted"), "left")
                ->join('outgoingPaymentInvoice', ' outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID ', array('invoiceID','paymentVoucherId'), 'left')
                // ->group('outgoingPaymentID')
                ->where(array('outgoingPayment.locationID' => $locationID/* , 'deleted' => 0 */));
        if ($searchKey) {
            $select->where->like('outgoingPayment.outgoingPaymentCode', '%' . $searchKey . '%');
            $select->limit(50);
        }
        if($whichPayment=='PI'){
            $select->where->notEqualTo('invoiceID',0);
        }else if($whichPayment=='PV'){
            $dataSet = [0];
            $select->where->AND->NEST->in('invoiceID',$dataSet)
                    ->orPredicate(
            new \Zend\Db\Sql\Predicate\IsNull('paymentVoucherId'));
            $select->where(array('outgoingPaymentType'=>'invoice'));
        }else if($whichPayment=='PVAdv'){
            $dataSet = [0];
            $select->where->AND->NEST->in('invoiceID',$dataSet)
                    ->orPredicate(
            new \Zend\Db\Sql\Predicate\IsNull('paymentVoucherId'));
            $select->where(array('outgoingPaymentType'=>'advance'));
        }else if ($whichPayment=='Cheque') {
            $select->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('*'));
            $select->where(array('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID' => 2));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentByID($payid)
    {
        $rowset = $this->tableGateway->select(array('outgoingPaymentID' => $payid));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getAllPaymentDetailsByID($payid)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
//        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('*'));
//        $select->join('location', 'salesInvoice.locationID = location.locationID', array('*'));
        $select->join('paymentTerm', 'outgoingPayment.paymentTermID = paymentTerm.paymentTermID', array('*'));
        $select->join('entity', 'outgoingPayment.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->where(array('outgoingPaymentID' => $payid));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getPaymentByCode($payCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array("deleted"), "left")
                ->where(array('outgoingPaymentCode' => $payCode, 'deleted' => 0));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset->current();
    }

    public function deletePayment($payid)
    {
        $rowset = $this->tableGateway->delete(array('paymentID' => $payid));
        return true;
    }

    public function update($data, $paymentID)
    {
        return $this->tableGateway->update($data, array('outgoingPaymentID' => $paymentID));
    }

    public function savePayments($payments, $invcdata, $supplierCreditUpdate, $supplierUpdate, $invoicePayment, $debitNoteSupplierUpdate, $debitNoteUpdate, $pvData, $currentCredit)
    {
        if (!$payments['debitNote']) {
            $payments['debitNote'] = NULL;
        }

        $data = array(
            'outgoingPaymentCode' => $payments['paymentID'],
            'outgoingPaymentDate' => $payments['date'],
            'paymentTermID' => $payments['paymentTerm'],
            'outgoingPaymentAmount' => $payments['amount'],
            'supplierID' => (!empty($payments['supplierID'])) ? $payments['supplierID'] : null,
            'outgoingPaymentDiscount' => (!empty($payments['discount'])) ? $payments['discount'] : 0,
            'outgoingPaymentMemo' => (!empty($payments['memo'])) ? $payments['memo'] : null,
            'outgoingPaymentType' => $payments['paymentType'],
            'debitNoteID' => $payments['debitNote'],
            'locationID' => $payments['locationID'],
            'entityID' => $payments['entityID'],
            'outgoingPaymentCreditAmount' => $payments['creditAmount'],
            'outgoingPaymentPaidAmount' => $payments['totalPaidAmount'],
            'outgoingPaymentBalanceAmount' => $payments['balanceAmount']
        );
        $sql = new Sql($this->tableGateway->getAdapter());
        if ($invcdata) {
            foreach ($invcdata as $key => $value) {
                $keyID = split('_', $key)[0];
                $updateToInvoice[$key] = $sql->update();
                $updateToInvoice[$key]->table('purchaseInvoice')
                        ->set($value)
                        ->where(array('purchaseInvoiceID' => $keyID));
                $statementToInvoice[$key] = $sql->getSqlStringForSqlObject($updateToInvoice[$key]);
            }
        }
        if ($pvData) {
            foreach ($pvData as $key => $value) {
                $keyID = split('_', $key)[0];
                $updateToPV[$key] = $sql->update();
                $updateToPV[$key]->table('paymentVoucher')
                        ->set($value)
                        ->where(array('paymentVoucherID' => $keyID));
                $statementToPV[$key] = $sql->getSqlStringForSqlObject($updateToPV[$key]);
            }
        }
        if ($supplierCreditUpdate) {
            $realCreditValueFromThisPayment = 0;
            $supID = null;
            foreach ($supplierCreditUpdate as $key => $value) {
                // calculate real credit value from this payment
                $realCreditValueFromThisPayment = floatval($value['supplierCreditBalance']);
                $supID = $value['supplierID'];
                
                // reset value array with total credit values
                $savingData = [
                    'supplierID' => $supID,
                    'supplierCreditBalance' => floatval($realCreditValueFromThisPayment)
                ]; 

                $updateSupplierCredit[0] = $sql->update();
                $updateSupplierCredit[0]->table('supplier')
                        ->set($savingData)
                        ->where(array('supplierID' => $savingData['supplierID']));
                $statementSupplierCredit[0] = $sql->getSqlStringForSqlObject($updateSupplierCredit[0]);
            }
            
        }

        foreach ($supplierUpdate as $key => $value) {
            $updateSupplierBalance[$key] = $sql->update();
            $updateSupplierBalance[$key]->table('supplier')
                    ->set($value)
                    ->where(array('supplierID' => $value['supplierID']));
            $statementSupplierBalance[$key] = $sql->getSqlStringForSqlObject($updateSupplierBalance[$key]);
        }

        $insert = $sql->insert();
        $db = $this->tableGateway->getAdapter()->getDriver()->getConnection();
        $insert->into('outgoingPayment')
                ->values($data);
        $statement = $sql->getSqlStringForSqlObject($insert);

        if ($debitNoteSupplierUpdate) {
            $updateDebitNoteSupplierBalance = $sql->update();
            $updateDebitNoteSupplierBalance->table('supplier')
                    ->set($debitNoteSupplierUpdate)
                    ->where(array('supplierID' => $debitNoteSupplierUpdate['supplierID']));
            $statementDebitNoteSupplierBalance = $sql->getSqlStringForSqlObject($updateDebitNoteSupplierBalance);
        }

        if ($debitNoteUpdate) {
            $updateDebitNote = $sql->update();
            $updateDebitNote->table('debitNote')
                    ->set($debitNoteUpdate)
                    ->where(array('debitNoteID' => $debitNoteUpdate['debitNoteID']));
            $statementDebitNote = $sql->getSqlStringForSqlObject($updateDebitNote);
        }

        // $db->beginTransaction();
        try {
            $outgoingPaymentID = $db->execute($statement)->getGeneratedValue();

            foreach ($invoicePayment as $key => $value) {
                $value['paymentID'] = $outgoingPaymentID;
                $InsertToSuppplierInvoicePayment[$key] = $sql->insert();
                $InsertToSuppplierInvoicePayment[$key]->into('outgoingPaymentInvoice')
                        ->values($value);
                $statementSupplierInvoicePayment[$key] = $sql->getSqlStringForSqlObject($InsertToSuppplierInvoicePayment[$key]);
            }
            if ($debitNoteUpdate) {
                $db->execute($statementDebitNote);
            }

            if ($statementToInvoice) {
                foreach ($statementToInvoice as $keys) {
                    $db->execute($keys);
                }
            }

            if ($statementToPV) {
                foreach ($statementToPV as $keys) {
                    $db->execute($keys);
                }
            }
            if ($supplierCreditUpdate) {
                foreach ($statementSupplierCredit as $keys) {
                    $db->execute($keys);
                }
            }
            foreach ($statementSupplierBalance as $keys) {
                $db->execute($keys);
            }
            foreach ($statementSupplierInvoicePayment as $keys) {
                $db->execute($keys);
            }
            if ($debitNoteSupplierUpdate) {
                $db->execute($statementDebitNoteSupplierBalance);
            }
            // $db->commit();
            return array('state' => true, 'data' => $payments['paymentID'], 'id' => $outgoingPaymentID);
        } catch (\Exception $e) {
            var_dump($e->__toString());
            // $db->rollBack();
            return false;
        }exit;
    }

    public function saveAdvancePayment($data, $sudata, $openingCreditFlag = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $db = $this->tableGateway->getAdapter()->getDriver()->getConnection();
        $insert = $sql->insert();
        $insert->into('outgoingPayment')
                ->values($data);
        $statementPayment = $sql->getSqlStringForSqlObject($insert);

        if ($openingCreditFlag == false) {
            $updateSupplierCredit = $sql->update();
            $updateSupplierCredit->table('supplier')
                    ->set($sudata)
                    ->where(array('supplierID' => $sudata['supplierID']));
            $statementSupplierCredit = $sql->getSqlStringForSqlObject($updateSupplierCredit);
        }

        // $db->beginTransaction();
        try {
            $incomingPaymentID = $db->execute($statementPayment)->getGeneratedValue();
            if ($openingCreditFlag == false) {
                $db->execute($statementSupplierCredit);
            }
            // $db->commit();
            return array('state' => true, 'data' => $incomingPaymentID);
        } catch (\Exception $e) {
            // $db->rollBack();
            return array('state' => false);
        }
    }

    public function checkSupplierPaymentByCode($paymentCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('outgoingPayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "outgoingPayment.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('outgoingPayment.outgoingPaymentCode' => $paymentCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $date
     * @return type
     */
    public function getDailyPaymentsDetailsByOnlyDate($date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->join('customer', 'payement.customerID = customer.customerID', array('customerName'))
                ->where(array('payement.date' => $date));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $customerID
     * @param type $date
     * @return type
     */
    public function getDailyPaymentsDetailsByDate($customerID, $date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->join('customer', 'payement.customerID = customer.customerID', array('customerName'))
                ->where(array('payement.customerID' => $customerID))
                ->where(array('payement.date' => $date));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }
        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * This file contains CustomerTransaction Report related Mysql functions
     * @param type $customerID
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getPaymentsDetailsByCusId($customerID, $fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->where(array('customerID' => $customerID))
        ->where->between('date', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * This file contains Customer Balances Report related Mysql function
     * @param type $customerID
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getPaymentsBalancesData($customerID, $fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->columns(array('amountPay' => new Expression('SUM(amount)'),
                    'setledPay' => new Expression('SUM(amount)')))
                ->where(array('customerID' => $customerID))
        ->where->between('date', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * get payment summary data
     * @param type $from_date
     * @param type $to_date
     * @return type
     */
    public function getPaymentsSummeryData($from_date, $to_date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('incomingPayment')
                ->columns(array('incomingPaymentID', 'incomingPaymentCode', 'incomingPaymentDate', 'incomingPaymentAmount', 'incomingPaymentMemo'))
                ->JOIN('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('paymentMethodID'))
                ->JOIN('paymentMethod', 'paymentMethod.paymentMethodID = incomingInvoicePayment.paymentMethodID', array('paymentMethodName'))
                ->join('paymentMethodsNumbers', 'paymentMethodsNumbers.paymentID = incomingPayment.incomingPaymentID', array('paymentMethodReferenceNumber', 'paymentMethodBank'), 'left')
                ->JOIN('customer', 'customer.customerID = incomingPayment.customerID', array('customerName'), 'left')
                ->group('incomingPayment.incomingPaymentID')
        ->where->between('incomingPaymentDate', $from_date, $to_date);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * get payment data for dash board graph
     * @param type $current
     * @param type $before
     * @return type
     */
    public function getPaymentGraphDataByDay($current, $before)
    {
        $days_array = $this->getDateArray($current, $before);

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('payement')
                ->columns(array(
                    $days_array[0] => new Expression("SUM(case when datediff(date, '" . $days_array[0] . "') = 0 then amount end)"),
                    $days_array[1] => new Expression("SUM(case when datediff(date, '" . $days_array[1] . "') = 0 then amount end)"),
                    $days_array[2] => new Expression("SUM(case when datediff(date, '" . $days_array[2] . "') = 0 then amount end)"),
                    $days_array[3] => new Expression("SUM(case when datediff(date, '" . $days_array[3] . "') = 0 then amount end)"),
                    $days_array[4] => new Expression("SUM(case when datediff(date, '" . $days_array[4] . "') = 0 then amount end)"),
                    $days_array[5] => new Expression("SUM(case when datediff(date, '" . $days_array[5] . "') = 0 then amount end)"),
                    $days_array[6] => new Expression("SUM(case when datediff(date, '" . $days_array[6] . "') = 0 then amount end)"),
                    $days_array[7] => new Expression("SUM(case when datediff(date, '" . $days_array[7] . "') = 0 then amount end)"),
                    $days_array[8] => new Expression("SUM(case when datediff(date, '" . $days_array[8] . "') = 0 then amount end)"),
                    $days_array[9] => new Expression("SUM(case when datediff(date, '" . $days_array[9] . "') = 0 then amount end)"),
                    $days_array[10] => new Expression("SUM(case when datediff(date, '" . $days_array[10] . "') = 0 then amount end)"),
                    $days_array[11] => new Expression("SUM(case when datediff(date, '" . $days_array[11] . "') = 0 then amount end)"),
                    $days_array[12] => new Expression("SUM(case when datediff(date, '" . $days_array[12] . "') = 0 then amount end)"),
                    $days_array[13] => new Expression("SUM(case when datediff(date, '" . $days_array[13] . "') = 0 then amount end)"),
                ))
        ->where->between('date', $before, $current);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $yesterday
     * @return type
     */
    public function getYesterdayPaymentData($yesterday)
    {

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->columns(array('date'))
                ->JOIN('invoicePayment', 'payement.paymentID = invoicePayment.paymentID', array('TotalAmount' => new Expression('SUM(invoicePayment.amount)')), 'inner')
                ->JOIN('paymentMethods', 'invoicePayment.paymentMethod = paymentMethods.id', array('Method' => new Expression('paymentMethods.method')), 'left')
                ->group('Method')
                ->where('Method IS NOT NULL')
                ->where(array('payement.date' => $yesterday));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $current
     * @param type $before
     * @return type
     */
    public function getDateArray($current, $before)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($before);
        $endTime = strtotime($current);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    public function getPaymentIdsList()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('paymentID' => 'incomingPaymentID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function returns CashInflow for daily monthly and yearly
     */
    function getCashInFlowData($fromData, $toData, $format = NULL)
    {
        $groupArray = array();
        if ($format == 'year') {
            $groupArray = array('Year');
        } else if ($format == 'month') {
            $groupArray = array('Month', 'Year');
        } else {
            $groupArray = array('Date', 'Month', 'Year');
        }
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('incomingPayment')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%M %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%d")'),
                    'Year' => new Expression('Year(incomingPaymentDate)'),
                    'TotalCashFlow' => new Expression('SUM(incomingPaymentAmount)')
                ))
                ->group($groupArray)
                ->order(array('incomingPayment.incomingPaymentDate'))
        ->where->between('incomingPaymentDate', $fromData, $toData);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this function returns cashinflow for current date month and year
     */
    public function getCurrentCashInFlowBy($type)
    {
        $whereClause;
        if ($type == 'year') {
            $whereClause = date('Y');
        } else if ($type == 'month') {
            $whereClause = date('Y-m');
        } else {
            $whereClause = date('Y-m-d');
        }
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('incomingPayment')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%m")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%m %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%d")'),
                    'Year' => new Expression('Year(incomingPaymentDate)'),
                    'TotalCashInFlow' => new Expression('SUM(incomingPaymentAmount)')
                ))
        ->where->like('incomingPaymentDate', '%' . $whereClause . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param String $type
     * @return $results
     */
    public function getCurrentCashOutFlowBy($type)
    {
        $whereClause = NULL;
        if ($type == 'year') {
            $whereClause = date('Y');
        } else if ($type == 'month') {
            $whereClause = date('Y-m');
        } else {
            $whereClause = date('Y-m-d');
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%m")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%m %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%d")'),
                    'Year' => new Expression('Year(outgoingPaymentDate)'),
                    'TotalCashOutFlow' => new Expression('SUM(outgoingInvoiceCashAmount)')
                ))
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('entity', 'entity.entityID = outgoingPayment.entityID', '*', 'left')
                ->where(array('outgoingPaymentInvoice.paymentMethodID' => 1))
                ->where(array('entity.deleted' => 0))
        ->where->like('outgoingPaymentDate', '%' . $whereClause . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param String $type
     * @return $results
     */
    public function getCurrentAdvancedPaymentBy($type, $paymentMethod)
    {
        $whereClause = NULL;
        if ($type == 'year') {
            $whereClause = date('Y');
        } else if ($type == 'month') {
            $whereClause = date('Y-m');
        } else {
            $whereClause = date('Y-m-d');
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%m")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%m %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%d")'),
                    'Year' => new Expression('Year(outgoingPaymentDate)'),
                    'TotalCashOutFlow' => new Expression('SUM(outgoingPaymentAmount)')
                ))
                ->join('paymentMethod', 'outgoingPayment.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('entity', 'entity.entityID = outgoingPayment.entityID', '*', 'left')
                ->where(array('outgoingPayment.paymentMethodID' => $paymentMethod))
                ->where(array('entity.deleted' => 0))
        ->where->like('outgoingPaymentDate', '%' . $whereClause . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param Date $fromData
     * @param Date $toData
     * @param String $format
     * @return $resultArray
     */
    function getCashOutFlowData($fromData, $toData, $format = NULL)
    {
        $groupArray = array();
        if ($format == 'year') {
            $groupArray = array('Year');
        } else if ($format == 'month') {
            $groupArray = array('Month', 'Year');
        } else {
            $groupArray = array('Date', 'Month', 'Year');
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array(
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%d")'),
                    'Year' => new Expression('Year(outgoingPaymentDate)'),
                    'TotalCashOutFlow' => new Expression('SUM(outGoingPaymentMethodPaidAmount)')
                ))
                // ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount'), 'left')
                // ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outgoingPaymentID',array('*'),'left')
                ->join('paymentMethod', 'outGoingPaymentMethodsNumbers.outGoingPaymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('entity', 'entity.entityID = outgoingPayment.entityID', '*', 'left')
                ->group($groupArray)
                ->order(array('outgoingPayment.outgoingPaymentDate'))
                ->where(array('entity.deleted' => 0))
                ->where(array('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID' => 1))
        ->where->between('outgoingPaymentDate', $fromData, $toData);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param Date $fromData
     * @param Date $toData
     * @param String $format
     * @return $resultArray
     */
    function getAdvancedPaymentData($fromData, $toData, $format = NULL, $paymentType)
    {
        $groupArray = array();
        if ($format == 'year') {
            $groupArray = array('Year');
        } else if ($format == 'month') {
            $groupArray = array('Month', 'Year');
        } else {
            $groupArray = array('Date', 'Month', 'Year');
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array(
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%d")'),
                    'Year' => new Expression('Year(outgoingPaymentDate)'),
                    'TotalCashOutFlow' => new Expression('SUM(outgoingPaymentAmount)')
                ))
                ->join('paymentMethod', 'outgoingPayment.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('entity', 'entity.entityID = outgoingPayment.entityID', '*', 'left')
                ->group($groupArray)
                ->order(array('outgoingPayment.outgoingPaymentDate'))
                ->where(array('entity.deleted' => 0))
                ->where(array('outgoingPayment.paymentMethodID' => $paymentType))
        ->where->between('outgoingPaymentDate', $fromData, $toData);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    /**
     * Get Cash Payments by Location
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @return type
     */
    public function getCashAdvancePaymentsByLocation($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->join('entity', 'outgoingPayment.entityID = entity.entityID', ['deleted']);
        $select->where([
            'outgoingPayment.locationID' => $locationID,
            'deleted' => 0,
            'outgoingPayment.paymentMethodID' => '1'
        ]);

        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getCashPurchaseInvoicePaymentsByLocation($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', ['*'], 'left');
        $select->join('entity', 'outgoingPayment.entityID = entity.entityID', ['deleted']);
        $select->where([
            'outgoingPayment.locationID' => $locationID,
            'deleted' => 0,
            'outgoingPaymentInvoice.paymentMethodID' => '1'
        ]);

        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getPaginatedPaymentsData($locationID,$whichPayment = NULL)
    {

            $select = new \Zend\Db\Sql\Select;
            $select->from('outgoingPayment')
                    ->columns(array('*'))
                    ->order(array('outgoingPaymentDate' => 'DESC', 'outgoingPaymentCode' => 'DESC',))
                    ->join('entity', 'outgoingPayment.entityID= entity.entityID ', array('deleted'), 'left')
                    ->join('supplier', ' supplier.supplierID= outgoingPayment.supplierID ', array('supplierName', 'supplierCode', 'supplierTitle'), 'left')
                    ->join('outgoingPaymentInvoice', ' outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID ', array('invoiceID','paymentVoucherId'), 'left')
                    ->group('outgoingPaymentID')
                    ->where(array('locationID' => $locationID));
            if($whichPayment=='PI'){
                $select->where->notEqualTo('invoiceID',0);
            }else if($whichPayment=='PV'){
                $dataSet = [0];
                $select->where->AND->NEST->in('invoiceID',$dataSet)
                        ->orPredicate(
                new \Zend\Db\Sql\Predicate\IsNotNull('paymentVoucherId'));
                $select->where(array('outgoingPaymentType'=>'invoice'));
            }else if($whichPayment=='PVAdv'){
                $dataSet = [0];
                $select->where->AND->NEST->in('invoiceID',$dataSet)
                        ->orPredicate(
                new \Zend\Db\Sql\Predicate\IsNull('paymentVoucherId'));
                $select->where(array('outgoingPaymentType'=>'advance'));
            }
            return new \Zend\Paginator\Paginator(
                    new \Zend\Paginator\Adapter\DbSelect(
                        $select, $this->tableGateway->getAdapter()
                    )
            );
    }

    /**
    * get all supplier paymentDetails
    *
    **/
    public function getPaymentsBySupplierIDs($supplierID = [], $status, $fromdate = null, $todate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('*'))
                ->join('supplier', 'supplier.supplierID = outgoingPayment.supplierID', array('*'), 'left')
                // ->join('outgoingPaymentInvoice', ' outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID ', array('invoiceID','paymentVoucherId', 'outgoingInvoiceCreditAmount'), 'left')
                ->join("status", "outgoingPayment.outgoingPaymentStatus = status.statusID", array("*"), "left");
        $select->where->in('outgoingPayment.supplierID', $supplierID);
        $select->where->in('outgoingPayment.outgoingPaymentStatus', $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('outgoingPayment.outgoingPaymentDate', $fromdate, $todate);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getSupplierChequesByPaymentId($paymentId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('*'))
                ->join('outGoingPaymentMethodsNumbers', 'outGoingPaymentMethodsNumbers.outgoingPaymentID = outgoingPayment.outgoingPaymentID', array('*'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID ', array('deleted'))
        ->where->equalTo('entity.deleted', 0);
        $select->where->equalTo('outgoingPayment.outgoingPaymentID', $paymentId);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    public function getPaymentsBySupplierIDsForSupplierBalance($supplierID = [], $status, $fromdate = null, $todate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('*'))
                ->join('supplier', 'supplier.supplierID = outgoingPayment.supplierID', array('*'), 'left')
                // ->join('outgoingPaymentInvoice', ' outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID ', array('invoiceID','paymentVoucherId', 'outgoingInvoiceCreditAmount','outgoingInvoiceCashAmount'), 'left')
                // ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceTotal','invoiceLeftToPayAmount' => new Expression('purchaseInvoiceTotal-purchaseInvoicePayedAmount')), 'left')
                // ->join('paymentVoucher', 'outgoingPaymentInvoice.paymentVoucherId = paymentVoucher.paymentVoucherID', array('paymentVoucherTotal','pvLeftToPayAmount' => new Expression('paymentVoucherTotal-paymentVoucherPaidAmount')), 'left')
                ->join("status", "outgoingPayment.outgoingPaymentStatus = status.statusID", array("*"), "left")
                ->join('entity', 'outgoingPayment.entityID = entity.entityID ', array('deleted','createdTimeStamp'),'left');
        $select->where->in('outgoingPayment.supplierID', $supplierID);
        $select->where->in('outgoingPayment.outgoingPaymentStatus', $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('entity.createdTimeStamp', $fromdate, $todate);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
    
    public function getPiPaymentDetailsByPoId($poId = null, $grnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('*'));
        $select->join('outgoingPaymentInvoice','outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID',array('*'),'left');
        $select->join('purchaseInvoice','outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID',array('*'),'left');
        $select->join('entity','outgoingPayment.entityID = entity.entityID',array('*'),'left');
        if (!is_null($poId)) {
            $select->where(array('purchaseInvoice.purchaseInvoicePoID' => $poId));
        }
        if (!is_null($grnID)) {
            $select->where(array('purchaseInvoice.purchaseInvoiceGrnID' => $grnID));
        }
        $select->group("outgoingPayment.outgoingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }
    
    public function getPiPaymentDetailsByCopiedPoId($poId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('*'));
        $select->join('outgoingPaymentInvoice','outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID',array('*'),'left');
        $select->join('purchaseInvoice','outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID',array('*'),'left');
        $select->join('grnProduct','purchaseInvoice.purchaseInvoiceGrnID = grnProduct.grnID',array('grnID'),'left');
        $select->join('entity','outgoingPayment.entityID = entity.entityID',array('*'),'left');
        $select->where(array('grnProduct.grnProductDocumentId' => $poId, 'grnProduct.grnProductDocumentTypeId' => 9));
        $select->group("outgoingPayment.outgoingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    // get pi payments by pi id
    public function getPiPaymentDetailsByPiId($piId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('*'));
        $select->join('outgoingPaymentInvoice','outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID',array('*'),'left');
        $select->join('purchaseInvoice','outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID',array('*'),'left');
        $select->join('entity','outgoingPayment.entityID = entity.entityID',array('*'),'left');
        $select->where(array('purchaseInvoice.purchaseInvoiceID' => $piId));
        $select->group("outgoingPayment.outgoingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getPiPaymentDetailsThroughPoByPrId($prId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('*'));
        $select->join('outgoingPaymentInvoice','outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID',array('*'),'left');
        $select->join('purchaseInvoice','outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID',array('*'),'left');
        $select->join('purchaseOrder','purchaseInvoice.purchaseInvoicePoID = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('grnProduct','grnProduct.grnProductDocumentId = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('purchaseReturn','grnProduct.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
        $select->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseReturn.purchaseReturnID' => $prId,'grnProduct.grnProductDocumentTypeId' => 9));
        $select->group("outgoingPayment.outgoingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getPiPaymentDetailsThroughGrnByPrId($prId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('*'));
        $select->join('outgoingPaymentInvoice','outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID',array('*'),'left');
        $select->join('purchaseInvoice','outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID',array('*'),'left');
        $select->join('grn','grn.grnID = purchaseInvoice.purchaseInvoiceGrnID',array('*'),'left');
        $select->join('purchaseReturn','grn.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
        $select->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseReturn.purchaseReturnID' => $prId));
        $select->group("outgoingPayment.outgoingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getPiPaymentDetailsByPrDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('*'));
        $select->join('outgoingPaymentInvoice','outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID',array('*'),'left');
        $select->join('purchaseInvoice','outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID',array('*'),'left');
        $select->join('debitNote','purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID',array('*'),'left');
        $select->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('debitNote.debitNoteID' => $debitNoteID));
        $select->group("outgoingPayment.outgoingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    /**
    * this function use to get payment details and payment related payment voucher and purchase invoice details
    * @param $paymentID
    * return mix
    **/
    public function getPaymentWithInvoiceDetailByPaymentID($paymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array("*"))
                ->join('supplier', 'outgoingPayment.supplierID =  supplier.supplierID', array("*"), "left")
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID =  outgoingPaymentInvoice.paymentID', array("*"), "left")
                ->where(array('outgoingPayment.outgoingPaymentID' => $paymentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        
        return $rowset;
    }

    /**
    * this function use to get all active payment details and purchase invoice/payment voucher details that related to the 
    * given purchase Invoice id or payment voucher ID
    * @param int $documentID
    * @param string $documentType
    * return mix
    **/
    public function getPaymentDetailsByPIIdOrPVId($documentID, $documentType)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('*'));
        $select->join('outgoingPaymentInvoice','outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID',array('*'),'left');
        $select->where(array('outgoingPayment.outgoingPaymentStatus' => 4));
        if ($documentType == 'purchaseInvoice') {
            $select->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceTotal','purchaseInvoicePayedAmount'),'left');
            $select->where(array('outgoingPaymentInvoice.invoiceID' => $documentID));
        } else if ($documentType == 'paymentVoucher') {
            $select->join('paymentVoucher', 'outgoingPaymentInvoice.paymentVoucherId = paymentVoucher.paymentVoucherID', array('paymentVoucherTotal','paymentVoucherPaidAmount','paymentVoucherAccountID'), 'left');
            $select->where(array('outgoingPaymentInvoice.paymentVoucherId' => $documentID));
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    /**
    * this function use to get purchase payment total value for daily, monthly and annual 
    * @param date $currentDate
    * @param date $startDate
    * @param string $type
    * return mix
    **/
    public function getPaymentValueByGivenDateRange($currentDate, $startDate, $type)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID');
        $select->columns(array('totalMoneyPayments' => new Expression('SUM(outgoingPaymentInvoice.outgoingInvoiceCashAmount)'), 'totalCreditPayments' => new Expression('SUM(outgoingPaymentInvoice.outgoingInvoiceCreditAmount)')));
        $select->where->notEqualto('outgoingPaymentInvoice.invoiceID', 0); // zero mean payment vouchers
        $select->where(array('outgoingPayment.outgoingPaymentType' => 'invoice', 'outgoingPayment.outgoingPaymentStatus' => 4));
        
        if ($type == 'daily') {
            $select->where->equalTo('outgoingPaymentDate', $currentDate);
        } else {
            $select->where->between('outgoingPaymentDate', $startDate, $currentDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getPaymentDetailsForDonut($currentDate, $startDate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('*'));
        $select->where(array('outgoingPayment.outgoingPaymentType' => 'invoice'));
        $select->where->notEqualto('outgoingPayment.outgoingPaymentStatus', 5);  // 5 mean deleted
        $select->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingInvoiceCashAmount', 'outgoingInvoiceCreditAmount'), 'left');
        $select->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodID'), 'left');
        $select->where->notEqualto('outgoingPaymentInvoice.invoiceID', 0); // zero mean payment vouchers
        if ($startDate != null) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $currentDate);
        } else {
            $select->where(array('outgoingPayment.outgoingPaymentDate' => $currentDate));

        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;      
    }

    public function getPaymentCountByGivenDate($currentDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID');
        $select->columns(array('totalPaymentCount' => new Expression('COUNT(outgoingPayment.outgoingPaymentID)')));
        $select->where->notEqualto('outgoingPaymentInvoice.invoiceID', 0); // zero mean payment vouchers
        $select->where(array('outgoingPayment.outgoingPaymentType' => 'invoice', 'outgoingPayment.outgoingPaymentStatus' => 4));
        $select->where->equalTo('outgoingPaymentDate', $currentDate);
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getPaymentValuesByGivenDateRange($startDate, $endDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID');
        $select->columns(array('paymentTotal' => new Expression('SUM(outgoingPaymentInvoice.outgoingInvoiceCashAmount + outgoingPaymentInvoice.outgoingInvoiceCreditAmount)'), 'outgoingPaymentDate'));
        $select->where->notEqualto('outgoingPaymentInvoice.invoiceID', 0); // zero mean payment vouchers
        $select->where(array('outgoingPayment.outgoingPaymentType' => 'invoice', 'outgoingPayment.outgoingPaymentStatus' => 4));
        $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        $select->group('outgoingPayment.outgoingPaymentDate');
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getPaymentDetailsForCashFlow($startDate, $endDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outgoingPaymentID',['*'],'left');
        $select->join('journalEntry','journalEntry.journalEntryDocumentID = outgoingPayment.outgoingPaymentID', array('journalEntryID'),'left')
                ->join(array('JEAc' => 'journalEntryAccounts'), "JEAc.journalEntryID = journalEntry.journalEntryID AND JEAc.financeAccountsID = outGoingPaymentMethodsNumbers.outGoingPaymentMethodFinanceAccountID", ['debit' => new Expression('JEAc.journalEntryAccountsDebitAmount'),'credit' => new Expression('JEAc.journalEntryAccountsCreditAmount')], 'left');
        $select->join('entity', 'entity.entityID = outgoingPayment.entityID', ['*'], 'left');
        $select->where(array('entity.deleted' => 0, 'outgoingPayment.outgoingPaymentStatus' => 4));
        $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        $select->group('outGoingPaymentMethodsNumbers.outGoingPaymentMethodsNumbersID');
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getPiTotalPaymentsByPiIdAnd($piId, $outgoingPaymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('*'));
        $select->join('outgoingPaymentInvoice','outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID',array('piPayed' => new Expression('SUM(outgoingInvoiceCashAmount) + SUM(outgoingInvoiceCreditAmount)')),'left');
        $select->join('entity','outgoingPayment.entityID = entity.entityID',array('*'),'left');
        $select->where(array('outgoingPaymentInvoice.invoiceID' => $piId));
        $select->where->lessThan('outgoingPayment.outgoingPaymentID', $outgoingPaymentID);
        $select->group("outgoingPayment.outgoingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getTotalPaymentByGivenDateRangeAndLocationID($startDate, $endDate, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('paymentTotal' => new Expression('SUM(outgoingPayment.outgoingPaymentPaidAmount)'), 'outgoingPaymentDate'));
        $select->where(array('outgoingPayment.outgoingPaymentType' => 'invoice', 'outgoingPayment.outgoingPaymentStatus' => 4));
        $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        $select->where(array('outgoingPayment.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function getPaymentsWithMethodsByGivenDateRangeAndLocationID($startDate, $endDate, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment');
        $select->columns(array('outgoingPaymentDate'))
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outgoingPaymentID',array('*','paymentTotal' => new Expression('SUM(outGoingPaymentMethodsNumbers.outGoingPaymentMethodPaidAmount)')),'left')
                ->join('paymentMethod', 'outGoingPaymentMethodsNumbers.outGoingPaymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left');
        $select->where(array('outgoingPayment.outgoingPaymentType' => 'invoice', 'outgoingPayment.outgoingPaymentStatus' => 4));
        $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        $select->where(array('outgoingPayment.locationID' => $locationID));
        $select->group("outGoingPaymentMethodsNumbers.outGoingPaymentMethodID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
