<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class GoodsIssueType
{

    public $goodsIssueTypeID;
    public $goodsIssueTypeName;
    public $goodsIssueTypeDescription;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->goodsIssueTypeID = (!empty($data['goodsIssueTypeID'])) ? $data['goodsIssueTypeID'] : null;
        $this->goodsIssueTypeName = (!empty($data['goodsIssueTypeName'])) ? $data['goodsIssueTypeName'] : null;
        $this->goodsIssueTypeDescription = (!empty($data['goodsIssueTypeDescription'])) ? $data['goodsIssueTypeDescription'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

