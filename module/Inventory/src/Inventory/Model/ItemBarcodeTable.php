<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class ItemBarcodeTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveItemBarcode(ItemBarcode $itemBarcode)
    {
        $data = array(
            'itemBarcodeID' => $itemBarcode->itemBarcodeID,
            'productID' => $itemBarcode->productID,
            'barCode' => $itemBarcode->barCode
        );

        try {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        } catch (\Exception $exc) {
            echo $exc;
            return false;
        }
    }

    public function getBarcodeByName($itemBarcode, $productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemBarcode')
                ->columns(array('*'))
                ->where(array('itemBarcode.barCode' => $itemBarcode));
        $select->where->notEqualTo('itemBarcode.productID', $productID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getRelatedBarCodesByProductID($productID, $isOutputArray = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemBarcode')
                ->columns(array('*'))
                ->where(array('itemBarcode.productID' => $productID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$isOutputArray) {
            return $results;
        } else {
            $dataSet = [];
            foreach ($results as $key => $value) {
                $dataSet[] = $value;
            }
            return $dataSet;
        }
    }

    public function deleteBarCodeByItemBarCodeID($itemBarCodeID) {
        try {
            $result = $this->tableGateway->delete(array('itemBarcodeID' => $itemBarCodeID));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }

}
