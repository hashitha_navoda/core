<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Debit Note Sub Product  Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DebitNoteSubProductTable
{

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDebitNoteSubProduct(DebitNoteSubProduct $debitNoteSubProduct)
    {
        $data = array(
            'debitNoteProductID' => $debitNoteSubProduct->debitNoteProductID,
            'productBatchID' => $debitNoteSubProduct->productBatchID,
            'productSerialID' => $debitNoteSubProduct->productSerialID,
            'debitNoteSubProductQuantity' => $debitNoteSubProduct->debitNoteSubProductQuantity,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

}
