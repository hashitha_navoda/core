<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PurchaseRequisitionProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePurchaseRequisitionProductTax(PurchaseRequisitionProductTax $poProductTax)
    {
        $data = array(
            'purchaseRequisitionID' => $poProductTax->purchaseRequisitionID,
            'purchaseRequisitionProductID' => $poProductTax->purchaseRequisitionProductID,
            'purchaseRequisitionTaxID' => $poProductTax->purchaseRequisitionTaxID,
            'purchaseRequisitionTaxPrecentage' => $poProductTax->purchaseRequisitionTaxPrecentage,
            'purchaseRequisitionTaxAmount' => $poProductTax->purchaseRequisitionTaxAmount,
        );
        $this->tableGateway->insert($data);
    }

    
    public function deleteDraftRecordTaxesByPoID($poID)
    {
        try {
            $result = $this->tableGateway->delete(array('purchaseRequisitionID' => $poID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

}

