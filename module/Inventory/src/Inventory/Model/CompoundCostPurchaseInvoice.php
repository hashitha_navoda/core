<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CompoundCostPurchaseInvoice
{

    public $compoundCostPurchaseInvoiceID;
    public $compoundCostTemplateID;
    public $purchaseInvoiceID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->compoundCostPurchaseInvoiceID = (!empty($data['compoundCostPurchaseInvoiceID'])) ? $data['compoundCostPurchaseInvoiceID'] : null;
        $this->compoundCostTemplateID = (!empty($data['compoundCostTemplateID'])) ? $data['compoundCostTemplateID'] : 0;
        $this->purchaseInvoiceID = (!empty($data['purchaseInvoiceID'])) ? $data['purchaseInvoiceID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}