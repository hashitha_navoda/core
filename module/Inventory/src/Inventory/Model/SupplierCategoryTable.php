<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 * This for add or edit supplierCategory
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Sql;

class SupplierCategoryTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select('supplierCategory');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new SupplierCategory());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $adapter = $this->tableGateway->getAdapter();
        $tg = new TableGateway('supplierCategory', $adapter);
        $rowset = $tg->select(function (Select $select) {
            $select->order('supplierCategoryID');
        });

        return $rowset;
    }

    public function fetchAllAsArray()
    {
        $allCategoryList = array();
        foreach ($this->fetchAll(false) as $cat) {
            $allCategoryList[$cat->supplierCategoryID] = $cat->supplierCategoryName;
        }

        return $allCategoryList;
    }

    public function saveSupplierCategory(SupplierCategory $supplierCategory)
    {
        $data = array(
//            'supplierCategoryID' => $supplierCategory->supplierCategoryID,
            'supplierCategoryName' => $supplierCategory->supplierCategoryName,
        );

        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function updateSupplierCategory(SupplierCategory $supplierCategory)
    {
        $data = array(
            'supplierCategoryID' => $supplierCategory->supplierCategoryID,
            'supplierCategoryName' => $supplierCategory->supplierCategoryName,
        );

        $result = $this->tableGateway->update($data, array('supplierCategoryID' => $supplierCategory->supplierCategoryID));
        return $result;
    }

    public function deleteSupplierCategory($supplierCategoryID)
    {
        try {
            $result = $this->tableGateway->delete(array('supplierCategoryID' => $supplierCategoryID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getSupplierCategoryByID($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('supplierCategoryID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getSupplierCategoryByName($supplierCategoryName)
    {
        $rowset = $this->tableGateway->select(array('supplierCategoryName' => $supplierCategoryName));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getCategoryforSearch($keyword)
    {
        $tbl = $this->tableGateway;

        $rowset = $tbl->select(function (Select $select) use ($keyword) {
            $select->where(new PredicateSet(array(new Operator('supplierCategoryName', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
            $select->order('supplierCategoryName ASC');
        });

        return $rowset;
    }

    public function getBySupplierCategoryBySupplierCategoryName($supplierCategoryName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplierCategory')
                ->columns(array('*'))
                ->where(array('supplierCategoryName' => $supplierCategoryName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results->current();
    }

}
