<?php

/**
 * @author Peushani Jayasekara <peushani@thinkcube.com>
 * This file contains product Image Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class ProductImageTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveImageDetails(ProductImage $productImage)
    {
        $data = array(
            'productImageID' => $productImage->productImageID,
            'productImageName' => $productImage->productImageName,
            'productID' => $productImage->productID,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function deleteImageDetails($productID)
    {
        try {
            $this->tableGateway->delete(array('productID' => $productID));

//return TRUE;
        } catch (\Exception $exc) {
            return FALSE;
        }
    }

    public function getDataByProductID($productID)
    {
        $rowset = $this->tableGateway->select(array('productID' => $productID));
        $row = $rowset->current();
        $row = (array) $row;
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    public function getImageNameByProductID($productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productImage')
                ->columns(array('productImageName'))
                ->where(array('productID' => $productID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

}
