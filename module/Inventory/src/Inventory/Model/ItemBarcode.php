<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ItemBarcode
{

    public $itemBarcodeID;
    public $productID;
    public $barCode;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->itemBarcodeID = (!empty($data['itemBarcodeID'])) ? $data['itemBarcodeID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->barCode = (!empty($data['barCode'])) ? $data['barCode'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
