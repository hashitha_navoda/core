<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Unit of Measure Table Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TransferProduct
{

    public $transferProductID;
    public $transferID;
    public $productID;
    public $productBatchID;
    public $productSerialID;
    public $transferProductQuantity;
    public $selectedUomId;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->transferProductID = (!empty($data['transferProductID'])) ? $data['transferProductID'] : 0;
        $this->transferID = (!empty($data['transferID'])) ? $data['transferID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : 0;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->transferProductQuantity = (!empty($data['transferProductQuantity'])) ? $data['transferProductQuantity'] : null;
        $this->selectedUomId = (!empty($data['selectedUomId'])) ? $data['selectedUomId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
