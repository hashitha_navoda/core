<?php
namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Depreciation
{

    public $depreciationID;
    public $productID;
    public $depreciationValue;
    public $depreciationDate;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->depreciationID = (!empty($data['depreciationID'])) ? $data['depreciationID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->depreciationValue = (!empty($data['depreciationValue'])) ? $data['depreciationValue'] : null;
        $this->depreciationDate = (!empty($data['depreciationDate'])) ? $data['depreciationDate'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function getInputFilter()
    {

    }

}

