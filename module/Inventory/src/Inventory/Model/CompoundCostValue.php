<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CompoundCostValue
{

    public $compoundCostValueID;
    public $compoundCostTypeID;
    public $amount;
    public $compoundCostTemplateID;
    public $supplierID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->compoundCostValueID = (!empty($data['compoundCostValueID'])) ? $data['compoundCostValueID'] : null;
        $this->compoundCostTypeID = (!empty($data['compoundCostTypeID'])) ? $data['compoundCostTypeID'] : null;
        $this->amount = (!empty($data['amount'])) ? $data['amount'] : null;
        $this->compoundCostTemplateID = (!empty($data['compoundCostTemplateID'])) ? $data['compoundCostTemplateID'] : null;
        $this->supplierID = (!empty($data['supplierID'])) ? $data['supplierID'] : null;
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
