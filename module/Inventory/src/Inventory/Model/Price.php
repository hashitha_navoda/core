<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Price implements InputFilterAwareInterface
{

    public $priceID;
    public $priceAmount;
    public $priceDiscountEligible;
    public $priceDiscount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->priceID = (!empty($data['priceID'])) ? $data['priceID'] : null;
        $this->priceAmount = (!empty($data['priceAmount'])) ? $data['priceAmount'] : null;
        $this->priceDiscountEligible = (!empty($data['priceDiscountEligible'])) ? $data['priceDiscountEligible'] : null;
        $this->priceDiscount = (!empty($data['priceDiscount'])) ? $data['priceDiscount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function getInputFilter()
    {

    }

}

