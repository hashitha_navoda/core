<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains price list Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PriceList implements InputFilterAwareInterface
{

    public $priceListId;
    public $priceListName;
    public $entityID;
    public $priceListState;
    public $priceListGlobal;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->priceListId = (!empty($data['priceListId'])) ? $data['priceListId'] : null;
        $this->priceListName = (!empty($data['priceListName'])) ? $data['priceListName'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : 0;
        $this->priceListState = (!empty($data['priceListState'])) ? $data['priceListState'] : 0;
        $this->priceListGlobal = (!empty($data['priceListGlobal'])) ? $data['priceListGlobal'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
