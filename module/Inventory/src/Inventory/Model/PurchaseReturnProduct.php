<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the POProduct model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseReturnProduct implements InputFilterAwareInterface
{

    public $purchaseReturnProductID;
    public $purchaseReturnID;
    public $purchaseReturnLocationProductID;
    public $purchaseReturnProductBatchID;
    public $purchaseReturnProductSerialID;
    public $purchaseReturnProductPrice;
    public $purchaseReturnProductDiscount;
    public $purchaseReturnProductPurchasedQty;
    public $purchaseReturnProductReturnedQty;
    public $purchaseReturnSubProductReturnedQty;
    public $purchaseReturnProductTotal;
    public $purchaseReturnProductGrnProductID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseReturnProductID = (!empty($data['purchaseReturnProductID'])) ? $data['purchaseReturnProductID'] : null;
        $this->purchaseReturnID = (!empty($data['purchaseReturnID'])) ? $data['purchaseReturnID'] : null;
        $this->purchaseReturnLocationProductID = (!empty($data['purchaseReturnLocationProductID'])) ? $data['purchaseReturnLocationProductID'] : null;
        $this->purchaseReturnProductBatchID = (!empty($data['purchaseReturnProductBatchID'])) ? $data['purchaseReturnProductBatchID'] : null;
        $this->purchaseReturnProductSerialID = (!empty($data['purchaseReturnProductSerialID'])) ? $data['purchaseReturnProductSerialID'] : null;
        $this->purchaseReturnProductPrice = (!empty($data['purchaseReturnProductPrice'])) ? $data['purchaseReturnProductPrice'] : 0.00;
        $this->purchaseReturnProductDiscount = (!empty($data['purchaseReturnProductDiscount'])) ? $data['purchaseReturnProductDiscount'] : null;
        $this->purchaseReturnProductPurchasedQty = (!empty($data['purchaseReturnProductPurchasedQty'])) ? $data['purchaseReturnProductPurchasedQty'] : 0;
        $this->purchaseReturnProductReturnedQty = (!empty($data['purchaseReturnProductReturnedQty'])) ? $data['purchaseReturnProductReturnedQty'] : 0;
        $this->purchaseReturnSubProductReturnedQty = (!empty($data['purchaseReturnSubProductReturnedQty'])) ? $data['purchaseReturnSubProductReturnedQty'] : 0;
        $this->purchaseReturnProductTotal = (!empty($data['purchaseReturnProductTotal'])) ? $data['purchaseReturnProductTotal'] : 0.00;
        $this->purchaseReturnProductGrnProductID = (!empty($data['purchaseReturnProductGrnProductID'])) ? $data['purchaseReturnProductGrnProductID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

