<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class LocationProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveLocationProduct(LocationProduct $product)
    {
        $data = array(
            'productID' => $product->productID,
            'locationID' => $product->locationID,
            'locationDiscountPercentage' => $product->locationDiscountPercentage,
            'locationDiscountValue' => $product->locationDiscountValue,
            'defaultSellingPrice' => $product->defaultSellingPrice,
            'locationProductMinInventoryLevel' => $product->locationProductMinInventoryLevel,
            'locationProductMaxInventoryLevel' => $product->locationProductMaxInventoryLevel,
            'locationProductReOrderLevel' => $product->locationProductReOrderLevel,
            'entityID' => $product->entityID,
            'defaultPurchasePrice' => $product->defaultPurchasePrice,
            'isUseDiscountSchemeForLocation' => $product->isUseDiscountSchemeForLocation,
            'locationDiscountSchemID' => $product->locationDiscountSchemID,
        );
        try {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getLocationProduct($productID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns(array('*'))
                ->join('product', 'locationProduct.productID = product.productID', array('*'),'left')
                ->join('entity', 'locationProduct.entityID = entity.entityID', array('*'), 'left')
                ->where(array('locationProduct.productID' => $productID, 'locationID' => $locationID, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function getLocationProductsByIds($productIDs = [], $locationIDs = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns(array('*'))
                ->join('entity', 'locationProduct.entityID = entity.entityID', array('*'), 'left');

        if ($productIDs) {
            $select->where->in('productID', $productIDs);
        }



        if ($locationIDs) {
            $select->where->in('locationID', $locationIDs);
        }

        $select->where(array('entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getLocationProductByLocationProductID($locationProductID)
    {
        $rowset = $this->tableGateway->select(array('locationProductID' => $locationProductID));
        $row = $rowset->current();
        return $row;
    }

    public function updateLocationProductQuantity($data, $locationProductID)
    {
        try {
            $this->tableGateway->update($data, array('locationProductID' => $locationProductID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getProductsFromLocation($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('*'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->join(array('lcpentity' => 'entity'), 'locationProduct.entityID = lcpentity.entityID', array('*'), 'left')
                ->where(array('locationProduct.locationID' => $locationID, 'entity.deleted' => '0', 'lcpentity.deleted' => '0'))
                ->order(array('product.productName' => 'ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getLocationsByProductID($productID, $getDeleted = false)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $deleted = array('entity.deleted' => 0);

        // if param is set, get all entries regardless of the deleted flag
        if ($getDeleted) {
            $deleted = array();
        }

        $select->from('locationProduct')
                ->columns(array('*'))
                ->join('location', 'locationProduct.locationID = location.locationID', array('*'), 'left')
                ->join('entity', 'locationProduct.entityID = entity.entityID', array('*'), 'left')
                ->where(array_merge(array('locationProduct.productID' => $productID), $deleted));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function updateLocationProduct(LocationProduct $product)
    {
        $data = array(
            'locationProductMinInventoryLevel' => $product->locationProductMinInventoryLevel,
            'locationProductMaxInventoryLevel' => $product->locationProductMaxInventoryLevel,
            'locationProductReOrderLevel' => $product->locationProductReOrderLevel,
            'defaultSellingPrice' => $product->defaultSellingPrice,
            'defaultPurchasePrice' => $product->defaultPurchasePrice,
            'locationDiscountPercentage' => $product->locationDiscountPercentage,
            'locationDiscountValue' => $product->locationDiscountValue,
        );

        try {
            $this->tableGateway->update($data, array('locationProductID' => $product->locationProductID));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public function updateLocationProductDiscount(LocationProduct $product)
    {
        $data = array(
            'locationDiscountPercentage' => $product->locationDiscountPercentage,
            'locationDiscountValue' => $product->locationDiscountValue,
        );

        try {
            $this->tableGateway->update($data, array('locationProductID' => $product->locationProductID));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    /**
     * @author Sandun  <Sandun@thinkcube.com>
     * @param int $productID
     * @param int $loctionID
     * @return $row
     */
    public function getLocationProductByProIdLocId($productID, $loctionID)
    {
        $rowset = $this->tableGateway->select(array('productID' => $productID, 'locationID' => $loctionID));
        $row = $rowset->current();
        return $row;
    }

    public function updateLocationAllProduct($productData, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        foreach ($productData as $key) {
            $updateProduct[$key['productID']] = $sql->update();
            $updateProduct[$key['productID']]->table('locationProduct')
                    ->set(array(
                        'locationProductMinInventoryLevel' => $key['locationProductMinInventoryLevel'],
                        'locationProductMaxInventoryLevel' => $key['locationProductMaxInventoryLevel'],
                        'locationProductReOrderLevel' => $key['locationProductReOrderLevel'],
                        'defaultSellingPrice' => $key['defaultSellingPrice'],
                        'locationDiscountPercentage' => $key['locationDiscountPercentage'],
                        'locationDiscountValue' => $key['locationDiscountValue'],
                    ))
                    ->where(array('productID' => $key['productID'], 'locationID' => $locationID));
            $statementToProduct[$key['productID']] = $sql->getSqlStringForSqlObject($updateProduct[$key['productID']]);
        }
        $db = $this->tableGateway->getAdapter()->getDriver()->getConnection();
        $db->beginTransaction();
        try {
            foreach ($statementToProduct as $keys) {
                $db->execute($keys);
            }
            $db->commit();
            return true;
        } catch (\Exception $e) {
            $db->rollBack();
            return false;
        }
    }

    public function updateProductToAllLocation($productData, $productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        foreach ($productData as $key) {
            $updateProduct[$key['locationID']] = $sql->update();
            $updateProduct[$key['locationID']]->table('locationProduct')
                    ->set(array(
                        'locationProductMinInventoryLevel' => $key['locationProductMinInventoryLevel'],
                        'locationProductMaxInventoryLevel' => $key['locationProductMaxInventoryLevel'],
                        'locationProductReOrderLevel' => $key['locationProductReOrderLevel'],
                        'defaultSellingPrice' => $key['defaultSellingPrice'],
                        'locationDiscountPercentage' => $key['locationDiscountPercentage'],
                        'locationDiscountValue' => $key['locationDiscountValue'],
                    ))
                    ->where(array('productID' => $productID, 'locationID' => $key['locationID']));
            $statementToProduct[$key['locationID']] = $sql->getSqlStringForSqlObject($updateProduct[$key['locationID']]);
        }

        $db = $this->tableGateway->getAdapter()->getDriver()->getConnection();
        $db->beginTransaction();
        try {
            foreach ($statementToProduct as $keys) {
                $db->execute($keys);
            }
            $db->commit();
            return true;
        } catch (\Exception $e) {
            $db->rollBack();
            return false;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param int $pro_id
     * @param array $data
     * @return $row
     */
    public function updateQty($data, $pro_id, $loc_id)
    {
        $row = $this->tableGateway->update($data, array('productID' => $pro_id, 'locationID' => $loc_id));
        return $row;
    }

    public function updateQuantityByID($data, $id)
    {
        $row = $this->tableGateway->update($data, array('locationProductID' => $id));
        return $row;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param array $locIds
     * @param int $proType
     * @return $results
     */
    public function getProducts($locIds, $proType)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct');
        $select->columns(array('*'));
        $select->join('location', 'locationProduct.locationID = location.locationID', array('*'));
        $select->join('product', 'locationProduct.productID = product.productID', array('*'));
        $select->join('entity', 'product.entityID = entity.entityID', array('*'));
        if ($proType) {
            $select->where(array('product.productTypeID' => 1));
        }
        $select->order('product.productName');
        $select->where(array('entity.deleted' => '0'));
        $select->where->notEqualTo('product.productTypeID', 2);
        $select->where->in('locationProduct.locationID', $locIds);
        $select->order('product.productName ACS');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @param array $proIds
     * @param array $locIds
     */
    public function getProductQtyList($proIds, $locIds, $isAllProducts, $categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct');
        $select->columns(array('qty' => new Expression('SUM(locationProduct.locationProductQuantity)')));
        $select->join('product', 'locationProduct.productID = product.productID', array(
            'pCD' => new Expression('product.productCode'),
            'pName' => new Expression('product.productName'),
            'pID' => new Expression('product.productID'),
            'pD' => new Expression('product.productDescription'),
            'pS' => new Expression('product.productState'),'entityID'), 'left');
        $select->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left');
        if ($locIds != null) {
            $select->join('location', 'location.locationID = locationProduct.locationID', array(
                'locName' => new Expression('location.locationName'),
                'locID' => new Expression('location.locationID')), 'left');
        }
        $select->join('entity', 'product.entityID = entity.entityID', array('deleted'));
        $select->group('product.productCode');
        $select->order('product.productCode');
        $select->where->equalTo('entity.deleted', '0');
        $select->where->equalTo('product.productTypeID', 1);
        $select->where->equalTo('product.productState', 1);
        if ($locIds != null) {
            $select->where->in('locationProduct.locationID', $locIds);
        }
        if (!$isAllProducts) {
            $select->where->in('product.productID', $proIds);
        }

        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param array $locIds
     * @return $results
     */
    public function getCategorys($locIds)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('*'))
                ->join('product', 'locationProduct.productID = product.productID', array('*'), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array('*'), 'left')
                ->join('location', 'locationProduct.locationID = location.locationID', array('*'), 'left')
                ->order('category.categoryName')
                ->where(array('category.categoryState' => 1))
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->in('location.locationID', $locIds);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @param array $proIds
     * @param array $locIds
     * @return $results
     */
    public function getProductQtyListByLocID($proIds, $locIds, $isAllProducts = false, $categoryIds = null, $activeFlag = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('defaultSellingPrice','defaultPurchasePrice',
                    'qty' => new Expression('locationProduct.locationProductQuantity'),
                    'locProID' => new Expression('locationProduct.locationProductID'),
                    'avgCost' => new Expression('locationProduct.locationProductAverageCostingPrice')))
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),
                    'pD' => new Expression('productDescription'),
                    'pDP' => new Expression('productDiscountPercentage'),
                    'productDefaultSellingPrice' => new Expression('productDefaultSellingPrice'),
                    'pDV' => new Expression('productDiscountValue'), 'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID'),
                    'locROL' => new Expression('locationProductReOrderLevel'),
                    'locMIL' => new Expression('locationProductMinInventoryLevel'),
                    'locMaxIL' => new Expression('locationProductMaxInventoryLevel'),
                    'locDP' => new Expression('locationDiscountPercentage'),
                    'locDV' => new Expression('locationDiscountValue')), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->in('locationProduct.locationID', $locIds);
        $select->where->equalTo('product.productState', '1');
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($isAllProducts){
            $select->where->equalTo('product.productState', '1');
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param array $proIds
     * @param array $locIds
     * @return $results
     */
    public function getItemWiseMinimumInvenLevelDetails($proIds, $locIds, $isAllProducts = false, $isReach = true)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array(
                    'qty' => new Expression('locationProduct.locationProductQuantity')))
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),
                    'maL' => new Expression('locationProduct.locationProductMaxInventoryLevel'),
                    'miL' => new Expression('locationProduct.locationProductMinInventoryLevel'), 'productState'))
                ->join('category', 'category.categoryID = product.categoryID', array(
                    'categoryName'))
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locID' => new Expression('location.locationID'),
                    'locationCode'))
                ->order('product.productName');

        if ($isReach) {
            $select->where('IFNULL(locationProduct.locationProductMinInventoryLevel,0) >= IFNULL(locationProduct.locationProductQuantity,0)');
        } 

        $select->where->EqualTo('product.productState', 1)
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->notEqualTo('locationProduct.locationProductMinInventoryLevel', 0)
            ->where->in('locationProduct.locationID', $locIds);
            if (!$isAllProducts) {
                $select->where->in('locationProduct.productID', $proIds);
            }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getItemWiseReorderLevelDetails($proIds, $locIds, $isAllProducts = false, $isReach = true)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array(
                    'qty' => new Expression('locationProduct.locationProductQuantity')))
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),
                    'maL' => new Expression('locationProduct.locationProductMaxInventoryLevel'),
                    'miL' => new Expression('locationProduct.locationProductMinInventoryLevel'),
                    'roL' => new Expression('locationProduct.locationProductReOrderLevel'), 'productState'))
                ->join('category', 'category.categoryID = product.categoryID', array(
                    'categoryName'))
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locID' => new Expression('location.locationID'),
                    'locationCode'))
                ->order('product.productName');

        if ($isReach) {
            $select->where('IFNULL(locationProduct.locationProductReOrderLevel,0) >= IFNULL(locationProduct.locationProductQuantity,0)');
        }

        $select->where->EqualTo('product.productState', 1)
                ->where->notEqualTo('locationProduct.locationProductReOrderLevel', 0)
                ->where->notEqualTo('product.productTypeID', 2)
                ->where->in('locationProduct.locationID', $locIds);
                if (!$isAllProducts) {
                    $select->where->in('locationProduct.productID', $proIds);
                }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getItemWiseMinimumInvenLevelDetailsForDahbaord($proIds, $locIds, $isAllProducts = false)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array(
                    'qty' => new Expression('locationProduct.locationProductQuantity')))
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),
                    'miL' => new Expression('locationProduct.locationProductMinInventoryLevel'), 'productState'))
                ->join('category', 'category.categoryID = product.categoryID', array(
                    'categoryName'),'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locID' => new Expression('location.locationID'),
                    'locationCode'))
                ->order('product.productName')
                ->where->EqualTo('product.productState', 1)
                ->where->notEqualTo('locationProduct.locationProductMinInventoryLevel', 0)
                ->where->notEqualTo('product.productTypeID', 2)
                ->where->in('locationProduct.locationID', $locIds);
                // if (!$isAllProducts) {
                //     $select->where->in('locationProduct.productID', $proIds);
                // }
        // $select->where->nest()->isNull('locationProduct.locationProductQuantity')
        // ->or->where->lessThanOrEqualTo('locationProduct.locationProductQuantity', 'locationProduct.locationProductMinInventoryLevel');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getOverdueReorderLevelItem($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array('*'));
        $select->from('locationProduct');
        $select->join('product', 'locationProduct.productID = product.productID', array('*'), 'left outer');
//        $select->join("logger", "logger.refID = invoice.id", array("refID", "userID"), "left outer");
//        $select->join("user", "logger.userID = user.userID", array("firstName", "email1"), "left outer");
        // TODO - check status ID
        $select->where->lessThanOrEqualTo('locationProduct.locationProductQuantity', new Expression('locationProduct.locationProductReOrderLevel'));
        $select->where(array('locationID' => $locationID));
        $select->order('product.productName ASC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getOverdueInventoryLevelItem($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array('*'));
        $select->from('locationProduct');
        $select->join('product', 'locationProduct.productID = product.productID', array('*'), 'left outer');
//        $select->join("logger", "logger.refID = invoice.id", array("refID", "userID"), "left outer");
//        $select->join("user", "logger.userID = user.userID", array("firstName", "email1"), "left outer");
        // TODO - check status ID
        $select->where->lessThanOrEqualTo('locationProduct.locationProductQuantity', new Expression('locationProduct.locationProductMinInventoryLevel'));
        $select->where(array('locationID' => $locationID));
        $select->order("product.productName ASC");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getLocationProductDetailsByProductID($productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns(array('*'))
                ->join('entity', 'locationProduct.entityID = entity.entityID', array('*'), 'left')
                ->where(array('productID' => $productID, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAllLocationProductsWithAllDetails()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns(array('locationProductID', 'locationProductQuantity', 'defaultSellingPrice'))
                ->join('entity', 'locationProduct.entityID = entity.entityID', array('entityID'), 'left')
                ->join('productBatch', 'locationProduct.locationProductID = productBatch.locationProductID', array('productBatchID', 'productBatchQuantity'), 'left')
                ->join('productSerial', 'locationProduct.locationProductID = productSerial.locationProductID', array('productSerialID', 'productSerialSold'), 'left')
                ->where(array('entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = array();
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function searchLocationProductsForDropdown($locationID, $searchKey, $flag = NULL, $isBomItm = False)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(['productCode', 'productName', 'productID', 'productBarcode','batchProduct','serialProduct',
                    'posi' => new Expression('LEAST('
                    . 'IF(POSITION(\'' . $searchKey . '\' in product.productName )>0,POSITION(\'' . $searchKey . '\' in product.productName), 9999),'
                    . 'IF(POSITION(\'' . $searchKey . '\' in product.productCode )>0,POSITION(\'' . $searchKey . '\' in product.productCode), 9999),'
                    . 'IF(POSITION(\'' . $searchKey . '\' in product.productBarcode )>0,POSITION(\'' . $searchKey . '\' in product.productBarcode), 9999)'
                    . ') '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(product.productName), CHAR_LENGTH(product.productCode), CHAR_LENGTH(product.productBarcode)) '),
                    'occr' => new Expression('LEAST('
                        .'IF((CHAR_LENGTH(product.productName) - CHAR_LENGTH(REPLACE(LOWER(product.productName), "' . $searchKey . '", ""))) >0, (CHAR_LENGTH(product.productName) - CHAR_LENGTH(REPLACE(LOWER(product.productName), "' . $searchKey . '", ""))), 9999),'
                        .'IF((CHAR_LENGTH(product.productCode) - CHAR_LENGTH(REPLACE(LOWER(product.productCode), "' . $searchKey . '", ""))) >0, (CHAR_LENGTH(product.productCode) - CHAR_LENGTH(REPLACE(LOWER(product.productCode), "' . $searchKey . '", ""))), 9999),'
                        .'IF((CHAR_LENGTH(product.productBarcode) - CHAR_LENGTH(REPLACE(LOWER(product.productBarcode), "' . $searchKey . '", ""))) >0, (CHAR_LENGTH(product.productBarcode) - CHAR_LENGTH(REPLACE(LOWER(product.productBarcode), "' . $searchKey . '", ""))), 9999)'
                        .')')
                    ])
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('locationProductID','locationProductQuantity'), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('productUomConversion','productUomDisplay'), 'left')
                ->join('productHandeling', 'product.productID = productHandeling.productID', array('*'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('entityID'), 'left')
                ->join(array('lcpentity' => 'entity'), 'locationProduct.entityID = lcpentity.entityID', array('entityID'), 'left')
                ->where(array('locationProduct.locationID' => $locationID, 'entity.deleted' => '0', 'lcpentity.deleted' => '0', 'productState' => 1, 'productUomDisplay'=> 1))
                ->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC, occr DESC'));

        if ($isBomItm) {
            $select->where->notEqualTo('product.productTypeID',2);
        }
        if ($flag != 1) {
            $select->where(array('productHandelingGiftCard' => 0));
        }
        $select->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $searchKey . '%'), new Operator('product.productCode', 'like', '%' . $searchKey . '%'), new Operator('product.productBarcode', 'like', '%' . $searchKey . '%'),new Operator('product.productDescription', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    // Search function for items select picker
    // Search returns result with serial and barcode numbers
    public function locationProductSerialSearch($locationID, $searchKey, $limit = 50)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array('productID'));
        $select->join(array('locEntity' => 'entity'), 'locationProduct.entityID = locEntity.entityID', array('deleted'));
        $select->join('productSerial', 'locationProduct.locationProductID = productSerial.locationProductID', array('productSerialID', 'productSerialCode'), 'left');
        $select->join(array('entity' => 'entity'), 'entity.entityID = product.entityID', array('deleted'), 'left');

        $select->where(new PredicateSet(array(new Operator('productSerial.productSerialCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->where(array('productSerial.productSerialSold' => 0));
        $select->where(array('productSerial.productSerialReturned' => 0));
        $select->where(array('locationID' => $locationID));
        $select->where(array('productState' => 1));

        $select->limit($limit);
        $query = $sql->prepareStatementForSqlObject($select);

        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * Get Loaction wise Inventory Product List
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param array $locationIDs
     * @param String $searchKey
     * @return array
     */
    public function searchLocationWiseInventoryProductsForDropdown($locationIDs = NULL, $searchKey = NULL, $isSearch = FALSE, $withNonInventory = FALSE, $categoryIds = NULL,$fixedAssetFlag = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('productCode', 'productName', 'productID'))
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('locationProductID'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('entityID'), 'left')
                ->join(array('lcpentity' => 'entity'), 'locationProduct.entityID = lcpentity.entityID', array('entityID'), 'left');
        if($withNonInventory){
            $select->where(array('entity.deleted' => '0', 'lcpentity.deleted' => '0', 'product.productState' => 1));
        } else {

            $select->where(array('product.productState' => 1 , 'entity.deleted' => '0', 'lcpentity.deleted' => '0', 'product.productTypeID' => 1));
        }
        if ($searchKey) {
            $select->order(array('product.productName' => 'ASC'));
        } else {
            $select->order(array('product.productID'));
        }
        $select->where->in('locationProduct.locationID', $locationIDs);
        if ($searchKey != NULL) {
            $select->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $searchKey . '%'), new Operator('product.productCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
            $select->limit(50);
        }
        if (!is_null($categoryIds)) {
            $select->where->in('product.categoryId', $categoryIds);
        }
        if ($fixedAssetFlag) {
            $select->join('productHandeling', 'product.productID = productHandeling.productID', array('productHandelingGiftCard','productHandelingFixedAssets'), 'left');
            $select->where(array('productHandeling.productHandelingFixedAssets' => 1, 'product.serialProduct' => 1));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    //get product details by location productID
    public function getProductsByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns(array('*'))
                ->join('product', 'locationProduct.productID = product.productID', array('*'), 'left')
                ->where(array('locationProduct.locationProductID' => $locationProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    /**
     * Get products which has been reached minimum Inventory level
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $products
     * @param type $locations
     * @return type
     */
    public function getInventoryLevelProducts($products, $locations, $isReachedMinQty = false, $isReachedReorderQty = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('locationProduct');
        $select->columns(['*']);
        $select->join('product', 'locationProduct.productID = product.productID', ['productName', 'productTypeID'], 'left');
        $select->join('entity', 'locationProduct.entityID = entity.entityID', ['deleted'], 'left');
        $select->where(['deleted' => '0', 'productTypeID' => '1']);
        if ($isReachedMinQty) {
            $select->where->nest()->lessThanOrEqualTo('locationProduct.locationProductQuantity', new Expression('locationProduct.locationProductMinInventoryLevel'))
            ->OR->isNull('locationProduct.locationProductQuantity');
        }
        if ($isReachedReorderQty) {
            $select->where->nest()->lessThanOrEqualTo('locationProduct.locationProductQuantity', new Expression('locationProduct.locationProductReOrderLevel'))
            ->OR->isNull('locationProduct.locationProductQuantity');
        }
        $select->where->in('locationProduct.productID', $products);
        $select->where->in('locationProduct.locationID', $locations);
        $statement = $sql->prepareStatementForSqlObject($select);

        $result = $statement->execute();
        if (count($result) > 0) {
            return iterator_to_array($result);
        } else {
            return[];
        }
    }

    //get active products from loctions
    public function getActiveProductsFromLocation($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('*'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->join(array('lcpentity' => 'entity'), 'locationProduct.entityID = lcpentity.entityID', array('*'), 'left')
                ->where(array('locationProduct.locationID' => $locationID, 'entity.deleted' => '0', 'lcpentity.deleted' => '0', 'product.productState' => 1))
                ->order(array('product.productName' => 'ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getLocationProducts($locationProductID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('*'), 'left')
                ->join('category','product.categoryID = category.categoryID',array('categoryName'))
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->join(array('lcpentity' => 'entity'), 'locationProduct.entityID = lcpentity.entityID', array('*'), 'left')
                ->where(array('locationProduct.locationID' => $locationID,
                    'entity.deleted' => '0',
                    'lcpentity.deleted' => '0',
                    'product.productState' => 1,
                    'locationProductID' => $locationProductID)
                )
                ->order(array('product.productName' => 'ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    /**
     * Get location product by product id
     * @param array $productId
     * @param int $locationId
     * @return mixed
     */
    public function getLocationProductsByproductId($productId, $locationId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('*'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->join(array('lcpentity' => 'entity'), 'locationProduct.entityID = lcpentity.entityID', array('*'), 'left')
                ->where(array(
                    'locationProduct.locationID' => $locationId,
                    'entity.deleted' => '0',
                    'lcpentity.deleted' => '0',
                    'product.productState' => '1'
                ))
                ->where->equalTo('product.productID', $productId);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute()->current();
    }

    /**
     * Update location product by array
     * @param array $data
     * @param string $locationProductId
     * @return boolean
     */
    public function updateLocationProductByArray($data, $locationProductId)
    {
        if ($locationProductId) {
            $this->tableGateway->update($data, array('locationProductID' => $locationProductId));
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Get location product by product code and location id
     * @param string $productCode
     * @param string $locationId
     * @return mixed
     */
    public function getLocationProductByProductCodeAndLocationId($productCode, $locationId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('*'), 'left')
                ->join('entity', 'locationProduct.entityID = entity.entityID', array('*'), 'left')
                ->where(array('product.productCode' => $productCode, 'locationProduct.locationID' => $locationId, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function getLocationProductMinInvntryLevelByLocationIDAndProductID($locationId = null, $productID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns(array('minTotal' => new Expression('SUM(locationProductMinInventoryLevel)'),
                        'productID',
                        'avlbeQty' => new Expression('SUM(locationProductQuantity)')))
                ->join('product','locationProduct.productID = product.productID',array('productCode','productName', 'categoryID'))
                ->join('category','product.categoryID = category.categoryID',array('categoryName'))
                ->group(array('locationProduct.productID'));
        $select->join('entity', 'product.entityID = entity.entityID', array('deleted'));
        $select->where(array('entity.deleted' => 0));
        $select->where('product.productState','=',1);
        if($locationId){
            $select->where->in('locationProduct.locationID',$locationId);
        }
        if($productID){
            $select->where->in('locationProduct.productID', $productID);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get location product details
     * @param array $productIds
     * @param array $locationIds
     * @return mixed
     */
    public function getLocationProductsDetails($productIds = [], $locationIds = [], $categoryIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns([
                    'locationProductID',
                    'productID',
                    'locationID',
                    'locationProductQuantity',
                    'defaultPurchasePrice'
                ])
                ->join('location', 'locationProduct.locationID = location.locationID', [
                    'locationName'
                ], 'left')
                ->join('product', 'locationProduct.productID = product.productID', [
                    'productCode',
                    'productName',
                    'entityID'
                ], 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity','product.entityID = entity.entityID',[],'left');
        $select->where->notEqualTo('entity.deleted',1);
        if ($productIds) {
            $select->where->in('locationProduct.productID', $productIds);
        }
        if ($locationIds) {
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get location product sales details
     * @param string $date
     * @param array $productIds
     * @param array $locationIds
     * @return mixed
     */
    public function getLocationProductSalesDetails($date, $productIds = [], $locationIds = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns([
                    'locationProductID',
                    'productID',
                    'locationID',
                    'locationProductQuantity'
                ])
                ->join('itemOut', 'locationProduct.locationProductID = itemOut.itemOutLocationProductID', [
                    'itemOutDocumentType',
                    'itemOutDocumentID'
                ], 'left')
                ->join('salesInvoice', 'itemOut.itemOutDocumentID = salesInvoice.salesInvoiceID', [
                    'salesInvoiceIssuedDate',
                    'recent1' => new Expression("case when salesInvoiceIssuedDate <= '" . $date . "' AND salesInvoiceIssuedDate > DATE_SUB('" . $date . "',INTERVAL 1 MONTH) then (itemOutQty-itemOutReturnQty) end"),
                    'recent2' => new Expression("case when salesInvoiceIssuedDate <= DATE_SUB('" . $date . "',INTERVAL 1 MONTH) AND salesInvoiceIssuedDate > DATE_SUB('" . $date . "',INTERVAL 2 MONTH) then (itemOutQty-itemOutReturnQty) end"),
                    'recent3' => new Expression("case when salesInvoiceIssuedDate <= DATE_SUB('" . $date . "',INTERVAL 2 MONTH) AND salesInvoiceIssuedDate > DATE_SUB('" . $date . "',INTERVAL 3 MONTH) then (itemOutQty-itemOutReturnQty) end"),
                ], 'left');
        if ($productIds) {
            $select->where->in('locationProduct.productID', $productIds);
        }
        if ($locationIds) {
            $select->where->in('locationProduct.locationID', $locationIds);
        }
        $select->where->equalTo('itemOut.itemOutDocumentType', 'Sales Invoice');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getLocationProductDetailsWithProductDetailsByLocIDAndProductID($productID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns(array('*'))
                ->join('location', 'locationProduct.locationID = location.locationID', array('*'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('*'), 'left');
        $select->where(array('locationProduct.productID' => $productID, 'locationProduct.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;

    }

    public function getProductQtyInListByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('qtyIn' => new Expression('SUM(itemInQty)'), 'costIn' => new Expression('SUM(itemInQty*itemInPrice)'), 'itemInDocumentType'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$fromDataWise) {
            $select->where->notEqualTo('itemIn.itemInDocumentType', 'Inventory Transfer');
        }
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }
        $select->where->between('itemIn.itemInDateAndTime', $fromDate, $toDate);
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyInListForSalesReturnWithDocumentDateByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false, $isAvoidGrouping= false, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        if ($isAvoidGrouping) {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('*'), 'left')
                ->join('salesReturn', 'itemIn.itemInDocumentID = salesReturn.salesReturnID', array('documentDate' => new Expression('salesReturn.salesReturnDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        } else {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('qtyIn' => new Expression('SUM(itemInQty)'), 'costIn' => new Expression('SUM(itemInQty*itemInPrice)'),'itemInDocumentType',  'itemInAverageCostingPrice'), 'left')
                ->join('salesReturn', 'itemIn.itemInDocumentID = salesReturn.salesReturnID', array('documentDate' => new Expression('salesReturn.salesReturnDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        }

        
        if (!$fromDataWise) {
            $select->where->notEqualTo('itemIn.itemInDocumentType', 'Inventory Transfer');
        }
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemIn.itemInDateAndTime', $fromDate);
            $select->where->lessThan('itemIn.itemInDateAndTime',$toDate);
        } else {
            $select->where->greaterThan('salesReturn.salesReturnDate', $fromDate);
            $select->where->lessThan('salesReturn.salesReturnDate',$toDate);
        }
        $select->where->in('itemIn.itemInDocumentType',['Sales Returns','Direct Return']);

        if (!$isAvoidGrouping) {
            $select->group(array('locationProduct.productID'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

     public function getProductQtyInListForCreditNoteCancelWithDocumentDateByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false, $isAvoidGrouping = false, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)'), 'itemOutDocumentType'), 'left')
                ->join('creditNote', 'itemOut.itemOutDocumentID = creditNote.creditNoteID',array('documentDate' => new Expression('creditNoteDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->notEqualTo('creditNote.statusID', 10)
        ->where->notEqualTo('creditNote.statusID', 8)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemOut.itemOutDateAndTime', $fromDate);
            $select->where->lessThan('itemOut.itemOutDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('creditNote.creditNoteDate', $fromDate);
            $select->where->lessThan('creditNote.creditNoteDate', $toDate);
        }
        
        $select->where->equalTo('itemOutDocumentType','CreditNote cancel');
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;

    }

    public function getProductQtyInListForCreditNoteWithDocumentDateByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false, $isAvoidGrouping = false, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        if ($isAvoidGrouping) {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('*'), 'left')
                ->join('creditNote', 'itemIn.itemInDocumentID = creditNote.creditNoteID',array('documentDate' => new Expression('creditNoteDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        } else {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('qtyIn' => new Expression('SUM(itemInQty)'), 'costIn' => new Expression('SUM(itemInQty*itemInPrice)'),'itemInDocumentType', 'itemInAverageCostingPrice'), 'left')
                ->join('creditNote', 'itemIn.itemInDocumentID = creditNote.creditNoteID',array('documentDate' => new Expression('creditNoteDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        }

        
        if (!$fromDataWise) {
            $select->where->notEqualTo('itemIn.itemInDocumentType', 'Inventory Transfer');
        }
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemIn.itemInDateAndTime', $fromDate);
            $select->where->lessThan('itemIn.itemInDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('creditNote.creditNoteDate', $fromDate);
            $select->where->lessThan('creditNote.creditNoteDate', $toDate);
        }   
        
        $select->where->equalTo('itemInDocumentType','Credit Note');
        if (!$isAvoidGrouping) {
            $select->group(array('locationProduct.productID'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyInListForGrnWithDocumentDateByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false, $isAvoidGrouping = false, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        if ($isAvoidGrouping) {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('*'), 'left')
                ->join('grn', 'itemIn.itemInDocumentID = grn.grnID', array('documentDate' => new Expression('grnDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        } else {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('qtyIn' => new Expression('SUM(itemInQty)'), 'costIn' => new Expression('SUM(itemInQty*itemInPrice)'),'itemInDocumentType', 'itemInAverageCostingPrice'), 'left')
                ->join('grn', 'itemIn.itemInDocumentID = grn.grnID', array('documentDate' => new Expression('grnDate'), 'status'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2);
            if (!$isActualDate) {
                $select->where->notEqualTo('grn.status', 10);
            }
            $select->where->in('locationProduct.locationID', $locIds);
        }

        if (!$fromDataWise) {
            $select->where->notEqualTo('itemIn.itemInDocumentType', 'Inventory Transfer');
        }
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemIn.itemInDateAndTime', $fromDate);
            $select->where->lessThan('itemIn.itemInDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('grn.grnDate', $fromDate);
            $select->where->lessThan('grn.grnDate', $toDate);
        }
        
        $select->where->equalTo('itemInDocumentType','Goods Received Note');
        if (!$isAvoidGrouping) {
            $select->group(array('locationProduct.productID'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }


    public function getProductQtyInListForPostiveAdjustmentWithDocumentDateByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false, $isAvoidGrouping = false, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        if ($isAvoidGrouping) {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('*'), 'left')
                ->join('goodsIssue', 'itemIn.itemInDocumentID = goodsIssue.goodsIssueID', array('documentDate' => new Expression('goodsIssueDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->notEqualTo('goodsIssue.goodsIssueStatus', 5)
            ->where->in('locationProduct.locationID', $locIds);
        } else {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('qtyIn' => new Expression('SUM(itemInQty)'), 'costIn' => new Expression('SUM(itemInQty*itemInPrice)'),'itemInDocumentType', 'itemInAverageCostingPrice'), 'left')
                ->join('goodsIssue', 'itemIn.itemInDocumentID = goodsIssue.goodsIssueID', array('documentDate' => new Expression('goodsIssueDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        }

        
        if (!$fromDataWise) {
            $select->where->notEqualTo('itemIn.itemInDocumentType', 'Inventory Transfer');
        }
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemIn.itemInDateAndTime', $fromDate);
            $select->where->lessThan('itemIn.itemInDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('goodsIssue.goodsIssueDate', $fromDate);
            $select->where->lessThan('goodsIssue.goodsIssueDate', $toDate);
        }
        
        $select->where->in('itemInDocumentType',['Inventory Positive Adjustment','Delete Negative Adjustment']);
        if (!$isAvoidGrouping) {
            $select->group(array('locationProduct.productID'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }


    public function getProductQtyInListForPurchaseInvoiceWithDocumentDateByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false, $isAvoidGrouping = false, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        if ($isAvoidGrouping) {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('*'), 'left')
                ->join('purchaseInvoice', 'itemIn.itemInDocumentID = purchaseInvoice.purchaseInvoiceID',  array('documentDate' => new Expression('purchaseInvoiceIssueDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->notEqualTo('purchaseInvoice.status', 10)
            ->where->notEqualTo('purchaseInvoice.status', 5)
            ->where->in('locationProduct.locationID', $locIds);
        } else {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('qtyIn' => new Expression('SUM(itemInQty)'), 'costIn' => new Expression('SUM(itemInQty*itemInPrice)'),'itemInDocumentType', 'itemInAverageCostingPrice'), 'left')
                ->join('purchaseInvoice', 'itemIn.itemInDocumentID = purchaseInvoice.purchaseInvoiceID',  array('documentDate' => new Expression('purchaseInvoiceIssueDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->notEqualTo('purchaseInvoice.status', 10)
            ->where->in('locationProduct.locationID', $locIds);
        }
        
        if (!$fromDataWise) {
            $select->where->notEqualTo('itemIn.itemInDocumentType', 'Inventory Transfer');
        }
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }
        if ($isActualDate) {
            $select->where->greaterThan('itemIn.itemInDateAndTime', $fromDate);
            $select->where->lessThan('itemIn.itemInDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('purchaseInvoice.purchaseInvoiceIssueDate', $fromDate);
            $select->where->lessThan('purchaseInvoice.purchaseInvoiceIssueDate', $toDate);
        }
        
        $select->where->equalTo('itemInDocumentType','Payment Voucher');
        if (!$isAvoidGrouping) {
            $select->group(array('locationProduct.productID'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyInListForExpensePurchaseVoucherWithDocumentDateByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false, $isAvoidGrouping = false, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        if ($isAvoidGrouping) {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('*'), 'left')
                ->join('paymentVoucher', 'itemIn.itemInDocumentID = paymentVoucher.paymentVoucherID', array('documentDate' => new Expression('paymentVoucherIssuedDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        } else {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('qtyIn' => new Expression('SUM(itemInQty)'), 'costIn' => new Expression('SUM(itemInQty*itemInPrice)'),'itemInDocumentType', 'itemInAverageCostingPrice'), 'left')
                ->join('paymentVoucher', 'itemIn.itemInDocumentID = paymentVoucher.paymentVoucherID', array('documentDate' => new Expression('paymentVoucherIssuedDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        }
        
        if (!$fromDataWise) {
            $select->where->notEqualTo('itemIn.itemInDocumentType', 'Inventory Transfer');
        }
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemIn.itemInDateAndTime', $fromDate);
            $select->where->lessThan('itemIn.itemInDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('paymentVoucher.paymentVoucherIssuedDate', $fromDate);
            $select->where->lessThan('paymentVoucher.paymentVoucherIssuedDate', $toDate);
        }
        
        $select->where->equalTo('itemInDocumentType','Expense Payment Voucher');
        if (!$isAvoidGrouping) {
            $select->group(array('locationProduct.productID'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyInListForSalesInvoiceWithDocumentDateByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds, $fromDataWise = false, $isAvoidGrouping = false , $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        if ($isAvoidGrouping) {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('*'), 'left')
                ->join('salesInvoice','itemIn.itemInDocumentID = salesInvoice.salesInvoiceID', array('documentDate' => new Expression('salesInvoice.salesInvoiceIssuedDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        } else {
            $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('qtyIn' => new Expression('SUM(itemInQty)'), 'costIn' => new Expression('SUM(itemInQty*itemInPrice)'),'itemInDocumentType', 'itemInAverageCostingPrice'), 'left')
                ->join('salesInvoice','itemIn.itemInDocumentID = salesInvoice.salesInvoiceID', array('documentDate' => new Expression('salesInvoice.salesInvoiceIssuedDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
            ->where->equalTo('entity.deleted', '0')
            ->where->notEqualTo('product.productTypeID', 2)
            ->where->in('locationProduct.locationID', $locIds);
        }
        
        if (!$fromDataWise) {
            $select->where->notEqualTo('itemIn.itemInDocumentType', 'Inventory Transfer');
        }
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }
        if ($isActualDate) {
            $select->where->greaterThan('itemIn.itemInDateAndTime', $fromDate);
            $select->where->lessThan('itemIn.itemInDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('salesInvoice.salesInvoiceIssuedDate', $fromDate);
            $select->where->lessThan('salesInvoice.salesInvoiceIssuedDate', $toDate);
        }   
        
        $select->where->in('itemInDocumentType', ['POS Invoice Cancel','Invoice Cancel']);
        if (!$isAvoidGrouping) {
            $select->group(array('locationProduct.productID'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }


    public function getProductQtyOutListByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false,$categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        $select->where->between('itemOut.itemOutDateAndTime', $fromDate,$toDate);
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyOutListOfSaleInvoiceDetailsByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false,$categoryIds, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)'), 'itemOutDocumentType'), 'left')
                ->join('salesInvoice','itemOut.itemOutDocumentID = salesInvoice.salesInvoiceID', array('salesInvoiceCode', 'salesInvoiceStatus' => new Expression('salesInvoice.statusID'),'documentOutDate' => new Expression('salesInvoiceIssuedDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->notEqualTo('salesInvoice.statusID', 10)
        ->where->notEqualTo('salesInvoice.statusID', 8)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemOut.itemOutDateAndTime', $fromDate);
            $select->where->lessThan('itemOut.itemOutDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('salesInvoice.salesInvoiceIssuedDate', $fromDate);
            $select->where->lessThan('salesInvoice.salesInvoiceIssuedDate', $toDate);
        }
        
        $select->where->equalTo('itemOutDocumentType','Sales Invoice');
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyOutListOfDeliveryNoteDetailsByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false,$categoryIds, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)'), 'itemOutDocumentType'), 'left')
                ->join('deliveryNote','itemOut.itemOutDocumentID = deliveryNote.deliveryNoteID', array('deliveryNoteCode', 'deliveryNoteStatus','documentOutDate' => new Expression('deliveryNoteDeliveryDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->notEqualTo('deliveryNote.deliveryNoteStatus', 10)
        ->where->notEqualTo('deliveryNote.deliveryNoteStatus', 15)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemOut.itemOutDateAndTime', $fromDate);
            $select->where->lessThan('itemOut.itemOutDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('deliveryNote.deliveryNoteDeliveryDate', $fromDate);
            $select->where->lessThan('deliveryNote.deliveryNoteDeliveryDate', $toDate);
        }
        
        $select->where->equalTo('itemOutDocumentType','Delivery Note');
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyOutListOfGRNDetailsByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false,$categoryIds, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)'), 'itemOutDocumentType'), 'left')
                ->join('grn','itemOut.itemOutDocumentID = grn.grnID', array('grnCode', 'status','documentOutDate' => new Expression('grnDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2);
        if (!$isActualDate) {
            $select->where->notEqualTo('grn.status', 10);
        }
        $select->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemOut.itemOutDateAndTime', $fromDate);
            $select->where->lessThan('itemOut.itemOutDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('grn.grnDate', $fromDate);
            $select->where->lessThan('grn.grnDate', $toDate);
        }
        
        $select->where->equalTo('itemOutDocumentType','Goods Received Note Cancel');
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyOutListOfPurchaseInvoiceCancelDetailsByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false,$categoryIds, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)'), 'itemOutDocumentType'), 'left')
                ->join('purchaseInvoice','itemOut.itemOutDocumentID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode', 'status','documentOutDate' => new Expression('purchaseInvoiceIssueDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->notEqualTo('purchaseInvoice.status', 10)
        ->where->notEqualTo('purchaseInvoice.status', 8)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemOut.itemOutDateAndTime', $fromDate);
            $select->where->lessThan('itemOut.itemOutDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('purchaseInvoice.purchaseInvoiceIssueDate', $fromDate);
            $select->where->lessThan('purchaseInvoice.purchaseInvoiceIssueDate', $toDate);
        }
        
        $select->where->equalTo('itemOutDocumentType','purchase Invoice cancel');
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyOutListOfEXPVCancelDetailsByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false,$categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)'), 'itemOutDocumentType'), 'left')
                ->join('paymentVoucher','itemOut.itemOutDocumentID = paymentVoucher.paymentVoucherID', array('paymentVoucherCode', 'paymentVoucherStatus','documentOutDate' => new Expression('paymentVoucherIssuedDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->notEqualTo('paymentVoucher.paymentVoucherStatus', 10)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        $select->where->greaterThan('paymentVoucher.paymentVoucherIssuedDate', $fromDate);
        $select->where->lessThan('paymentVoucher.paymentVoucherIssuedDate', $toDate);
        $select->where->equalTo('itemOutDocumentType','Payment Voucher Cancel');
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyOutListOfGoodIssueDetailsByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false,$categoryIds, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)'), 'itemOutDocumentType'), 'left')
                ->join('goodsIssue','itemOut.itemOutDocumentID = goodsIssue.goodsIssueID', array('goodsIssueCode', 'goodsIssueStatus','documentOutDate' => new Expression('goodsIssueDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->notEqualTo('goodsIssue.goodsIssueStatus', 10)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemOut.itemOutDateAndTime', $fromDate);
            $select->where->lessThan('itemOut.itemOutDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('goodsIssue.goodsIssueDate', $fromDate);
            $select->where->lessThan('goodsIssue.goodsIssueDate', $toDate);
        }

        
        $select->where->in('itemOutDocumentType',['Delete Positive Adjustment','Inventory Negative Adjustment']);
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyOutListOfPurchaseReturnDetailsByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false,$categoryIds, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)'), 'itemOutDocumentType'), 'left')
                ->join('purchaseReturn','itemOut.itemOutDocumentID = purchaseReturn.purchaseReturnID', array('purchaseReturnCode', 'purchaseReturnStatus','documentOutDate' => new Expression('purchaseReturnDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->notEqualTo('purchaseReturn.purchaseReturnStatus', 10)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemOut.itemOutDateAndTime', $fromDate);
            $select->where->lessThan('itemOut.itemOutDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('purchaseReturn.purchaseReturnDate', $fromDate);
            $select->where->lessThan('purchaseReturn.purchaseReturnDate', $toDate);
        }
        
        $select->where->in('itemOutDocumentType',['Purchase Return','Direct Purchase Return']);
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductQtyOutListOfDebitNoteDetailsByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false,$categoryIds, $isActualDate = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('qtyOut' => new Expression('SUM(itemOutQty)'), 'qtyOutCost' => new Expression('SUM(itemOutQty*itemOutAverageCostingPrice)'), 'itemOutDocumentType'), 'left')
                ->join('debitNote','itemOut.itemOutDocumentID = debitNote.debitNoteID', array('debitNoteCode', 'debitNoteStatus' => new Expression('debitNote.statusID'),'documentOutDate' => new Expression('debitNoteDate')), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->notEqualTo('debitNote.statusID', 10)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }

        if ($isActualDate) {
            $select->where->greaterThan('itemOut.itemOutDateAndTime', $fromDate);
            $select->where->lessThan('itemOut.itemOutDateAndTime', $toDate);
        } else {
            $select->where->greaterThan('debitNote.debitNoteDate', $fromDate);
            $select->where->lessThan('debitNote.debitNoteDate', $toDate);
        }
        
        $select->where->in('itemOutDocumentType',['Debit Note','Direct Debit Note']);
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductsByLocIDAndProID($proIds, $locIds, $isAllProducts = false, $activeFlag = false ,$categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array('locationProductID'
                    ))
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locCD' => new Expression('location.locationCode'),
                    'locID' => new Expression('location.locationID')), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->in('locationProduct.locationID', $locIds);
        if (!$isAllProducts) {
            $select->where->in('locationProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);
        }
        $select->group(array('locationProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function updateAvgCostingByLocationProductID($data, $id)
    {
        try {
            $this->tableGateway->update($data, array('locationProductID' => $id));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }


    /**
    * get all product Quatities with their batch details for given location ids and product ids
    * @param array $locationIDs
    * @param array $productIDs
    * @param boolean $allProductFlag
    * return array
    **/
    public function getBatchDetailsByLocationIDsAndProductIDs($locationIDs = [], $prodcutIDs = [], $allProductFlag, $categoryIds)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('locationProduct')
                    ->columns(array('*'))
                    ->join('productBatch', 'locationProduct.locationProductID = productBatch.locationProductID',array('productBatchID','productBatchCode','productBatchQuantity'), 'left')
                    ->join('product', 'locationProduct.productID = product.productID', array('productCode','productName','productDefaultPurchasePrice','productDefaultSellingPrice'), 'left')
                    ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                    ->join('itemIn', 'productBatch.productBatchID = itemIn.itemInBatchID', array('itemInPrice', 'itemInDiscount'), 'left')
                    ->join('category', 'product.categoryID = category.categoryID', array('categoryName' => new Expression('categoryName')),'left')
                    ->where(array('product.productState' => 1, 'product.batchProduct' => 1,'entity.deleted' => 0));
            $select->where->greaterThan('productBatch.productBatchQuantity', 0);
            $select->where->in('locationProduct.locationID', $locationIDs);

            if(!$allProductFlag){
                $select->where->in('product.productID', $prodcutIDs);
            }
            if($categoryIds != null){
                $select->where->in('product.categoryID', $categoryIds);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() == 0) {
                return 0;
            } else {
                foreach ($results as $key => $value) {
                    $countArray[$value['productBatchID']] = $value;
                }
                return $countArray;
            }
        } catch (Zend_Exception $e) {
            echo "Message: " . $e->getMessage() . "\n";
        }

    }

    /**
    * get all product Quantities with their Serial details for given location ids and product ids
    * @param array $locationIDs
    * @param array $productIDs
    * @param boolean $allProductFlag
    * return array
    **/
    public function getSerialDetailsByLocationIDsAndProductIDs($locationIDs = [], $prodcutIDs = [], $allProductFlag, $categoryIds)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('locationProduct')
                    ->columns(array('*'))
                    ->join('productSerial', 'locationProduct.locationProductID = productSerial.locationProductID',array('productSerialID','productSerialCode'), 'left')
                    ->join('product', 'locationProduct.productID = product.productID', array('productCode','productName','productDefaultPurchasePrice','productDefaultSellingPrice'), 'left')
                    ->join('entity', 'product.entityID = entity.entityID', array('deleted'), 'left')
                    ->join('itemIn', 'productSerial.productSerialID = itemIn.itemInSerialID', array('itemInPrice', 'itemInDiscount'), 'left')
                    ->join('category', 'product.categoryID = category.categoryID', array('categoryName' => new Expression('categoryName')),'left')
                    ->where(array('product.productState' => 1, 'product.serialProduct' => 1,'entity.deleted' => 0,'productSerial.productSerialSold' => 0, 'productSerial.productSerialReturned' => 0));
            $select->where->in('locationProduct.locationID', $locationIDs);

            if(!$allProductFlag){
                $select->where->in('product.productID', $prodcutIDs);
            }

            if($categoryIds != null){
                $select->where->in('product.categoryID', $categoryIds);
            }

            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() == 0) {
                return 0;
            } else {
                foreach ($results as $val) {
                    $countArray[] = $val;
                }

                return $countArray;
            }
        } catch (Zend_Exception $e) {
            echo "Message: " . $e->getMessage() . "\n";
        }

    }

    public function getLocationProductDetailsWithProductDetails($productID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('locationProduct')
                ->columns(array('*'))
                ->join('product', 'locationProduct.productID = product.productID', array('*'), 'left')
                ->join('entity', 'locationProduct.entityID = entity.entityID', array('*'), 'left')
                ->where(array('locationProduct.productID' => $productID, 'locationID' => $locationID, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function getLocationProductAndUomByProductCodeAndLocationId($productCode, $locationId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('*'), 'left')
                ->join('entity', 'locationProduct.entityID = entity.entityID', array('*'), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('*'), 'left')
                ->where(array('product.productCode' => $productCode, 'locationProduct.locationID' => $locationId, 'entity.deleted' => '0', 'productUomDisplay' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function getProductsFromLocationForInventoryDashbaord($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('*'))
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('*'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('*'), 'left')
                ->join(array('lcpentity' => 'entity'), 'locationProduct.entityID = lcpentity.entityID', array('*'), 'left')
                ->where(array('locationProduct.locationID' => $locationID, 'entity.deleted' => '0', 'lcpentity.deleted' => '0','product.productTypeID' => 1 ,'product.productState' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }



    public function searchLocationWiseInventoryProductsForDashboard($locationIDs = NULL, $searchKey = NULL, $isSearch = FALSE, $withNonInventory = FALSE, $categoryIds = NULL,$fixedAssetFlag = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('productID'))
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('locationProductID'), 'left')
                ->join('entity', 'product.entityID = entity.entityID', array('entityID'), 'left')
                ->join(array('lcpentity' => 'entity'), 'locationProduct.entityID = lcpentity.entityID', array('entityID'), 'left');
        // if($withNonInventory){
        //     $select->where(array('entity.deleted' => '0', 'lcpentity.deleted' => '0', 'product.productState' => 1));
        // } else {

            $select->where(array('product.productState' => 1 , 'entity.deleted' => '0', 'lcpentity.deleted' => '0', 'product.productTypeID' => 1));
        // }
        // if ($searchKey) {
        //     $select->order(array('product.productName' => 'ASC'));
        // } else {
            $select->order(array('product.productID'));
        // }
        $select->where->in('locationProduct.locationID', $locationIDs);
        // if ($searchKey != NULL) {
        //     $select->where(new PredicateSet(array(new Operator('product.productName', 'like', '%' . $searchKey . '%'), new Operator('product.productCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        //     $select->limit(50);
        // }
        // if (!is_null($categoryIds)) {
        //     $select->where->in('product.categoryId', $categoryIds);
        // }
        // if ($fixedAssetFlag) {
        //     $select->join('productHandeling', 'product.productID = productHandeling.productID', array('productHandelingGiftCard','productHandelingFixedAssets'), 'left');
        //     $select->where(array('productHandeling.productHandelingFixedAssets' => 1, 'product.serialProduct' => 1));
        // }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }



    public function updateLocationProductMrpDetails($dataSet, $locationIds, $productID)
    {
        try {
            $this->tableGateway->update($dataSet, array('locationID' => $locationIds, 'productID' => $productID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}
