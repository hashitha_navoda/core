<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Product Category Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class ProductCategoryTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select('category');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ProductCategory());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $adpater = $this->tableGateway->getAdapter();
        $productcategory = new TableGateway('category', $adpater);
        $rowset = $productcategory->select(function (Select $select) {
                    $select->order('categoryName ASC');
                    $select->where(array('categoryState' => 1));
                });
        return $rowset;
    }

    public function getProductCategory($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('categoryID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getProductCategoryByName($categoryName)
    {
        $rowset = $this->tableGateway->select(array('categoryName' => $categoryName));
        $row = $rowset->current();
        if (!$row) {
            return FALSE;
        }
        return $row;
    }

    public function saveProductCategory(productCategory $productCategory)
    {
        $data = array(
            'categoryID' => $productCategory->categoryID,
            'categoryName' => $productCategory->categoryName,
            'categoryParentID' => $productCategory->categoryParentID,
            'categoryLevel' => $productCategory->categoryLevel,
            'categoryPrefix' => $productCategory->categoryPrefix,
            'categoryState' => $productCategory->categoryState,
        );
        $result = $this->tableGateway->insert($data);
        return $result;
    }

    public function updateProductCategory(productCategory $productCategory)
    {
        $data = array(
            'categoryName' => $productCategory->categoryName,
            'prefix' => $productCategory->prefix,
            'state' => $productCategory->state,
        );

        $this->tableGateway->update($data, array('productCategoriesID' => $productCategory->productCategoriesID));
    }

    public function updateProductCategoryState(productCategory $productCategory)
    {
        $data = array(
            'state' => $productCategory->state,
        );

        $this->tableGateway->update($data, array('productCategoriesID' => $productCategory->productCategoriesID));
    }

    public function deleteProductCategory($id)
    {
        try {
            $this->tableGateway->delete(array('productCategoriesID' => $id));
            return TRUE;
        } catch (\Exception $exc) {
            return FALSE;
        }
    }

    public function getCategoryforSearch($category)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('categoryName LIKE ?' => '%' . $category . '%'));
        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }

}
