<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PurchaseRequisitionTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param boolean  $checkAllProductCopied check all product of po is copied
     * @param Paginator true or false
     * @return paginator object if true, else resultset
     */
    public function getpurchaseRequisitions($paginated = FALSE, $userActiveLocationID = NULL, $status = NULL, $checkAllProductCopied = False)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->order('purchaseRequisition.purchaseRequisitionID DESC');
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseRequisition.purchaseRequisitionRetrieveLocation = location.locationID', array('locationName', 'locationCode', 'locationID'));
        $select->join('supplier', 'purchaseRequisition.purchaseRequisitionSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'), 'left');
        $select->where(array('deleted' => '0'));
        if ($userActiveLocationID != NULL) {
            $select->where(array('locationID' => $userActiveLocationID));
        }
        if ($status != NULL) {
            $select->where(array('status' => $status));
        } else {
            $select->where->notEqualTo('status' ,10);

        }
        if ($checkAllProductCopied) {
            $select->join('purchaseRequisitionProduct', 'purchaseRequisition.purchaseRequisitionID = purchaseRequisitionProduct.purchaseRequisitionID', '*');
            $select->where(array('purchaseRequisitionProduct.copied' => 0));
        }
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * @param String $poSearchKey
     * @return ResultSet
     */
    public function getPosforSearch($poSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $poSearchKey . '\' in supplier.supplierName )>0,POSITION(\'' . $poSearchKey . '\' in supplier.supplierName), 9999),'
                    . 'IF(POSITION(\'' . $poSearchKey . '\' in purchaseRequisition.purchaseRequisitionCode )>0,POSITION(\'' . $poSearchKey . '\' in purchaseRequisition.purchaseRequisitionCode), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(supplier.supplierName ), CHAR_LENGTH(purchaseRequisition.purchaseRequisitionCode )) '),
            '*',
        ));
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseRequisition.purchaseRequisitionRetrieveLocation = location.locationID', array('locationName'));
        $select->join('supplier', 'purchaseRequisition.purchaseRequisitionSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('purchaseRequisition.purchaseRequisitionCode', 'like', '%' . $poSearchKey . '%'), new Operator('supplier.supplierName', 'like', '%' . $poSearchKey . '%')), PredicateSet::OP_OR));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPoByPoCode($poCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->join('purchaseRequisitionProduct', 'purchaseRequisition.purchaseRequisitionID = purchaseRequisitionProduct.purchaseRequisitionID', array('*'));
        $select->join('locationProduct', 'purchaseRequisitionProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'));
        $select->join('supplier', 'purchaseRequisition.purchaseRequisitionSupplierID = supplier.supplierID', array('supplierCode', 'supplierName'), 'left');
        $select->join('location', 'purchaseRequisition.purchaseRequisitionRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('purchaseRequisitionProductTax', 'purchaseRequisitionProduct.purchaseRequisitionProductID = purchaseRequisitionProductTax.purchaseRequisitionProductID', array('purchaseRequisitionTaxID', 'purchaseRequisitionTaxPrecentage', 'purchaseRequisitionTaxAmount'), 'left');
        $select->join('tax', 'purchaseRequisitionProductTax.purchaseRequisitionTaxID = tax.id', array('taxName'), 'left');
        $select->where(array('purchaseRequisitionCode' => $poCode));
        $select->where(array('productUomBase' => '1','purchaseRequisition.status' => '3'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPReqByPoID($purchaseRequisitionID, $toGrn = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', ['createdBy','createdTimeStamp'], 'left');
        $select->join('purchaseRequisitionType', 'purchaseRequisition.purchaseRequisitionType = purchaseRequisitionType.purchaseRequisitionTypeId', array('purchaseRequisitionTypeName'), 'left');
        $select->join('user', 'user.userID = entity.createdBy', ['createdUser' => 'userUsername'], 'left');
        $select->join('purchaseRequisitionProduct', 'purchaseRequisition.purchaseRequisitionID = purchaseRequisitionProduct.purchaseRequisitionID', array('*'));
        $select->join('locationProduct', 'purchaseRequisitionProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('supplier', 'purchaseRequisition.purchaseRequisitionSupplierID = supplier.supplierID', array('supplierCode', 'supplierName', 'supplierEmail', 'supplierAddress', 'supplierTitle', 'supplierOutstandingBalance', 'supplierCreditBalance'), 'left');
        $select->join('location', 'purchaseRequisition.purchaseRequisitionRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('purchaseRequisitionProductTax', 'purchaseRequisitionProduct.purchaseRequisitionProductID = purchaseRequisitionProductTax.purchaseRequisitionProductID', array('purchaseRequisitionTaxID', 'purchaseRequisitionTaxPrecentage', 'purchaseRequisitionTaxAmount'), 'left');
        $select->join('tax', 'purchaseRequisitionProductTax.purchaseRequisitionTaxID = tax.id', array('taxName'), 'left');
        $select->where(array('purchaseRequisition.purchaseRequisitionID' => $purchaseRequisitionID));
        $select->where(array('productUomBase' => '1'));
        if ($toGrn) {
            $select->where(array('purchaseRequisitionProduct.copied' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function savePReq(purchaseRequisition $po)
    {
        $data = array(
            'purchaseRequisitionCode' => $po->purchaseRequisitionCode,
            'purchaseRequisitionSupplierID' => $po->purchaseRequisitionSupplierID,
            'purchaseRequisitionSupplierReference' => $po->purchaseRequisitionSupplierReference,
            'purchaseRequisitionRetrieveLocation' => $po->purchaseRequisitionRetrieveLocation,
            'purchaseRequisitionEstDelDate' => $po->purchaseRequisitionEstDelDate,
            'purchaseRequisitionDate' => $po->purchaseRequisitionDate,
            'purchaseRequisitionDescription' => $po->purchaseRequisitionDescription,
            'purchaseRequisitionDeliveryCharge' => $po->purchaseRequisitionDeliveryCharge,
            'purchaseRequisitionTotal' => $po->purchaseRequisitionTotal,
            'purchaseRequisitionShowTax' => $po->purchaseRequisitionShowTax,
            'status' => $po->status,
            'entityID' => $po->entityID,
            'purchaseRequisitionDraftFlag' => $po->purchaseRequisitionDraftFlag,
            'purchaseRequisitionType' => $po->purchaseRequisitionType,
            'purchaseRequisitionHashValue' => $po->purchaseRequisitionHashValue
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return type
     */
    public function fetchDescAll()
    {
        $adpater = $this->tableGateway->getAdapter();
        $grn = new TableGateway('purchaseRequisition', $adpater);
        $rowset = $grn->select(function (Select $select) {
            $select->order('purchaseRequisitionEstDelDate DESC');
        });
        return $rowset;
    }

    public function poStatusDetails($poIds, $isAllPO = false)
    {
        $isAllPO = filter_var($isAllPO, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition')
                ->columns(array(
                    'poID' => new Expression('purchaseRequisition.purchaseRequisitionID'),
                    'poCD' => new Expression('purchaseRequisition.purchaseRequisitionCode'),
                    'poTot' => new Expression('purchaseRequisition.purchaseRequisitionTotal'),
                    'poDate' => new Expression('purchaseRequisition.purchaseRequisitionEstDelDate')))
                ->join('supplier', 'supplier.supplierID=purchaseRequisition.purchaseRequisitionSupplierID', array(
                    'sCD' => new Expression('supplier.supplierCode'),
                    'sTitle' => new Expression('supplier.supplierTitle'),
                    'sName' => new Expression('supplier.supplierName')))
                ->join('status', 'status.statusID=purchaseRequisition.status', array('statusName'))
                ->order('purchaseRequisition.purchaseRequisitionEstDelDate DESC');
        $select->where(array('purchaseRequisition.purchaseRequisitionDraftFlag' => '0'));
        if (!$isAllPO) {
            $select->where->in('purchaseRequisition.purchaseRequisitionID', $poIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updatePReqStatus($purchaseRequisitionID, $statusID)
    {
        $data = array(
            'status' => $statusID
        );
        return $this->tableGateway->update($data, array('purchaseRequisitionID' => $purchaseRequisitionID));
    }

    public function getLocationdata()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('location')
                ->columns(array('locationID', 'locationName'))
                ->where('locationID IS NOT NULL ');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();


        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    public function getPaymentTypeData()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('paymentMethod')
                ->columns(array('paymentMethodID', 'paymentMethodName'))
                ->where('paymentMethodID IS NOT NULL ');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();


        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    public function getMonthlyPaymentData($fromDate, $toDate, $paymentNumber, $locationNumber) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode' => 'outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'Year' => new Expression('Year(outgoingPaymentDate)'),
                    'IssuedDate' => 'outgoingPaymentDate',
                    'outgoingPaymentID' => 'outgoingPaymentID',
                    'TotalAmount' => 'outgoingPaymentAmount',
                    'paymentMethodID' => 'paymentMethodID',
                    'locationID' => 'locationID',
                    'supplierID' => 'supplierID',
                    'outgoingPaymentType',
                    'entityID'
                ))
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount'), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodReferenceNumber'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode', 'supplierTitle', 'supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order(array('outgoingPaymentID'))
                ->group(array('outgoingPaymentID', 'outgoingPaymentInvoiceID'))                
        ->where->equalTo('entity.deleted', 0)
        ->where->between('outgoingPaymentDate', $fromDate, $toDate)
        ->where->in('outgoingPayment.locationID', $locationNumber)
        ->where->NEST->in('paymentMethod.paymentMethodID', $paymentNumber)
        ->or->in('outgoingPayment.paymentMethodID', $paymentNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    public function getDailyPaymentData($fromDate, $toDate, $paymentNumber, $locationNumber)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode' => 'outgoingPaymentCode',
                    'Day' => 'outgoingPaymentDate',
                    'IssuedDate' => 'outgoingPaymentDate',
                    'outgoingPaymentID' => 'outgoingPaymentID',
                    'TotalAmount' => 'outgoingPaymentAmount',
                    'paymentMethodID' => 'paymentMethodID',
                    'locationID' => 'locationID',
                    'supplierID' => 'supplierID',
                    'outgoingPaymentType',
                    'entityID'
                ))
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount'), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodReferenceNumber'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode','supplierTitle','supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order(array('outgoingPaymentID'))
                ->group(array('outgoingPaymentID','outgoingPaymentInvoiceID'))
        ->where->equalTo('entity.deleted', 0)
        ->where->between('outgoingPaymentDate', $fromDate, $toDate)
        ->where->in('outgoingPayment.locationID', $locationNumber)
        ->where->NEST->in('paymentMethod.paymentMethodID', $paymentNumber)
        ->or->in('outgoingPayment.paymentMethodID', $paymentNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        
        return $resultArray;
    }

    public function getAnnualPaymentData($year, $paymentNumber, $locationNumber)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'IssuedDate' => new Expression('outgoingPaymentDate'),
                    'outgoingPaymentID',
                    'paymentMethodID' => 'paymentMethodID',
                    'TotalAmount' => new Expression('outgoingPaymentAmount'),
                    'locationID',
                    'supplierID',
                    'outgoingPaymentType',
                    'entityID'
                ))
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount'), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodReferenceNumber'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode','supplierTitle','supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order('outgoingPaymentID')
                ->group(array('outgoingPaymentID','outgoingPaymentInvoiceID'))
        ->where->equalTo('entity.deleted', 0)
        ->where->like('outgoingPayment.outgoingPaymentDate', '%' . $year . '%')
        ->where->in('outgoingPayment.locationID', $locationNumber)
        ->where->NEST->in('paymentMethod.paymentMethodID', $paymentNumber)
        ->or->in('outgoingPayment.paymentMethodID', $paymentNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        
        return $resultArray;
    }

    public function getPaymentSummeryData($fromDate, $toDate, $paymentNumber, $locationNumber)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingoPaymentCode' => 'outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'IssuedDate' => new Expression('outgoingPaymentDate'),
                    'outgoingPaymentID',
                    'paymentMethodID' => 'paymentMethodID',
                    'TotalAmount' => new Expression('outgoingPaymentAmount'),
                    'locationID',
                    'supplierID',
                    'outgoingPaymentType',
                    'entityID'
                ))
                ->join('supplier', 'outgoingPayment.supplierID = supplier.supplierID', array('title' => new Expression('supplier.supplierTitle'), 'name' => new Expression('supplier.supplierName')), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodReferenceNumber'), 'left')
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentVoucher', 'outgoingPaymentInvoice.paymentVoucherId = paymentVoucher.paymentVoucherID', array('paymentVoucherCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order('outgoingPaymentID')
                ->group(array('outgoingPaymentID','outgoingPaymentInvoiceID'))
        ->where->equalTo('entity.deleted', 0)
        ->where->between('outgoingPaymentDate', $fromDate, $toDate)
        ->where->in('outgoingPayment.locationID', $locationNumber)
        ->where->NEST->in('paymentMethod.paymentMethodID', $paymentNumber)
        ->or->in('outgoingPayment.paymentMethodID', $paymentNumber);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        
        return $resultArray;
    }

    public function checkpurchaseRequisitionByCode($purchaseRequisitionCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('purchaseRequisition')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "purchaseRequisition.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('purchaseRequisition.purchaseRequisitionCode' => $purchaseRequisitionCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * Search po for Drop down
     * @param type $searchKey
     * @param type $status
     * @param type $locationID
     * @param type $checkAllProductCopied
     * @return type
     */
    public function searchPReqForDropdown($searchKey, $status, $locationID = FALSE, $checkAllProductCopied = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->order('purchaseRequisition.purchaseRequisitionID DESC');
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseRequisition.purchaseRequisitionRetrieveLocation = location.locationID', array('locationName', 'locationCode', 'locationID'));
        $select->join('supplier', 'purchaseRequisition.purchaseRequisitionSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(array('entity.deleted' => '0', 'purchaseRequisition.purchaseRequisitionDraftFlag' => '0'));
        $select->where->like('purchaseRequisitionCode', '%' . $searchKey . '%');
        $select->limit(50);
        if ($locationID) {
            $select->where(array('location.locationID' => $locationID));
        }
        if ($status != NULL) {
            $select->where(array('status' => $status));
        }
        if ($checkAllProductCopied) {
            $select->join('purchaseRequisitionProduct', 'purchaseRequisition.purchaseRequisitionID = purchaseRequisitionProduct.purchaseRequisitionID', '*');
            $select->where(array('purchaseRequisitionProduct.copied' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPReqListForSearch($preqID = false, $supplierID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseRequisition.purchaseRequisitionRetrieveLocation = location.locationID', array('locationName'));
        $select->join('supplier', 'purchaseRequisition.purchaseRequisitionSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        if ($preqID) {
            $select->where(array('purchaseRequisition.purchaseRequisitionID' => $preqID));
        }
        if ($supplierID) {
            $select->where(array('purchaseRequisition.purchaseRequisitionSupplierID' => $supplierID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

     public function getPurchaseRequisitionByID($id)
    {
        $rowset = $this->tableGateway->select(array('purchaseRequisitionID' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function getPOWithGrns($poIDs = false, $productIDs = false, $fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', array('deleted'));
        $select->join('purchaseRequisitionProduct', 'purchaseRequisition.purchaseRequisitionID');
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where->between('entity.createdTimeStamp',$fromDate, $toDate);

        if ($poIDs) {
            $select->where->in('purchaseRequisition.purchaseRequisitionID', $poIDs);
        }
        if ($productIDs) {
            $select->where(array('purchaseRequisition.purchaseRequisitionSupplierID' => $supplierID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get purchase order product details
     * @param array $purchaseRequisitionIds
     * @param string $fromDate
     * @param string $toDate
     * @return mixed
     */
    public function getpurchaseRequisitionWiseProducts($purchaseRequisitionIds = [], $fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition')
                ->columns([
                    'purchaseRequisitionID',
                    'purchaseRequisitionCode',
                    'status',
                    'deliveryDate' => new Expression('purchaseRequisitionEstDelDate')
                    ])
                ->join('purchaseRequisitionProduct', 'purchaseRequisition.purchaseRequisitionID = purchaseRequisitionProduct.purchaseRequisitionID', [
                    'poTotalQty' => new Expression('sum(purchaseRequisitionProductQuantity)'),
                    'purchaseRequisitionProductID',
                    'locationProductID'
                    ], 'left')
                ->join('entity', 'purchaseRequisition.entityID = entity.entityID', [
                    'deleted',
                    'poDate' => new Expression('DATE_FORMAT(`createdTimeStamp`, "%Y-%m-%d")')
                ])
                ->where->notEqualTo('status', 5);
        $select->where(array('purchaseRequisition.purchaseRequisitionDraftFlag' => '0'));
        if ($purchaseRequisitionIds) {
            $select->where->in('purchaseRequisition.purchaseRequisitionID', $purchaseRequisitionIds);
        }
        if ($fromDate && $toDate) {
            $select->where->between('createdTimeStamp', $fromDate, $toDate);
        }
        $select->group(['purchaseRequisitionID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get draft purchase orders
     * @return mixed
     */
    public function getDraftpurchaseRequisition()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition')
                ->where->equalTo('purchaseRequisition.status', '7');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updateDraftToPo($dataSet = [])
    {
        return $this->tableGateway->update($dataSet, array('purchaseRequisitionID' => $dataSet['purchaseRequisitionID']));
    }

    public function updateDraftDetails($dataSet)
    {
        return $this->tableGateway->update($dataSet, array('purchaseRequisitionID' => $dataSet['purchaseRequisitionID']));
    }

    public function getPoProductDetails($poID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionProduct');
        $select->where(array('purchaseRequisitionID' => $poID));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * get all PO details by given supplier id(s)
    *
    **/
    public function getpurchaseRequisitionBySupplierIDs($supplierIDs = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseRequisition")
                ->columns(array("*"))
                ->join("supplier", "purchaseRequisition.purchaseRequisitionSupplierID = supplier.supplierID", array("*"), "left")
                ->join('status', 'purchaseRequisition.status = status.statusID',array('statusName'),'left')
                ->join('entity', 'purchaseRequisition.entityID = entity.entityID',array('deleted', 'createdTimeStamp'))
                ->order(array('purchaseRequisitionEstDelDate' => 'DESC'))
                ->where(array('deleted' => 0));
        $select->where->in('purchaseRequisition.purchaseRequisitionSupplierID' ,$supplierIDs);
        $select->where->in('purchaseRequisition.status' , $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('purchaseRequisition.purchaseRequisitionEstDelDate', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getpurchaseRequisitionBySupplierIDsForSupplierBalance($supplierIDs = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseRequisition")
                ->columns(array("*"))
                ->join("supplier", "purchaseRequisition.purchaseRequisitionSupplierID = supplier.supplierID", array("*"), "left")
                ->join('status', 'purchaseRequisition.status = status.statusID',array('statusName'),'left')
                ->join('entity', 'purchaseRequisition.entityID = entity.entityID',array('deleted', 'createdTimeStamp'))
                ->order(array('createdTimeStamp' => 'DESC'))
                ->where(array('deleted' => 0));
        $select->where->in('purchaseRequisition.purchaseRequisitionSupplierID' ,$supplierIDs);
        $select->where->in('purchaseRequisition.status' , $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('entity.createdTimeStamp', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }
   
    public function getPODetailsByPoId($poID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseRequisition.purchaseRequisitionID' => $poID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;
    }

    // get po basic details with time stamp by pr id 
    public function getPODetailsByPrId($prID = null, $grnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->columns(array('*'));
        $select->join('grnProduct','grnProduct.grnProductDocumentId = purchaseRequisition.purchaseRequisitionID',array('*'),'left');
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($prID)) {
            $select->join('purchaseReturn','grnProduct.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
            $select->where(array('purchaseReturn.purchaseReturnID' => $prID,'grnProduct.grnProductDocumentTypeId' => 9));
        }

        if (!is_null($grnID)) {
            $select->where(array('grnProduct.grnID' => $grnID,'grnProduct.grnProductDocumentTypeId' => 9));
        }
        $select->group("purchaseRequisition.purchaseRequisitionID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    // get po basic details with time stamp related to pi  by debit note id 
    public function getPiRelatedPODetailsByDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->columns(array('*'));
        $select->join('purchaseInvoice','purchaseRequisition.purchaseRequisitionID = purchaseInvoice.purchaseInvoicePoID',array('*'),'left');
        $select->join('debitNote','purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID',array('*'),'left');
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('debitNote.debitNoteID' => $debitNoteID));
        $select->group("purchaseRequisition.purchaseRequisitionID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    // get po basic details with time stamp related to grn by debit note id 
    public function getGrnRelatedPODetailsByDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisition');
        $select->columns(array('*'));
        $select->join('grnProduct','grnProduct.grnProductDocumentId = purchaseRequisition.purchaseRequisitionID',array('*'),'left');
        $select->join('purchaseInvoice','grnProduct.grnID = purchaseInvoice.purchaseInvoiceGrnID',array('*'),'left');
        $select->join('debitNote','purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID',array('*'),'left');
        $select->join('entity', 'purchaseRequisition.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('debitNote.debitNoteID' => $debitNoteID));
        $select->group("purchaseRequisition.purchaseRequisitionID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }
}
