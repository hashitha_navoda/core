<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class PurchaseInvoiceProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePurchaseInvoiceProduct(PurchaseInvoiceProduct $piProduct)
    {
        $data = array(
            'purchaseInvoiceID' => $piProduct->purchaseInvoiceID,
            'locationProductID' => $piProduct->locationProductID,
            'productBatchID' => $piProduct->productBatchID,
            'productSerialID' => $piProduct->productSerialID,
            'purchaseInvoiceProductQuantity' => $piProduct->purchaseInvoiceProductQuantity,
            'purchaseInvoiceProductTotalQty' => $piProduct->purchaseInvoiceProductTotalQty,
            'purchaseInvoiceProductPrice' => $piProduct->purchaseInvoiceProductPrice,
            'purchaseInvoiceProductDiscount' => $piProduct->purchaseInvoiceProductDiscount,
            'purchaseInvoiceProductTotal' => $piProduct->purchaseInvoiceProductTotal,
            'purchaseInvoiceProductDocumentTypeID' => $piProduct->purchaseInvoiceProductDocumentTypeID,
            'purchaseInvoiceProductDocumentID' => $piProduct->purchaseInvoiceProductDocumentID,
            'purchaseInvoiceProductSelectedUomId' => $piProduct->purchaseInvoiceProductSelectedUomId,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function CheckPurchaseInvoiceProductByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoiceProduct')
                ->columns(array('*'))
                ->join('purchaseInvoice', 'purchaseInvoice.purchaseInvoiceID =  purchaseInvoiceProduct.purchaseInvoiceID', array('entityID'), 'left')
                ->join('entity', 'entity.entityID = purchaseInvoice.entityID', array('deleted'), 'left')
                ->where(array('locationProductID' => $locationProductID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function getPVProductByPVIDForDebitNote($PVID, $documentType = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoiceProduct")
                ->columns(array("*"));
                if ($documentType) {
                    $select->join("grnProduct", "purchaseInvoiceProduct.purchaseInvoiceProductDocumentID = grnProduct.grnProductID", array("grnProductID"), "left");
                }
                $select->join("locationProduct", "purchaseInvoiceProduct.locationProductID = locationProduct.locationProductID", array("*"), "left")
                ->join("product", "locationProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion'), "left")
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("purchaseInvoiceProductTax", " purchaseInvoiceProduct.purchaseInvoiceProductID= purchaseInvoiceProductTax.purchaseInvoiceProductID", array("purchaseInvoiceProductTaxID", "purchaseInvoiceTaxID", "purchaseInvoiceTaxPrecentage", "purchaseInvoiceTaxAmount"), "left")
                ->join("tax", "purchaseInvoiceProductTax.purchaseInvoiceTaxID = tax.id", array("*"), "left")
                ->where(array('productUomConversion' => '1'))
                ->where(array("purchaseInvoiceProduct.purchaseInvoiceID" => $PVID));
        if ($documentType) {
            $select->group("purchaseInvoiceProduct.purchaseInvoiceProductID","grnProduct.grnProductID");
        } 
        // $select->group("purchaseInvoiceProduct.purchaseInvoiceProductPrice");
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getPurchaseInvoiceProductByLocationProductID($locationProductID, $sortByDate = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoiceProduct');
        $select->columns(array('*', 'itemInPrice' => new Expression('purchaseInvoiceProductPrice')));
        $select->join('purchaseInvoice', 'purchaseInvoiceProduct.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID', array('itemInDate' => new Expression('purchaseInvoiceIssueDate'), 'purchaseInvoiceID'), 'left');
        if ($sortByDate) {
            $select->order(array('itemInDate DESC'));
        }
        $select->where(array('locationProductID' => $locationProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = array();
        foreach ($results as $result) {
            $result['documentID'] = $result['purchaseInvoiceID'];
            $result['documentType'] = 'Payment Voucher';
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }
    /** 
    * use to update purhcaseInvoiceProduct Copied item references
    * @param array $data
    * @param int $copyDocumentTypeID
    * @param int $copyDocumentID
    * return boolean
    **/
    public function updateCopiedDocumentReference($data, $copyDocumentTypeID, $copyDocumentID)
    {
        try {
            $this->tableGateway->update($data, array('purchaseInvoiceProductDocumentTypeID' => $copyDocumentTypeID, 'purchaseInvoiceProductDocumentID' => $copyDocumentID));
            return true;
        } catch (Exception $e) {
            error_log($e);
            return false;
        }
    }

    /*
    * This function is used to update purchase invoice products.
    */
    public function updatePurchaseInvoicePoductByPurchaseInvoiceID($data, $invoiceID)
    {
        try {
            $this->tableGateway->update($data, array('purchaseInvoiceID' => $invoiceID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get purchase invoice product details by purchase invoice product id
     * @param $purchaseInvocieProductId
     */
    public function getPurchaseInvoiceProductDetailsByPurchaseInvoiceProductID($purchaseInvoiceProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoiceProduct');
        $select->where(array('purchaseInvoiceProductID' => $purchaseInvoiceProductID));
        $select->join('grnProduct', 'purchaseInvoiceProduct.purchaseInvoiceProductDocumentID = grnProduct.grnProductID', array('*'), 'left');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * This function is used to get purchase invoice product details by purchase invoice id
     * @param $purchaseInvocieId
     */
    public function getPurchaseInvoiceProductDetailsByPurchaseInvoiceID($purchaseInvoiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoiceProduct');
        $select->where(array('purchaseInvoiceID' => $purchaseInvoiceID, 'purchaseInvoiceProductDocumentTypeID' => 10));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }


    public function getPVProductByPVIDForDebitNoteCreate($PVID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoiceProduct")
                ->columns(array("*"));
        $select->join("locationProduct", "purchaseInvoiceProduct.locationProductID = locationProduct.locationProductID", array("*"), "left")
                ->join("product", "locationProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion'), "left")
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("purchaseInvoiceProductTax", " purchaseInvoiceProduct.purchaseInvoiceProductID= purchaseInvoiceProductTax.purchaseInvoiceProductID", array("purchaseInvoiceProductTaxID", "purchaseInvoiceTaxID", "purchaseInvoiceTaxPrecentage", "purchaseInvoiceTaxAmount"), "left")
                ->join("tax", "purchaseInvoiceProductTax.purchaseInvoiceTaxID = tax.id", array("*"), "left")
                ->where(array('productUomConversion' => '1'))
                ->where(array("purchaseInvoiceProduct.purchaseInvoiceID" => $PVID));
        $select->join("grnProduct", "purchaseInvoiceProduct.purchaseInvoiceProductDocumentID = grnProduct.grnProductID", array("grnID"), "left");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * This function is used to get purchase invoice by purchase invoice  product id
     * @param $purchaseInvoiceProductID
     */
    public function getPurchaseInvoiceDetailsByPurchaseInvoiceProductID($purchaseInvoiceProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoiceProduct');
        $select->join('purchaseInvoice', 'purchaseInvoice.purchaseInvoiceID = purchaseInvoiceProduct.purchaseInvoiceID',array('purchaseInvoiceGrnID'),'left');
        $select->where(array('purchaseInvoiceProductID' => $purchaseInvoiceProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getAllPiProductsByPoIDForSupReport($purchaseInvoiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoiceProduct',['purchaseInvoiceProductID', 'locationProductID', 'purchaseInvoiceProductPrice', 
                    'purchaseInvoiceProductQuantity', 
                    'purchaseInvoiceProductTotal'
                    ], 'left')
                ->join('locationProduct', 'purchaseInvoiceProduct.locationProductID = locationProduct.locationProductID', ['productID','locationID'], 'left')
                ->join('product', 'locationProduct.productID = product.productID', ['productCode', 'productName'], 'left')
                ->where->equalTo('purchaseInvoiceProduct.purchaseInvoiceID', $purchaseInvoiceID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultArray = [];
        if (sizeof($result) > 0) {
            foreach ($result as $key => $value) {
                $resultArray[] = $value;
            }
        }

        return $resultArray;
    }
}
