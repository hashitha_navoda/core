<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseRequisitionProductTax implements InputFilterAwareInterface
{

    public $purchaseRequisitionProductTaxID;
    public $purchaseRequisitionID;
    public $purchaseRequisitionProductID;
    public $purchaseRequisitionTaxID;
    public $purchaseRequisitionTaxPrecentage;
    public $purchaseRequisitionTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseRequisitionProductTaxID = (!empty($data['purchaseRequisitionProductTaxID'])) ? $data['purchaseRequisitionProductTaxID'] : null;
        $this->purchaseRequisitionID = (!empty($data['purchaseRequisitionID'])) ? $data['purchaseRequisitionID'] : null;
        $this->purchaseRequisitionProductID = (!empty($data['purchaseRequisitionProductID'])) ? $data['purchaseRequisitionProductID'] : null;
        $this->purchaseRequisitionTaxID = (!empty($data['purchaseRequisitionTaxID'])) ? $data['purchaseRequisitionTaxID'] : null;
        $this->purchaseRequisitionTaxPrecentage = (!empty($data['purchaseRequisitionTaxPrecentage'])) ? $data['purchaseRequisitionTaxPrecentage'] : null;
        $this->purchaseRequisitionTaxAmount = (!empty($data['purchaseRequisitionTaxAmount'])) ? $data['purchaseRequisitionTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

