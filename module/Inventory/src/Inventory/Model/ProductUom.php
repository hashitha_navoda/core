<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductUom implements InputFilterAwareInterface
{

    public $productID;
    public $uomID;
    public $productUomConversion;
    public $productUomDisplay;
    public $productUomBase;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->uomID = (!empty($data['uomID'])) ? $data['uomID'] : null;
        $this->productUomConversion = (!empty($data['productUomConversion'])) ? $data['productUomConversion'] : null;
        $this->productUomDisplay = (isset($data['productUomDisplay'])) ? $data['productUomDisplay'] : 0;
        $this->productUomBase = (isset($data['productUomBase'])) ? $data['productUomBase'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
