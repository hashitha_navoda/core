<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\ExpiryAlertNotification;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class FreeIssueDetailsTable 
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveFreeIssueDetails($data)
    {
        $dataSet = array(
            'majorProductID' => $data->majorProductID,
            'majorQuantity' => $data->majorQuantity,
            'majorConditionType' => $data->majorConditionType,
            'selectedMajorUom' => $data->selectedMajorUom,
            'locationID' => $data->locationID,
        );

        try {
            $this->tableGateway->insert($dataSet);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        } catch (\Exception $exc) {
            echo $exc;
            return false;
        }
    }
    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('freeIssueDetails')
                    ->columns(array('*'));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('freeIssueDetails');
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    } 

    public function getFreeItemDetailsByProductID($productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('freeIssueDetails')
                ->columns(array('*'))
                ->where(array('freeIssueDetails.majorProductID' => $productID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset->current();
    }


    public function updateFreeIssueDetails($freeIssueDetailsID, $data)
    {
        if ($this->tableGateway->update($data, array('freeIssueDetailID' => $freeIssueDetailsID))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
