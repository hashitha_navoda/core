<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Tax Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductHandeling implements InputFilterAwareInterface
{

    public $productID;
    public $productHandelingManufactureProduct;
    public $productHandelingPurchaseProduct;
    public $productHandelingSalesProduct;
    public $productHandelingGiftCard;
    public $productHandelingBomItem;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->productHandelingManufactureProduct = (!empty($data['productHandelingManufactureProduct'])) ? $data['productHandelingManufactureProduct'] : 0;
        $this->productHandelingPurchaseProduct = (!empty($data['productHandelingPurchaseProduct'])) ? $data['productHandelingPurchaseProduct'] : 0;
        $this->productHandelingSalesProduct = (!empty($data['productHandelingSalesProduct'])) ? $data['productHandelingSalesProduct'] : 0;
        $this->productHandelingConsumables = (!empty($data['productHandelingConsumables'])) ? $data['productHandelingConsumables'] : 0;
        $this->productHandelingFixedAssets = (!empty($data['productHandelingFixedAssets'])) ? $data['productHandelingFixedAssets'] : 0;
        $this->productHandelingGiftCard = (!empty($data['productHandelingGiftCard'])) ? $data['productHandelingGiftCard'] : 0;
        $this->productHandelingBomItem = (!empty($data['productHandelingBomItem'])) ? $data['productHandelingBomItem'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
