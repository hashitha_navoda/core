<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the PI model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseInvoice implements InputFilterAwareInterface
{

    public $purchaseInvoiceID;
    public $purchaseInvoiceCode;
    public $purchaseInvoiceSupplierID;
    public $purchaseInvoiceSupplierReference;
    public $purchaseInvoiceRetrieveLocation;
    public $purchaseInvoicePaymentDueDate;
    public $purchaseInvoiceIssueDate;
    public $purchaseInvoicePoID;
    public $purchaseInvoiceGrnID;
    public $purchaseInvoiceComment;
    public $purchaseInvoiceDeliveryCharge;
    public $purchaseInvoiceShowTax;
    public $status;
    public $purchaseInvoiceTotal;
    public $purchaseInvoicePayedAmount;
    public $paymentTermID;
    public $entityID;
    public $purchaseInvoiceWiseTotalDiscount;
    public $purchaseInvoiceWiseTotalDiscountType;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseInvoiceID = (!empty($data['purchaseInvoiceID'])) ? $data['purchaseInvoiceID'] : null;
        $this->purchaseInvoiceCode = (!empty($data['purchaseInvoiceCode'])) ? $data['purchaseInvoiceCode'] : null;
        $this->purchaseInvoiceSupplierID = (!empty($data['purchaseInvoiceSupplierID'])) ? $data['purchaseInvoiceSupplierID'] : null;
        $this->purchaseInvoiceSupplierReference = (!empty($data['purchaseInvoiceSupplierReference'])) ? $data['purchaseInvoiceSupplierReference'] : null;
        $this->purchaseInvoiceRetrieveLocation = (!empty($data['purchaseInvoiceRetrieveLocation'])) ? $data['purchaseInvoiceRetrieveLocation'] : null;
        $this->purchaseInvoicePaymentDueDate = (!empty($data['purchaseInvoicePaymentDueDate'])) ? $data['purchaseInvoicePaymentDueDate'] : null;
        $this->purchaseInvoiceIssueDate = (!empty($data['purchaseInvoiceIssueDate'])) ? $data['purchaseInvoiceIssueDate'] : null;
        $this->purchaseInvoicePoID = (!empty($data['purchaseInvoicePoID'])) ? $data['purchaseInvoicePoID'] : null;
        $this->purchaseInvoiceGrnID = (!empty($data['purchaseInvoiceGrnID'])) ? $data['purchaseInvoiceGrnID'] : null;
        $this->purchaseInvoiceComment = (!empty($data['purchaseInvoiceComment'])) ? $data['purchaseInvoiceComment'] : null;
        $this->purchaseInvoiceDeliveryCharge = (!empty($data['purchaseInvoiceDeliveryCharge'])) ? $data['purchaseInvoiceDeliveryCharge'] : 0.00;
        $this->purchaseInvoiceShowTax = (!empty($data['purchaseInvoiceShowTax'])) ? $data['purchaseInvoiceShowTax'] : 0;
        $this->status = (!empty($data['status'])) ? $data['status'] : 3;
        $this->purchaseInvoiceTotal = (!empty($data['purchaseInvoiceTotal'])) ? $data['purchaseInvoiceTotal'] : 0.00;
        $this->purchaseInvoicePayedAmount = (!empty($data['purchaseInvoicePayedAmount'])) ? $data['purchaseInvoicePayedAmount'] : 0.00;
        $this->paymentTermID = (!empty($data['paymentTermID'])) ? $data['paymentTermID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->purchaseInvoiceWiseTotalDiscount = (!empty($data['purchaseInvoiceWiseTotalDiscount'])) ? $data['purchaseInvoiceWiseTotalDiscount'] : 0.00;
        $this->purchaseInvoiceWiseTotalDiscountType = (!empty($data['purchaseInvoiceWiseTotalDiscountType'])) ? $data['purchaseInvoiceWiseTotalDiscountType'] : null;
        $this->purchaseInvoiceWiseTotalDiscountRate = (!empty($data['purchaseInvoiceWiseTotalDiscountRate'])) ? $data['purchaseInvoiceWiseTotalDiscountRate'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
