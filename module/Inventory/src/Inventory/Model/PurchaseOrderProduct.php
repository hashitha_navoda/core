<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the POProduct model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseOrderProduct implements InputFilterAwareInterface
{

    public $purchaseOrderProductID;
    public $purchaseOrderID;
    public $locationProductID;
    public $purchaseOrderProductQuantity;
    public $purchaseOrderProductPrice;
    public $purchaseOrderProductDiscount;
    public $purchaseOrderProductTotal;
    public $copied;
    public $purchaseOrderProductCopiedQuantity;
    public $purchaseOrderProductDocumentId;
    public $purchaseOrderProductDocumentTypeId;
    public $purchaseOrderProductDoumentTypeCopiedQuantity;
    public $purchaseOrderProductSelectedUomId;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseOrderProductID = (!empty($data['purchaseOrderProductID'])) ? $data['purchaseOrderProductID'] : null;
        $this->purchaseOrderID = (!empty($data['purchaseOrderID'])) ? $data['purchaseOrderID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->purchaseOrderProductQuantity = (!empty($data['purchaseOrderProductQuantity'])) ? $data['purchaseOrderProductQuantity'] : null;
        $this->purchaseOrderProductPrice = (!empty($data['purchaseOrderProductPrice'])) ? $data['purchaseOrderProductPrice'] : null;
        $this->purchaseOrderProductDiscount = (!empty($data['purchaseOrderProductDiscount'])) ? $data['purchaseOrderProductDiscount'] : null;
        $this->purchaseOrderProductTotal = (!empty($data['purchaseOrderProductTotal'])) ? $data['purchaseOrderProductTotal'] : null;
        $this->copied = (!empty($data['copied'])) ? $data['copied'] : 0;
        $this->purchaseOrderProductCopiedQuantity = (!empty($data['purchaseOrderProductCopiedQuantity'])) ? $data['purchaseOrderProductCopiedQuantity'] : 0;
        $this->purchaseOrderProductDocumentId = (!empty($data['purchaseOrderProductDocumentId'])) ? $data['purchaseOrderProductDocumentId'] : null;
        $this->purchaseOrderProductDocumentTypeId = (!empty($data['purchaseOrderProductDocumentTypeId'])) ? $data['purchaseOrderProductDocumentTypeId'] : null;
        $this->purchaseOrderProductDoumentTypeCopiedQuantity = (!empty($data['purchaseOrderProductDoumentTypeCopiedQuantity'])) ? $data['purchaseOrderProductDoumentTypeCopiedQuantity'] : null;
        $this->purchaseOrderProductSelectedUomId = (!empty($data['purchaseOrderProductSelectedUomId'])) ? $data['purchaseOrderProductSelectedUomId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
