<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PurchaseOrderTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param boolean  $checkAllProductCopied check all product of po is copied
     * @param Paginator true or false
     * @return paginator object if true, else resultset
     */
    public function getPurchaseOrders($paginated = FALSE, $userActiveLocationID = NULL, $status = NULL, $checkAllProductCopied = False)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->order('purchaseOrder.purchaseOrderID DESC');
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseOrder.purchaseOrderRetrieveLocation = location.locationID', array('locationName', 'locationCode', 'locationID'));
        $select->join('supplier', 'purchaseOrder.purchaseOrderSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'), 'left');
        $select->where(array('deleted' => '0'));
        if ($userActiveLocationID != NULL) {
            $select->where(array('locationID' => $userActiveLocationID));
        }
        if ($status != NULL) {
            $select->where(array('status' => $status));
        } else {
            $select->where->notEqualTo('status' ,10);
            // $select->where->notEqualTo('status' ,13);

        }
        if ($checkAllProductCopied) {
            $select->join('purchaseOrderProduct', 'purchaseOrder.purchaseOrderID = purchaseOrderProduct.purchaseOrderID', '*');
            $select->where(array('purchaseOrderProduct.copied' => 0));
        }
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param String $poSearchKey
     * @return ResultSet
     */
    public function getPosforSearch($poSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $poSearchKey . '\' in supplier.supplierName )>0,POSITION(\'' . $poSearchKey . '\' in supplier.supplierName), 9999),'
                    . 'IF(POSITION(\'' . $poSearchKey . '\' in purchaseOrder.purchaseOrderCode )>0,POSITION(\'' . $poSearchKey . '\' in purchaseOrder.purchaseOrderCode), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(supplier.supplierName ), CHAR_LENGTH(purchaseOrder.purchaseOrderCode )) '),
            '*',
        ));
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseOrder.purchaseOrderRetrieveLocation = location.locationID', array('locationName'));
        $select->join('supplier', 'purchaseOrder.purchaseOrderSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('purchaseOrder.purchaseOrderCode', 'like', '%' . $poSearchKey . '%'), new Operator('supplier.supplierName', 'like', '%' . $poSearchKey . '%')), PredicateSet::OP_OR));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPoByPoCode($poCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->join('purchaseOrderProduct', 'purchaseOrder.purchaseOrderID = purchaseOrderProduct.purchaseOrderID', array('*'));
        $select->join('locationProduct', 'purchaseOrderProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'));
        $select->join('supplier', 'purchaseOrder.purchaseOrderSupplierID = supplier.supplierID', array('supplierCode', 'supplierName'), 'left');
        $select->join('location', 'purchaseOrder.purchaseOrderRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('purchaseOrderProductTax', 'purchaseOrderProduct.purchaseOrderProductID = purchaseOrderProductTax.purchaseOrderProductID', array('purchaseOrderTaxID', 'purchaseOrderTaxPrecentage', 'purchaseOrderTaxAmount'), 'left');
        $select->join('tax', 'purchaseOrderProductTax.purchaseOrderTaxID = tax.id', array('taxName'), 'left');
        $select->where(array('purchaseOrderCode' => $poCode));
        $select->where(array('productUomBase' => '1','purchaseOrder.status' => '3'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPoByPoID($poID, $toGrn = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', ['createdBy','createdTimeStamp'], 'left');
        $select->join('user', 'user.userID = entity.createdBy', ['createdUser' => 'userUsername'], 'left');
        $select->join('purchaseOrderProduct', 'purchaseOrder.purchaseOrderID = purchaseOrderProduct.purchaseOrderID', array('*'));
        $select->join('locationProduct', 'purchaseOrderProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('supplier', 'purchaseOrder.purchaseOrderSupplierID = supplier.supplierID', array('supplierCode', 'supplierName', 'supplierEmail', 'supplierAddress', 'supplierTitle', 'supplierOutstandingBalance', 'supplierCreditBalance'), 'left');
        $select->join('location', 'purchaseOrder.purchaseOrderRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('purchaseOrderProductTax', 'purchaseOrderProduct.purchaseOrderProductID = purchaseOrderProductTax.purchaseOrderProductID', array('purchaseOrderTaxID', 'purchaseOrderTaxPrecentage', 'purchaseOrderTaxAmount'), 'left');
        $select->join('tax', 'purchaseOrderProductTax.purchaseOrderTaxID = tax.id', array('taxName'), 'left');
        $select->where(array('purchaseOrder.purchaseOrderID' => $poID));
        $select->where(array('productUomBase' => '1'));
        if ($toGrn) {
            $select->where(array('purchaseOrderProduct.copied' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function savePo(PurchaseOrder $po)
    {
        $data = array(
            'purchaseOrderCode' => $po->purchaseOrderCode,
            'purchaseOrderSupplierID' => $po->purchaseOrderSupplierID,
            'purchaseOrderSupplierReference' => $po->purchaseOrderSupplierReference,
            'purchaseOrderRetrieveLocation' => $po->purchaseOrderRetrieveLocation,
            'purchaseOrderExpDelDate' => $po->purchaseOrderExpDelDate,
            'purchaseOrderDate' => $po->purchaseOrderDate,
            'purchaseOrderDescription' => $po->purchaseOrderDescription,
            'purchaseOrderDeliveryCharge' => $po->purchaseOrderDeliveryCharge,
            'purchaseOrderTotal' => $po->purchaseOrderTotal,
            'purchaseOrderShowTax' => $po->purchaseOrderShowTax,
            'status' => $po->status,
            'purchaseOrderHashValue' => $po->purchaseOrderHashValue,
            'entityID' => $po->entityID,
            'purchaseOrderDraftFlag' => $po->purchaseOrderDraftFlag,
            'pucrhaseRequisitionID' => $po->pucrhaseRequisitionID,
            'purchaseOrderApproved' => $po->purchaseOrderApproved
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return type
     */
    public function fetchDescAll()
    {
        $adpater = $this->tableGateway->getAdapter();
        $grn = new TableGateway('purchaseOrder', $adpater);
        $rowset = $grn->select(function (Select $select) {
            $select->order('purchaseOrderExpDelDate DESC');
        });
        return $rowset;
    }

    public function poStatusDetails($poIds, $isAllPO = false)
    {
        $isAllPO = filter_var($isAllPO, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder')
                ->columns(array(
                    'poID' => new Expression('purchaseOrder.purchaseOrderID'),
                    'poCD' => new Expression('purchaseOrder.purchaseOrderCode'),
                    'poTot' => new Expression('purchaseOrder.purchaseOrderTotal'),
                    'poDate' => new Expression('purchaseOrder.purchaseOrderExpDelDate')))
                ->join('supplier', 'supplier.supplierID=purchaseOrder.purchaseOrderSupplierID', array(
                    'sCD' => new Expression('supplier.supplierCode'),
                    'sTitle' => new Expression('supplier.supplierTitle'),
                    'sName' => new Expression('supplier.supplierName')))
                ->join('status', 'status.statusID=purchaseOrder.status', array('statusName'))
                ->order('purchaseOrder.purchaseOrderExpDelDate DESC');
        $select->where(array('purchaseOrder.purchaseOrderDraftFlag' => '0'));
        $select->where->notEqualTo('purchaseOrder.status', 10);
        if (!$isAllPO) {
            $select->where->in('purchaseOrder.purchaseOrderID', $poIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updatePoStatus($poID, $statusID)
    {
        $data = array(
            'status' => $statusID
        );
        return $this->tableGateway->update($data, array('purchaseOrderID' => $poID));
    }

    public function getLocationdata()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('location')
                ->columns(array('locationID', 'locationName'))
                ->where('locationID IS NOT NULL ');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();


        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    public function getPaymentTypeData()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('paymentMethod')
                ->columns(array('paymentMethodID', 'paymentMethodName'))
                ->where('paymentMethodID IS NOT NULL ');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();


        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    public function getMonthlyPaymentData($fromDate, $toDate, $paymentNumber, $locationNumber) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode' => 'outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'Year' => new Expression('Year(outgoingPaymentDate)'),
                    'IssuedDate' => 'outgoingPaymentDate',
                    'outgoingPaymentID' => 'outgoingPaymentID',
                    'TotalAmount' => 'outgoingPaymentAmount',
                    'paymentMethodID' => 'paymentMethodID',
                    'locationID' => 'locationID',
                    'supplierID' => 'supplierID',
                    'outgoingPaymentType',
                    'entityID'
                ))
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount', 'outgoingInvoiceCreditAmount'), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodReferenceNumber'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode', 'supplierTitle', 'supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order(array('outgoingPaymentID'))
                ->group(array('outgoingPaymentID', 'outgoingPaymentInvoiceID'))                
        ->where->equalTo('entity.deleted', 0)
        ->where->between('outgoingPaymentDate', $fromDate, $toDate)
        ->where->in('outgoingPayment.locationID', $locationNumber)
        ->where->NEST->in('paymentMethod.paymentMethodID', $paymentNumber)
        ->or->in('outgoingPayment.paymentMethodID', $paymentNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }


    public function getMonthlyPaymentDataForNewReport($fromDate, $toDate, $paymentNumber, $locationNumber) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode' => 'outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'Year' => new Expression('Year(outgoingPaymentDate)'),
                    'IssuedDate' => 'outgoingPaymentDate',
                    'outgoingPaymentID' => 'outgoingPaymentID',
                    'TotalAmount' => 'outgoingPaymentAmount',
                    'paymentMethodID' => 'paymentMethodID',
                    'locationID' => 'locationID',
                    'supplierID' => 'supplierID',
                    'outgoingPaymentType',
                    'entityID',
                    'outgoingPaymentCreditAmount',
                    'outgoingPaymentPaidAmount',
                    'outgoingPaymentBalanceAmount'
                ))
                
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode', 'supplierTitle', 'supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order(array('outgoingPaymentID'))
                ->group(array('outgoingPaymentID'))                
        ->where->equalTo('entity.deleted', 0)
        ->where->between('outgoingPaymentDate', $fromDate, $toDate)
        ->where->in('outgoingPayment.locationID', $locationNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    public function getCreditPaymentData($fromDate, $toDate, $paymentNumber, $locationNumber, $year = NULL) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode' => 'outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'Year' => new Expression('Year(outgoingPaymentDate)'),
                    'Day' => 'outgoingPaymentDate',
                    'IssuedDate' => 'outgoingPaymentDate',
                    'outgoingPaymentID' => 'outgoingPaymentID',
                    'TotalAmount' => 'outgoingPaymentAmount',
                    'paymentMethodID' => 'paymentMethodID',
                    'locationID' => 'locationID',
                    'supplierID' => 'supplierID',
                    'outgoingPaymentType',
                    'entityID'
                ))
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount', 'outgoingInvoiceCreditAmount'), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodReferenceNumber'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode', 'supplierTitle', 'supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order(array('outgoingPaymentID'))
                ->group(array('outgoingPaymentID', 'outgoingPaymentInvoiceID'))                
        ->where->equalTo('entity.deleted', 0);

        if (!empty($year)) {
            $select->where->like('outgoingPayment.outgoingPaymentDate', '%' . $year . '%');
        } else {
            $select->where->between('outgoingPaymentDate', $fromDate, $toDate);
        }

        $select->where->in('outgoingPayment.locationID', $locationNumber)
        ->where->notEqualTo('outgoingPaymentInvoice.outgoingInvoiceCreditAmount', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    public function getDailyPaymentData($fromDate, $toDate, $paymentNumber, $locationNumber)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode' => 'outgoingPaymentCode',
                    'Day' => 'outgoingPaymentDate',
                    'IssuedDate' => 'outgoingPaymentDate',
                    'outgoingPaymentID' => 'outgoingPaymentID',
                    'TotalAmount' => 'outgoingPaymentAmount',
                    'paymentMethodID' => 'paymentMethodID',
                    'locationID' => 'locationID',
                    'supplierID' => 'supplierID',
                    'outgoingPaymentType',
                    'entityID'
                ))
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount'), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodReferenceNumber'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentVoucher', 'outgoingPaymentInvoice.paymentVoucherId = paymentVoucher.paymentVoucherID', array('paymentVoucherCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode','supplierTitle','supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order(array('outgoingPaymentID'))
                ->group(array('outgoingPaymentID','outgoingPaymentInvoiceID'))
        ->where->equalTo('entity.deleted', 0)
        ->where->between('outgoingPaymentDate', $fromDate, $toDate)
        ->where->in('outgoingPayment.locationID', $locationNumber)
        ->where->NEST->in('paymentMethod.paymentMethodID', $paymentNumber)
        ->or->in('outgoingPayment.paymentMethodID', $paymentNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        
        return $resultArray;
    }

    public function getDailyPaymentDataForNewReport($fromDate, $toDate, $paymentNumber, $locationNumber) 
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode' => 'outgoingPaymentCode',
                    'Day' => 'outgoingPaymentDate',
                    'IssuedDate' => 'outgoingPaymentDate',
                    'outgoingPaymentID' => 'outgoingPaymentID',
                    'TotalAmount' => 'outgoingPaymentAmount',
                    'paymentMethodID' => 'paymentMethodID',
                    'locationID' => 'locationID',
                    'supplierID' => 'supplierID',
                    'outgoingPaymentType',
                    'entityID',
                    'outgoingPaymentCreditAmount',
                    'outgoingPaymentPaidAmount',
                    'outgoingPaymentBalanceAmount'
                ))
                
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode', 'supplierTitle', 'supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order(array('outgoingPaymentID'))
                ->group(array('outgoingPaymentID'))                
        ->where->equalTo('entity.deleted', 0)
        ->where->between('outgoingPaymentDate', $fromDate, $toDate)
        ->where->in('outgoingPayment.locationID', $locationNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    public function getAnnualPaymentData($year, $paymentNumber, $locationNumber)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'IssuedDate' => new Expression('outgoingPaymentDate'),
                    'outgoingPaymentID',
                    'paymentMethodID' => 'paymentMethodID',
                    'TotalAmount' => new Expression('outgoingPaymentAmount'),
                    'locationID',
                    'supplierID',
                    'outgoingPaymentType',
                    'entityID'
                ))
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount'), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodReferenceNumber'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode','supplierTitle','supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order('outgoingPaymentID')
                ->group(array('outgoingPaymentID','outgoingPaymentInvoiceID'))
        ->where->equalTo('entity.deleted', 0)
        ->where->like('outgoingPayment.outgoingPaymentDate', '%' . $year . '%')
        ->where->in('outgoingPayment.locationID', $locationNumber)
        ->where->NEST->in('paymentMethod.paymentMethodID', $paymentNumber)
        ->or->in('outgoingPayment.paymentMethodID', $paymentNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        
        return $resultArray;
    }

    public function getAnnualPaymentDataForNewReport($year, $paymentNumber, $locationNumber) 
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingPaymentCode' => 'outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'IssuedDate' => 'outgoingPaymentDate',
                    'outgoingPaymentID' => 'outgoingPaymentID',
                    'TotalAmount' => 'outgoingPaymentAmount',
                    'paymentMethodID' => 'paymentMethodID',
                    'locationID' => 'locationID',
                    'supplierID' => 'supplierID',
                    'outgoingPaymentType',
                    'entityID',
                    'outgoingPaymentCreditAmount',
                    'outgoingPaymentPaidAmount',
                    'outgoingPaymentBalanceAmount'
                ))
                
                ->join('supplier', 'outgoingPayment.supplierID=supplier.supplierID', array('supplierCode', 'supplierTitle', 'supplierName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order(array('outgoingPaymentID'))
                ->group(array('outgoingPaymentID'))                
        ->where->equalTo('entity.deleted', 0)
        ->where->like('outgoingPaymentDate', '%' . $year . '%')
        ->where->in('outgoingPayment.locationID', $locationNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    public function getPaymentSummeryData($fromDate, $toDate, $paymentNumber, $locationNumber)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingoPaymentCode' => 'outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'IssuedDate' => new Expression('outgoingPaymentDate'),
                    'outgoingPaymentID',
                    'paymentMethodID' => 'paymentMethodID',
                    'TotalAmount' => new Expression('outgoingPaymentAmount'),
                    'locationID',
                    'supplierID',
                    'outgoingPaymentType',
                    'entityID'
                ))
                ->join('supplier', 'outgoingPayment.supplierID = supplier.supplierID', array('title' => new Expression('supplier.supplierTitle'), 'name' => new Expression('supplier.supplierName')), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outgoingPayment.outgoingPaymentID = outGoingPaymentMethodsNumbers.outGoingPaymentID', array('outGoingPaymentMethodReferenceNumber'), 'left')
                ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentInvoiceID', 'outgoingInvoiceCashAmount','outgoingInvoiceCreditAmount'), 'left')
                ->join('purchaseInvoice', 'outgoingPaymentInvoice.invoiceID = purchaseInvoice.purchaseInvoiceID', array('purchaseInvoiceCode'), 'left')
                ->join('paymentVoucher', 'outgoingPaymentInvoice.paymentVoucherId = paymentVoucher.paymentVoucherID', array('paymentVoucherCode'), 'left')
                ->join('paymentMethod', 'outgoingPaymentInvoice.paymentMethodID = paymentMethod.paymentMethodID', array('paymentMethodName'), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order('outgoingPaymentID')
                ->group(array('outgoingPaymentID','outgoingPaymentInvoiceID'))
        ->where->equalTo('entity.deleted', 0)
        ->where->between('outgoingPaymentDate', $fromDate, $toDate)
        ->where->in('outgoingPayment.locationID', $locationNumber)
        ->where->NEST->in('paymentMethod.paymentMethodID', $paymentNumber)
        ->or->in('outgoingPayment.paymentMethodID', $paymentNumber)
        ->or->notEqualTo('outgoingPaymentInvoice.outgoingInvoiceCreditAmount', 0);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        
        return $resultArray;
    }

    public function getPaymentSummeryDataForNewReport($fromDate, $toDate, $paymentNumber, $locationNumber)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('outgoingPayment')
                ->columns(array('outgoingoPaymentCode' => 'outgoingPaymentCode',
                    'Month' => new Expression('DATE_FORMAT(`outgoingPaymentDate`, "%M")'),
                    'IssuedDate' => new Expression('outgoingPaymentDate'),
                    'outgoingPaymentID',
                    'paymentMethodID' => 'paymentMethodID',
                    'TotalAmount' => new Expression('outgoingPaymentAmount'),
                    'locationID',
                    'supplierID',
                    'outgoingPaymentType',
                    'entityID',
                    'outgoingPaymentCreditAmount',
                    'outgoingPaymentPaidAmount',
                    'outgoingPaymentBalanceAmount'
                ))
                ->join('supplier', 'outgoingPayment.supplierID = supplier.supplierID', array('title' => new Expression('supplier.supplierTitle'), 'name' => new Expression('supplier.supplierName')), 'left')
                ->join('location', 'outgoingPayment.locationID=location.locationID', array('locationName'), 'left')
                ->join('entity', 'outgoingPayment.entityID=entity.entityID', array('deleted'), 'left')
                ->order('outgoingPaymentID')
                ->group(array('outgoingPaymentID'))
        ->where->equalTo('entity.deleted', 0)
        ->where->between('outgoingPaymentDate', $fromDate, $toDate)
        ->where->in('outgoingPayment.locationID', $locationNumber);
        // ->where->NEST->in('paymentMethod.paymentMethodID', $paymentNumber)
        // ->or->in('outgoingPayment.paymentMethodID', $paymentNumber)
        // ->or->notEqualTo('outgoingPaymentInvoice.outgoingInvoiceCreditAmount', 0);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        
        return $resultArray;
    }

    public function checkPurchaseOrderByCode($purchaseOrderCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('purchaseOrder')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "purchaseOrder.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('purchaseOrder.purchaseOrderCode' => $purchaseOrderCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * Search po for Drop down
     * @param type $searchKey
     * @param type $status
     * @param type $locationID
     * @param type $checkAllProductCopied
     * @return type
     */
    public function searchPoForDropdown($searchKey, $status, $locationID = FALSE, $checkAllProductCopied = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->order('purchaseOrder.purchaseOrderID DESC');
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseOrder.purchaseOrderRetrieveLocation = location.locationID', array('locationName', 'locationCode', 'locationID'));
        $select->join('supplier', 'purchaseOrder.purchaseOrderSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(array('entity.deleted' => '0', 'purchaseOrder.purchaseOrderDraftFlag' => '0'));
        $select->where->like('purchaseOrderCode', '%' . $searchKey . '%');
        $select->limit(50);
        if ($locationID) {
            $select->where(array('location.locationID' => $locationID));
        }
        if ($status != NULL) {
            $select->where(array('status' => $status));
        }
        if ($checkAllProductCopied) {
            $select->join('purchaseOrderProduct', 'purchaseOrder.purchaseOrderID = purchaseOrderProduct.purchaseOrderID', '*');
            $select->where(array('purchaseOrderProduct.copied' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPOListForSearch($poID = false, $supplierID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseOrder.purchaseOrderRetrieveLocation = location.locationID', array('locationName'));
        $select->join('supplier', 'purchaseOrder.purchaseOrderSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        if ($poID) {
            $select->where(array('purchaseOrder.purchaseOrderID' => $poID));
        }
        if ($supplierID) {
            $select->where(array('purchaseOrder.purchaseOrderSupplierID' => $supplierID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

     public function getPurchaseOrderByID($id)
    {
        $rowset = $this->tableGateway->select(array('purchaseOrderID' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function getPOWithGrns($poIDs = false, $productIDs = false, $fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', array('deleted'));
        $select->join('purchaseOrderProduct', 'purchaseOrder.purchaseOrderID');
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where->between('entity.createdTimeStamp',$fromDate, $toDate);

        if ($poIDs) {
            $select->where->in('purchaseOrder.purchaseOrderID', $poIDs);
        }
        if ($productIDs) {
            $select->where(array('purchaseOrder.purchaseOrderSupplierID' => $supplierID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get purchase order product details
     * @param array $purchaseOrderIds
     * @param string $fromDate
     * @param string $toDate
     * @return mixed
     */
    public function getPurchaseOrderWiseProducts($purchaseOrderIds = [], $fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder')
                ->columns([
                    'purchaseOrderID',
                    'purchaseOrderCode',
                    'status',
                    'deliveryDate' => new Expression('purchaseOrderExpDelDate')
                    ])
                ->join('purchaseOrderProduct', 'purchaseOrder.purchaseOrderID = purchaseOrderProduct.purchaseOrderID', [
                    'poTotalQty' => new Expression('sum(purchaseOrderProductQuantity)'),
                    'purchaseOrderProductID',
                    'locationProductID'
                    ], 'left')
                ->join('entity', 'purchaseOrder.entityID = entity.entityID', [
                    'deleted',
                    'poDate' => new Expression('DATE_FORMAT(`createdTimeStamp`, "%Y-%m-%d")')
                ])
                ->where->notEqualTo('status', 5);
        $select->where(array('purchaseOrder.purchaseOrderDraftFlag' => '0'));
        if ($purchaseOrderIds) {
            $select->where->in('purchaseOrder.purchaseOrderID', $purchaseOrderIds);
        }
        if ($fromDate && $toDate) {
            $select->where->between('createdTimeStamp', $fromDate, $toDate);
        }
        $select->group(['purchaseOrderID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get draft purchase orders
     * @return mixed
     */
    public function getDraftPurchaseOrder()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder')
                ->where->equalTo('purchaseOrder.status', '7');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updateDraftToPo($dataSet = [])
    {
        return $this->tableGateway->update($dataSet, array('purchaseOrderID' => $dataSet['purchaseOrderID']));
    }

    public function updateDraftPo($dataSet = [])
    {
        return $this->tableGateway->update($dataSet, array('purchaseOrderID' => $dataSet['purchaseOrderID']));
    }

    public function getPoProductDetails($poID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrderProduct');
        $select->where(array('purchaseOrderID' => $poID));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * get all PO details by given supplier id(s)
    *
    **/
    public function getPurchaseOrderBySupplierIDs($supplierIDs = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseOrder")
                ->columns(array("*"))
                ->join("supplier", "purchaseOrder.purchaseOrderSupplierID = supplier.supplierID", array("*"), "left")
                ->join('status', 'purchaseOrder.status = status.statusID',array('statusName'),'left')
                ->join('entity', 'purchaseOrder.entityID = entity.entityID',array('deleted', 'createdTimeStamp'))
                ->order(array('purchaseOrderExpDelDate' => 'DESC'))
                ->where(array('deleted' => 0));
        $select->where->in('purchaseOrder.purchaseOrderSupplierID' ,$supplierIDs);
        $select->where->in('purchaseOrder.status' , $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('purchaseOrder.purchaseOrderExpDelDate', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getPurchaseOrderBySupplierIDsForSupplierBalance($supplierIDs = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseOrder")
                ->columns(array("*"))
                ->join("supplier", "purchaseOrder.purchaseOrderSupplierID = supplier.supplierID", array("*"), "left")
                ->join('status', 'purchaseOrder.status = status.statusID',array('statusName'),'left')
                ->join('entity', 'purchaseOrder.entityID = entity.entityID',array('deleted', 'createdTimeStamp'))
                ->order(array('createdTimeStamp' => 'DESC'))
                ->where(array('deleted' => 0));
        $select->where->in('purchaseOrder.purchaseOrderSupplierID' ,$supplierIDs);
        $select->where->in('purchaseOrder.status' , $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('entity.createdTimeStamp', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }
   
    public function getPODetailsByPoId($poID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseOrder.purchaseOrderID' => $poID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;
    }

    // get po basic details with time stamp by pr id 
    public function getPODetailsByPrId($prID = null, $grnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->columns(array('*'));
        $select->join('grnProduct','grnProduct.grnProductDocumentId = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($prID)) {
            $select->join('purchaseReturn','grnProduct.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
            $select->where(array('purchaseReturn.purchaseReturnID' => $prID,'grnProduct.grnProductDocumentTypeId' => 9));
        }

        if (!is_null($grnID)) {
            $select->where(array('grnProduct.grnID' => $grnID,'grnProduct.grnProductDocumentTypeId' => 9));
        }
        $select->group("purchaseOrder.purchaseOrderID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    // get po basic details with time stamp related to pi  by debit note id 
    public function getPiRelatedPODetailsByDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->columns(array('*'));
        $select->join('purchaseInvoice','purchaseOrder.purchaseOrderID = purchaseInvoice.purchaseInvoicePoID',array('*'),'left');
        $select->join('debitNote','purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID',array('*'),'left');
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('debitNote.debitNoteID' => $debitNoteID));
        $select->group("purchaseOrder.purchaseOrderID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    // get po basic details with time stamp related to grn by debit note id 
    public function getGrnRelatedPODetailsByDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->columns(array('*'));
        $select->join('grnProduct','grnProduct.grnProductDocumentId = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('purchaseInvoice','grnProduct.grnID = purchaseInvoice.purchaseInvoiceGrnID',array('*'),'left');
        $select->join('debitNote','purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID',array('*'),'left');
        $select->join('entity', 'purchaseOrder.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('debitNote.debitNoteID' => $debitNoteID));
        $select->group("purchaseOrder.purchaseOrderID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    public function getPoStatus($fromdate, $toDate, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrder');
        $select->columns(array('poTotal' => new Expression('SUM(purchaseOrder.purchaseOrderTotal)')))
               ->join('status', 'status.statusID = purchaseOrder.status', array('statusName'));
        $select->where->between('purchaseOrderDate', $fromdate, $toDate);
        $select->where(array('purchaseOrder.purchaseOrderRetrieveLocation' => $locationID));
        $select->group("purchaseOrder.status");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }
}
