<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CompoundCostTemplate
{

    public $compoundCostTemplateID;
    public $compoundCostTemplateDescription;
    public $compoundCostTemplateName;
    public $locationID;
    public $entityID;
    public $status;
    public $compoundCostTemplateCostCalType;
    public $compoundCostTemplateCode;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->compoundCostTemplateID = (!empty($data['compoundCostTemplateID'])) ? $data['compoundCostTemplateID'] : null;
        $this->compoundCostTemplateDescription = (!empty($data['compoundCostTemplateDescription'])) ? $data['compoundCostTemplateDescription'] : null;
        $this->compoundCostTemplateName = (!empty($data['compoundCostTemplateName'])) ? $data['compoundCostTemplateName'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
        $this->compoundCostTemplateCostCalType = (!empty($data['compoundCostTemplateCostCalType'])) ? $data['compoundCostTemplateCostCalType'] : 1;
        $this->compoundCostTemplateCode = (!empty($data['compoundCostTemplateCode'])) ? $data['compoundCostTemplateCode'] : null;
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
