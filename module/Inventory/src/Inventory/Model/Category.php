<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * This file contains Category Table Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Category
{

    public $categoryID;
    public $categoryName;
    public $categoryParentID;
    public $categoryLevel;
    public $categoryPrefix;
    public $categoryState;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->categoryID = (!empty($data['categoryID'])) ? $data['categoryID'] : null;
        $this->categoryName = (!empty($data['categoryName'])) ? $data['categoryName'] : null;
        $this->categoryParentID = (!empty($data['categoryParentID'])) ? $data['categoryParentID'] : 0;
        $this->categoryLevel = (!empty($data['categoryLevel'])) ? $data['categoryLevel'] : 0;
        $this->categoryPrefix = (!empty($data['categoryPrefix'])) ? $data['categoryPrefix'] : null;
        $this->categoryState = (!empty($data['categoryState'])) ? $data['categoryState'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'categoryID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'categoryName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 255,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'categoryPrefix',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 10,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'categoryParentID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'digits'
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
