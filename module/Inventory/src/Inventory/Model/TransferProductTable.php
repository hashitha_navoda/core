<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Unit Measure Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class TransferProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveTransferProduct(TransferProduct $trasnferProduct)
    {
        $data = array(
            'transferID' => $trasnferProduct->transferID,
            'productID' => $trasnferProduct->productID,
            'productBatchID' => $trasnferProduct->productBatchID,
            'productSerialID' => $trasnferProduct->productSerialID,
            'transferProductQuantity' => $trasnferProduct->transferProductQuantity,
            'selectedUomId' => $trasnferProduct->selectedUomId,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function CheckTransferProductByProductID($productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transferProduct')
                ->columns(array('*'))
                ->join('transfer', 'transfer.transferID = transferProduct.transferID', array('entityID'), 'left')
                ->join('entity', 'entity.entityID = transfer.entityID', array('deleted'), 'left')
                ->where(array('productID' => $productID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function getProductTransferQtyInListByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null,$activeFlag = false, $categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('transferProduct')
                ->columns(array('transferProductID', 'qtyTransferIn' => new Expression('SUM(transferProductQuantity)')
                    ))
                ->join('transfer', 'transferProduct.transferID = transfer.transferID', array('transferID'))
                ->join('product', 'transferProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = transfer.locationIDIn', array())
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('itemIn', 'transfer.transferID = itemIn.itemInDocumentID', array('inCost' => new Expression('SUM(itemInQty*itemInPrice)'),'inQty' => new Expression('SUM(itemInQty)')))
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->equalTo('itemIn.itemInDocumentType', 'Inventory Transfer')
        ->where->in('transfer.locationIDIn', $locIds);
        if (!$isAllProducts) {
            $select->where->in('transferProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);   
        }

        $select->where->between('transfer.transferDate', $fromDate, $toDate);
        $select->group(array('transferProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }

    public function getProductTransferQtyOutListByLocIDAndDate($proIds, $locIds, $isAllProducts = false, $fromDate = null, $toDate = null, $activeFlag = false, $categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('transferProduct')
                ->columns(array('transferProductID', 'qtyTransferOut' => new Expression('SUM(transferProductQuantity)')
                    ))
                ->join('transfer', 'transferProduct.transferID = transfer.transferID', array('transferID'))
                ->join('product', 'transferProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),'entityID'), 'left')
                ->join('location', 'location.locationID = transfer.locationIDOut', array())
                ->join('entity', 'product.entityID = entity.entityID', array('deleted'),'left')
                ->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('itemIn', 'transfer.transferID = itemIn.itemInDocumentID', array('outCost' => new Expression('SUM(itemInQty*itemInPrice)'),'outQty' => new Expression('SUM(itemInQty)')))
                ->order('product.productCode')
        ->where->equalTo('entity.deleted', '0')
        ->where->equalTo('itemIn.itemInDocumentType', 'Inventory Transfer')
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->in('transfer.locationIDOut', $locIds);
        if (!$isAllProducts) {
            $select->where->in('transferProduct.productID', $proIds);
        }
        if($activeFlag){
            $select->where(array('product.productState' => 1));
        }
        if($categoryIds != null){
            $select->where->in('product.categoryID', $categoryIds);   
        }

        $select->where->between('transfer.transferDate', $fromDate, $toDate);
        $select->group(array('transferProduct.productID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $value) {
            $resultSet[] = $value;
        }
        return $resultSet;
    }
    /** 
    * this function use to get transfer details for given date range
    * @param array $proIds
    * @param array $locIds
    * @param date $fromDate
    * @param date $toDate
    * @param array $categoryIds
    * return array
    */
    public function gettransferProductSetByEndDate($proIds, $locIds, $fromDate = null, $toDate = null, $categoryIds, $isDocDateWise = false)
    {
        $adapter = $this->tableGateway->getAdapter();

        if ($isDocDateWise) {

            $statement = $adapter->query("SELECT transfer.transferID, transfer.transferDate, transferProduct.transferProductID,transferProduct.transferProductQuantity, itemIn.itemInAverageCostingPrice, product.productCode AS pCD, product.productID AS pID, product.productName AS pName, category.categoryName AS categoryName, locationProduct.locationID, transfer.locationIDIn, itemIn.itemInLocationProductID, locationProduct.locationProductID FROM transferProduct LEFT JOIN transfer ON transferProduct.transferID = transfer.transferID LEFT JOIN product ON transferProduct.productID = product.productID LEFT JOIN entity ON product.entityID = entity.entityID LEFT JOIN category ON product.categoryID = category.categoryID LEFT JOIN itemIn ON transfer.transferID = itemIn.itemInDocumentID LEFT JOIN locationProduct on product.productID = locationProduct.productID
                WHERE entity.deleted = 0 AND itemIn.itemInDocumentType = 'Inventory Transfer' AND product.productTypeID != 2 AND transfer.locationIDOut in ($locIds)  AND transferProduct.productID in ($proIds) AND category.categoryID in ($categoryIds) HAVING locationProduct.locationID = transfer.locationIDIn and itemIn.itemInLocationProductID = locationProduct.locationProductID");
        } else {
            $statement = $adapter->query("SELECT transfer.transferID, transfer.transferDate, transferProduct.transferProductID,transferProduct.transferProductQuantity, itemIn.itemInAverageCostingPrice, itemIn.itemInDateAndTime, product.productCode AS pCD, product.productID AS pID, product.productName AS pName, category.categoryName AS categoryName, locationProduct.locationID, transfer.locationIDIn, itemIn.itemInLocationProductID, locationProduct.locationProductID FROM transferProduct LEFT JOIN transfer ON transferProduct.transferID = transfer.transferID LEFT JOIN product ON transferProduct.productID = product.productID LEFT JOIN entity ON product.entityID = entity.entityID LEFT JOIN category ON product.categoryID = category.categoryID LEFT JOIN itemIn ON transfer.transferID = itemIn.itemInDocumentID LEFT JOIN locationProduct on product.productID = locationProduct.productID
                WHERE entity.deleted = 0 AND itemIn.itemInDocumentType = 'Inventory Transfer' AND product.productTypeID != 2 AND transfer.locationIDOut in ($locIds)  AND transferProduct.productID in ($proIds) AND category.categoryID in ($categoryIds) HAVING locationProduct.locationID = transfer.locationIDIn and itemIn.itemInLocationProductID = locationProduct.locationProductID");
        }
        $results = $statement->execute();
        
        return $results;
    }



}
