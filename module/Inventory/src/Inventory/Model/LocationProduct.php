<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class LocationProduct implements InputFilterAwareInterface
{

    public $locationProductID;
    public $productID;
    public $locationID;
    public $locationProductQuantity;
    public $locationProductMinInventoryLevel;
    public $locationProductMaxInventoryLevel;
    public $locationProductReOrderLevel;
    public $locationDiscountPercentage;
    public $locationDiscountValue;
    public $defaultSellingPrice;
    public $lastSellingPrice;
    public $lastPurchasePrice;
    public $locationProductAverageCostingPrice;
    public $locationProductLastItemInID;
    public $priceID;
    public $entityID;
    public $defaultPurchasePrice;
    public $isUseDiscountSchemeForLocation;
    public $locationDiscountSchemID;
    protected $inputFilter;

    public function exchangeArray($data)
    {

        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->locationProductQuantity = (!empty($data['locationProductQuantity'])) ? $data['locationProductQuantity'] : null;
        $this->locationProductMinInventoryLevel = (!empty($data['locationProductMinInventoryLevel'])) ? $data['locationProductMinInventoryLevel'] : 0;
        $this->locationProductMaxInventoryLevel = (!empty($data['locationProductMaxInventoryLevel'])) ? $data['locationProductMaxInventoryLevel'] : 0;
        $this->locationProductReOrderLevel = (!empty($data['locationProductReOrderLevel'])) ? $data['locationProductReOrderLevel'] : 0;
        $this->locationDiscountPercentage = $data['locationDiscountPercentage'];
        $this->locationDiscountValue = $data['locationDiscountValue'];
        $this->defaultSellingPrice = (!empty($data['defaultSellingPrice'])) ? $data['defaultSellingPrice'] : 0;
        $this->lastSellingPrice = (!empty($data['lastSellingPrice'])) ? $data['lastSellingPrice'] : null;
        $this->lastPurchasePrice = (!empty($data['lastPurchasePrice'])) ? $data['lastPurchasePrice'] : null;
        $this->locationProductAverageCostingPrice = (!empty($data['locationProductAverageCostingPrice'])) ? $data['locationProductAverageCostingPrice'] : 0.00;
        $this->locationProductLastItemInID = (!empty($data['locationProductLastItemInID'])) ? $data['locationProductLastItemInID'] : 0;
        $this->priceID = (!empty($data['priceID'])) ? $data['priceID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->defaultPurchasePrice = (!empty($data['defaultPurchasePrice'])) ? $data['defaultPurchasePrice'] : 0.00;
        $this->isUseDiscountSchemeForLocation = (!empty($data['isUseDiscountSchemeForLocation'])) ? $data['isUseDiscountSchemeForLocation'] : 0;
        $this->locationDiscountSchemID = (!empty($data['locationDiscountSchemID'])) ? $data['locationDiscountSchemID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function getInputFilter()
    {

    }

}
