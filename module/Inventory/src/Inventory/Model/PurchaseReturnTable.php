<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PurchaseReturnTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param Paginator true or false
     * @return paginator object if true, else resultset
     */
    public function getPurchaseReturns($paginated = FALSE, $userActiveLocationID = NULL, $status = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->order('purchaseReturnID DESC');
        $select->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('deleted'));
        $select->join('grn', 'purchaseReturn.purchaseReturnGrnID = grn.grnID', array('grnRetrieveLocation', 'grnSupplierID'),'left');
        $select->join(['grnLoaction' => 'location'], 'grn.grnRetrieveLocation = grnLoaction.locationID', array('gLocationName' => new Expression('grnLoaction.locationName'),'gLocationCode' => new Expression('grnLoaction.locationCode'),'gLocationID' => new Expression('grnLoaction.locationID')),'left');
        $select->join(['prLoaction' => 'location'], 'purchaseReturn.prLocationID = prLoaction.locationID', array('pLocationName' => new Expression('prLoaction.locationName'),'pLocationCode' => new Expression('prLoaction.locationCode'),'pLocationID' => new Expression('prLoaction.locationID')),'left');
        $select->join(['grnSupplier' => 'supplier'], 'grn.grnSupplierID = grnSupplier.supplierID', array('gSupplierName' => new Expression('grnSupplier.supplierName'),'gSupplierTitle' => new Expression('grnSupplier.supplierTitle')),'left');
        $select->join(['prSupplier' => 'supplier'], 'purchaseReturn.prSupplierID = prSupplier.supplierID', array('prSupplierName' => new Expression('prSupplier.supplierName'),'prSupplierTitle' => new Expression('prSupplier.supplierTitle')),'left');
        $select->where(array('deleted' => '0'));
        if ($userActiveLocationID != NULL) {
            $select->where(new PredicateSet(array(new Operator('grnLoaction.locationID', '=', $userActiveLocationID), new Operator('prLoaction.locationID', '=',$userActiveLocationID)), PredicateSet::OP_OR));
        }
        if ($status != NULL) {
            $select->where(array('status' => $status));
        }
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param String $prSearchKey
     * @return ResultSet
     */
    public function getPurchaseReturnsforSearch($prSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $prSearchKey . '\' in purchaseReturn.purchaseReturnCode )>0,POSITION(\'' . $prSearchKey . '\' in purchaseReturn.purchaseReturnCode), 9999),'
                    . 'IF(POSITION(\'' . $prSearchKey . '\' in supplier.supplierName )>0,POSITION(\'' . $prSearchKey . '\' in supplier.supplierName), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(purchaseReturn.purchaseReturnCode ), CHAR_LENGTH(supplier.supplierName )) '),
            '*',
        ));
        $select->order('purchaseReturnID DESC');
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('deleted'));
        $select->join('grn', 'purchaseReturn.purchaseReturnGrnID = grn.grnID', array('grnRetrieveLocation', 'grnSupplierID'));
        $select->join('location', 'grn.grnRetrieveLocation = location.locationID', array('locationName', 'locationCode', 'locationID'));
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('purchaseReturn.purchaseReturnCode', 'like', '%' . $prSearchKey . '%'), new Operator('supplier.supplierName', 'like', '%' . $prSearchKey . '%')), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function checkPrByCode($prCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('purchaseReturn')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "purchaseReturn.purchaseReturnEntityID = e.entityID")
                    ->join(array('g' => 'grn'), "purchaseReturn.purchaseReturnGrnID = g.grnID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('purchaseReturn.purchaseReturnCode' => $prCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * This function return pr data for the preview
     * @param type $prID
     */
    public function getPurchaseReturnDetailsByGrnID($grnID = null, $grnIds = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->join('purchaseReturnProduct', 'purchaseReturn.purchaseReturnID = purchaseReturnProduct.purchaseReturnID', array('*'));
        $select->join('locationProduct', 'purchaseReturnProduct.purchaseReturnLocationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('productBatch', 'purchaseReturnProduct.purchaseReturnProductBatchID = productBatch.productBatchID', array('productBatchID', 'productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity'), 'left');
        $select->join('productSerial', 'purchaseReturnProduct.purchaseReturnProductSerialID = productSerial.productSerialID', array('productSerialID', 'productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate'), 'left');
        $select->join('purchaseReturnProductTax', 'purchaseReturnProduct.purchaseReturnProductID = purchaseReturnProductTax.purchaseReturnProductID', array('purchaseReturnTaxID', 'purchaseReturnTaxPrecentage', 'purchaseReturnTaxAmount'), 'left');
        $select->join('tax', 'purchaseReturnProductTax.purchaseReturnTaxID = tax.id', array('taxName'), 'left');
        $select->join('grn', 'purchaseReturn.purchaseReturnGrnID = grn.grnID', array('*'), 'left');
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID', array('supplierCode', 'supplierTitle', 'supplierName', 'supplierAddress', 'supplierEmail', 'supplierOutstandingBalance', 'supplierCreditBalance'), 'left');
        $select->join('location', 'grn.grnRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        if (is_null($grnIds)) {
            $select->where(array('purchaseReturn.purchaseReturnGrnID' => $grnID));
        } else {
            $select->where->in('purchaseReturn.purchaseReturnGrnID', $grnIds);
        }
        $select->where(array('productUomConversion' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * This function return pr data
     * @param type $prID
     */
    public function getPurchaseReturnDetailsByPrID($prID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->join('purchaseReturnProduct', 'purchaseReturn.purchaseReturnID = purchaseReturnProduct.purchaseReturnID', array('*'));
        $select->join('locationProduct', 'purchaseReturnProduct.purchaseReturnLocationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('productBatch', 'purchaseReturnProduct.purchaseReturnProductBatchID = productBatch.productBatchID', array('productBatchID', 'productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity'), 'left');
        $select->join('productSerial', 'purchaseReturnProduct.purchaseReturnProductSerialID = productSerial.productSerialID', array('productSerialID', 'productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate'), 'left');
        $select->join('purchaseReturnProductTax', 'purchaseReturnProduct.purchaseReturnProductID = purchaseReturnProductTax.purchaseReturnProductID', array('purchaseReturnTaxID', 'purchaseReturnTaxPrecentage', 'purchaseReturnTaxAmount'), 'left');
        $select->join('tax', 'purchaseReturnProductTax.purchaseReturnTaxID = tax.id', array('taxName'), 'left');
        $select->join('grn', 'purchaseReturn.purchaseReturnGrnID = grn.grnID', array('grnRetrieveLocation', 'grnSupplierID'),'left');
        $select->join(['grnLoaction' => 'location'], 'grn.grnRetrieveLocation = grnLoaction.locationID', array('gLocationName' => new Expression('grnLoaction.locationName'),'gLocationCode' => new Expression('grnLoaction.locationCode'),'gLocationID' => new Expression('grnLoaction.locationID')),'left');
        $select->join(['prLoaction' => 'location'], 'purchaseReturn.prLocationID = prLoaction.locationID', array('pLocationName' => new Expression('prLoaction.locationName'),'pLocationCode' => new Expression('prLoaction.locationCode'),'pLocationID' => new Expression('prLoaction.locationID')),'left');
        $select->join(['grnSupplier' => 'supplier'], 'grn.grnSupplierID = grnSupplier.supplierID', array('gSupplierName' => new Expression('grnSupplier.supplierName'),'gSupplierTitle' => new Expression('grnSupplier.supplierTitle')),'left');
        $select->join(['prSupplier' => 'supplier'], 'purchaseReturn.prSupplierID = prSupplier.supplierID', array('prSupplierName' => new Expression('prSupplier.supplierName'),'prSupplierTitle' => new Expression('prSupplier.supplierTitle')),'left');
        $select->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->where(array('purchaseReturn.purchaseReturnID' => $prID));
        $select->where(array('productUomConversion' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function savePr(PurchaseReturn $pr)
    {
        $data = array(
            'purchaseReturnCode' => $pr->purchaseReturnCode,
            'purchaseReturnGrnID' => $pr->purchaseReturnGrnID,
            'purchaseReturnDate' => $pr->purchaseReturnDate,
            'purchaseReturnComment' => $pr->purchaseReturnComment,
            'purchaseReturnShowTax' => $pr->purchaseReturnShowTax,
            'purchaseReturnTotal' => $pr->purchaseReturnTotal,
            'purchaseReturnStatus' => $pr->purchaseReturnStatus,
            'purchaseReturnEntityID' => $pr->purchaseReturnEntityID,
            'directReturnFlag' => $pr->directReturnFlag,
            'prSupplierID' => $pr->prSupplierID,
            'prLocationID' => $pr->prLocationID,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function getStockValueData($pIds, $locIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->columns(array('*'));
        $select->join('purchaseReturnProduct', 'purchaseReturn.purchaseReturnID = purchaseReturnProduct.purchaseReturnID', array('*'));
        $select->join('locationProduct', 'purchaseReturnProduct.purchaseReturnLocationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('location', 'location.locationID = locationProduct.locationID', array('locationName', 'locationID', 'locationCode'), 'left');
        $select->order('product.productID', 'grnProduct.grnID');
        $select->group(array('purchaseReturn.purchaseReturnID', 'purchaseReturnLocationProductID'));
        $select->where(array('purchaseReturnProductReturnedQty IS NOT NULL', 'purchaseReturnProductPrice IS NOT NULL'));
        if ($locIds != NULL) {
            $select->where->in('locationProduct.locationID', $locIds);
        }
        if ($pIds != NULL) {
            $select->where->in('product.productID', $pIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function searchPurchaseReturnsFroDropdown($searchPRKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->columns(array('purchaseReturnID', 'purchaseReturnCode'));
        $select->order('purchaseReturnID DESC');
        $select->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('deleted'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where->like('purchaseReturnCode', '%' . $searchPRKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPurchaseReturnsBySearch($prID = false, $supplierID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->columns(array('*'));
        $select->order('purchaseReturnID DESC');
        $select->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('deleted'));
        $select->join('grn', 'purchaseReturn.purchaseReturnGrnID = grn.grnID', array('grnRetrieveLocation', 'grnSupplierID'),'left');
        $select->join(['grnLoaction' => 'location'], 'grn.grnRetrieveLocation = grnLoaction.locationID', array('gLocationName' => new Expression('grnLoaction.locationName'),'gLocationCode' => new Expression('grnLoaction.locationCode'),'gLocationID' => new Expression('grnLoaction.locationID')),'left');
        $select->join(['prLoaction' => 'location'], 'purchaseReturn.prLocationID = prLoaction.locationID', array('pLocationName' => new Expression('prLoaction.locationName'),'pLocationCode' => new Expression('prLoaction.locationCode'),'pLocationID' => new Expression('prLoaction.locationID')),'left');
        $select->join(['grnSupplier' => 'supplier'], 'grn.grnSupplierID = grnSupplier.supplierID', array('gSupplierName' => new Expression('grnSupplier.supplierName'),'gSupplierTitle' => new Expression('grnSupplier.supplierTitle')),'left');
        $select->join(['prSupplier' => 'supplier'], 'purchaseReturn.prSupplierID = prSupplier.supplierID', array('prSupplierName' => new Expression('prSupplier.supplierName'),'prSupplierTitle' => new Expression('prSupplier.supplierTitle')),'left');
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        if ($prID) {
            $select->where(array('purchaseReturnID' => $prID));
        }
        if ($supplierID) {
            $select->where(new PredicateSet(array(new Operator('prSupplier.supplierID', '=', $supplierID), new Operator('grnSupplier.supplierID', '=',$supplierID)), PredicateSet::OP_OR));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPurchaseReturnByCode($purchaseReturnCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->join('grn', 'grn.grnID = purchaseReturn.purchaseReturnGrnID', array('grnCode'));
        $select->join('location', 'location.locationID = grn.grnRetrieveLocation', array('locationName', 'locationCode'));
        $select->where(array('purchaseReturnCode' => $purchaseReturnCode));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getPurchaseReturnById($purchaseReturnId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->join('grn', 'grn.grnID = purchaseReturn.purchaseReturnGrnID', array('grnCode'));
        $select->join('location', 'location.locationID = grn.grnRetrieveLocation', array('locationName', 'locationCode'));
        $select->where(array('purchaseReturnID' => $purchaseReturnId));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getPurchaseReturnDataById($purchaseReturnId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->join('location', 'location.locationID = purchaseReturn.prLocationID', array('locationName', 'locationCode'));
        $select->where(array('purchaseReturnID' => $purchaseReturnId));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getPurchaseReturnByIdForJE($purchaseReturnId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->join('grn', 'grn.grnID = purchaseReturn.purchaseReturnGrnID', array('grnCode'),'left');
        $select->join('location', 'location.locationID = grn.grnRetrieveLocation', array('locationName', 'locationCode'),'left');
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID',array("*"),'left');
        $select->where(array('purchaseReturnID' => $purchaseReturnId));
        $select->where->notEqualTo('purchaseReturn.purchaseReturnStatus', 5);
        $select->where->notEqualTo('purchaseReturn.purchaseReturnStatus', 10);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }
    
    /**
     * Get grn product return detals
     * @param string $grnId
     * @param string $locationProductId
     * @return mixed
     */
    public function getGrnProductReturnDetailsByGrnIdAndLocationProductId($grnId, $locationProductId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseReturn')
               ->columns(['*'])
               ->join('purchaseReturnProduct', 'purchaseReturn.purchaseReturnID = purchaseReturnProduct.purchaseReturnID', ['purchaseReturnProductID' ,'purchaseReturnLocationProductID','purchaseReturnProductReturnedQty','purchaseReturnProductTotal',
                   'purchaseReturnProductReturnedQty0' => new Expression('SUM(purchaseReturnProductReturnedQty)'), 
                   'purchaseReturnProductTotal0' => new Expression('SUM(purchaseReturnProductTotal)')], 'left')
               ->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', ['deleted'], 'left')
               ->where->equalTo('entity.deleted', 0)
               ->where->equalTo('purchaseReturn.purchaseReturnGrnID', $grnId)
               ->where->equalTo('purchaseReturnProduct.purchaseReturnLocationProductID', $locationProductId);
        $select->group(array('purchaseReturnProduct.purchaseReturnLocationProductID','purchaseReturn.purchaseReturnID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * Get grn product tax return detals
     * @param string $grnId
     * @param string $locationProductId
     * @return mixed
     */
    public function getGrnProductTaxByGrnIdAndLocationProductId($grnId, $locationProductId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseReturn')
               ->columns(['purchaseReturnID','purchaseReturnGrnID'])
               ->join('purchaseReturnProduct', 'purchaseReturn.purchaseReturnID = purchaseReturnProduct.purchaseReturnID', ['purchaseReturnProductID', 'purchaseReturnLocationProductID'], 'left')
               ->join('purchaseReturnProductTax', 'purchaseReturnProduct.purchaseReturnProductID = purchaseReturnProductTax.purchaseReturnProductID', [
                   'purchaseReturnTaxAmount', 'purchaseReturnTaxID'], 'left')
               ->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', ['deleted'], 'left')
               ->where->equalTo('entity.deleted', 0)
               ->where->equalTo('purchaseReturn.purchaseReturnGrnID', $grnId)
               ->where->equalTo('purchaseReturnProduct.purchaseReturnLocationProductID', $locationProductId);
        $select->group(array('purchaseReturnProduct.purchaseReturnLocationProductID','purchaseReturnProductTax.purchaseReturnTaxID','purchaseReturn.purchaseReturnID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
    * get all purchase return details by given grn supplier
    *
    **/
    public function getPurchaseReturnBySupplierIDs($supplierID = [], $status, $fromdate = null, $todate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn')
                ->columns(array('*'))
                ->join('entity', ' purchaseReturn.purchaseReturnEntityID = entity.entityID', array('deleted','createdTimeStamp'), 'left')
                ->join('grn', 'purchaseReturn.purchaseReturnGrnID = grn.grnID', array('grnCode','grnSupplierID'), 'left')
                ->join("status", "purchaseReturn.purchaseReturnStatus = status.statusID", array("*"), "left")
                ->where(array('deleted' => 0 ));
        $select->where->in('grn.grnSupplierID', $supplierID);
        $select->where->in('purchaseReturn.purchaseReturnStatus', $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('purchaseReturn.purchaseReturnDate', $fromdate, $todate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getGrnProductReturnDetailsByGrnIdAndGrnProductId($grnId, $grnProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseReturn')
               ->columns(['*'])
               ->join('purchaseReturnProduct','purchaseReturn.purchaseReturnID = purchaseReturnProduct.purchaseReturnID', array('*'), 'left')
               ->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', ['deleted'], 'left')
               ->where->equalTo('entity.deleted', 0)
               ->where->equalTo('purchaseReturn.purchaseReturnGrnID', $grnId)
               ->where->equalTo('purchaseReturnProduct.purchaseReturnProductGrnProductID', $grnProductID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    // get po related purchase return
    public function getPurchaseReturnDataByPoId($poId = null, $grnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->columns(array('*'));
        $select->join('entity','purchaseReturn.purchaseReturnEntityID = entity.entityID',array('*'),'left');
        if (!is_null($poId)) {
            $select->join('grnProduct','purchaseReturn.purchaseReturnGrnID = grnProduct.grnID',array('grnID'),'left');
            $select->where(array('grnProduct.grnProductDocumentId' => $poId, 'grnProduct.grnProductDocumentTypeId' => 9));
        }
        if (!is_null($grnID)) {
            $select->where(array('purchaseReturn.purchaseReturnGrnID' => $grnID));
        }
        $select->group("purchaseReturn.purchaseReturnID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get pr basic details with time stamp by pr id 
    public function getPRDetailsByPrId($prID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseReturn.purchaseReturnID' => $prID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;
    }

    // get pr basic details with time stamp related to grn by debit note id 
    public function getPRDetailsByDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->columns(array('*'));
        $select->join('grn', 'purchaseReturn.purchaseReturnGrnID = grn.grnID', array('*'),'left');
        $select->join('purchaseInvoice', 'grn.grnID = purchaseInvoice.purchaseInvoiceGrnID', array('*'),'left');
        $select->join('debitNote','purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID',array('*'),'left');
        $select->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('debitNote.debitNoteID' => $debitNoteID));
        $select->group("debitNote.debitNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getDirectPurchaseReturnById($purchaseReturnId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseReturn');
        $select->join('location', 'location.locationID = purchaseReturn.prLocationID', array('locationName', 'locationCode'),'left');
        $select->join('supplier', 'purchaseReturn.prSupplierID = supplier.supplierID',array("*"),'left');
        $select->where(array('purchaseReturnID' => $purchaseReturnId));
        $select->where->notEqualTo('purchaseReturn.purchaseReturnStatus', 5);
        $select->where->notEqualTo('purchaseReturn.purchaseReturnStatus', 10);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getTotalPurchaseReturnByDateRangeAndLocationID($startDate,$endDate, $locationID, $supplierFlag = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseReturn")
                ->columns(array('prTotal' => new Expression('SUM(purchaseReturnTotal)')))
                ->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('deleted'), 'left')
                ->join('supplier', 'purchaseReturn.prSupplierID = supplier.supplierID',array("supplierID"),'left')
                ->where->notEqualTo("purchaseReturn.purchaseReturnStatus", 10);
        $select->where->notEqualTo("purchaseReturn.purchaseReturnStatus", 5);
        $select->where->between('purchaseReturnDate', $startDate, $endDate);
        $select->where(array('entity.deleted' => 0, 'purchaseReturn.prLocationID' => $locationID));

        if ($supplierFlag) {
            $select->group('prSupplierID');    
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        if ($supplierFlag) {
            return $results;   
        } else {
            return $results->current();   
        }
    }

    public function getTotalPurchaseReturnByDateRangeAndLocationIDDashboard($startDate,$endDate, $locationID, $supplierFlag = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseReturn")
                ->columns(array('prTotal' => new Expression('SUM(purchaseReturnTotal)')))
                ->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('deleted'), 'left')
                // ->join('supplier', 'purchaseReturn.prSupplierID = supplier.supplierID',array("*"),'left')
                ->where->notEqualTo("purchaseReturn.purchaseReturnStatus", 10);
        $select->where->notEqualTo("purchaseReturn.purchaseReturnStatus", 5);
        $select->where->between('purchaseReturnDate', $startDate, $endDate);
        $select->where(array('entity.deleted' => 0, 'purchaseReturn.prLocationID' => $locationID));

        // if ($supplierFlag) {
        //     $select->group('prSupplierID');    
        // }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        // if ($supplierFlag) {
        //     return $results;   
        // } else {
            return $results->current();   
        // }
    }
}
