<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the POProductTax model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseOrderProductTax implements InputFilterAwareInterface
{

    public $purchaseOrderProductTaxID;
    public $purchaseOrderID;
    public $purchaseOrderProductID;
    public $purchaseOrderTaxID;
    public $purchaseOrderTaxPrecentage;
    public $purchaseOrderTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseOrderProductTaxID = (!empty($data['purchaseOrderProductTaxID'])) ? $data['purchaseOrderProductTaxID'] : null;
        $this->purchaseOrderID = (!empty($data['purchaseOrderID'])) ? $data['purchaseOrderID'] : null;
        $this->purchaseOrderProductID = (!empty($data['purchaseOrderProductID'])) ? $data['purchaseOrderProductID'] : null;
        $this->purchaseOrderTaxID = (!empty($data['purchaseOrderTaxID'])) ? $data['purchaseOrderTaxID'] : null;
        $this->purchaseOrderTaxPrecentage = (!empty($data['purchaseOrderTaxPrecentage'])) ? $data['purchaseOrderTaxPrecentage'] : null;
        $this->purchaseOrderTaxAmount = (!empty($data['purchaseOrderTaxAmount'])) ? $data['purchaseOrderTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

