<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BomSubItem
{

    public $bomSubItemID;
    public $bomID;
    public $productID;
    public $bomSubItemQuantity;
    public $bomSubItemUnitPrice;
    public $bomSubItemCost;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->bomSubItemID = (!empty($data['bomSubItemID'])) ? $data['bomSubItemID'] : null;
        $this->bomID = (!empty($data['bomID'])) ? $data['bomID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->bomSubItemQuantity = (!empty($data['bomSubItemQuantity'])) ? $data['bomSubItemQuantity'] : 0;
        $this->bomSubItemUnitPrice = (!empty($data['bomSubItemUnitPrice'])) ? $data['bomSubItemUnitPrice'] : 0;
        $this->bomSubItemCost = (!empty($data['bomSubItemCost'])) ? $data['bomSubItemCost'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
