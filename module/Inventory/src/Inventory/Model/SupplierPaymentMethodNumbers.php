<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains supplier payments method Numbers Table Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class SupplierPaymentMethodNumbers
{

    public $outGoingPaymentMethodsNumbersID;
    public $outGoingPaymentID;
    public $outGoingPaymentMethodID;
    public $outGoingPaymentMethodFinanceAccountID;
    public $outGoingPaymentMethodReferenceNumber;
    public $outGoingPaymentMethodChequeReference;
    public $outGoingPaymentMethodBank;
    public $outGoingPaymentMethodCardID;
    public $outGoingPaymentMethodBankTransferBankId;
    public $outGoingPaymentMethodBankTransferAccountId;
    public $outGoingPaymentMethodBankTransferSupplierBankName;
    public $outGoingPaymentMethodBankTransferSupplierAccountNumber;
    public $outGoingPaymentMethodChequeBankAccountID;
    public $outGoingPaymentMethodPaidAmount;
    
    /**
     * @var int cheque reconciliation status.
     * 
     * 0 - not reconcile
     * 1 - reconciled
     */
    public $outGoingPaymentMethodReconciliationStatus;
    
    /**
     * @var int post-dated cheque status.
     * 
     * 0 - not post-dated cheque
     * 1 - post-dated cheque
     */
    public $postdatedChequeStatus;
    
    /**
     * @var Date post-dated cheque date.
     */
    public $postdatedChequeDate;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->outGoingPaymentMethodsNumbersID = (!empty($data['outGoingPaymentMethodsNumbersID'])) ? $data['outGoingPaymentMethodsNumbersID'] : null;
        $this->outGoingPaymentID = (!empty($data['outGoingPaymentID'])) ? $data['outGoingPaymentID'] : null;
        $this->outGoingPaymentMethodID = (!empty($data['outGoingPaymentMethodID'])) ? $data['outGoingPaymentMethodID'] : null;
        $this->outGoingPaymentMethodFinanceAccountID = (!empty($data['outGoingPaymentMethodFinanceAccountID'])) ? $data['outGoingPaymentMethodFinanceAccountID'] : null;
        $this->outGoingPaymentMethodReferenceNumber = (!empty($data['outGoingPaymentMethodReferenceNumber'])) ? $data['outGoingPaymentMethodReferenceNumber'] : null;
        $this->outGoingPaymentMethodChequeReference = (!empty($data['outGoingPaymentMethodChequeReference'])) ? $data['outGoingPaymentMethodChequeReference'] : null;
        $this->outGoingPaymentMethodBank = (!empty($data['outGoingPaymentMethodBank'])) ? $data['outGoingPaymentMethodBank'] : null;
        $this->outGoingPaymentMethodCardID = (!empty($data['outGoingPaymentMethodCardID'])) ? $data['outGoingPaymentMethodCardID'] : null;
        $this->outGoingPaymentMethodBankTransferBankId = (!empty($data['outGoingPaymentMethodBankTransferBankId'])) ? $data['outGoingPaymentMethodBankTransferBankId'] : null;
        $this->outGoingPaymentMethodBankTransferAccountId = (!empty($data['outGoingPaymentMethodBankTransferAccountId'])) ? $data['outGoingPaymentMethodBankTransferAccountId'] : null;
        $this->outGoingPaymentMethodBankTransferSupplierBankName = (!empty($data['outGoingPaymentMethodBankTransferSupplierBankName'])) ? $data['outGoingPaymentMethodBankTransferSupplierBankName'] : null;
        $this->outGoingPaymentMethodBankTransferSupplierAccountNumber = (!empty($data['outGoingPaymentMethodBankTransferSupplierAccountNumber'])) ? $data['outGoingPaymentMethodBankTransferSupplierAccountNumber'] : null;
        $this->outGoingPaymentMethodChequeBankAccountID = (!empty($data['outGoingPaymentMethodChequeBankAccountID'])) ? $data['outGoingPaymentMethodChequeBankAccountID'] : null;
        $this->outGoingPaymentMethodReconciliationStatus = (!empty($data['outGoingPaymentMethodReconciliationStatus'])) ? $data['outGoingPaymentMethodReconciliationStatus'] : null;
        $this->postdatedChequeStatus = (!empty($data['postdatedChequeStatus'])) ? $data['postdatedChequeStatus'] : 0;
        $this->postdatedChequeDate = (!empty($data['postdatedChequeDate'])) ? $data['postdatedChequeDate'] : null;
        $this->outGoingPaymentMethodPaidAmount = (!empty($data['outGoingPaymentMethodPaidAmount'])) ? $data['outGoingPaymentMethodPaidAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
