<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\ExpiryAlertNotification;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class FreeIssueItemsTable 
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveFreeIssueItems($data)
    {
        $dataSet = array(
            'freeIssueDetailsID' => $data->freeIssueDetailsID,
            'productID' => $data->productID,
            'quantity' => $data->quantity,
            'selectedUom' => $data->selectedUom,
        );

        try {
            $this->tableGateway->insert($dataSet);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        } catch (\Exception $exc) {
            echo $exc;
            return false;
        }
    }

    public function getRelatedFreeIssuesByFreeIssueDetailsID($freeIssueDetailsID) {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('freeIssueItems')
                    ->columns(array('*'))
                    ->join('product', 'freeIssueItems.productID = product.productID', array('productName', 'productCode'), 'left')
                    ->join('uom', 'freeIssueItems.selectedUom = uom.uomID', array('*'), 'left')
                    ->where(array('freeIssueItems.freeIssueDetailsID' => $freeIssueDetailsID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }

            $results = [];
            foreach ($rowset as $key => $value) {
                $results[] = $value;
            }

            return $results;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function deleteFreeIssueItems($freeIssueDetailsID)
    {
        try {
            $this->tableGateway->delete(array('freeIssueDetailsID' => $freeIssueDetailsID));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }
}
