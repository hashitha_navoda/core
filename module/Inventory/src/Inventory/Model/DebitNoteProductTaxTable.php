<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Debit Note Product Tax Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DebitNoteProductTaxTable
{

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDebitNoteProductTax(DebitNoteProductTax $debitNoteProductTax)
    {
        $data = array(
            'debitNoteProductID' => $debitNoteProductTax->debitNoteProductID,
            'taxID' => $debitNoteProductTax->taxID,
            'debitNoteProductTaxPercentage' => $debitNoteProductTax->debitNoteProductTaxPercentage,
            'debitNoteProductTaxAmount' => $debitNoteProductTax->debitNoteProductTaxAmount,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDebitNoteProductTaxByDebitNoteProductID($debitNoteProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("debitNoteProductTax")
                ->columns(array("*"))
                ->join("tax", "debitNoteProductTax.taxID = tax.id", array("*"), "left")
                ->where(array("debitNoteProductID" => $debitNoteProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

}
