<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CompoundCostGrn
{

    public $compoundCostGrnID;
    public $compoundCostTemplateID;
    public $grnID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->compoundCostGrnID = (!empty($data['compoundCostGrnID'])) ? $data['compoundCostGrnID'] : null;
        $this->compoundCostTemplateID = (!empty($data['compoundCostTemplateID'])) ? $data['compoundCostTemplateID'] : 0;
        $this->grnID = (!empty($data['grnID'])) ? $data['grnID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}