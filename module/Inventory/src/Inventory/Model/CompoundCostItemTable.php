<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\CompoundCostItem;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class CompoundCostItemTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCompoundItem(CompoundCostItem $compoundCostItem)
    {
        try {
            $data = array(
                'productID' => $compoundCostItem->productID,
                'locationProductID' => $compoundCostItem->locationProductID,
                'compoundCostItemQty' => $compoundCostItem->compoundCostItemQty,
                'compoundCostItemPurchasePrice' => $compoundCostItem->compoundCostItemPurchasePrice,
                'compoundCostItemCostAmount' => $compoundCostItem->compoundCostItemCostAmount,
                'compoundCostItemTotalUnitCost' => $compoundCostItem->compoundCostItemTotalUnitCost,
                'compoundCostTemplateID' => $compoundCostItem->compoundCostTemplateID,
                'compoundCostItemWiseTotalCost' => $compoundCostItem->compoundCostItemWiseTotalCost,
                'supplierID' => $compoundCostItem->supplierID,
            );
            $this->tableGateway->insert($data);
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;

        } catch (\Exception $exc) {
            error_log($exc);

        }
    }

}