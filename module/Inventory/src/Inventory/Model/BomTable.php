<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\Bom;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class BomTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveBomDetails(Bom $bom)
    {
        $data = array(
            'bomCode' => $bom->bomCode,
            'bomTotalCost' => $bom->bomTotalCost,
            'bomDiscription' => $bom->bomDiscription,
            'isJEPostSubItemWise' => $bom->isJEPostSubItemWise,
        );
        try {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        } catch (\Exception $exc) {
            echo $exc;
            return false;
        }
    }

    public function updateBomDetails(Bom $bom)
    {
        $data = array(
            'productID' => $bom->productID,
            'bomItemType' => $bom->bomItemType,
        );
        try {
            $this->tableGateway->update($data, array('bomID' => $bom->bomID));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function updateBomDetailsByBomID(Bom $bom)
    {
        $data = array(
            'bomCode' => $bom->bomCode,
            'bomTotalCost' => $bom->bomTotalCost,
            'bomDiscription' => $bom->bomDiscription,
        );
        try {
            $this->tableGateway->update($data, array('bomID' => $bom->bomID));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bom');
        $select->join('product', 'bom.productID=  product.productID', array("productName", "productDescription"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->join('productUom', 'product.productID=  productUom.productID', array("uomID"), "left");
        $select->join('uom', 'productUom.uomID=  uom.uomID', array("uomAbbr", "uomName"), "left");
        $select->order('bomID DESC');
        $select->where(array('entity.deleted' => 0, 'productUom.productUomBase' => 1));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);

            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getBomDataByBomID($bomID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bom');
        $select->join('product', 'bom.productID=  product.productID', array("productName", "productDescription"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->join('productUom', 'product.productID=  productUom.productID', array("uomID"), "left");
        $select->join('uom', 'productUom.uomID=  uom.uomID', array("uomName"), "left");
        $select->where(array('bom.bomID' => $bomID, 'productUom.productUomBase' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function searchBomDetailsForDropDown($locationID, $searchKey, $isAdList = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bom');
        $select->columns(array('bomCode', 'bomID'));
        $select->join('product', 'bom.productID=  product.productID', array("productName"), "left");
        $select->join('locationProduct', 'bom.productID=  locationProduct.productID', array("locationID"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(new PredicateSet(array(new Operator('bom.bomCode', 'like', '%' . $searchKey . '%'), new Operator('product.productName', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('locationProduct.locationID', '=', $locationID))));

        if ($isAdList) {
            $select->where(new PredicateSet(array(new Operator('bom.bomItemType', '=', 1))));
        }

        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function searchSubItemBaseBomDetailsForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bom');
        $select->columns(array('bomCode', 'bomID'));
        $select->join('product', 'bom.productID=  product.productID', array("productName"), "left");
        $select->join('locationProduct', 'bom.productID=  locationProduct.productID', array("locationID"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(new PredicateSet(array(new Operator('bom.bomCode', 'like', '%' . $searchKey . '%'), new Operator('product.productName', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('bom.bomItemType', '=', 2))));
        // $select->where(new PredicateSet(array(new Operator('locationProduct.locationID', '=', $locationID))));

        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getSubItemBasedAllBomIds()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bom');
        $select->columns(array('bomID'));
        $select->where(new PredicateSet(array(new Operator('bom.bomItemType', '=', 2))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getBomDataAndProductDataByBomIDAndLocationID($bomID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bom');
        $select->join('product', 'bom.productID=  product.productID', array("productName", "productID"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->join('locationProduct', 'product.productID=  locationProduct.productID', array("locationProductQuantity", "locationID", "locationProductID"), "left");
        $select->join('productUom', 'product.productID=  productUom.productID', array("uomID", "productUomConversion", "productUomDisplay"), "left");
        $select->join('uom', 'productUom.uomID=  uom.uomID', array("uomAbbr", "uomDecimalPlace"), "left");
        $select->where(array('bom.bomID' => $bomID, 'locationProduct.locationID' => $locationID, 'entity.deleted' => 0, 'productUom.productUomDisplay' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function bomSearchByKey($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bom');
        $select->join('product', 'bom.productID=  product.productID', array("productName", "productDescription"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->join('productUom', 'product.productID=  productUom.productID', array("uomID"), "left");
        $select->join('uom', 'productUom.uomID=  uom.uomID', array("uomName"), "left");
        $select->where(new PredicateSet(array(new Operator('bom.bomCode', 'like', '%' . $searchKey . '%'), new Operator('product.productName', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $select->where(new PredicateSet(array(new Operator('productUom.productUomBase', '=', 1))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getBomDataByProductID($productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bom');
        $select->where(array('bom.productID' => $productID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
