<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PurchaseInvoiceTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param Paginator true or false
     * @return paginator object if true, else resultset
     */
    public function getPurchaseInvoices($paginated = FALSE, $userActiveLocationID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->order('purchaseInvoiceID DESC');
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseInvoice.purchaseInvoiceRetrieveLocation = location.locationID', array('locationName', 'locationID'));
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        // $select->where(array('deleted' => '0'));
        $select->where->notEqualTo('purchaseInvoice.status',10);
        if ($userActiveLocationID != NULL) {
            $select->where(array('locationID' => $userActiveLocationID));
        }

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param String $piSearchKey
     * @return ResultSet
     */
    public function getPisforSearch($piSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $piSearchKey . '\' in purchaseInvoice.purchaseInvoiceCode )>0,POSITION(\'' . $piSearchKey . '\' in purchaseInvoice.purchaseInvoiceCode), 9999),'
                    . 'IF(POSITION(\'' . $piSearchKey . '\' in supplier.supplierName )>0,POSITION(\'' . $piSearchKey . '\' in supplier.supplierName), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(purchaseInvoice.purchaseInvoiceCode ), CHAR_LENGTH(supplier.supplierName )) '),
            '*',
        ));
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseInvoice.purchaseInvoiceRetrieveLocation = location.locationID', array('locationName'));
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('purchaseInvoice.purchaseInvoiceCode', 'like', '%' . $piSearchKey . '%'), new Operator('supplier.supplierName', 'like', '%' . $piSearchKey . '%')), PredicateSet::OP_OR));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPIsByPICode($piCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->join('purchaseInvoiceProduct', 'purchaseInvoice.purchaseInvoiceID = purchaseInvoiceProduct.purchaseInvoiceID', array('*'));
        $select->join('locationProduct', 'purchaseInvoiceProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'));
        $select->join('productBatch', 'purchaseInvoiceProduct.productBatchID = productBatch.productBatchID', array('productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity'), 'left');
        $select->join('productSerial', 'purchaseInvoiceProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate'), 'left');
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierCode', 'supplierName'), 'left');
        $select->join('location', 'purchaseInvoice.purchaseInvoiceRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('purchaseInvoiceProductTax', 'purchaseInvoiceProduct.purchaseInvoiceProductID = purchaseInvoiceProductTax.purchaseInvoiceProductID', array('purchaseInvoiceTaxID', 'purchaseInvoiceTaxPrecentage', 'purchaseInvoiceTaxAmount'), 'left');
        $select->join('tax', 'purchaseInvoiceProductTax.purchaseInvoiceTaxID = tax.id', array('taxName'), 'left');
        $select->where(array('purchaseInvoiceCode' => $piCode));
        $select->where(array('productUomBase' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPIsByPIID($piID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->join('purchaseInvoiceProduct', 'purchaseInvoice.purchaseInvoiceID = purchaseInvoiceProduct.purchaseInvoiceID', array('*'));
        $select->join('locationProduct', 'purchaseInvoiceProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('productBatch', 'purchaseInvoiceProduct.productBatchID = productBatch.productBatchID', array('productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity'), 'left');
        $select->join('productSerial', 'purchaseInvoiceProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate','productSerialWarrantyPeriodType'), 'left');
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierCode', 'supplierName', 'supplierEmail', 'supplierAddress', 'supplierTitle'), 'left');
        $select->join('location', 'purchaseInvoice.purchaseInvoiceRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('purchaseInvoiceProductTax', 'purchaseInvoiceProduct.purchaseInvoiceProductID = purchaseInvoiceProductTax.purchaseInvoiceProductID', array('purchaseInvoiceTaxID', 'purchaseInvoiceTaxPrecentage', 'purchaseInvoiceTaxAmount'), 'left');
        $select->join('tax', 'purchaseInvoiceProductTax.purchaseInvoiceTaxID = tax.id', array('taxName','taxSuspendable'), 'left');
        $select->join('paymentTerm', 'purchaseInvoice.paymentTermID = paymentTerm.paymentTermID', array('paymentTermName'), 'left');
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->join('grnProduct', 'grnProduct.grnProductID = purchaseInvoiceProduct.purchaseInvoiceProductDocumentID', array('grnID'), 'left');
        $select->where(array('purchaseInvoice.purchaseInvoiceID' => $piID));
        $select->where(array('productUomBase' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPIsByPIIDForSupReport($piID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->join('purchaseInvoiceProduct', 'purchaseInvoice.purchaseInvoiceID = purchaseInvoiceProduct.purchaseInvoiceID', array('*'));
        $select->join('locationProduct', 'purchaseInvoiceProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('productBatch', 'purchaseInvoiceProduct.productBatchID = productBatch.productBatchID', array('productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity'), 'left');
        $select->join('productSerial', 'purchaseInvoiceProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate'), 'left');
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierCode', 'supplierName', 'supplierEmail', 'supplierAddress', 'supplierTitle'), 'left');
        $select->join('location', 'purchaseInvoice.purchaseInvoiceRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('purchaseInvoiceProductTax', 'purchaseInvoiceProduct.purchaseInvoiceProductID = purchaseInvoiceProductTax.purchaseInvoiceProductID', array('purchaseInvoiceTaxID', 'purchaseInvoiceTaxPrecentage', 'purchaseInvoiceTaxAmount'), 'left');
        $select->join('tax', 'purchaseInvoiceProductTax.purchaseInvoiceTaxID = tax.id', array('taxName','taxSuspendable'), 'left');
        $select->join('paymentTerm', 'purchaseInvoice.paymentTermID = paymentTerm.paymentTermID', array('paymentTermName'), 'left');
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->join('grnProduct', 'grnProduct.grnProductID = purchaseInvoiceProduct.purchaseInvoiceProductDocumentID', array('grnID'), 'left');
        $select->where(array('purchaseInvoice.purchaseInvoiceID' => $piID));
        $select->where(array('productUomBase' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultSet = [];

        if (sizeof($result) > 0) {

            foreach ($result as $key => $value) {
                $resultSet[] = $value;
            }
        }
        return $resultSet;
    }

    public function savePi(PurchaseInvoice $pi)
    { 
        $data = array(
            'purchaseInvoiceCode' => $pi->purchaseInvoiceCode,
            'purchaseInvoiceSupplierID' => $pi->purchaseInvoiceSupplierID,
            'purchaseInvoiceSupplierReference' => $pi->purchaseInvoiceSupplierReference,
            'purchaseInvoiceRetrieveLocation' => $pi->purchaseInvoiceRetrieveLocation,
            'purchaseInvoicePaymentDueDate' => $pi->purchaseInvoicePaymentDueDate,
            'purchaseInvoiceIssueDate' => $pi->purchaseInvoiceIssueDate,
            'purchaseInvoicePoID' => $pi->purchaseInvoicePoID,
            'purchaseInvoiceGrnID' => $pi->purchaseInvoiceGrnID,
            'purchaseInvoiceComment' => $pi->purchaseInvoiceComment,
            'purchaseInvoiceDeliveryCharge' => $pi->purchaseInvoiceDeliveryCharge,
            'purchaseInvoiceShowTax' => $pi->purchaseInvoiceShowTax,
            'status' => $pi->status,
            'purchaseInvoiceTotal' => $pi->purchaseInvoiceTotal,
            'purchaseInvoicePayedAmount' => $pi->purchaseInvoicePayedAmount,
            'paymentTermID' => $pi->paymentTermID,
            'entityID' => $pi->entityID,
            'purchaseInvoiceWiseTotalDiscount' => $pi->purchaseInvoiceWiseTotalDiscount,
            'purchaseInvoiceWiseTotalDiscountType' => $pi->purchaseInvoiceWiseTotalDiscountType,
            'purchaseInvoiceWiseTotalDiscountRate' => $pi->purchaseInvoiceWiseTotalDiscountRate,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function getOverduePurchaseInvoices($locationID)
    {
        $overdueStatusID = 6;
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("purchaseInvoiceSupplierID", "purchaseInvoiceID", "purchaseInvoiceCode", "purchaseInvoiceIssueDate", "purchaseInvoicePaymentDueDate", "purchaseInvoiceRetrieveLocation"));
        $select->from('purchaseInvoice');
        $select->join("supplier", " supplier.supplierID = purchaseInvoice.purchaseInvoiceSupplierID", array("supplierName"), "left outer");
//        $select->join("logger", "logger.refID = invoice.id", array("refID", "userID"), "left outer");
//        $select->join("user", "logger.userID = user.userID", array("firstName", "email1"), "left outer");
        // TODO - check status ID
//        $select->where->lessThanOrEqualTo("purchaseInvoicePaymentDueDate", new Expression('CURDATE()'))->AND->equalTo('purchaseInvoiceRetrieveLocation', $locationID);
        $select->where->equalTo('purchaseInvoiceRetrieveLocation', $locationID)->AND->equalTo('status', $overdueStatusID);
        $select->order("purchaseInvoicePaymentDueDate ASC");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @return type
     */
    public function getOverdueinvoiceIDs($locationID)
    {
        $openStatusID = 3;
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array('purchaseInvoiceID'));
        $select->from('purchaseInvoice');
        $select->where->lessThan('purchaseInvoicePaymentDueDate', new Expression('CURDATE()'))->AND->equalTo('purchaseInvoiceRetrieveLocation', $locationID)->AND->equalTo('status', $openStatusID);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateOverDueInvoiceStatus($purchaseInvoiceID, $statusID)
    {
        $data = array('status' => $statusID);
        $this->tableGateway->update($data, array('purchaseInvoiceID' => $purchaseInvoiceID));
    }

    public function getLocationRelatedHalfPurchaseInvoices($locationID, $searchKey = FALSE)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array("*"))
                ->join('entity', 'purchaseInvoice.entityID=entity.entityID', array('*'), 'left')
                ->order(array('purchaseInvoiceIssueDate' => 'DESC'))
                ->where(array('deleted' => '0', 'purchaseInvoiceRetrieveLocation' => $locationID))
        ->where->notEqualTo('purchaseInvoiceTotal', 'purchaseInvoicePayedAmount');
        $select->where->notEqualTo('purchaseInvoice.status', '10');
        if ($searchKey) {
            $select->where->like('purchaseInvoice.purchaseInvoiceCode', '%' . $searchKey . '%');
            $select->limit(50);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }

        return $results;
    }

    public function getPurchaseInvoiceBySupplierID($id, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array("*"))
                ->join("supplier", "purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID", array("*"), "left")
                ->join("status", "purchaseInvoice.status = status.statusID", array("*"), "left")
                ->order(array('purchaseInvoiceIssueDate' => 'DESC'))
                ->where(array('purchaseInvoice.purchaseInvoiceSupplierID' => $id, 'purchaseInvoice.purchaseInvoiceRetrieveLocation' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getPurchaseInvoiceByID($id, $status = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $id = (int) $id;
        $select = new Select();
        $select->from('purchaseInvoice')
                ->columns(array('*'))
                ->order(array('purchaseInvoiceIssueDate' => 'DESC'))
                ->where(array('purchaseInvoiceID' => $id));
        if($status != ''){
            $select->where(array('status' => $status));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getPurchaseInvoiceByIDAndStatus($id, $status = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $id = (int) $id;
        $select = new Select();
        $select->from('purchaseInvoice')
                ->columns(array('*'))
                ->order(array('purchaseInvoiceIssueDate' => 'DESC'))
                ->where(array('purchaseInvoiceID' => $id));
        $select->where->in('status' ,$status);
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }


    public function getPurchaseInvoiceByIDForJE($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $id = (int) $id;
        $select = new Select();
        $select->from('purchaseInvoice')
                ->columns(array('*'))
                ->order(array('purchaseInvoiceIssueDate' => 'DESC'))
                ->where(array('purchaseInvoiceID' => $id));
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID',array("*"),'left');
        $select->where->notEqualTo('purchaseInvoice.status', 5);
        $select->where->notEqualTo('purchaseInvoice.status', 10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function updatePaymentOfInvoice(PurchaseInvoice $invoice)
    {
        $data = array(
            'purchaseInvoicePayedAmount' => $invoice->purchaseInvoicePayedAmount,
            'status' => $invoice->status,
        );
        $this->tableGateway->update($data, array('purchaseInvoiceID' => $invoice->purchaseInvoiceID));
        return TRUE;
    }

    public function checkPurchaseInvoiceByCode($purchaseInvoiceCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('purchaseInvoice')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "purchaseInvoice.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('purchaseInvoice.purchaseInvoiceCode' => $purchaseInvoiceCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * @return database query
     * get payment voucher details
     */
    public function getPaymentVoucherDetails($fromDate, $toDate, $locList)
    {
        //only get open,overdue and closed records.
        $status = [3,4,6];
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseInvoice')
                ->columns(array('PVID' => new Expression('purchaseInvoiceCode'),
                    'Date' => new Expression('purchaseInvoicePaymentDueDate'),
                    'IssueDate' => new Expression('purchaseInvoiceIssueDate'),
                    'Amount' => new Expression('purchaseInvoiceTotal'),
                    'location' => new Expression('location.locationName'),
                    'ID' => new Expression('purchaseInvoiceID'),
                    'SupplierName' => new Expression('supplier.supplierName')
                ))
                ->join('status', 'purchaseInvoice.status = status.statusID')
                ->join('location', 'location.locationID = purchaseInvoice.purchaseInvoiceRetrieveLocation')
                ->join('supplier', 'supplier.supplierID = purchaseInvoice.purchaseInvoiceSupplierID')
        ->where->in('purchaseInvoice.purchaseInvoiceRetrieveLocation', $locList)
        ->where->in('purchaseInvoice.status',$status)
        ->where->between('purchaseInvoiceIssueDate', $fromDate, $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = array();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }
    
    public function getDebitNoteWithTaxDetails($fromDate, $toDate, $locList)
    {
        //only get open,overdue and closed records.
        $status = [3,4,6];
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseInvoice')
                ->columns(array('PVID' => new Expression('purchaseInvoice.purchaseInvoiceCode'),
                    'location' => new Expression('location.locationName'),
                    'ID' => new Expression('purchaseInvoice.purchaseInvoiceID'),                    
                ))
                ->join('status', 'purchaseInvoice.status = status.statusID')
                ->join('location', 'location.locationID = purchaseInvoice.purchaseInvoiceRetrieveLocation')
                ->join('debitNote','purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID', array('debitNoteTotal'),'left')
                ->join('debitNoteProduct','debitNote.debitNoteID = debitNoteProduct.debitNoteID', array('debitNoteProductQuantity'),'left')
                ->join('debitNoteProductTax','debitNoteProduct.debitNoteProductID = debitNoteProductTax.debitNoteProductID', array('debitNoteTax' => new Expression('SUM(debitNoteProductTax.debitNoteProductTaxAmount)')),'left')
                ->join('tax','debitNoteProductTax.taxID = tax.id', array('taxType'),'left')
        ->where->in('purchaseInvoice.status',$status)
        ->where->between('purchaseInvoice.purchaseInvoiceIssueDate', $fromDate, $toDate);
        $select->group(['purchaseInvoice.purchaseInvoiceID','tax.taxType', 'debitNoteProductTax.debitNoteProductID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getPaymentVoucherWithTaxDetails($fromDate, $toDate, $locList)
    {
        //only get open,overdue and closed records.
        $status = [3,4,6];
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseInvoice')
                ->columns(array('PVID' => new Expression('purchaseInvoice.purchaseInvoiceCode'),
                    'Date' => new Expression('purchaseInvoice.purchaseInvoicePaymentDueDate'),
                    'IssueDate' => new Expression('purchaseInvoice.purchaseInvoiceIssueDate'),
                    'Amount' => new Expression('purchaseInvoice.purchaseInvoiceTotal'),
                    'location' => new Expression('location.locationName'),
                    'ID' => new Expression('purchaseInvoice.purchaseInvoiceID'),
                    'SupplierName' => new Expression('supplier.supplierName'),
                    
                ))
                ->join('status', 'purchaseInvoice.status = status.statusID')
                ->join('location', 'location.locationID = purchaseInvoice.purchaseInvoiceRetrieveLocation')
                ->join('supplier', 'supplier.supplierID = purchaseInvoice.purchaseInvoiceSupplierID')
                ->join('purchaseInvoiceProductTax','purchaseInvoice.purchaseInvoiceID = purchaseInvoiceProductTax.purchaseInvoiceID', array('purchaseInvoiceTaxID','purchaseInvoiceTaxAmount'),'left')
                ->join('purchaseInvoiceProduct','purchaseInvoice.purchaseInvoiceID = purchaseInvoiceProduct.purchaseInvoiceID', array('purchaseInvoiceProductTotalQty','productSerialID'),'left')
        ->where->in('purchaseInvoice.status',$status)
        ->where->between('purchaseInvoice.purchaseInvoiceIssueDate', $fromDate, $toDate);
        $select->group(['purchaseInvoice.purchaseInvoiceID', 'purchaseInvoiceProductTax.purchaseInvoiceProductTaxID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    //get location related purchase voucher
    public function getLocationRelatedPV($locationID, $searchKey = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array("*"))
                ->join('entity', 'purchaseInvoice.entityID=entity.entityID', array('*'), 'left')
                ->order(array('purchaseInvoiceIssueDate' => 'DESC'))
                ->where(array('deleted' => '0', 'purchaseInvoiceRetrieveLocation' => $locationID));
        if ($searchKey) {
            $select->where->like('purchaseInvoiceCode', '%' . $searchKey . '%');
            $select->limit(50);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getPurchaseVoucherByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierName', 'supplierCode'));
        $select->where(array('purchaseInvoiceID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function searchPIForDropdown($searchKey, $locationID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'));
        $select->where(new PredicateSet(array(new Operator('purchaseInvoiceCode', 'like', '%' . $searchKey . '%'))));
        $select->where->notEqualTo('purchaseInvoice.status', '10');
        if ($locationID) {
            $select->where(array('purchaseInvoiceRetrieveLocation' => $locationID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPVsForSearch($PVID = false, $supplierID = false, $locationID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseInvoice.purchaseInvoiceRetrieveLocation = location.locationID', array('locationName'));
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where->notEqualTo('purchaseInvoice.status',10);
        if ($PVID) {
            $select->where(array('purchaseInvoice.purchaseInvoiceID' => $PVID));
        }
        if ($supplierID) {
            $select->where(array('supplier.supplierID' => $supplierID));
        }
        if ($locationID) {
            $select->where(array('purchaseInvoiceRetrieveLocation' => $locationID));
        }
        $select->order(array('purchaseInvoiceIssueDate' => 'DESC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }


    public function getPVsBySerialCode($searchString, $locationID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'purchaseInvoice.purchaseInvoiceRetrieveLocation = location.locationID', array('locationName'));
        $select->join('purchaseInvoiceProduct', 'purchaseInvoice.purchaseInvoiceID = purchaseInvoiceProduct.purchaseInvoiceID', array('*'));
        $select->join('productSerial', 'purchaseInvoiceProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate'), 'left');
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where->notEqualTo('purchaseInvoice.status',10);
        $select->where(new PredicateSet(array(new Operator('productSerial.productSerialCode', 'like', '%' . $searchString . '%')), PredicateSet::OP_OR));
        if ($locationID) {
            $select->where(array('purchaseInvoiceRetrieveLocation' => $locationID));
        }
        $select->group(array('purchaseInvoice.purchaseInvoiceID'));
        // $select->order(array('purchaseInvoiceIssueDate' => 'DESC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param int $productID
     * @param int $locationID
     * @return array $resultArray
     */
    public function getPIDiscountByProductID($productID, $locationID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('purchaseInvoiceID'));
        $select->join('purchaseInvoiceProduct', 'purchaseInvoiceProduct.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID', array(
            'purchaseInvoiceProductDiscount', 'purchaseInvoiceProductTotalQty', 'purchaseInvoiceProductPrice'));
        $select->join('locationProduct', 'locationProduct.locationProductID = purchaseInvoiceProduct.locationProductID');
        $select->join('product', 'locationProduct.productID = product.productID');
        $select->where(array('product.productID' => $productID));
        if ($locationID != NULL) {
            $select->where(array('purchaseInvoice.purchaseInvoiceRetrieveLocation' => $locationID));
        }
        $select->order("purchaseInvoiceProduct.purchaseInvoiceProductID");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function updatePIStatusForCancel($pIID, $statusID)
    {
        $data = array(
            'status' => $statusID
        );
        return $this->tableGateway->update($data, array('purchaseInvoiceID' => $pIID));
    }

    public function updatePiDetails($data = [], $ID)
    {
        try{
            $this->tableGateway->update($data, array('purchaseInvoiceID' => $ID));
            return true;
        } catch(Exception $e){
            return false;
        }
    }
    
    /**
    * get all purchase invoices (with out cancaled and replace) for related supplier id(s)
    *
    **/
    public function getPurchaseInvoiceBySupplierIDs($supplierIDs = [],$status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array("*"))
                ->join("supplier", "purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID", array("*"), "left")
                ->join('status', 'purchaseInvoice.status = status.statusID',array('statusName'),'left')
                ->order(array('purchaseInvoiceIssueDate' => 'DESC'))
                ->join('entity', 'purchaseInvoice.entityID = entity.entityID',array('deleted', 'createdTimeStamp'))
                ->where(array('deleted' => 0));
        $select->where->in('purchaseInvoice.purchaseInvoiceSupplierID' ,$supplierIDs);
        $select->where->in('purchaseInvoice.status' , $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('purchaseInvoice.purchaseInvoiceIssueDate', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }


    public function getAgedSupplierPiAdvanceData($supplierIds, $endDate, $groupBy, $isAllSuppliers = false)
    {
        $agedAnalysisData = [];
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('purchaseInvoiceIssueDate',
            'purchaseInvoiceCode', 'purchaseInvoiceID', 'purchaseInvoiceSupplierID','status', 'purchaseInvoiceTotal'
        ));
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle', 'supplierCode'), 'left');
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'), 'left');
        $select->order(array('supplier.supplierName'));
        if ($groupBy) {
            $select->group('supplier.supplierID');
        }
        $select->where->lessThanOrEqualTo('purchaseInvoiceIssueDate', $endDate);
        if ($supplierIds && !$isAllSuppliers) {
            $select->where->in('purchaseInvoice.purchaseInvoiceSupplierID', $supplierIds);
        }
        $select->where->notEqualTo('purchaseInvoice.purchaseInvoiceSupplierID', 0);
        $select->where->in('purchaseInvoice.status', ['3','6','4']);
        $select->where(array('deleted' => '0'));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $agedAnalysisData[] = $val;
        }

        return $agedAnalysisData;
    }

     public function getAgedSupplierPiData($supplierIds, $endDate, $groupBy, $isAllSuppliers = false)
    {
        $agedAnalysisData = [];
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('purchaseInvoiceIssueDate',
            'purchaseInvoiceCode', 'purchaseInvoiceID', 'purchaseInvoiceSupplierID','purchaseInvoiceIssueDate','status', 'purchaseInvoiceTotal'
        ));
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle', 'supplierCode'), 'left');
        
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'), 'left');
        $select->order(array('purchaseInvoice.purchaseInvoiceCode'));
        if ($groupBy) {
            $select->group('supplier.supplierID');
        }
        $select->where->lessThanOrEqualTo('purchaseInvoiceIssueDate', $endDate);
        if ($supplierIds && !$isAllSuppliers) {
            $select->where->in('purchaseInvoice.purchaseInvoiceSupplierID', $supplierIds);
        }
        $select->where->notEqualTo('purchaseInvoice.purchaseInvoiceSupplierID', 0);
        // $select->where->notEqualTo('purchaseInvoice.status', 4);
        $select->where->notEqualTo('purchaseInvoice.status', 5);
        $select->where->notEqualTo('purchaseInvoice.status', 10);
        $select->where(array('deleted' => '0'));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $agedAnalysisData[] = $val;
        }

        return $agedAnalysisData;
    }

    public function checkPIhasAnyPayments($pIID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('outgoingPayment')
                    ->columns(array('*'))
                    ->join('outgoingPaymentInvoice', 'outgoingPayment.outgoingPaymentID = outgoingPaymentInvoice.paymentID',array('invoiceID'),'left')
                    ->where(array('outgoingPaymentInvoice.invoiceID' => $pIID));
            $select->where->notEqualTo('outgoingPayment.outgoingPaymentStatus',5);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            $results = [];
            foreach ($rowset as $value) {
                $results = $value;
            }
            if (!$results) {
                return null;
            }
            return $results;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getPurchaseInvoiceBasicDataByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->where(array('purchaseInvoiceID' => $id));
        $select->where->notEqualTo('status',5);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getPurchaseInvoiceBasicDataByPoId($poId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('entity','purchaseInvoice.entityID = entity.entityID',array('*'),'left');
        $select->where(array('purchaseInvoicePoID' => $poId));
        $select->where->notEqualTo('status',5);
        $select->group("purchaseInvoice.purchaseInvoiceID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getPurchaseInvoiceBasicDataCopiedFromGrnByPoId($poId = null, $grnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('grnProduct','purchaseInvoice.purchaseInvoiceGrnID = grnProduct.grnID',array('grnID'),'left');
        $select->join('entity','purchaseInvoice.entityID = entity.entityID',array('*'),'left');
        if (!is_null($poId)) {
            $select->where(array('grnProduct.grnProductDocumentId' => $poId, 'grnProduct.grnProductDocumentTypeId' => 9));
        }
        if (!is_null($grnID)) {
            $select->where(array('purchaseInvoice.purchaseInvoiceGrnID' => $grnID));   
        }
        $select->where->notEqualTo('status',5);
        $select->group("purchaseInvoice.purchaseInvoiceID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }


    public function getPurchaseOrderDataRelatedToPi($piId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('purchaseOrder','purchaseInvoice.purchaseInvoicePoID = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('entity','purchaseOrder.entityID = entity.entityID',array('*'),'left');
        $select->where(array('purchaseInvoice.purchaseInvoiceID' => $piId));
        $select->group("purchaseOrder.purchaseOrderID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    //get pi related grn by pi id
    public function getGrnDetailsByPiId($piId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice')
                ->columns(['*'])
                ->join('grn', 'purchaseInvoice.purchaseInvoiceGrnID = grn.grnID', array('*'), 'left')
                ->join('entity', 'grn.entityID = entity.entityID', array('*'), 'left');
        $select->where->equalTo('purchaseInvoice.purchaseInvoiceID', $piId);
        $select->group("grn.grnID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get grn related po by pi id
    public function getPoDetailsRelatedToGrnByPiId($piId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice')
                ->columns(['*'])
                ->join('grnProduct', 'purchaseInvoice.purchaseInvoiceGrnID = grnProduct.grnID', array('*'), 'left')
                ->join('purchaseOrder', 'grnProduct.grnProductDocumentId = purchaseOrder.purchaseOrderID', array('*'), 'left')
                ->join('entity', 'purchaseOrder.entityID = entity.entityID', array('*'), 'left');
        $select->where->equalTo('purchaseInvoice.purchaseInvoiceID', $piId);
        $select->group("purchaseOrder.purchaseOrderID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get grn related pr by pi id
    public function getPrDetailsRelatedToGrnByPiId($piId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice')
                ->columns(['*'])
                ->join('grn', 'purchaseInvoice.purchaseInvoiceGrnID = grn.grnID', array('*'), 'left')
                ->join('purchaseReturn','grn.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left')
                ->join('entity', 'purchaseReturn.purchaseReturnEntityID = entity.entityID', array('*'), 'left');
        $select->where->equalTo('purchaseInvoice.purchaseInvoiceID', $piId);
        $select->group("purchaseReturn.purchaseReturnID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPurchaseInvoiceBySupplierIDsForSupplierBalance($supplierIDs = [],$status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array("*"))
                ->join("supplier", "purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID", array("*"), "left")
                ->join('status', 'purchaseInvoice.status = status.statusID',array('statusName'),'left')
                ->join('entity', 'purchaseInvoice.entityID = entity.entityID',array('deleted', 'createdTimeStamp'))
                ->order(array('createdTimeStamp' => 'DESC'))
                ->where(array('deleted' => 0));
        $select->where->in('purchaseInvoice.purchaseInvoiceSupplierID' ,$supplierIDs);
        $select->where->in('purchaseInvoice.status' , $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('entity.createdTimeStamp', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getPurchaseInvoiceBalanceAmountAfterPayment($invoiceID, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice')
                ->columns(array('remainingInvoiceAmount' => new Expression('SUM(purchaseInvoiceTotal-purchaseInvoicePayedAmount)')))
                ->join('supplier', 'supplier.supplierID = purchaseInvoice.purchaseInvoiceSupplierID', array(),'left')
                ->join('status', 'purchaseInvoice.status = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'supplier.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'purchaseInvoice.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0','purchaseInvoiceID' => $invoiceID));
                $select->where->in('purchaseInvoice.status',$status);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 0) {
            return NULL;
        }
        return $results->current();
    }
    
    public function getPurchaseInvoiceBasicDataWithTimeStampByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('entity','purchaseInvoice.entityID = entity.entityID',array('*'),'left');
        $select->where(array('purchaseInvoiceID' => $id));
        $select->where->notEqualTo('status',5);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getPurchaseInvoiceBasicDataByPrId($prId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('purchaseOrder','purchaseInvoice.purchaseInvoicePoID = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('grnProduct','grnProduct.grnProductDocumentId = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('purchaseReturn','grnProduct.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseReturn.purchaseReturnID' => $prId,'grnProduct.grnProductDocumentTypeId' => 9));
        $select->group("purchaseInvoice.purchaseInvoiceID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getPurchaseInvoiceBasicDataRelatedToGrnByPrId($prId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('grn','grn.grnID = purchaseInvoice.purchaseInvoiceGrnID',array('*'),'left');
        $select->join('purchaseReturn','grn.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseReturn.purchaseReturnID' => $prId));
        $select->group("purchaseInvoice.purchaseInvoiceID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getPurchaseInvoiceBasicDataByDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->columns(array('*'));
        $select->join('debitNote','purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID',array('*'),'left');
        $select->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('debitNote.debitNoteID' => $debitNoteID));
        $select->group("purchaseInvoice.purchaseInvoiceID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getPiWiseDebitNote($fromDate = null, $toDate = null, $piIds = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseInvoice')
                ->columns(['purchaseInvoiceID','purchaseInvoiceCode','purchaseInvoiceIssueDate','purchaseInvoiceTotal', 'status','entityID'])
                ->join('debitNote', 'purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID', ['debitNoteCode','debitNoteDate','debitNoteTotal','entityID','statusID'], 'left')
                ->join('debitNoteProduct', 'debitNote.debitNoteID = debitNoteProduct.debitNoteID', [
                    'copiedAmount' => new Expression('SUM(debitNoteProductTotal)'),'debitNoteID'], 'left')
                ->join(['status' => 'status'], 'purchaseInvoice.status = status.statusID', ['piStatus' => 'statusName'], 'left')
                ->join(['dNstatus' => 'status'], 'debitNote.statusID = dNstatus.statusID', ['dNStatus' => 'statusName'], 'left')
                ->join(['dNentity' => 'entity'], 'debitNote.entityID = dNentity.entityID', ['dNdeleted' => 'deleted'], 'left')
                ->join(['pIentity' => 'entity'], 'purchaseInvoice.entityID = pIentity.entityID', ['pIdeleted' => 'deleted'], 'left')
                ->where->equalTo('dNentity.deleted', 0)
                ->where->equalTo('pIentity.deleted', 0);
        if ($fromDate && $toDate) {
            $select->where->between('purchaseInvoice.purchaseInvoiceIssueDate', $fromDate, $toDate);
        }
        if ($piIds) {
            $select->where->in('purchaseInvoice.purchaseInvoiceID', $piIds);
        }
        $select->group(['purchaseInvoice.purchaseInvoiceID','debitNote.debitNoteID']);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
    * this function use to get daily or monthly or anual purchase invoices
    * @param date $currentDate
    * @param date $startDate
    * @param string $type
    * return mix
    **/
    public function getInvoiceTotalByTimePerid($currentDate,$startDate = null, $type)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array('TotalSales' => new Expression('SUM(purchaseInvoiceTotal)')))
        ->where->in("purchaseInvoice.status", [3,4,6]);
        if ($type == 'daily') {
            $select->where->equalTo('purchaseInvoiceIssueDate', $currentDate);
        } else {
            $select->where->between('purchaseInvoiceIssueDate', $startDate, $currentDate);
            
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDailyInvoiceCount($currentDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array('TotalSalesCount' => new Expression('COUNT(purchaseInvoiceID)')))
        ->where->in("purchaseInvoice.status", [3,4,6]);
        $select->where->equalTo('purchaseInvoiceIssueDate', $currentDate);
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoiceTotalByDate($startDate, $endDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array('TotalSales' => new Expression('SUM(purchaseInvoiceTotal)'),'purchaseInvoiceIssueDate'))
        ->where->in("purchaseInvoice.status", [3,4,6]);
        $select->where->between('purchaseInvoiceIssueDate', $startDate, $endDate);
        $select->group('purchaseInvoiceIssueDate');
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getTotalPurchaseByDateRangeAndLocationID($startDate,$endDate, $locationID, $supplierFlag = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array('invTotal' => new Expression('SUM(purchaseInvoiceTotal)'),'purchaseInvoiceSupplierID'))
                ->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'))
                ->where->notEqualTo("purchaseInvoice.status", 10);
        $select->where->notEqualTo("purchaseInvoice.status", 5);
        $select->where->between('purchaseInvoiceIssueDate', $startDate, $endDate);
        $select->where(array('entity.deleted' => 0, 'purchaseInvoice.purchaseInvoiceRetrieveLocation' => $locationID));

        if ($supplierFlag) {
            $select->group('purchaseInvoice.purchaseInvoiceSupplierID');    
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        if ($supplierFlag) {
            return $results;   
        } else {
            return $results->current();   
        }
    }

    public function getTotalPurchaseByDateRangeAndLocationIDAndStatus($startDate,$endDate, $locationID, $status)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array('invAmount' => new Expression('purchaseInvoiceTotal'),'invPayedAmountAmount' => new Expression('purchaseInvoicePayedAmount'),'purchaseInvoiceID','purchaseInvoiceIssueDate'))
                ->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'), 'left');
        $select->where->between('purchaseInvoiceIssueDate', $startDate, $endDate);
        $select->where(array('entity.deleted' => 0, 'purchaseInvoice.purchaseInvoiceRetrieveLocation' => $locationID, 'purchaseInvoice.status' => $status));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;   
    }

    public function getTotalPurchaseByDateRangeAndLocationIDForDashboard($startDate,$endDate, $locationID, $supplierFlag = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array('invTotal' => new Expression('SUM(purchaseInvoiceTotal)')))
                ->join('entity', 'purchaseInvoice.entityID = entity.entityID', array('deleted'), 'left')
                // ->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'))
                ->where->notEqualTo("purchaseInvoice.status", 10);
        $select->where->notEqualTo("purchaseInvoice.status", 5);
        $select->where->between('purchaseInvoiceIssueDate', $startDate, $endDate);
        $select->where(array('entity.deleted' => 0, 'purchaseInvoice.purchaseInvoiceRetrieveLocation' => $locationID));

        if ($supplierFlag) {
            $select->group('purchaseInvoiceSupplierID');    
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        if ($supplierFlag) {
            return $results;   
        } else {
            return $results->current();   
        }
    }


}
