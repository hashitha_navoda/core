<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class GoodsIssueTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $paginated
     * @param \Zend\Db\Sql\Select $select
     * @return \Zend\Paginator\Paginator
     */
    public function fetchAll($paginated = FALSE, $locationId = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('goodsIssueID', 'goodsIssueCode', 'goodsIssueDate', 'goodsIssueReason', 'goodsIssueStatus', 'goodsIssueComment'));
        $select->join('entity', 'entity.entityID = goodsIssue.entityID');
        $select->join('status', 'status.statusID = goodsIssue.goodsIssueStatus', array('statusName'), 'left');
        $select->order('goodsIssueID DESC');
        if ($locationId) {
            $select->where(array('goodsIssue.locationID' => $locationId));
        }
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * @author Sandun  <Sandun@thinkcube.com>
     * get count of goodsIssueID column
     * @return int value
     */
    public function getCountColums()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('countColum' => new Expression('Max(goodsIssueID)')));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        $row = $rowset->current();
        return $row;
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @param \Inventory\Model\GoodsIssue $goodsIssue
     * @return type
     */
    public function saveGoodIssue(GoodsIssue $goodsIssue)
    {
        $data = array(
            'goodsIssueCode' => $goodsIssue->goodsIssueCode,
            'locationID' => $goodsIssue->locationID,
            'goodsIssueDate' => $goodsIssue->goodsIssueDate,
            'goodsIssueReason' => $goodsIssue->goodsIssueReason,
            'goodsIssueComment' => $goodsIssue->goodsIssueComment,
            'goodsIssueTypeID' => $goodsIssue->goodsIssueTypeID,
            'entityID' => $goodsIssue->entityID
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $id
     * @return type
     * @throws \Exception
     */
    public function getGoodsIssue($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('goodsIssueID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function addGoodsIssue($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('goodsIssueID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $ID
     * @param type $data
     * @return type
     */
    public function update($data, $ID)
    {
        return $this->tableGateway->update($data, array('goodsIssueID' => $ID));
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $gID
     * @return $rowset
     */
    public function getGoodIssueByID($gID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('*'));
        $select->join('goodsIssueProduct', 'goodsIssue.goodsIssueID = goodsIssueProduct.goodsIssueID', array(
            'goodsIssueID', 'productBatchID', 'productSerialID',
            'locationProductID', 'goodsIssueProductQuantity',
            'unitOfPrice', 'uomConversionRate'), 'left');
        $select->join('goodsIssueType', 'goodsIssueType.goodsIssueTypeID = goodsIssue.goodsIssueTypeID', array('goodsIssueTypeName'));
        $select->join('locationProduct', 'goodsIssueProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('grnProduct', 'locationProduct.locationProductID = grnProduct.locationProductID', array('grnProductPrice'), 'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'));
        $select->join('uom', 'goodsIssueProduct.uomID = uom.uomID', array('*'), 'left');
        $select->join('productBatch', 'productBatch.productBatchID = goodsIssueProduct.productBatchID', array('productBatchCode',
            'productBatchExpiryDate',
            'productBatchWarrantyPeriod',
            'productBatchManufactureDate',
            'productBatchQuantity'), 'left');
        $select->join('productSerial', 'productSerial.productSerialID = goodsIssueProduct.productSerialID', array('productSerialCode',
            'productSerialWarrantyPeriod',
            'productSerialExpireDate',
            'productSerialSold'), 'left');
        $select->join('location', 'goodsIssue.locationID = location.locationID', array(
            'locationCode', 'locationName'), 'left');
        $select->group(array('goodsIssueProduct.goodsIssueProductID'));
        $select->where(array('goodsIssue.goodsIssueID' => $gID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * This function use to get data for print view
     */
    public function getDataToPreview($gID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('*'));
        $select->join('goodsIssueProduct', 'goodsIssue.goodsIssueID = goodsIssueProduct.goodsIssueID', array(
            'goodsIssueID', 'productBatchID', 'productSerialID', 'locationProductID', 'goodsIssueProductQuantity', 'unitOfPrice'), 'left');
        $select->join('locationProduct', 'goodsIssueProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('grnProduct', 'locationProduct.locationProductID = grnProduct.locationProductID', array('grnProductPrice'), 'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('uom', 'goodsIssueProduct.uomID = uom.uomID', array('*'), 'left');
        $select->join('location', 'goodsIssue.locationID = location.locationID', array(
            'locationCode', 'locationName'), 'left');
        $select->join('entity', 'goodsIssue.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->group(array('goodsIssueProduct.goodsIssueProductID'));
        $select->where(array('goodsIssue.goodsIssueID' => $gID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function getDataForJE($gID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('*'));
        $select->join('goodsIssueProduct', 'goodsIssue.goodsIssueID = goodsIssueProduct.goodsIssueID', array(
            'goodsIssueID', 'productBatchID', 'productSerialID', 'locationProductID', 'goodsIssueProductQuantity', 'unitOfPrice'), 'left');
        $select->join('locationProduct', 'goodsIssueProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('grnProduct', 'locationProduct.locationProductID = grnProduct.locationProductID', array('grnProductPrice'), 'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('uom', 'goodsIssueProduct.uomID = uom.uomID', array('*'), 'left');
        $select->join('location', 'goodsIssue.locationID = location.locationID', array(
            'locationCode', 'locationName'), 'left');
        $select->join('entity', 'goodsIssue.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->group(array('goodsIssueProduct.goodsIssueProductID'));
        $select->where(array('goodsIssue.goodsIssueID' => $gID));
        $select->where->notEqualTo('goodsIssue.goodsIssueStatus', 5);
        $select->where->notEqualTo('goodsIssue.goodsIssueStatus', 10);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $adjustmentString
     * @return $result
     */
    public function getAdjustmentforSearch($adjustmentString, $paginated = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('goodsIssueID', 'goodsIssueCode', 'goodsIssueDate', 'goodsIssueReason', 'goodsIssueStatus'));
        $select->join('entity', 'entity.entityID = goodsIssue.entityID');
        $select->join('status', 'status.statusID = goodsIssue.goodsIssueStatus', array('statusName'), 'left');
        $select->order('goodsIssueID DESC');
        $select->where(array('deleted' => 0));
        $select->where->like('goodsIssueReason', '%' . $adjustmentString . '%')
        ->OR->like('goodsIssueCode', '%' . $adjustmentString . '%');

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return $rowset
     */
    public function getProductLists()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('*'));
        $select->join('goodsIssueProduct', 'goodsIssue.goodsIssueID = goodsIssueProduct.goodsIssueID', array(
            'goodsIssueID', 'productBatchID', 'productSerialID', 'locationProductID', 'goodsIssueProductQuantity', 'unitOfPrice'), 'left');
        $select->join('goodsIssueType', 'goodsIssueType.goodsIssueTypeID = goodsIssue.goodsIssueTypeID', array('goodsIssueTypeName'));
        $select->join('locationProduct', 'goodsIssueProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('uom', 'goodsIssueProduct.uomID = uom.uomID', array('*'), 'left');
        $select->join('location', 'goodsIssue.locationID = location.locationID', array(
            'locationCode', 'locationName'), 'left');
        $select->join('entity', 'entity.entityID = goodsIssue.entityID', array('*'), 'left');
        $select->where(array('goodsIssue.goodsIssueTypeID' => 2, 'goodsIssue.goodsIssueStatus' => 4, 'entity.deleted' => 0));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function checkAdjusmentByCode($adjusmnetCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('goodsIssue')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "goodsIssue.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('goodsIssue.goodsIssueCode' => $adjusmnetCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getStockValueData($pIds, $locIds, $adjustmentType, $status, $fromDate)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('goodsIssueID', 'goodsIssueStatus'));
        $select->join('goodsIssueProduct', 'goodsIssueProduct.goodsIssueID = goodsIssue.goodsIssueID', array('unitOfPrice',
            'goodsIssueProductQuantity',
            'goodsIssueProductID',
            'unitOfPrice',
            'uomID'), 'left');
        $select->join('locationProduct', 'locationProduct.locationProductID = goodsIssueProduct.locationProductID', array('productID'), 'left');
        $select->join('productUom', 'productUom.uomID = goodsIssueProduct.uomID', array('productUomConversion'), 'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productName', 'productCode'), 'left');
        $select->join('location', 'locationProduct.locationID = location.locationID', array('locationName', 'locationID', 'locationCode'), 'left');
        $select->order('locationProduct.productID');
        $select->group(array('locationProduct.locationProductID', 'goodsIssueProduct.goodsIssueProductID'));
        $select->where(array(
            'locationProduct.productID IS NOT NULL',
            'goodsIssueProduct.unitOfPrice IS NOT NULL',
            'goodsIssueProduct.goodsIssueProductQuantity IS NOT NULL'));
        if ($adjustmentType != NULL) {
            $select->where(array('goodsIssueTypeID' => $adjustmentType));
        }
        if ($locIds != NULL) {
            $select->where->in('locationProduct.locationID', $locIds);
        }
        if ($pIds != NULL) {
            $select->where->in('product.productID', $pIds);
        }
        if ($status != NULL) {
            $select->where(array('goodsIssue.goodsIssueStatus' => $status));
        }
        if ($fromDate != NULL) {
            $select->where->lessThan('goodsIssueDate', $fromDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultArray = [];
        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    /**
     * Search Adjustments for dropdown
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $searchKey
     * @return type
     */
    public function searchAdjustmentForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('goodsIssueID', 'goodsIssueCode'));
        $select->join('entity', 'entity.entityID = goodsIssue.entityID');
        $select->order('goodsIssueID DESC');
        $select->where(array('deleted' => 0));
        $select->where->like('goodsIssueCode', '%' . $searchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get Adjustments For Search by AdjustmentID
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $adjustmetID
     * @return type
     */
    public function getAdjustmentForSearchByAdjustmentsID($adjustmetID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('goodsIssueID', 'goodsIssueCode', 'goodsIssueDate', 'goodsIssueReason', 'goodsIssueStatus'));
        $select->join('entity', 'entity.entityID = goodsIssue.entityID');
        $select->join('status', 'status.statusID = goodsIssue.goodsIssueStatus', array('statusName'), 'left');
        $select->order('goodsIssueID DESC');
        $select->where(array('deleted' => 0, 'goodsIssue.goodsIssueID' => $adjustmetID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getAdjutmentByCode($adjustmentCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('goodsIssueID'));
        $select->join('location', "location.locationID = goodsIssue.locationID", array('locationName', 'locationCode'), 'left');
        $select->where(array('goodsIssueCode' => $adjustmentCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getAdjutmentByAdjutmentId($adjustmentId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssue');
        $select->columns(array('goodsIssueID', 'goodsIssueCode'));
        $select->join('location', "location.locationID = goodsIssue.locationID", array('locationName', 'locationCode'), 'left');
        $select->where(array('goodsIssueID' => $adjustmentId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getAdjustmentByAdjustmentIDForRec($adjustmentId, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('goodsIssue')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "goodsIssue.entityID = e.entityID", array('deleted', 'createdTimeStamp'), 'left')
                    ->where(array('goodsIssue.goodsIssueID' => $adjustmentId));
                $select->where(array('e.deleted' => '0'));
            
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

}
