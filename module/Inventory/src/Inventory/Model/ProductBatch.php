<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductBatch
{

    public $productBatchID;
    public $locationProductID;
    public $productBatchCode;
    public $productBatchExpiryDate;
    public $productBatchWarrantyPeriod;
    public $productBatchManufactureDate;
    public $productBatchQuantity;
    public $productBatchPrice;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->productBatchCode = (!empty($data['productBatchCode'])) ? $data['productBatchCode'] : null;
        $this->productBatchExpiryDate = (!empty($data['productBatchExpiryDate'])) ? $data['productBatchExpiryDate'] : null;
        $this->productBatchWarrantyPeriod = (!empty($data['productBatchWarrantyPeriod'])) ? $data['productBatchWarrantyPeriod'] : null;
        $this->productBatchManufactureDate = (!empty($data['productBatchManufactureDate'])) ? $data['productBatchManufactureDate'] : null;
        $this->productBatchQuantity = (!empty($data['productBatchQuantity'])) ? $data['productBatchQuantity'] : null;
        $this->productBatchPrice = (!empty($data['productBatchPrice'])) ? $data['productBatchPrice'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

