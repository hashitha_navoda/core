<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Tax Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

//use Zend\Db\Adapter\Driver\DriverInterface;

class ProductHandelingTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {

    }

    public function saveProductHandeling(ProductHandeling $data)
    {
        $productHandeling = array(
            'productID' => $data->productID,
            'productHandelingManufactureProduct' => $data->productHandelingManufactureProduct,
            'productHandelingPurchaseProduct' => $data->productHandelingPurchaseProduct,
            'productHandelingSalesProduct' => $data->productHandelingSalesProduct,
            'productHandelingConsumables' => $data->productHandelingConsumables,
            'productHandelingFixedAssets' => $data->productHandelingFixedAssets,
            'productHandelingGiftCard' => $data->productHandelingGiftCard,
            'productHandelingBomItem' => $data->productHandelingBomItem,
        );
        try {
            $this->tableGateway->insert($productHandeling);
            return $this->tableGateway->lastInsertValue;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function updateProductHandeling(ProductHandeling $data)
    {
        $productHandeling = array(
            'productHandelingManufactureProduct' => $data->productHandelingManufactureProduct,
            'productHandelingPurchaseProduct' => $data->productHandelingPurchaseProduct,
            'productHandelingSalesProduct' => $data->productHandelingSalesProduct,
            'productHandelingConsumables' => $data->productHandelingConsumables,
            'productHandelingFixedAssets' => $data->productHandelingFixedAssets,
            'productHandelingGiftCard' => $data->productHandelingGiftCard,
        );
        try {
            $this->tableGateway->update($productHandeling, array('productID' => $data->productID));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function checkProductHandeling($productID)
    {
        $result = $this->tableGateway->select(array('productID' => $productID));
        if ($result->current()) {
            return true;
        } else {
            return false;
        }
    }

    public function getProductHandlingByProductID($productID)
    {
        $result = $this->tableGateway->select(array('productID' => $productID));
        return $result->current();
    }

}
