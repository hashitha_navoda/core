<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\CompoundCostValue;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class CompoundCostValueTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCompCostValue(CompoundCostValue $compoundCostValue)
    {
        try {
            $data = array(
                'compoundCostTypeID' => $compoundCostValue->compoundCostTypeID,
                'amount' => $compoundCostValue->amount,
                'compoundCostTemplateID' => $compoundCostValue->compoundCostTemplateID,
                'supplierID' => $compoundCostValue->supplierID,
            );
            $this->tableGateway->insert($data);
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;

        } catch (\Exception $exc) {
            error_log($exc);
        }
    }

}