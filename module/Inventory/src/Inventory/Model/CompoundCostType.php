<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CompoundCostType
{

    public $compoundCostTypeID;
    public $compoundCostTypeName;
    public $compoundCostTypeCode;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->compoundCostTypeID = (!empty($data['compoundCostTypeID'])) ? $data['compoundCostTypeID'] : null;
        $this->compoundCostTypeName = (!empty($data['compoundCostTypeName'])) ? $data['compoundCostTypeName'] : null;
        $this->compoundCostTypeCode = (!empty($data['compoundCostTypeCode'])) ? $data['compoundCostTypeCode'] : null;
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
