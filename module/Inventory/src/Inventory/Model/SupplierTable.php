<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Supplier Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Exception\InvalidQueryException;
use Zend\Db\Exception;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class SupplierTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select('supplier');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Supplier());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $adpater = $this->tableGateway->getAdapter();
        $supplier = new TableGateway('supplier', $adpater);
        $rowset = $supplier->select(function (Select $select) {
            $select->order('supplierName ASC');
        });
        return $rowset;
    }

    public function fetchAllAsc()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array('*'))
                ->join('entity', 'supplier.entityID = entity.entityID', array('deleted'), 'left')
                ->order('supplierName ASC')
                ->where(array('deleted' => 0))
                ->where('supplierName IS NOT NULL')
                ->where('supplierCode IS NOT NULL');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getSuppliers($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier');
        $select->join('entity', 'supplier.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $results = $this->tableGateway->selectWith($select);
            return $results;
        }
    }

    public function getSupplierFromID($id)
    {
        $id = (int) $id;
        $row = NULL;
        $rowset = $this->tableGateway->select(array('supplierID' => $id));
        foreach ($rowset as $val) {
            $row = $val;
        }
        if ($row == NULL) {
            return NULL;
        } else {
            return $row;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param String $supplierString
     * @return ResultSet
     */
    public function getSuppliersforSearch($supplierString)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $supplierString . '\' in supplier.supplierName )>0,POSITION(\'' . $supplierString . '\' in supplier.supplierName), 9999),'
                    . 'IF(POSITION(\'' . $supplierString . '\' in supplier.supplierCode )>0,POSITION(\'' . $supplierString . '\' in supplier.supplierCode), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(supplier.supplierName ), CHAR_LENGTH(supplier.supplierCode )) '),
            '*',
        ));
        $select->from('supplier');
        $select->join('entity', 'supplier.entityID = entity.entityID', array('deleted'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('supplier.supplierName', 'like', '%' . $supplierString . '%'), new Operator('supplier.supplierCode', 'like', '%' . $supplierString . '%')), PredicateSet::OP_OR));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $result = $this->tableGateway->selectWith($select);
        return $result;
    }

    /**
     * @param type $supplierCode
     * @author Damith Thamara <damith@thinkcube.com>
     * @return If success supplier data else NULL
     */
    public function getSupplierByCode($supplierCode)
    {
        $rowset = $this->tableGateway->select(array('supplierCode' => $supplierCode));
        $row = $rowset->current();
        if ($row != NULL) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getNotDeletedSupplierByCode($supplierCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier');
        $select->join('entity', 'supplier.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        $select->where(array('supplierCode' => $supplierCode));
        $results = $this->tableGateway->selectWith($select);
        $row = $results->current();
        if ($row != NULL) {
            return $row;
        } else {
            return NULL;
        }
    }

    /**
     * @param input param is Supplier Model
     * @author Damith Thamara <damith@thinkcube.com>
     */
    public function saveSupplier(Supplier $supplier)
    {
        $data = array(
            'supplierCode' => $supplier->supplierCode,
            'supplierTitle' => $supplier->supplierTitle,
            'supplierName' => $supplier->supplierName,
            'supplierCurrency' => $supplier->supplierCurrency,
            'supplierAddress' => $supplier->supplierAddress,
            'supplierTelephoneNumber' => $supplier->supplierTelephoneNumber,
            'supplierEmail' => $supplier->supplierEmail,
            'supplierVatNumber' => $supplier->supplierVatNumber,
            'supplierCreditLimit' => $supplier->supplierCreditLimit,
            'supplierPaymentTerm' => $supplier->supplierPaymentTerm,
            'supplierOutstandingBalance' => $supplier->supplierOutstandingBalance,
            'supplierCreditBalance' => $supplier->supplierCreditBalance,
            'supplierOther' => $supplier->supplierOther,
            'supplierPayableAccountID' => $supplier->supplierPayableAccountID,
            'supplierPurchaseDiscountAccountID' => $supplier->supplierPurchaseDiscountAccountID,
            'supplierGrnClearingAccountID' => $supplier->supplierGrnClearingAccountID,
            'supplierAdvancePaymentAccountID' => $supplier->supplierAdvancePaymentAccountID,
            'entityID' => $supplier->entityID
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function updateSupplier($data, $supplierID)
    {
        $result = $this->tableGateway->update($data, array('supplierID' => $supplierID));
        return $result;
    }

    public function updateSupplierByCurrencyGainAndLoss($data, $supplierID)
    {
        try {
            $result = $this->tableGateway->update($data, array('supplierID' => $supplierID));
            return true;

        } catch (\Exception $e) {
            return false;            
        }
    }

    public function deleteSupplier($supplierID)
    {
        try {
            $result = $this->tableGateway->delete(array('supplierID' => $supplierID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param Array $suppIds
     * @return Array $results
     */
    public function supplierDetailsWithGrn($suppIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array('sID' => new Expression('supplier.supplierID'),
                    'sCD' => new Expression('supplier.supplierCode'),
                    'sName' => new Expression('supplier.supplierName'),
                    'sTitle' => new Expression('supplier.supplierTitle'),
                    'adrs' => new Expression('supplier.supplierAddress'),
                    'tNo' => new Expression('supplier.supplierTelephoneNumber'),
                    'sOutBlnc' => new Expression('supplier.supplierOutstandingBalance')))
                ->join('grn', 'supplier.supplierID = grn.grnSupplierID', array(
                    'totPurches' => new Expression('SUM(grn.grnTotal )')
                        ), 'left')
                ->group('supplier.supplierID')
                ->order('supplier.supplierName ASC')
        ->where->in('supplier.supplierID', $suppIds);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getSupplierNameList($suppIds, $isAllSuppliers = false)
    {
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array('sID' => new Expression('supplier.supplierID'),
                    'sCD' => new Expression('supplier.supplierCode'),
                    'sTitle' => new Expression('supplier.supplierTitle'),
                    'sName' => new Expression('supplier.supplierName')))
                ->join('entity', 'supplier.entityID = entity.entityID', array('deleted'), 'left')
                ->group('supplierID')
                ->where(array('deleted' => 0));
        if (!$isAllSuppliers) {
            $select->where->in('supplierID', $suppIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param Array $suppIds
     * @return $results
     */
    public function supplierWisePaymentVoucherItemDetails($suppIds, $isAllSuppliers = false)
    {
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array(
                    'sCD' => new Expression('supplier.supplierCode'),
                    'sName' => new Expression('supplier.supplierName')))
                ->join('purchaseInvoice', 'supplier.supplierID = purchaseInvoice.purchaseInvoiceSupplierID', array(
                    'sID' => new Expression('supplier.supplierID')
                        ), 'left')
                ->join('purchaseInvoiceProduct', 'purchaseInvoiceProduct.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID', array(
                    'totPurchaseQty' => new Expression('SUM(purchaseInvoiceProduct.purchaseInvoiceProductQuantity)')
                        ), 'left')
                ->join('locationProduct', 'locationProduct.locationProductID = purchaseInvoiceProduct.locationProductID', array(
                    'locProID' => new Expression('locationProduct.locationProductID')
                        ), 'left')
                ->join('product', 'product.productID = locationProduct.productID', array(
                    'pID' => new Expression('product.productID'),
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName')
                        ), 'left')
                ->join('entity', 'supplier.entityID = entity.entityID', array('deleted'), 'left')
                ->group(array('supplier.supplierID', 'purchaseInvoiceProduct.locationProductID'))
                ->order('supplier.supplierName ASC')
                ->where('product.productID IS NOT NULL');
        $select->where->notEqualto('purchaseInvoice.status',10);
        if (!$isAllSuppliers) {
            $select->where->in('supplier.supplierID', $suppIds);
        }
        $select->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function updateSupplierCurrentAndCreditBalance(Supplier $supplier)
    {
        try {
            $data = array(
                'supplierOutstandingBalance' => $supplier->supplierOutstandingBalance,
                'supplierCreditBalance' => $supplier->supplierCreditBalance,
            );
            $this->tableGateway->update($data, array('supplierID' => $supplier->supplierID));
            return TRUE;
        } catch (\Exception $exc) {
            echo $exc;
        }exit;
    }

    public function updateSupplierOutStandingBalance(Supplier $supplierDetails)
    {
        try {
            $data = array(
                'supplierOutstandingBalance' => $supplierDetails->supplierOutstandingBalance,
            );
            $this->tableGateway->update($data, array('supplierID' => $supplierDetails->supplierID));
            return TRUE;
        } catch (\Exception $exc) {
            echo $exc;
        }exit;
    }

    public function updateSupplierCredit(Supplier $supplier)
    {
        $data = array(
            'supplierCreditBalance' => $supplier->supplierCreditBalance,
        );
        $this->tableGateway->update($data, array('supplierID' => $supplier->supplierID));
        return TRUE;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param Array $suppIds
     * @return $results
     */
    public function supplierPaymentVoucherDetails($suppIds, $isAllSuppliers = false)
    {
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array('*'))
                ->join('purchaseInvoice', 'supplier.supplierID = purchaseInvoice.purchaseInvoiceSupplierID', array(
                    'totPaymentVoucher' => new Expression('SUM(purchaseInvoice.purchaseInvoiceTotal )')
                        ), 'left')
                ->join('entity', 'supplier.entityID = entity.entityID', array('deleted'), 'left')
                ->group('supplier.supplierID')
                ->order('supplier.supplierName ASC')
                ->where->notEqualTo('purchaseInvoice.status', 10);
        if (!$isAllSuppliers) {
            $select->where->in('supplier.supplierID', $suppIds);
        }
        $select->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param Array $suppIds
     * @return $results
     */
    public function supplierWiseDebitNoteItemDetails($suppIds, $isAllSuppliers = false)
    {
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array(
                    'sCD' => new Expression('supplier.supplierCode'),
                    'sName' => new Expression('supplier.supplierName'),
                    'sID' => new Expression('supplier.supplierID')))
                ->join('debitNote', 'debitNote.supplierID = supplier.supplierID', array(
                    'debitNoteID'), 'left')
                ->join('debitNoteProduct', 'debitNoteProduct.debitNoteID = debitNote.debitNoteID', array(
                    'totDebitNoteQty' => new Expression('SUM(debitNoteProduct.debitNoteProductQuantity)')), 'left')
                ->join('product', 'product.productID = debitNoteProduct.productID', array(
                    'pID' => new Expression('product.productID'),
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName')
                        ), 'left')
                 ->join('entity', 'supplier.entityID = entity.entityID', array('deleted'), 'left')
                ->group(array('supplier.supplierID', 'debitNoteProduct.productID'))
                ->order('supplier.supplierName ASC')
                ->where('product.productID IS NOT NULL');
        if (!$isAllSuppliers) {
            $select->where->in('supplier.supplierID', $suppIds);
        }
        $select->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $supplierSearchKey
     * @return type
     */
    public function searchSupplierForDropDown($supplierSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(['supplierName', 'supplierCode', 'supplierID',
                    'posi' => new Expression('LEAST('
                    . 'IF(POSITION(\'' . $supplierSearchKey . '\' in supplierName )>0,POSITION(\'' . $supplierSearchKey . '\' in supplierName), 9999),'
                    . 'IF(POSITION(\'' . $supplierSearchKey . '\' in supplierCode )>0,POSITION(\'' . $supplierSearchKey . '\' in supplierCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(supplierName), CHAR_LENGTH(supplierCode)) '),
                    'occr' => new Expression('LEAST('
                        .'IF((CHAR_LENGTH(supplierName) - CHAR_LENGTH(REPLACE(LOWER(supplierName), "' . $supplierSearchKey . '", ""))) >0, (CHAR_LENGTH(supplierName) - CHAR_LENGTH(REPLACE(LOWER(supplierName), "' . $supplierSearchKey . '", ""))), 9999),'
                        .'IF((CHAR_LENGTH(supplierCode) - CHAR_LENGTH(REPLACE(LOWER(supplierCode), "' . $supplierSearchKey . '", ""))) >0, (CHAR_LENGTH(supplierCode) - CHAR_LENGTH(REPLACE(LOWER(supplierCode), "' . $supplierSearchKey . '", ""))), 9999)'
                        .')')
                    ])
                ->join('entity', 'supplier.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
                ->where(new PredicateSet(array(new Operator('supplierName', 'like', '%' . $supplierSearchKey . '%'), new Operator('supplierCode', 'like', '%' . $supplierSearchKey . '%')), PredicateSet::OP_OR));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC, occr DESC'));        
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getSuppliersByIDs($supplierIDs = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array(
                    'sCD' => new Expression('supplier.supplierCode'),
                    'sName' => new Expression('supplier.supplierName'),
                    'sID' => new Expression('supplier.supplierID')))
                ->join('entity', 'supplier.entityID = entity.entityID', array('deleted'), 'left')
                ->group(array('supplier.supplierID'))
                ->order('supplier.supplierName ASC');
        $select->where->in('supplier.supplierID', $supplierIDs);
        $select->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
       
        return $results;
    }

    /*
     * This function check the supplier has any transaction
     */
    public function checkSupplierTransaction($supplierID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array('supplierID'))
                ->join('entity', 'supplier.entityID = entity.entityID', array('deleted'), 'left')
                ->join('purchaseOrder', 'supplier.supplierID =  purchaseOrder.purchaseOrderSupplierID', array('purchaseOrderID'), 'left')
                ->join('purchaseInvoice', 'supplier.supplierID =  purchaseInvoice.purchaseInvoiceSupplierID', array('  purchaseInvoiceID'), 'left')
                ->join('outgoingPayment', 'supplier.supplierID =  outgoingPayment.supplierID', array('outgoingPaymentID'), 'left')
                ->join('debitNote', 'supplier.supplierID =  debitNote.supplierID', array('debitNoteID'), 'left')
                ->join('debitNotePayment', 'supplier.supplierID =  debitNotePayment.supplierID', array('debitNotePaymentID'), 'left')
                ->join('grn', 'supplier.supplierID =  grn.grnSupplierID', array('grnID'), 'left')
                ->join('paymentVoucher', 'supplier.supplierID =  paymentVoucher.paymentVoucherSupplierID', array('paymentVoucherID'), 'left')
                ->join('productSupplier', 'supplier.supplierID =  productSupplier.supplierID', array('productID'), 'left')
                ->join('supplierCategoryComponent', 'supplier.supplierID =  supplierCategoryComponent.supplierID', array('supplierCategoryComponentID'), 'left')
                ->where(array('entity.deleted' => '0', 'supplier.supplierID' => $supplierID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results;
        return $row;
    }

    public function importReplaceSupplier($data, $supplierID)
    {
        try {
            $this->tableGateway->update($data, array('supplierID' => $supplierID));
            return true;
        } catch (\Exception $exc) {
            return false;
        }
    }

     /*
    * get the product data by gl account id
    */
    public function getSupplierByGlAccountID($glAccountID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array('*'))
                ->where(new PredicateSet(array(new Operator('supplierPayableAccountID', '=',$glAccountID), 
                    new Operator('supplierPurchaseDiscountAccountID', '=',$glAccountID),
                    new Operator('supplierGrnClearingAccountID', '=',$glAccountID),
                    new Operator('supplierAdvancePaymentAccountID', '=',$glAccountID)
                ), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }


    public function getSuppliersCountForDashboard()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array('supplierID'));
        $select->join('entity', 'supplier.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));

        $results = $this->tableGateway->selectWith($select);
        return $results;
    }
}
