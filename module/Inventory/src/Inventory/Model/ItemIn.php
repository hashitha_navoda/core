<?php

/**
 * @author Damith Thamara<damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ItemIn
{

    public $itemInID;
    public $itemInIndex;
    
    /**
     *
     * @var string item in document type
     *
     * Inventory Positive Adjustment
     * Goods Received Note
     * Inventory Transfer
     * Delivery Note Edit
     * Sales Returns
     * Invoice Edit
     * Invoice Cancel
     * Payment Voucher
     * Credit Note
     */
    public $itemInDocumentType;
    public $itemInDocumentID;
    public $itemInLocationProductID;
    public $itemInBatchID;
    public $itemInSerialID;
    public $itemInQty;
    public $itemInPrice;
    public $itemInAverageCostingPrice;
    public $itemInDiscount;
    public $itemInSoldQty;
    public $itemInDateAndTime;
    public $itemInDeletedFlag;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->itemInID = (!empty($data['itemInID'])) ? $data['itemInID'] : null;
        $this->itemInIndex = (!empty($data['itemInIndex'])) ? $data['itemInIndex'] : null;
        $this->itemInDocumentType = (!empty($data['itemInDocumentType'])) ? $data['itemInDocumentType'] : null;
        $this->itemInDocumentID = (!empty($data['itemInDocumentID'])) ? $data['itemInDocumentID'] : null;
        $this->itemInLocationProductID = (!empty($data['itemInLocationProductID'])) ? $data['itemInLocationProductID'] : null;
        $this->itemInBatchID = (!empty($data['itemInBatchID'])) ? $data['itemInBatchID'] : null;
        $this->itemInSerialID = (!empty($data['itemInSerialID'])) ? $data['itemInSerialID'] : null;
        $this->itemInQty = (!empty($data['itemInQty'])) ? $data['itemInQty'] : 0;
        $this->itemInPrice = (!empty($data['itemInPrice'])) ? $data['itemInPrice'] : 0;
        $this->itemInAverageCostingPrice = (!empty($data['itemInAverageCostingPrice'])) ? $data['itemInAverageCostingPrice'] : 0;
        $this->itemInDiscount = (!empty($data['itemInDiscount'])) ? $data['itemInDiscount'] : 0;
        $this->itemInSoldQty = (!empty($data['itemInSoldQty'])) ? $data['itemInSoldQty'] : 0;
        $this->itemInDateAndTime = (!empty($data['itemInDateAndTime'])) ? $data['itemInDateAndTime'] : NULL;
        $this->itemInDeletedFlag = (!empty($data['itemInDeletedFlag'])) ? $data['itemInDeletedFlag'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
