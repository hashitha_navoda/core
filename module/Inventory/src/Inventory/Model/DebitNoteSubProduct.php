<?php

/**
 * @author Ashan madushka <ashan@thinkcube.com>
 * This file contains debitNote sub product model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class DebitNoteSubProduct
{

    public $debitNoteSubProductID;
    public $debitNoteProductID;
    public $productBatchID;
    public $productSerialID;
    public $debitNoteSubProductQuantity;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->debitNoteSubProductID = (!empty($data['debitNoteSubProductID'])) ? $data['debitNoteSubProductID'] : null;
        $this->debitNoteProductID = (!empty($data['debitNoteProductID'])) ? $data['debitNoteProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->debitNoteSubProductQuantity = (!empty($data['debitNoteSubProductQuantity'])) ? $data['debitNoteSubProductQuantity'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
