<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains debit note payments details Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DebitNotePaymentDetailsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDebitNotePaymentDetails($debitNotePaymentDetails)
    {
        $data = array(
            'paymentMethodID' => $debitNotePaymentDetails->paymentMethodID,
            'debitNotePaymentID' => $debitNotePaymentDetails->debitNotePaymentID,
            'debitNoteID' => $debitNotePaymentDetails->debitNoteID,
            'debitNotePaymentDetailsAmount' => $debitNotePaymentDetails->debitNotePaymentDetailsAmount,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDebitNotePaymentDetailsByDebitNotePaymentID($debitNotePaymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNotePaymentDetails')
                ->columns(array('*'))
                ->where(array('debitNotePaymentID' => $debitNotePaymentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
