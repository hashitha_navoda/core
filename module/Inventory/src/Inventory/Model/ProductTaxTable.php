<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Tax Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

//use Zend\Db\Adapter\Driver\DriverInterface;

class ProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {

    }

    public function saveProductTax(ProductTax $data)
    {
        $productTax = array(
            'taxID' => $data->taxID,
            'productID' => $data->productID,
        );

        try {
            $this->tableGateway->insert($productTax);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function deleteProductTax($productID)
    {
        $this->tableGateway->delete(array('productID' => $productID));
    }

    public function deleteProductTaxByProductIDAndTaxID($productID, $taxID)
    {
        $this->tableGateway->delete(array('productID' => $productID, 'taxID' => $taxID));

        return true;
    }

    public function getProductTaxByProductID($productID)
    {
        $rowset = $this->tableGateway->select(array('productID' => $productID));
        return $rowset;
    }

    public function getProTaxDetailsByProductID($productID = null, $status = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productTax');
        $select->columns(array('*'));
        $select->join("product", "productTax.productID = product.productID", array('productTaxEligible'));
        $select->join("tax", "productTax.taxID = tax.id", array('*'));
        if($productID != ''){
            $select->where(array("product.productID" => $productID));
        }
        if ($status) {
            $select->where(array("tax.state" => 1));

        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getProductTaxDetailsByProductIDList($productIDs = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productTax');
        $select->columns(array('*'));
        $select->join("product", "productTax.productID = product.productID", array('productTaxEligible'));
        $select->join("tax", "productTax.taxID = tax.id", array('*'));
        $select->where(array("tax.state" => 1));
        $select->where->in('productTax.productID', $productIDs);
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

}
