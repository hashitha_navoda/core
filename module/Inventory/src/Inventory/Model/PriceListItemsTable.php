<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Price List items Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class PriceListItemsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //price list items save function
    public function savePriceListItems(PriceListItems $priceListItems)
    {
        $data = array(
            'priceListId' => $priceListItems->priceListId,
            'productId' => $priceListItems->productId,
            'priceListItemsPrice' => $priceListItems->priceListItemsPrice,
            'priceListItemsDiscount' => $priceListItems->priceListItemsDiscount,
            'priceListItemsDiscountType' => $priceListItems->priceListItemsDiscountType,
        );
        try {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } catch (\Exception $e) {
            return false;
        }
    }

    //update price list items save function
    public function updatePriceListItems(PriceListItems $priceListItems)
    {
        $data = array(
            'priceListItemsPrice' => $priceListItems->priceListItemsPrice,
            'priceListItemsDiscount' => $priceListItems->priceListItemsDiscount,
            'priceListItemsDiscountType' => $priceListItems->priceListItemsDiscountType
        );
        try {
            $this->tableGateway->update($data, array('productId' => $priceListItems->productId, 'priceListId' => $priceListItems->priceListId,));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    //get price list items by priceListId
    public function getPriceListItemsByPriceListIdAndLocationId($priceListId, $locationId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('priceListItems');
        $select->join('priceListLocations', 'priceListItems.priceListId = priceListLocations.priceListId', array('locationId'));
        $select->where(array('priceListItems.priceListId' => $priceListId, 'priceListLocations.locationId' => $locationId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get price list product ids
     * @param type $priceListId
     * @return array
     */
    public function getPriceListProductListByPriceListId($priceListId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('priceListItems');
        $select->where(array('priceListItems.priceListId' => $priceListId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $priceListIds = [];
        foreach ($result as $item) {
            $priceListIds[] = $item['productId'];
        }
        return $priceListIds;
    }

}
