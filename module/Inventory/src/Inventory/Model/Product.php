<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Product
{

    public $productID;
    public $productCode;
    public $productBarcode;
    public $productName;
    public $productTypeID;
    public $productDescription;
    public $productDiscountEligible;
    public $productDiscountPercentage;
    public $productDiscountValue;
    public $productDefaultOpeningQuantity;
    public $productDefaultSellingPrice;
    public $productDefaultMinimumInventoryLevel;
    public $productDefaultMaximunInventoryLevel;
    public $productDefaultReorderLevel;
    public $productImageURL;
    public $productState;
    public $categoryID;
    public $productGlobalProduct;
    public $batchProduct;
    public $serialProduct;
    public $hsCode;
    public $customDutyValue;
    public $customDutyPercentage;
    public $entityID;
    public $productDefaultPurchasePrice;
    public $rackID;
    public $itemDetail;
    public $productSalesAccountID;
    public $productInventoryAccountID;
    public $productCOGSAccountID;
    public $productAdjusmentAccountID;
    public $productDepreciationAccountID;
    public $productPurchaseDiscountEligible;
    public $productPurchaseDiscountPercentage;
    public $productPurchaseDiscountValue;
    public $isUseDiscountScheme;
    public $discountSchemID;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->productCode = (!empty($data['productCode'])) ? $data['productCode'] : null;
        $this->productBarcode = (!empty($data['productBarcode'])) ? $data['productBarcode'] : null;
        $this->productName = (!empty($data['productName'])) ? $data['productName'] : null;
        $this->productTypeID = (!empty($data['productTypeID'])) ? $data['productTypeID'] : null;
        $this->productDescription = (!empty($data['productDescription'])) ? $data['productDescription'] : null;
        $this->productDiscountEligible = (!empty($data['productDiscountEligible'])) ? (int) $data['productDiscountEligible'] : 0;
        $this->productDiscountPercentage = ($data['productDiscountPercentage'] >= 0) ? $data['productDiscountPercentage'] : null;
        $this->productDiscountValue = ($data['productDiscountValue'] >= 0 && $data['productDiscountValue'] != '') ? $data['productDiscountValue'] : null;
        $this->productDefaultOpeningQuantity = (!empty($data['productDefaultOpeningQuantity'])) ? $data['productDefaultOpeningQuantity'] : 0;
        $this->productDefaultSellingPrice = (!empty($data['productDefaultSellingPrice'])) ? $data['productDefaultSellingPrice'] : 0.00;
        $this->productDefaultMinimumInventoryLevel = (!empty($data['productDefaultMinimumInventoryLevel'])) ? $data['productDefaultMinimumInventoryLevel'] : 0;
        $this->productDefaultMaximunInventoryLevel = (!empty($data['productDefaultMaximunInventoryLevel'])) ? $data['productDefaultMaximunInventoryLevel'] : 0;
        $this->productDefaultReorderLevel = (!empty($data['productDefaultReorderLevel'])) ? $data['productDefaultReorderLevel'] : 0;
        $this->productTaxEligible = (!empty($data['productTaxEligible'])) ? $data['productTaxEligible'] : 0;
        $this->productImageURL = (!empty($data['productImageURL'])) ? $data['productImageURL'] : null;
        $this->productState = (!empty($data['productState'])) ? $data['productState'] : 0;
        $this->categoryID = (!empty($data['categoryID'])) ? $data['categoryID'] : null;
        $this->productGlobalProduct = (!empty($data['productGlobalProduct'])) ? $data['productGlobalProduct'] : 0;
        $this->batchProduct = (!empty($data['batchProduct'])) ? $data['batchProduct'] : 0;
        $this->serialProduct = (!empty($data['serialProduct'])) ? $data['serialProduct'] : 0;
        $this->hsCode = (!empty($data['hsCode'])) ? $data['hsCode'] : null;
        $this->customDutyValue = (!empty($data['customDutyValue'])) ? $data['customDutyValue'] : 0;
        $this->customDutyPercentage = (!empty($data['customDutyPercentage'])) ? $data['customDutyPercentage'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : 0;
        $this->productDefaultPurchasePrice = (!empty($data['productDefaultPurchasePrice'])) ? $data['productDefaultPurchasePrice'] : 0.00;
        $this->rackID = (!empty($data['rackID'])) ? $data['rackID'] : null;
        $this->itemDetail = (!empty($data['itemDetail'])) ? $data['itemDetail'] : null;
        $this->productSalesAccountID = (!empty($data['productSalesAccountID'])) ? $data['productSalesAccountID'] : null;
        $this->productInventoryAccountID = (!empty($data['productInventoryAccountID'])) ? $data['productInventoryAccountID'] : null;
        $this->productCOGSAccountID = (!empty($data['productCOGSAccountID'])) ? $data['productCOGSAccountID'] : null;
        $this->productAdjusmentAccountID = (!empty($data['productAdjusmentAccountID'])) ? $data['productAdjusmentAccountID'] : null;
        $this->productDepreciationAccountID = (!empty($data['productDepreciationAccountID'])) ? $data['productDepreciationAccountID'] : null;
        $this->productPurchaseDiscountEligible = (!empty($data['productPurchaseDiscountEligible'])) ? (int) $data['productPurchaseDiscountEligible'] : 0;
        $this->productPurchaseDiscountPercentage = $data['productPurchaseDiscountPercentage'];
        $this->productPurchaseDiscountValue = $data['productPurchaseDiscountValue'];
        $this->isUseDiscountScheme = (!empty($data['isUseDiscountScheme'])) ?  $data['isUseDiscountScheme'] : 0;
        $this->discountSchemID = (!empty($data['discountSchemID'])) ? $data['discountSchemID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'productID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'productCode',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 25,
                                    'messages' => array(
                                        'stringLengthTooLong' => 'Product Code should not be more than 25 characters'
                                    )
                                ),
                            ),
                            array(
                                'name' => 'Regex',
                                'options' => array(
//                                    'pattern' => '/^[\w.-]*$/',
                                    'pattern' => '/^[0-9A-Za-z\-_\.\%()+$#^&*!]*$/',
                                    'messages' => array(
                                        'regexNotMatch' => 'Item Code should only contain alphanumeric characters, dashes and underscores. No spaces are allowed.'
                                    )
                                ),
                            ),
//                            array(
//                                'name' => 'Alnum',
//                                'options' => array(
//                                    'messages' => array(
//                                        'notAlnum' => 'Product Code should only contain Alphanumeric characters. No whitespaces are allowed.'
//                                    )
//                                ),
//                            )
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'productBarcode',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            // array(
                            //     'name' => 'StringLength',
                            //     'options' => array(
                            //         'encoding' => 'UTF-8',
                            //         'min' => 1,
                            //         'max' => 2000,
                            //         'messages' => array(
                            //             'stringLengthTooLong' => 'Product Barcode should not be more than 25 characters'
                            //         )
                            //     ),
                            // ),
                            // array(
                            //     'name' => 'Regex',
                            //     'options' => array(
                            //         'pattern' => '/^[0-9A-Za-z\-_\.\% ]*$/',
                            //         'messages' => array(
                            //             'regexNotMatch' => 'Product Barcode you entered is invalid'
                            //         )
                            //     ),
                            // ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'productDescription',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 200,
                                    'messages' => array(
                                        'stringLengthTooLong' => 'Product Description should not be more than 200 characters'
                                    )
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'productName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 200,
                                    'messages' => array(
                                        'stringLengthTooLong' => 'Product Name should not be more than 200 characters'
                                    )
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'productCategoryID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'digits'
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
