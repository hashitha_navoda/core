<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ExpiryAlertNotification
{

    public $expiryAlertNotificationID;
    public $productID;
    public $NumberOfDays;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->expiryAlertNotificationID = (!empty($data['expiryAlertNotificationID'])) ? $data['expiryAlertNotificationID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->NumberOfDays = (!empty($data['NumberOfDays'])) ? $data['NumberOfDays'] : null;
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
