<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains supplier payments methos numbers Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class SupplierPaymentMethodNumbersTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        if ($paginated) {
            $select = new Select('paymentMethodsNumbers');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new PaymentMethodsNumbers());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function savePaymentMethodsNumbers(SupplierPaymentMethodNumbers $supplierPaymentMethodsNumbers)
    {
        $data = array(
            'outGoingPaymentID' => $supplierPaymentMethodsNumbers->outGoingPaymentID,
            'outGoingPaymentMethodID' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodID,
            'outGoingPaymentMethodFinanceAccountID' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodFinanceAccountID,
            'outGoingPaymentMethodReferenceNumber' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodReferenceNumber,
            'outGoingPaymentMethodChequeReference' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodChequeReference,
            'outGoingPaymentMethodBank' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodBank,
            'outGoingPaymentMethodCardID' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodCardID,
            'outGoingPaymentMethodBankTransferBankId' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodBankTransferBankId,
            'outGoingPaymentMethodBankTransferAccountId' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodBankTransferAccountId,
            'outGoingPaymentMethodBankTransferSupplierBankName' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodBankTransferSupplierBankName,
            'outGoingPaymentMethodBankTransferSupplierAccountNumber' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodBankTransferSupplierAccountNumber,
            'outGoingPaymentMethodChequeBankAccountID' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodChequeBankAccountID,
            'postdatedChequeStatus' => $supplierPaymentMethodsNumbers->postdatedChequeStatus,
            'postdatedChequeDate' => $supplierPaymentMethodsNumbers->postdatedChequeDate,
            'outGoingPaymentMethodPaidAmount' => $supplierPaymentMethodsNumbers->outGoingPaymentMethodPaidAmount
        );

        $this->tableGateway->insert($data);
        return true;
    }

    //get outgoing payment method bank transfer details by paymentID

    public function getOutGoingPaymentMethodBankTransferDetailsByPaymentID($paymentId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers');
        $select->where(array('outGoingPaymentMethodsNumbers.outGoingPaymentID' => $paymentId, 'outGoingPaymentMethodsNumbers.outGoingPaymentMethodID' => 5));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     *
     * Get Outgoing bank transfers by accountId.
     * @param string $accountId
     * @return mixed
     */
    public function getOutgoingBankTransfersByAccountId($accountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPaymentInvoice', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingInvoiceCashAmount', 'paymentMethodID'))
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPaymentInvoice.paymentMethodID', 5)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodBankTransferAccountId', $accountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     *
     * Get Outgoing bank transfers by accountId.
     * @param string $accountId
     * @return mixed
     */
    public function getTotalOutgoingBankTransfersByAccountId($accountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPaymentInvoice', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingInvoiceCashAmount', 'paymentMethodID'))
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPaymentInvoice.paymentMethodID', 5)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodBankTransferAccountId', $accountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     *
     * Get invoice payment issued cheques by issued accountId.
     * @param string $accountId
     * @return mixed
     */
    public function getIssuedChequesByAccountId($accountId = null, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType'))
                ->join('supplier', ' supplier.supplierID= outgoingPayment.supplierID ', array('supplierName', 'supplierCode', 'supplierTitle'), 'left')
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.outgoingPaymentType', 'invoice')
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 2);
        if (!is_null($accountId)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID', $accountId);
        }
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    
    public function getIssuedChequesByAccountIdForDashboard($accountId = null, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType'))
                ->join('supplier', ' supplier.supplierID= outgoingPayment.supplierID ', array('supplierName', 'supplierCode', 'supplierTitle'), 'left')
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.outgoingPaymentType', 'invoice')
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 2);
        if (!is_null($accountId)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID', $accountId);
        }
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     *
     * Update payment methods numbers
     * @param array $data
     * @param string $paymentMethodId
     * @return boolean
     */
    public function updatePaymentMethodsNumbers($data, $paymentMethodId)
    {
        if ($paymentMethodId) {
            if ($this->tableGateway->update($data, array('outGoingPaymentMethodsNumbersID' => $paymentMethodId))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * @author Madawa Chandrarathne <madawa@thinkcube.com>
     *
     * Get all postdated cheques
     * @param string $postdatedDate cheque postdated date
     * @return Mixed
     */
    public function getPostdatedCheques($postdatedDate,$locationId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array("*"))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('locationID', 'outgoingPaymentAmount'), 'left')
                ->join('account', 'outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID = account.accountId', array('bankId'), 'left')
                ->join('bank', 'account.bankId = bank.bankId', array('bankName'),'left')
        ->where->equalTo('outGoingPaymentMethodsNumbers.postdatedChequeStatus', 1)
        ->where->equalTo('outgoingPayment.locationID', $locationId)
        ->where->equalTo('outGoingPaymentMethodsNumbers.postdatedChequeDate', $postdatedDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    //get payment method details by payment ID
    public function getPaymentMethodDetailsByPaymentId($paymentId)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->join('account', 'outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID=account.accountId', array('*'), 'left');
        $select->where(array('outGoingPaymentMethodsNumbers.outGoingPaymentID' => $paymentId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentMethodDetailsById($Id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->join('account', 'outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID=account.accountId', array('*'), 'left');
        $select->where(array('outGoingPaymentMethodsNumbers.outGoingPaymentMethodsNumbersID' => $Id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //get payment method details by payment ID
    public function getPaymentMethodDetailsByPaymentIdAndPaymentMethodId($paymentId, $paymentMethodIDs)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->join('account', 'outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID=account.accountId', array('*'), 'left');
        $select->where(array('outGoingPaymentMethodsNumbers.outGoingPaymentID' => $paymentId));
        $select->where->in('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', $paymentMethodIDs);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
    
    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     *
     * Get advanced payment issued cheques by issued accountId.
     * @param string $accountId
     * @return mixed
     */
    public function getAdvancedPaymentIssuedChequesByAccountId($accountId = null, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','outgoingPaymentAmount','paymentMethodID'))
                ->join('supplier', ' supplier.supplierID= outgoingPayment.supplierID ', array('supplierName', 'supplierCode', 'supplierTitle'), 'left')
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.outgoingPaymentType', 'advance')
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 2);
        if (!is_null($accountId)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID', $accountId);
        }
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    
    public function getAdvancedPaymentIssuedChequesByAccountIdForDashboard($accountId = null, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','outgoingPaymentAmount','paymentMethodID'))
                ->join('supplier', ' supplier.supplierID= outgoingPayment.supplierID ', array('supplierName', 'supplierCode', 'supplierTitle'), 'left')
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.outgoingPaymentType', 'advance')
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 2);
        if (!is_null($accountId)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID', $accountId);
        }
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     *
     * Get advanced payment outgoing bank transfers by accountId.
     * @param string $accountId
     * @return mixed
     */
    public function getAdvancedPaymentOutgoingBankTransfersByAccountId($accountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','outgoingPaymentAmount','paymentMethodID'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 5)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodBankTransferAccountId', $accountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    public function getOutgoingAdvancedPaymentByGlAccountId($glAccountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','outgoingPaymentAmount','paymentMethodID'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodFinanceAccountID', $glAccountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get issue cheque by finacial account id 
    public function getIssuedChequesByfinancialAccountId($financeAccountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPaymentInvoice', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingInvoiceCashAmount', 'paymentMethodID'))
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPaymentInvoice.paymentMethodID', 2)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodFinanceAccountID', $financeAccountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get issued cheques
    public function getIssuedChequesList()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','supplierID'))
                ->join('supplier', ' supplier.supplierID= outgoingPayment.supplierID ', array('supplierName', 'supplierCode', 'supplierTitle'), 'left')
                // ->join('outgoingPaymentInvoice', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingPaymentAmount' => new Expression('SUM(outgoingInvoiceCashAmount)')))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->group('outgoingPaymentID')
        ->order(array('outGoingPaymentID' => 'DESC'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 2);
        // ->where->equalTo('outgoingPaymentInvoice.paymentMethodID', 2);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getSupplierChequesByPaymentId($paymentId = NULL, $supplierID = NULL, $startDate = NULL, $endDate = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','supplierID'))
                ->join('supplier', ' supplier.supplierID= outgoingPayment.supplierID ', array('supplierName', 'supplierCode', 'supplierTitle'), 'left')
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
                ->group('outgoingPaymentID')
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 2);

        if (!is_null($paymentId)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentID', $paymentId);
        }

        if (!is_null($supplierID)) {
            $select->where->equalTo('outgoingPayment.supplierID', $supplierID);
        }

        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    // get cheque advanced payments by gl account id
    public function getAdvancedPaymentIssuedChequesByGlAccountId($financeAccountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','outgoingPaymentAmount','paymentMethodID'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.paymentMethodID', 2)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodFinanceAccountID', $financeAccountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get out going bank transfer by gl account id
    public function getOutgoingBankTransfersByGlAccountId($financeAccountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPaymentInvoice', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingInvoiceCashAmount', 'paymentMethodID'))
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPaymentInvoice.paymentMethodID', 5)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodFinanceAccountID', $financeAccountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    // get outgoing advance payment bank transfer by gl account id
    public function getAdvancedPaymentOutgoingBankTransfersByGlAccountId($financeAccountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','outgoingPaymentAmount','paymentMethodID'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.paymentMethodID', 5)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodFinanceAccountID', $financeAccountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }


     // get outgoing advance cash payment by gl account id
    public function getAdvancedPaymentCashByGlAccountId($financeAccountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','outgoingPaymentAmount','paymentMethodID'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.paymentMethodID', 1)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodFinanceAccountID', $financeAccountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getOutgoingCashByGlAccountId($financeAccountId, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPaymentInvoice', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingInvoiceCashAmount', 'paymentMethodID'))
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPaymentInvoice.paymentMethodID', 1)
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodFinanceAccountID', $financeAccountId);
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && !$endDate) {
            $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }


    public function getIssuedAgedInvoiceCheques($paymentDate = null, $chequeDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPaymentInvoice', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPaymentInvoice.paymentID', array('outgoingInvoiceCashAmount', 'paymentMethodID'))
                ->join('outgoingPayment', 'outgoingPaymentInvoice.paymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPaymentInvoice.paymentMethodID', 2);

        if (!is_null($chequeDate)) {
            $select->where->greaterThanOrEqualTo('outGoingPaymentMethodsNumbers.postdatedChequeDate',$chequeDate);
        } else {
            $select->where->greaterThanOrEqualTo('outgoingPayment.outgoingPaymentDate',$paymentDate);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }


    public function getIssuedAgedAdvanceCheques($paymentDate = null, $chequeDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*'))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentCode', 'outgoingPaymentDate','outgoingPaymentType','outgoingPaymentAmount','paymentMethodID'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.paymentMethodID', 2);
        
        if (!is_null($chequeDate)) {
            $select->where->greaterThanOrEqualTo('outGoingPaymentMethodsNumbers.postdatedChequeDate',$chequeDate);
        } else {
            $select->where->greaterThanOrEqualTo('outgoingPayment.outgoingPaymentDate',$paymentDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

     /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     *
     * Get invoice payment issued cheques by issued accountId.
     * @param string $accountId
     * @return mixed
     */
    public function getOutgoingCashAmountOfIssuedCheques($accountId = null, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*','totalOutgoingAmount' => new Expression('SUM(outGoingPaymentMethodPaidAmount)')))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentDate'))
                // ->join('supplier', ' supplier.supplierID= outgoingPayment.supplierID ', array('supplierName', 'supplierCode', 'supplierTitle'), 'left')
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.outgoingPaymentType', 'invoice')
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 2);
        // if (!is_null($accountId)) {
        //     $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodChequeBankAccountID', $accountId);
        // }
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        // if ($startDate && !$endDate) {
        //     $select->where->greaterThan('outgoingPayment.outgoingPaymentDate', $startDate);
        // }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }


    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     *
     * Get advanced payment issued cheques by issued accountId.
     * @param string $accountId
     * @return mixed
     */
    public function getTotalOutgoingPaymentAmountInAdvancedPaymentIssuedCheques($accountId = null, $recStatus = null, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('outGoingPaymentMethodsNumbers')
                ->columns(array('*','totalOutgoingAmount' => new Expression('SUM(outGoingPaymentMethodPaidAmount)')))
                ->join('outgoingPayment', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('outgoingPaymentDate'))
                ->join('entity', 'outgoingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('outgoingPayment.outgoingPaymentType', 'advance')
        ->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 2);
        
        if (!is_null($recStatus)) {
            $select->where->equalTo('outGoingPaymentMethodsNumbers.outGoingPaymentMethodReconciliationStatus', $recStatus);
        }
        if ($startDate && $endDate) {
            $select->where->between('outgoingPayment.outgoingPaymentDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}
