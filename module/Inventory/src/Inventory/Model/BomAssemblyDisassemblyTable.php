<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Inventory\Model\BomAssemblyDisassembly;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class BomAssemblyDisassemblyTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveBomAssemblyDisassemblyDetails(BomAssemblyDisassembly $BomAssemblyDisassembly)
    {

        $data = array(
            'bomAssemblyDisassemblyType' => $BomAssemblyDisassembly->bomAssemblyDisassemblyType,
            'bomAssemblyDisassemblyReference' => $BomAssemblyDisassembly->bomAssemblyDisassemblyReference,
            'bomID' => $BomAssemblyDisassembly->bomID,
            'bomAssemblyDisassemblyParentQuantity' => $BomAssemblyDisassembly->bomAssemblyDisassemblyParentQuantity,
            'bomAssemblyDisassemblyComment' => $BomAssemblyDisassembly->bomAssemblyDisassemblyComment,
        );
        try {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        } catch (\Exception $exc) {
            echo $exc;
            return false;
        }
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bomAssemblyDisassembly');
        $select->join('bom', 'bomAssemblyDisassembly.bomID=  bom.bomID', array("productID"), "left");
        $select->join('product', 'bom.productID=  product.productID', array("productName"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->order('bomAssemblyDisassemblyID DESC');
        $select->where(array('entity.deleted' => 0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);

            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function bomAdSearchByKey($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bomAssemblyDisassembly');
        $select->join('bom', 'bomAssemblyDisassembly.bomID=  bom.bomID', array("productID"), "left");
        $select->join('product', 'bom.productID=  product.productID', array("productName"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(new PredicateSet(array(new Operator('bomAssemblyDisassembly.bomAssemblyDisassemblyReference', 'like', '%' . $searchKey . '%'), new Operator('bomAssemblyDisassembly.bomAssemblyDisassemblyComment', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getBomADDetailsByBomID($productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bomAssemblyDisassembly');
        $select->join('bom', 'bomAssemblyDisassembly.bomID=  bom.bomID', array("productID"), "left");
        $select->where(array('bom.productID' => $productID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
