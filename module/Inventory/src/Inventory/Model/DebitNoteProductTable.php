<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Debit Note Product Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DebitNoteProductTable
{

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDebitNoteProducts(DebitNoteProduct $debitNoteProduct)
    {
        $data = array(
            'debitNoteID' => $debitNoteProduct->debitNoteID,
            'purchaseInvoiceProductID' => $debitNoteProduct->purchaseInvoiceProductID,
            'productID' => $debitNoteProduct->productID,
            'debitNoteProductPrice' => $debitNoteProduct->debitNoteProductPrice,
            'debitNoteProductDiscount' => $debitNoteProduct->debitNoteProductDiscount,
            'debitNoteProductDiscountType' => $debitNoteProduct->debitNoteProductDiscountType,
            'debitNoteProductQuantity' => $debitNoteProduct->debitNoteProductQuantity,
            'debitNoteProductTotal' => $debitNoteProduct->debitNoteProductTotal,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDebitNoteProductsByInvoiceID($pvID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("debitNoteProduct")
                ->columns(array("*"))
                ->join("purchaseInvoiceProduct", "debitNoteProduct.purchaseInvoiceProductID=  purchaseInvoiceProduct.purchaseInvoiceProductID", array("*"), "left")
                ->join("grnProduct", "grnProduct.grnProductID=  purchaseInvoiceProduct.purchaseInvoiceProductDocumentID", array("grnID"), "left")
                ->where(array("purchaseInvoiceProduct.purchaseInvoiceID" => $pvID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDebitNoteProductAndSubProductDataByPvProductID($pvProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("debitNoteProduct")
                ->columns(array("*"))
                ->join('debitNoteSubProduct', 'debitNoteSubProduct.debitNoteProductID=debitNoteProduct.debitNoteProductID', array('debitNoteSubProductID', 'productBatchID', 'productSerialID', 'debitNoteSubProductQuantity'), 'left')
                ->join('debitNote', 'debitNoteProduct.debitNoteID=debitNote.debitNoteID', array('*'), 'left')
                ->join('entity', 'debitNote.entityID=entity.entityID', array('*'), 'left')
                ->where(array('debitNoteProduct.purchaseInvoiceProductID' => $pvProductID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getAllDebitNoteProductDetailsByDebitNoteID($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNoteProduct');
        $select->join(
                'product', 'debitNoteProduct.productID = product.productID', array(
            '*',
                )
        );
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->where(array('productUomBase' => '1'));
        $select->where(array('debitNoteProduct.debitNoteID' => $debitNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
