<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Debit Note Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DebitNoteTable
{

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('debitNote')
                    ->columns(array('*'))
                    ->join('supplier', 'debitNote.supplierID = supplier.supplierID', array('supplierName', 'supplierCode'), 'left')
                    ->order(array('debitNoteDate' => 'DESC'))
                    ->where(array('locationID' => $locationID));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote');
        $select->where(array('locationID' => $locationID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function checkDebitNoteByCode($debitNoteCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNote.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('debitNote.debitNoteCode' => $debitNoteCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getDebitNotesByPvID($pvID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNote.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('debitNote.purchaseInvoiceID' => $pvID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function saveDebitNote(DebitNote $debitNote)
    {
        $data = array(
            'debitNoteCode' => $debitNote->debitNoteCode,
            'supplierID' => $debitNote->supplierID,
            'debitNoteDate' => $debitNote->debitNoteDate,
            'debitNoteTotal' => $debitNote->debitNoteTotal,
            'locationID' => $debitNote->locationID,
            'paymentTermID' => $debitNote->paymentTermID,
            'purchaseInvoiceID' => $debitNote->purchaseInvoiceID,
            'debitNoteComment' => $debitNote->debitNoteComment,
            'statusID' => $debitNote->statusID,
            'debitNotePaymentEligible' => $debitNote->debitNotePaymentEligible,
            'debitNotePaymentAmount' => $debitNote->debitNotePaymentAmount,
            'entityID' => $debitNote->entityID,
            'directDebitNoteFlag' => $debitNote->directDebitNoteFlag,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDebitNoteByDebitNoteID($debitNoteID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNote.entityID = e.entityID")
                    ->join('supplier', "debitNote.supplierID = supplier.supplierID")
                    ->join('debitNotePaymentDetails', "debitNote.debitNoteID = debitNotePaymentDetails.debitNoteID",array('debitNoteSettled' => new Expression('SUM(debitNotePaymentDetails.debitNotePaymentDetailsAmount)')))
                    ->join('debitNotePayment', "debitNotePaymentDetails.debitNotePaymentID = debitNotePayment.debitNotePaymentID",array('debitNoteSettledAmount' => new Expression('SUM(debitNotePayment.debitNotePaymentAmount)')))
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->where(array('e.deleted' => '0'))
                    ->where(array('debitNote.debitNoteID' => $debitNoteID,));
            $select->where->notEqualTo('debitNotePayment.statusID',5);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getDebitNoteByDebitNoteIDForJE($debitNoteID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNote.entityID = e.entityID")
                    ->join('supplier', "debitNote.supplierID = supplier.supplierID")
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->where(array('e.deleted' => '0'))
                    ->where(array('debitNote.debitNoteID' => $debitNoteID,));
            $select->where->notEqualTo('debitNote.statusID', 5);
            $select->where->notEqualTo('debitNote.statusID', 10);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getDebitNoteBySupplierIDAndLocationID($supplierID, $locationID, $groupFlag = false)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNote.entityID = e.entityID")
                    ->join('supplier', "debitNote.supplierID = supplier.supplierID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('debitNote.supplierID' => $supplierID, 'debitNote.locationID' => $locationID));
            if ($groupFlag) {
                $select->join('debitNotePaymentDetails', "debitNote.debitNoteID = debitNotePaymentDetails.debitNoteID",
                            array('debitNoteSettled' => new Expression('SUM(debitNotePaymentDetails.debitNotePaymentDetailsAmount)')),'left')
                        ->join('debitNotePayment', "debitNotePaymentDetails.debitNotePaymentID = debitNotePayment.debitNotePaymentID",array('debitNoteSettledAmount' => new Expression('SUM(debitNotePayment.debitNotePaymentAmount)')),'left')
                        ->group('debitNote.debitNoteID');
            }
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getDebitNotesByDate($fromdate, $todate, $supplierID = NULL, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("debitNote")
                ->columns(array("*"))
                ->join("supplier", "debitNote.supplierID = supplier.supplierID", array("supplierName", "supplierCode"), "left")
                ->order(array('debitNoteDate' => 'DESC'))
                ->where(array("debitNote.locationID" => $locationID));
        if ($supplierID != NULL) {
            $select->where(array("debitNote.supplierID" => $supplierID));
        }
        $select->where->between('debitNoteDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getActiveDebitNote($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("debitNote")
                ->columns(array("*"))
                ->order(array('debitNoteDate' => 'DESC'))
                ->where(array("debitNote.locationID" => $locationID, 'statusID' => '3', 'debitNotePaymentEligible' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function updateDebitNoteSettleAmountAndStatusID(DebitNote $debitNote)
    {
        $debitNoteData = array(
            'debitNoteSettledAmount' => $debitNote->debitNoteSettledAmount,
            'statusID' => $debitNote->statusID,
        );
        $this->tableGateway->update($debitNoteData, array('debitNoteID' => $debitNote->debitNoteID));
        return TRUE;
    }

    public function supplierDebitNoteDetails($supplierId, $isAllSuppliers = false)
    {
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('supplier')
                ->columns(array('*'))
                ->join('debitNote', 'supplier.supplierID = debitNote.supplierID', array(
                    'totDebitNote' => new Expression('SUM(debitNote.debitNoteTotal )')
                        ), 'left')
                ->join('entity', 'supplier.entityID = entity.entityID', array('deleted'), 'left')
                ->group('supplier.supplierID')
                ->order('supplier.supplierName ASC');
        if (!$isAllSuppliers) {
            $select->where->in('supplier.supplierID', $supplierId);
        }
        $select->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    /**
     * Search Debit note For Dropdown
     * @param type $searchKey
     * @param type $locationID
     * @return null
     */
    public function searchDebitNoteForDropdown($searchKey, $locationID = false, $isActive = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote');
        $select->where(array('locationID' => $locationID));
        $select->where->like('debitNoteCode', '%' . $searchKey . '%');
        if ($isActive) {
            $select->where(array('statusID' => '3', 'debitNotePaymentEligible' => '1'));
        }
        $select->limit(50);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getDebitNoteByCode($debitNoteCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote');
        $select->join('location', 'location.locationID = debitNote.locationID', array('locationName', 'locationCode'));
        $select->where(array('debitNoteCode' => $debitNoteCode));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getDebitNoteById($debitNoteId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote');
        $select->join('location', 'location.locationID = debitNote.locationID', array('locationName', 'locationCode'));
        $select->where(array('debitNoteID' => $debitNoteId));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    /**
    * get all debit note details by given supplier IDs
    *
    **/
    public function getDebitNoteBySupplierIDs($supplierID = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "debitNote.entityID = e.entityID", array('deleted','createdTimeStamp'))
                ->join('supplier', "debitNote.supplierID = supplier.supplierID", array('*'), 'left')
                ->join("status", "debitNote.statusID = status.statusID", array("*"), "left")
                ->where(array('e.deleted' => '0'));
        $select->where->in('debitNote.supplierID', $supplierID);
        $select->where->in('debitNote.statusID', $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('debitNote.debitNoteDate', $fromdate, $todate);
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
        
    }

    public function getPiDebitNoteTotalByPiId($piId) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote')
                ->columns([
                    'total' => new Expression('SUM(debitNoteTotal)')
                ])
                ->join('entity', "debitNote.entityID = entity.entityID", ['deleted'])
                ->where(['entity.deleted' => '0'])
                ->where(['debitNote.purchaseInvoiceID' => $piId]);
        $query = $sql->prepareStatementForSqlObject($select);
        return $query->execute()->current();
    }

    // get po related debit note
    public function getDirectPoRelatedDebitNoteDataByPoId($poId = null, $grnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNote');
        $select->columns(array('*'));
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('entity','debitNote.entityID = entity.entityID',array('*'),'left');
        if (!is_null($poId)) {
            $select->where(array('purchaseInvoice.purchaseInvoicePoID' => $poId));
        }
        if (!is_null($grnID)) {
            $select->where(array('purchaseInvoice.purchaseInvoiceGrnID' => $grnID));
        }
        $select->group("debitNote.debitNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get po related debit note by grn
    public function getPoRelatedDebitNoteDataByPoId($poId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNote');
        $select->columns(array('*'));
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('grnProduct','purchaseInvoice.purchaseInvoiceGrnID = grnProduct.grnID',array('grnID'),'left');
        $select->join('entity','debitNote.entityID = entity.entityID',array('*'),'left');
        $select->where(array('grnProduct.grnProductDocumentId' => $poId, 'grnProduct.grnProductDocumentTypeId' => 9));
        $select->group("debitNote.debitNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }


    // get pi related debit note
    public function getPiRelatedDebitNoteDataByPiId($piId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNote');
        $select->columns(array('*'));
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('entity','debitNote.entityID = entity.entityID',array('*'),'left');
        $select->where(array('purchaseInvoice.purchaseInvoiceID' => $piId));
        $select->group("debitNote.debitNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get po related debit note by pr id
    public function getPoRelatedDebitNoteDataByPrId($prId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNote');
        $select->columns(array('*'));
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('purchaseOrder','purchaseInvoice.purchaseInvoicePoID = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('grnProduct','grnProduct.grnProductDocumentId = purchaseOrder.purchaseOrderID',array('*'),'left');
        $select->join('purchaseReturn','grnProduct.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
        $select->join('entity', 'debitNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseReturn.purchaseReturnID' => $prId,'grnProduct.grnProductDocumentTypeId' => 9));
        $select->group("debitNote.debitNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get grn related debit note by pr id
    public function getGrnRelatedDebitNoteDataByPrId($prId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNote');
        $select->columns(array('*'));
        $select->join('purchaseInvoice','debitNote.purchaseInvoiceID = purchaseInvoice.purchaseInvoiceID',array('purchaseInvoiceID'),'left');
        $select->join('grn','grn.grnID = purchaseInvoice.purchaseInvoiceGrnID',array('*'),'left');
        $select->join('purchaseReturn','grn.grnID = purchaseReturn.purchaseReturnGrnID',array('*'),'left');
        $select->join('entity', 'debitNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('purchaseReturn.purchaseReturnID' => $prId));
        $select->group("debitNote.debitNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get debit note basic details with time stamp by dn id 
    public function getDebitNoteDetailsByDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNote');
        $select->columns(array('*'));
        $select->join('entity', 'debitNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('debitNote.debitNoteID' => $debitNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;
    }
    
    public function getDebitNoteBySupplierIDsForSupplierBalance($supplierID = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "debitNote.entityID = e.entityID", array('deleted','createdTimeStamp'))
                ->join('supplier', "debitNote.supplierID = supplier.supplierID", array('*'), 'left')
                ->join("status", "debitNote.statusID = status.statusID", array("*"), "left")
                ->where(array('e.deleted' => '0'));
        $select->where->in('debitNote.supplierID', $supplierID);
        $select->where->in('debitNote.statusID', $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('e.createdTimeStamp', $fromdate, $todate);
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
        
    }

    public function getPurchaseInvoiceRelatedDebitNotesForInvoiceRemainingAmount($purchaseInvoiceID, $status, $debitNoteID)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('debitNote')
                ->columns(array('supplierID','TotalDebitAmountAfterInvoice' => new \Zend\Db\Sql\Expression('SUM(debitNoteTotal)')))
                ->join('supplier', 'supplier.supplierID = debitNote.supplierID', array(),'left')
                ->join('status', 'debitNote.statusID = status.statusID',array(),'left')
                ->join('entity', 'supplier.entityID = entity.entityID', array(), 'left')
                ->where(array('entity.deleted' => '0', 'debitNote.purchaseInvoiceID' => $purchaseInvoiceID));
                $select->where->in('debitNote.statusID',$status);
                $select->where->lessThan('debitNote.debitNoteID', $debitNoteID);
        $select->group(array("debitNote.purchaseInvoiceID"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 0) {
            return NULL;
        }
        return $results->current();
    }

    public function getPiDebitNoteTotalByPiIdForSupplierBalance($piId, $date) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote')
                ->columns([
                    'total' => new Expression('SUM(debitNoteTotal)')
                ])
                ->join('entity', "debitNote.entityID = entity.entityID", ['deleted'])
                ->where(['entity.deleted' => '0'])
                ->where(['debitNote.purchaseInvoiceID' => $piId]);
        $select->where->lessThan('entity.createdTimeStamp', $date);
        $query = $sql->prepareStatementForSqlObject($select);
        return $query->execute()->current();
    }

    public function getDebitNoteTotalByDateRangeAndLocationID($fromdate, $todate, $locationID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote')
                ->columns([
                    'total' => new Expression('SUM(debitNoteTotal)')
                ])
                ->join('entity', "debitNote.entityID = entity.entityID", ['deleted'])
                ->where(['entity.deleted' => '0', 'debitNote.locationID' => $locationID]);
        $select->where->between('debitNote.debitNoteDate', $fromdate, $todate);
        $query = $sql->prepareStatementForSqlObject($select);
        return $query->execute()->current();
    }

    public function getDebitNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromdate, $todate, $locationID, $invoiceIDs) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('debitNote')
                ->columns([
                    'total' => new Expression('SUM(debitNoteTotal)')
                ])
                ->join('entity', "debitNote.entityID = entity.entityID", ['deleted'])
                ->where(['entity.deleted' => '0', 'debitNote.locationID' => $locationID]);
        $select->where->in('debitNote.purchaseInvoiceID', $invoiceIDs);
        $select->where->between('debitNote.debitNoteDate', $fromdate, $todate);
        $query = $sql->prepareStatementForSqlObject($select);
        return $query->execute()->current();
    }

     public function getDebitNoteByDebitNoteIDForRec($debitNoteID, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('debitNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "debitNote.entityID = e.entityID")
                    ->where(array('debitNote.debitNoteID' => $debitNoteID));
                $select->where(array('e.deleted' => '0'));
            
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }
}
