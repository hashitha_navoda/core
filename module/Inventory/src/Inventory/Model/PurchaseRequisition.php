<?php
namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseRequisition implements InputFilterAwareInterface
{

    public $purchaseRequisitionID;
    public $purchaseRequisitionCode;
    public $purchaseRequisitionSupplierID;
    public $purchaseRequisitionSupplierReference;
    public $purchaseRequisitionRetrieveLocation;
    public $purchaseRequisitionEstDelDate;
    public $purchaseRequisitionDate;
    public $purchaseRequisitionDescription;
    public $purchaseRequisitionDeliveryCharge;
    public $purchaseRequisitionTotal;
    public $purchaseRequisitionShowTax;
    public $status;
    public $entityID;
    public $purchaseRequisitionDraftFlag;
    public $purchaseRequisitionType;
    public $purchaseRequisitionApproved;
    public $purchaseRequisitionHashValue;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseRequisitionID = (!empty($data['purchaseRequisitionID'])) ? $data['purchaseRequisitionID'] : null;
        $this->purchaseRequisitionCode = (!empty($data['purchaseRequisitionCode'])) ? $data['purchaseRequisitionCode'] : null;
        $this->purchaseRequisitionSupplierID = (!empty($data['purchaseRequisitionSupplierID'])) ? $data['purchaseRequisitionSupplierID'] : null;
        $this->purchaseRequisitionSupplierReference = (!empty($data['purchaseRequisitionSupplierReference'])) ? $data['purchaseRequisitionSupplierReference'] : null;
        $this->purchaseRequisitionRetrieveLocation = (!empty($data['purchaseRequisitionRetrieveLocation'])) ? $data['purchaseRequisitionRetrieveLocation'] : null;
        $this->purchaseRequisitionEstDelDate = (!empty($data['purchaseRequisitionEstDelDate'])) ? $data['purchaseRequisitionEstDelDate'] : null;
        $this->purchaseRequisitionDate = (!empty($data['purchaseRequisitionDate'])) ? $data['purchaseRequisitionDate'] : null;
        $this->purchaseRequisitionDescription = (!empty($data['purchaseRequisitionDescription'])) ? $data['purchaseRequisitionDescription'] : null;
        $this->purchaseRequisitionDeliveryCharge = (!empty($data['purchaseRequisitionDeliveryCharge'])) ? $data['purchaseRequisitionDeliveryCharge'] : 0.00;
        $this->purchaseRequisitionTotal = (!empty($data['purchaseRequisitionTotal'])) ? $data['purchaseRequisitionTotal'] : 0.00;
        $this->purchaseRequisitionShowTax = (!empty($data['purchaseRequisitionShowTax'])) ? $data['purchaseRequisitionShowTax'] : 0;
        $this->status = (!empty($data['status'])) ? $data['status'] : 3;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->purchaseRequisitionDraftFlag = (!empty($data['purchaseRequisitionDraftFlag'])) ? $data['purchaseRequisitionDraftFlag'] : 0;
        $this->purchaseRequisitionType = (!empty($data['purchaseRequisitionType'])) ? $data['purchaseRequisitionType'] : null;
        $this->purchaseRequisitionApproved = (!empty($data['purchaseRequisitionApproved'])) ? $data['purchaseRequisitionApproved'] : null;
        $this->purchaseRequisitionHashValue = (!empty($data['purchaseRequisitionHashValue'])) ? $data['purchaseRequisitionHashValue'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

