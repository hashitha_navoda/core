<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class GrnProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveGrnProductTax(GrnProductTax $grnProductTax)
    {
        $data = array(
            'grnID' => $grnProductTax->grnID,
            'grnProductID' => $grnProductTax->grnProductID,
            'grnTaxID' => $grnProductTax->grnTaxID,
            'grnTaxPrecentage' => $grnProductTax->grnTaxPrecentage,
            'grnTaxAmount' => $grnProductTax->grnTaxAmount,
        );
        $this->tableGateway->insert($data);
    }

    public function getGrnProductTax($grnID = NULL, $locationProductID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("grnProductTax")
                ->columns(array("*"))
                ->join("grnProduct", "grnProduct.grnProductID = grnProductTax.grnProductID", array("*"))
                ->where(array(
                    'grnProduct.grnID' => $grnID,
                    'grnProduct.locationProductID' => $locationProductID))
                ->group('grnTaxID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $data = [];
        foreach ($results as $result) {
            $data[] = $result;
        }
        return $data;
    }

    /**
     * Get product tax details
     * @param string $grnId
     * @param string $locationProductId
     * @return mixed
     */
    public function getGrnProductTaxDetails($grnId, $locationProductId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("grnProductTax")
                ->columns(['*'])
                ->join("grnProduct", "grnProduct.grnProductID = grnProductTax.grnProductID", ['locationProductID'], 'left')
                ->join("tax", "grnProductTax.grnTaxID = tax.id", ['taxName'], 'left')
        ->where->equalTo('grnProductTax.grnID', $grnId)
        ->where->equalTo('grnProduct.locationProductID', $locationProductId);
        $select->group(['grnProductTax.grnProductID','grnProductTax.grnProductTaxID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getGrnProductTaxDetailByGrnIDAndGrnProductID($grnID, $grnProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("grnProductTax")
                ->columns(['*'])
        ->where->equalTo('grnProductTax.grnID', $grnID)
        ->where->equalTo('grnProductTax.grnProductID', $grnProductID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateGrnProductTax($data,$grnProductTaxID)
    {
        try {
            $this->tableGateway->update($data,array('grnProductTaxID' => $grnProductTaxID));
            return true;
        } catch (Exception $e) {
            return false;   
        }
        
    }

    public function deleteGrnProductTax($grnID, $grnProductID) {
        try {
            $result = $this->tableGateway->delete(array('grnID' => $grnID, 'grnProductID' => $grnProductID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function deleteGrnProductTaxByGrnProductTaxID($grnProductTaxID) {
        try {
            $result = $this->tableGateway->delete(array('grnProductTaxID' => $grnProductTaxID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }   
}
