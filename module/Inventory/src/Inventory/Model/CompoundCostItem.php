<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CompoundCostItem
{

    public $compoundCostItemID;
    public $productID;
    public $locationProductID;
    public $compoundCostItemQty;
    public $compoundCostItemPurchasePrice;
    public $compoundCostItemCostAmount;
    public $compoundCostItemTotalUnitCost;
    public $compoundCostTemplateID;
    public $compoundCostItemWiseTotalCost;
    public $supplierID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->compoundCostItemID = (!empty($data['compoundCostItemID'])) ? $data['compoundCostItemID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->compoundCostItemQty = (!empty($data['compoundCostItemQty'])) ? $data['compoundCostItemQty'] : null;
        $this->compoundCostItemPurchasePrice = (!empty($data['compoundCostItemPurchasePrice'])) ? $data['compoundCostItemPurchasePrice'] : null;
        $this->compoundCostItemCostAmount = (!empty($data['compoundCostItemCostAmount'])) ? $data['compoundCostItemCostAmount'] : null;
        $this->compoundCostItemTotalUnitCost = (!empty($data['compoundCostItemTotalUnitCost'])) ? $data['compoundCostItemTotalUnitCost'] : null;
        $this->compoundCostTemplateID = (!empty($data['compoundCostTemplateID'])) ? $data['compoundCostTemplateID'] : null;
        $this->compoundCostItemWiseTotalCost = (!empty($data['compoundCostItemWiseTotalCost'])) ? $data['compoundCostItemWiseTotalCost'] : null;
        $this->supplierID = (!empty($data['supplierID'])) ? $data['supplierID'] : null;
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}