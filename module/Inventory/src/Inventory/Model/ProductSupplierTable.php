<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Supplier Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class ProductSupplierTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveProductSupplier($data)
    {
        $data = array(
            'supplierID' => $data->supplierID,
            'productID' => $data->productID,
        );

        try {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function deleteProductSupplier($supplierID)
    {
        $this->tableGateway->delete(array('supplierID' => $supplierID));
    }

    public function deleteProductSupplierByProductIDAndSupplierID($productID, $supplierID)
    {
        $this->tableGateway->delete(array('productID' => $productID, 'supplieriD' => $supplierID));

        return true;
    }

    public function deleteProductSupplierByProductID($productID)
    {
        $this->tableGateway->delete(array('productID' => $productID));
    }

    public function getProductSupplierByProductID($productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productSupplier');
        $select->columns(array('*'));
        $select->join('supplier', 'supplier.supplierID = productSupplier.supplierID', array('*'), 'left');
        $select->where(array('productSupplier.productID' => $productID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * Get product wise supplier details
     * @param array $productIds
     * @return mixed
     */
    public function getProductSuppliersByProductIds($productIds = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('productSupplier');
        $select->columns(array('*'));
        $select->join('supplier', 'supplier.supplierID = productSupplier.supplierID', array('*'), 'left');
        if ($productIds) {
            $select->where->in('productSupplier.productID', $productIds);
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        $supplierList = [];
        foreach ($rowset as $row) {
            $supplierList[$row['productID']][] = $row['supplierName'];
        }
        return $supplierList;
    }

}
