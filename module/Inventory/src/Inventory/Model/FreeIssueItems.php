<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class FreeIssueItems
{

    public $freeIssueItemsID;
    public $freeIssueDetailsID;
    public $productID;
    public $quantity;
    public $selectedUom;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->freeIssueItemsID = (!empty($data['freeIssueItemsID'])) ? $data['freeIssueItemsID'] : null;
        $this->freeIssueDetailsID = (!empty($data['freeIssueDetailsID'])) ? $data['freeIssueDetailsID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->quantity = (!empty($data['quantity'])) ? $data['quantity'] : null;
        $this->selectedUom = (!empty($data['selectedUom'])) ? $data['selectedUom'] : null;
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
