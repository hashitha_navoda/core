<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DraftGrnProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveGrnProduct($grnProduct)
    {
        $data = array(
            'grnID' => $grnProduct->grnID,
            'locationProductID' => $grnProduct->locationProductID,
            'productBatchID' => $grnProduct->productBatchID,
            'productSerialID' => $grnProduct->productSerialID,
            'grnProductPrice' => $grnProduct->grnProductPrice,
            'grnProductDiscount' => $grnProduct->grnProductDiscount,
            'grnProductTotalQty' => $grnProduct->grnProductTotalQty,
            'grnProductQuantity' => $grnProduct->grnProductQuantity,
            'grnProductUom' => $grnProduct->grnProductUom,
            'grnProductTotal' => $grnProduct->grnProductTotal,
            'rackID' => $grnProduct->rackID,
            'grnProductDocumentId' => $grnProduct->grnProductDocumentId,
            'grnProductDocumentTypeId' => $grnProduct->grnProductDocumentTypeId,
            'grnProductDoumentTypeCopiedQuantity' => $grnProduct->grnProductDoumentTypeCopiedQuantity,
            'grnProductDiscountType' => $grnProduct->grnProductDiscountType,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function getGrnProductDetails($grnID, $notCopied = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct');
        $select->join('locationProduct', 'grnProduct.locationProductID = locationProduct.locationProductID', array('*'),'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productID'),"left");
        $select->where(array('grnID' => $grnID));
        if ($notCopied) {
            $select->where(array('copied' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function updateCopiedGrnProducts($locationPID, $grnID)
    {
        $data = array(
            'copied' => 1
        );
        $this->tableGateway->update($data, array('locationProductID' => $locationPID, 'grnID' => $grnID));
    }

    public function updateCopiedGrnProductsByPi($piIndexID, $grnID)
    {
        $data = array(
            'copied' => 1
        );
        $this->tableGateway->update($data, array('grnProductID' => $piIndexID, 'grnID' => $grnID));
    }

    public function getUnCopiedGrnProducts($grnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct');
        $select->where(array('grnID' => $grnID));
        $select->where(array('copied' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getStockValueData($pIds, $locIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct');
        $select->columns(array('grnProductTotalQty', 'grnProductID', 'grnProductPrice', 'grnProductQuantity', 'mTGRN' => new Expression('grnProduct.grnProductPrice*grnProduct.grnProductQuantity')));
        $select->join('locationProduct', 'locationProduct.locationProductID = grnProduct.locationProductID', array('productID'), 'left');
        $select->join('location', 'location.locationID = locationProduct.locationID', array('locationName', 'locationID', 'locationCode'), 'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productName', 'productCode'), 'left');
        $select->order('product.productID', 'grnProduct.grnID');
        $select->group(array('grnProduct.grnID', 'product.productID'));
        $select->where(array('grnProductQuantity IS NOT NULL', 'grnProductPrice IS NOT NULL'));
        if ($locIds != NULL) {
            $select->where->in('locationProduct.locationID', $locIds);
        }
        if ($pIds != NULL) {
            $select->where->in('product.productID', $pIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function CheckGrnProductByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct')
                ->columns(array('*'))
                ->where(array('locationProductID' => $locationProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function getGrnProductByLocationProductID($locationProductID, $sortByDate = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct');
        $select->columns(array('*', 'itemInPrice' => new Expression('grnProductPrice')));
        $select->join('grn', 'grnProduct.grnID = grn.grnID', array('itemInDate' => new Expression('grnDate'), 'grnID'), 'left');
        if ($sortByDate) {
            $select->order(array('itemInDate DESC'));
        }
        $select->where(array('locationProductID' => $locationProductID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = array();
        foreach ($results as $result) {
            $result['documentID'] = $result['grnID'];
            $result['documentType'] = 'Goods Received Note';
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function getGrnDiscountByProductID($productID, $locationID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn');
        $select->columns(array('grnID'));
        $select->join('grnProduct', 'grnProduct.grnID = grn.grnID', array(
            'grnProductDiscount', 'grnProductTotalQty', 'grnProductPrice'));
        $select->join('locationProduct', 'locationProduct.locationProductID = grnProduct.locationProductID');
        $select->join('product', 'locationProduct.productID = product.productID');
        $select->where(array('product.productID' => $productID));
        if ($locationID != NULL) {
            $select->where(array('grn.grnRetrieveLocation' => $locationID));
        }
        $select->order("grnProduct.grnProductID");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }
    
    /**
     * Get invoice products by grn id
     * @param string $grnId
     * @param array $productIds
     * @return type
     */
    public function getGrnProductDetailsByGrnId($grnId, $productIds = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct',['grnProductID', 'locationProductID', 'grnProductPrice', 
                    'grnProductQuantity' => new Expression('SUM(grnProductQuantity)'), 
                    'grnProductTotal' => new Expression('SUM(grnProductTotal)')
                    ], 'left')
                ->join('locationProduct', 'grnProduct.locationProductID = locationProduct.locationProductID', ['productID','locationID'], 'left')
                ->join('product', 'locationProduct.productID = product.productID', ['productCode', 'productName'], 'left')
                ->where->equalTo('grnProduct.grnID', $grnId);
        if ($productIds) {
            $select->where->in('locationProduct.productID', $productIds);
        }
        $select->group("grnProduct.locationProductID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updateGrnProductsByGrnProductID($data, $grnProductID)
    {
        try {
            $this->tableGateway->update($data, array('grnProductID' => $grnProductID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getGrnProductsByGrnIdLpidPsidAndPbid($grnId, $lpid, $psid, $pbid,$grnProIndex = null,$pUP = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct');
        $select->columns(array('*'));
        $select->where(array('grnProduct.grnID' => $grnId, 'grnProduct.locationProductID' => $lpid ));
        if($psid){
            $select->where(array('grnProduct.productSerialID' => $psid));
        }
        if($pbid){
            $select->where(array('grnProduct.productBatchID' => $pbid));
        }
        if($grnProIndex){
            $select->where(array('grnProduct.grnProductID' => $grnProIndex));
        }
        if($pUP){
            $select->where(array('grnProduct.grnProductPrice' => $pUP));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateGrnProductsByLocationProductIDAndGrnID($data, $locationPID, $grnID , $grnPID = null)
    {
        try {

            $conditions = (!is_null($grnPID)) ? ['grnProductID' => $grnPID, 'grnID' => $grnID] : ['locationProductID' => $locationPID, 'grnID' => $grnID];
            $this->tableGateway->update($data, $conditions);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Get purchasing order wise copied grn items
     * @param int $poId
     * @return mixed
     */
    public function getCopiedGrnItemsByPoId($poId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct')
                ->columns(['*'])
                ->join('grn', 'grnProduct.grnID = grn.grnID', [
                    'grnCode',
                    'grnDate',
                    'copiedQty' => new Expression('SUM(grnProductDoumentTypeCopiedQuantity)')
                    ], 'left');
        $select->where->equalTo('grnProductDocumentId', $poId)
               ->where->equalTo('grnProductDocumentTypeId', 9);
        $select->group("grnProduct.grnID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getgrnProductQtyBylocationIDAndGrnID($grnID, $locationProductID, $pUP)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct')
                ->columns(array(
                    'grnProductTotalQty' => new Expression('SUM(grnProductTotalQty)'),
                    'grnProductID',
                    'locationProductID',
                    'grnID'));
                
        $select->where(array('grnID' => $grnID, 'locationProductID' => $locationProductID, 'grnProductPrice' => $pUP));
    
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;

    }

    public function updateSerilaProductCopyStateByCanceledPI($serialIDs)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $update = $sql->update();
        $update->table('grnProduct')
                ->set(array('copied' => '0', 'grnProductCopiedQuantity' => '0'))
                ->where->in('productSerialID', $serialIDs);
        
        $statement = $sql->prepareStatementForSqlObject($update);
        $results    = $statement->execute();

    }

    public function updateCopiedGrnProductsQtyByPi($locationProductID, $qty)
    {
        $data = array(
            'grnProductTotalCopiedQuantity' => $qty
        );
        $this->tableGateway->update($data, array('locationProductID' => $locationProductID));
    }

    public function getGrnAllBatchSerialProductDetailsByGrnId($grnId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct',['grnProductID', 'locationProductID', 'grnProductPrice', 
                    'grnProductQuantity', 
                    'grnProductTotal'
                    ], 'left')
                ->join('locationProduct', 'grnProduct.locationProductID = locationProduct.locationProductID', ['productID','locationID'], 'left')
                ->join('product', 'locationProduct.productID = product.productID', ['productCode', 'productName'], 'left')
                ->where->equalTo('grnProduct.grnID', $grnId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getGrnProductDetailsByGrnProductID($grnProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct');
        $select->join('locationProduct', 'grnProduct.locationProductID = locationProduct.locationProductID', ['productID','locationID'], 'left');
        $select->where(array('grnProductID' => $grnProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getGrnProductLocationProductDetailsByGrnID($grnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct');
        $select->where(array('grnID' => $grnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getGrnLocationProductDetailsByGrnId($grnId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct',['locationProductID'])
                ->where->equalTo('grnProduct.grnID', $grnId);
        $select->group("grnProduct.locationProductID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getGrnProductDetailsByGrnIdAndLocationProductId($grnId, $locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct',['grnProductID', 'locationProductID', 'grnProductPrice', 
                    'grnProductQuantity' => new Expression('SUM(grnProductQuantity)'), 
                    'grnProductTotal' => new Expression('SUM(grnProductTotal)')
                    ], 'left')
                ->join('locationProduct', 'grnProduct.locationProductID = locationProduct.locationProductID', ['productID','locationID'], 'left')
                ->join('product', 'locationProduct.productID = product.productID', ['productCode', 'productName'], 'left')
                ->where->equalTo('grnProduct.grnID', $grnId)
                ->where->equalTo('grnProduct.locationProductID', $locationProductID);
        $select->group("grnProduct.grnProductPrice");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updateCopiedGrnProductsByGrnProductIdAndLocationProductId($locationPID, $grnProductID)
    {
        $data = array(
            'copied' => 1
        );
        $this->tableGateway->update($data, array('locationProductID' => $locationPID, 'grnProductID' => $grnProductID));
    }

    //get po related grn by po id
    public function getGrnDetailsByPoId($poId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct')
                ->columns(['*'])
                ->join('grn', 'grnProduct.grnID = grn.grnID', array('*'), 'left')
                ->join('entity', 'grn.entityID = entity.entityID', array('*'), 'left');
        $select->where->equalTo('grnProductDocumentId', $poId)
               ->where->equalTo('grnProductDocumentTypeId', 9);
        $select->group("grnProduct.grnID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }


    public function getGrnProductWithTaxDetailsByGrnID($grnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct')
                ->columns(['*'])
                ->join('grnProductTax', 'grnProduct.grnProductID = grnProductTax.grnProductID', array('*'), 'left');
        $select->where(array('grnProduct.grnID' => $grnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    /**
    * this function use to update grn copied document id
    * @param array $data
    * @param int $copyDocumentTypeID
    * @param int  $copyDocumentID
    * return boolean
    **/
    public function updateCopiedID($data, $copyDocumentTypeID, $copyDocumentID)
    {
        try {
            $this->tableGateway->update($data, array('grnProductDocumentTypeId' => $copyDocumentTypeID, 'grnProductDocumentId' => $copyDocumentID));
            return true;
        } catch (Exception $e) {
            error_log($e);
            return false;
        }
    }

    /**
     * This function is used to get grn total qty by location product id and grn id
     *
     */
    public function getgrnProductTotalQtyBylocationIDAndGrnID($grnID, $locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct')
                ->columns(array(
                    'grnProductTotalQty' => new Expression('SUM(grnProductTotalQty)'),
                    'grnProductID',
                    'locationProductID',
                    'grnID'));
                
        $select->where(array('grnID' => $grnID, 'locationProductID' => $locationProductID));
    
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getgrnProductDetailsBylocationIDAndGrnIDAndUnitPrice($grnID, $locationProductID, $pUP)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grnProduct')
                ->columns(array('*'));
                
        $select->where(array('grnID' => $grnID, 'locationProductID' => $locationProductID, 'grnProductPrice' => $pUP));
    
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
