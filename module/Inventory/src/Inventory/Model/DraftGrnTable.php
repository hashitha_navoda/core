<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class DraftGrnTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adpater = $this->tableGateway->getAdapter();
        $grn = new TableGateway('grn', $adpater);
        $rowset = $grn->select(function (Select $select) {
            $select->order('grnDate ASC');
        });
        return $rowset;
    }

    public function fetchDescAll()
    {
        $adpater = $this->tableGateway->getAdapter();
        $grn = new TableGateway('grn', $adpater);
        $rowset = $grn->select(function (Select $select) {
            $select->order('grnDate DESC');
        });
        return $rowset;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param Paginator true or false
     * @return paginator object if true, else resultset
     */
    public function getGrns($paginated = FALSE, $userActiveLocationID = NULL, $status = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn');
        $select->order('grnID DESC');
        $select->join('entity', 'grn.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'grn.grnRetrieveLocation = location.locationID', array('locationName', 'locationCode', 'locationID'));
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(array('deleted' => '0'));
        $select->where->notEqualTo('grn.status', 10);
        if ($userActiveLocationID != NULL) {
            $select->where(array('locationID' => $userActiveLocationID));
        }
        if ($status != NULL) {
            $select->where(array('status' => $status));
        }
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param String $grnSearchKey
     * @return ResultSet
     */
    public function getGrnsforSearch($grnSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $grnSearchKey . '\' in grn.grnCode )>0,POSITION(\'' . $grnSearchKey . '\' in grn.grnCode), 9999),'
                    . 'IF(POSITION(\'' . $grnSearchKey . '\' in supplier.supplierName )>0,POSITION(\'' . $grnSearchKey . '\' in supplier.supplierName), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(grn.grnCode ), CHAR_LENGTH(supplier.supplierName )) '),
            '*',
        ));
        $select->order('grnDate DESC');
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->join('entity', 'grn.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'grn.grnRetrieveLocation = location.locationID', array('locationName'));
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('grn.grnCode', 'like', '%' . $grnSearchKey . '%'), new Operator('supplier.supplierName', 'like', '%' . $grnSearchKey . '%')), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getGrnBySupplier($supplierID)
    {
        $rowset = $this->tableGateway->select(array('grnSupplierID' => $supplierID));
        return $rowset;
    }

    public function getGrnByGrnCode($grnCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn');
        $select->join('grnProduct', 'grn.grnID = grnProduct.grnID', array('*'));
        $select->join('locationProduct', 'grnProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'));
        $select->join('productBatch', 'grnProduct.productBatchID = productBatch.productBatchID', array('productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity'), 'left');
        $select->join('productSerial', 'grnProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate'), 'left');
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID', array('supplierCode', 'supplierName'), 'left');
        $select->join('location', 'grn.grnRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('grnProductTax', 'grnProduct.grnProductID = grnProductTax.grnProductID', array('grnTaxID', 'grnTaxPrecentage', 'grnTaxAmount'), 'left');
        $select->join('tax', 'grnProductTax.grnTaxID = tax.id', array('taxName'), 'left');
        $select->where(array('grnCode' => $grnCode));
        $select->where(array('productUomBase' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getDraftGrnByGrnID($grnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('draftGrn');
        $select->where(array('draftGrn.grnID' => $grnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updateDraftGrn($dataSet = [])
    {
        return $this->tableGateway->update($dataSet, array('grnID' => $dataSet['grnID']));
    }

    public function saveGrn($grn)
    {
        $data = array(
            'grnCode' => $grn->grnCode,
            'grnSupplierID' => $grn->grnSupplierID,
            'grnSupplierReference' => $grn->grnSupplierReference,
            'grnRetrieveLocation' => $grn->grnRetrieveLocation,
            'grnDate' => $grn->grnDate,
            'grnDeliveryCharge' => $grn->grnDeliveryCharge,
            'grnTotal' => $grn->grnTotal,
            'grnComment' => $grn->grnComment,
            'grnShowTax' => $grn->grnShowTax,
            'status' => $grn->status,
            'entityID' => $grn->entityID,
            'grnUploadFlag' => $grn->grnUploadFlag,
            'grnActGrnData' => $grn->grnActGrnData,
            'grnHashValue' => $grn->grnHashValue,
            'isGrnApproved' => $grn->isGrnApproved,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function grnStatusDetails( $grnIds, $isAllGRNs = false, $fromDate = false, $toDate = false, $locations = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn')
                ->columns(array(
                    'gID' => new Expression('grn.grnID'),
                    'gCD' => new Expression('grn.grnCode'),
                    'gDate' => new Expression('grn.grnDate'),
                    'gTot' => new Expression('grn.grnTotal')))
                ->join('supplier', 'supplier.supplierID=grn.grnSupplierID', array(
                    'sCD' => new Expression('supplier.supplierCode'),
                    'sName' => new Expression('supplier.supplierName')))
                ->join('status', 'grn.status = status.statusID', array('statusName'), 'left')
                ->order('grn.grnDate DESC');
        $select->where->notEqualTo('grn.status',10);
        if (!$isAllGRNs) {
            $select->where->in('grn.grnID', $grnIds);
        }
        if($fromDate && $toDate) {
            $select->where->between('grn.grnDate', $fromDate, $toDate);
        }
        if($locations) {
            $select->where->in('grn.grnRetrieveLocation', $locations);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updateGrnStatus($grnID, $statusID)
    {
        try {
            $data = array(
                'status' => $statusID
            );
            $this->tableGateway->update($data, array('grnID' => $grnID));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function checkGrnByCode($grnCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('draftGrn')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "draftGrn.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('draftGrn.grnCode' => $grnCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * Search Grn For Drop down
     * @param type $searchKey
     * @param type $status
     * @param type $locationID
     * @return type
     */
    public function searchGrnForDropdown($searchKey, $status = false, $locationID = false, $supplierID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn');
        $select->order('grnID DESC');
        $select->join('entity', 'grn.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'grn.grnRetrieveLocation = location.locationID', array('locationName', 'locationCode', 'locationID'));
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(array('deleted' => '0'));
        $select->where->notEqualTo('grn.status', 10);
        if ($status) {
            $select->where(array('status' => $status));
        }
        if ($locationID) {
            $select->where(array('location.locationID' => $locationID));
        }
        if ($supplierID) {
            $select->where(array('grn.grnSupplierID' => $supplierID));
        }
        $select->where->like('grnCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get Grns By search
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $grnID
     * @param type $supplierID
     * @return type
     */
    public function getGrnsBySearch($grnID = false, $supplierID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn');
        $select->columns(array('*'));
        $select->order('grnDate DESC');
        $select->join('entity', 'grn.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'grn.grnRetrieveLocation = location.locationID', array('locationName'));
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where->notEqualTo('grn.status', 10);
        if ($grnID) {
            $select->where(array('grn.grnID' => $grnID));
        }
        if ($supplierID) {
            $select->where(array('grn.grnSupplierID' => $supplierID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get Grn details by grnId
     * @param type $grnId
     * @return type
     */
    public function grnDetailsByGrnId($grnId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn')
                ->columns(array('*'))
                ->join('entity', 'grn.entityID = entity.entityID', array('deleted'))
                ->where->equalTo('grn.grnID', $grnId)
                ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if (!$result) {
            return null;
        }
        return $result->current();
    }

    public function grnDetailsByGrnIdForJE($grnId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn')
                ->columns(array('*'))
                ->join('entity', 'grn.entityID = entity.entityID', array('deleted'))
                ->join('supplier', 'grn.grnSupplierID = supplier.supplierID', ['supplierCode', 'supplierName'], 'left')
                ->where->equalTo('grn.grnID', $grnId)
                ->where->equalTo('entity.deleted', 0);
        $select->where->notEqualTo('grn.status', 5);
        $select->where->notEqualTo('grn.status', 10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if (!$result) {
            return null;
        }
        return $result->current();
    }

      public function getGrnDetailsByGrnId($grnID)
    {
        $rowset = $this->tableGateway->select(array('grnID' => $grnID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }
    
    /**
     * Get grn details
     * @param array $grnIds
     * @param array $productIds
     * @param array $supplierIds
     * @param array $locationIds
     * @param string $fromDate
     * @param string $toDate
     * @return mixed
     */
    public function getGrnDetails($grnIds = [], $productIds = [], $supplierIds = [], $locationIds = [], $fromDate = null, $toDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn')
                ->columns(['*', 'grnTotalAmount' => new Expression('grnTotal + grnDeliveryCharge')])
                ->join('supplier', 'grn.grnSupplierID = supplier.supplierID', ['supplierCode', 'supplierName'], 'left')
                ->join('entity', 'grn.entityID = entity.entityID', ['deleted'], 'left')
                ->join('grnProduct', 'grn.grnID = grnProduct.grnID', ['grnProductID', 'locationProductID'], 'left')
                ->join('locationProduct', 'grnProduct.locationProductID = locationProduct.locationProductID', ['productID'], 'left')
                ->join('product', 'locationProduct.productID = product.productID', ['productCode', 'productName'], 'left')
                ->join('grnProductTax', 'grn.grnID = grnProductTax.grnID', ['grnTotalTax' => new Expression('SUM(grnTaxAmount)')], 'left')
                ->where->equalTo('entity.deleted', 0);
        $select->where->notEqualTo('grn.status',10);
        if ($fromDate && $toDate) {
            $select->where->between('grn.grnDate', $fromDate, $toDate);
        }
        if ($grnIds) {
            $select->where->in('grn.grnID', $grnIds);
        }
        if ($productIds) {
            $select->where->in('locationProduct.productID', $productIds);
        }
        if ($supplierIds) {
            $select->where->in('grn.grnSupplierID', $supplierIds);
        }
        if ($locationIds) {
            $select->where->in('grn.grnRetrieveLocation', $locationIds);
        }
        $select->group("grn.grnID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get grn supplier details
     * @param int $grnId
     * @return mixed
     */
    public function getGrnSupplierDetails($grnId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn')
                ->columns(['*'])
                ->join('supplier', 'grn.grnSupplierID = supplier.supplierID', ['*'], 'left')
                ->where->equalTo('grn.grnID', $grnId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->current();
        return $result;
    }

    /**
    * get all grn details by given supplier id(s)
    *
    **/
    public function getGrnBySupplierIDs($supplierIDs = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("grn")
                ->columns(array("*"))
                ->join("supplier", "grn.grnSupplierID = supplier.supplierID", array("*"), "left")
                ->join('status', 'grn.status = status.statusID',array('statusName'),'left')
                ->join('grnProduct','grn.grnID = grnProduct.grnID', array('grnProductDocumentId', 'grnProductDocumentTypeId'), 'left')
                ->join('entity', 'grn.entityID = entity.entityID', array('deleted', 'createdTimeStamp'))
                ->order(array('grnDate' => 'DESC'))
                ->where(array('deleted' => 0));
        $select->where->in('grn.grnSupplierID' ,$supplierIDs);
        $select->where->in('grn.status' , $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('grn.grnDate', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function updateGrnRecord($grnID, $data = [])
    {
        try {
            $this->tableGateway->update($data, array('grnID' => $grnID));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function checkGrnnByCode($grnCode = null,$grnId = null)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('grn')
                    ->columns(array('*'));
            if ($grnCode) {
                $select->where(array('grn.grnCode' => $grnCode));
            }
            if ($grnId) {
                $select->where(array('grn.grnID' => $grnId));
            }
            $select->where->notEqualTo('grn.status',4);
            $select->where->notEqualTo('grn.status',10);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }
    
    public function getPiRelatedToGrnByGrnID($grnID){
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("purchaseInvoice")
                ->columns(array("*"))
                ->where(array("purchaseInvoiceGrnID" => $grnID));
        $select->where->notEqualTo('purchaseInvoice.status',5);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    } 


    public function getGrnForPreviewByGrnID($grnID, $toPv = FALSE,$checkItemIn = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('draftGrn');
        $select->join('draftGrnProduct', 'draftGrn.grnID = draftGrnProduct.grnID', array('*'));
        $select->join('locationProduct', 'draftGrnProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('draftProductBatch', 'draftGrnProduct.productBatchID = draftProductBatch.productBatchID', array('productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity', 'productBatchPrice'), 'left');
        $select->join('draftProductSerial', 'draftGrnProduct.productSerialID = draftProductSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate', 'productSerialSold', 'productSerialReturned'), 'left');
        $select->join('supplier', 'draftGrn.grnSupplierID = supplier.supplierID', array('supplierCode', 'supplierName', 'supplierEmail', 'supplierAddress', 'supplierTitle', 'supplierOutstandingBalance', 'supplierCreditBalance'), 'left');
        $select->join('location', 'draftGrn.grnRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('draftGrnProductTax', 'draftGrnProduct.grnProductID = draftGrnProductTax.grnProductID', array('grnTaxID', 'grnTaxPrecentage', 'grnTaxAmount'), 'left');
        $select->join('tax', 'draftGrnProductTax.grnTaxID = tax.id', array('taxName'), 'left');
        $select->join('entity', 'draftGrn.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        if ($checkItemIn) {
            $select->join('itemIn', 'draftGrn.grnID = itemIn.itemInDocumentID', array('itemInSoldQty'), 'left');
        }
        $select->where(array('draftGrn.grnID' => $grnID));
        $select->where(array('productUomBase' => '1'));
        $select->where->notEqualTo('draftGrn.status',10);
        if ($toPv) {
            $select->where(array('draftGrnProduct.copied' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    
    }    

    public function getGrnForItemMovingByGrnID($grnID, $toPv = FALSE,$checkItemIn = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn');
        $select->join('grnProduct', 'grn.grnID = grnProduct.grnID', array('*'));
        $select->join('locationProduct', 'grnProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('productBatch', 'grnProduct.productBatchID = productBatch.productBatchID', array('productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity', 'productBatchPrice'), 'left');
        $select->join('productSerial', 'grnProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate', 'productSerialSold', 'productSerialReturned'), 'left');
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID', array('supplierCode', 'supplierName', 'supplierEmail', 'supplierAddress', 'supplierTitle', 'supplierOutstandingBalance', 'supplierCreditBalance'), 'left');
        $select->join('location', 'grn.grnRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('grnProductTax', 'grnProduct.grnProductID = grnProductTax.grnProductID', array('grnTaxID', 'grnTaxPrecentage', 'grnTaxAmount'), 'left');
        $select->join('tax', 'grnProductTax.grnTaxID = tax.id', array('taxName'), 'left');
        $select->join('entity', 'grn.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        if ($checkItemIn) {
            $select->join('itemIn', 'grn.grnID = itemIn.itemInDocumentID', array('itemInSoldQty'), 'left');
        }
        $select->where(array('grn.grnID' => $grnID));
        $select->where(array('productUomBase' => '1'));
        if ($toPv) {
            $select->where(array('grnProduct.copied' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get pr related grn by pr id
    public function getGrnDetailsByPrId($prId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn')
                ->columns(['*'])
                ->join('purchaseReturn', 'grn.grnID = purchaseReturn.purchaseReturnGrnID', array('*'), 'left')
                ->join('entity', 'grn.entityID = entity.entityID', array('*'), 'left');
        $select->where->equalTo('purchaseReturn.purchaseReturnID', $prId);
        $select->group("grn.grnID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get pi related grn by debit note id
    public function getPiRelatedGrnDetailsByDebitNoteId($debitNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn')
                ->columns(['*'])
                ->join('purchaseInvoice', 'grn.grnID = purchaseInvoice.purchaseInvoiceGrnID', array('*'), 'left')
                ->join('debitNote', 'purchaseInvoice.purchaseInvoiceID = debitNote.purchaseInvoiceID', array('*'), 'left')
                ->join('entity', 'grn.entityID = entity.entityID', array('*'), 'left');
        $select->where->equalTo('debitNote.debitNoteID', $debitNoteID);
        $select->group("grn.grnID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    public function getGrnBySupplierIDsForSupplierBalance($supplierIDs = [], $status = [], $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("grn")
                ->columns(array("*"))
                ->join("supplier", "grn.grnSupplierID = supplier.supplierID", array("*"), "left")
                ->join('status', 'grn.status = status.statusID',array('statusName'),'left')
                ->join('grnProduct','grn.grnID = grnProduct.grnID', array('grnProductDocumentId', 'grnProductDocumentTypeId'), 'left')
                ->join('entity', 'grn.entityID = entity.entityID', array('deleted', 'createdTimeStamp'))
                ->order(array('createdTimeStamp' => 'DESC'))
                ->where(array('deleted' => 0));
        $select->where->in('grn.grnSupplierID' ,$supplierIDs);
        $select->where->in('grn.status' , $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('entity.createdTimeStamp', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getGrnWisePurchaseReturn($fromDate = null, $toDate = null, $grnIds = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('grn')
                ->columns(['grnID','grnCode','grnDate','grnTotal', 'status','entityID'])
                ->join('purchaseReturn', 'grn.grnID = purchaseReturn.purchaseReturnGrnID', ['purchaseReturnCode','purchaseReturnDate','purchaseReturnTotal','purchaseReturnEntityID','purchaseReturnStatus'], 'left')
                ->join('purchaseReturnProduct', 'purchaseReturn.purchaseReturnID = purchaseReturnProduct.purchaseReturnID', [
                    'copiedAmount' => new Expression('SUM(purchaseReturnProductTotal)'),'purchaseReturnID'], 'left')
                ->join(['status' => 'status'], 'grn.status = status.statusID', ['grnStatus' => 'statusName'], 'left')
                ->join(['pRstatus' => 'status'], 'purchaseReturn.purchaseReturnStatus = pRstatus.statusID', ['pRStatus' => 'statusName'], 'left')
                ->join(['pRentity' => 'entity'], 'purchaseReturn.purchaseReturnEntityID = pRentity.entityID', ['sideleted' => 'deleted'], 'left')
                ->join(['gentity' => 'entity'], 'grn.entityID = gentity.entityID', ['grndeleted' => 'deleted'], 'left')
                ->where->equalTo('pRentity.deleted', 0)
                ->where->equalTo('gentity.deleted', 0);
        if ($fromDate && $toDate) {
            $select->where->between('grn.grnDate', $fromDate, $toDate);
        }
        if ($grnIds) {
            $select->where->in('grn.grnID', $grnIds);
        }
        $select->group(['grn.grnID','purchaseReturn.purchaseReturnID']);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function getGrnWisePurchaseInvoice($fromDate = null, $toDate = null, $grnIds = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('grn')
                ->columns(['grnID','grnCode','grnDate','grnTotal', 'status','entityID'])
                ->join('purchaseInvoice', 'grn.grnID = purchaseInvoice.purchaseInvoiceGrnID', ['purchaseInvoiceCode','purchaseInvoiceIssueDate','purchaseInvoiceTotal','entityID','status'], 'left')
                ->join('purchaseInvoiceProduct', 'purchaseInvoice.purchaseInvoiceID = purchaseInvoiceProduct.purchaseInvoiceID', [
                    'copiedAmount' => new Expression('SUM(purchaseInvoiceProductTotal)'),'purchaseInvoiceID'], 'left')
                ->join(['status' => 'status'], 'grn.status = status.statusID', ['grnStatus' => 'statusName'], 'left')
                ->join(['pIstatus' => 'status'], 'purchaseInvoice.status = pIstatus.statusID', ['pIStatus' => 'statusName'], 'left')
                ->join(['pRentity' => 'entity'], 'purchaseInvoice.entityID = pRentity.entityID', ['sideleted' => 'deleted'], 'left')
                ->join(['gentity' => 'entity'], 'grn.entityID = gentity.entityID', ['grndeleted' => 'deleted'], 'left')
                ->where->equalTo('pRentity.deleted', 0)
                ->where->equalTo('gentity.deleted', 0);
        if ($fromDate && $toDate) {
            $select->where->between('grn.grnDate', $fromDate, $toDate);
        }
        if ($grnIds) {
            $select->where->in('grn.grnID', $grnIds);
        }
        $select->group(['grn.grnID','purchaseInvoice.purchaseInvoiceID']);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function getEditedGrnDetails($fromDate , $toDate, $supplierIDs = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn')
                ->columns(['*'])
                ->where->equalTo('grn.status', 10);
        if ($fromDate && $toDate) {
            $select->where->between('grn.grnDate', $fromDate, $toDate);
        }
        if ($supplierIDs) {
            $select->where->in('grn.grnSupplierID', $supplierIDs);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getGrnByGrnCodeForEdiedGrn($grnCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn')
                ->columns(['*'])
                ->where->notEqualTo('grn.status', 10);
        $select->where->equalTo('grn.grnCode',$grnCode);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getGrnBasicDetailsByGrnId($grnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn');
        $select->columns(array('*'));
        $select->join('entity', 'grn.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('grn.grnID' => $grnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;
    }

    public function getGrnDetailsByGrnIDs($grnIDs)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('grn');
        $select->join('grnProduct', 'grn.grnID = grnProduct.grnID', array('*'));
        $select->join('locationProduct', 'grnProduct.locationProductID = locationProduct.locationProductID', array('productID','LocProductId' => new Expression('locationProduct.locationProductID'),'locationProductQuantity'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('productBatch', 'grnProduct.productBatchID = productBatch.productBatchID', array('productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity', 'productBatchPrice'), 'left');
        $select->join('productSerial', 'grnProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate', 'productSerialSold', 'productSerialReturned','serialLocationId' => new Expression('productSerial.locationProductID')), 'left');
        $select->join('supplier', 'grn.grnSupplierID = supplier.supplierID', array('supplierCode', 'supplierName', 'supplierEmail', 'supplierAddress', 'supplierTitle', 'supplierOutstandingBalance', 'supplierCreditBalance'), 'left');
        $select->join('location', 'grn.grnRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('grnProductTax', 'grnProduct.grnProductID = grnProductTax.grnProductID', array('grnTaxID', 'grnTaxPrecentage', 'grnTaxAmount'), 'left');
        $select->join('tax', 'grnProductTax.grnTaxID = tax.id', array('taxName'), 'left');
        $select->join('entity', 'grn.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->join('itemIn', 'grn.grnID = itemIn.itemInDocumentID', array('itemInSoldQty'), 'left');
        $select->where->in('grn.grnID',$grnIDs);
        $select->where(array('productUomBase' => '1'));
        $select->where(array('grnProduct.copied' => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param Paginator true or false
     * @return paginator object if true, else resultset
     */
    public function getDraftGrns($paginated = FALSE, $userActiveLocationID = NULL, $status = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('draftGrn');
        $select->join('supplier', 'draftGrn.grnSupplierID = supplier.supplierID', array('supplierName'), 'left');
        $select->join('location', 'draftGrn.grnRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->order('grnID DESC');
        $select->where->in('draftGrn.status',[3, 16, 13]);
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

}
