<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PurchaseOrderProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePurchaseOrderProduct(PurchaseOrderProduct $poProduct)
    {
        $data = array(
            'purchaseOrderID' => $poProduct->purchaseOrderID,
            'locationProductID' => $poProduct->locationProductID,
            'purchaseOrderProductQuantity' => $poProduct->purchaseOrderProductQuantity,
            'purchaseOrderProductPrice' => $poProduct->purchaseOrderProductPrice,
            'purchaseOrderProductDiscount' => $poProduct->purchaseOrderProductDiscount,
            'purchaseOrderProductTotal' => $poProduct->purchaseOrderProductTotal,
            'copied' => $poProduct->copied,
            'purchaseOrderProductCopiedQuantity' => $poProduct->purchaseOrderProductCopiedQuantity,
            'purchaseOrderProductDocumentId' => $poProduct->purchaseOrderProductDocumentId,
            'purchaseOrderProductDocumentTypeId' => $poProduct->purchaseOrderProductDocumentTypeId,
            'purchaseOrderProductDoumentTypeCopiedQuantity' => $poProduct->purchaseOrderProductDoumentTypeCopiedQuantity,
            'purchaseOrderProductSelectedUomId' => $poProduct->purchaseOrderProductSelectedUomId
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function updateCopiedPoProducts($locationPID, $poID)
    {
        $data = array(
            'copied' => 1
        );
        $this->tableGateway->update($data, array('locationProductID' => $locationPID, 'purchaseOrderID' => $poID));
    }

    public function updateCopiedPoProductQty($locationPID, $poID, $productQty)
    {
        $data = array(
            'purchaseOrderProductCopiedQuantity' => $productQty
        );
        $this->tableGateway->update($data, array('locationProductID' => $locationPID, 'purchaseOrderID' => $poID));
    }

    public function getUnCopiedPoProducts($poID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrderProduct');
        $select->where(array('purchaseOrderID' => $poID));
        $select->where(array('copied' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getPoProduct($poId, $locationProductId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrderProduct');
        $select->where(array('purchaseOrderID' => $poId));
        $select->where(array('locationProductID' => $locationProductId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    public function CheckPurchaseOrderProductByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrderProduct')
                ->columns(array('*'))
                ->join('purchaseOrder', 'purchaseOrder.purchaseOrderID = purchaseOrderProduct.purchaseOrderID', array('entityID'), 'left')
                ->join('entity', 'entity.entityID = purchaseOrder.entityID', array('deleted'), 'left')
                ->where(array('locationProductID' => $locationProductID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function getPurchaseOrderProductQtyByProductID($fromDate = null, $toDate, $moreRecords = null, $productID = null, $locationID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrderProduct')
                ->columns(array('totalQty' => new Expression('SUM(purchaseOrderProduct.purchaseOrderProductQuantity)'),
                        'totalAssign' => new Expression('SUM(purchaseOrderProduct.purchaseOrderProductCopiedQuantity)')))
                ->join('locationProduct', 'purchaseOrderProduct.locationProductID = locationProduct.locationProductID'
                        , array('locationProductID'), 'left')
                ->join('purchaseOrder', 'purchaseOrder.purchaseOrderID = purchaseOrderProduct.purchaseOrderID', 
                        array('purchaseOrderRetrieveLocation'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID'), 'left');
        $select->join('entity', 'product.entityID = entity.entityID', array('deleted'));
        $select->group(array('product.productID'));
        $select->where(array('purchaseOrder.status' => 3, 'purchaseOrderProduct.copied' => 0, 'entity.deleted' => 0));
        $select->where('product.productState','=',1);
        if($productID){
            $select->where(array('product.productID' => $productID));
        }
        if($locationID){
            $select->where->in('purchaseOrder.purchaseOrderRetrieveLocation', $locationID);
        }
        if($moreRecords){
            $select->where->greaterThan('purchaseOrder.purchaseOrderExpDelDate', $toDate);
        } else{
            $select->where->between('purchaseOrder.purchaseOrderExpDelDate', $fromDate, $toDate);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function deleteDraftRecordsByPoID($poID)
    {
        try {
            $result = $this->tableGateway->delete(array('purchaseOrderID' => $poID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function updatePoCopiedStateAndCopiedQty($updatedDetaSet = [])
    {
        return $this->tableGateway->update($updatedDetaSet, array('purchaseOrderProductID' => $updatedDetaSet['purchaseOrderProductID']));
    }

    public function getAllPoProductsByPoID($purchaseOrderID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrderProduct',['purchaseOrderProductID', 'locationProductID', 'purchaseOrderProductPrice', 
                    'purchaseOrderProductQuantity', 
                    'purchaseOrderProductTotal','purchaseOrderProductDocumentId','purchaseOrderProductDocumentTypeId','purchaseOrderProductDoumentTypeCopiedQuantity'
                    ], 'left')
                ->join('locationProduct', 'purchaseOrderProduct.locationProductID = locationProduct.locationProductID', ['productID','locationID'], 'left')
                ->join('product', 'locationProduct.productID = product.productID', ['productCode', 'productName'], 'left')
                ->where->equalTo('purchaseOrderProduct.purchaseOrderID', $purchaseOrderID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getAllPoProductsByPoIDForSupReport($purchaseOrderID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrderProduct',['purchaseOrderProductID', 'locationProductID', 'purchaseOrderProductPrice', 
                    'purchaseOrderProductQuantity', 
                    'purchaseOrderProductTotal','purchaseOrderProductDocumentId','purchaseOrderProductDocumentTypeId','purchaseOrderProductDoumentTypeCopiedQuantity'
                    ], 'left')
                ->join('locationProduct', 'purchaseOrderProduct.locationProductID = locationProduct.locationProductID', ['productID','locationID'], 'left')
                ->join('product', 'locationProduct.productID = product.productID', ['productCode', 'productName'], 'left')
                ->where->equalTo('purchaseOrderProduct.purchaseOrderID', $purchaseOrderID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultArray = [];
        if (sizeof($result) > 0) {
            foreach ($result as $key => $value) {
                $resultArray[] = $value;
            }
        }

        return $resultArray;
    }

    public function getPurchaseOrderProductByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseOrderProduct')
                ->columns(array('totalQty' => new Expression('SUM(purchaseOrderProduct.purchaseOrderProductQuantity)')))
                ->join('purchaseOrder', 'purchaseOrder.purchaseOrderID = purchaseOrderProduct.purchaseOrderID', array('entityID'), 'left')
                ->join('entity', 'entity.entityID = purchaseOrder.entityID', array('deleted'), 'left')
                ->where(array('locationProductID' => $locationProductID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }
}
