<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Tax Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductTax implements InputFilterAwareInterface
{

    public $taxID;
    public $productID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->taxID = (!empty($data['taxID'])) ? $data['taxID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
//            $factory = new InputFactory();
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'id',
//                        'required' => false,
//                        'filters' => array(
//                            array('name' => 'Int'),
//                        ),
//            )));
//
//    
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

