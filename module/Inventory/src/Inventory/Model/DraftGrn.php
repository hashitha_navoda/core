<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the GRN model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DraftGrn implements InputFilterAwareInterface
{

    public $grnID;
    public $grnCode;
    public $grnSupplierID;
    public $grnSupplierReference;
    public $grnRetrieveLocation;
    public $grnDate;
    public $grnDeliveryCharge;
    public $grnTotal;
    public $grnComment;
    public $grnShowTax;
    public $status;
    public $entityID;
    public $grnUploadFlag;
    public $grnActGrnData;
    public $grnHashValue;
    public $isGrnApproved;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->grnID = (!empty($data['grnID'])) ? $data['grnID'] : null;
        $this->grnCode = (!empty($data['grnCode'])) ? $data['grnCode'] : null;
        $this->grnSupplierID = (!empty($data['grnSupplierID'])) ? $data['grnSupplierID'] : null;
        $this->grnSupplierReference = (!empty($data['grnSupplierReference'])) ? $data['grnSupplierReference'] : null;
        $this->grnRetrieveLocation = (!empty($data['grnRetrieveLocation'])) ? $data['grnRetrieveLocation'] : null;
        $this->grnDate = (!empty($data['grnDate'])) ? $data['grnDate'] : null;
        $this->grnDeliveryCharge = (!empty($data['grnDeliveryCharge'])) ? $data['grnDeliveryCharge'] : 0.00;
        $this->grnTotal = (!empty($data['grnTotal'])) ? $data['grnTotal'] : null;
        $this->grnComment = (!empty($data['grnComment'])) ? $data['grnComment'] : null;
        $this->grnShowTax = (!empty($data['grnShowTax'])) ? $data['grnShowTax'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : 3;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->grnUploadFlag = (!empty($data['grnUploadFlag'])) ? $data['grnUploadFlag'] : 0;
        $this->grnActGrnData = (!empty($data['grnActGrnData'])) ? $data['grnActGrnData'] : null;
        $this->grnHashValue = (!empty($data['grnHashValue'])) ? $data['grnHashValue'] : null;
        $this->isGrnApproved = (!empty($data['isGrnApproved'])) ? $data['isGrnApproved'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

