<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\CompoundCostTemplate;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class CompoundCostTemplateTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function compoundTemplateSave(CompoundCostTemplate $compoundCostTemplate)
    {
        try {
            $data = array(
                'compoundCostTemplateDescription' => $compoundCostTemplate->compoundCostTemplateDescription,
                'compoundCostTemplateName' => $compoundCostTemplate->compoundCostTemplateName,
                'locationID' => $compoundCostTemplate->locationID,
                'entityID' => $compoundCostTemplate->entityID,
                'status' => $compoundCostTemplate->status,
                'compoundCostTemplateCostCalType' => $compoundCostTemplate->compoundCostTemplateCostCalType,
                'compoundCostTemplateCode' => $compoundCostTemplate->compoundCostTemplateCode,
                
            );
            $this->tableGateway->insert($data);
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;

        } catch (\Exception $exc) {
            error_log($exc);
        }
    }

    public function getCompoundCostDetails($compoundCostTemplateID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('compoundCostTemplate');
        $select->join('compoundCostItem', 'compoundCostTemplate.compoundCostTemplateID =  compoundCostItem.compoundCostTemplateID', array("*"), "left");
        $select->join('entity', 'compoundCostTemplate.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('compoundCostTemplate.compoundCostTemplateID' => $compoundCostTemplateID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getAllCompundCostDetails($paginated = FALSE, $userActiveLocationID = NULL, $status = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('compoundCostTemplate');
        $select->order('compoundCostTemplateID DESC');
        $select->join('entity', 'compoundCostTemplate.entityID = entity.entityID', array('deleted', 'createdTimeStamp'));
        $select->where(array('deleted' => '0'));
        if ($userActiveLocationID != NULL) {
            $select->where(array('compoundCostTemplate.locationID' => $userActiveLocationID));
        }
        if ($status != NULL) {
            $select->where(array('compoundCostTemplate.status' => $status));
        }
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

}