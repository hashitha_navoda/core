<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the GRNProductTax model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DraftGrnProductTax implements InputFilterAwareInterface
{

    public $grnProductTaxID;
    public $grnID;
    public $grnProductID;
    public $grnTaxID;
    public $grnTaxPrecentage;
    public $grnTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->grnProductTaxID = (!empty($data['grnProductTaxID'])) ? $data['grnProductTaxID'] : null;
        $this->grnID = (!empty($data['grnID'])) ? $data['grnID'] : null;
        $this->grnProductID = (!empty($data['grnProductID'])) ? $data['grnProductID'] : null;
        $this->grnTaxID = (!empty($data['grnTaxID'])) ? $data['grnTaxID'] : null;
        $this->grnTaxPrecentage = (!empty($data['grnTaxPrecentage'])) ? $data['grnTaxPrecentage'] : null;
        $this->grnTaxAmount = (!empty($data['grnTaxAmount'])) ? $data['grnTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

