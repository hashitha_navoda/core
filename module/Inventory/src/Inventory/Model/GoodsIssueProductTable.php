<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class GoodsIssueProductTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveGoodIssueProduct(GoodsIssueProduct $goodsIssueProduct)
    {

        $data = array(
            'productBatchID' => $goodsIssueProduct->productBatchID,
            'productSerialID' => $goodsIssueProduct->productSerialID,
            'goodsIssueID' => $goodsIssueProduct->goodsIssueID,
            'locationProductID' => $goodsIssueProduct->locationProductID,
            'goodsIssueProductQuantity' => $goodsIssueProduct->goodsIssueProductQuantity,
            'unitOfPrice' => $goodsIssueProduct->unitOfPrice,
            'uomID' => $goodsIssueProduct->uomID,
            'goodsIssueUomID' => $goodsIssueProduct->goodsIssueUomID,
            'uomConversionRate' => $goodsIssueProduct->uomConversionRate
        );
        $row = $this->tableGateway->insert($data);
        return $row;
    }

    public function getGoodsIssueProduct($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('goodsIssueProductID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function addGoodsIssue($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('goodsIssueProductID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function update($b_code, $data)
    {
        $row = $this->tableGateway->update($data, array('goodsIssueProductID' => $b_code));

        return $row;
    }

    public function getGodIsuProByBatchID($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('productBatchID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getGodIsuProByGudIsuID($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('goodsIssueID' => $id));
        return $rowset;
    }

    public function CheckGoodsIssueProductByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssueProduct')
                ->columns(array('*'))
                ->join('goodsIssue', 'goodsIssue.goodsIssueID = goodsIssueProduct.goodsIssueID', array('entityID','goodsIssueReason'), 'left')
                ->join('entity', 'entity.entityID = goodsIssue.entityID', array('deleted'), 'left')
                ->where(array('locationProductID' => $locationProductID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function getGoodsIssueProductByLocationProductID($locationProductID, $sortByDate = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssueProduct');
        $select->columns(array('*', 'itemInPrice' => new Expression('unitOfPrice')));
        $select->join('goodsIssue', 'goodsIssueProduct.goodsIssueID = goodsIssue.goodsIssueID', array('itemInDate' => new Expression('goodsIssueDate'), 'goodsIssueID'), 'left');
        if ($sortByDate) {
            $select->order(array('itemInDate DESC'));
        }
        $select->where(array('locationProductID' => $locationProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = array();
        foreach ($results as $result) {
            $result['documentID'] = $result['goodsIssueID'];
            $result['documentType'] = 'Inventory Positive Adjustment';
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function getGoodIssueProducts($goodsIssueID, $subFlag = FALSE, $locationProductID = NULL, $type = NULL)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('goodsIssueProduct');
        $select->columns(['*']);
        $select->join('locationProduct', 'goodsIssueProduct.locationProductID = locationProduct.locationProductID', ['productID', 'locationProductID']);
        $select->join('product', 'product.productID = locationProduct.productID', ['productCode', 'productName']);
        if ($locationProductID != NULL) {
            $select->where(['goodsIssueProduct.locationProductID' => $locationProductID]);
        }
        if (!$subFlag) {
            if ($type == 'positive') {
                $select->columns(['totalQty' => new Expression('SUM(goodsIssueProductQuantity)'),'goodsIssueProductQuantity', 'productBatchID', 'productSerialID', 'unitOfPrice']);
            } elseif ($type == 'negative') {
                $select->columns(['totalQty' => new Expression('SUM(goodsIssueProductQuantity)')]);
            }
            $select->group('goodsIssueProduct.locationProductID');
        }
        $select->where(['goodsIssueID' => $goodsIssueID]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results) {
            return $results;
        }
        return NULL;
    }

}
