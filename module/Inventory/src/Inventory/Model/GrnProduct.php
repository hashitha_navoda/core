<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the GRNProduct model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class GrnProduct implements InputFilterAwareInterface
{
    public $grnProductID;
    public $grnID;
    public $locationProductID;
    public $productBatchID;
    public $productSerialID;
    public $grnProductPrice;
    public $grnProductDiscount;
    public $grnProductTotalQty;
    public $grnProductQuantity;
    public $grnProductUom;
    public $grnProductTotal;
    public $rackID;
    public $copied;
    public $grnProductCopiedQuantity;
    public $grnProductTotalCopiedQuantity;
    public $grnProductDiscountType;

    /**
     *
     * @var int document id
     */
    public $grnProductDocumentId;

    /**
     *
     * @var int document type referance
     */
    public $grnProductDocumentTypeId;

    /**
     *
     * @var decimal document type copied quantity
     */
    public $grnProductDoumentTypeCopiedQuantity;

    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->grnProductID = (!empty($data['grnProductID'])) ? $data['grnProductID'] : null;
        $this->grnID = (!empty($data['grnID'])) ? $data['grnID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->grnProductPrice = (!empty($data['grnProductPrice'])) ? $data['grnProductPrice'] : null;
        $this->grnProductDiscount = (!empty($data['grnProductDiscount'])) ? $data['grnProductDiscount'] : null;
        $this->grnProductTotalQty = (!empty($data['grnProductTotalQty'])) ? $data['grnProductTotalQty'] : null;
        $this->grnProductQuantity = (!empty($data['grnProductQuantity'])) ? $data['grnProductQuantity'] : null;
        $this->grnProductUom = (!empty($data['grnProductUom'])) ? $data['grnProductUom'] : null;
        $this->grnProductTotal = (!empty($data['grnProductTotal'])) ? $data['grnProductTotal'] : null;
        $this->rackID = (!empty($data['rackID'])) ? $data['rackID'] : null;
        $this->copied = (!empty($data['copied'])) ? $data['copied'] : 0;
        $this->grnProductCopiedQuantity = (!empty($data['grnProductCopiedQuantity'])) ? $data['grnProductCopiedQuantity'] : null;
        $this->grnProductTotalCopiedQuantity = (!empty($data['grnProductTotalCopiedQuantity'])) ? $data['grnProductTotalCopiedQuantity'] : null;
        $this->grnProductDocumentId = (!empty($data['grnProductDocumentId'])) ? $data['grnProductDocumentId'] : null;
        $this->grnProductDocumentTypeId = (!empty($data['grnProductDocumentTypeId'])) ? $data['grnProductDocumentTypeId'] : null;
        $this->grnProductDoumentTypeCopiedQuantity = (!empty($data['grnProductDoumentTypeCopiedQuantity'])) ? $data['grnProductDoumentTypeCopiedQuantity'] : 0;
        $this->grnProductDiscountType = (!empty($data['grnProductDiscountType'])) ? $data['grnProductDiscountType'] : 'percentage';
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
