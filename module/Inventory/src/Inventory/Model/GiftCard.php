<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Gift Card Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class GiftCard
{

    public $giftCardId;
    public $giftCardCode;
    public $giftCardDuration;
    public $giftCardIssueDate;
    public $giftCardExpireDate;
    public $giftCardValue;
    public $giftCardStatus;
    public $giftCardGlAccountID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->giftCardId = (!empty($data['giftCardId'])) ? $data['giftCardId'] : null;
        $this->giftCardCode = (!empty($data['giftCardCode'])) ? $data['giftCardCode'] : null;
        $this->giftCardDuration = (!empty($data['giftCardDuration'])) ? $data['giftCardDuration'] : 0;
        $this->giftCardIssueDate = (!empty($data['giftCardIssueDate'])) ? $data['giftCardIssueDate'] : null;
        $this->giftCardExpireDate = (!empty($data['giftCardExpireDate'])) ? $data['giftCardExpireDate'] : null;
        $this->giftCardValue = (!empty($data['giftCardValue'])) ? $data['giftCardValue'] : 0.00;
        $this->giftCardStatus = (!empty($data['giftCardStatus'])) ? $data['giftCardStatus'] : 2;
        $this->giftCardGlAccountID = (!empty($data['giftCardGlAccountID'])) ? $data['giftCardGlAccountID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'giftCardId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'giftCardCode',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 255,
                                ),
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
