<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\CompoundCostType;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class CompoundCostTypeTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function compoundCostTypeSearchByKey($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('compoundCostType');
        $select->where(array('compoundCostTypeIsDelete' => '0'));
        $select->where(new PredicateSet(array(new Operator('compoundCostTypeName', 'like', '%' . $searchKey . '%'), new Operator('compoundCostTypeCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getAllCostTypes()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('compoundCostType');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function fetchAll($paginated = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('compoundCostType');
        $select->where(array('compoundCostTypeIsDelete' =>0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getCostTypeDetailsByCode($code)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('compoundCostType');
        $select->where(array('compoundCostTypeIsDelete' => 0, 'compoundCostTypeCode' => $code));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function save(CompoundCostType $compoundCostType)
    {
        try {
            $data = array(
                'compoundCostTypeName' => $compoundCostType->compoundCostTypeName,
                'compoundCostTypeCode' => $compoundCostType->compoundCostTypeCode,
            );
            $this->tableGateway->insert($data);
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;

        } catch (\Exception $exc) {
            return null;
            error_log($exc);

        }
    }

    public function update($data, $costTypeID)
    {
        try {
            $result = $this->tableGateway->update($data, array('compoundCostTypeID' => $costTypeID));
            return $result;
        } catch (\Exception $exc) {
            return null;
            error_log($exc);
        }
    }

}
