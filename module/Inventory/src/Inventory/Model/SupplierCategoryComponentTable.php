<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 * This for add or edit supplier category data
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;

class SupplierCategoryComponentTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     *
     * @param int $supplierID
     * @param array $supplierCategoryIDs
     * @return array inserted SupplierCategoryComponentIDs
     */
    public function insertData($supplierID, $supplierCategoryIDs)
    {
        $insertedIDs = array();
        foreach ($supplierCategoryIDs as $supplierCategoryID) {
            $this->tableGateway->insert(array(
                'supplierID' => (int) $supplierID,
                'supplierCategoryID' => (int) $supplierCategoryID[0],
            ));
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            $insertedIDs[] = $insertedID;
        }
        return $insertedIDs;
    }

    /**
     *
     * @param int $supplierID
     * @param int $supplierCategoryID
     * @return int inserted SupplierCategoryComponentIDs
     */
    public function insertSingleData($supplierID, $supplierCategoryID)
    {
        $this->tableGateway->insert(array(
            'supplierID' => $supplierID,
            'supplierCategoryID' => $supplierCategoryID,
        ));
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();

        return $insertedID;
    }

    /**
     * Check particular data exists in the table
     * @param \Core\Model\SupplierCategoryComponent $sCC
     * @return boolean
     */
    public function isDataExists($supplierID, $supplierCategoryID)
    {
        $data = array(
            'supplierID' => $supplierID,
            'supplierCategoryID' => $supplierCategoryID,
        );

        $rowset = $this->tableGateway->select($data);

        if ($rowset->current()) {
            return true;
        } else {
            return false;
        }

//        return $rowset;
    }

    /**
     * get as array for a supplierID
     * @param int $supplierID
     * @return array list of category IDs
     */
    public function getCategoryBySupplierID($supplierID)
    {
        $supplierID = (int) $supplierID;
        $rowset = $this->tableGateway->select(array('supplierID' => $supplierID));
        $resultArray = array();
        foreach ($rowset as $result) {
            $resultArray[] = $result->supplierCategoryID;
        }
        return $resultArray;
    }

    public function deleteData($supplierID, $supplierCategoryID)
    {
        try {
            $result = $this->tableGateway->delete(array(
                'supplierID' => $supplierID,
                'supplierCategoryID' => $supplierCategoryID,
            ));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    /**
     * delete data according to the categoryID
     * @param int $supplierCategoryID
     */
    public function deleteByCategoryID($supplierCategoryID)
    {
        try {
            $result = $this->tableGateway->delete(array('supplierCategoryID' => $supplierCategoryID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    /**
     * delete data according to hte supplierID
     * @param int $supplierID
     */
    public function deleteBySupplierID($supplierID)
    {
        try {
            $result = $this->tableGateway->delete(array('supplierID' => $supplierID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    /**
     * get data according to the supplierCategoryID
     * @param int $supplierCategoryID
     */
    public function getDataBySupplierCategoryID($supplierCategoryID)
    {
        $rowset = $this->tableGateway->select(array('supplierCategoryID' => $supplierCategoryID));
        return $rowset;
    }

    public function updateSupplierCategoryComponentBySupplierID(SupplierCategoryComponent $supplierCategoryComponent)
    {
        try {
            $data = [
                'supplierCategoryID' => $supplierCategoryComponent->supplierCategoryID,
            ];
            $this->tableGateway->update($data, array('supplierID' => $supplierCategoryComponent->supplierID));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

}
