<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class PurchaseReturnProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePurchaseReturnProductTax(PurchaseReturnProductTax $prProductTax)
    {
        $data = array(
            'purchaseReturnID' => $prProductTax->purchaseReturnID,
            'purchaseReturnProductID' => $prProductTax->purchaseReturnProductID,
            'purchaseReturnTaxID' => $prProductTax->purchaseReturnTaxID,
            'purchaseReturnTaxPrecentage' => $prProductTax->purchaseReturnTaxPrecentage,
            'purchaseReturnTaxAmount' => $prProductTax->purchaseReturnTaxAmount,
        );
        $this->tableGateway->insert($data);
    }

}

