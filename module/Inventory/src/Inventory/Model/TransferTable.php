<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Unit Measure Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Predicate\Like;

class TransferTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationId = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('*'))
                ->join('location', 'transfer.locationIDIn = location.locationID', array('locationInName' => new Expression('location.locationName')), 'left')
                ->join(array('locationOut' => 'location'), 'transfer.locationIDOut = locationOut.locationID', array('locationOutName' => new Expression('locationOut.locationName')), 'left')
                ->join('entity', 'transfer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('status', 'transfer.transferStatus = status.statusID', array('statusName'), 'left')
                ->order(array('transferDate' => 'DESC', 'transferID' => 'DESC'))
                ->where(array('entity.deleted' => '0'));
        if ($locationId) {
            $select->where->equalTo('transfer.locationIDIn', $locationId)
                    ->or
                    ->equalTo('transfer.locationIDOut', $locationId);
        }
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function saveTransfer(Transfer $trasnfer)
    {

        $data = array(
            'transferCode' => $trasnfer->transferCode,
            'locationIDIn' => $trasnfer->locationIDIn,
            'locationIDOut' => $trasnfer->locationIDOut,
            'transferDate' => $trasnfer->transferDate,
            'transferDescription' => $trasnfer->transferDescription,
            'entityID' => $trasnfer->entityID,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getTransferByCode($code)
    {
        $rowset = $this->tableGateway->select(array('transferCode' => $code));
        $row = $rowset->current();
        return $row;
    }

    public function getTransferByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('*'))
                ->join('location', 'transfer.locationIDIn = location.locationID', array('locationInName' => new Expression('location.locationName')), 'left')
                ->join(array('locationOut' => 'location'), 'transfer.locationIDOut = locationOut.locationID', array('locationOutName' => new Expression('locationOut.locationName')), 'left')
                ->join('entity', 'transfer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0'))
                ->where(array('transfer.transferID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results) {
            $row = $results->current();
            return $row;
        } else {
            return $results;
        }
    }

    public function getTransferredProducts($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('*')) //'serialProductBatchID' => new Expression('productSerial.productBatchID'),
                ->join('transferProduct', 'transfer.transferID = transferProduct.transferID', array('*'), 'left')
                ->join('product', 'transferProduct.productID = product.productID', array('*'), 'left')
                ->join('productBatch', 'transferProduct.productBatchID = productBatch.productBatchID', array('productBatchCode'), 'left')
                ->join('productSerial', 'transferProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod'), 'left')
                ->join('productUom', 'productUom.productID = product.productID', array('*'), 'left')
                ->join('uom', 'uom.uomID = productUom.uomID', array('*'), 'left')
                ->join('entity', 'transfer.entityID = entity.entityID', array('*'), 'left')
                ->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left')
                ->where(array('entity.deleted' => '0'))
                ->where(array('transfer.transferID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results) {
            return $results;
        } else {
            return $results;
        }
    }

    public function getTransfersForSearch($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('*'))
                ->join('location', 'transfer.locationIDIn = location.locationID', array('locationInName' => new Expression('location.locationName')), 'left')
                ->join(array('locationOut' => 'location'), 'transfer.locationIDOut = locationOut.locationID', array('locationOutName' => new Expression('locationOut.locationName')), 'left')
                ->join('entity', 'transfer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('status', 'transfer.transferStatus = status.statusID', array('statusName'), 'left')
                ->where(array('entity.deleted' => '0'));

        $where = new Where();
        $where->like('transferCode', '%' . $keyword . '%');
//        ->or->like('location.locationName', '%' . $keyword . '%')
//        ->or->like('locationOut.locationName', '%' . $keyword . '%');
        $select->where($where);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param array $locFromIds
     * @param array $locToIds
     * @return $results
     */
    public function getTransferByLocIds($locFromIds, $locToIds, $fromDate = null, $toDate = null, $productIds = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('transferDate', 'tCD' => new Expression('transfer.transferCode'),
                    'tID' => new Expression('transfer.transferID')))
                ->join('transferProduct', 'transfer.transferID = transferProduct.transferID', array(
                    'qty' => new Expression('SUM(transferProduct.transferProductQuantity)')))
                ->join('product', 'product.productID =  transferProduct.productID', array(
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),
                    'pCD' => new Expression('product.productCode')))
                ->join('location', 'location.locationID = transfer.locationIDIn', array(
                    'locFromName' => new Expression('location.locationName'),
                    'locFromID' => new Expression('transfer.locationIDIn')))
                ->join(array('A' => 'location'), 'A.locationID = transfer.locationIDOut', array(
                    'locToName' => new Expression('A.locationName'),
                    'locToID' => new Expression('transfer.locationIDOut')))
                ->group(array('transferProduct.productID', 'transfer.transferID'))
        ->where->in('transfer.locationIDIn', $locFromIds)
        ->where->in('transfer.locationIDOut', $locToIds);

        if ($fromDate != null && $toDate != null) {
            $select->where->between('transfer.transferDate', $fromDate, $toDate);
        }

        if ($productIds != null) {
            $select->where->in('transferProduct.productID', $productIds);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function checkTransferByCode($transferCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('transfer')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "transfer.entityID = e.entityID")
//                    ->where(array('locationIDOut' => $locationID))
                    ->where(array('e.deleted' => '0'))
                    ->where(array('transfer.transferCode' => $transferCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param array $proIds
     * @param array $locIds
     * @param string $tansferType
     * @return $results
     */
    public function getLocationTransferData($proIds, $locIds, $tansferType)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from('transfer');
        $select->columns(array('transferID', $tansferType));
        $select->join('transferProduct', 'transfer.transferID = transferProduct.transferID', array(
            'transferProductQuantity' => new Expression('SUM(transferProductQuantity)')));
        $select->join('product', 'product.productID = transferProduct.productID ', array(
            'productID',
            'productName',
            'productCode'));
        $select->join('location', 'location.locationID = transfer.' . $tansferType, array(
            'locationName',
            'locationCode'));
        $select->order(array('transfer.transferID'));
        $select->group(array('transferProduct.productID',
            'transferProduct.transferID'));
        $select->where->in('transferProduct.productID', $proIds);
        $select->where->in('transfer.' . $tansferType, $locIds);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * Search transfers for drop down
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $searchKey
     * @return type
     */
    public function searchTransferForDropdown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('transferID', 'transferCode'))
                ->join('entity', 'transfer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0'));
        $select->where->like('transferCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Search transfers for document dropdown
     * @author Ashan Madushka <ashan@thinkcube.com>
     * @param type $searchKey
     * @return type
     */
    public function searchTransferForDocumentDropdown($searchKey, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('transferID', 'transferCode'))
                ->join('entity', 'transfer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0'));
        $select->where->like('transferCode', '%' . $searchKey . '%');
        if ($locationID) {
            $select->where->equalTo('locationIDIn', $locationID)->OR->equalTo('locationIDOut', $locationID);
        }
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get transfers by transferID
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $transferID
     * @return type
     */
    public function getTransfersByTransferID($transferID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('*'))
                ->join('location', 'transfer.locationIDIn = location.locationID', array('locationInName' => new Expression('location.locationName')), 'left')
                ->join(array('locationOut' => 'location'), 'transfer.locationIDOut = locationOut.locationID', array('locationOutName' => new Expression('locationOut.locationName')), 'left')
                ->join('entity', 'transfer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('status', 'transfer.transferStatus = status.statusID', array('statusName'), 'left')
                ->where(array('entity.deleted' => '0'));
        $select->where(array('transfer.transferID' => $transferID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getTransfersById($transferId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(array('*'))
                ->join('location', 'transfer.locationIDIn = location.locationID', array('locationInName' => new Expression('location.locationName'),'locationInID' => new Expression('location.locationID')), 'left')
                ->join(array('locationOut' => 'location'), 'transfer.locationIDOut = locationOut.locationID', array('locationOutName' => new Expression('locationOut.locationName'),'locationOutID' => new Expression('locationOut.locationID')), 'left');
        $select->where(array('transfer.transferID' => $transferId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Check transfers available for given product and location
     * @param type $productId
     * @param type $locationId
     * @return mixed
     */
    public function CheckTransferInByLocationProductId($productId, $locationId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('transfer')
                ->columns(['*'])
                ->join('transferProduct', 'transfer.transferID = transferProduct.transferID', ['*'], 'left')
                ->where(array('transferProduct.productID' => $productId, 'transfer.locationIDIn' => $locationId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

}
