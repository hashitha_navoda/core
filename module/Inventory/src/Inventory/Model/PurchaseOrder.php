<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the PO model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseOrder implements InputFilterAwareInterface
{

    public $purchaseOrderID;
    public $purchaseOrderCode;
    public $purchaseOrderSupplierID;
    public $purchaseOrderSupplierReference;
    public $purchaseOrderRetrieveLocation;
    public $purchaseOrderExpDelDate;
    public $purchaseOrderDate;
    public $purchaseOrderDescription;
    public $purchaseOrderDeliveryCharge;
    public $purchaseOrderTotal;
    public $purchaseOrderShowTax;
    public $status;
    public $entityID;
    public $purchaseOrderHashValue;
    public $purchaseOrderApproved;
    public $purchaseOrderDraftFlag;
    public $pucrhaseRequisitionID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseOrderID = (!empty($data['purchaseOrderID'])) ? $data['purchaseOrderID'] : null;
        $this->purchaseOrderCode = (!empty($data['purchaseOrderCode'])) ? $data['purchaseOrderCode'] : null;
        $this->purchaseOrderSupplierID = (!empty($data['purchaseOrderSupplierID'])) ? $data['purchaseOrderSupplierID'] : null;
        $this->purchaseOrderSupplierReference = (!empty($data['purchaseOrderSupplierReference'])) ? $data['purchaseOrderSupplierReference'] : null;
        $this->purchaseOrderRetrieveLocation = (!empty($data['purchaseOrderRetrieveLocation'])) ? $data['purchaseOrderRetrieveLocation'] : null;
        $this->purchaseOrderExpDelDate = (!empty($data['purchaseOrderExpDelDate'])) ? $data['purchaseOrderExpDelDate'] : null;
        $this->purchaseOrderDate = (!empty($data['purchaseOrderDate'])) ? $data['purchaseOrderDate'] : null;
        $this->purchaseOrderDescription = (!empty($data['purchaseOrderDescription'])) ? $data['purchaseOrderDescription'] : null;
        $this->purchaseOrderDeliveryCharge = (!empty($data['purchaseOrderDeliveryCharge'])) ? $data['purchaseOrderDeliveryCharge'] : 0.00;
        $this->purchaseOrderTotal = (!empty($data['purchaseOrderTotal'])) ? $data['purchaseOrderTotal'] : 0.00;
        $this->purchaseOrderShowTax = (!empty($data['purchaseOrderShowTax'])) ? $data['purchaseOrderShowTax'] : 0;
        $this->status = (!empty($data['status'])) ? $data['status'] : 3;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->purchaseOrderDraftFlag = (!empty($data['purchaseOrderDraftFlag'])) ? $data['purchaseOrderDraftFlag'] : 0;
        $this->pucrhaseRequisitionID = (!empty($data['pucrhaseRequisitionID'])) ? $data['pucrhaseRequisitionID'] : null;
        $this->purchaseOrderHashValue = (!empty($data['purchaseOrderHashValue'])) ? $data['purchaseOrderHashValue'] : null;
        $this->purchaseOrderApproved = (!empty($data['purchaseOrderApproved'])) ? $data['purchaseOrderApproved'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

