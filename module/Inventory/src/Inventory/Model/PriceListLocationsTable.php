<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Price List Locations Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class PriceListLocationsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //price list locations save function
    public function savePriceListLocations(PriceListLocations $priceListLocations)
    {
        $data = array(
            'priceListId' => $priceListLocations->priceListId,
            'locationId' => $priceListLocations->locationId,
        );
        try {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } catch (\Exception $e) {
            return false;
        }
    }

    //get Price list Locations by PriceListId
    public function getPriceListLocationsDataByPriceListId($priceListId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('priceList');
        $select->order('priceListName DESC');
        $select->join('priceListLocations', 'priceList.priceListId = priceListLocations.priceListId', array('locationId'));
        $select->join('location', 'priceListLocations.locationId = location.locationID', array('locationCode', 'locationName'));
        $select->join('entity', 'priceList.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0', 'priceList.priceListId' => $priceListId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
