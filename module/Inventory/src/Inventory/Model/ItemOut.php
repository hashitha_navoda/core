<?php

/**
 * @author Damith Thamara<damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ItemOut
{

    public $itemOutID;
    public $itemOutDocumentType;
    public $itemOutDocumentID;
    public $itemOutLocationProductID;
    public $itemOutBatchID;
    public $itemOutSerialID;
    public $itemOutItemInID;
    public $itemOutQty;
    public $itemOutPrice;
    public $itemOutAverageCostingPrice;
    public $itemOutDiscount;
    public $itemOutReturnQty;
    public $itemOutDateAndTime;
    public $itemOutJobCardReferenceID;
    public $itemOutDeletedFlag;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->itemOutID = (!empty($data['itemOutID'])) ? $data['itemOutID'] : null;
        $this->itemOutDocumentType = (!empty($data['itemOutDocumentType'])) ? $data['itemOutDocumentType'] : null;
        $this->itemOutDocumentID = (!empty($data['itemOutDocumentID'])) ? $data['itemOutDocumentID'] : null;
        $this->itemOutLocationProductID = (!empty($data['itemOutLocationProductID'])) ? $data['itemOutLocationProductID'] : null;
        $this->itemOutBatchID = (!empty($data['itemOutBatchID'])) ? $data['itemOutBatchID'] : null;
        $this->itemOutSerialID = (!empty($data['itemOutSerialID'])) ? $data['itemOutSerialID'] : null;
        $this->itemOutItemInID = (!empty($data['itemOutItemInID'])) ? $data['itemOutItemInID'] : 0;
        $this->itemOutQty = (!empty($data['itemOutQty'])) ? $data['itemOutQty'] : 0;
        $this->itemOutPrice = (!empty($data['itemOutPrice'])) ? $data['itemOutPrice'] : 0;
        $this->itemOutAverageCostingPrice = (!empty($data['itemOutAverageCostingPrice'])) ? $data['itemOutAverageCostingPrice'] : 0;
        $this->itemOutDiscount = (!empty($data['itemOutDiscount'])) ? $data['itemOutDiscount'] : 0;
        $this->itemOutReturnQty = (!empty($data['itemOutReturnQty'])) ? $data['itemOutReturnQty'] : 0;
        $this->itemOutDateAndTime = (!empty($data['itemOutDateAndTime'])) ? $data['itemOutDateAndTime'] : NULL;
        $this->itemOutJobCardReferenceID = (!empty($data['itemOutJobCardReferenceID'])) ? $data['itemOutJobCardReferenceID'] : NULL;
        $this->itemOutDeletedFlag = (!empty($data['itemOutDeletedFlag'])) ? $data['itemOutDeletedFlag'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
