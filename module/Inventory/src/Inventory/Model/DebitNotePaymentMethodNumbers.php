<?php

/**
 * @author Ashan     <ashan@thinkcube.com>
 * This file contains debit Note Payment method numbers Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DebitNotePaymentMethodNumbers implements InputFilterAwareInterface
{

    public $debitNotePaymentMethodNumbersID;
    public $debitNotePaymentID;
    public $paymentMethodID;
    public $debitNotePaymentMethodReferenceNumber;
    public $debitNotePaymentMethodBank;
    public $debitNotePaymentMethodCardID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->debitNotePaymentMethodNumbersID = (!empty($data['debitNotePaymentMethodNumbersID'])) ? $data['debitNotePaymentMethodNumbersID'] : null;
        $this->debitNotePaymentID = (!empty($data['debitNotePaymentID'])) ? $data['debitNotePaymentID'] : null;
        $this->paymentMethodID = (!empty($data['paymentMethodID'])) ? $data['paymentMethodID'] : null;
        $this->debitNotePaymentMethodReferenceNumber = (!empty($data['debitNotePaymentMethodReferenceNumber'])) ? $data['debitNotePaymentMethodReferenceNumber'] : null;
        $this->debitNotePaymentMethodBank = (!empty($data['debitNotePaymentMethodBank'])) ? $data['debitNotePaymentMethodBank'] : null;
        $this->debitNotePaymentMethodCardID = (!empty($data['debitNotePaymentMethodCardID'])) ? $data['debitNotePaymentMethodCardID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
