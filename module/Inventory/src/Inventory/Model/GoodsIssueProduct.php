<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class GoodsIssueProduct
{

    public $goodsIssueProductID;
    public $productBatchID;
    public $productSerialID;
    public $goodsIssueID;
    public $locationProductID;
    public $goodsIssueProductQuantity;
    public $unitOfPrice;
    public $uomID;
    public $goodsIssueUomID;
    public $uomConversionRate;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->goodsIssueProductID = (!empty($data['goodsIssueProductID'])) ? $data['goodsIssueProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->goodsIssueID = (!empty($data['goodsIssueID'])) ? $data['goodsIssueID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->goodsIssueProductQuantity = (!empty($data['goodsIssueProductQuantity'])) ? $data['goodsIssueProductQuantity'] : null;
        $this->unitOfPrice = (!empty($data['unitOfPrice'])) ? $data['unitOfPrice'] : null;
        $this->uomID = (!empty($data['uomID'])) ? $data['uomID'] : null;
        $this->goodsIssueUomID = (!empty($data['goodsIssueUomID'])) ? $data['goodsIssueUomID'] : null;
        $this->uomConversionRate = (!empty($data['uomConversionRate'])) ? $data['uomConversionRate'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
