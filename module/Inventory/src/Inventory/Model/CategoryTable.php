<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * This file contains Category Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class CategoryTable
{

    protected $tableGateway;
    protected $categoryPaths = array();
    protected $tmpName = "";
    protected $tmpArray = array();
    static $categoryPathNameSep = " < ";
    static $categoryLevels = 5;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category');
        $select->order('categoryName DESC');
        $select->join('entity', 'category.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getCategories()
    {
        $allCategoryList = array();
        foreach ($this->fetchAll() as $cat) {
            $cat = (object) $cat;
            $allCategoryList[$cat->categoryID] = $cat->categoryName;
        }

        return $allCategoryList;
    }

    /**
     * Generate array of all products with its ancestors as a single path
     * @return array
     */
    public function getHierarchyPaths()
    {

        $list = $this->getHierarchy();

        $this->tmpName = "";
        $this->tmpNamesArr = array();
        $this->_getHierarchyPathIterator($list[0]['child']);

        //remove last category level name from each string
        $newList = [];
        foreach ($this->tmpNamesArr as $catID => $path) {
//            $newList[$catID] = $path;
            $newList[$catID] = substr($path, (strpos($path, '<')) ? : strlen($path));
        }

        $this->tmpNamesArr = $newList;

        asort($this->tmpNamesArr, SORT_STRING);
        // to preserve key order when passed as json, wrap with another array
//        $tmpNamesArr = array();
//        foreach ($this->tmpNamesArr as $k => $v) {
//            $tmpNamesArr[] = array($k => $v);
//        }

        return $this->tmpNamesArr;
    }

    /**
     * Generate categories array nested under each parent
     * @return array
     */
    public function getHierarchy()
    {

        $res = $this->fetchAllHierarchical();
        $all = array();

        foreach ($res as $loo) {
            $all[$loo->categoryID] = array("data" => $loo);
        }

        // TODO check for performance
        foreach ($all as $key => &$loo) {
            if (isset($loo['data'])) {
                $all[$loo['data']->categoryParentID]['child'][$loo['data']->categoryID] = $loo;
                unset($all[$key]);
            }
        }

        return $all;
    }

    public function fetchAllHierarchical()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = $this->_generateQuery(self::$categoryLevels);
        $rowset = $adapter->query($sql, $adapter::QUERY_MODE_EXECUTE);

        return $rowset;
    }

    /**
     * Generates query to get hierarchical data of categories
     * @param int $level Category level to dig down to
     * @return string SQL Query
     */
    private function _generateQuery($level)
    {
        $queryParts = array();

        for ($i = 0; $i < $level; $i++) {
            $queryParts[] = $this->_generateQueryForLevel($i);
        }

        $query = implode(" UNION ", $queryParts);

        $query = <<<SQL
        SELECT *, @row := @row + 1 as row
        FROM
            ({$query}) r,
            (SELECT @row := 0) v
        ORDER BY
            row DESC
SQL;

        return $query;
    }

    /**
     * Generate query to get products of a category level
     * @param int $level
     * @return string SQL Query
     */
    private function _generateQueryForLevel($level = 0)
    {
        $baseQ = "SELECT category.* FROM category, entity WHERE category.categoryParentID = 0 AND category.entityID = entity.entityID AND entity.deleted = 0";
        $wrapperQ = "SELECT category.*  FROM category, entity WHERE category.categoryParentID IN (__SUBQ__) AND category.entityID = entity.entityID AND entity.deleted = 0";

        $genQ = $baseQ;
        for ($i = 0; $i < $level; $i++) {
            $genQ = str_replace("*", "categoryId", $genQ);
            $genQ = str_replace("__SUBQ__", $genQ, $wrapperQ);
        }

        return $genQ;
    }

    private function _getHierarchyPathIterator($list, $lastLevel = "")
    {

        foreach ($list as $loo) {
            if (isset($loo['data'])) {
                $this->tmpNamesArr[$loo['data']->categoryID] = $this->_getHierarchyPathAppender($lastLevel, $loo['data']->categoryName);
            }

            if (isset($loo['child'])) {
                $this->_getHierarchyPathIterator($loo['child'], end($this->tmpNamesArr));
            }
        }

        return $this->tmpNamesArr;
    }

    private function _getHierarchyPathAppender($lastLevel, $thisLevel)
    {
        if (empty($lastLevel)) {
            return $thisLevel;
        } else {
            return $thisLevel . self::$categoryPathNameSep . $lastLevel;
        }
    }

    public function getCategory($id)
    {
        $id = (int) $id;
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array('*'))
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryID' => $id, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function getCategoryByNameAndParentID($name, $parentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array('*'))
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryName' => $name, 'categoryParentID' => $parentID, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function getCategoryByPrefixAndParentID($prefix, $parentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array('*'))
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryPrefix' => $prefix, 'categoryParentID' => $parentID, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    public function saveCategory(Category $category)
    {

        // TODO save categoryLevel
        // if categoryID is passed, update current record
        if ($category->categoryID != null) {
            return $this->updateCategory($category);
        }

        $data = array(
            'categoryName' => $category->categoryName,
            'categoryParentID' => $category->categoryParentID,
            'categoryLevel' => $category->categoryLevel,
            'categoryPrefix' => $category->categoryPrefix,
            'categoryState' => $category->categoryState,
            'entityID' => $category->entityID
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateCategory(Category $category)
    {
        $data = array(
            'categoryName' => $category->categoryName,
            'categoryParentID' => $category->categoryParentID,
            'categoryLevel' => $category->categoryLevel,
            'categoryPrefix' => $category->categoryPrefix,
            'categoryState' => $category->categoryState
        );
        $this->tableGateway->update($data, array('categoryID' => $category->categoryID));
        return true;
    }

    public function updateCategoryState(Category $category)
    {
        $data = array(
            'categoryState' => $category->categoryState,
        );
        $this->tableGateway->update($data, array('categoryID' => $category->categoryID));
        return true;
    }

    public function isParent($id)
    {
        $id = (int) $id;
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array('*'))
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryParentID' => $id, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results->current();
        return(isset($row->categoryID)) ? TRUE : FALSE;
    }

    //this is used for check whether this parent category related categorys are active or inactive
    public function checkParent($id, $state)
    {
        $id = (int) $id;
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array('*'))
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryParentID' => $id, 'categoryState' => $state, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results->current();
        return(isset($row->categoryID)) ? TRUE : FALSE;
    }

    //this is used for check whether this categorys are active or inactive
    public function checkCategory($id, $state)
    {
        $id = (int) $id;
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array('*'))
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryID' => $id, 'categoryState' => $state, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results->current();
        return(isset($row->categoryID)) ? TRUE : FALSE;
    }

    public function getCategoryforSearch($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in categoryName )>0,POSITION(\'' . $keyword . '\' in categoryName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in categoryPrefix )>0,POSITION(\'' . $keyword . '\' in categoryPrefix), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(categoryName ), CHAR_LENGTH(categoryPrefix )) '),
                    '*'))
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left');
        $select->where(new PredicateSet(array(new Operator('category.categoryName', 'like', '%' . $keyword . '%'), new Operator('category.categoryPrefix', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return type
     */
    public function actionFetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from('category')
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryState' => '1', 'deleted' => 0))
                ->order('categoryName');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param array $cIds
     * @param array $locIds
     * @return $results
     */
    public function getCategoryQtyList($cIds, $locIds)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('category')
                ->columns(array('qty' => new Expression('SUM(locationProduct.locationProductQuantity)')))
                ->join('product', 'product.categoryID = category.categoryID', array(
                    'pID' => new Expression('product.productID'),
                    'pName' => new Expression('product.productName'),
                    'pCD' => new Expression('product.productCode'),
                    'cID' => new Expression('category.categoryID'),
                    'cName' => new Expression('category.categoryName'),
                    'pTID' => new Expression('productTypeID')
                        ), 'left')
                ->join('locationProduct', 'product.productID = locationProduct.productID', array(
                    'locProID' => new Expression('locationProduct.locationProductID')), 'left')
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where('product.categoryID IS NOT NULL')
                ->where(array('entity.deleted' => 0))
                ->group(array('locationProduct.productID'))
        ->where->notEqualTo('product.productTypeID', 2)
//        ->where->in('locationProduct.locationID', $locIds)
        ->where->in('category.categoryID', $cIds);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getCategoryList($cIds)
    {

        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array('cName' => new Expression('categoryName'),
                    'cID' => new Expression('categoryID')))
                ->order('categoryID')
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryState' => '1', 'deleted' => 0))
        ->where->in('categoryID', $cIds);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @param type $cIds
     * @param type $locIds
     * @return $results
     */
    public function getCategoryWiseMinimumInvenLevelDetails($cIds, $locIds, $isReach = true)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array(
                    'qty' => new Expression('locationProduct.locationProductQuantity'),
                    'locDefaultPrice' => new Expression('locationProduct.defaultSellingPrice')))
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),
                    'miL' => new Expression('locationProduct.locationProductMinInventoryLevel')))
                ->join('category', 'category.categoryID = product.categoryID', array(
                    'categoryName',
                    'categoryID'))
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locID' => new Expression('location.locationID'),
                    'locCD' => new Expression('locationCode')))
                ->join('entity', 'product.entityID=entity.entityID', array('deleted'))
                ->order('product.productName');

        if ($isReach) {
            $select->where('IFNULL(locationProduct.locationProductMinInventoryLevel,0) >= IFNULL(locationProduct.locationProductQuantity,0)');
        }

            $select->where(array('deleted' => 0, 'product.productState' => 1))
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->in('locationProduct.locationID', $locIds)
        ->where->in('category.categoryID', $cIds);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getCategoryWiseReorderLevelDetails($cIds, $locIds, $isReach = true)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('locationProduct')
                ->columns(array(
                    'qty' => new Expression('locationProduct.locationProductQuantity'),
                    'locDefaultPrice' => new Expression('locationProduct.defaultSellingPrice')))
                ->join('product', 'locationProduct.productID = product.productID', array(
                    'pCD' => new Expression('product.productCode'),
                    'pName' => new Expression('product.productName'),
                    'pID' => new Expression('product.productID'),
                    'roL' => new Expression('locationProduct.locationProductReOrderLevel')))
                ->join('category', 'category.categoryID = product.categoryID', array(
                    'categoryName',
                    'categoryID'))
                ->join('location', 'location.locationID = locationProduct.locationID', array(
                    'locName' => new Expression('location.locationName'),
                    'locID' => new Expression('location.locationID'),
                    'locCD' => new Expression('locationCode')))
                ->join('entity', 'product.entityID=entity.entityID', array('deleted'))
                ->order('product.productName');

        if ($isReach) {
            $select->where('IFNULL(locationProduct.locationProductReOrderLevel,0) >= IFNULL(locationProduct.locationProductQuantity,0)');
        }
            $select->where(array('deleted' => 0, 'product.productState' => 1))
        ->where->notEqualTo('product.productTypeID', 2)
        ->where->in('locationProduct.locationID', $locIds)
        ->where->in('category.categoryID', $cIds);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * get count of rows what wizardFlag is 1
     * @return null|boolean
     */
    public function checkWizardFlag()
    {
        $rowset = $this->tableGateway->select(array('wizardFlag' => 1));

        if (count($rowset) == 0) {
            return NULL;
        }
        return true;
    }

    //get category by  default category Name
    public function getCategoryByCategoryName($categoryName)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array('*'))
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryState' => '1', 'deleted' => 0, 'categoryName' => $categoryName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $results = (object) $results->current();
        return $results;
    }

    /**
     * Get all category list or get category list according to search key
     * @param string $searchKey
     * @return mixed
     */
    public function getActiveCategories($searchKey = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('category')
                ->columns(array('*'))
                ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                ->where(array('categoryState' => '1', 'deleted' => 0));
        if (isset($searchKey)) {
            $select->where(new PredicateSet(array(new Operator('category.categoryName', 'like', '%' . $searchKey . '%'))));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * get category details by parent ID
    * @param int $id
    * @return mixed
    **/
    public function getCategoryByParentID($id)
    {
        try {
            $id = (int) $id;
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('category')
                    ->columns(array('*'))
                    ->join('entity', 'category.entityID = entity.entityID', array('*'), 'left')
                    ->where(array('category.categoryParentID' => $id, 'entity.deleted' => '0'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            return $results;

        } catch (Exception $e) {
            return false;
        }
    }

}
