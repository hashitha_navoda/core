<?php
namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class DepreciationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('depreciation');
        $select->join('entity', 'depreciation.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function saveDepreciation($depreciationData)
    {
        $data = array(
            'productID' => $depreciationData->productID,
            'depreciationValue' => $depreciationData->depreciationValue,
            'depreciationDate' => $depreciationData->depreciationDate,
            'entityID' => $depreciationData->entityID
        );
        try {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } catch (\Exception $e) {
            return false;
        }
    }

    //get Price list by PriceListName
    public function getTotalDepreciationByProductID($productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('depreciation')
               ->columns(['totalDepreciation' => new Expression('SUM(depreciationValue)')])
               ->join('entity', 'depreciation.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0', 'depreciation.productID' => $productID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

   public function getTotalDepreciationByProductIDArray($productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('depreciation')
               ->columns(['productID','totalDepreciation' => new Expression('SUM(depreciationValue)')])
               ->join('entity', 'depreciation.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        $select->where->in('productID',$productID);
        $select->group(array('depreciation.productID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    //get Price list by PriceListId
    public function getPriceListItemsDataByPriceListId($priceListId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('priceList');
        $select->order('priceListName DESC');
        $select->join('priceListItems', 'priceList.priceListId = priceListItems.priceListId', array('productId', 'priceListItemsPrice', 'priceListItemsDiscountType', 'priceListItemsDiscount'));
        $select->join('product', 'priceListItems.productId = product.productID', array('productCode', 'productName'));
        $select->join('entity', 'priceList.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0', 'priceList.priceListId' => $priceListId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}
