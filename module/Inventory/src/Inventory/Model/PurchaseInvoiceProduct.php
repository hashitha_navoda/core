<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the PIProduct model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseInvoiceProduct implements InputFilterAwareInterface
{

    public $purchaseInvoiceProductID;
    public $purchaseInvoiceID;
    public $locationProductID;
    public $productBatchID;
    public $productSerialID;
    public $purchaseInvoiceProductQuantity;
    public $purchaseInvoiceProductTotalQty;
    public $purchaseInvoiceProductPrice;
    public $purchaseInvoiceProductDiscount;
    public $purchaseInvoiceProductTotal;
    public $purchaseInvoiceProductDocumentTypeID;
    public $purchaseInvoiceProductDocumentID;
    public $purchaseInvoiceProductSelectedUomId;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseInvoiceProductID = (!empty($data['purchaseInvoiceProductID'])) ? $data['purchaseInvoiceProductID'] : null;
        $this->purchaseInvoiceID = (!empty($data['purchaseInvoiceID'])) ? $data['purchaseInvoiceID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->purchaseInvoiceProductQuantity = (!empty($data['purchaseInvoiceProductQuantity'])) ? $data['purchaseInvoiceProductQuantity'] : null;
        $this->purchaseInvoiceProductTotalQty = (!empty($data['purchaseInvoiceProductTotalQty'])) ? $data['purchaseInvoiceProductTotalQty'] : null;
        $this->purchaseInvoiceProductPrice = (!empty($data['purchaseInvoiceProductPrice'])) ? $data['purchaseInvoiceProductPrice'] : 0.00;
        $this->purchaseInvoiceProductDiscount = (!empty($data['purchaseInvoiceProductDiscount'])) ? $data['purchaseInvoiceProductDiscount'] : 0;
        $this->purchaseInvoiceProductTotal = (!empty($data['purchaseInvoiceProductTotal'])) ? $data['purchaseInvoiceProductTotal'] : 0.00;
        $this->purchaseInvoiceProductDocumentTypeID = (!empty($data['purchaseInvoiceProductDocumentTypeID'])) ? $data['purchaseInvoiceProductDocumentTypeID'] : null;
        $this->purchaseInvoiceProductDocumentID = (!empty($data['purchaseInvoiceProductDocumentID'])) ? $data['purchaseInvoiceProductDocumentID'] : null;
        $this->purchaseInvoiceProductSelectedUomId = (!empty($data['purchaseInvoiceProductSelectedUomId'])) ? $data['purchaseInvoiceProductSelectedUomId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

