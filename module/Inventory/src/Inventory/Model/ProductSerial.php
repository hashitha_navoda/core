<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductSerial
{

    public $productSerialID;
    public $locationProductID;
    public $productBatchID;
    public $productSerialCode;
    public $productSerialWarrantyPeriod;
    public $productSerialWarrantyPeriodType;
    public $productSerialExpireDate;
    public $productSerialSold;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialCode = (!empty($data['productSerialCode'])) ? $data['productSerialCode'] : null;
        $this->productSerialWarrantyPeriod = (!empty($data['productSerialWarrantyPeriod'])) ? $data['productSerialWarrantyPeriod'] : null;
        $this->productSerialWarrantyPeriodType = (!empty($data['productSerialWarrantyPeriodType'])) ? $data['productSerialWarrantyPeriodType'] : null;
        $this->productSerialExpireDate = (!empty($data['productSerialExpireDate'])) ? $data['productSerialExpireDate'] : null;
        $this->productSerialSold = (!empty($data['productSerialSold'])) ? $data['productSerialSold'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

