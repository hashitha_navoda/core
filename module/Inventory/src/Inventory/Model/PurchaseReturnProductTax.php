<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the POProductTax model
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PurchaseReturnProductTax implements InputFilterAwareInterface
{

    public $purchaseReturnProductTaxID;
    public $purchaseReturnID;
    public $purchaseReturnProductID;
    public $purchaseReturnTaxID;
    public $purchaseReturnTaxPrecentage;
    public $purchaseReturnTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->purchaseReturnProductTaxID = (!empty($data['purchaseReturnProductTaxID'])) ? $data['purchaseReturnProductTaxID'] : null;
        $this->purchaseReturnID = (!empty($data['purchaseReturnID'])) ? $data['purchaseReturnID'] : null;
        $this->purchaseReturnProductID = (!empty($data['purchaseReturnProductID'])) ? $data['purchaseReturnProductID'] : null;
        $this->purchaseReturnTaxID = (!empty($data['purchaseReturnTaxID'])) ? $data['purchaseReturnTaxID'] : null;
        $this->purchaseReturnTaxPrecentage = (!empty($data['purchaseReturnTaxPrecentage'])) ? $data['purchaseReturnTaxPrecentage'] : null;
        $this->purchaseReturnTaxAmount = (!empty($data['purchaseReturnTaxAmount'])) ? $data['purchaseReturnTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

