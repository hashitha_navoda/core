<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\CompoundCostPurchaseInvoice;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class CompoundCostPurchaseInvoiceTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCompoundCostPurchaseInvoice(CompoundCostPurchaseInvoice $compoundCostPurchaseInvoice)
    {
        try {
            $data = array(
                'compoundCostTemplateID' => $compoundCostPurchaseInvoice->compoundCostTemplateID,
                'purchaseInvoiceID' => $compoundCostPurchaseInvoice->purchaseInvoiceID,
            );
            $this->tableGateway->insert($data);
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;

        } catch (\Exception $exc) {
            error_log($exc);

        }
    }

}