<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Price List Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class PriceListTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('priceList');
        $select->order('priceListName DESC');
        $select->join('priceListLocations', 'priceList.priceListId = priceListLocations.priceListId', array('locationId'));
        $select->join('entity', 'priceList.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($locationID) {
            $select->where(array('priceListLocations.locationId' => $locationID));
        }
        $select->group('priceListId');

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //price list save function
    public function savePriceList(PriceList $priceList)
    {
        $data = array(
            'priceListName' => $priceList->priceListName,
            'entityID' => $priceList->entityID,
            'priceListState' => $priceList->priceListState,
            'priceListGlobal' => $priceList->priceListGlobal,
        );
        try {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } catch (\Exception $e) {
            return false;
        }
    }

    //get Price list by PriceListName
    public function getPriceListByPriceListName($priceListName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('priceList')
               ->columns(['*'])
               ->join('entity', 'priceList.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0', 'priceList.priceListName' => $priceListName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    //get Price list by PriceListID
    public function getPriceListByPriceListId($priceListId)
    {
        $rowset = $this->tableGateway->select(array('priceListId' => $priceListId));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    //get Price list by PriceListId
    public function getPriceListItemsDataByPriceListId($priceListId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('priceList');
        $select->order('priceListName DESC');
        $select->join('priceListItems', 'priceList.priceListId = priceListItems.priceListId', array('productId', 'priceListItemsPrice', 'priceListItemsDiscountType', 'priceListItemsDiscount'));
        $select->join('product', 'priceListItems.productId = product.productID', array('productCode', 'productName'));
        $select->join('entity', 'priceList.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0', 'priceList.priceListId' => $priceListId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //inactive PriceList
    public function activeInactivePriceList($priceListId, $priceListState)
    {
        $data = array(
            'priceListState' => $priceListState,
        );
        try {
            $this->tableGateway->update($data, array('priceListId' => $priceListId,));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function fetchAllForDorpDown()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('priceList')
                ->columns(array('*'))
                ->order('priceListName ASC');
        $select->join('entity', 'priceList.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $data = array();
        foreach ($results as $value) {
            $data[$value['priceListId']] = $value['priceListName'];
        }
        return $data;
    }

}
