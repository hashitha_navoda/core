<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Gift Card Table Functions
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class GiftCardTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //save gift card details
    public function saveGiftCard(GiftCard $giftCard)
    {

        $data = array(
            'giftCardCode' => $giftCard->giftCardCode,
            'giftCardDuration' => $giftCard->giftCardDuration,
            'giftCardIssueDate' => $giftCard->giftCardIssueDate,
            'giftCardExpireDate' => $giftCard->giftCardExpireDate,
            'giftCardValue' => $giftCard->giftCardValue,
            'giftCardStatus' => $giftCard->giftCardStatus,
            'giftCardGlAccountID' => $giftCard->giftCardGlAccountID,
        );

        try {
            $this->tableGateway->insert($data);
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    //get gift card details by giftCardCode
    public function getGiftCardDetailsByGiftCardCode($giftCardCode)
    {
        $rowset = $this->tableGateway->select(array('giftCardCode' => $giftCardCode));
        $row1 = $rowset->current();
        $row = (object) $row1;
        if (!$row) {
            throw new \Exception("Could not find row $giftCardCode");
        }
        return $row;
    }

    //update gift card details by gift card code
    public function updateGiftCardDetailsByGiftCardCode($giftCardData, $giftCardCode)
    {
        try {
            $this->tableGateway->update($giftCardData, array('giftCardCode' => $giftCardCode));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    //update gift card details by gift card code
    public function updateGiftCardDetailsByGiftCardId($giftCardData, $giftCardId)
    {
        try {
            $this->tableGateway->update($giftCardData, array('giftCardId' => $giftCardId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    //get all open gift cards
    public function getAllOpenGiftCards()
    {
        $rowset = $this->tableGateway->select(array('giftCardStatus' => 3));
        return $rowset;
    }

     //get gift card details by giftCardCode
    public function getGiftCardDetailsByGiftCardID($giftCardID)
    {
        $rowset = $this->tableGateway->select(array('giftCardId' => $giftCardID));
        $row1 = $rowset->current();
        $row = (object) $row1;
        if (!$row) {
            throw new \Exception("Could not find row $giftCardID");
        }
        return $row;
    }

}
