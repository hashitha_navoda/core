<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\ExpiryAlertNotification;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class ExpiryAlertNotificationTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function addAlerts($addedID, $exAlerts)
    {
        $data = array(
            'productID' => $addedID,
            'NumberOfDays' => $exAlerts,
        );
        try {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        } catch (\Exception $exc) {
            echo $exc;
            return false;
        }
    }
    /**
    * this function use to get all expire Alert details by given product ID
    * @param int $productID
    * return array
    **/
    public function getDetailsByProductID($productID)
    {
        try {
            
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('expiryAlertNotification');
            $select->where(array('productID' => $productID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            
            $returnData = array(
                'data' => $rowset,
                'status' => true  
                );
            return $returnData;
        
        } catch (\Exception $ex) {
            error_log($ex);
            $returnData = array(
                'status' => false  
                );
            return $returnData;
        }
    }

    /**
    * this function use to delete existing records by given product ID
    * @param int $productID
    * return array
    **/
    public function deleteDetailsByProductID ($productID)
    {
         try {
            
            $sql = new Sql($this->tableGateway->getAdapter());
            $delete = $sql->delete();
            $delete->from('expiryAlertNotification');
            $delete->where(array('productID' => $productID));
            $query = $sql->prepareStatementForSqlObject($delete);
            $rowset = $query->execute();
            return true;
        
        } catch(\Exception $ex) {
            error_log($ex);
            return false;
        }
    }

}
