<?php

/**
 * @author ashan     <ashan@thinkcube.com>
 * This file contains debit note payment method numbers Table Functions
 */

namespace Inventory\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DebitNotePaymentMethodNumbersTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDebitNotePaymentMethodsNumbers(DebitNotePaymentMethodNumbers $DebitNotePaymentMethodsNumbers)
    {
        $data = array(
            'debitNotePaymentID' => $DebitNotePaymentMethodsNumbers->debitNotePaymentID,
            'paymentMethodID' => $DebitNotePaymentMethodsNumbers->paymentMethodID,
            'debitNotePaymentMethodReferenceNumber' => $DebitNotePaymentMethodsNumbers->debitNotePaymentMethodReferenceNumber,
            'debitNotePaymentMethodBank' => $DebitNotePaymentMethodsNumbers->debitNotePaymentMethodBank,
            'debitNotePaymentMethodCardID' => $DebitNotePaymentMethodsNumbers->debitNotePaymentMethodCardID,
        );
        $this->tableGateway->insert($data);
        return true;
    }

}
