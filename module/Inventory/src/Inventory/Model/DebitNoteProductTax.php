<?php

/**
 * @author Ashan madushka <ashan@thinkcube.com>
 * This file contains debitNote product tax model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class DebitNoteProductTax
{

    public $debitNoteProductTaxID;
    public $debitNoteProductID;
    public $taxID;
    public $debitNoteProductTaxPercentage;
    public $debitNoteProductTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->debitNoteProductTaxID = (!empty($data['debitNoteProductTaxID'])) ? $data['debitNoteProductTaxID'] : null;
        $this->debitNoteProductID = (!empty($data['debitNoteProductID'])) ? $data['debitNoteProductID'] : null;
        $this->taxID = (!empty($data['taxID'])) ? $data['taxID'] : null;
        $this->debitNoteProductTaxPercentage = (!empty($data['debitNoteProductTaxPercentage'])) ? $data['debitNoteProductTaxPercentage'] : 0.00;
        $this->debitNoteProductTaxAmount = (!empty($data['debitNoteProductTaxAmount'])) ? $data['debitNoteProductTaxAmount'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
