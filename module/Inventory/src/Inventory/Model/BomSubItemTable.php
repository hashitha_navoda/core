<?php

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\Product;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class BomSubItemTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveBomSubItem(BomSubItem $bomSubItem)
    {
        $data = array(
            'bomID' => $bomSubItem->bomID,
            'productID' => $bomSubItem->productID,
            'bomSubItemQuantity' => $bomSubItem->bomSubItemQuantity,
            'bomSubItemUnitPrice' => $bomSubItem->bomSubItemUnitPrice,
            'bomSubItemCost' => $bomSubItem->bomSubItemCost,
        );
        try {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        } catch (\Exception $exc) {
            echo $exc;
            return false;
        }
    }

    public function getBomSubDataByBomID($bomID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bomSubItem');
        $select->join('bom', 'bomSubItem.bomID=  bom.bomID', array('bomCode', 'bomTotalCost', 'bomDiscription'), "left");
        $select->join('product', 'bomSubItem.productID=  product.productID', array("productName", "productDescription", "productTypeID"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->join('productUom', 'product.productID=  productUom.productID', array("uomID", "productUomConversion", "productUomDisplay"), "left");
        $select->order('bomID DESC');
        $select->where(array('bomSubItem.bomID' => $bomID, 'productUom.productUomDisplay' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function deleteBomSubDataByBomID($bomID)
    {
        $result = $this->tableGateway->delete(array('bomID' => $bomID));
    }

    public function getBomSubDataAndProductDataByBomIDAndLocationID($bomID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bomSubItem');
        $select->columns(array('bomSubItemID', 'bomID', 'bomSubItemQuantity'));
        $select->join('product', 'bomSubItem.productID=  product.productID', array("productName", "productID", "productDefaultSellingPrice","productTypeID"), "left");
        $select->join('entity', 'product.entityID=  entity.entityID', array("deleted"), "left");
        $select->join('locationProduct', 'product.productID=  locationProduct.productID', array("locationProductQuantity", "locationID", "locationProductID", "defaultSellingPrice", "defaultPurchasePrice"), "left");
        $select->join('productUom', 'product.productID=  productUom.productID', array("uomID", "productUomConversion", "productUomDisplay"), "left");
        $select->join('uom', 'productUom.uomID=  uom.uomID', array("uomAbbr", "uomDecimalPlace"), "left");
        $select->where(array('bomSubItem.bomID' => $bomID, 'locationProduct.locationID' => $locationID, 'entity.deleted' => 0, 'productUom.productUomDisplay' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
