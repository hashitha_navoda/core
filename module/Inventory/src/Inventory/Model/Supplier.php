<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Supplier Model Functions
 */

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Supplier implements InputFilterAwareInterface
{

    public $supplierID;
    public $supplierTitle;
    public $supplierCode;
    public $supplierName;
    public $supplierCurrency;
    public $supplierAddress;
    public $supplierTelephoneNumber;
    public $supplierEmail;
    public $supplierVatNumber;
    public $supplierCreditLimit;
    public $supplierPaymentTerm;
    public $supplierOutstandingBalance;
    public $supplierCreditBalance;
    public $supplierOther;
    public $supplierPayableAccountID;
    public $supplierPurchaseDiscountAccountID;
    public $supplierGrnClearingAccountID;
    public $supplierAdvancePaymentAccountID;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->supplierID = (!empty($data['supplierID'])) ? $data['supplierID'] : null;
        $this->supplierCode = (!empty($data['supplierCode'])) ? $data['supplierCode'] : null;
        $this->supplierTitle = (!empty($data['supplierTitle'])) ? $data['supplierTitle'] : null;
        $this->supplierName = (!empty($data['supplierName'])) ? $data['supplierName'] : null;
        $this->supplierCurrency = (!empty($data['supplierCurrency'])) ? $data['supplierCurrency'] : null;
        $this->supplierAddress = (!empty($data['supplierAddress'])) ? $data['supplierAddress'] : null;
        $this->supplierTelephoneNumber = (!empty($data['supplierTelephoneNumber'])) ? $data['supplierTelephoneNumber'] : null;
        $this->supplierEmail = (!empty($data['supplierEmail'])) ? $data['supplierEmail'] : null;
        $this->supplierVatNumber = (!empty($data['supplierVatNumber'])) ? $data['supplierVatNumber'] : null;
        $this->supplierCreditLimit = (!empty($data['supplierCreditLimit'])) ? $data['supplierCreditLimit'] : null;
        $this->supplierPaymentTerm = (!empty($data['supplierPaymentTerm'])) ? $data['supplierPaymentTerm'] : 1;
        $this->supplierOutstandingBalance = (!empty($data['supplierOutstandingBalance'])) ? $data['supplierOutstandingBalance'] : 0.00;
        $this->supplierCreditBalance = (!empty($data['supplierCreditBalance'])) ? $data['supplierCreditBalance'] : 0.00;
        $this->supplierOther = (!empty($data['supplierOther'])) ? $data['supplierOther'] : null;
        $this->supplierPayableAccountID = (!empty($data['supplierPayableAccountID'])) ? $data['supplierPayableAccountID'] : null;
        $this->supplierPurchaseDiscountAccountID = (!empty($data['supplierPurchaseDiscountAccountID'])) ? $data['supplierPurchaseDiscountAccountID'] : null;
        $this->supplierGrnClearingAccountID = (!empty($data['supplierGrnClearingAccountID'])) ? $data['supplierGrnClearingAccountID'] : null;
        $this->supplierAdvancePaymentAccountID = (!empty($data['supplierAdvancePaymentAccountID'])) ? $data['supplierAdvancePaymentAccountID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'supplierID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'supplierName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'supplierAddress',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'supplierTelephoneNumber',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'supplierEmail',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
