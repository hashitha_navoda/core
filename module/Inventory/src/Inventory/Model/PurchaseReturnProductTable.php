<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Inventory\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class PurchaseReturnProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePurchaseReturnProduct(PurchaseReturnProduct $prProduct)
    {
        $data = array(
            'purchaseReturnID' => $prProduct->purchaseReturnID,
            'purchaseReturnLocationProductID' => $prProduct->purchaseReturnLocationProductID,
            'purchaseReturnProductBatchID' => $prProduct->purchaseReturnProductBatchID,
            'purchaseReturnProductSerialID' => $prProduct->purchaseReturnProductSerialID,
            'purchaseReturnProductPrice' => $prProduct->purchaseReturnProductPrice,
            'purchaseReturnProductDiscount' => $prProduct->purchaseReturnProductDiscount,
            'purchaseReturnProductPurchasedQty' => $prProduct->purchaseReturnProductPurchasedQty,
            'purchaseReturnProductReturnedQty' => $prProduct->purchaseReturnProductReturnedQty,
            'purchaseReturnSubProductReturnedQty' => $prProduct->purchaseReturnSubProductReturnedQty,
            'purchaseReturnProductTotal' => $prProduct->purchaseReturnProductTotal,
            'purchaseReturnProductGrnProductID' => $prProduct->purchaseReturnProductGrnProductID,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    /**
     * Get grn wise return product details
     * @return array
     */
    public function getGrnWiseProductReturnQuantityDetails()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseReturnProduct')
                ->columns([
                    'purchaseReturnProductID',
                    'purchaseReturnID',
                    'purchaseReturnLocationProductID',
                    'purchaseReturnProductSerialID',
                    'purchaseReturnProductBatchID',
                    'grnReturnQty' => new Expression(''
                            . 'CASE '
                            . 'WHEN purchaseReturnProductSerialID IS NULL AND purchaseReturnProductBatchID IS NULL THEN SUM(purchaseReturnProductReturnedQty) '
                            . 'ELSE SUM(purchaseReturnSubProductReturnedQty) END'),
                    ])
                ->join('purchaseReturn', 'purchaseReturnProduct.purchaseReturnID = purchaseReturn.purchaseReturnID', ['purchaseReturnGrnID'], 'left');
        $select->group("purchaseReturn.purchaseReturnGrnID", "purchaseReturnProduct.purchaseReturnLocationProductID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowSet = $statement->execute();

        $grnReturnArr = [];
        foreach ($rowSet as $row) {
            $grnReturnArr[$row['purchaseReturnGrnID']] = $row['grnReturnQty'];
        }
        return $grnReturnArr;
    }

}

