<?php

namespace Inventory\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PoDraft
{

    public $poDraftID;
    public $poDraftDetails;
 
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->poDraftID = (!empty($data['poDraftID'])) ? $data['poDraftID'] : null;
        $this->poDraftDetails = (!empty($data['poDraftDetails'])) ? $data['poDraftDetails'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}