<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains SupplierAPI related controller functions
 */

namespace Inventory\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Core\Controller\CoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Inventory\Model\Supplier;
use Core\Model\Entity;
use Zend\Session\Container;
use Inventory\Model\SupplierCategory;
use Inventory\Model\SupplierCategoryComponent;
use Inventory\Model\Product;
use Inventory\Form\ProductForm;
use Inventory\Model\ProductUom;
use Inventory\Model\ProductHandeling;
use Inventory\Model\LocationProduct;

class SupplierAPIController extends CoreController
{

    protected $paginator;
    protected $supplierCategorypaginator;

    public function saveSupplierAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        $supplierCheck = $this->CommonTable('Inventory\Model\SupplierTable')->getNotDeletedSupplierByCode($post['supplierCode']);
        if ($supplierCheck != NULL) {
            $this->msg = $this->getMessage('ERR_SUPAPI_CODEEXIST');
            $this->status = 'false';
            return $this->JSONRespond();
        } else {
            $postData['supplierTitle'] = $post['supplierTitle'];
            $postData['supplierName'] = $post['supplierName'];
            $postData['supplierCode'] = $post['supplierCode'];
            $postData['supplierCurrency'] = $post['supplierCurrency'];
            $postData['supplierTelephoneNumber'] = $post['supplierTelephoneNumber'];
            $postData['supplierEmail'] = $post['supplierEmail'];
            $postData['supplierAddress'] = $post['supplierAddress'];
            $postData['supplierVatNumber'] = $post['supplierVatNumber'];
            $postData['supplierCreditLimit'] = $post['supplierCreditLimit'];
            $postData['supplierPaymentTerm'] = $post['supplierPaymentTerm'];
            $postData['supplierOutstandingBalance'] = $post['supplierOutstandingBalance'];
            $postData['supplierCreditBalance'] = $post['supplierCreditBalance'];
            $postData['supplierOther'] = $post['supplierOther'];
            $postData['supplierPayableAccountID'] = $post['supplierPayableAccountID'];
            $postData['supplierPurchaseDiscountAccountID'] = $post['supplierPurchaseDiscountAccountID'];
            $postData['supplierGrnClearingAccountID'] = $post['supplierGrnClearingAccountID'];
            $postData['supplierAdvancePaymentAccountID'] = $post['supplierAdvancePaymentAccountID'];
            $supplierModel = new Supplier();
            $supplierModel->exchangeArray($postData);
            $entityID = $this->createEntity();
            $supplierModel->entityID = $entityID;
            $insertedID = $this->CommonTable('Inventory\Model\SupplierTable')->saveSupplier($supplierModel);

            $supplierCategoryIDs = $post['supplierCategory'];
            if (is_array($supplierCategoryIDs)) {
                $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->insertData($insertedID, $supplierCategoryIDs);
            }
            $this->msg = $this->getMessage('SUCC_SUPAPI_ADD');
            // $this->msg = 'Supplier added successfully.!';
            $this->status = true;
            $addedSupplierData['supplierID'] = $insertedID;
            $addedSupplierData['supplierName'] = $postData['supplierName'];
            $addedSupplierData['supplierCode'] = $postData['supplierCode'];
            $this->data = $addedSupplierData;
            $this->flashMessenger()->addMessage(['status'=>true,'msg'=>$this->msg]);

            $this->setLogMessage("supplier ".$postData['supplierCode']."-".$post['supplierName']." created.");
            return $this->JSONRespond();
        }
    }

    public function updateSupplierAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        if ($request->isPost()) {
            $supplierID = $post['supplierID'];
            $categoryIDs = $post['supplierCategory'];
            $existingSuppliercategoryIDs = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->getCategoryBySupplierID($supplierID);
            if (is_array($categoryIDs)) {
                $this->_updateSupplierCategory($supplierID, $categoryIDs);
            }
            $postData['supplierTitle'] = $post['supplierTitle'];
            $postData['supplierName'] = $post['supplierName'];
            $postData['supplierCode'] = $post['supplierCode'];
            $postData['supplierCurrency'] = $post['supplierCurrency'];
            $postData['supplierTelephoneNumber'] = $post['supplierTelephoneNumber'];
            $postData['supplierEmail'] = $post['supplierEmail'];
            $postData['supplierAddress'] = $post['supplierAddress'];
            $postData['supplierVatNumber'] = $post['supplierVatNumber'];
            $postData['supplierCreditLimit'] = $post['supplierCreditLimit'];
            $postData['supplierPaymentTerm'] = $post['supplierPaymentTerm'];
            $postData['supplierOther'] = $post['supplierOther'];
            $postData['supplierPayableAccountID'] = $post['supplierPayableAccountID'];
            $postData['supplierPurchaseDiscountAccountID'] = $post['supplierPurchaseDiscountAccountID'];
            $postData['supplierGrnClearingAccountID'] = $post['supplierGrnClearingAccountID'];
            $postData['supplierAdvancePaymentAccountID'] = $post['supplierAdvancePaymentAccountID'];

            //get supplier previous data and new data arrays
            $changesArray = $this->getChangeDetailsOfSupplier($postData,$supplierID,$categoryIDs,$existingSuppliercategoryIDs);
            //update supplier details

            $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplier($postData, $supplierID);
            $entityID = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID)->entityID;
            //update entity info
            $this->updateEntity($entityID);

            $this->getPaginatedSuppliers();
            $suppliersView = new ViewModel(array('suppliers' => $this->paginator));
            $suppliersView->setTerminal(true);
            $suppliersView->setTemplate('inventory/supplier/supplier-list');
            
            //set log details
            $previousData = json_encode($changesArray['previousData']);
            $newData = json_encode($changesArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("supplier ".$changesArray['previousData']['supplierCode']." updated.");

            $this->msg = $this->getMessage('SUCC_SUPAPI_UPDATE');
            $this->status = true;
            $this->html = $suppliersView;
            return $this->JSONRespondHtml();
        }
    }

    private function getChangeDetailsOfSupplier($postData,$supplierID,$categoryIDs,$existingSuppliercategoryIDs)
    {
        $preData = (array) $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
        $previousData['supplierTitle'] = $preData['supplierTitle'];
        $previousData['supplierName'] = $preData['supplierName'];
        $previousData['supplierCode'] = $preData['supplierCode'];
        $previousData['supplierCurrency'] = $preData['supplierCurrency'];
        $previousData['supplierTelephoneNumber'] = $preData['supplierTelephoneNumber'];
        $previousData['supplierEmail'] = $preData['supplierEmail'];
        $previousData['supplierAddress'] = $preData['supplierAddress'];
        $previousData['supplierVatNumber'] = $preData['supplierVatNumber'];
        $previousData['supplierCreditLimit'] = $preData['supplierCreditLimit'];
        $previousData['supplierPaymentTerm'] = $preData['supplierPaymentTerm'];
        $previousData['supplierOther'] = $preData['supplierOther'];
        $previousData['supplierPayableAccountID'] = $preData['supplierPayableAccountID'];
        $previousData['supplierPurchaseDiscountAccountID'] = $preData['supplierPurchaseDiscountAccountID'];
        $previousData['supplierGrnClearingAccountID'] = $preData['supplierGrnClearingAccountID'];
        $previousData['supplierAdvancePaymentAccountID'] = $preData['supplierAdvancePaymentAccountID'];
        $previousData['supplierCategories'] = $existingSuppliercategoryIDs;

        $postData['supplierCategories'] = $categoryIDs;
        
        return array('previousData' => $previousData, 'newData' => $postData);
    }

    /**
     * this check data already data exists in supplierCategoryComponent table,
     *  then add or delete data in SupplierCategoryCompentTable
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param int $supplierID
     * @param array $newSuppliercategoryIDs
     */
    private function _updateSupplierCategory($supplierID, $newSuppliercategoryIDs)
    {
//        add data if not already exists
        foreach ($newSuppliercategoryIDs as $supplierCategoryID) {
            $isExixts = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->isDataExists($supplierID, $supplierCategoryID[0]);

            if (!$isExixts) {
                $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->insertSingleData($supplierID, $supplierCategoryID[0]);
            }
        }
//        delete if 'new array' doesn't having the data
        $existingSuppliercategoryIDs = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->getCategoryBySupplierID($supplierID);
        foreach ($existingSuppliercategoryIDs as $existingSuppliercategoryID) {
            $result = FALSE;

            foreach ($newSuppliercategoryIDs as $newSuppliercategoryID) {
                $result = ($existingSuppliercategoryID == $newSuppliercategoryID[0]) ? TRUE : FALSE;
                if ($result) {
                    break;
                }
            }

            if (!$result) {
                $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->deleteData($supplierID, $existingSuppliercategoryID);
            }
        }
    }

    public function deleteSupplierAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            //supplier update transaction begin
            $this->beginTransaction();
            //check request supplier id is null
            if (!is_null($request->getPost('sID'))) {
                $supplierID = $request->getPost('sID');
                $chksuplr = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                //check whether request delete supplier in database or not
                if ($chksuplr != NULL) {
                    //check whether supplier have transaction
                    $checkSupplier = $this->CommonTable('Inventory\Model\SupplierTable')->checkSupplierTransaction($supplierID);
                    $suplTran = ($checkSupplier) ? (object) $checkSupplier->current() : '';
                    //if supplier does not have transaction then delete supplier from database
                    if ($suplTran == '' || !$suplTran->purchaseOrderID && !$suplTran->purchaseInvoiceID && !$suplTran->outgoingPaymentID && !$suplTran->debitNoteID && !$suplTran->debitNotePaymentID && !$suplTran->grnID && !$suplTran->paymentVoucherID && !$suplTran->productID) {
                        $entityID = $chksuplr->entityID;
                        $res = $this->updateDeleteInfoEntity($entityID);
                        if (!is_null($suplTran->supplierCategoryComponentID)) {
                            $delteSupCategory = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->deleteBySupplierID($supplierID);
                        }
                        if ($res == false) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_SUPAPI_SUP_CANNOT_DELETE');
                            $this->data = FALSE;
                        } else {
                            $this->commit();
                            $this->setLogMessage("Supplier ".$chksuplr->supplierCode ." is deleted");
                            $this->status = true;
                            $this->msg = $this->getMessage('SUCC_SUPAPI_DELE');
                            $this->data = TRUE;
                        }
                        //if supplier have any transaction the given message for user
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_SUPAPI_SUP_CANNOT_DELETE');
                        $this->data = FALSE;
                    }
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_SUPAPI_INVALID_SUP');
                    $this->data = FALSE;
                }
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_SUPAPI_SUP_CANNOT_DELETE');
                $this->data = FALSE;
            }
        }
        return $this->JSONRespond();
    }

    public function getSupplierDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierCategoryArray = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->getCategoryBySupplierID($request->getPost('sID'));
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($request->getPost('sID'));
            
            $financeAccountsArray = $this->getFinanceAccounts();
            $supplierPayableAccountID = $supplierData->supplierPayableAccountID;
            $supplierPayableAccountName = $financeAccountsArray[$supplierPayableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPayableAccountID]['financeAccountsName'];
            $supplierPurchaseDiscountAccountID = $supplierData->supplierPurchaseDiscountAccountID;
            $supplierPurchaseDiscountAccountName = $financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsName'];
            $supplierGrnClearingAccountID = $supplierData->supplierGrnClearingAccountID;
            $supplierGrnClearingAccountName = $financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsName'];
            $supplierAdvancePaymentAccountID = $supplierData->supplierAdvancePaymentAccountID;
            $supplierAdvancePaymentAccountName = $financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsName'];
            
            $suppplierDArray = array(
                'sN' => $supplierData->supplierName,
                'sNT' => $supplierData->supplierTitle,
                'sC' => $supplierData->supplierCode,
                'sCA' => (is_array($supplierCategoryArray)) ? $supplierCategoryArray : NULL,
                'sCU' => $supplierData->supplierCurrency,
                'sA' => $supplierData->supplierAddress,
                'sT' => $supplierData->supplierTelephoneNumber,
                'sE' => $supplierData->supplierEmail,
                'sV' => $supplierData->supplierVatNumber,
                'sCL' => $supplierData->supplierCreditLimit,
                'sP' => $supplierData->supplierPaymentTerm,
                'sOB' => $supplierData->supplierOutstandingBalance,
                'sCB' => $supplierData->supplierCreditBalance,
                'sO' => $supplierData->supplierOther,
                'sPAID' => $supplierPayableAccountID,
                'sPAN' => $supplierPayableAccountName,
                'sPDAID' => $supplierPurchaseDiscountAccountID,
                'sPDAN' => $supplierPurchaseDiscountAccountName,
                'sGCAID' => $supplierGrnClearingAccountID,
                'sGCAN' => $supplierGrnClearingAccountName,
                'sAPAID' => $supplierAdvancePaymentAccountID,
                'sAPAN' => $supplierAdvancePaymentAccountName,
            );
            $this->data = $suppplierDArray;
            $this->setLogMessage("Retrieve supplier details of ".$supplierData->supplierCode.".");
            return $this->JSONRespond();
        }
    }

    public function getSuppliersForSearchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->setLogMessage("Supplier list accessed.");
            if ($request->getPost('searchSupplierString') != '') {
                $suppliers = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliersforSearch($request->getPost('searchSupplierString'));
                $supplierListView = new ViewModel(array('suppliers' => $suppliers, 'paginated' => false));
                $supplierListView->setTemplate('inventory/supplier/supplier-list');
                $this->html = $supplierListView;
                return $this->JSONRespondHtml();
            } else {
                $this->getPaginatedSuppliers();
                $supplierListView = new ViewModel(array(
                    'suppliers' => $this->paginator
                ));
                $supplierListView->setTemplate('inventory/supplier/supplier-list');
                $this->html = $supplierListView;
                return $this->JSONRespondHtml();
            }
        }
    }

    public function getSupplierListViewAction()
    {
        $this->getPaginatedSuppliers();
        $supplierListView = new ViewModel(array('suppliers' => $this->paginator));
        $supplierListView->setTemplate('inventory/supplier/supplier-list');
        $this->html = $supplierListView;
        return $this->JSONRespondHtml();
    }

    private function getPaginatedSuppliers()
    {
        $this->paginator = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliers(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    /**
     * search supplier list for display list
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchSupplierCategoryAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        $keyword = $post['supplierCategoryName'];

        if (!empty($keyword)) {
            $categoryList = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->getCategoryforSearch($keyword);
            $paginated = false;
        } else {
            $this->getPaginatedSupplierCategory();
            $categoryList = $this->supplierCategorypaginator;
            $paginated = true;
        }

        $view = new ViewModel(array(
            'categoryList' => $categoryList,
            'paginated' => $paginated
        ));
        $view->setTerminal(true);
        $view->setTemplate('inventory/supplier/supplier-category-list');

        $this->setLogMessage("Supplier category list accessed.");

        $this->status = true;
        $this->html = $view;
        return $this->JSONRespondHtml();
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function saveSupplierCategoryAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        $supplierCategoryCheck = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->getSupplierCategoryByName($post['supplierCategoryName']);

        if ($supplierCategoryCheck != NULL) {
            $this->setLogMessage("Error occred When create supplier category called ".$post['supplierCategoryName'].'.');
            $this->msg = $this->getMessage('ERR_SUPAPI_CATGEXIST');
            $this->status = 'false';
            return $this->JSONRespond();
        } else {
            $postData['supplierCategoryName'] = $post['supplierCategoryName'];

            $supplierCategory = new SupplierCategory();

            $supplierCategory->exchangeArray($postData);
            $insertedID = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->saveSupplierCategory($supplierCategory);

            $this->getPaginatedSupplierCategory();

            $view = new ViewModel(array(
                'categoryList' => $this->supplierCategorypaginator,
                'paginated' => true
            ));
            $view->setTerminal(true);
            $view->setTemplate('inventory/supplier/supplier-category-list');

            $this->setLogMessage($postData['supplierCategoryName']." supplier category created.");

            $this->status = true;
            $this->data = '';
            $this->html = $view;
            $this->msg = $this->getMessage('SUCC_SUPAPI_CATGADD');
            return $this->JSONRespondHtml();
        }
    }

    /**
     * get Supplier Category Name according to supplierCategoryID
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function getSupplierCategoryNameAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $supplierCategoryName = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->getSupplierCategoryByID($post['supplierCategoryID']);

            $this->setLogMessage("Retrieve ".$supplierCategoryName->supplierCategoryName." supplier category.");
            $this->status = true;
            $this->data = $supplierCategoryName;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function updateSupplierCategoryAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        $isExistsName = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->getSupplierCategoryByName($post['supplierCategoryName']);
        $preData = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->getSupplierCategoryByID($post['supplierCategoryID']);

        if ($isExistsName == NULL || $isExistsName->supplierCategoryID == $post['supplierCategoryID']) {
            $newData = array(
                'supplierCategoryName' =>$post['supplierCategoryName'],
            );

            $previousData = array(
                'supplierCategoryName' =>$preData->supplierCategoryName,
            );
            
            $supplierCategory = new SupplierCategory();
            $supplierCategory->supplierCategoryID = $post['supplierCategoryID'];
            $supplierCategory->supplierCategoryName = $post['supplierCategoryName'];
            $this->CommonTable('Inventory\Model\SupplierCategoryTable')->updateSupplierCategory($supplierCategory);

            $this->getPaginatedSupplierCategory();

            $view = new ViewModel(array(
                'categoryList' => $this->supplierCategorypaginator,
                'paginated' => true
            ));
            $view->setTerminal(true);
            $view->setTemplate('inventory/supplier/supplier-category-list');

            $newData = json_encode($newData);
            $previousData = json_encode($previousData);
            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage($preData->supplierCategoryName." Supplier category updated.");

            $this->status = true;
            $this->data = '';
            $this->html = $view;
            $this->msg = $this->getMessage('SUCC_SUPAPI_CATGUPDATE');
            return $this->JSONRespondHtml();
        } else {
            $this->setLogMessage("Error occred When update supplier category called ".$preData->supplierCategoryName.'.');
            $this->status = false;
            $this->msg = $this->getMessage('ERR_SUPAPI_NAMEEXIST');
            return $this->JSONRespond();
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function deleteSupplierCategoryAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        if ($request->isPost()) {
            $supplierCategoryID = $post['supplierCategoryID'];
            $post = $request->getPost();
            $checkSupplierCategory = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->getDataBySupplierCategoryID($supplierCategoryID);
            $SupplierCategoryData = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->getSupplierCategoryByID($supplierCategoryID);
            if ($checkSupplierCategory->count() == 0) {
                $deletedID = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->deleteSupplierCategory($supplierCategoryID);

                if ($deletedID) {
                    $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->deleteByCategoryID($supplierCategoryID);
                    $this->getPaginatedSupplierCategory();

                    $view = new ViewModel(array(
                        'categoryList' => $this->supplierCategorypaginator,
                        'paginated' => true
                    ));
                    $view->setTerminal(true);
                    $view->setTemplate('inventory/supplier/supplier-category-list');

                    $this->setLogMessage($SupplierCategoryData->supplierCategoryName." supplier category deleted.");
                    $this->status = true;
                    $this->data = '';
                    $this->html = $view;
                    $this->msg = $this->getMessage('SUCC_SUPAPI_CATGDELE');
                    return $this->JSONRespondHtml();
                } else {
                    $this->setLogMessage($SupplierCategoryData->supplierCategoryName."supplier category delete error occured.");
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_SUPAPI_CATGDELE');
                    return $this->JSONRespondHtml();
                }
            } else {
                $this->setLogMessage($SupplierCategoryData->supplierCategoryName."supplier category delete error occred.");
                $this->status = false;
                $this->msg = $this->getMessage('ERR_SUPAPI_CATGUSEDDELE');
                return $this->JSONRespondHtml();
            }
        }
    }

    /**
     * @author sharmilan <sharmilan@gmail.com>
     * @param type $perPage
     */
    protected function getPaginatedSupplierCategory($perPage = 6)
    {
        $this->supplierCategorypaginator = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->fetchAll(true);
        $this->supplierCategorypaginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->supplierCategorypaginator->setItemCountPerPage($perPage);
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchSuppliersForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $supplierSearchKey = $searchrequest->getPost('searchKey');
            $suppliers = $this->CommonTable('Inventory\Model\SupplierTable')->searchSupplierForDropDown($supplierSearchKey);

            $supplierList = array();
            foreach ($suppliers as $supplier) {
                $temp['value'] = $supplier['supplierID'];
                $temp['text'] = $supplier['supplierName'] . '-' . $supplier['supplierCode'];
                $supplierList[] = $temp;
            }

            $this->data = array('list' => $supplierList);
            $this->setLogMessage("Get supplier list for dropdown.");
            return $this->JSONRespond();
        }
    }

    //get supplier import confirm view
    public function getImportConfirmAction()
    {
        $viewmodel = new ViewModel();
        $viewmodel->setTerminal(true);
        return $viewmodel;
    }

    //supplier import action
    public function importSupplierAction()
    {
        $request = $this->getRequest();
        //check whether request is post
        $writtenrowcount = 0;
        $replacerowcount = 0;
        if ($request->isPost()) {
            $subCategory = $request->getPost('subCategory');
            $subTitles = $request->getPost('subTitle');

            //supplier import transaction begin
            $this->beginTransaction();
            foreach ($subCategory as $key => $value) {
                $id = explode('-', $value)[0];
                if ($id == 'new_category') {
                    $category = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->getBySupplierCategoryBySupplierCategoryName($key);
                    //check new category name exist
                    if ($category['supplierCategoryID']) {//if exist assign existing category id
                        $subCategory[$key] = $category['supplierCategoryID'];
                    } else {// if not exist craete category
                        $categoryData = [
                            'supplierCategoryName' => $key
                        ];
                        $newCategory = new SupplierCategory();
                        $newCategory->exchangeArray($categoryData);
                        $categoryId = $this->CommonTable('Inventory/Model/SupplierCategoryTable')->saveSupplierCategory($newCategory);
                        if (!$categoryId) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_SUPAPI_SUP_SAVE');
                            return $this->JSONRespond();
                        }
                        $subCategory[$key] = $categoryId;
                    }
                }
            }

            $delim = $request->getPost('delim');
            //open csv file
            $filecontent = fopen('/tmp/ezBiz.supplierImportData.csv', "r");

            if ($request->getPost('header') == TRUE) {
                $linecontent = fgetcsv($filecontent, 1000, $delim);
            }
            $colarray = $request->getPost('col');

            $columncount = $request->getPost('columncount');
            
            //check this account use accounting..
            $companyDetails = $this->getCompanyDetails();
            $companyUseAccountingFlag = false;
            $glAcconts = [];
            if($companyDetails[0]->companyUseAccounting == 1){
                $companyUseAccountingFlag = true;
                //get supplier related default accounts
                $accDetails = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
                $glAcconts = iterator_to_array($accDetails)[0];
            }

            //validate supplier CSV
            $supValidate = $this->validateSupplierCsv($delim, $linecontent, $colarray, $request->getPost('header'));
            if (!$supValidate['status']) {
                $this->rollback();
                $this->status = false;
                $this->msg = $supValidate['msg'];
                return $this->JSONRespond();
            }
                            
            
            //supplier data loop
            while ($linecontent = fgetcsv($filecontent, 1000, $delim)) {
                $importarray = array();
                $columns = count($linecontent);
                //get ecah customer details for array
                for ($content = 0; $content < $columns; $content++) {
                    if ($colarray[$content] == 'Supplier Title') {
                        $importarray['supplierTitle'] = $subTitles[$linecontent[$content]];
                    } elseif ($colarray[$content] == 'Supplier Name') {
                        $importarray['supplierName'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Supplier Code') {
                        $importarray['supplierCode'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Currency') {
                        $importarray['supplierCurrency'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Telephone') {
                        $importarray['supplierTelephoneNumber'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Email') {
                        $importarray['supplierEmail'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Address') {
                        $importarray['supplierAddress'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Tax Registration No') {
                        $importarray['supplierVatNumber'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Outstanding Balance') {
                        $importarray['supplierOutstandingBalance'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Credit Balance') {
                        $importarray['supplierCreditBalance'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Credit Limit') {
                        $importarray['supplierCreditLimit'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Other') {
                        $importarray['supplierOther'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Supplier Category') {
                        $importarray['supplierCategory'] = $subCategory[$linecontent[$content]];
                    }
                }

                //set supplier default gLAccounts
                if($companyUseAccountingFlag){
                    $importarray['supplierPayableAccountID'] = $glAcconts->glAccountSetupPurchasingAndSupplierPayableAccountID;
                    $importarray['supplierPurchaseDiscountAccountID'] = $glAcconts->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID;
                    $importarray['supplierGrnClearingAccountID'] = $glAcconts->glAccountSetupPurchasingAndSupplierGrnClearingAccountID;
                    $importarray['supplierAdvancePaymentAccountID'] = $glAcconts->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID;
                }
                //set supplier exchange array
                $importSupplier = new Supplier();
                $importSupplier->exchangeArray($importarray);
                
                if (is_null($importSupplier->supplierCode)) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = "Supplier Code is missing in some supplier, Therefore cannot continue.";
                    return $this->JSONRespond();
                }

                $currencyData = $this->CommonTable('Core\Model\CurrencyTable')->getCurrencyByName($importSupplier->supplierCurrency);
                if ($currencyData) {
                    $importSupplier->supplierCurrency = $currencyData->currencyID;
                } else {
                    $currencyData = $this->CommonTable('Settings\Model\DisplaySetupTable')->getSystemCurrency()->current();
                    $importSupplier->supplierCurrency = $currencyData['currencyID'];
                }
                if ($request->getPost('method') == 'discard') {
                    //check supplier is alredy in database
                    $searchSupplier = $this->CommonTable('Inventory\Model\SupplierTable')->getNotDeletedSupplierByCode($importSupplier->supplierCode);
                    //if supplier not in database save those suppliers
                    if (!isset($searchSupplier->supplierID)) {
                        //create entity id for customer
                        $entityID = $this->createEntity();
                        $importSupplier->entityID = $entityID;
                        //call supplier save function in supplier table
                        $importres = $this->CommonTable('Inventory\Model\SupplierTable')->saveSupplier($importSupplier);
                        if ($importres == TRUE) {
                            $importarray['supplierID'] = $importres;
                            $importarray['supplierCategoryID'] = $importarray['supplierCategory'];
                            $importSupCategory = new SupplierCategoryComponent();
                            $importSupCategory->exchangeArray($importarray);
                            //save supplier category
                            if (!is_null($importSupCategory->supplierCategoryID)) {
                                $supCategoryData = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->insertSingleData($importSupCategory->supplierID, $importSupCategory->supplierCategoryID);
                            }
                            $writtenrowcount++;
                            if (floatval($importSupplier->supplierOutstandingBalance) > 0) {
                                $invResult = $this->storeInvoiceForSupplierOpeningBalance($importres, $importSupplier);
                                if (!$invResult['status']) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $invResult['msg'];
                                    return $this->JSONRespond();
                                }
                            }
                            if (floatval($importSupplier->supplierCreditBalance) > 0) {
                                $advPaymentResult = $this->storeAdavancePaymentForSupplierOpeningCreditBalance($importres, $importSupplier);
                                if (!$advPaymentResult['status']) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $advPaymentResult['msg'];
                                    return $this->JSONRespond();
                                }
                            }
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_SUPAPI_SUP_SAVE');
                            return $this->JSONRespond();
                        }
                    }
                } else if ($request->getPost('method') == 'replace') {
                    //check supplier is alredy in database
                    $searchSupplier = $this->CommonTable('Inventory\Model\SupplierTable')->getNotDeletedSupplierByCode($importSupplier->supplierCode);
                    //if supplier is in database update those suppliers
                    if (isset($searchSupplier->supplierID)) {
                        //set supplier entity id for new update
                        $importSupplier->supplierID = $searchSupplier->supplierID;
                        $importSupplier->entityID = $searchSupplier->entityID;
                        $importSupplier->supplierCode = $searchSupplier->supplierCode;

                        if (floatval($searchSupplier->supplierOutstandingBalance) != 0 && floatval($searchSupplier->supplierCreditBalance) != 0) {
                            $supData = array(
                                'supplierTitle' => $importSupplier->supplierTitle,
                                'supplierName' => $importSupplier->supplierName,
                                'supplierCurrency' => $importSupplier->supplierCurrency,
                                'supplierAddress' => $importSupplier->supplierAddress,
                                'supplierTelephoneNumber' => $importSupplier->supplierTelephoneNumber,
                                'supplierEmail' => $importSupplier->supplierEmail,
                                'supplierVatNumber' => $importSupplier->supplierVatNumber,
                                'supplierOther' => $importSupplier->supplierOther,
                                'supplierCreditLimit' => $importSupplier->supplierCreditLimit,
                            );
                        } else if (floatval($searchSupplier->supplierOutstandingBalance) != 0 && floatval($searchSupplier->supplierCreditBalance) == 0) {
                            $supData = array(
                                'supplierTitle' => $importSupplier->supplierTitle,
                                'supplierName' => $importSupplier->supplierName,
                                'supplierCurrency' => $importSupplier->supplierCurrency,
                                'supplierAddress' => $importSupplier->supplierAddress,
                                'supplierTelephoneNumber' => $importSupplier->supplierTelephoneNumber,
                                'supplierEmail' => $importSupplier->supplierEmail,
                                'supplierVatNumber' => $importSupplier->supplierVatNumber,
                                'supplierCreditBalance' => $importSupplier->supplierCreditBalance,
                                'supplierOther' => $importSupplier->supplierOther,
                                'supplierCreditLimit' => $importSupplier->supplierCreditLimit,
                            );
                        } else if (floatval($searchSupplier->supplierOutstandingBalance) == 0 && floatval($searchSupplier->supplierCreditBalance) != 0) {
                            $supData = array(
                                'supplierTitle' => $importSupplier->supplierTitle,
                                'supplierName' => $importSupplier->supplierName,
                                'supplierCurrency' => $importSupplier->supplierCurrency,
                                'supplierAddress' => $importSupplier->supplierAddress,
                                'supplierTelephoneNumber' => $importSupplier->supplierTelephoneNumber,
                                'supplierEmail' => $importSupplier->supplierEmail,
                                'supplierVatNumber' => $importSupplier->supplierVatNumber,
                                'supplierOutstandingBalance' => $importSupplier->supplierOutstandingBalance,
                                'supplierOther' => $importSupplier->supplierOther,
                                'supplierCreditLimit' => $importSupplier->supplierCreditLimit,
                            );
                        } else {
                            $supData = array(
                                'supplierTitle' => $importSupplier->supplierTitle,
                                'supplierName' => $importSupplier->supplierName,
                                'supplierCurrency' => $importSupplier->supplierCurrency,
                                'supplierAddress' => $importSupplier->supplierAddress,
                                'supplierTelephoneNumber' => $importSupplier->supplierTelephoneNumber,
                                'supplierEmail' => $importSupplier->supplierEmail,
                                'supplierVatNumber' => $importSupplier->supplierVatNumber,
                                'supplierOutstandingBalance' => $importSupplier->supplierOutstandingBalance,
                                'supplierCreditBalance' => $importSupplier->supplierCreditBalance,
                                'supplierOther' => $importSupplier->supplierOther,
                                'supplierCreditLimit' => $importSupplier->supplierCreditLimit,
                            );
                        }

                        //update supplier
                        $importres = $this->CommonTable('Inventory\Model\SupplierTable')->importReplaceSupplier($supData,$importSupplier->supplierID);
                        if ($importres == TRUE) {
                            $importarray['supplierID'] = $searchSupplier->supplierID;
                            $importarray['supplierCategoryID'] = $importarray['supplierCategory'];
                            $importSupCategory = new SupplierCategoryComponent();
                            $importSupCategory->exchangeArray($importarray);
                            //update supplier
                            $supCategoryOldData = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->getCategoryBySupplierID($importSupCategory->supplierID);
                            if (empty($supCategoryOldData)) {
                                if (!is_null($importSupCategory->supplierCategoryID)) {
                                    $supCategoryData = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->insertSingleData($importSupCategory->supplierID, $importSupCategory->supplierCategoryID);
                                }
                            } else {
                                $supCategoryData = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->updateSupplierCategoryComponentBySupplierID($importSupCategory);
                            }
                            $replacerowcount++;
                            if ((floatval($searchSupplier->supplierOutstandingBalance) != floatval($importSupplier->supplierOutstandingBalance)) && $importSupplier->supplierOutstandingBalance != NULL) {
                                $locationID = $this->user_session->userActiveLocation['locationID'];
                                $invDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBySupplierID($searchSupplier->supplierID,$locationID);

                                if (count($invDetails) == 0) {
                                    $invResult = $this->storeInvoiceForSupplierOpeningBalance($searchSupplier->supplierID, $importSupplier);
                                    if (!$invResult['status']) {
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $invResult['msg'];
                                        return $this->JSONRespond();
                                    }
                                } else if (count($invDetails) == 1) {
                                    foreach ($invDetails as $key => $value) {
                                        if ($value['purchaseInvoiceComment'] === "Created By Supplier Opening Balance" && floatval($value['purchaseInvoicePayedAmount']) == 0 && $value['status'] == "3") {
                                            $invoiceData = [
                                                'purchaseInvoiceTotal' => $importSupplier->supplierOutstandingBalance
                                            ];
                                            $invoiceUpdateStatus = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->updatePiDetails($invoiceData, $value['purchaseInvoiceID']);

                                            $invProductData = array(
                                                'purchaseInvoiceProductPrice' => $importSupplier->supplierOutstandingBalance,
                                                'purchaseInvoiceProductTotal' => $importSupplier->supplierOutstandingBalance
                                            );
                                            
                                            //update purchase invoice product table
                                            $updateProductTable = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->updatePurchaseInvoicePoductByPurchaseInvoiceID($invProductData, $value['purchaseInvoiceID']);
                                        } else {
                                            $this->rollback();
                                            $this->status = false;
                                            $this->msg = $this->getMessage('ERR_SUPPAPI_SUP_REPLACE_PI_CLOSED');
                                            return $this->JSONRespond();
                                        }
                                    }
                                } else if (count($invDetails) > 1) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_SUPPAPI_SUP_REPLACE_MORE_PI');
                                    return $this->JSONRespond();
                                }                               
                            }
                            
                            if ((floatval($searchSupplier->supplierCreditBalance) != floatval($importSupplier->supplierCreditBalance)) && $importSupplier->supplierCreditBalance != NULL) {
                                $locationID = $this->user_session->userActiveLocation['locationID'];
                                $paymentDetails = $this->CommonTable('SupplierPaymentsTable')->getPaymentsBySupplierID($searchSupplier->supplierID,$locationID, null);

                                if (count($paymentDetails) == 0) {
                                    $advPaymentResult = $this->storeAdavancePaymentForSupplierOpeningCreditBalance($searchSupplier->supplierID, $importSupplier);
                                    if (!$advPaymentResult['status']) {
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $advPaymentResult['msg'];
                                        return $this->JSONRespond();
                                    }
                                } else if (count($paymentDetails) == 1) {
                                    foreach ($paymentDetails as $key => $value) {
                                        if ($value['outgoingPaymentMemo'] === "Created By Supplier Opening Credit Balance" && $value['outgoingPaymentStatus'] == "4") {
                                            $paymentData = [
                                                'outgoingPaymentAmount' => $importSupplier->supplierCreditBalance
                                            ];
                                            $paymentUpdateStatus = $this->CommonTable('SupplierPaymentsTable')->update($paymentData, $value['outgoingPaymentID']);

                                        } else {
                                            $this->rollback();
                                            $this->status = false;
                                            $this->msg = $this->getMessage('ERR_SUPPAPI_SUP_REPLACE_ADV_EDITED');
                                            return $this->JSONRespond();
                                        }
                                    }
                                } else if (count($paymentDetails) > 1) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_SUPPAPI_SUP_REPLACE_MORE_PAYMENT');
                                    return $this->JSONRespond();
                                }                               
                            }

                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_SUPAPI_SUP_SAVE');
                            return $this->JSONRespond();
                        }
                        //if supplier not in database save those suppliers
                    } else {
                        //create entity id for supplier
                        $entityID = $this->createEntity();
                        $importSupplier->entityID = $entityID;
                        //save supplier
                        $importres = $this->CommonTable('Inventory\Model\SupplierTable')->saveSupplier($importSupplier);
                        if ($importres == TRUE) {
                            $importarray['supplierID'] = $importres;
                            $importarray['supplierCategoryID'] =  $importarray['supplierCategory'];
                            $importSupCategory = new SupplierCategoryComponent();
                            $importSupCategory->exchangeArray($importarray);
                            //save supplier category
                            if (!is_null($importSupCategory->supplierCategoryID)) {
                                $supCategoryData = $this->CommonTable('Inventory\Model\SupplierCategoryComponentTable')->insertSingleData($importSupCategory->supplierID, $importSupCategory->supplierCategoryID);
                            }
                            $writtenrowcount++;
                            if (floatval($importSupplier->supplierOutstandingBalance) > 0) {
                                $invResult = $this->storeInvoiceForSupplierOpeningBalance($importres, $importSupplier);
                                if (!$invResult['status']) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $invResult['msg'];
                                    return $this->JSONRespond();
                                }
                            }
                            if (floatval($importSupplier->supplierCreditBalance) > 0) {
                                $advPaymentResult = $this->storeAdavancePaymentForSupplierOpeningCreditBalance($importres, $importSupplier);
                                if (!$advPaymentResult['status']) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $advPaymentResult['msg'];
                                    return $this->JSONRespond();
                                }
                            }
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_SUPAPI_SUP_SAVE');
                            return $this->JSONRespond();
                        }
                    }
                }
            }
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('ERR_SUPAPI_SUP_SAVE_SUCC', array($writtenrowcount, $replacerowcount));
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_SUPAPI_REQ_NOTPOST');
        }
        return $this->JSONRespond();
    }

    /*
    * This fuction is used validate customer import csv
    * @param $delimeter, $fileContent
    * @return @resultset
    */
    public function validateSupplierCsv($delim, $linecontent, $colarray, $header)
    {
        $finalArray = array();
        $fContent = fopen('/tmp/ezBiz.supplierImportData.csv', "r");
        while ($linecontent = fgetcsv($fContent, 1000, $delim)) {
            $importarray = array();
            $columns = count($linecontent);
            //get ecah customer details for array
            for ($content = 0; $content < $columns; $content++) {
                if ($colarray[$content] == 'Supplier Title') {
                    $importarray['supplierTitle'] = $subTitles[$linecontent[$content]];
                } elseif ($colarray[$content] == 'Supplier Name') {
                    $importarray['supplierName'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Supplier Code') {
                    $importarray['supplierCode'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Currency') {
                    $importarray['supplierCurrency'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Telephone') {
                    $importarray['supplierTelephoneNumber'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Email') {
                    $importarray['supplierEmail'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Address') {
                    $importarray['supplierAddress'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Tax Registration No') {
                    $importarray['supplierVatNumber'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Outstanding Balance') {
                    $importarray['supplierOutstandingBalance'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Credit Balance') {
                    $importarray['supplierCreditBalance'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Credit Limit') {
                    $importarray['supplierCreditLimit'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Other') {
                    $importarray['supplierOther'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Supplier Category') {
                    $importarray['supplierCategory'] = $subCategory[$linecontent[$content]];
                }
            }
            $finalArray[] = $importarray;
        }

        $supplierCodeArray = Array();
        foreach ($finalArray as $key => $value) {
            if ($value['supplierCode'] == "") {
                return ['status' => false, 'msg' => $this->getMessage('ERR_SUP_CODE_EMPTY_IN_CSV')];
            }
            array_push($supplierCodeArray, $value['supplierCode']);
            if ($header == 1) {
                if ($key != 0) {
                    if (!is_null($value['supplierTelephoneNumber'])) {
                        if(!is_numeric($value['supplierTelephoneNumber'])) {
                            return ['status' => false, 'msg' => $this->getMessage('ERR_SUP_TELE_NON_NUMER')];               
                        } else if (strlen($value['supplierTelephoneNumber']) < 9 || strlen($value['supplierTelephoneNumber']) > 13) {
                            return ['status' => false, 'msg' => $this->getMessage('ERR_SUP_TELE_NUMER_LENGTH')];               
                        } 
                    } 

                    if (!is_null($value['supplierOutstandingBalance'])) {
                        if (!is_numeric($value['supplierOutstandingBalance'])) {
                            return ['status' => false, 'msg' => $this->getMessage('ERR_SUP_NON_NUMERIC_OUTSTANDING')];               
                        }
                    }

                    if (!is_null($value['supplierCreditBalance'])) {
                        if (!is_numeric($value['supplierCreditBalance'])) {
                            return ['status' => false, 'msg' => $this->getMessage('ERR_SUP_NON_NUMERIC_CREDIT')];               
                        }
                    }

                    if (!is_null($value['supplierCreditLimit'])) {
                        if (!is_numeric($value['supplierCreditLimit'])) {
                            return ['status' => false, 'msg' => $this->getMessage('ERR_SUP_NON_NUMERIC_CREDIT_LIMIT')];               
                        }
                    } 

                    if (!is_null($value['supplierEmail'])) {
                        if (!filter_var($value['supplierEmail'], FILTER_VALIDATE_EMAIL)) {
                            return ['status' => false, 'msg' => $this->getMessage('ERR_SUP_IMPORT_EMAIL')];               
                        }
                    } 
                }
            }
        }

        if (count($supplierCodeArray) != count(array_unique($supplierCodeArray))) {
            return ['status' => false, 'msg' => $this->getMessage('ERR_SUP_CODE_DUPLICATE_IN_CSV')];
        }

        return ['status' => true];

    }

     /*
    * This function is used to create invoice for customer opening balance
    * @param customerID, customer details
    * @return array
    */
    public function storeInvoiceForSupplierOpeningBalance($supplierID, $supplierDetails)
    {
        $nonInventoryProductDetails = $this->CommonTable("Inventory\Model\ProductTable")->getActiveNonInventoryItems()->current();
        if (!$nonInventoryProductDetails) {
            $res = $this->saveNonInventoryProduct();
            if ($res['status']) {
                $nonInventoryProductDetails = $this->CommonTable("Inventory\Model\ProductTable")->getActiveNonInventoryItems()->current();
            } else {
                return ['status' => false, 'msg'=> $res['msg']];
            }
        }
        
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $result = $this->getReferenceNoForLocation('14', $locationID);
        $stlocationReferenceID = $result['locRefID'];
        $invoiceCode = $this->getReferenceNumber($stlocationReferenceID);

        if ($invoiceCode == "") {
            return ['status' => false, 'msg'=> $this->getMessage('ERR_DELINOTECON_CHANGE_REF_MAX_REACH')];
        }

        $locationProductID = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProduct($nonInventoryProductDetails['productID'], $locationID);
        $invProducts = [];
        $invProducts[0]['locationPID'] = $locationProductID->locationProductID;
        $invProducts[0]['pID'] = $nonInventoryProductDetails['productID'];
        $invProducts[0]['pCode'] = $nonInventoryProductDetails['productCode'];
        $invProducts[0]['pName'] = $nonInventoryProductDetails['productName'];
        $invProducts[0]['pQuantity'] = "1";
        $invProducts[0]['pDiscount'] = "0";
        $invProducts[0]['pUnitPrice'] = $supplierDetails->supplierOutstandingBalance;
        $invProducts[0]['pUom'] = "1";
        $invProducts[0]['pTotal'] = $supplierDetails->supplierOutstandingBalance;
        $invProducts[0]['productType'] = "2";
        $invProducts[0]['stockUpdate'] = "true";
        $invProducts[0]['productindexID'] = "";
        $invProducts[0]['pisubProID'] = "";
        $invProducts[0]['grnFlag'] = "false";
        $invProducts[0]['poFlag'] = "false";
        $invoiceDetails = [
            'sID' => $supplierID,
            'piC' => $invoiceCode,
            'dD' =>  $this->convertDateToStandardFormat(date("Y-m-d")),
            'iD' => $this->convertDateToStandardFormat(date("Y-m-d")),
            'sR' => "",
            'rL' => $locationID,
            'cm' => "Created By Supplier Opening Balance",
            'pr' => $invProducts,
            'dC' => "",
            'fT' => $supplierDetails->supplierOutstandingBalance,
            'sT' => "0",
            'lRID' => $stlocationReferenceID,
            'pT' => "1",
        ];

        $invoice = $this->getServiceLocator()->get("PurchaseInvoiceController");
        $invSaveState = $invoice->storePi($invoiceDetails, $supOpeningBalance = true);
        if($invSaveState['status']){
            return ['status' => true, 'msg'=> $invSaveState['msg']];
        } else {
            return ['status' => false, 'msg'=> $invSaveState['msg']];
        }
    }

    /*
    * This function is used to create non- inventory item for customer opening balance
    */
    public function saveNonInventoryProduct()
    {
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->user_session->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
            }
        }
        $proHandling = array('productHandelingManufactureProduct');
        $proUOM = [];
        $proUOM[1]['uomConversion'] = "1";
        $proUOM[1]['baseUom'] = "true";
        $proUOM[1]['displayUom'] = "true";

        $post = [
            'productCode' => "nonInventoryForSupBalance",
            'productBarcode' => "",
            'productName' => "Non Inventory - Supplier opening balance",
            'productTypeID' => "2",
            'handeling' => $proHandling,
            'productDescription' => "",
            'productDiscountPercentage' => "",
            'productDiscountValue' => "",
            'purchaseDiscountEligible' => false,
            'productPurchaseDiscountPercentage' => "",
            'productPurchaseDiscountValue' => "",
            'productImageURL' => "0",
            'productState' => "1",
            'categoryID' => "1",
            'productGlobalProduct' => true,
            'batchProduct' => false,
            'serialProduct' => false,
            'hsCode' => "",
            'customDutyValue' => "",
            'customDutyPercentage' => "",
            'productDefaultOpeningQuantity' => "",
            'productDefaultSellingPrice' => "",
            'productDefaultMinimumInventoryLevel' => "",
            'productDefaultReorderLevel' => "",
            'productTaxEligible' => false,
            'discountEligible' => false,
            'productDefaultPurchasePrice' => "",
            'rackID' => "",
            'flag' => "product",
            'itemDetail' => "",
            'uomDisplay' => "1",
            'displayUoms' => "1",
            'uom' => $proUOM,
            'productSalesAccountID' => $productSalesAccountID,
            'productInventoryAccountID' => $productInventoryAccountID,
            'productCOGSAccountID' => $productCOGSAccountID,
            'productAdjusmentAccountID' => $productAdjusmentAccountID,
        ];

        $product = $this->getServiceLocator()->get("ProductAPIController");
        $productSaveState = $product->storeProduct($post, $openingBalanceFlag = true);
        if($productSaveState['status']){
            return ['status' => true, 'data' => $productSaveState['data']['productID'], 'msg'=> $productSaveState['msg']];
        } else {
            return ['status' => false, 'data' => $productSaveState['data']['productID'], 'msg'=> $productSaveState['msg']];
        }
    }

    /*
    * This function is used to save advance payment for supplier opening credit
    * @param SupplierID, $supplier Details
    */
    public function storeAdavancePaymentForSupplierOpeningCreditBalance($supplierID, $supplierDetails)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $result = $this->getReferenceNoForLocation('13', $locationID);
        $stlocationReferenceID = $result['locRefID'];
        $advPayCode = $this->getReferenceNumber($stlocationReferenceID);

        $cashPaymentMethodID = "";
        $paymentMethodsGlAccounts = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentMethodsGlAccounts as $key => $value) {
            if ($value['paymentMethodName'] == "Cash") {
                $cashPaymentMethodID = $value['paymentMethodPurchaseFinanceAccountID'];
            }
        }

        if ($cashPaymentMethodID == "") {
            return ['msg' => false, 'msg' => "Gl Account is not set for the payment method cash", 'data' => null];
        }
        $myObj->paymentMethodID = 1;
        $myObj->methodID = 1;
        $myObj->paymentMethodFinanceAccountID = $cashPaymentMethodID;

        $paymentMethods = json_encode($myObj);
        // $paymentMethodArray[] = $paymentMethods;
        $advDetails = [
            'paymentID' => $advPayCode,
            'supplierID' => $supplierID,
            'date' => $this->convertDateToStandardFormat(date("Y-m-d")),
            'amount' => $supplierDetails->supplierCreditBalance ,
            'memo' => "Created By Supplier Opening Credit Balance",
            'locationID' => $locationID,
            'advancePaymentMethodID' => "1",
            'paymentType' => "advance",
            'PaymentMethodDetails' => $paymentMethods,
            'ignoreBudgetLimit' => true,
        ];

        $advancePayment = $this->getServiceLocator()->get("OutGoingPaymentController");
        $advSaveState = $advancePayment->storeAdvancePayment($advDetails ,true);
        if($advSaveState['state']){
            $payMethodNumberDetails = [
                'paymentID' => $advSaveState['id'],
                'PaymentMethodDetails' => $paymentMethods,
                'paymentType' => "advance",
                'amount' => $supplierDetails->supplierCreditBalance ,
            ];

            $paymentMethodNumbers = $this->getServiceLocator()->get("OutGoingPaymentController");
            $res = $paymentMethodNumbers->savePaymentMethodsNumbers($payMethodNumberDetails);
            if ($res['status']) {
                return ['status' => true, 'msg'=> $res['msg']];
            } else {
                return ['status' => false, 'msg'=> $res['msg']];
            }
        } else {
            return ['status' => false, 'msg'=> $advSaveState['msg']];
        }
    }
}

/////////////////// END OF SUPPLIERAPI CONTROLLER \\\\\\\\\\\\\\\\\\\\