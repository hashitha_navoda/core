<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Inventory\Form\SupplierForm;
use Zend\I18n\Translator\Translator;

class PurchaseRequistionController extends CoreController
{

    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'purchaseRequistion_upper_menu';
    protected $_poViewDetails;
    protected $paginator;
    protected $useAccounting;
    
    const PURCHASE_REQUISITION_DOCUMENT_TYPE = 'Purchase Requisition';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion related to Purchase Requisition creation
     */
    public function createAction()
    {
        $param1= $this->params()->fromRoute('param1');
        $param2 = $this->params()->fromRoute('param2');

        return $this->storePo($param1,$param2);
             
    }
    public function editAction()
    {
        $param1 = 'edit';
        $param2 = $this->params()->fromRoute('param1');

        return $this->storePo($param1,$param2);
    }

    public function storePo($param1 = null, $param2 = null) 
    {
        $this->getSideAndUpperMenus('Purchase Requisition', 'Create Purchase Requisition', 'PURCHASING');
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        while ($row = $currencyResultSet->current()) {
            $currencies[$row->currencyID] = $row->currencyName;
        }

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        while ($row = $paymentTermsResultSet->current()) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }

        // get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $supplierTitle = $config['personTitle'];

        $currencyValue = $this->getDefaultCurrency();
        $supplierCategory = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->fetchAllAsArray();

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(34, $locationID);
        $rid = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        $supplierCode = $this->getReferenceNumber($locationReferenceID);

        while ($supplierCode) {
            $supCheck = $this->CommonTable('Inventory\Model\SupplierTable')->getNotDeletedSupplierByCode($supplierCode);
            if (!is_null($supCheck)) {
                $this->updateReferenceNumber($locationReferenceID);
                $supplierCode = $this->getReferenceNumber($locationReferenceID);
            } else {
                break;
            }
        }

        $this->purchaseRequistionType = $this->CommonTable('Settings/Model/PurchaseRequisitionTypeTable')->fetchAll();

        $purchaseRequistionTypes = array();
        foreach ($this->purchaseRequistionType as $pTyp) {
            $purchaseRequistionTypes[$pTyp['purchaseRequisitionTypeId']] = $pTyp['purchaseRequisitionTypeName'];
        }

        $supplierForm = new SupplierForm(
                array(
            'paymentTerms' => $paymentTerms,
            'currencies' => $currencies,
            'supplierTitle' => $supplierTitle,
            'supplierCategory' => $supplierCategory,
            'currencyCountry' => $currencyValue,
        ));

        $supplierForm->get('supplierCode')->setAttribute('value', $supplierCode)->setAttribute('data-supcode', $supplierCode);

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID != ''){
                $supplierPayableAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID;
                $supplierPayableAccountName = $financeAccountsArray[$supplierPayableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPayableAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-id',$supplierPayableAccountID);
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-value',$supplierPayableAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID != ''){
                $supplierPurchaseDiscountAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID;
                $supplierPurchaseDiscountAccountName = $financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-id',$supplierPurchaseDiscountAccountID);
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-value',$supplierPurchaseDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID != ''){
                $supplierGrnClearingAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID;
                $supplierGrnClearingAccountName = $financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsName'];
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-id',$supplierGrnClearingAccountID);
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-value',$supplierGrnClearingAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID != ''){
                $supplierAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID;
                $supplierAdvancePaymentAccountName = $financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsName'];
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-id',$supplierAdvancePaymentAccountID);
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-value',$supplierAdvancePaymentAccountName);
            }
        }

        $supplierAddView = new ViewModel(array('supplierForm' => $supplierForm,'useAccounting'=>$this->useAccounting));
        $supplierAddView->setTemplate('inventory/supplier/supplier-add-modal');
        $grnForm = new GrnForm();
        
        if($param1 && $param2){
            if ($param1 == 'edit') {
                // this Purchase Requisition come from edit
                $grnForm->get('poDraft')->setValue($param2);
                // $grnForm->get('poDraft')->setAttribute('class', 'frommrp');
                $grnForm->get('poDraft')->setAttribute('class', 'fromedit frommrp');
            } else {
                // this condition use to create Purchase Requisition for single records of mrp
                $locationID = $this->user_session->userActiveLocation['locationID'];
                $proDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProduct($param1);
                $grnForm->get('comment')->setAttribute('data-locationid', $locationID);
                $grnForm->get('comment')->setAttribute('class', 'frommrp');
                $grnForm->get('comment')->setAttribute('data-productid', $param1);
                $grnForm->get('comment')->setAttribute('data-proqty', $param2);
                $grnForm->get('comment')->setAttribute('data-proName', $proDetails->productCode .'-'. $proDetails->productName);
            }
        } else if($param1){
            // this condition use to create a Purchase Requisition for draft Purchase Requisitions.
            $grnForm->get('poDraft')->setValue($param1);
            $grnForm->get('poDraft')->setAttribute('class', 'frommrp');
        }
      
        $grnForm->get('discount')->setAttribute('id', 'poDiscount');
        $grnForm->get('grnNo')->setAttribute('id', 'poNo')->setAttribute('name', 'poNo');
        $grnForm->get('grnSaveButton')->setAttribute('id', 'poSave')->setAttribute('name', 'poSaveButton');
        $grnForm->get('grnCancelButton')->setAttribute('id', 'poCancel')->setAttribute('name', 'poCancelButton');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'suppliers',
            'userLocations',
            'taxes'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/po/viewPurchaseRequistion.js');

        $dateFormat = $this->getUserDateFormat();

        $poAddView = new ViewModel(
                array(
            'grnForm' => $grnForm,
            'purchaseRequistionTypes' => $purchaseRequistionTypes,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'dateFormat' => $dateFormat,
                )
        );
        $poAddView->setTemplate('inventory/purchase-requistion/create');
        $poAddView->addChild($supplierAddView, 'supplierAddView');
        $refNotSet = new ViewModel(array(
            'title' => 'Reference Number not set',
            'msg' => $this->getMessage('REQ_OUTPAY_ADD'),
            'controller' => 'company',
            'action' => 'reference'
        ));
        $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
        $poAddView->addChild($refNotSet, 'refNotSet');

        $documenTypes = $this->getDocumentTypes();
        $dtypes = array();
        foreach ($documenTypes as $key => $val) {
            if ($key == 1 || $key == 2 || $key == 3 || $key == 4 || $key == 7 || $key == 9 || $key == 10 || $key == 12 || $key == 15 || $key == 19 || $key == 20 || $key == 22 || $key == 23) {
                $dtypes[$key] = $val;
            }
        }

        $documentReference = new ViewModel(array(
            'title' => 'Document Reference',
            'documentType' => $dtypes,
        ));

        if ($param1 == "edit") {
            $attachmentsView = new ViewModel();
            $attachmentsView->setTemplate('core/modal/attachmentView');
            $poAddView->addChild($attachmentsView, 'attachmentsView');
        }

        $documentReference->setTemplate('/core/modal/document-reference.phtml');
        $poAddView->addChild($documentReference, 'documentReference');

        $this->setLogMessage("Purchase Requisition create page accessed.");
        return $poAddView;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Purchase Requisition', 'View Purchase Requisition', 'PURCHASING');
        $userActiveLocation = $this->user_session->userActiveLocation;
        $poList = $this->getPaginatedPurchaseRequisitions($userActiveLocation['locationID']);
        $this->getViewHelper('HeadScript')->prependFile('/js/view.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/po/viewPurchaseRequistion.js');
        $poSearchView = new ViewModel();
        $poSearchView->setTemplate('inventory/purchase-requistion/search-header');
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/purchase-order/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('inventory/purchase-order/document-view');
        $statusArray = ['cancelled' => 'Cancelled', 'closed' => 'Closed', 'open' => 'Open'];
        $statusModal = new ViewModel(array(
            'statusArray' => $statusArray
        ));
        $statusModal->setTemplate('/core/modal/status');

        $dateFormat = $this->getUserDateFormat();

        $poViewList = new ViewModel(
                array(
            'poList' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat
        ));
        $poViewList->setTemplate('inventory/purchase-requistion/purchase-requistion-list');
        $poView = new ViewModel();
        $poView->addChild($poViewList, 'poList');
        $poView->addChild($statusModal, 'statusChange');
        $poView->addChild($poSearchView, 'poSearchHeader');
        $poView->addChild($docHistoryView, 'docHistoryView');
        $poView->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $poView->addChild($attachmentsView, 'attachmentsView');

        $this->setLogMessage("Purchase Requisition list page accessed.");
        return $poView;
    }

    public function viewAction()
    {

        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $this->getSideAndUpperMenus('Purchase Requisition', 'View Purchase Requisition', 'PURCHASING');
            $grnData = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getPReqByPoID($paramIn);
            $poDetails = array();

            // 9 = document type for Purchase Requisition. from documentType table.
            $ref_docs = $this->getDocumentReferenceBySourceData(39, $paramIn);
            $doc_types = $this->getDocumentTypes();

            $document_reference = [];
            foreach ($ref_docs as $doc_id => $type){
                $document_reference[$doc_types[$doc_id]] = $type;
            }


            while ($row = $grnData->current()) {
                $tempPo = array();
                $poID = $row['purchaseRequisitionID'];
                $statusId = $row['status'];
                $tempPo['poID'] = $row['purchaseRequisitionID'];
                $tempPo['poC'] = $row['purchaseRequisitionCode'];
                $tempPo['poSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempPo['poSR'] = $row['purchaseRequisitionSupplierReference'];
                $tempPo['poRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempPo['poD'] = $this->convertDateToUserFormat($row['purchaseRequisitionEstDelDate']);
                $tempPo['purchaseOrderDate'] = $this->convertDateToUserFormat($row['purchaseRequisitionDate']);
                $tempPo['poDC'] = $row['purchaseRequisitionDeliveryCharge'];
                $tempPo['pReqType'] = $row['purchaseRequisitionTypeName'];
                $tempPo['poT'] = $row['purchaseRequisitionTotal'];
                $tempPo['poDes'] = $row['purchaseRequisitionDescription'];
                $poProducts = (isset($poDetails[$row['purchaseRequisitionID']]['poProducts'])) ? $poDetails[$row['purchaseRequisitionID']]['poProducts'] : array();
                if ($row['purchaseRequisitionProductID'] != NULL) {

                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                    $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['purchaseRequisitionProductPrice'], $productUom);
                    $thisqty = $this->getProductQuantityViaDisplayUom($row['purchaseRequisitionProductQuantity'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                    $productTaxes = (isset($poProducts[$row['productCode']]['pT'])) ? $poProducts[$row['productCode']]['pT'] : array();
                    $poProducts[$row['productCode']] = array('poPC' => $row['productCode'], 'poPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'poPP' => $unitPrice, 'poPD' => $row['purchaseRequisitionProductDiscount'], 'poPQ' => $thisqty['quantity'], 'poPT' => $row['purchaseRequisitionProductTotal'], 'poPUom' => $thisqty['uomAbbr']);

                    if ($row['purchaseRequisitionTaxID'] != NULL) {
                        $productTaxes[$row['purchaseRequisitionTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['purchaseRequisitionTaxPrecentage'], 'pTA' => $row['purchaseRequisitionTaxAmount']);
                    }
                    $poProducts[$row['productCode']]['pT'] = $productTaxes;
                }
                $tempPo['poProducts'] = $poProducts;
                $poDetails[$row['purchaseRequisitionID']] = $tempPo;
            }
            $statusCode = $this->getStatusCode($statusId);
            $poForm = new GrnForm();
            $poForm->get('supplier')->setValue($poDetails[$poID]['poSN']);
            $poForm->get('supplier')->setAttribute('disabled', TRUE);
            $poForm->get('deliveryDate')->setValue($poDetails[$poID]['poD']);
            $poForm->get('deliveryDate')->setAttribute('disabled', TRUE);
            $poForm->get('purchaseOrderDate')->setValue($poDetails[$poID]['purchaseOrderDate']);
            $poForm->get('purchaseOrderDate')->setAttribute('disabled', TRUE);
            $poForm->get('grnNo')->setValue($poDetails[$poID]['poC']);
            $poForm->get('supplierReference')->setValue($poDetails[$poID]['poSR']);
            $poForm->get('supplierReference')->setAttribute('disabled', TRUE);
            $poForm->get('retrieveLocation')->setValue($poDetails[$poID]['poRL']);
            $poForm->get('retrieveLocation')->setAttribute('disabled', TRUE);
            $poForm->get('comment')->setValue($poDetails[$poID]['poDes']);
            $poForm->get('comment')->setAttribute('disabled', TRUE);
            $poForm->get('discount')->setAttribute('id', 'poDiscount');
            $poSubTotal = number_format((floatval($poDetails[$poID]['poT']) - floatval($poDetails[$poID]['poDC'])), 2);
            $totalProTaxes = array();
            foreach ($poDetails[$poID]['poProducts'] as $product) {
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = (floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA']));
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }
            $poSingleView = new ViewModel(
                    array(
                'poData' => $poDetails[$poID],
                'poForm' => $poForm,
                'deliCharge' => $poDetails[$poID]['poDC'],
                'pReqType' => $poDetails[$poID]['pReqType'],
                'poSubTotal' => $poSubTotal,
                'pTotalTax' => $totalProTaxes,
                'poStatus' => $statusCode,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'documentReference' => $document_reference)
            );
            $poSingleView->setTemplate('inventory/purchase-requistion/purchase-requistion-view');

            $this->setLogMessage("View Purchase Requisition ".$tempPo['poC'].".");
            return $poSingleView;
        } else {
            $this->setLogMessage("Redirect to Purchase Requisition list.");
            $this->redirect()->toRoute('purchaseRequistion', array('action' => 'list'));
        }
    }

    public function previewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $data = $this->_getDataForPurchaseRequisitionView($paramIn);
            $data['purchaseOrderCode'] = $data['poC'];
            $data['total'] = $data['poT'];
            $path = "/purchase-requistion/document/"; //.$paramIn;
            $translator = new Translator();
            $createNew = $translator->translate('New Purchase Requisition');
            $createPath = "/purchase-requistion/create";
            $poT = number_format($data['poT'], 2);

            $data["email"] = array(
                "to" => $data['poSE'],
                "subject" => "Purchase Requisition from " . $data['companyName'],
                "body" => <<<EMAILBODY

Dear {$data['poSName']}, <br /><br />

Thank you for your inquiry. <br /><br />

A Purchase Requisition has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>Purchase Requisition Number:</strong> {$data['poC']} <br />
<strong>Purchase Requisition Amount:</strong> {$data['currencySymbol']}{$poT} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
            );

            $this->setLogMessage("Preview Purchase Requisition ".$data['purchaseOrderCode']);
            $documentType = self::PURCHASE_REQUISITION_DOCUMENT_TYPE;
            $commonView = $this->getCommonPreview($data, $path, $createNew, $documentType, $paramIn, $createPath, true);

            return  $commonView;
            
        }
    }

    public function documentPdfAction()
    {
        $poId = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Purchase Requisition';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/inventory/purchase-requistion/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->_getDataForPurchaseRequisitionView($poId);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($poId, $documentType, $documentData, $templateID);

        return;
    }

    public function documentAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = self::PURCHASE_REQUISITION_DOCUMENT_TYPE;
        $poID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $poID, $templateID);
        
        //generate csv file
        $this->generatePurchaseRequisitionCsvFile($poID, $templateID);

        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForPurchaseRequisitionView($poID);
        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);
        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($poID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }
    
    private function generatePurchaseRequisitionCsvFile($poId, $templateId) 
    {
        $documentType = self::PURCHASE_REQUISITION_DOCUMENT_TYPE;
        
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template'); 	
        if (!$templateId) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateId = $defaultTpl['templateID'];
        }
        
        $companyName = $this->getSubdomain();
        $fileName = $this->getDocumentFileName($documentType, $poId, $templateId);
        $arr = str_split($fileName, 2);
        $poFolderPath = "/userfiles/" . md5($companyName) . "/documents/" . strtolower(preg_replace('/([^a-zA-Z0-9])*/', '', $documentType));

        $filePath = ROOT_PATH . '/public' . $poFolderPath . "/" . $arr[0] . "/" . $arr[1];   
        $file = $filePath . '/' . substr($fileName, 4) . '.csv';
        
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777, true);
        }        
        $fp = fopen($file, 'w');        
        $csvData = $this->getPurchaseRequisitionCsvData($poId);

        foreach ($csvData as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
        
        return $file;
    }


    private function getPurchaseRequisitionCsvData($poId) 
    {
        $list = [];
        $poData = $this->_getDataForPurchaseRequisitionView($poId);
        
        array_push($list, ['PURCHASE REQUISITION']);        
        array_push($list, []);        
        array_push($list, [
            $poData['companyName'],
            $poData['companyAddress'],
            'Tel: ' . $poData['telephoneNumber'],
            'Email: ' . $poData['email'],
            'Tax No: ' . $poData['trNumber'],
        ]);
        array_push($list, [
            'Purchase Requisition Code:' . $poData['poC'],
            'Delivery Date:' . $poData['poD'],
            'Purchase Requisition Date' . $poData['purchaseOrderDate'],
            'Retrieve Location:' . $poData['current_location'],
            'Supplier Reference:',
        ]);
        array_push($list, [$poData['poSName']]);
        array_push($list, []);
        array_push($list, [
            'ITEM CODE',
            'ITEM NAME',
            'QUANTITY',
            'UOM',
            'PRICE ('. $poData['companyCurrencySymbol']. ')',
            'DISCOUNT (%)',
            'TAXES ('. $poData['companyCurrencySymbol']. ')',
            'TOTAL ('. $poData['companyCurrencySymbol']. ')'
        ]);

        $tableContentArr = [];
        if (!empty($poData['poProducts'])) {
            foreach ($poData['poProducts'] as $productCode => $product) {
                $tableContent = [
                    $product['poPC'],
                    $product['poPN'],
                    $product['poPQ'],
                    $product['poPUom'],
                    $product['poPP'],
                    $product['poPD'],
                    '',
                    $product['poPT'],
                ];
                array_push($list, $tableContent);                
            }
        } else {
            $tableContentArr = ["no matching records found"];
        }
        array_push($list, $subTotalRow = [
            '',
            '',
            '',
            '',
            '',
            'Sub Total',
            $poData['poSubTotal'],
        ]);        
        array_push($list, [
            '',
            '',
            '',
            '',
            '',
            'Delivery Charges',
            $poData['poDC'],
        ]);
        array_push($list, [
            '',
            '',
            '',
            '',
            '',
            'Total',
            $poData['poT'],
        ]);
        
        return $list;
    }

    private function _getDocumentDataTable($poID, $documentSize = 'A4')
    {

        $data_table_vars = $this->_getDataForPurchaseRequisitionView($poID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/inventory/purchase-requistion/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    private function _getDataForPurchaseRequisitionView($poID)
    {
        if (!empty($this->_poViewDetails)) {
            return $this->_poViewDetails;
        }

        $data = $this->getDataForDocumentView();

        $poData = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getPReqByPoID($poID);
        $poDetails = array();
        foreach ($poData as $row) {
            $tempPo = array();
            $tempPo['poID'] = $row['purchaseRequisitionID'];
            $tempPo['poC'] = $row['purchaseRequisitionCode'];
            $tempPo['poSName'] = $row['supplierTitle'] . ' ' . $row['supplierName'];
            $tempPo['poSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
            $tempPo['poSE'] = $row['supplierEmail'];
            $tempPo['poSA'] = $row['supplierAddress'];
            $tempPo['poSR'] = $row['purchaseRequisitionSupplierReference'];
            $tempPo['poRL'] = $row['locationCode'] . '-' . $row['locationName'];
            $tempPo['poD'] = $this->convertDateToUserFormat($row['purchaseRequisitionEstDelDate']);
            $tempPo['purchaseOrderDate'] = $this->convertDateToUserFormat($row['purchaseRequisitionDate']);
            $tempPo['poDC'] = $row['purchaseRequisitionDeliveryCharge'];
            $tempPo['poT'] = $row['purchaseRequisitionTotal'];
            $tempPo['poSTax'] = $row['purchaseRequisitionShowTax'];
            $tempPo['poDes'] = $row['purchaseRequisitionDescription'];
            $tempPo['poCreatedBy'] = $row['createdUser'];
            $tempPo['createdTimeStamp'] = $this->getUserDateTime($row['createdTimeStamp']);
            $poProducts = (isset($poDetails[$row['purchaseRequisitionID']]['poProducts'])) ? $poDetails[$row['purchaseRequisitionID']]['poProducts'] : array();
            if ($row['purchaseRequisitionProductID'] != NULL) {
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['purchaseRequisitionProductPrice'], $productUom);
                $thisqty = $this->getProductQuantityViaDisplayUom($row['purchaseRequisitionProductQuantity'], $productUom);
                $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                $productTaxes = (isset($poProducts[$row['productCode']]['pT'])) ? $poProducts[$row['productCode']]['pT'] : array();
                $poProducts[$row['productCode']] = array('poPC' => $row['productCode'], 'poPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'poPP' => $unitPrice, 'poPD' => $row['purchaseRequisitionProductDiscount'], 'poPQ' => $thisqty['quantity'], 'poPT' => $row['purchaseRequisitionProductTotal'], 'poPUom' => $thisqty['uomAbbr']);

                if ($row['purchaseRequisitionTaxID'] != NULL) {
                    $productTaxes[$row['purchaseRequisitionTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['purchaseRequisitionTaxPrecentage'], 'pTA' => $row['purchaseRequisitionTaxAmount']);
                }
                $poProducts[$row['productCode']]['pT'] = $productTaxes;
            }
            $tempPo['poProducts'] = $poProducts;
            $poDetails[$row['purchaseRequisitionID']] = $tempPo;
        }
        $poSubTotal = (floatval($poDetails[$poID]['poT']) - floatval($poDetails[$poID]['poDC']));
        $totalProTaxes = array();
        foreach ($poDetails[$poID]['poProducts'] as $product) {
            foreach ($product['pT'] as $tKey => $proTax) {
                if (isset($totalProTaxes[$tKey])) {
                    $totalProTaxes[$tKey]['pTA'] = (floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA']));
                } else {
                    $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                }
            }
        }
        $poDetails[$poID]['poSubTotal'] = $poSubTotal;
        $poDetails[$poID]['poTotalTax'] = $totalProTaxes;
        $data = array_merge($data, $poDetails[$poID]);
        $data['today_date'] = gmdate('Y-m-d');

        $displaySetup = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();
        $data['currencySymbol'] = $displaySetup['currencySymbol'];
        return $this->_poViewDetails = $data;
    }

    private function getPaginatedPurchaseRequisitions($userActiveLocationID = NULL)
    {
        $this->paginator = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getpurchaseRequisitions(true, $userActiveLocationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

}

?>
