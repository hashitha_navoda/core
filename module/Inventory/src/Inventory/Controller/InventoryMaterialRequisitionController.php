<?php 
namespace Inventory\Controller;


use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Inventory\Model\PoDraft;
use Inventory\Model\PurchaseOrder;
use Inventory\Model\PurchaseOrderProduct;
use Inventory\Model\PurchaseOrderProductTax;

class InventoryMaterialRequisitionController extends CoreController 
{
	protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'material_requisition_upper_menu';
    protected $user_session;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
    }
     public function createAction()
    {
        $this->getSideAndUpperMenus('Material Requisition', 'Check Materials', 'PURCHASING');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];

        $allLocations = $this->allLocations;

        foreach ($allLocations as $key => $value) {
            $locIDs[] = $key;
            $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
        }

        $index = new ViewModel(array(
            'debitNoteID' => $debitNoteID,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationNames' => $locNames,
            'referenceNumber' => $rid,
            'paymentTerm' => $payterm,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
     
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/material-requisition.js');
        $this->setLogMessage("Material requisition page accessed.");
        return $index;
    }

    public function loadDataAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->msg = '';
            $this->status = false;
            return $this->JSONRespond();
        }

        $respond = $this->getService('InventoryMaterialRequisitionService')->getDataForRequisition($request->getPost());

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond(); 
                
    }

    public function createPoAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        $dataSet = $request->getPost();
        $defaultNullCustomer = [];
        $withDefaultCUstomer = [];
        foreach (array_filter($dataSet['data']) as $key => $value) {
            if($value['supID'] != 0 ){
                $suplierID = $value['supID'];
                $withDefaultCustomer[$suplierID][] =$value; 
                
            } else {
                $defaultNullCustomer[] = $value;
             
            }
        }

        if(sizeof($withDefaultCustomer) > 0){
            foreach ($withDefaultCustomer as $supID => $proDetails) {
                $createNotNullSupplierDraft = $this->setDataForPoDraft($proDetails, $supID);
                $savePoDraft = $this->savePurchaseOrderDraft($createNotNullSupplierDraft);

            }
        }

        if(sizeof($defaultNullCustomer) > 0){
            $createNullCustomerDraft = $this->setDataForPoDraft($defaultNullCustomer);
            $savePoDraft = $this->savePurchaseOrderDraft($createNullCustomerDraft);
        }
         $this->status = true;
         $this->data = null;
         return $this->JSONRespond();
    }

    public function setDataForPoDraft($dataSet, $supplierID = null)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $productSet = [];
        $totalValue = 0;
        foreach ($dataSet as $key => $proDetails) {
            //get locationProductDetails 
            $productDetails = $this->CommonTable('Inventory\Model\LocationProductTable')
                    ->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($proDetails['proID'], $locationID);
            $productData = iterator_to_array($productDetails);
            
            $totalTaxValue = $this->
                calculateProductUnitTax($productData[0]['defaultPurchasePrice'], $productData[0]['productID']);
            
            $productSet[$productData[0]['productID']] = array(
                'locationProductID' => $productData[0]['locationProductID'],
                'purchaseOrderProductQuantity' => $proDetails['reqQty'],
                'purchaseOrderProductPrice' => $productData[0]['defaultPurchasePrice'],
                'purchaseOrderProductTotal' => $proDetails['reqQty']*($productData[0]['defaultPurchasePrice'] + $totalTaxValue),
                );
            $totalValue += $proDetails['reqQty']*($productData[0]['defaultPurchasePrice'] + $totalTaxValue);
            if($productData[0]['productTaxEligible'] == 1){
                $productTaxDetails = $this->CommonTable('Inventory\Model\ProductTaxTable')
                        ->getProTaxDetailsByProductID($productData[0]['productID']);
                foreach ($productTaxDetails as $key => $taxDetails) {
                    $singleTaxValue = $this->caluculateProductSingleTax($productData[0]['defaultPurchasePrice'], $taxDetails);
                    
                    $productSet[$productData[0]['productID']]['tax'][] = array(
                        'purchaseOrderTaxID' => $taxDetails['taxID'],
                        'purchaseOrderTaxPrecentage' => $taxDetails['taxPrecentage'],
                        'purchaseOrderTaxAmount' => $singleTaxValue*$proDetails['reqQty'],
                    );
                        
                }
            }
        }

        $refData = $this->getReferenceNoForLocation(32, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $entityID = $this->createEntity();
        $purchaseDetails = array(
            'purchaseOrderCode' => $rid,
            'purchaseOrderRetrieveLocation' => $locationID,
            'purchaseOrderTotal' => $totalValue,
            'entityID' => $entityID,
            'productDetails' => $productSet,
        );
        if($supplierID != ''){
            $purchaseDetails['purchaseOrderSupplierID'] = $supplierID;
        }
        $this->updateReferenceNumber($lrefID);

        return $purchaseDetails;
                    
    }


    public function savePurchaseOrderDraft($dataSet)
    {
        $date = date("Y-m-d");
        $poData = array(
            'purchaseOrderCode' => $dataSet['purchaseOrderCode'],
            'purchaseOrderSupplierID' => $dataSet['purchaseOrderSupplierID'],
            'purchaseOrderRetrieveLocation' => $dataSet['purchaseOrderRetrieveLocation'],
            'purchaseOrderTotal' => $dataSet['purchaseOrderTotal'],
            'entityID' => $dataSet['entityID'],
            'status' => 7,
            'purchaseOrderDraftFlag' => 1,
            'purchaseOrderExpDelDate' => $date,
        );
        
        $poM = new PurchaseOrder();
        $poM->exchangeArray($poData);
        $poID = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->savePo($poM);
        // $this->updateReferenceNumber($locationReferenceID);
        
        $poProductM = new PurchaseOrderProduct();
        foreach ($dataSet['productDetails'] as $key => $product) {
            $poProductData = array(
                'purchaseOrderID' => $poID,
                'locationProductID' => $product['locationProductID'],
                'purchaseOrderProductQuantity' => $product['purchaseOrderProductQuantity'],
                'purchaseOrderProductPrice' => $product['purchaseOrderProductPrice'],
                'purchaseOrderProductTotal' => $product['purchaseOrderProductTotal'],
            );
            $poProductM->exchangeArray($poProductData);
            $insertedPoProductID = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->savePurchaseOrderProduct($poProductM);
        
            if(sizeof($product['tax']) > 0){
                foreach ($product['tax'] as $key => $taxValue) {
                    $poPTaxesData = array(
                            'purchaseOrderID' => $poID,
                            'purchaseOrderProductID' => $insertedPoProductID,
                            'purchaseOrderTaxID' => $taxValue['purchaseOrderTaxID'],
                            'purchaseOrderTaxPrecentage' => $taxValue['purchaseOrderTaxPrecentage'],
                            'purchaseOrderTaxAmount' => $taxValue['purchaseOrderTaxAmount']
                        );
                        $poPTaxM = new PurchaseOrderProductTax();
                        $poPTaxM->exchangeArray($poPTaxesData);
                        $this->CommonTable('Inventory\Model\PurchaseOrderProductTaxTable')->savePurchaseOrderProductTax($poPTaxM);  
                }
            }
         
        }
                    
        
    }

}