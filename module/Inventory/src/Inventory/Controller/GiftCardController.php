<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Gift Card controller functions
 */

namespace Inventory\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Inventory\Form\GiftCardForm;

class GiftCardController extends CoreController
{

    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'product_upper_menu';
    protected $useAccounting;

     public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Item', 'Add Gift Card', 'INVENTORY');

        $location = $this->user_session->userActiveLocation['locationName'];
        $data = array(
            'location' => $location,
        );

        $form = new GiftCardForm($data);

        $paymentMethodData = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethodById(6);
        $accountID = 0;
        $accountName = '';
        if($paymentMethodData['paymentMethodInSales'] == 1){
            $accountName = $paymentMethodData['salesFinanceAccountsCode'].'_'.$paymentMethodData['salesFinanceAccountsName'];
            $accountID = $paymentMethodData['paymentMethodSalesFinanceAccountID'];
        }
        $form->get('giftCardGlAccountID')->setAttribute('data-id', $accountID);
        $form->get('giftCardGlAccountID')->setAttribute('data-value', $accountName);
        
        $view = new ViewModel(array(
            'form' => $form,
            'useAccounting' => $this->useAccounting,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/giftCard.js');

        return $view;
    }

    //list action of the gift card
    public function listAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $this->getSideAndUpperMenus('Item', 'View Gift Card', 'INVENTORY');

        $this->getPaginatedGiftCard($locationID);

        $giftCardListView = new ViewModel(array(
            'cSymbol' => $this->companyCurrencySymbol,
            'giftCard' => $this->paginator,
            'paginated' => true
        ));

        $giftCardListView->setTemplate('inventory/gift-card/gift-card-list');
        $mainViewModel = new ViewModel();
        $mainViewModel->addChild($giftCardListView, 'giftCardList');

        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/giftCard.js');

        $this->setLogMessage("Add product view accessed");
        return $mainViewModel;
    }

    public function getPaginatedGiftCard($locationID)
    {
        $this->paginator = $this->CommonTable('Inventory/Model/ProductTable')->giftCardFetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    //view action of gift card
    public function viewAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->getSideAndUpperMenus('Item', 'View Gift Card', 'INVENTORY');

        $giftCardId = $this->params()->fromRoute('param1');

        $location = $this->user_session->userActiveLocation['locationName'];
        $data = array(
            'location' => $location,
        );

        $giftCards = (object) $this->CommonTable('Inventory/Model/ProductTable')->getProductByProductID($giftCardId);
        $locationProductID = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($giftCardId, $locationID)->locationProductID;
        $serialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductsByLocProductId($locationProductID);
        if ($serialData != 0) {
            foreach ($serialData as $sdata) {
                $giftCardStatus = $this->CommonTable('Inventory\Model\GiftCardTable')->getGiftCardDetailsByGiftCardCode($sdata->productSerialCode);
                if ($giftCardStatus->giftCardStatus != "5") {
                    $giftCardSerial[$sdata->productSerialID] = $sdata->productSerialCode;
                }
                if (!isset($duration)) {
                    $giftCard = $this->CommonTable('Inventory\Model\GiftCardTable')->getGiftCardDetailsByGiftCardCode($sdata->productSerialCode);
                    $duration = $giftCard->giftCardDuration;
                }
            }
        }

        $form = new GiftCardForm($data);

        $form->get('giftCardId')->setValue($giftCards->productID)->setAttribute('disabled', true);
        $form->get('giftCardName')->setValue($giftCards->productName)->setAttribute('disabled', true);
        $form->get('giftCardPrice')->setValue(number_format($giftCards->productDefaultSellingPrice,2,'.',''))->setAttribute('disabled', true);
        $form->get('giftCardDurationDays')->setValue($duration)->setAttribute('disabled', true);
        $form->get('giftCardDiscountPrecentage')->setValue($giftCards->productDiscountPercentage)->setAttribute('disabled', true);

        $view = new ViewModel(array(
            'form' => $form,
            'giftCardSerial' => $giftCardSerial,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/giftCard.js');

        return $view;
    }

}
