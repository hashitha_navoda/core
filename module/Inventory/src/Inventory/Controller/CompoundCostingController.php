<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Inventory\Form\ProductHandelingForm;
use Inventory\Form\BomForm;
use Zend\View\Model\ViewModel;
use Inventory\Form\CategoryForm;

class CompoundCostingController extends CoreController
{

    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'compound_costing_upper_menu';

    function createAction()
    {
        $this->getSideAndUpperMenus('Landing Cost', 'Costing', 'INVENTORY');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locations = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
        while ($r = $locations->current()) {
            $r = (object) $r;
            $location[$r->locationID] = $r->locationName;
        }
        // cost calculate types
        $costCalculateType = [
            1 => 'Quantity Base',
            2 => 'Total Amount Base'
        ];
        
        $compoundCostingView = new ViewModel(array(
            'location' => $location,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'defaultUom' => $defaultUomID,
            'uomList' => $uoms,
            'currentLocationID' => $locationID,
            'costCalculateType' => $costCalculateType
        ));
        
        $costMappingView = new ViewModel(array());
        $costMappingView->setTemplate('inventory/compound-costing/map-costing');


        $compoundCostingView->addChild($costMappingView, 'costMappingView');
        $compoundCostingView->setTemplate('inventory/compound-costing/create');

        $this->getViewHelper('HeadScript')->prependFile('/js/compound-cost.js');        
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        return $compoundCostingView;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Landing Cost', 'List', 'INVENTORY');
        $userActiveLocation = $this->user_session->userActiveLocation;
        $compoundCostList = $this->getPaginatedCompoundCostDetails($userActiveLocation['locationID']);
        $this->getViewHelper('HeadScript')->prependFile('/js/view.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/compound-cost-view.js');
        
        $dateFormat = $this->getUserDateFormat();
        $compoundCostViewList = new ViewModel(
                array(
            'landedCostList' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat
        ));
        return $compoundCostViewList;
    }

    private function getPaginatedCompoundCostDetails($userActiveLocationID = NULL)
    {
        $this->paginator = $this->CommonTable('Inventory\Model\CompoundCostTemplateTable')->getAllCompundCostDetails(true, $userActiveLocationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

}
