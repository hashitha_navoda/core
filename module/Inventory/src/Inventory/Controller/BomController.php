<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Inventory\Form\ProductHandelingForm;
use Inventory\Form\BomForm;
use Zend\View\Model\ViewModel;
use Inventory\Form\CategoryForm;

class BomController extends CoreController
{

    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'bill_of_material_upper_menu';

    function createAction()
    {
        $this->getSideAndUpperMenus('Bill of Material', 'BOM Create', 'INVENTORY');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locations = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
        while ($r = $locations->current()) {
            $r = (object) $r;
            $location[$r->locationID] = $r->locationName;
        }
        $BomForm = new BomForm(array(
            'location' => $location,
        ));

        $productHandelingForm = new ProductHandelingForm();
        $productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", true);
        $defaultUomID = $this->CommonTable('Settings\Model\UomTable')->getUomByName('Unit')->uomID;
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        $bomView = new ViewModel(array(
            'bomForm' => $BomForm,
            'productHandelingForm' => $productHandelingForm,
            'location' => $location,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'defaultUom' => $defaultUomID,
            'uomList' => $uoms,
            'currentLocationID' => $locationID,
        ));
        
        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $categoryForm = new CategoryForm($allCategoryList);
        $productCatSubView = new ViewModel(array(
            'categoryForm' => $categoryForm
        ));

        $productCatSubView->setTemplate('/inventory/product/add-category');
        $bomView->addChild($productCatSubView, 'categoryFormView');
        $bomView->categoryForm = $categoryForm;
        
        $bomView->setTemplate('/inventory/bom/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/bom.js');        
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        $this->setLogMessage("Add product view accessed");
        return $bomView;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Bill of Material', 'BOM List', 'INVENTORY');
        $userDateFormat = $this->getUserDateFormat();
        $bomList = $this->getPaginatedBom();
        $paginatedflag = true;

        $bomListView = new ViewModel(array(
            'bomList' => $bomList,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/bom.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');
        return $bomListView;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Bill of Material', 'BOM List', 'INVENTORY');
        $bomID = $this->params()->fromRoute('param1');
        $bomDetails = $this->CommonTable("Inventory\Model\BomTable")->getBomDataByBomID($bomID)->current();
        
        $product = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($bomDetails['productID']);
        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $bomDetails['productCategory'] = $allCategoryList[$product['categoryID']];
                
        $bomSubItemDetails = $this->CommonTable("Inventory\Model\BomSubItemTable")->getBomSubDataByBomID($bomID);
        foreach ($bomSubItemDetails as $value) {
            $uomDetails = $this->CommonTable('Inventory\Model\ProductUomTable')->getDisplayProductUomByProductID($value['productID'])->current();
            $value['bomSubItemUnitPrice'] = ($value['bomSubItemUnitPrice']) * ($uomDetails['productUomConversion']);
            $value['bomSubItemQuantity'] = $value['bomSubItemQuantity'] / $uomDetails['productUomConversion'];
            $value['uomAbbr'] = $uomDetails['uomAbbr'];
            $dataArray[] = $value;
        }
        $bomView = new ViewModel(array(
            'bomDetails' => $bomDetails,
            'bomSubItem' => $dataArray,
        ));
        
        $this->getViewHelper('HeadScript')->prependFile('/js/bom.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');
        return $bomView;
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Bill of Material', 'BOM List', 'INVENTORY');
        $bomID = $this->params()->fromRoute('param1');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $bomDetails = $this->CommonTable("Inventory\Model\BomTable")->getBomDataByBomID($bomID)->current();
        $bomSubItemDetails = $this->CommonTable("Inventory\Model\BomSubItemTable")->getBomSubDataByBomID($bomID);
        foreach ($bomSubItemDetails as $value) {
            $value['bomSubItemQuantity'] = $value['bomSubItemQuantity'] / $value['productUomConversion'];
            $value['bomSubItemUnitPrice'] = $value['bomSubItemUnitPrice'] * $value['productUomConversion'];
            $dataArray[] = $value;
        }
        $locations = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
        while ($r = $locations->current()) {
            $r = (object) $r;
            $location[$r->locationID] = $r->locationName;
        }
        $bomForm = new BomForm(array(
            'location' => $location,
        ));
        $bomForm->get('BomRef')->setValue($bomDetails['bomCode'])->setAttribute('disabled', true);
        $bomForm->get('BOMName')->setValue($bomDetails['productName']);
        $bomForm->get('productDescription')->setValue($bomDetails['bomDiscription']);
        $bomForm->get('BOMID')->setValue($bomDetails['bomID']);
        $bomEdit = new ViewModel(array(
            'bomSubItem' => $dataArray,
            'bomForm' => $bomForm,
            'location' => $location,
            'bomID' => $bomDetails['bomID'],
            'productID' => $bomDetails['productID'],
            'currentLocationID' => $locationID,
        ));
        
        //get product details
        $product = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($bomDetails['productID']);
        
        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $categoryForm = new CategoryForm($allCategoryList);
        $categoryForm->get('categoryParentID')->setValue($product['categoryID']);
        $productCatSubView = new ViewModel(array(
            'categoryForm' => $categoryForm
        ));

        $productCatSubView->setTemplate('/inventory/product/add-category');
        $bomEdit->addChild($productCatSubView, 'categoryFormView');
        $bomEdit->categoryForm = $categoryForm;

        $this->getViewHelper('HeadScript')->prependFile('/js/bom.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');
        return $bomEdit;
    }

    /**
     * bom item Assembly and Dissembly action
     * @return ViewModel
     */
    public function adAction()
    {
        $this->getSideAndUpperMenus('Bill of Material', 'BOM A/D', 'INVENTORY');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $bomAd = new ViewModel(array(
            'currentLocation' => $locationID,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/bom-ad.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');
        return $bomAd;
    }

    public function adListAction()
    {
        $this->getSideAndUpperMenus('Bill of Material', 'BOM A/D LIST', 'INVENTORY');
        $userDateFormat = $this->getUserDateFormat();
        $bomADList = $this->getPaginatedBomAD();
        $paginatedflag = true;

        $bomADListView = new ViewModel(array(
            'bomADList' => $bomADList,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/bom.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');
        return $bomADListView;
    }

    public function getPaginatedBom()
    {
        $this->bomList = $this->CommonTable('Inventory\Model\BomTable')->fetchAll(true);
        $this->bomList->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->bomList->setItemCountPerPage(10);
        return $this->bomList;
    }

    public function getPaginatedBomAD()
    {
        $this->bomADList = $this->CommonTable('Inventory\Model\BomAssemblyDisassemblyTable')->fetchAll(true);
        $this->bomADList->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->bomADList->setItemCountPerPage(10);
        return $this->bomADList;
    }

}
