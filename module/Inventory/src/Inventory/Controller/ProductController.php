<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Inventory Product related controller functions
 */

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Inventory\Form\ProductForm;
use Inventory\Form\ProductdataimportForm;
use Inventory\Form\ProductHandelingForm;
use Inventory\Form\ProductCategoryForm;
use Inventory\Form\UomForm;
use Inventory\Form\BomForm;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Inventory\Form\CategoryForm;
use Inventory\Form\ProductImportForm;
use Inventory\Model\Product;
use Inventory\Model\Category;
use Inventory\Model\LocationProduct;
use Inventory\Model\ProductUom;
use Inventory\Model\ProductHandeling;
use Inventory\Exception\ProductUploadException;
use Zend\I18n\Translator\Translator;
use Inventory\Model\GoodsIssue;
use Inventory\Model\GoodsIssueProduct;
use Inventory\Model\ItemIn;
use Accounting\Model\FinanceAccounts;
use Inventory\Model\ItemBarcode;
use Inventory\Form\GiftCardForm;

class ProductController extends CoreController
{

    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'product_upper_menu';
    protected $useAccounting;

    const ITEM_IMPORT_DIR = "/tmp/";
    const ITEM_IMPORT_FILE_PREFIX = "ez-biz-item-import-";
    const ITEM_IMPORT_GOODS_ISSUE_REASON = "Imported Item Quantity";
    const ITEM_IMPORT_GOODS_ISSUE_TYPE_ID = 2;
    const ITEM_IMPORT_LOCATION_REFERENCE_ID = 28;
    const ITEM_IMPORT_DOCUMENT_TYPE = 'Inventory Positive Adjustment';
    const ITEM_IMPORT_UOM_CONVERSION = 1.0;
    const ITEM_IMPORT_UOM_DISPLAY = 1;
    const ITEM_IMPORT_UOM_BASE = 1;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'inventory_side_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {

        $this->getSideAndUpperMenus('Item', 'Add Item', 'INVENTORY');
        $companyFolder = $this->getCompanyImageFolderName();

        $sup = array();
        $tax = array();
        $ctax = array();
        $location = array();
        $productType = array();

        $availability = false;
        while (!$availability) {
            $refData = $this->getReferenceNoForLocation(33, 1);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];
            $availability = $this->CommonTable('Inventory/Model/ProductTable')->checkAvailabilityOfProductCode($rid);
            if($availability) {
                break;
            } else {
                $this->updateReferenceNumber($lrefID);
            }
        }

        $tax_resource = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
        while ($t = $tax_resource->current()) {
            $tax[$t->id] = $t->taxName;
            if ($t->taxType == 'c') {
                $ctax[$t->id] = $t->taxName;
            }
        }

        $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
        foreach ($supplier as $key => $singleSupp) {
            $sup[$singleSupp['supplierID']] = $singleSupp['supplierName'];

        }

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        while ($pType = $proType->current()) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $locations = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
        while ($r = $locations->current()) {
            $r = (object) $r;
            $location[$r->locationID] = $r->locationName;
        }

        $tr_number = $this->CommonTable('CompanyTable')->getTRNumber();
        $taxStatus = isset($tr_number->trNumber) ? TRUE : FALSE;

        $productForm = new ProductForm(array(
            'supplier' => $sup,
            'tax' => $tax,
            'productType' => $productType,
            'location' => $location,
            'referenceNo' => $rid
        ));

        $financeAccountsArray = $this->getFinanceAccounts();

        $productForm->get('submit')->setValue('Save');
        $productForm->get('productState')->setValue('Active');

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $productSalesAccountName = $financeAccountsArray[$productSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productSalesAccountID]['financeAccountsName'];
                $productForm->get('productSalesAccountID')->setAttribute('data-id',$productSalesAccountID);
                $productForm->get('productSalesAccountID')->setAttribute('data-value',$productSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $productInventoryAccountName = $financeAccountsArray[$productInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productInventoryAccountID]['financeAccountsName'];
                $productForm->get('productInventoryAccountID')->setAttribute('data-id',$productInventoryAccountID);
                $productForm->get('productInventoryAccountID')->setAttribute('data-value',$productInventoryAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $productCOGSAccountName = $financeAccountsArray[$productCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productCOGSAccountID]['financeAccountsName'];
                $productForm->get('productCOGSAccountID')->setAttribute('data-id',$productCOGSAccountID);
                $productForm->get('productCOGSAccountID')->setAttribute('data-value',$productCOGSAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $productAdjusmentAccountName = $financeAccountsArray[$productAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productAdjusmentAccountID]['financeAccountsName'];
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-id',$productAdjusmentAccountID);
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-value',$productAdjusmentAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultDepreciationAccountID != ''){
                $productDepreciationAccountID = $glAccountSetupData->glAccountSetupItemDefaultDepreciationAccountID;
                $productDepereciationAccountName = $financeAccountsArray[$productDepreciationAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productDepreciationAccountID]['financeAccountsName'];
                $productForm->get('productDeperciationAccountID')->setAttribute('data-id',$productDepreciationAccountID);
                $productForm->get('productDeperciationAccountID')->setAttribute('data-value',$productDepereciationAccountName);
            }
        }

        $productHandelingForm = new ProductHandelingForm();
        $productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", true);
        $defaultUomID = $this->CommonTable('Settings\Model\UomTable')->getUomByName('Unit')->uomID;
        $index = new ViewModel(array(
            'productForm' => $productForm,
            'productHandelingForm' => $productHandelingForm,
            'tax' => $tax,
            'ctax' => $ctax,
            'cdn_url' => $this->cdnUrl,
            'location' => $location,
            'companyFolder' => $companyFolder,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'defaultUom' => $defaultUomID,
            'taxStatus' => $taxStatus,
            'useAccounting' => $this->useAccounting,
        ));

        // for category add modal window
        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $defaultCategoryID = array_search('General', $allCategoryList);
        $categoryForm = new CategoryForm($allCategoryList);
        $categoryForm->get('categoryParentID')->setAttribute('data-id', $defaultCategoryID);
        $productCatSubView = new ViewModel(array(
            'categoryForm' => $categoryForm
        ));

        $productCatSubView->setTemplate('/inventory/product/add-category');
        $index->addChild($productCatSubView, 'categoryFormView');

        $index->categoryForm = $categoryForm;

        $expireAlertsView = new ViewModel();
        $expireAlertsView->setTemplate('/inventory/product/expiry-alerts');
        $index->addChild($expireAlertsView, 'expiryAlerts');

        //load item attribute details to the product creation screen
        $attrDetails = [];
        $itemAttrDataSet = $this->CommonTable('/settings/Model/itemAttributeValueTable')->selectAll();
        foreach ($itemAttrDataSet as $key => $value) {
            $attrDetails[$value['itemAttributeID']] = array(
                'itemAttributeID' => $value['itemAttributeID'],
                'description' => $value['itemAttributeCode']." - ".$value['itemAttributeName'],
                );
        }
        $itemAttributeAddView = new ViewModel(
            array(
                'itemAttrDetails' => $attrDetails,
            ));
        $itemAttributeAddView->setTemplate('/inventory/product/add-item-attribute');
        $index->addChild($itemAttributeAddView, 'itemAttributeAddView');

        $barCodeAddModal = new ViewModel();
        $barCodeAddModal->setTemplate('/inventory/product/add-bar-codes-modal');
        $index->addChild($barCodeAddModal, 'barCodeAddModal');

        $itemAttributeAddValueView = new ViewModel();
        $itemAttributeAddValueView->setTemplate('/inventory/product/add-item-attr-value');
        $index->addChild($itemAttributeAddValueView, 'itemAttributeAddValueView');


        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        $index->uomList = $uoms;

        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/product.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/script.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories',
            'uomByID'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/category.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jquery-ui.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.min.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        $this->setLogMessage("Add product view accessed");

        return $index;
    }

    public function editAction()
    {
        $productDetailsView = $this->indexAction();
        $productDetailsView->setTemplate('inventory/product/index');
        $productDetailsView->editMode = true;
        $productDetailsView->productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", false);

        $productID = $this->params()->fromRoute('param1');
        $productsArr = $this->CommonTable('Inventory/Model/ProductTable')->getProductByProductID($productID);

        if ($productsArr['isUseDiscountScheme'] == 1) {
            $productsArr['discountSchemeEligible'] = 1;
        }
        $productUomDisplayArr = $this->CommonTable('Inventory\Model\ProductUomTable')->getDisplayProductUomByProductID($productID)->current();
        $productsArr['categoryParentID'] = $productsArr['categoryID'];
        $productsArr['productDefaultSellingPrice'] = number_format(($productsArr['productDefaultSellingPrice'] * $productUomDisplayArr['productUomConversion']), 2, '.', '');
        $productsArr['productDefaultPurchasePrice'] = number_format(($productsArr['productDefaultPurchasePrice'] * $productUomDisplayArr['productUomConversion']), 2, '.', '');
        if (!is_null($productsArr['productPurchaseDiscountValue'])) {
            $productsArr['productPurchaseDiscountValue'] = number_format($productsArr['productPurchaseDiscountValue'], 2, '.', '');
        }
        if (!is_null($productsArr['productDiscountValue'])) {
            $productsArr['productDiscountValue'] = number_format($productsArr['productDiscountValue'], 2, '.', '');
        }
        $productsArr['productDefaultMinimumInventoryLevel'] = number_format($productsArr['productDefaultMinimumInventoryLevel'], 2);
        $productsArr['productDefaultMaximunInventoryLevel'] = number_format($productsArr['productDefaultMaximunInventoryLevel'], 2);
        $productsArr['productDefaultReorderLevel'] = number_format($productsArr['productDefaultReorderLevel'], 2);

        // get uoms for product
        $productUomArr = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomByProductID($productID);

        $productDetailsView->productUoms = $productUomArr;
        // get suppliers for product
        $productSuppliersArr = $this->CommonTable('Inventory\Model\ProductSupplierTable')->getProductSupplierByProductID($productID);
        $productDetailsView->productSuppliers = $productSuppliersArr;

        // get taxes for product
        $productTaxArr = $this->CommonTable('Inventory\Model\ProductTaxTable')->getProductTaxByProductID($productID);
        $productTaxes = array();
        foreach ($productTaxArr as $pTax) {
            $productTaxes['tax' . $pTax->taxID] = $pTax->taxID;
        }

        // get locations for product
        $productLocationsArr = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationsByProductID($productID);
        $productLocations = array();
        foreach ($productLocationsArr as $pLocation) {
            $productLocations['location' . $pLocation['locationID']] = $pLocation['locationID'];
        }

        $allProductDetails = $productsArr + $productLocations + $productTaxes;
        // hide 0 values
        $allProductDetails['productID'] = $productID;
        $allProductDetails['customDutyPercentage'] = ($allProductDetails['customDutyPercentage'] == 0) ? '' : $allProductDetails['customDutyPercentage'] + 0;
        $allProductDetails['customDutyValue'] = ($allProductDetails['customDutyValue'] == 0) ? '' : $allProductDetails['customDutyValue'] + 0;

        $financeAccountsArray = $this->getFinanceAccounts();

        $productDetailsView->imageUrl = $allProductDetails['productImageURL'];
        $productForm = $productDetailsView->productForm;
        $productForm->populate($allProductDetails);
        $productForm->get('batchProduct')->setAttribute('disabled', true);
        $productForm->get('serialProduct')->setAttribute('disabled', true);
        $productForm->get('rackId')->setValue($productsArr['rackID']);
        $productForm->get('itemDetail')->setValue($productsArr['itemDetail']);

        if($this->useAccounting == 1){
            $productSalesAccountID = $productsArr['productSalesAccountID'];
            $productSalesAccountName = $financeAccountsArray[$productSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productSalesAccountID]['financeAccountsName'];
            $productForm->get('productSalesAccountID')->setAttribute('data-id',$productSalesAccountID);
            $productForm->get('productSalesAccountID')->setAttribute('data-value',$productSalesAccountName);

            $productInventoryAccountID = $productsArr['productInventoryAccountID'];
            $productInventoryAccountName = $financeAccountsArray[$productInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productInventoryAccountID]['financeAccountsName'];
            $productForm->get('productInventoryAccountID')->setAttribute('data-id',$productInventoryAccountID);
            $productForm->get('productInventoryAccountID')->setAttribute('data-value',$productInventoryAccountName);

            $productCOGSAccountID = $productsArr['productCOGSAccountID'];
            $productCOGSAccountName = $financeAccountsArray[$productCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productCOGSAccountID]['financeAccountsName'];
            $productForm->get('productCOGSAccountID')->setAttribute('data-id',$productCOGSAccountID);
            $productForm->get('productCOGSAccountID')->setAttribute('data-value',$productCOGSAccountName);

            $productAdjusmentAccountID = $productsArr['productAdjusmentAccountID'];
            $productAdjusmentAccountName = $financeAccountsArray[$productAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productAdjusmentAccountID]['financeAccountsName'];
            $productForm->get('productAdjusmentAccountID')->setAttribute('data-id',$productAdjusmentAccountID);
            $productForm->get('productAdjusmentAccountID')->setAttribute('data-value',$productAdjusmentAccountName);
        }

        if (!empty($productsArr['discountSchemID'])) {
            $schemeData = $this->CommonTable('DiscountSchemeTable')->getSchemeBySchemeID($productsArr['discountSchemID']);

            $discountSchemeName = $schemeData['discountSchemeName'].'_'.$schemeData['discountSchemeCode'];

            $productForm->get('discountSchemeID')->setAttribute('data-id',$productsArr['discountSchemID']);
            $productForm->get('discountSchemeID')->setAttribute('data-value',$discountSchemeName);
        }

        $productDetailsView->productDetails = $allProductDetails;

        $categoryForm = $productDetailsView->categoryForm;
        $categoryForm->populate($productsArr);

        $productHandelingForm = $productDetailsView->productHandelingForm;
        $productHandelingForm->populate($productsArr);

        $index = new ViewModel();

        $index->addChild($productDetailsView, 'productDetails');

        $productLocationsView = $this->locationProductDetailsAction();
        $productLocationsView->setTemplate('inventory/product/location-product-details');
        $productLocationsView->editMode = true;

        $index->addChild($productLocationsView, 'productLocations');

        $this->getSideAndUpperMenus('Item', 'View Items', 'INVENTORY');

        return $index;
    }

    public function locationProductDetailsAction()
    {

        $this->getSideAndUpperMenus('Item', 'Add Item', 'INVENTORY');

        $productID = $this->params()->fromRoute('param1');

        $product = $this->CommonTable('Inventory/Model/ProductTable')->getProductByProductID($productID);

        $locations = array();
        $locationArr = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationsByProductID($productID);
        foreach ($locationArr as $l) {
            $grnProduct = $this->CommonTable('Inventory\Model\GrnProductTable')->CheckGrnProductByLocationProductID($l['locationProductID']);
            $pvProduct = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->CheckPurchaseInvoiceProductByLocationProductID($l['locationProductID']);
            $adjuesmentProduct = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->CheckGoodsIssueProductByLocationProductID($l['locationProductID']);
            if ($adjuesmentProduct->goodsIssueReason == "Adding opening Quantity") {
                $l['openQty'] = $adjuesmentProduct->goodsIssueProductQuantity;
            }
            $itemInEntry = $this->CommonTable('Inventory\Model\ItemInTable')->checkProductByLocationProductID($l['locationProductID']);

            //product was checked in grn, pv and adjustment - adding condition to check in itemIn table
            if (isset($grnProduct->grnProductID) || isset($pvProduct->purchaseInvoiceProductID) || isset($adjuesmentProduct->goodsIssueProductID) || $itemInEntry || $product['batchProduct'] || $product['serialProduct'] || ($product['productTypeID'] == 2)) {
                $l['isLPQDisabled'] = TRUE;
            } else {
                $l['isLPQDisabled'] = FALSE;
            }
            $locations[] = $l;
        }

        $productUoms = $this->CommonTable('Inventory\Model\ProductUomTable')->getAsendingProductUomByProductID($productID);
        $productUomArray = array();
        foreach ($productUoms as $productuom) {
            $tempArray = array();
            $tempArray['uomID'] = $productuom['uomID'];
            $tempArray['uA'] = $productuom['uomAbbr'];
            $tempArray['uN'] = $productuom['uomName'];
            $tempArray['uC'] = $productuom['productUomConversion'];
            $tempArray['uS'] = "1";
            $tempArray['pUDisplay'] = $productuom['productUomDisplay'];
            $tempArray['uDP'] = $productuom['uomDecimalPlace'];
            $productUomArray[$productuom['uomID']] = $tempArray;
        }
        $locationView = new ViewModel(array(
            'locations' => $locations,
            'productName' => $product['productName'],
            'productID' => $productID,
            'product' => $product,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'productDefaultOpeningQuantity' => $product['productDefaultOpeningQuantity'],
            'productDefaultSellingPrice' => $product['productDefaultSellingPrice'],
            'productDefaultMinimumInventoryLevel' => $product['productDefaultMinimumInventoryLevel'],
            'productDefaultMaximunInventoryLevel' => $product['productDefaultMaximunInventoryLevel'],
            'productDefaultReorderLevel' => $product['productDefaultReorderLevel'],
            'productDiscountPercentage' => $product['productDiscountPercentage'],
            'productDiscountValue' => number_format($product['productDiscountValue'],2),
            'productTypeID' => $product['productTypeID'],
            'productUoms' => $productUomArray,
            'productDefaultPurchasePrice' => $product['productDefaultPurchasePrice'],
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/product-location.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        return $locationView;
    }

    public function editProductsByLocationAction()
    {
        $this->getSideAndUpperMenus('Item', 'Edit Items By Location', 'INVENTORY');

        $product = $this->CommonTable('Inventory/Model/ProductTable')->fetchAll();
        while ($pro = $product->current()) {
            $ProductsValue[$pro->productID] = $pro;
        }
        $location = $this->CommonTable('Core\Model\LocationTable')->gerActiveUserLocation($this->user_session->userID);
        while ($loc = $location->current()) {
            $loc = (Object) $loc;
            $locationValue[$loc->locationID] = $loc;
        }
        $locationView = new ViewModel(array(
            'locations' => $locationValue,
            'products' => $ProductsValue,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/location-product.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        return $locationView;
    }

    public function addCategoryAction()
    {
        $defaultcategory = null;
        $prc = array();
        $view = new ViewModel();
        $form1 = new CategoryForm(array(
        ));
        $view->form1 = $form1;

        return $view;
    }

    public function listAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $this->getSideAndUpperMenus('Item', 'View Items', 'INVENTORY');

        $this->getPaginatedProducts($locationID);

        $productListView = new ViewModel(array(
            'cSymbol' => $this->companyCurrencySymbol,
            'products' => $this->paginator,
            'paginated' => true
        ));

        $productListView->setTemplate('inventory/product/product-list');
        $mainViewModel = new ViewModel();
        $mainViewModel->addChild($productListView, 'productList');

        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/script.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['categories']);
        $this->getViewHelper('HeadScript')->prependFile('/js/product.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jquery.ui.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.min.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        $this->setLogMessage("Add product view accessed");
        return $mainViewModel;
    }

    public function getPaginatedProducts($locationID, $fixedAssetFlag = false)
    {
        $this->paginator = $this->CommonTable('Inventory/Model/ProductTable')->fetchAll(true, $locationID, $fixedAssetFlag);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
//        $this->setLogMessage("Product list accessed");
    }

    public function addUomAction()
    {
        $uoms = $this->CommonTable('Settings\Model\UomTable')->fetchAll();

        $view = new ViewModel();
        $view->uoms = $uoms;

        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        return $view;
    }

    public function importAction()
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $this->getSideAndUpperMenus('Item', 'Item Import', 'INVENTORY');
        $proimportform = new ProductdataimportForm();
        $proimportform->get('submit')->setValue('Next');
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($_FILES["productfileupload"]["tmp_name"] == null) {
                return array('form' => $proimportform, 'fileerror' => 'nofile');
            } else {

            }
            if ($request->getPost('productdelimiter') == 0) {
                $delim = ',';
            } elseif ($request->getPost('productdelimiter') == 1) {
                $delim = ';';
            } elseif ($request->getPost('productdelimiter') == 2) {
                $delim = '.';
            } else {
                $delim = ',';
            }

            if ($request->getPost('productheader') == 1) {
                $headerchecked = TRUE;
            } else {
                $headerchecked = FALSE;
            }
            $content = fopen($_FILES["productfileupload"]["tmp_name"], "r");
            $encodingtype = $request->getPost('productcharacterencoding');
            $fileenc = file($_FILES["productfileupload"]["tmp_name"]);
            $string = $fileenc[0];
            if ($encodingtype == 0) {
                $enres = mb_check_encoding($string, 'UTF-8');
                if ($enres == FALSE) {
                    return array('form' => $proimportform, 'fileerror' => 'utferror');
                }
            } elseif ($encodingtype == 1) {
                $enres = mb_check_encoding($string, 'ISO-8859-1');
                if ($enres == FALSE) {
                    return array('form' => $proimportform, 'fileerror' => 'isoerror');
                }
            }

            move_uploaded_file($_FILES["productfileupload"]["tmp_name"], '/tmp/ezBiz.productImportData.csv');
            chmod('/tmp/ezBiz.productImportData.csv', 0777);
            $productcolumncount = 0;
            if ($headerchecked == TRUE) {
                $headerlist = fgetcsv($content, 1000, $delim);
                $firstdataline = fgetcsv($content, 1000, $delim);
                for ($headers = 0; $headers < sizeof($headerlist); $headers++) {
                    $da[$headers]['header'] = $headerlist[$headers];
                    $da[$headers]['row'] = $firstdataline[$headers];
                }
                $productcolumncount = sizeof($headerlist);
            } elseif ($headerchecked == FALSE) {
                $firstdataline = fgetcsv($content, 1000, $delim);
                for ($columns = 0; $columns < sizeof($firstdataline); $columns++) {
                    $da[$columns]['header'] = "column" . ($columns + 1);
                    $da[$columns]['row'] = $firstdataline[$columns];
                }
                $productcolumncount = sizeof($firstdataline);
            }
            $view = new ViewModel(array(
                'data' => $da,
                'form' => $proimportform,
                'header' => TRUE,
                'delim' => $delim,
                'columns' => $productcolumncount
            ));
            $view->setTemplate("inventory/product/importmapping");
            $this->setLogMessage("Product import view accessed");
            return $view;
        }
        return array('form' => $proimportform);
    }

    /**
     * Uploaded file data
     * @param array $data
     * @return array
     */
    private function getFileData($data)
    {
        $fileName = tempnam( self::ITEM_IMPORT_DIR, self::ITEM_IMPORT_FILE_PREFIX);
        move_uploaded_file( $data['productFile']["tmp_name"], $fileName);
        chmod( $fileName, 0777);

        $fileenc = file($fileName);
        $encodingStatus = mb_check_encoding($fileenc[0], ($data['productCharacterEncoding'] == 0) ? 'UTF-8' : 'ISO-8859-1');

        if(!$encodingStatus){
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_ENCODING'),
                'data' => []
            ];
        }

        if (($handle = fopen($fileName, "r")) == FALSE) {
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_READ'),
                'data' => []
            ];
        }

        $dataArr = [];
        while (($fileData = fgetcsv($handle, 1000, $data['productDelimiter'])) !== FALSE) {
            $rowElementStatusArr = [];
            $rowElementStatusArr = array_map(function($element){
                $element = trim($element);
                return (empty($element)) ? false : true;
            }, $fileData);
            if(in_array(true, $rowElementStatusArr)){//for ignore empty lines
                $dataArr[] = $fileData;
            }
        }
        fclose($handle);
        return [
            'status' => true,
            'msg' => $this->getMessage('SUCC_UPLOADED_FILE'),
            'data' => $dataArr
        ];
    }

    public function itemImportAction()
    {
        $this->getSideAndUpperMenus('Item', 'Item Import', 'INVENTORY');

        $importForm = new ProductImportForm();
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return new ViewModel(array('form' => $importForm,'error' => []));
        }
        $postData = array_merge_recursive(
            $request->getPost()->toArray(),
            $request->getFiles()->toArray()
        );
        $importForm->setData($postData);
        if (!$importForm->isValid()) {
            return new ViewModel(array(
                'form' => $importForm,
                'error' => $importForm->getMessages('productFile')
                )
            );
        }
        $fileData = $this->getFileData($postData);
        if (!$fileData['status']) {
            return new ViewModel(array(
                'form' => $importForm,
                'error' => [$fileData['msg']]
                )
            );
        }
        if (count($fileData['data']) == 0 || ($postData['productHeader'] == 1 && count($fileData['data']) < 2)) {//if empty file
            return new ViewModel(array(
                'form' => $importForm,
                'error' => [$this->getMessage('ERR_EMTY_FILE')]
                )
            );
        }
        $fileHandlerType = $postData['productHandleType'];

        $itemList = $mapArr = [];
        //for separate headers and product data
        if ($postData['productHeader'] == 1) {
            $headerArr = array_shift(array_slice($fileData['data'], 0, 1));
            $itemList = array_slice($fileData['data'], 1);
            $row1 = array_shift(array_slice($itemList, 0, 1));
            $mapArr = array_combine($headerArr, $row1);
            $headers = true;
        } else {
            $itemList = $fileData['data'];
            $mapArr = array_shift(array_slice($fileData['data'], 0, 1));
            $headers = false;
        }

        //get categories
        $categories = $this->CommonTable('Inventory/Model/CategoryTable')->fetchAll();
        //product types
        $types = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        //get uoms
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        //get config file data
        $config = $this->getServiceLocator()->get('config')['fields'];

        $view = new ViewModel(array(
            'fileData' => $fileData['data'],
            'fileHandlerType' => $fileHandlerType,
            'mapData' => $mapArr,
            'headers' => $headers,
            'itemList' => $types,
            'categories' => $categories,
            'typeList' => $types,
            'uomList' => $uoms,
            'config' => $config,
            'locations' => $this->user_session->userAllLocations
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/item-import.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'productDetails',
            'itemImportConfig',
            'location'
        ]);
        $view->setTemplate("inventory/product/product-mapping");
        return $view;
    }

    public function enumerateModalAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespondHtml();
        }
        $postData = $request->getPost()->toArray();
        $fileData = unserialize($postData['fileData']);

        $itemList = [];
        //for separate headers and product data
        if ($postData['header'] == 1) {
            $itemList = array_slice($fileData, 1);
        } else {
            $itemList = $fileData;
        }

        $columnData = [];
        foreach ($itemList as $item) {
            foreach ($item as $key => $value) {
                $value = trim($value);
                if (!(empty($value) && $value != '0') && empty($columnData[$key])) {
                    $columnData[$key][] = $value;
                } else if (!(empty($value) && $value != '0') && !in_array($value, $columnData[$key])) {
                    $columnData[$key][] = $value;
                }
            }
        }

        $config = $this->getServiceLocator()->get('config')['fields'];
        $fieldData = [];
        switch ($postData['field']) {
            case 'itemCategory':
                $categories = $this->CommonTable('Inventory/Model/CategoryTable')->fetchAll();
                foreach ($categories as $category) {
                    $fieldData[$category['categoryID']] = $category['categoryName'];
                }
                $translator = new Translator();
                $fieldData['0'] = $translator->translate('ADD NEW CATEGORY');
                break;
            case 'itemType':
                $types = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
                foreach ($types as $type) {
                    $fieldData[$type->productTypeID] = $type->productTypeName;
                }
                break;
            case 'uomId':
            case 'uom1Id':
            case 'uom2Id':
                $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
                foreach ($uoms as $uom) {
                    $fieldData[$uom->uomID] = $uom->uomName;
                }
                break;
            case 'productHandelingManufactureProduct':
            case 'productHandelingPurchaseProduct':
            case 'productHandelingSalesProduct':
            case 'productHandelingConsumables':
            case 'productHandelingFixedAssets':
                $fieldData = $config['productHandelingType']['subList'];
                break;
            case 'serialProduct':
                $fieldData = $config['serialProduct']['list'];
                break;
            case 'batchProduct':
                $fieldData = $config['batchProduct']['list'];
                break;
            case 'stockInHand':
                $fieldData = $this->user_session->userAllLocations;
                break;
        }

        $view = new ViewModel(array(
            'enumerators' => $columnData[$postData['columnId']],
            'fieldData' => $fieldData
        ));
        $view->setTemplate("inventory/product/enumerator-modal");

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_ENUM_RETRIEVE');
        $this->html = $view;
        $this->data = count($columnData[$postData['columnId']]);

        return $this->JSONRespondHtml();
    }

    function importSimulateAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespondHtml();
        }

        $postData = $request->getPost()->toArray();
        $fileData = unserialize($postData['fileData']);
        if (!$fileData) {
            return $this->JSONRespondHtml();
        }

        $itemList = $fileData;
        if ($postData['header'] == 1) {
            $itemList = array_slice($fileData, 1);
        }

        $temp = array(
            'itemCode' => null,
            'itemBarcode' => null,
            'itemName' => null,
            'itemType' => null,
            'salesDiscountEligible' => 0,
            'salesDiscountVal' => 0,
            'salesDiscountPer' => 0,
            'purchaseDiscountEligible' => 0,
            'purchaseDiscountVal' => 0,
            'purchaseDiscountPer' => 0,
            'minInventoryLevel' => 0,
            'rackId' => null,
            'reoderLevel' => 0,
            'sellPrice' => 0,
            'purPrice' => 0,
            'taxEligible' => 0,
            'itemCategory' => null,
            'globalProduct' => 0,
            'batchProduct' => 0,
            'serialProduct' => 0,
            'stockInHand' => 0,
            'locations' => null,
            'uomId' => null,
            'customDutyValue' => 0,
            'productState' => 1,
            'hsCode' => null,
            'productHandelingTypes' => null,
            'productDescription' => null
        );
        $mappedArr = [];
        //get item row
        foreach ($itemList as $row => $item) {
            //get field map details
            foreach ($postData['mapData'] as $field => $map) {

                if ($field == 'productHandelingTypes') {
                    $temp['productHandelingTypes'] = $map;
                } else if ($field == 'salesDiscount' && $map['defaultValue'] == "1") {
                    $temp['salesDiscountEligible'] = 1;
                    $temp['salesDiscountPer'] = (double)$item[$map['column']];
                    $temp['salesDiscountVal'] = null;
                } else if ($field == 'salesDiscount' && $map['defaultValue'] == "2") {
                    $temp['salesDiscountEligible'] = 1;
                    $temp['salesDiscountVal'] = (double)$item[$map['column']];
                    $temp['salesDiscountPer'] = null;
                } else if ($field == 'purchaseDiscount' && $map['defaultValue'] == "1") {
                    $temp['purchaseDiscountEligible'] = 1;
                    $temp['purchaseDiscountPer'] = (double)$item[$map['column']];
                    $temp['purchaseDiscountVal'] = null;
                } else if ($field == 'purchaseDiscount' && $map['defaultValue'] == "2") {
                    $temp['purchaseDiscountEligible'] = 1;
                    $temp['purchaseDiscountVal'] = (double)$item[$map['column']];
                    $temp['purchaseDiscountPer'] = null;
                } else {
                    $column = $map['column']; //get column number
                    $subset = isset($map['subset']) ? $map['subset'] : null; //get subset

                    if (is_numeric($column)) {//is column exist
                        $fileValue = trim($item[$column]); //given text file value for an item
                        if (!(empty($fileValue) && $fileValue != '0')) {
                            if (empty($subset)) {
                                $fieldValue = $fileValue;
                            } else {
                                $fieldValue = $subset[$fileValue];
                            }
                        } else {//if empty set default
                            if($field == 'stockInHand'){//if stock in hand null set value
                                $fieldValue = 0;
                            } else if ($field == 'itemName') {
                                $fieldValue = "";
                            } else {
                                $fieldValue = (preg_match('/^\s*$/',$map['defaultValue'])) ? $temp[$field] : $map['defaultValue'];
                            }
                        }
                    } else {
                        $fieldValue = $map['defaultValue'];
                    }
                    $temp[$field] = $fieldValue;
                }
            }
            $temp['sellPrice'] = str_replace(',', '', $temp['sellPrice']);
            $temp['minInventoryLevel'] = str_replace(',', '', $temp['minInventoryLevel']);
            $temp['rackId'] = str_replace(',', '', $temp['rackId']);
            $temp['reoderLevel'] = str_replace(',', '', $temp['reoderLevel']);
            $temp['purPrice'] = str_replace(',', '', $temp['purPrice']);
            $temp['stockLocation'] = isset($postData['mapData']['stockInHand']) ? $postData['mapData']['stockInHand']['defaultValue'] : null;
            $temp['globalProduct'] = ($postData['location']['type'] == 'local') ? 0 : 1;
            $temp['locations'] = (!empty($postData['location']['list'])) ? $postData['location']['list'] : array_keys($this->user_session->userAllLocations);

            $mappedArr[] = $temp;
        }

        $bucket = $this->validateItems($mappedArr);
        $view = new ViewModel(array(
            'bucket' => $bucket,
            'noumberOfRecords' => count($mappedArr),
            'fileHandlerType' => $postData['fileHandlerType'],
            'stockUpdate' => (isset($postData['mapData']['stockInHand'])) ? true : false
        ));
        $view->setTemplate("inventory/product/import-simulate");

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_ITEM_SIMULATE');
        $this->html = $view;
        $this->data = $bucket;

        return $this->JSONRespondHtml();
    }

    /**
     * validate item field data
     * @param type $items
     * @return array
     */
    private function validateItems($items)
    {
        $bucket = ['success' => [], 'warning' => [], 'error' => []];

        $itemCodeArr = array_map(function($element) {
            return $element['itemCode'];
        }, $items);

        foreach ($items as $row => $item) {
            if (empty($item['itemCode'])) {//required
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_CODE_EMPTY')));
            } else if (!preg_match('/^[0-9A-Za-z\-_\.\%()+$#^&*!]*$/', $item['itemCode'])) { //only contain alphanumeric characters
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_CODE_PATTERN')));
            } else if (strlen($item['itemCode']) > 25) { //number of characters
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_CODE_LENGTH')));
            } else if (array_search($item['itemCode'], $itemCodeArr) != $row) {//check duplicate
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_CODE_DUPLICATE')));
            } else if (empty($item['itemName'])) {//required
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_NAME_EMPTY')));
            } else if (strlen($item['itemName']) > 200) {//number of characters
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_NAME_LENGTH')));
            } else if (!preg_match('/^[0-9A-Za-z\-_\.\% ]*$/', $item['itemBarcode'])) {//pattern match
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_BARCODE_PATTERN')));
            } else if (!is_null($item['itemBarcode']) && strlen($item['itemBarcode']) > 25) {//number of charecters
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_BARCODE_LENGTH')));
            } else if (empty($item['itemType'])) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_TYPE_EMPTY')));
            } else if (empty($item['itemCategory'])) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_CAT_EMPTY')));
            } else if (empty($item['uomId'])) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_UOM_EMPTY')));
            } else if ($this->isProductHandelingTypeExist($item) === false) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_HANDLING_TYPE_EMPTY')));
            } else if (!is_numeric($item['minInventoryLevel'])) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_MIN_INV_TYPE')));
            } else if (!is_numeric($item['reoderLevel'])) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_REORDER_TYPE')));
            } else if ($item['reoderLevel'] && is_null($item['minInventoryLevel'])) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_MIN_INV_MANDATORY')));
            } else if ($item['reoderLevel'] < $item['minInventoryLevel']) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_MIN_INV_LOWER')));
            } else if (!is_numeric($item['sellPrice'])) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_SELLING_PRICE_TYPE')));
            } else if (!is_numeric($item['purPrice'])) {
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_PURCHASING_PRICE_TYPE')));
            } else if (!is_null($item['productDescription']) && strlen($item['productDescription']) > 200) {//number of charecters
                array_push($bucket['error'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_DESCRIPTION_LENGTH')));
            } else if ($this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($item['itemCode'])) {
                array_push($bucket['warning'], array('row' => $row, 'item' => $item, 'msg' => $this->getMessage('ERR_ITEM_CODE_EXIST')));
            } else {
                array_push($bucket['success'], array('row' => $row, 'item' => $item, 'msg' => 'valid record'));
            }
        }
        return $bucket;
    }

    /**
     * for validate product handeling types
     * @param type $item
     * @return mixed
     */
    private function isProductHandelingTypeExist($item)
    {
        $statusArr = [];
        $productHandelingTypes = $item['productHandelingTypes'];
        foreach ($productHandelingTypes as $type) {
            $statusArr[] = $item[$type];
        }
        return array_search(1, $statusArr);
    }

    public function finalizeItemImportAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespondHtml();
        }

        $postData = $request->getPost()->toArray();
        $items = unserialize($postData['items']);
        if (count($postData['error']) > 0) {
            $this->msg = $this->getMessage('ERR_UPLOADED_FILE_ERRORS');
            return $this->JSONRespondHtml();
        }

        $bucket = ['saved' => [], 'merged' => [], 'error' => []];
        foreach ($items as $category => $itemList) {

            if($category == 'success'){
                if(count($itemList) > 0){
                    $bucket = $this->saveItems( $itemList, 'success', $bucket);

                    $stockArray = array_map(function($item) {
                        return (isset($item['item']['stockInHand']) && $item['item']['stockInHand'] > 0) ? true : false;
                    }, $itemList);

                    if(!empty($postData['stockInHandLocation']) && in_array(true, $stockArray)){
                        try {
                            $this->createPositiveAdjustment($itemList, $bucket, $postData['stockInHandLocation'], $postData['ignoreBudgetLimit']);
                        } catch (ProductUploadException $e) {
                            $bucket['error'][] = $e->getMessage();
                        }
                    }
                }
            } else if($category == 'warning') {
                if($postData['fileHandlerType'] == 'merge' && count($itemList) > 0){
                    $bucket = $this->saveItems($itemList, 'warning', $bucket);
                }
            }
        }

        $view = new ViewModel(array(
            'numOfItems'   => count($items['success']) + count($items['warning']) + count($items['error']),
            'numOfSuccess' => count($bucket['saved']) + count($bucket['merged']),
            'numOfSave'    => count($bucket['saved']),
            'numOfMerge'   => $postData['fileHandlerType'] == 'merge' ? count($bucket['merged']) : 0,
            'numOfSkipped' => $postData['fileHandlerType'] != 'merge' ? count($items['warning']) : 0,
            'numOfErrors'  => count($bucket['error']),
            'errors'       => $bucket['error']
        ));
        $view->setTemplate("inventory/product/finalize-item-import");

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_ITEM_UPLOAD');
        $this->html = $view;

        return $this->JSONRespondHtml();
    }

    /**
     * Save product items
     * @param array $items
     * @param string $type
     * @param array $bucket
     * @return array
     */
    private function saveItems( $items, $type, $bucket)
    {

        $itemSalesAccountID = null;
        $itemInventoryAccountID = null;
        $itemCOGSAccountID = null;
        $itemAdjusmentAccountID = null;

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            $itemSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
            $itemInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
            $itemCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
            $itemAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
        }
        foreach ($items as $item) {
            $this->beginTransaction();
            try {

                $item['item']['itemSalesAccountID'] = $itemSalesAccountID;
                $item['item']['itemInventoryAccountID'] = $itemInventoryAccountID;
                $item['item']['itemCOGSAccountID'] = $itemCOGSAccountID;
                $item['item']['itemAdjusmentAccountID'] = $itemAdjusmentAccountID;

                $result = ($type == 'success') ? $this->importValidRecord($item['item']) : $this->importDuplicateRecord($item['item']);
                if ($result) {
                    ($type == 'success') ? $bucket['saved'][] = $result : $bucket['merged'][] = $result;
                    $this->commit();
                }
            } catch (ProductUploadException $e) {
                $bucket['error'][] = $e->getMessage();
                $this->rollback();
                break;
            } catch (Exception $e) {
                $bucket['error'][] = $e->getMessage();
                $this->rollback();
                break;
            }
        }
        return $bucket;
    }

    /**
     * create positive adjustment
     * @param array $itemList
     * @param int $locationId
     * @return boolean
     * @throws ProductUploadException
     */
    private function createPositiveAdjustment( $itemList, $bucket, $locationId, $ignoreBudgetLimit)
    {
        $this->beginTransaction();

        $itemInventoryAccountID = null;
        $itemAdjusmentAccountID = null;

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();
            $itemInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;

            $accountExists = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountsByAccountsCode("4450")->current();
            if ($accountExists) {
                $itemAdjusmentAccountID = $accountExists['financeAccountsID'];
            } else {
                $data = array(
                    'financeAccountsCode' => "4450",
                    'financeAccountsName' => "Opening inventory",
                    'financeAccountClassID' => "14",
                    'financeAccountStatusID' => "1",
                    'entityID' => $this->createEntity()
                );

                $financeAccounts = new FinanceAccounts();
                $financeAccounts->exchangeArray($data);
                $financeAccountsID = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->saveFinanceAccount($financeAccounts);

                if ($financeAccountsID) {
                    $itemAdjusmentAccountID = $financeAccountsID;
                } else {
                    $this->rollback();
                    throw new ProductUploadException('Error while creating account for positive adjustment');
                }             
            }
        }

        $referenceDetails = $this->getLocationReferenceDetails(self::ITEM_IMPORT_LOCATION_REFERENCE_ID, null);
        if (!$goodsIssueEntityId = $this->createEntity()) {
            $this->rollback();
            throw new ProductUploadException('Error while creating positive adjustment');
        }
        $adjusmentCode = $this->getReferenceNumber($referenceDetails->locationReferenceID);
        $goodsIssueData = [
            'locationID' => $locationId,
            'goodsIssueCode' => $adjusmentCode,
            'goodsIssueDate' => $this->getGMTDateTime('Y-m-d'),
            'goodsIssueReason' => self::ITEM_IMPORT_GOODS_ISSUE_REASON,
            'goodsIssueTypeID' => self::ITEM_IMPORT_GOODS_ISSUE_TYPE_ID,
            'entityID' => $goodsIssueEntityId
        ];
        $goodsIssue = new GoodsIssue();
        $goodsIssue->exchangeArray($goodsIssueData);
        $goodsIssueId = $this->CommonTable('Inventory\Model\GoodsIssueTable')->saveGoodIssue($goodsIssue);
        if (!$goodsIssueId) {
            $this->rollback();
            throw new ProductUploadException('Error while creating positive adjustment');
        }

        $stockUpdatedItems = [];
        $accountProduct = array();
        //item list
        foreach ($itemList as $itemDetails) {
            $item = $itemDetails['item'];
            //for add as adjustment it should be non serial, non batch, inventory and should have staock value
            if($item['batchProduct'] != 1 && $item['serialProduct'] != 1 && $item['itemType'] == 1 && $item['stockInHand'] > 0 && in_array($item['itemCode'], $bucket['saved'])){
                $stockUpdatedItems[] = $item['itemCode'];
                $product = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByProductCodeAndLocationId($item['itemCode'], $locationId);
                $quantityData = array(
                    'locationProductQuantity' => $item['stockInHand'],
                    'locationProductAverageCostingPrice' => ($_SESSION['ezBizUser']['displaySettings']->averageCostingFlag) ? $item['purPrice'] : 0
                );
                $qtyStatus = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($quantityData, $product['locationProductID']);
                if(!$qtyStatus){
                    $this->rollback();
                    throw new ProductUploadException('Error while updating item quantity of "'+$item['itemCode']+'" to location');
                }
                //save goods issue product
                $goodIssueProductData = array(
                    'goodsIssueID' => $goodsIssueId,
                    'locationProductID' => $product['locationProductID'],
                    'goodsIssueProductQuantity' => floatval($item['stockInHand']),
                    'unitOfPrice' => (isset($item['purPrice'])) ? $item['purPrice'] : 0.00,
                    'uomID' => $item['uomId']
                );
                $goodsIssueProduct = new GoodsIssueProduct();
                $goodsIssueProduct->exchangeArray($goodIssueProductData);
                $goodsIssueProductId = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->saveGoodIssueProduct($goodsIssueProduct);
                if(!$goodsIssueProductId){
                    $this->rollback();
                    throw new ProductUploadException('Error while saving goods issue product');
                }

                if ($this->useAccounting == 1) {
                    $unitPrice = (isset($item['purPrice'])) ? $item['purPrice'] : 0.00;
                    $productTotal = floatval($item['stockInHand'])  * $unitPrice;

                    if(isset($accountProduct[$itemAdjusmentAccountID]['creditTotal'])){
                        $accountProduct[$itemAdjusmentAccountID]['creditTotal'] += $productTotal;
                    }else{
                        $accountProduct[$itemAdjusmentAccountID]['creditTotal'] = $productTotal;
                        $accountProduct[$itemAdjusmentAccountID]['debitTotal'] = 0.00;
                        $accountProduct[$itemAdjusmentAccountID]['accountID'] = $itemAdjusmentAccountID;
                    }

                    if(isset($accountProduct[$itemInventoryAccountID]['debitTotal'])){
                        $accountProduct[$itemInventoryAccountID]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$itemInventoryAccountID]['debitTotal'] = $productTotal;
                        $accountProduct[$itemInventoryAccountID]['creditTotal'] = 0.00;
                        $accountProduct[$itemInventoryAccountID]['accountID'] = $itemInventoryAccountID;
                    }
                }

                //save to item in
                $itemInData = array(
                    'itemInDocumentType' => self::ITEM_IMPORT_DOCUMENT_TYPE,
                    'itemInDocumentID' => $goodsIssueId,
                    'itemInLocationProductID' => $product['locationProductID'],
                    'itemInBatchID' => NULL,
                    'itemInSerialID' => NULL,
                    'itemInQty' => floatval($item['stockInHand']),
                    'itemInPrice' => (isset($item['purPrice'])) ? $item['purPrice'] : 0.00,
                    'itemInDateAndTime' => $this->getGMTDateTime(),
                );
                $itemIn = new ItemIn();
                $itemIn->exchangeArray($itemInData);
                $itemInId = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemIn);
                if(!$itemInId){
                    $this->rollback();
                    throw new ProductUploadException('Error while saving to item in');
                }
            }
        }

        //save journal entry for positive adjustment

        if ($this->useAccounting == 1) {
            //create data array for the journal entry save.
            $i=0;
            $journalEntryAccounts = array();

            foreach ($accountProduct as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Inventroy Positive Adjustments '.$adjusmentCode.'.';
                $i++;
            }

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $this->convertDateToStandardFormat(date("Y/m/d")),
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Positive Adjustment '.$adjusmentCode.'.',
                'documentTypeID' => 16,
                'journalEntryDocumentID' => $goodsIssueId,
                'ignoreBudgetLimit' => true,
                );

            $resultData = $this->saveJournalEntry($journalEntryData);
            if(!$resultData['status']){
                $this->rollback();
                throw new ProductUploadException('Error while creating journal entry');
            }
        }

        if(count($stockUpdatedItems) == 0){//if any product stock not updated
            $this->rollback();
            return true;
        }

        $refNumUpdateStatus = $this->updateReferenceNumber($referenceDetails->locationReferenceID);
        if(!$refNumUpdateStatus){
            $this->rollback();
            throw new ProductUploadException('Error while updating reference number');
        }

        $this->commit();
        return true;
    }

    /**
     * for save new item save
     * @param array $item
     * @return int
     * @throws ProductUploadException
     */
    private function importValidRecord($item)
    {
        //for check category exist
        $category = $this->CommonTable('Inventory\Model\CategoryTable')->getCategory($item['itemCategory']);
        if (!$category->categoryID) {//create category if not exist
           $item['itemCategory'] = preg_replace('/^new_category-/', '', $item['itemCategory']);
            //check new category name exist
            $category = $this->CommonTable('Inventory\Model\CategoryTable')->getCategoryByNameAndParentID($item['itemCategory'], 0);
            if ($category->categoryID) {//if exist assign existing category id
                $item['itemCategory'] = $category->categoryID;
            } else {// if not exist craete category
                if (!$categoryEntityId = $this->createEntity()) {
                    throw new ProductUploadException('Error while creating category for item ' . $item['itemCategory']);
                }
                $categoryData = [
                    'categoryName' => $item['itemCategory'],
                    'categoryState' => 1,
                    'entityID' => $categoryEntityId
                ];
                $newCategory = new Category();
                $newCategory->exchangeArray($categoryData);
                $categoryId = $this->CommonTable('Inventory/Model/CategoryTable')->saveCategory($newCategory);
                if (!$categoryId) {
                    throw new ProductUploadException('Error while creating category for item ' . $item['itemCategory']);
                }
                $item['itemCategory'] = $categoryId;
            }
        }

        if (!$productEntityId = $this->createEntity()) {
            throw new ProductUploadException('Error while creating category for item ' . $item['itemCategory']);
        }
        //inserting product
        $product = [
            'productCode' => $item['itemCode'],
            'productBarcode' => (!empty($item['itemBarcode'])) ? $item['itemBarcode'] : $item['itemCode'],
            'productName' => $item['itemName'],
            'productTypeID' => $item['itemType'],
            'productDescription' => $item['productDescription'],
            'categoryID' => $item['itemCategory'],
            'productDefaultMinimumInventoryLevel' => $item['minInventoryLevel'],
            'productDefaultReorderLevel' => $item['reoderLevel'],
            'productDefaultSellingPrice' => $item['sellPrice'],
            'productDefaultPurchasePrice' => $item['purPrice'],
            'productDiscountEligible' => $item['salesDiscountEligible'],
            'productDiscountPercentage' => $item['salesDiscountPer'],
            'productDiscountValue' => $item['salesDiscountVal'],
            'productPurchaseDiscountEligible' => $item['purchaseDiscountEligible'],
            'productPurchaseDiscountPercentage' => $item['purchaseDiscountPer'],
            'productPurchaseDiscountValue' => $item['purchaseDiscountVal'],
            'productState' => $item['productState'],
            'batchProduct' => ($item['batchProduct'] == 1) ? true : false,
            'serialProduct' => ($item['serialProduct'] == 1) ? true : false,
            'productGlobalProduct' => $item['globalProduct'],
            'hsCode' => $item['hsCode'],
            'productSalesAccountID' => $item['itemSalesAccountID'],
            'productInventoryAccountID' => $item['itemInventoryAccountID'],
            'productCOGSAccountID' => $item['itemCOGSAccountID'],
            'productAdjusmentAccountID' => $item['itemAdjusmentAccountID'],
            'entityID' => $productEntityId,
            'rackID' => $item['rackId']
        ];

        $product['itemBarCodes'] = [
            [
                "itemBarCodeID" => "new",
                "itemBarCode" => $product['productBarcode'],
            ]

        ];




        $importProduct = new Product();
        $importProduct->exchangeArray($product);
        $productId = $this->CommonTable('Inventory\Model\ProductTable')->saveProduct($importProduct);
        if (!$productId) { //if fail to create product
            throw new ProductUploadException('Error while inserting item ' . $item['itemCode']);
        }

        $itemBarcode = new ItemBarcode();
        foreach ($product['itemBarCodes'] as $key => $value) {
            $getbarcodeDetail = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getBarcodeByName($value['itemBarCode'], $productId);

            if (sizeof($getbarcodeDetail) > 0) {
                throw new ProductUploadException('Error while inserting item ' . $item['itemCode']);
            }

            $dataSet = [
                'productID' => $productId,
                'barCode' => $value['itemBarCode']
            ];
            $itemBarcode->exchangeArray($dataSet);

            $addBarCode = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->saveItemBarcode($itemBarcode);
        }

        //inserting location product
        foreach ($item['locations'] as $location) {
            if(!$locationEntityId = $this->createEntity()) {
                throw new ProductUploadException('Error while inserting item "'+$item['itemCode']+'" to location');
            }
            $locationProductData = [
                'productID' => $productId,
                'locationID' => $location,
                'locationProductMinInventoryLevel' => $item['minInventoryLevel'],
                'defaultSellingPrice' => $item['sellPrice'],
                'defaultPurchasePrice' => $item['purPrice'],
                'locationProductMinInventoryLevel' => $item['minInventoryLevel'],
                'locationProductReOrderLevel' => $item['reoderLevel'],
                'locationDiscountPercentage' => $item['salesDiscountPer'],
                'locationDiscountValue' => $item['salesDiscountVal'],
                'entityID' => $locationEntityId
            ];

            $locationProduct = new LocationProduct();
            $locationProduct->exchangeArray($locationProductData);
            $response = $this->CommonTable('Inventory\Model\LocationProductTable')->saveLocationProduct($locationProduct);

            if (!$response) {
                throw new ProductUploadException('Error while inserting item "'+$item['itemCode']+'" to location');
            }
        }

        //inserting product uom
        $productUomArrayList = array();
        $productUomArrayList['uomId'] = [
            'productID' => $productId,
            'uomID' => $item['uomId'],
            'productUomConversion' => 1.0,
            'productUomDisplay' => 1,
            'productUomBase' => 1
        ];

        if($item['uom1Id']){
            $productUomArrayList['uom1Id'] = [
                'productID' => $productId,
                'uomID' => $item['uom1Id'],
                'productUomConversion' => $item['uom1Conversion'],
                'productUomDisplay' => 0,
                'productUomBase' => 0
            ];
        }

        if($item['uom2Id']){
            $productUomArrayList['uom2Id'] = [
                'productID' => $productId,
                'uomID' => $item['uom2Id'],
                'productUomConversion' => $item['uom2Conversion'],
                'productUomDisplay' => 0,
                'productUomBase' => 0
            ];
        }

        if($item['displayUom']){
            $productUomArrayList['uomId']['productUomDisplay'] = 0;
            $productUomArrayList[$item['displayUom']]['productUomDisplay'] = 1;
        }

        foreach($productUomArrayList as $productUomArr){    
            $productUom = new ProductUom();
            $productUom->exchangeArray($productUomArr);
            $productUomId = $this->CommonTable('Inventory\Model\ProductUomTable')->saveProductUom($productUom);
            if (!$productUomId) {//if fail to insert to product uom
                throw new ProductUploadException('Error while inserting UOM for item '.$item['itemCode']);
            }
        }
        //inserting product handelling type
        $handellingTypeArr = ['productID' => $productId];
        foreach ($item['productHandelingTypes'] as $handelingType) {
            $handellingTypeArr[$handelingType] = ($item[$handelingType] == 1) ? true : false;
        }

        $productHandeling = new ProductHandeling();
        $productHandeling->exchangeArray($handellingTypeArr);
        $productHandelingId = $this->CommonTable('Inventory\Model\ProductHandelingTable')->saveProductHandeling($productHandeling);
        if (!$productHandelingId) {//if fail to insert to product uom
            throw new ProductUploadException('Error while inserting item "'+$item['itemCode']+'" handling type');
        }

        return $item['itemCode'];
    }

    /**
     * for update existing item
     * @param array $item
     * @return int
     * @throws ProductUploadException
     */
    private function importDuplicateRecord($item)
    {
        //for check category exist
        $category = $this->CommonTable('Inventory\Model\CategoryTable')->getCategory($item['itemCategory']);
        if (!$category->categoryID) {//create category if not exist
            $item['itemCategory'] = preg_replace('/^new_category-/', '', $item['itemCategory']);
            //check new category name exist
            $category = $this->CommonTable('Inventory\Model\CategoryTable')->getCategoryByNameAndParentID($item['itemCategory'], 0);
            if ($category->categoryID) {//if exist assign existing category id
                $item['itemCategory'] = $category->categoryID;
            } else {// if not exist craete category
                if (!$categoryEntityId = $this->createEntity()) {
                    throw new ProductUploadException('Error while creating category for item ' . $item['itemCategory']);
                }
                $categoryData = [
                    'categoryName' => $item['itemCategory'],
                    'categoryState' => 1,
                    'entityID' => $categoryEntityId
                ];
                $newCategory = new Category();
                $newCategory->exchangeArray($categoryData);
                $categoryId = $this->CommonTable('Inventory/Model/CategoryTable')->saveCategory($newCategory);
                if (!$categoryId) {
                    throw new ProductUploadException('Error while creating category for item ' . $item['itemCategory']);
                }
                $item['itemCategory'] = $categoryId;
            }
        }

        $existingProduct = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($item['itemCode']);
        $product = [
            'productBarcode' => empty(!$item['itemBarcode']) ? $item['itemBarcode'] : $existingProduct['productBarcode'],
            'productName' => empty(!$item['itemName']) ? $item['itemName'] : $existingProduct['productName'],
            'productTypeID' => empty(!$item['itemType']) ? $item['itemType'] : $existingProduct['productTypeID'],
            'productDescription' => empty(!$item['productDescription']) ? $item['productDescription'] : $existingProduct['productDescription'],
            'categoryID' => empty(!$item['itemCategory']) ? $item['itemCategory'] : $existingProduct['categoryID'],
            'productDefaultMinimumInventoryLevel' => empty(!$item['minInventoryLevel']) ? $item['minInventoryLevel'] : $existingProduct['productDefaultMinimumInventoryLevel'],
            'productDefaultReorderLevel' => empty(!$item['reoderLevel']) ? $item['reoderLevel'] : $existingProduct['productDefaultReorderLevel'],
            'productDefaultSellingPrice' => empty(!$item['sellPrice']) ? $item['sellPrice'] : 0,
            'productDefaultPurchasePrice' => empty(!$item['purPrice']) ? $item['purPrice'] : 0,
            'productDiscountPercentage' => empty(!$item['salesDiscountPer']) ? $item['salesDiscountPer'] : $existingProduct['productDiscountPercentage'],
            'productDiscountValue' => empty(!$item['salesDiscountVal']) ? $item['salesDiscountVal'] : $existingProduct['productDiscountValue'],
            'productPurchaseDiscountPercentage' => empty(!$item['purchaseDiscountPer']) ? $item['purchaseDiscountPer'] : $existingProduct['productPurchaseDiscountPercentage'],
            'productPurchaseDiscountValue' => empty(!$item['purchaseDiscountVal']) ? $item['purchaseDiscountVal'] : $existingProduct['productPurchaseDiscountValue'],
            'productState' => empty(!$item['productState']) ? $item['productState'] : $existingProduct['productState'],
            'hsCode' => empty(!$item['hsCode']) ? $item['hsCode'] : $existingProduct['hsCode'],
            'rackID' => empty(!$item['rackId']) ? $item['rackId'] : $existingProduct['rackID'],
        ];




        if (empty(!$item['itemBarcode'])) { 

            $getProductBarcodes = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getRelatedBarCodesByProductID($existingProduct['productID']);

            if (sizeof($getProductBarcodes > 0)) {
                foreach ($getProductBarcodes as $key => $val) {
                    $deleteBarCode = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->deleteBarCodeByItemBarCodeID($val['itemBarcodeID']);
                }
            } 
        }

        $productUpdateStatus = $this->CommonTable('Inventory\Model\ProductTable')->updateProductByArray($product, $existingProduct['productID']);
        if (!$productUpdateStatus) {
            throw new ProductUploadException('Error while updating item ' . $item['itemCode']);
        }

        if (empty(!$item['itemBarcode'])) {
            $product['itemBarCodes'] =   [
                [
                    "itemBarCodeID" => "new",
                    "itemBarCode" => $item['itemBarcode'],
                ]

            ];
        } else {
            $product['itemBarCodes'] = [];
        }  

        if (empty(!$item['itemBarcode'])) { 

            // $getProductBarcodes = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getRelatedBarCodesByProductID($existingProduct['productID']);

            // sizeof($getProductBarcodes > 0) {
            //     foreach ($getProductBarcodes as $key => $val) {
            //         $deleteBarCode = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->deleteBarCodeByItemBarCodeID($val['itemBarcodeID']);
            //     }
            // } 

            $itemBarcode = new ItemBarcode();
            foreach ($product['itemBarCodes'] as $key => $value) {
                $getbarcodeDetail = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getBarcodeByName($value['itemBarCode'], $existingProduct['productID']);

                if (sizeof($getbarcodeDetail) > 0) {
                    throw new ProductUploadException('Error while inserting item ' . $item['itemCode']);
                }

                $dataSet = [
                    'productID' => $existingProduct['productID'],
                    'barCode' => $value['itemBarCode']
                ];
                $itemBarcode->exchangeArray($dataSet);

                $addBarCode = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->saveItemBarcode($itemBarcode);
            }
        }

        $this->updateEntity($existingProduct['entityID']);
        //get location products
        $locationProducts = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsByProductID($existingProduct['productID']);
        foreach ($locationProducts as $locationProduct) {
            $locationProductData = [
                'locationProductMinInventoryLevel' => empty(!$item['minInventoryLevel']) ? $item['minInventoryLevel'] : $locationProduct['locationProductMinInventoryLevel'],
                'defaultSellingPrice' => empty(!$item['sellPrice']) ? $item['sellPrice'] : 0,
                'defaultPurchasePrice' => empty(!$item['purPrice']) ? $item['purPrice'] : 0,
                'locationDiscountPercentage' => empty(!$item['salesDiscountPer']) ? $item['salesDiscountPer'] : $locationProduct['locationDiscountPercentage'],
                'locationDiscountValue' => empty(!$item['salesDiscountVal']) ? $item['salesDiscountVal'] : $locationProduct['locationDiscountValue']
            ];
            $lpUpdateStatus = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($locationProductData, $locationProduct['locationProductID']);

            if(!$lpUpdateStatus){
                throw new ProductUploadException('Error while inserting item "'+$item['itemCode']+'" to location');
            }
            $this->updateEntity($locationProduct['entityID']);
        }

        //update product handling type
        $handellingTypeArr = ['productID' => $existingProduct['productID']];
        foreach ($item['productHandelingTypes'] as $handelingType) {
            $handellingTypeArr[$handelingType] = ($item[$handelingType] == 1) ? true : false;
        }

        $productHandeling = new ProductHandeling();
        $productHandeling->exchangeArray($handellingTypeArr);
        $productHandelingStatus = $this->CommonTable('Inventory\Model\ProductHandelingTable')->updateProductHandeling($productHandeling);
        if (!$productHandelingStatus) {//if fail to insert to product uom
            throw new ProductUploadException('Error while inserting item "'+$item['itemCode']+'" handling type');
        }

        return $item['itemCode'];
    }

    //item price list
    public function itemPriceListAction()
    {
        $this->getSideAndUpperMenus('Item', 'Items Price List', 'INVENTORY');

        $locationValue = [];
        $locationIDs = [];
        $location = $this->CommonTable('Core\Model\LocationTable')->gerActiveUserLocation($this->user_session->userID);
        foreach ($location as $loc) {
            $loc = (Object) $loc;
            $locationValue[$loc->locationID] = $loc;
            $locationIDs[] = $loc->locationID;
        }

        $this->getPaginatedPriceList($locationIDs);

        $locationView = new ViewModel(array(
            'locations' => $locationValue,
            'priceList' => $this->paginator,
            'paginated' => true
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/item-price-list.js');

        return $locationView;
    }

    public function getPaginatedPriceList($locationIDs)
    {
        $this->paginator = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAll(true, $locationIDs);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }


    //item price list
    public function freeItemIssueListAction()
    {
        $this->getSideAndUpperMenus('Item', 'Free Issue Items', 'INVENTORY');

        $locationID = $this->user_session->userActiveLocation['locationID'];

        $this->getPaginatedProductsForFreeIssueItem($locationID);


        //get itemIssueDetails
       $freeIssues =  $this->CommonTable('Inventory\Model\FreeIssueDetailsTable')->fetchAll();
       $freeItemsAddedItems = [];
       foreach ($freeIssues as $key => $value) {
            $freeItems =  $this->CommonTable('Inventory\Model\FreeIssueItemsTable')->getRelatedFreeIssuesByFreeIssueDetailsID($value['freeIssueDetailID']);

            if (sizeof($freeItems) > 0) {
                $freeItemsAddedItems[] = $value['majorProductID'];
            }

       }
      
        $locationView = new ViewModel(array(
            'locations' => $locationValue,
            'itemList' => $this->paginator,
            'paginated' => true,
            'currentLoc' => $locationID,
            'freeItemsAddedItems' => $freeItemsAddedItems
        ));

        $freeItems = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $freeItems->setTemplate('/inventory/product/add-free-items-modal');
        $locationView->addChild($freeItems, 'addFreeItems');

        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/free-item-issue.js');

        return $locationView;
    }

    public function getPaginatedProductsForFreeIssueItem($locationID, $fixedAssetFlag = false)
    {
        $this->paginator = $this->CommonTable('Inventory/Model/ProductTable')->fetchAll(true, $locationID, $fixedAssetFlag, true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

}