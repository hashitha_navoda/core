<?php

namespace Inventory\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;
use Inventory\Model\Product;

/**
 *
 * GiftCardRestfullController
 *
 *
 * Restfull controller for products
 */
class GiftCardRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList()
    {
        $this->service = $this->getService('ProductService');
        $locationID = $this->getRequest()->getHeaders()->get("Locationid")->getFieldValue();

        $giftCardDetails = $this->service->getGiftCardList($locationID);

        if ($giftCardDetails['status']) {
            return $this->returnJsonSuccess($giftCardDetails['data'], $giftCardDetails['msg']);
        }

        return $this->returnJsonError($products['msg']);
    }

    public function get($id)
    {
        return new JsonModel(['status' => true, 'data' => null, 'msg' => 'successfuly done']);
    }

    public function create($data)
    {
        # code...
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
