<?php

namespace Inventory\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;
use Inventory\Model\Product;

/**
 *
 * ProductRestfullController
 *
 *
 * Restfull controller for products
 */
class ProductRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList($searchKey = "", $timestamp = "")
    {
        $this->service = $this->getService('ProductService');
        $locationID = $this->getRequest()->getHeaders()->get("Locationid")->getFieldValue();
        $timestamp = (isset($_GET['timestamp'])) ? $_GET['timestamp'] : null;
        $productDetails = $this->service->getAllProductsForDesktopPOS($locationID, $timestamp);

        if ($productDetails['status']) {
            return $this->returnJsonSuccess($productDetails['data'], $productDetails['msg']);
        }

        return $this->returnJsonError($products['msg']);
    }

    public function get($id)
    {
        return new JsonModel(['status' => true, 'data' => null, 'msg' => 'successfuly done']);
    }

    public function create($data)
    {
        # code...
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
