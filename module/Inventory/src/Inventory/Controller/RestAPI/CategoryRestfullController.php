<?php

namespace Inventory\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;
use Inventory\Model\Category;

/**
 *
 * CategoryRestfullController
 *
 *
 * Restfull controller for products
 */
class CategoryRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList($searchKey = "", $timestamp = "")
    {
        $this->service = $this->getService('CategoryService');

        $categoryDetails = $this->service->getAllCategoriesForDesktopPOS($locationID, $timestamp = null);

        if ($categoryDetails['status']) {
            return $this->returnJsonSuccess($categoryDetails['data'], $categoryDetails['msg']);
        }

        return $this->returnJsonError($categories['msg']);
    }

    public function get($id)
    {
        return new JsonModel(['status' => true, 'data' => null, 'msg' => 'successfuly done']);
    }

    public function create($data)
    {
        # code...
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
