<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Inventory Product related controller functions
 */

namespace Inventory\Controller;

use Inventory\Controller\ProductController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Inventory\Form\TransferForm;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;

class TransferController extends ProductController
{

    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'transfer_upper_menu';
    protected $downMenus = '';
    private $_transferViewData;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'inventory_side_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Transfer', 'Add Transfer', 'INVENTORY');

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(16, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];
        $dateFormat = $this->getUserDateFormat();
        $form = new TransferForm();
        $form->get('date')->setAttribute('placeholder', $dateFormat);
        $form->get('date')->setAttribute("data-dateformat", $dateFormat);

        $form->get('transferCode')->setValue($rid)->setAttribute('disabled', true);

        if ($locationName) {
            $form->get('locationOut')->setValue($locationCode . ' - ' . $locationName)->setAttribute('disabled', true);
        }

        $product = $this->CommonTable('Inventory/Model/ProductTable')->fetchAll();
        while ($pro = $product->current()) {
            $ProductsValue[$pro->productID] = $pro;
        }
        $location = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
        while ($loc = $location->current()) {
            $loc = (object) $loc;
            $locationValue[$loc->locationID] = $loc;
        }

        $transferView = new ViewModel(array(
            'form' => $form,
            'locations' => $locationValue,
            'products' => $ProductsValue,
            'locationID' => $locationID,
            'locationRefID' => $lrefID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Transfer Reference Number not set';
                $msg = $this->getMessage('REQ_OUTPAY_ADD');
            } else {
                $title = 'Transfer Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('REQ_OUTPAY_CHANGE');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $transferView->addChild($refNotSet, 'refNotSet');
        }
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['location']);
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/transfer.js');

        return $transferView;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Transfer', 'View Transfers', 'INVENTORY');
        $form = new TransferForm();

        $trasnferID = $this->params()->fromRoute('param1');
        $transferDetails = $this->CommonTable('Inventory\Model\TransferTable')->getTransferByID($trasnferID);
        $transferredProducts = $this->CommonTable('Inventory\Model\TransferTable')->getTransferredProducts($trasnferID);
        $transferredProductsGrouped = array();
        $batchSerialCount = array();
        foreach ($transferredProducts as $p) {
            if (!isset($transferredProductsGrouped[$p['productID']])) {
                $transferredProductsGrouped[$p['productID']] = array_intersect_key($p, array_flip(array('productID', 'productCode', 'productName', 'transferProductQuantity')));
            }
            if (!($p['productUomConversion'] > 1)) {
                $transferredProductsGrouped[$p['productID']]['uomDecimalPlace'] = $p['uomDecimalPlace'];
            }
            $transferredProductsGrouped[$p['productID']]['selectedUomId'] = $p['selectedUomId'];
            // set uoms
            if ($p['selectedUomId'] == $p['uomID']) {
                $transferredProductsGrouped[$p['productID']]['uoms'][$p['uomID']] = array_intersect_key($p, array_flip(array('uomID', 'productUomConversion', 'uomName', 'uomAbbr', 'uomDecimalPlace', 'productUomDisplay','productUomBase')));
            } else if(is_null($p['selectedUomId'])) {
                $transferredProductsGrouped[$p['productID']]['uoms'][$p['uomID']] = array_intersect_key($p, array_flip(array('uomID', 'productUomConversion', 'uomName', 'uomAbbr', 'uomDecimalPlace', 'productUomDisplay','productUomBase')));
            }

            if (!empty($p['productSerialID'])) {
                $batchSerialCount[$p['productID']]['s' . $p['productSerialID']] = $p['transferProductQuantity'];
                $transferredProductsGrouped[$p['productID']]['serial'][$p['productSerialID']] = array_intersect_key($p, array_flip(array('productBatchID', 'productBatchCode', 'productSerialID', 'productSerialCode', 'transferProductQuantity')));
                $transferredProductsGrouped[$p['productID']]['transferProductQuantity'] = array_sum($batchSerialCount[$p['productID']]);
            } else if (!empty($p['productBatchID'])) {
                $batchSerialCount[$p['productID']]['b' . $p['productBatchID']] = $p['transferProductQuantity'];
                $transferredProductsGrouped[$p['productID']]['batch'][$p['productBatchID']] = array_intersect_key($p, array_flip(array('productBatchID', 'productBatchCode', 'transferProductQuantity')));
                $transferredProductsGrouped[$p['productID']]['transferProductQuantity'] = array_sum($batchSerialCount[$p['productID']]);
            }
        }
        $uomId;
        foreach ($transferredProductsGrouped as $key => $value) {
            if (!is_float($value['transferProductQuantity'])) {
                foreach ($value['uoms'] as $val) {
                    if ($val['productUomDisplay'] == "1" && $val['uomDecimalPlace'] != "0") {
                        $uomId[$key] = $val['uomID'];
                    } else {
                        if ($val['productUomBase'] == "1") {
                            $uomId[$key] = $val['uomID'];
                        }
                    }
                }
            } else {
                foreach ($value['uoms'] as $val) {
                    if ($val['productUomDisplay'] == "1") {
                        $uomId[$key] = $val['uomID'];
                    }
                }
            }
        }
        foreach ($transferredProductsGrouped as $key => $value) {
            if (is_null($value['selectedUomId'])) {
                foreach ($value['uoms'] as $val) {
                    if ($uomId[$key] != $val['uomID'] && $uomId[$key] != null) {
                        unset($value['uoms'][$val['uomID']]);
                    }
                }
                $transferredProductsGrouped[$value['productID']] = $value;
            }
        }

        $productsUom = array();
        foreach ($transferredProductsGrouped as $product) {
            $tempArray = array();
            $tempArray1 = array();
            foreach ($product['uoms'] as $uom) {
                $tempArray['uomID'] = $uom['uomID'];
                $tempArray['uA'] = $uom['uomAbbr'];
                $tempArray['uN'] = $uom['uomName'];
                $tempArray['uC'] = $uom['productUomConversion'];
                $tempArray['uS'] = "1";
                $tempArray['uDP'] = $uom['uomDecimalPlace'];
                $tempArray['pUDisplay'] = 1;
                $tempArray['transferProductQuantity'] = $product['transferProductQuantity'];
                $tempArray1[$uom['uomID']] = $tempArray;
            }
            $productsUom[$product[productID]]['uoms'] = $tempArray1;
            if ($product['serial']) {
                $productsUom[$product[productID]]['serial'] = $product['serial'];
            }
        }

        $dateFormat = $this->getUserDateFormat();
        $transferView = new ViewModel(array(
            'form' => $form,
            'transfer' => $transferDetails,
            'transferredProducts' => $transferredProductsGrouped,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'productsUom' => json_encode($productsUom),
            'dateFormat' => $dateFormat
        ));

        $transferView->setTemplate('/inventory/transfer/view');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['location']);
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/transfer.js');

        return $transferView;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Transfer', 'View Transfers', 'INVENTORY');

        $this->getPaginatedTransfers();
        $dateFormat = $this->getUserDateFormat();

        $transferListView = new ViewModel(array(
            'statuses' => $this->getStatusesList(),
            'transfer' => $this->paginator,
            'paginated' => true,
            'dateFormat' => $dateFormat
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/script.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/transfer.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jquery.ui.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jquery.Jcrop.min.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $transferListView->addChild($attachmentsView, 'attachmentsView');

        $this->setLogMessage("Add Transfer view accessed");
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['location']);
        return $transferListView;
    }

    public function getPaginatedTransfers()
    {
        $locationId = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Inventory\Model\TransferTable')->fetchAll(true, $locationId);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    public function transferViewAction()
    {
        return $this->documentPreviewAction();
    }

    public function documentPreviewAction()
    {

        $transferID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($transferID);
        $data['salesinvoiceTotalAmount'] = number_format($data['salesinvoiceTotalAmount'], 2);
        $path = "/transfer/document/";
        $translator = new Translator();
        $createNew = $translator->translate('New Transfer');
        $createPath = "/transfer/create";
        $data["email"] = array(
            "to" => '',
            "subject" => "Transfer from " . $data['companyName'],
            "body" => <<<EMAILBODY

A Transfer has been generated and is attached herewith. <br /><br />

<strong>Transfer No:</strong> {$data['transferCode']} <br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );
        $documentType = 'Inventory Transfer';
        $transferView = $this->getCommonPreview($data, $path, $createNew, $documentType, $transferID, $createPath);
        $transferView->setTerminal(TRUE);

        return $transferView;
    }

    public function documentPdfAction()
    {
        $transferID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Inventory Transfer';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/inventory/transfer/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($transferID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($transferID, $documentType, $documentData, $templateID);

        return;
    }

    public function documentAction()
    {
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $inventoryAdjustmentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Inventory Transfer';
        echo $this->generateDocument($inventoryAdjustmentID, $documentType, $templateID);
        exit;
    }

    public function getDataForDocument($transferID)
    {
        if (!empty($this->_transferViewData)) {
            return $this->_transferViewData;
        }

        $transferDetails = (object) $this->CommonTable('Inventory\Model\TransferTable')->getTransferredProducts($transferID);

        $transferData = array();
        $itemArray = array();
        $pQuantityArray = [];

        // extract sub prodcuts details from item
        $_items = [];
        $transferSubProducts = [];

        foreach ($transferDetails as $t) {
            if (!in_array($t['transferProductID'], $itemArray)) {
                $itemArray[] = $t['transferProductID'];
                $pQuantityArray[$t['productID']] += $t['transferProductQuantity'];
                $transferData['inventdata'][$t['productID']] = $t;
                $transferData['inventdata'][$t['productID']]['transferProductQuantity'] = $pQuantityArray[$t['productID']];
            }

            $transferCode = $t['transferCode'];
            $transferDate = $t['transferDate'];
            $transferDescription = $t['transferDescription'];
            $transferData['userUsername'] = $t['userUsername'];
            $transferData['createdTimeStamp'] = $this->getUserDateTime($t['createdTimeStamp']);

            $locationInName = $this->CommonTable('Settings\Model\LocationTable')->getLocationByID($t['locationIDIn'])->locationName;
            $locationOutName = $this->CommonTable('Settings\Model\LocationTable')->getLocationByID($t['locationIDOut'])->locationName;

            // assign each item to items array
            $_items[] = $t;

            // batch product
             if ($t['batchProduct'] == 1) {
                $tProductId = $t['productID'];
                $tProductBatchId = $t['productBatchID'];
                $tProductBatchCode = $t['productBatchCode'];
                if ($t['uomDecimalPlace'] == 0) {
                    $qty = floor($t['transferProductQuantity']);
                } else {
                    $qty = number_format($t['transferProductQuantity'], $t['uomDecimalPlace']);
                }
                $batchData = [
                    'code' => $tProductBatchCode,
                    'qty' => $qty,
                    'type' => 'batch',
                    'uom' => $t['uomAbbr']
                ];

                // For batch serial items
                // Eg: if 5 serial items from 2 batches were invoiced,
                // check if batch data is already in array and increment it's quantity by 1
                if ($t['serialProduct'] == 1 && isset($transferSubProducts[$tProductId][$tProductBatchId])) {
                    $batchData['qty'] = $transferSubProducts[$tProductId][$tProductBatchId]['qty'] + $batchData['qty'];
                }

                $transferSubProducts[$tProductId][$tProductBatchId] = $batchData;
            } // if
        } // foreach

        // serial product
        foreach ($_items as $item) {
            if ($item['serialProduct'] == 1) {
                $tProductId = $item['productID'];
                $tProductSerialId = $item['productSerialID'];
                $tProductSerialCode = $item['productSerialCode'];
                $tProductBatchId = $item['productBatchID'];

                $serialData = [
                    'code' => $tProductSerialCode,
                    'warranty' => $item['productSerialWarrantyPeriod'],
                    'type' => 'serial'
                ];

                if ($tProductBatchId) {
                    $transferSubProducts[$tProductId][$tProductBatchId]['type'] = 'batch_serial';
                    $transferSubProducts[$tProductId][$tProductBatchId]['serial'][$tProductSerialId] = $serialData;
                } else {
                    $transferSubProducts[$tProductId][$tProductSerialId] = $serialData;
                }
            }// if
        }

        $subProInStrFormat = [];
        foreach ($transferSubProducts as $key => $subProduct) {
            foreach ($subProduct as $item) {
                switch ($item['type']) {
                    case 'batch':
                        $str = 'Batch: ' . $item['code'] . ', Quantity: ' . $item['qty'] . ' ' . $item['uom'];

                        if (!array_key_exists($key, $subProInStrFormat)) {
                            $subProInStrFormat[$key] = [];
                        }
                        $subProInStrFormat[$key][] = $str;
                        break;

                    case 'serial':
                        $str = 'Serial No: ' . $item['code'] . ', ';
                        $str .= ($item['warranty']) ? 'Warranty: ' . $item['warranty'] : '';

                        if (!array_key_exists($key, $subProInStrFormat)) {
                            $subProInStrFormat[$key] = [];
                        }
                        $subProInStrFormat[$key][] = $str;
                        break;

                    case 'batch_serial':
                        $str = 'Batch: ' . $item['code'] . ', Quantity: ' . $item['qty'] . ' ' . $item['uom'];

                        if (!array_key_exists($key, $subProInStrFormat)) {
                            $subProInStrFormat[$key] = [];
                        }
                        $subProInStrFormat[$key][] = $str;

                        // serial
                        foreach ($item['serial'] as $serialItem) {
                            $serialStr = '- Serial No: ' . $serialItem['code'] . ', ';
                            $serialStr .= ($serialItem['warranty']) ? 'Warranty: ' . $serialItem['warranty'] : '';
                            $subProInStrFormat[$key][] = $serialStr;
                        }
                        break;
                }
            }
        }

        $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
        $displayData = $result->current();
        $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
        $averageCostingFlag = $displayData['averageCostingFlag'];
        // converting uom to display uom
        foreach ($transferData['inventdata'] as $productID => $t) {
            $locationProductID = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($t['productID'], $t['locationIDIn'])->locationProductID;
            $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDataByTranferId($t['transferID'], $locationProductID);


          
            if ($averageCostingFlag) {
                foreach ($itemInData as $key => $value) {
                    if ($key == (sizeof($itemInData)-1)) {
                        $averageCostingPrice = $value['itemInAverageCostingPrice'];
                    } 
                }
                $totalCost = floatval($averageCostingPrice) * floatval($t['transferProductQuantity']);
            }


            if ($FIFOCostingFlag) {

                $totalCost = 0;
                foreach ($itemInData as $key => $value) {
                    $totalCost += (floatval($value['itemInPrice']) - floatval($itemInDiscount)) * floatval($value['itemInQty']);
                }
            }
            

            $transferData['inventdata'][$productID]['itemCost'] = $totalCost;

            if (is_null($t['selectedUomId'])) {
                $productUomm = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productID);
                foreach ($productUomm as $value) {
                    if ($value['productUomDisplay'] == "1" && $value['uomDecimalPlace'] != 0) {
                        $productUom[] = $value;
                    } else {
                        if ($value['productUomBase'] == "1") {
                            $productUom[] = $value;
                        }
                    }
                }
            } else {
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productID, $t['selectedUomId']);
            }

            if($t['transferProductQuantity'] == 0) {
                $t['transferProductQuantity'] = '-';
            } else {
                $productQtyDetails = $this->getProductQuantityViaDisplayUom($t['transferProductQuantity'], $productUom);
                $transferData['inventdata'][$productID]['transferProductQuantity'] = $productQtyDetails['quantity'];
            }
            $transferData['inventdata'][$productID]['uomAbbr'] = $productQtyDetails['uomAbbr'];

            // assign transferSubProducts details to trasferData
            $transferData['inventdata'][$productID]['transferSubProducts'] = $subProInStrFormat[$productID];
        }

        $transferData['transferCode'] = $transferCode;
        $transferData['transferDate'] = $transferDate;
        $transferData['transferDescription'] = $transferDescription;
        $transferData['locationInName'] = $locationInName;
        $transferData['locationOutName'] = $locationOutName;
        $basicData = $this->getDataForDocumentView();
        $data = array_merge($basicData, $transferData);

        return $this->_transferViewData = $data;
    }

    //set adjustment print view template.
    public function getDocumentDataTable($transferID, $documentSize = 'A4')
    {
        $data_table_vars = $this->getDataForDocument($transferID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/inventory/transfer/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

}
