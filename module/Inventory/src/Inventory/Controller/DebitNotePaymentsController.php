<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file debit note payments related controller functions
 */

namespace Inventory\Controller;

//use Invoice\Form\CustomerPaymentsForm;
//use Invoice\Form\AddCustomerForm;
//use Invoice\Form\AdvancePaymentsForm;
use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * This is the debit note Payments controller class
 */
class DebitNotePaymentsController extends CoreController
{

    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'debit_note_payment_upper_menu';
    protected $user_session;
    protected $useAccounting;
    private $_debitNotePaymentViewData;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Debit Note Payments', 'Create Payments', 'PURCHASING');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(18, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $debitNoteID = $this->params()->fromRoute('param1');

//        $debitNote = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNoteID);

        $paymentterm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        while ($t = $paymentterm->current()) {
            $payterm[$t->paymentTermID] = $t->paymentTermName;
        }

        $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        $payMethod = array();
        foreach ($paymentMethos as $key => $t) {
            $payMethod[$t['paymentMethodID']] = $t;
        }

//        $paymentmethod = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
//        while ($t = $paymentmethod->current()) {
//            $paymethod[$t->paymentMethodID] = $t->paymentMethodName;
//        }
//        $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAll();
//        while ($t = $supplier->current()) {
//            $t = (object) $t;
//            $cust[$t->supplierID] = $t->supplierName;
//        }


        /** create a viewModel with data
         */
        $index = new ViewModel(array(
            'debitNoteID' => $debitNoteID,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'referenceNumber' => $rid,
            'paymentTerm' => $payterm,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'payMethods' => $payMethod,
            'useAccounting' => $this->useAccounting,
        ));
        
         // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $index->addChild($dimensionAddView, 'dimensionAddView');
        
        $check = 0;
        if ($rid == '' || $rid == NULL) {
            $check = 1;
            if ($lrefID == null) {
                $title = 'Debit Note Payment Reference Number not set';
                $msg = $this->getMessage('ERR_DNPAYMENT_ADD_REF');
            } else {
                $title = 'Debit Note Payment Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DNPAYMENT_CHANGE_REF');
            }
        }
        if ($check == 1) {
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $index->addChild($refNotSet, 'refNotSet');
        }
        $this->setLogMessage("Debit note payment create page accessed.");
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['location']);
        $this->getViewHelper('HeadScript')->prependFile('/js/Dn/debitNotePayment.js');
        return $index;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Debit Note Payments', 'View Payment', 'PURCHASING');
        $this->getPaginatedDebitNotePayments();
        $payments = new ViewModel(array(
            'debitNotePayments' => $this->paginator,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'statuses' => $this->getStatusesList()
        ));

        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/debit-note/doc-history');
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $documentView = new ViewModel();
        $documentView->setTemplate('inventory/debit-note/document-view');
        $payments->addChild($attachmentsView, 'attachmentsView');
        $payments->addChild($docHistoryView, 'docHistoryView');
        $payments->addChild($documentView, 'documentView');

        $this->getViewHelper('HeadScript')->prependFile('/js/Dn/viewDebitNotePayments.js');
        $this->setLogMessage("Debit note payment list page accessed.");
        return $payments;
    }

    public function getPaginatedDebitNotePayments()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function viewDebitNotePaymentReceiptAction()
    {
        return $this->documentPreviewAction();
    }

    public function documentPreviewAction()
    {
        $debitNotePaymentID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($debitNotePaymentID);
        $data['debitNotePaymentID'] = $data['debitNotePaymentID'];
        $data['total'] = number_format($data['total'], 2);
        $path = "/debit-note-payments/document/";
        $createNew = "New Debit Note Payment";
        $createPath = "/debit-note-payments/create";

        $data["email"] = array(
            "to" => $data['supplierEmail'],
            "subject" => "Debit Note Payment from " . $data['companyName'],
            "body" => <<<EMAILBODY

Dear {$data['supplierName']}, <br /><br />

Thank you for your inquiry. <br /><br />

A Debit Note Payment has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>CN Number:</strong> {$data['debitNotePaymentCode']} <br />
<strong>CN Amount:</strong> {$data['currencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $journalEntryValues = [];
        if($this->useAccounting == 1){
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('18',$debitNotePaymentID);
            $journalEntryID = $journalEntryData['journalEntryID'];
            $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryValues = $jeResult['JEDATA']; 
        }

        $JEntry = new ViewModel(
            array(
             'journalEntry' => $journalEntryValues,
             'closeHidden' => true 
             )
            );
        $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $documentType = 'Debit Note Payment';
        $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $debitNotePaymentID, $createPath);
        $preview->addChild($JEntry, 'JEntry');
        
        $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
        $additionalButton->setTemplate('additionalButton');
        $preview->addChild($additionalButton, 'additionalButton');

        $this->setLogMessage("Preview debit note payment ".$data['debitNotePaymentCode'].' .');
        return $preview;
    }

    public function getDataForDocument($debitNotePaymentID)
    {

        if (!empty($this->_debitNotePaymentViewData)) {
            return $this->_debitNotePaymentViewData;
        }

        $data = $this->getDataForDocumentView();

        $debitNotePaymentData = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentID($debitNotePaymentID, true)->current();
        $supplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($debitNotePaymentData['supplierID']);
        $debitNotePaymentData['createdTimeStamp'] = $this->getUserDateTime($debitNotePaymentData['createdTimeStamp']);

        $data = array_merge($data, $debitNotePaymentData, (array) $supplierDetails);

        // to be used by below functions - convert to object
        $debitNotePaymentData = (object) $debitNotePaymentData;

        $debitNotePaymentDetailsData = $this->CommonTable('Inventory\Model\DebitNotePaymentDetailsTable')->getDebitNotePaymentDetailsByDebitNotePaymentID($debitNotePaymentID);

        $paymentmethod = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentmethod as $t) {
            $paymethod[$t['paymentMethodID']] = $t['paymentMethodName'];
        }

        $paymentterm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        foreach ($paymentterm as $t) {
            $payterm[$t->paymentTermID] = $t->paymentTermName;
        }


        $sub_total = 0;
        foreach ($debitNotePaymentDetailsData as $val) {

            $val = (object) $val;
            $debitNote = (object) $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteID($val->debitNoteID)->current();

            $records[] = array(
                $debitNote->debitNoteCode,
                number_format($debitNote->debitNotePaymentAmount, 2),
                number_format($debitNote->debitNoteSettled, 2),
                $paymethod[$val->paymentMethodID],
                number_format($debitNote->debitNotePaymentAmount - $debitNote->debitNoteSettled, 2),
                number_format($val->debitNotePaymentDetailsAmount, 2),
            );
            $sub_total+= $val->debitNotePaymentDetailsAmount;
        }

        $data['type'] = _("Debit Note Payment");
        $data['debitNotePaymentTerm'] = $payterm[$debitNotePaymentData->paymentTermID];
        $data['state'] = $debitNotePaymentData->statusID;
        $data['doc_data'] = array(
            _("Date") => $debitNotePaymentData->debitNotePaymentDate,
            _("Payment terms") => $payterm[$debitNotePaymentData->paymentTermID],
            _("Debit Note Payment No") => $debitNotePaymentData->debitNotePaymentCode,
        );
        $data['sup_data'] = array(
            _("address") => '',
        );
        $data['table'] = array(
            'col_size' => array(10, 90, 290, 50, 100, 100),
            'col_allign' => array("left", "left", "left", "right", "right"),
            'headers' => array(
                _("Code") => 2,
                _("Amount </br>(" . $data['currencySymbol']) . ")" => 1,
                _("Total Settled Amount </br>(" . $data['currencySymbol']) . ")" => 1,
                _("Payment Method") => 1,
                _("Left To Pay </br>(" . $data['currencySymbol']) . ")" => 1,
                _("Paid amount </br>(" . $data['currencySymbol']) . ")" => 1
            ),
            'records' => $records,
        );
        $data['comment'] = $debitNotePaymentData->debitNotePaymentMemo;
        $data['sub_total'] = $sub_total;
        $data['total'] = $debitNotePaymentData->debitNotePaymentAmount - $debitNotePaymentData->debitNotePaymentDiscount;
        $data['discount'] = $debitNotePaymentData->debitNotePaymentDiscount;
        $data['discountStatement'] = '';
        $data['supplierName'] = $data['supplierTitle'] . ' ' . $data['supplierName'];

        return $this->_debitNotePaymentViewData = $data;
    }

    public function documentAction()
    {
        $documentType = 'Debit Note Payment';
        $debitNotePaymentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        echo $this->generateDocument($debitNotePaymentID, $documentType, $templateID);
        exit;
    }

    public function getDocumentDataTable($debitNotePaymentID, $documentSize = 'A4')
    {

        $data_table_vars = $this->getDataForDocument($debitNotePaymentID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/inventory/debit-note-payments/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * get Debit note payments list in search list
     * @return \Zend\View\Model\ViewModel
     */
    public function getDebitNotePaymentListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->getPaginatedDebitNotePayments();
            $supplierDebitNotePayments = new ViewModel(array(
                'debitNotePayments' => $this->paginator,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'statuses' => $this->getStatusesList(),
                'paginated' => TRUE,
            ));
            $supplierDebitNotePayments->setTerminal(TRUE);
            $supplierDebitNotePayments->setTemplate('inventory/debit-note-payments/debit-note-payment-list');
            return $supplierDebitNotePayments;
        }
    }

    public function documentPdfAction()
    {
        $dnpID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Debit Note Payment';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/inventory/debit-note-payments/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($dnpID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($dnpID, $documentType, $documentData, $templateID);

        return;
    }

}
