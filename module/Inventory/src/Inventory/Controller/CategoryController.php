<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * This file contains Category related controller functions
 */

namespace Inventory\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Inventory\Form\CategoryForm;

class CategoryController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'settings_products_upper_menu';

    public function indexAction()
    {

        $this->getSideAndUpperMenus('Product Setup', 'Category Setup');

        $categoryList = $this->getPaginatedCategory();

        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $form = new CategoryForm($allCategoryList);

        $view = new ViewModel(array(
            'form' => $form,
            'categoryList' => $categoryList,
            'paginated' => true
        ));

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['categories']);
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/category.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return $view;
    }

    protected function getPaginatedCategory($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Inventory/Model/CategoryTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Category list accessed');

        return $this->paginator;
    }

}
