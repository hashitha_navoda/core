<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Form\SupplierForm;
use Inventory\Form\SupplierCategoryForm;
use Inventory\Form\DataImportForm;
use Zend\I18n\Translator\Translator;

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This controller contains supplier realated functions
 */
class SupplierController extends CoreController
{

    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'supplier_upper_menu';
    protected $paginator;
    protected $supplierCategorypaginator;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'purchasing_side_menu'.$this->packageID;
        }
    }

    public function addAction()
    {
        $this->getSideAndUpperMenus('Suppliers', 'Add Supplier', 'PURCHASING');
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        while ($row = $currencyResultSet->current()) {
            $currencies[$row->currencyID] = $row->currencyName;
        }

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(34, $locationID);
        $rid = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        $supplierCode = $this->getReferenceNumber($locationReferenceID);

        while ($supplierCode) {
            $supCheck = $this->CommonTable('Inventory\Model\SupplierTable')->getNotDeletedSupplierByCode($supplierCode);
            if (!is_null($supCheck)) {
                $this->updateReferenceNumber($locationReferenceID);
                $supplierCode = $this->getReferenceNumber($locationReferenceID);
            } else {
                break;
            }
        }
        
        $supplierCategory = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->fetchAllAsArray();

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        while ($row = $paymentTermsResultSet->current()) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }

        // get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $supplierTitle = $config['personTitle'];

        $currencyValue = $this->getDefaultCurrency();
        $supplierForm = new SupplierForm(
                array(
            'paymentTerms' => $paymentTerms,
            'currencies' => $currencies,
            'supplierTitle' => $supplierTitle,
            'supplierCategory' => $supplierCategory,
            'currencyCountry' => $currencyValue,
        ));

        $supplierForm->get('supplierCode')->setAttribute('value', $supplierCode)->setAttribute('data-supcode', $supplierCode);
        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID != ''){
                $supplierPayableAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID;
                $supplierPayableAccountName = $financeAccountsArray[$supplierPayableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPayableAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-id',$supplierPayableAccountID);
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-value',$supplierPayableAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID != ''){
                $supplierPurchaseDiscountAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID;
                $supplierPurchaseDiscountAccountName = $financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-id',$supplierPurchaseDiscountAccountID);
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-value',$supplierPurchaseDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID != ''){
                $supplierGrnClearingAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID;
                $supplierGrnClearingAccountName = $financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsName'];
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-id',$supplierGrnClearingAccountID);
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-value',$supplierGrnClearingAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID != ''){
                $supplierAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID;
                $supplierAdvancePaymentAccountName = $financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsName'];
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-id',$supplierAdvancePaymentAccountID);
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-value',$supplierAdvancePaymentAccountName);
            }
        }

        $this->setLogMessage("Supplier create page accessed.");
        $supplierAddView = new ViewModel(array('supplierForm' => $supplierForm, 'useAccounting'=>$this->useAccounting));
        return $supplierAddView;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Suppliers', 'View Suppliers', 'PURCHASING');
        $this->getPaginatedSuppliers();
        $searchHeaderView = new ViewModel();
        $searchHeaderView->setTemplate('inventory/supplier/search-header');
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        while ($row = $currencyResultSet->current()) {
            $currencies[$row->currencyID] = $row->currencyName;
        }

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        while ($row = $paymentTermsResultSet->current()) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }
        // get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $supplierTitle = $config['personTitle'];
        $supplierCategory = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->fetchAllAsArray();

        $supplierForm = new SupplierForm(
                array(
            'paymentTerms' => $paymentTerms,
            'currencies' => $currencies,
            'supplierTitle' => $supplierTitle,
            'supplierCategory' => $supplierCategory,
        ));
        $supplierForm->get('supplierFormCancel')->setAttribute('class', 'hidden');
        $supplierForm->get('supplierFormButton')->setValue('Update Supplier');
        $supplierForm->get('supplierFormButton')->setAttribute('id', 'updateSupplier');
        $supplierAddView = new ViewModel(array('supplierForm' => $supplierForm,'useAccounting' => $this->useAccounting));
        $supplierAddView->setTemplate('inventory/supplier/add');
        $supplierListView = new ViewModel(array(
            'suppliers' => $this->paginator
        ));
        $supplierListView->setTemplate('inventory/supplier/supplier-list');
        $mainViewModel = new ViewModel();
        $mainViewModel->addChild($searchHeaderView, 'supplierSearchHeader');
        $mainViewModel->addChild($supplierAddView, 'createSupplierForm');
        $mainViewModel->addChild($supplierListView, 'supplierList');
        $this->setLogMessage("Supplier list page accessed.");
        return $mainViewModel;
    }

    private function getPaginatedSuppliers()
    {
        $this->paginator = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliers(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    /**
     * Access supplier category
     * @author sharmilan <sharmilan@thinkcube.com>
     *
     */
    public function supplierCategoryAction()
    {
        $this->getSideAndUpperMenus('Suppliers', 'Supplier Category', 'PURCHASING');

        $supplierCategoryList = $this->getPaginatedSupplierCategory();

        $addSupplierCategoryForm = new SupplierCategoryForm();
        $categoryViewModel = new ViewModel(array(
            'addSupplierCategoryForm' => $addSupplierCategoryForm,
            'categoryList' => $supplierCategoryList,
            'paginated' => true
        ));
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        $this->setLogMessage("Supplier category list page accessed.");
        return $categoryViewModel;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $perPage
     * @return type
     */
    protected function getPaginatedSupplierCategory($perPage = 6)
    {
        $this->supplierCategorypaginator = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->fetchAll(true);
        $this->supplierCategorypaginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->supplierCategorypaginator->setItemCountPerPage($perPage);

        return $this->supplierCategorypaginator;
    }

    public function importAction()
    {
        $this->getSideAndUpperMenus('Suppliers', 'Supplier Import', 'PURCHASING');

        $form = new DataImportForm();
        $form->get('submit')->setValue(_('Next'));

        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($_FILES["fileupload"]["tmp_name"] == null) {
                return array('form' => $form, 'fileerror' => 'nofile');
            } else if (substr($_FILES["fileupload"]["name"], -3) != 'csv') {
                return array('form' => $form, 'fileerror' => 'utferror');
            }
            if ($request->getPost('delimiter') == 0) {
                $delim = ',';
            } elseif ($request->getPost('delimiter') == 1) {
                $delim = ';';
            } elseif ($request->getPost('delimiter') == 2) {
                $delim = '.';
            } else {
                $delim = ',';
            }

            if ($request->getPost('header') == 1) {
                $headerchecked = TRUE;
            } else {
                $headerchecked = FALSE;
            }
            $content = fopen($_FILES["fileupload"]["tmp_name"], "r");
            $encodingtype = $request->getPost('characterencoding');
            $fileenc = file($_FILES["fileupload"]["tmp_name"]);
            $string = $fileenc[0];
            if ($encodingtype == 0) {
                $enres = mb_check_encoding($string, 'UTF-8');
                if ($enres == FALSE) {
                    return array('form' => $form, 'fileerror' => 'utferror');
                }
            } elseif ($encodingtype == 1) {
                $enres = mb_check_encoding($string, 'ISO-8859-1');
                if ($enres == FALSE) {
                    return array('form' => $form, 'fileerror' => 'isoerror');
                }
            }
            move_uploaded_file($_FILES["fileupload"]["tmp_name"], '/tmp/ezBiz.supplierImportData.csv');
            chmod('/tmp/ezBiz.supplierImportData.csv', 0777);

            $columncount = 0;

            if ($headerchecked == TRUE) {
                $headerlist = fgetcsv($content, 1000, $delim);
                $firstdataline = fgetcsv($content, 1000, $delim);
                for ($headers = 0; $headers < sizeof($headerlist); $headers++) {
                    $da[$headers]['header'] = $headerlist[$headers];
                    $da[$headers]['row'] = $firstdataline[$headers];
                }
                $columncount = sizeof($headerlist);
            } elseif ($headerchecked == FALSE) {
                $firstdataline = fgetcsv($content, 1000, $delim);
                for ($columns = 0; $columns < sizeof($firstdataline); $columns++) {
                    $da[$columns]['header'] = "Column" . ($columns + 1);
                    $da[$columns]['row'] = $firstdataline[$columns];
                }
                $columncount = sizeof($firstdataline);
            }
            $fileData = $this->getFileDataForSupplierImport($request->getPost()->toArray(),$request->getFiles()->toArray());
            $view = new ViewModel(array(
                'data' => $da,
                'form' => $form,
                'header' => TRUE,
                'delim' => $delim,
                'columns' => $columncount,
                'fData' => $fileData['data']
            ));
            $view->setTemplate("inventory/supplier/importmapping");
            return $view;
        }
        $this->setLogMessage("Supplier import page accessed");
        return array('form' => $form);
    }

    private function getFileDataForSupplierImport($data, $file)
    {
        $fileName = '/tmp/ezBiz.supplierImportData.csv';
        move_uploaded_file( $file['fileupload']["tmp_name"], $fileName);
        chmod( $fileName, 0777);

        $fileenc = file($fileName);
        $encodingStatus = mb_check_encoding($fileenc[0], ($data['characterencoding'] == 0) ? 'UTF-8' : 'ISO-8859-1');

        if(!$encodingStatus){
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_ENCODING'),
                'data' => []
            ];
        }

        if (($handle = fopen($fileName, "r")) == FALSE) {
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_READ'),
                'data' => []
            ];
        }

        if ($data['delimiter'] == 0) {
            $delim = ',';
        } elseif ($data['delimiter'] == 1) {
            $delim = ';';
        } elseif ($data['delimiter'] == 2) {
            $delim = '.';
        } else {
            $delim = ',';
        }

        $dataArr = [];
        while (($fileData = fgetcsv($handle, 1000, $delim)) !== FALSE) {
            $rowElementStatusArr = [];
            $rowElementStatusArr = array_map(function($element){
                $element = trim($element);
                return (empty($element)) ? false : true;
            }, $fileData);
            if(in_array(true, $rowElementStatusArr)){//for ignore empty lines
                $dataArr[] = $fileData;
            }
        }
        fclose($handle);
        return [
            'status' => true,
            'msg' => $this->getMessage('SUCC_UPLOADED_FILE'),
            'data' => $dataArr
        ];
    }

    public function enumerateModalAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespondHtml();
        }
        $postData = $request->getPost()->toArray();
        $fileData = unserialize($postData['fileData']);
        $itemList = [];
        //for separate headers and product data
        if ($postData['header'] == 1) {
            $itemList = array_slice($fileData, 1);
        } else {
            $itemList = $fileData;
        }
        
        $columnData = [];
        foreach ($itemList as $item) {
            foreach ($item as $key => $value) {
                $value = trim($value);
                if (!(empty($value) && $value != '0') && empty($columnData[$key])) {
                    $columnData[$key][] = $value;
                } else if (!(empty($value) && $value != '0') && !in_array($value, $columnData[$key])) {
                    $columnData[$key][] = $value;
                }
            }
        }

        $config = $this->getServiceLocator()->get('config')['fields'];
        $fieldData = [];

        if ($postData['field'] == 'supplierCategory') {
            $categories = $this->CommonTable('Inventory/Model/SupplierCategoryTable')->fetchAll();
            foreach ($categories as $category) {
                $fieldData[$category->supplierCategoryID] = $category->supplierCategoryName;
            }
            $translator = new Translator();
            $fieldData['0'] = $translator->translate('ADD NEW CATEGORY');
        } else if ($postData['field'] == 'supplierTitle') {
            $fieldData['Mr.'] = 'Mr.';
            $fieldData['Mrs.'] = 'Mrs.';
            $fieldData['Dr.'] = 'Dr.';
        }
        $view = new ViewModel(array(
            'enumerators' => $columnData[$postData['columnId']],
            'fieldData' => $fieldData
        ));
        $view->setTemplate("inventory/supplier/enumerator-modal");

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_ENUM_RETRIEVE');
        $this->html = $view;
        $this->data = count($columnData[$postData['columnId']]);

        return $this->JSONRespondHtml();
    }
}

?>
