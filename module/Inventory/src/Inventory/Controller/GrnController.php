<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Inventory\Form\SupplierForm;
use Inventory\Form\GrnItemImportForm;
use Zend\I18n\Translator\Translator;

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */
class GrnController extends CoreController
{

    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'grn_upper_menu';
    protected $downMenus = '';
    protected $paginator;
    protected $useAccounting;
    private $_grnViewDetails;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'inventory_side_menu'.$this->packageID;
        }
    }
    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this function set the data for the grn creation
     * @return viewModel Grn Create ViewModel
     */
    public function createAction()
    {
        $copyPoCode = '';
        $copyPoID = '';
        $compoundCostID = '';
        $paramIn = $this->params()->fromRoute('param1');
        $paramTwo = $this->params()->fromRoute('param2');

        /**
         * check if grn is going to create from a Purchase Order
         * If PO Code passed, PO data will passed in to the Grn
         */
        if(!empty($paramIn)) {
            if ($paramIn == 'compound') {
                $compoundCostID = $paramTwo;
            } else {
                $poCheck = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPoByPoCode(trim($paramIn));
                $row = $poCheck->current();
                if ($row != false) {
                    $copyPoID = $row['purchaseOrderID'];
                    $copyPoCode = $this->params()->fromRoute('param1');
                }
            }
        }
        
        $this->getSideAndUpperMenus('GRN', 'Create GRN', 'INVENTORY');
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        while ($row = $currencyResultSet->current()) {
            $currencies[$row->currencyID] = $row->currencyName;
        }

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        while ($row = $paymentTermsResultSet->current()) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }

        // get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $supplierTitle = $config['personTitle'];
        $currencyValue = $this->getDefaultCurrency();
        $supplierCategory = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->fetchAllAsArray();

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(34, $locationID);
        $rid = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        $supplierCode = $this->getReferenceNumber($locationReferenceID);

        while ($supplierCode) {
            $supCheck = $this->CommonTable('Inventory\Model\SupplierTable')->getNotDeletedSupplierByCode($supplierCode);
            if (!is_null($supCheck)) {
                $this->updateReferenceNumber($locationReferenceID);
                $supplierCode = $this->getReferenceNumber($locationReferenceID);
            } else {
                break;
            }
        }

        $supplierForm = new SupplierForm(
                array(
            'paymentTerms' => $paymentTerms,
            'currencies' => $currencies,
            'supplierTitle' => $supplierTitle,
            'supplierCategory' => $supplierCategory,
            'currencyCountry' => $currencyValue,
        ));

        $supplierForm->get('supplierCode')->setAttribute('value', $supplierCode)->setAttribute('data-supcode', $supplierCode);
        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID != ''){
                $supplierPayableAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID;
                $supplierPayableAccountName = $financeAccountsArray[$supplierPayableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPayableAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-id',$supplierPayableAccountID);
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-value',$supplierPayableAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID != ''){
                $supplierPurchaseDiscountAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID;
                $supplierPurchaseDiscountAccountName = $financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-id',$supplierPurchaseDiscountAccountID);
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-value',$supplierPurchaseDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID != ''){
                $supplierGrnClearingAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID;
                $supplierGrnClearingAccountName = $financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsName'];
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-id',$supplierGrnClearingAccountID);
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-value',$supplierGrnClearingAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID != ''){
                $supplierAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID;
                $supplierAdvancePaymentAccountName = $financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsName'];
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-id',$supplierAdvancePaymentAccountID);
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-value',$supplierAdvancePaymentAccountName);
            }
        }

        $supplierForm->get('supplierOutstandingBalance')->setAttribute('disabled', true);
        $supplierForm->get('supplierCreditBalance')->setAttribute('disabled', true);
        $supplierAddView = new ViewModel(array('supplierForm' => $supplierForm, 'useAccounting'=>$this->useAccounting));
        $supplierAddView->setTemplate('inventory/supplier/supplier-add-modal');
        $grnForm = new GrnForm();
        $grnForm->get('discount')->setAttribute('id', 'grnDiscount');
        $grnForm->get('comment')->setAttribute('data-id', $compoundCostID);
        $grnForm->get('supplierReference')->setAttribute('data-curencysymbol', $this->companyCurrencySymbol);
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'userLocations',
            'taxes'
        ]);

        $dateFormat = $this->getUserDateFormat();
        $todayDateTime = $this->getUserDateTime();
        $todayDate = date('Y-m-d', strtotime($todayDateTime));
        
        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');

        $checkIsPOApprovalActive = $this->CommonTable('Settings/Model/ApprovalDocumentDetailsTable')->checkDocIsActiveForApprovalWF(10);
        $approvalWorkflows = [];

        if ($checkIsPOApprovalActive['isActive']) {
            $getApprovalWorkflows = $this->CommonTable('Settings/Model/ApprovalWorkflowsTable')->getApprovalWorkflowsByDocID(10);

            foreach ($getApprovalWorkflows as $key => $value) {
                $approvalWorkflows[] = $value;
            }

        }

        $grnAddView = new ViewModel(
                array(
            'grnForm' => $grnForm,
            'copyToPoCode' => $copyPoCode,
            'copyToPoID' => $copyPoID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'packageID' => $this->packageID,
            'todayDate' => $todayDate,
            'dateFormat' => $dateFormat,
            'copyFromCostingID' => $compoundCostID,
            'isActiveApproval' => $checkIsPOApprovalActive['isActive'],
            'approvalWorkflows' => $approvalWorkflows
                )
        );
        $grnAddView->addChild($supplierAddView, 'supplierAddView');
        $grnProductsView = new ViewModel(array('dateFormat' => $dateFormat));
        $grnProductsView->setTemplate('inventory/grn/grn-add-products');

        $autoFillView = new ViewModel(array('dateFormat' => $dateFormat));
        $autoFillView->setTemplate('inventory/serialNumberAutoFill');
        $grnProductsView->addChild($autoFillView, 'SerialNumberAutoFill');
        $grnAddView->addChild($grnProductsView, 'grnAddProducts');

        $uploadForm = new GrnItemImportForm();

        $itemUploadView = new ViewModel(array('uploadForm' => $uploadForm));
        $itemUploadView->setTemplate('inventory/grn/grn-product-upload');
        $grnAddView->addChild($itemUploadView, 'itemUploadView');

        $grnItemImpotView = new ViewModel();
        $grnItemImpotView->setTemplate('inventory/grn/grn-item-import');
        $grnAddView->addChild($grnItemImpotView, 'grnItemImpotView');

        $grnItemUomUploadView = new ViewModel();
        $grnItemUomUploadView->setTemplate('inventory/grn/grn-item-upload-uom-map');
        $grnAddView->addChild($grnItemUomUploadView, 'grnItemUomUploadView');

        $grnItemUploadDiscMapView = new ViewModel();
        $grnItemUploadDiscMapView->setTemplate('inventory/grn/grn-item-upload-discount-map');
        $grnAddView->addChild($grnItemUploadDiscMapView, 'grnItemUploadDiscMapView');

        $grnAddView->addChild($dimensionAddView, 'dimensionAddView');
        
        $refNotSet = new ViewModel(array(
            'title' => 'Reference Number not set',
            'msg' => $this->getMessage('REQ_OUTPAY_ADD'),
            'controller' => 'company',
            'action' => 'reference'
        ));
        $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
        $grnAddView->addChild($refNotSet, 'refNotSet');

        $documenTypes = $this->getDocumentTypes();
        $dtypes = array();
        foreach ($documenTypes as $key => $val) {
            if ($key == 1 || $key == 2 || $key == 3 || $key == 4 || $key == 7 || $key == 9 || $key == 10 || $key == 12 || $key == 15 || $key == 19 || $key == 20 || $key == 22 || $key == 23 ||  $key == 11) {
                $dtypes[$key] = $val;
            }
        }

        $documentReference = new ViewModel(array(
            'title' => 'Document Reference',
            'documentType' => $dtypes,
        ));

        $documentReference->setTemplate('/core/modal/document-reference.phtml');
        $grnAddView->addChild($documentReference, 'documentReference');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $grnAddView->addChild($attachmentsView, 'attachmentsView');

        return $grnAddView;
    }

    /**
    * use to edit GRN
    *
    **/
    public function editAction()
    {
        $this->getSideAndUpperMenus('GRN', 'View GRNs', 'INVENTORY');
        $grnID = $this->params()->fromRoute('param1');

        $grn = $this->CommonTable("Inventory\Model\GrnTable")->getPiRelatedToGrnByGrnID($grnID);
        if($grn->current() != NULL){
            $this->flashMessenger()->addMessage(['status' => FALSE, 'msg' => 'This GRN used to create Purchase invoice.So you can not edit this GRN..!']);

            return $this->redirect()->toRoute(null, array(
                        'module' => 'inventory',
                        'controller' => 'grn',
                        'action' => 'list')
            );
        }

        $grnEdit = $this->createAction();
        $grnEdit->setTemplate('inventory/grn/create');
        $index = new ViewModel(
            array(
                'editMode' => true,
                'grnID' => $grnID,
                ));
        $index->addChild($grnEdit, 'grnEdit');
        // $invoiceProductsView = new ViewModel();
        // $invoiceProductsView->setTemplate('inventory/purchase-invoice/purchase-invoice-add-products');
        // $index->addChild($invoiceProductsView, 'invoiceProducts');

        return $index;
    }




    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion related to grn view
     * Get the grn list and set the paginator
     * @return viewModel Grn List ViewModel
     */
    public function listAction()
    {
        $this->getSideAndUpperMenus('GRN', 'View GRNs', 'INVENTORY');
        $userActiveLocation = $this->user_session->userActiveLocation;
        $grnList = $this->getPaginatedGrns($userActiveLocation['locationID']);
        $this->getViewHelper('HeadScript')->prependFile('/js/view.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/grn/viewGrn.js');
        $grnSearchView = new ViewModel();
        $grnSearchView->setTemplate('inventory/grn/search-header');
        $statusArray = ['closed' => 'Closed', 'open' => 'Open'];
        $statusModal = new ViewModel(array(
            'statusArray' => $statusArray
        ));
        $statusModal->setTemplate('/core/modal/status');
        $dateFormat = $this->getUserDateFormat();
        $grnViewList = new ViewModel(
                array(
            'grnList' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat
        ));
        $grnViewList->setTemplate('inventory/grn/grn-list');
        $grnView = new ViewModel();
        $grnView->addChild($grnViewList, 'grnList');
        $grnView->addChild($grnSearchView, 'grnSearchHeader');
        $grnView->addChild($statusModal, 'statusChange');
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/purchase-order/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('inventory/purchase-order/document-view');
        $grnView->addChild($docHistoryView, 'docHistoryView');
        $grnView->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $grnView->addChild($attachmentsView, 'attachmentsView');

        return $grnView;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion will view a grn (ID should be passed as a param1)
     * @return viewModel Grn View ViewModel
     */
    public function viewAction()
    {

        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $this->getSideAndUpperMenus('GRN', 'View GRNs', 'INVENTORY');
            $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnForPreviewByGrnID($paramIn);
            $grnDetails = array();
            $grnID;

            // 10 = document type for GRN. from documentType table.
            $ref_docs = $this->getDocumentReferenceBySourceData(10, $paramIn);
            $doc_types = $this->getDocumentTypes();

            $document_reference = [];
            foreach ($ref_docs as $doc_id => $type){
                $document_reference[$doc_types[$doc_id]] = $type;
            }

            $grnDetails = $this->setGrnDataToReturnFormat($grnData);
            $grnID = $grnDetails['GRNID'];


            $grnForm = new GrnForm();
            $grnForm->get('supplier')->setValue($grnDetails[$grnID]['gSN']);
            $grnForm->get('supplier')->setAttribute('disabled', TRUE);
            $grnForm->get('deliveryDate')->setValue($grnDetails[$grnID]['gD']);
            $grnForm->get('deliveryDate')->setAttribute('disabled', TRUE);
            $grnForm->get('grnNo')->setValue($grnDetails[$grnID]['gCd']);
            $grnForm->get('supplierReference')->setValue($grnDetails[$grnID]['gSR']);
            $grnForm->get('supplierReference')->setAttribute('disabled', TRUE);
            $grnForm->get('retrieveLocation')->setValue($grnDetails[$grnID]['gRL']);
            $grnForm->get('retrieveLocation')->setAttribute('disabled', TRUE);
            $grnForm->get('comment')->setValue($grnDetails[$grnID]['gC']);
            $grnForm->get('comment')->setAttribute('disabled', TRUE);
            $grnForm->get('discount')->setAttribute('id', 'grnDiscount');
            $grnSubTotal = number_format((floatval($grnDetails[$grnID]['gT']) - floatval($grnDetails[$grnID]['gDC'])), 2);
            $totalProTaxes = array();
            //Set Grn Total Tax Array
            foreach ($grnDetails[$grnID]['gProducts'] as $product) {
                if(isset($product['pT'])){
                    foreach ($product['pT'] as $tKey => $proTax) {
                        if (isset($totalProTaxes[$tKey])) {
                            $totalProTaxes[$tKey]['pTA'] = (floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA']));
                        } else {
                            $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                        }
                    }
                }

            }
            $grnSingleView = new ViewModel(
                    array(
                'grnData' => $grnDetails[$grnID],
                'grnForm' => $grnForm,
                'deliCharge' => $grnDetails[$grnID]['gDC'],
                'grnSubTotal' => $grnSubTotal,
                'pTotalTax' => $totalProTaxes,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'documentReference' => $document_reference
            ));
            $grnSingleView->setTemplate('inventory/grn/grn-view');
            return $grnSingleView;
        } else {
            $this->redirect()->toRoute('grn', array('action' => 'list'));
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion will provide a print view of a grn (ID should be passed as a param1)
     * document action should be called inside the view
     * @return viewModel Grn View ViewModel
     */
    public function previewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $data = $this->_getDataForGrnView($paramIn);
            $data['grnCode'] = $data['gCd'];
            $data['total'] = number_format($data['gT'], 2);
            $path = "/grn/document/"; //.$paramIn;
            $translator = new Translator();
            $createNew = $translator->translate('New GRN');
//            $createNew = "New GRN";
            $createPath = "/grn/create";
            $gT = number_format($data['gT'], 2);

            $data["email"] = array(
                "to" => $data['gSEmail'],
                "subject" => "GRN from " . $data['companyName'],
                "body" => <<<EMAILBODY

Dear {$data['gSName']}, <br /><br />

Thank you for your inquiry. <br /><br />

A GRN has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>GRN Number:</strong> {$data['gCd']} <br />
<strong>GRN Amount:</strong> {$data['currencySymbol']}{$gT} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
            );

            $journalEntryValues = [];
            if($this->useAccounting == 1){
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('10',$paramIn);
                $journalEntryID = $journalEntryData['journalEntryID'];
                $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
                $journalEntryValues = $jeResult['JEDATA'];
            }

            $JEntry = new ViewModel(
                array(
                 'journalEntry' => $journalEntryValues,
                 'closeHidden' => true
                 )
                );
            $JEntry->setTemplate('accounting/journal-entries/view-modal');

            $documentType = 'Goods Received Note';
            $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paramIn, $createPath);
            $preview->addChild($JEntry, 'JEntry');

            $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
            $additionalButton->setTemplate('additionalButton');
            $preview->addChild($additionalButton, 'additionalButton');

            return $preview;
        }
    }

    public function documentPdfAction()
    {
        $grnID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Goods Received Note';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/inventory/grn/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->_getDataForGrnView($grnID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($grnID, $documentType, $documentData, $templateID);

        return;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion will echo the print view of a grn (ID should be passed as a param1)
     * @return viewModel Grn View ViewModel
     */
    public function documentAction()
    {
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = 'Goods Received Note';
        $grnID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $grnID, $templateID);
//        if ($document = $tpl->getDocumentFile($filename, $documentType)) {
//            echo $document;
//            exit;
//        }
        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForGrnView($grnID);
        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);
        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($grnID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                $data['data_table_indetail'] = $this->_getDocumentDataTable($grnID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])) . '-indetail'));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    private function _getDocumentDataTable($grnID, $documentSize = 'A4')
    {

        $data_table_vars = $this->_getDataForGrnView($grnID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/inventory/grn/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion create a fully detailed data set of a grn (grnID must be passed)
     * @return Array grnData
     */
    private function _getDataForGrnView($grnID)
    {
        if (!empty($this->_grnViewDetails)) {
            return $this->_grnViewDetails;
        }

        $data = $this->getDataForDocumentView();

        $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnForPreviewByGrnID($grnID);
        $grnDetails = array();

        $grnDetails = $this->setGrnDataToReturnFormat($grnData, true);

        $grnSubTotal = number_format((floatval($grnDetails[$grnID]['gT']) - floatval($grnDetails[$grnID]['gDC'])), 2);
        //Set a total tax Array
        $totalProTaxes = array();

        foreach ($grnDetails[$grnID]['gProducts'] as $key => $product) {
            $productBaseUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductBaseUomID($product['productID']);

            if ($productBaseUom == $product['productUomID']) {
                $uomData = $this->CommonTable('Settings/Model/UomTable')->getUom($productBaseUom);
                $unitPrice = round(($product['gPP']), 10);
                $unitPrice = number_format($unitPrice, 2, '.', '');

                $grnDetails[$grnID]['gProducts'][$key]['gPQ'] = number_format($product['gPQ'], 2, '.', '');
                $grnDetails[$grnID]['gProducts'][$key]['gPP'] = $unitPrice;
            } else {
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product['productID']);

                $unitPrice = $this->getProductUnitPriceViaDisplayUom($product['gPP'], $productUom);
                $qty = $this->getProductQuantityViaDisplayUom(floatval($product['gPQ']), $productUom);

                $grnDetails[$grnID]['gProducts'][$key]['gPQ'] = ($qty['quantity'] == 0) ? '-' : $qty['quantity'];
                $grnDetails[$grnID]['gProducts'][$key]['gPUom'] = $qty['uomAbbr'];
                $grnDetails[$grnID]['gProducts'][$key]['gPUomDecimal'] = $qty['uomDecimalPlace'];
                $grnDetails[$grnID]['gProducts'][$key]['gPP'] = $unitPrice;
            }

            if(isset($product['pT'])){
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = (floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA']));
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }

        }

        $grnSupplier = $this->CommonTable('Inventory\Model\GrnTable')->getGrnSupplierDetails($grnID);

        //get supplier details
        $data['gSName'] = ($grnSupplier['supplierTitle']) ? $grnSupplier['supplierTitle'] ." ". $grnSupplier['supplierName'] : $grnSupplier['supplierName'];
        $data['gSAddress'] = $grnSupplier['supplierAddress'];
        $data['gSR'] = $grnSupplier['grnSupplierReference'];
        $data['gD'] = $grnSupplier['grnDate'];
        $data['gC'] = $grnSupplier['grnComment'];

        $grnDetails[$grnID]['grnSubTotal'] = $grnSubTotal;
        $grnDetails[$grnID]['grnTotalTax'] = $totalProTaxes;
        $data = array_merge($data, $grnDetails[$grnID]);
        $data['today_date'] = gmdate('Y-m-d');
        $data['currencySymbol'] = $this->companyCurrencySymbol;
        return $this->_grnViewDetails = $data;
    }

    private function getPaginatedGrns($userActiveLocationID = NULL)
    {
        $this->paginator = $this->CommonTable('Inventory\Model\GrnTable')->getGrns(true, $userActiveLocationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    private function setGrnDataToReturnFormat($grnData, $isSelectedUomWise = false)
    {
        $rowCount = 0;
        $tempProduct = [];
        $btchData = [];
        $serialData = [];
        $tempG = array();
        $taxDetails = [];

        foreach ($grnData as $key => $row) {

                $grnID = $row['grnID'];
                $tempG['gID'] = $row['grnID'];
                $tempG['gCd'] = $row['grnCode'];
                $tempG['gSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempG['gSID'] = $row['grnSupplierID'];
                $tempG['gSR'] = $row['grnSupplierReference'];
                $tempG['gRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempG['retriveLocation'] = $row['grnRetrieveLocation'];
                $tempG['gD'] = $this->convertDateToUserFormat($row['grnDate']);
                $tempG['gDC'] = $row['grnDeliveryCharge'];
                $tempG['gT'] = $row['grnTotal'];
                $tempG['gC'] = $row['grnComment'];
                $tempG['isApproved'] = $row['isApprovedThroughWF'];
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                if ($isSelectedUomWise) {
                    $unitPrice = $row['grnProductPrice'];
                    $productQty = $row['grnProductTotalQty'];
                    $uomSelect = $row['grnProductUom'];
                    $uomData = $this->CommonTable('Settings/Model/UomTable')->getUom($uomSelect);
                    $uomAbbr = $uomData->uomAbbr;
                } else {
                    $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['grnProductPrice'], $productUom);
                    $productQtyDetails = $this->getProductQuantityViaDisplayUom($row['grnProductTotalQty'], $productUom);
                    $productQty = $productQtyDetails['quantity'];
                    $uomSelect = $row['uomID'];
                    $uomAbbr = $productQtyDetails['uomAbbr'];
                }

                
                

                if($row['grnUploadFlag']){
                    if(isset($tempProduct[$rowCount]['bP'][$row['productBatchID']]) && $row['productSerialID']){

                    } else if(!$row['productBatchID'] && $row['productSerialID'] && $tempProduct[$rowCount]['lPID'] == $row['locationProductID']){

                    } else if($tempProduct[$rowCount]['lPID'] != $row['locationProductID']){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    } else if($tempProduct[$rowCount]['gPP'] != $unitPrice){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    }
                } else {
                    if($tempProduct[$rowCount]['lPID'] != $row['locationProductID']){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    } else if($tempProduct[$rowCount]['gPP'] != $unitPrice){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    }

                }
                $discSymbol = null;
                if($row['grnProductDiscountType'] == 'value'){
                    $discSymbol = $this->companyCurrencySymbol;
                } else if($row['grnProductDiscountType'] == 'percentage') {
                    $discSymbol = '%';
                }

                $tempProduct[$rowCount] = array(
                    'gPC' => $row['productCode'],
                    'gPN' => $row['productName'],
                    'lPID' => $row['locationProductID'],
                    'gPP' => $unitPrice,
                    'gPD' => $row['grnProductDiscount'],
                    'gPQ' => $productQty,
                    'gPT' => $row['grnProductTotal'],
                    'gPUom' => $uomAbbr,
                    'gPUomDecimal' => $row['uomDecimalPlace'],
                    'gPDT' => $discSymbol,
                    'productID' => $row['productID'],
                    'productUomID' => $uomSelect,
                    'productPoID' => $row['grnProductDocumentId'],
                    'productType' => $row['productTypeID'],
                    'grnProductID' => $row['grnProductID'],
                    );

                if ($row['grnTaxID'] != NULL) {

                    $taxDetails[$row['grnTaxID']] = array(
                        'pTN' => $row['taxName'],
                        'pTP' => $row['grnTaxPrecentage'],
                        'pTA' => $row['grnTaxAmount'],
                        'TXID' => $row['grnTaxID'],

                        );
                    $tempProduct[$rowCount]['pT'] = $taxDetails;
                }

                if($row['productBatchID']){

                    $btchData[$row['productBatchID']] = array(
                        'bC' => $row['productBatchCode'],
                        'bED' => $row['productBatchExpiryDate'],
                        'bW' => $row['productBatchWarrantyPeriod'],
                        'bMD' => $row['productBatchManufactureDate'],
                        'bQ' => $row['grnProductQuantity'],
                        'bPrice' => $row['productBatchPrice'],
                        );
                    $tempProduct[$rowCount]['bP'] = $btchData;

                    if($row['productSerialID']){
                        $serialData[$row['productSerialID']] = array(
                            'sC' => $row['productSerialCode'],
                            'sW' => $row['productSerialWarrantyPeriod'],
                            'sWT' => $row['productSerialWarrantyPeriodType'],
                            'sED' => $row['productSerialExpireDate'],
                            'sBC' => $row['productBatchCode']
                            );
                        $tempProduct[$rowCount]['sP'] = $serialData;

                    }

                } else if($row['productSerialID']){
                    $serialData[$row['productSerialID']] = array(
                        'sC' => $row['productSerialCode'],
                        'sW' => $row['productSerialWarrantyPeriod'],
                        'sWT' => $row['productSerialWarrantyPeriodType'],
                        'sED' => $row['productSerialExpireDate'],
                        'sBC' => $row['productBatchCode']
                        );
                    $tempProduct[$rowCount]['sP'] = $serialData;
                }
                $tempG['gProducts'] = $tempProduct;
                $grnDetails[$row['grnID']] = $tempG;
                $grnDetails['GRNID'] = $grnID;
        }
        return $grnDetails;
    }

    public function createdGrnDataSetByGrnIDAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        $grnID = $request->getPost('grnID');

        $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnID($grnID);
        $grnDetails = array();

        $grnDetails = $this->setGrnDataToReturnFormat($grnData, true);

        $grnSubTotal = number_format((floatval($grnDetails[$grnID]['gT']) - floatval($grnDetails[$grnID]['gDC'])), 2);
        //Set a total tax Array
        $totalProTaxes = array();
        foreach ($grnDetails[$grnID]['gProducts'] as $product) {
            if(isset($product['pT'])){
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = (floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA']));
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }

        }

        $grnSupplier = $this->CommonTable('Inventory\Model\GrnTable')->getGrnSupplierDetails($grnID);

        //get supplier details
        $data['gSName'] = ($grnSupplier['supplierTitle']) ? $grnSupplier['supplierTitle'] ." ". $grnSupplier['supplierName'] : $grnSupplier['supplierName'];
        $data['gSAddress'] = $grnSupplier['supplierAddress'];
        $data['gSR'] = $grnSupplier['grnSupplierReference'];
        $data['gD'] = $grnSupplier['grnDate'];
        $data['gC'] = $grnSupplier['grnComment'];
        $data['status'] = $grnSupplier['status'];

        $grnDetails[$grnID]['grnSubTotal'] = $grnSubTotal;
        $grnDetails[$grnID]['grnTotalTax'] = $totalProTaxes;
        $data = array_merge($data, $grnDetails[$grnID]);
        $data['today_date'] = gmdate('Y-m-d');
        $data['currencySymbol'] = $this->companyCurrencySymbol;
        $locationProducts = Array();
        foreach ($data['gProducts'] as $grnProducts) {
            $locationProducts['locationProducts'][$grnProducts['productID']] = $this->getLocationProductDetails($data['retriveLocation'], $grnProducts['productID']);
        }
        $locationPro = Array();
        $inactiveItems = Array();
        foreach ($data['gProducts'] as $gProducts) {
            $locationPro[$gProducts['productID']] = $this->getLocationProductDetails($data['retriveLocation'], $gProducts['productID']);
            if ($locationPro[$gProducts['productID']] == null) {
                $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($gProducts['productID']);
                $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
            }
        }
        
        $inactiveItemFlag = false;
        $errorMsg = null;
        if (!empty($inactiveItems)) {
            $inactiveItemFlag = true;
            $errorItems = implode(",",$inactiveItems);        
            $errorMsg = $this->getMessage('ERR_GRN_ITM_INACTIVE', [$errorItems]);
        }
        $data['errorMsg'] = $errorMsg;
        $data['inactiveItemFlag'] = $inactiveItemFlag;

        $docrefData=$this->getDocumentReferenceBySourceData(10, $grnID);
        $data['docRefData'] = $docrefData;

        $dimensionData = [];
        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(10,$grnID);
        foreach ($jEDimensionData as $value) {
            if (!is_null($value['journalEntryID'])) {
                $temp = [];
                if($value['dimensionType'] == "job" || $value['dimensionType'] == "project") {
                    if ($value['dimensionType'] == "job") {
                        $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($value['dimensionValueID']);
                        $valueTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                        $dimensionTxt = "Job";
                    } else if ($value['dimensionType'] == "project") {
                        $proData = $this->CommonTable('Jobs\Model\ProjectTable')->getProjectByProjectId($value['dimensionValueID']);
                        $valueTxt = $proData->projectCode.'-'.$proData->projectName;
                        $dimensionTxt = "Project";
                    }

                    $temp['dimensionTypeId'] = $value['dimensionType'];
                    $temp['dimensionTypeTxt'] = $dimensionTxt;
                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                    $temp['dimensionValueTxt'] = $valueTxt;
                } else {
                    $temp['dimensionTypeId'] = $value['dimensionType'];
                    $temp['dimensionTypeTxt'] = $value['dimensionName'];
                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                    $temp['dimensionValueTxt'] = $value['dimensionValue'];
                }
                $dimensionData[$grnDetails[$grnID]['gCd']][$value['dimensionType']] = $temp;
            }
        }

        $data['dimensionData'] = $dimensionData;

        $uploadedAttachments = [];
        $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($grnID, 10);
        $companyName = $this->getSubdomain();
        $path = '/userfiles/' . md5($companyName) . "/attachments";

        foreach ($attachmentData as $value) {
            $temp = [];
            $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
            $temp['documentRealName'] = $value['documentRealName'];
            $temp['docLink'] = $path . "/" .$value['documentName'];
            $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
        }

        $data['uploadedAttachments'] = $uploadedAttachments;
        $data = array_merge($data, $locationProducts);

        $this->status = true;
        $this->data = $data;
        return $this->JSONRespond();

    }

    public function draftGrnListAction() {
        $this->getSideAndUpperMenus('GRN', 'View Draft GRNs', 'INVENTORY');
        
        $userActiveLocation = $this->user_session->userActiveLocation;
        $draftGrnList = $this->getPaginatedDraftGrns($userActiveLocation['locationID']);
        $this->getViewHelper('HeadScript')->prependFile('/js/grn/draft_grn_list.js');

        $grnViewList = new ViewModel(
            array(
            'draftGrnList' =>$this->paginator,
            'statuses' => $this->getStatusesList(),
            
        ));

        return $grnViewList;

    }

    private function getPaginatedDraftGrns($userActiveLocationID = NULL)
    {
        $this->paginator = $this->CommonTable('Inventory\Model\DraftGrnTable')->getDraftGrns(true, $userActiveLocationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }


    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion will echo the print view of a grn (ID should be passed as a param1)
     * @return viewModel Grn View ViewModel
     */
    public function draftDocumentAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = 'Goods Received Note';
        $grnID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName('Draft Goods Received Note', $grnID, $templateID);
//        if ($document = $tpl->getDocumentFile($filename, $documentType)) {
//            echo $document;
//            exit;
//        }
        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForDraftGrnView($grnID);
        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);
        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($grnID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                $data['data_table_indetail'] = $this->_getDocumentDataTable($grnID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])) . '-indetail'));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion will provide a print view of a grn (ID should be passed as a param1)
     * document action should be called inside the view
     * @return viewModel Grn View ViewModel
     */
    public function draftPreviewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $data = $this->_getDataForDraftGrnView($paramIn);
            $data['grnCode'] = $data['gCd'];
            $data['total'] = number_format($data['gT'], 2);
            $path = "/grn/draftDocument/"; //.$paramIn;
            $translator = new Translator();
            $createNew = $translator->translate('New GRN');
//            $createNew = "New GRN";
            $createPath = "/grn/create";
            $gT = number_format($data['gT'], 2);

            $documentType = 'Goods Received Note';
            $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paramIn, $createPath);
            return $preview;
        }
    }

 /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion create a fully detailed data set of a grn (grnID must be passed)
     * @return Array grnData
     */
    private function _getDataForDraftGrnView($grnID)
    {
        if (!empty($this->_grnViewDetails)) {
            return $this->_grnViewDetails;
        }

        $data = $this->getDataForDocumentView();

        $grnData = $this->CommonTable('Inventory\Model\DraftGrnTable')->getGrnForPreviewByGrnID($grnID);
        $grnDetails = array();

        $grnDetails = $this->setGrnDataToReturnFormat($grnData);

        $grnSubTotal = number_format((floatval($grnDetails[$grnID]['gT']) - floatval($grnDetails[$grnID]['gDC'])), 2);
        //Set a total tax Array
        $totalProTaxes = array();
        foreach ($grnDetails[$grnID]['gProducts'] as $product) {
            if(isset($product['pT'])){
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = (floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA']));
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }

        }

        $grnSupplier = $this->CommonTable('Inventory\Model\GrnTable')->getGrnSupplierDetails($grnID);

        //get supplier details
        $data['gSName'] = ($grnSupplier['supplierTitle']) ? $grnSupplier['supplierTitle'] ." ". $grnSupplier['supplierName'] : $grnSupplier['supplierName'];
        $data['gSAddress'] = $grnSupplier['supplierAddress'];
        $data['gSR'] = $grnSupplier['grnSupplierReference'];
        $data['gD'] = $grnSupplier['grnDate'];
        $data['gC'] = $grnSupplier['grnComment'];

        $grnDetails[$grnID]['grnSubTotal'] = $grnSubTotal;
        $grnDetails[$grnID]['grnTotalTax'] = $totalProTaxes;
        $data = array_merge($data, $grnDetails[$grnID]);
        $data['today_date'] = gmdate('Y-m-d');
        $data['currencySymbol'] = $this->companyCurrencySymbol;
        return $this->_grnViewDetails = $data;
    }


}

?>
