<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Inventory\Form\SupplierForm;
use Zend\I18n\Translator\Translator;
use Zend\Session\Container;

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */
class PurchaseInvoiceController extends CoreController {

    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'pi_upper_menu';
    protected $_piViewDetails;
    protected $paginator;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'purchasing_side_menu'.$this->packageID;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion related to purchase invoice creation
     */
    public function createAction() {
        $this->getSideAndUpperMenus('Purchase Invoice', 'Create PI', 'PURCHASING');
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        while ($row = $currencyResultSet->current()) {
            $currencies[$row->currencyID] = $row->currencyName;
        }

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        while ($row = $paymentTermsResultSet->current()) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }

        // get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $supplierTitle = $config['personTitle'];

        $currencyValue = $this->getDefaultCurrency();
        $supplierCategory = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->fetchAllAsArray();

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(34, $locationID);
        $rid = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        $supplierCode = $this->getReferenceNumber($locationReferenceID);

        while ($supplierCode) {
            $supCheck = $this->CommonTable('Inventory\Model\SupplierTable')->getNotDeletedSupplierByCode($supplierCode);
            if (!is_null($supCheck)) {
                $this->updateReferenceNumber($locationReferenceID);
                $supplierCode = $this->getReferenceNumber($locationReferenceID);
            } else {
                break;
            }
        }

        $supplierForm = new SupplierForm(
                array(
            'paymentTerms' => $paymentTerms,
            'currencies' => $currencies,
            'supplierTitle' => $supplierTitle,
            'supplierCategory' => $supplierCategory,
            'currencyCountry' => $currencyValue,
        ));

        $supplierForm->get('supplierCode')->setAttribute('value', $supplierCode)->setAttribute('data-supcode', $supplierCode);
        
        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID != ''){
                $supplierPayableAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID;
                $supplierPayableAccountName = $financeAccountsArray[$supplierPayableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPayableAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-id',$supplierPayableAccountID);
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-value',$supplierPayableAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID != ''){
                $supplierPurchaseDiscountAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID;
                $supplierPurchaseDiscountAccountName = $financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-id',$supplierPurchaseDiscountAccountID);
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-value',$supplierPurchaseDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID != ''){
                $supplierGrnClearingAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID;
                $supplierGrnClearingAccountName = $financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsName'];
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-id',$supplierGrnClearingAccountID);
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-value',$supplierGrnClearingAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID != ''){
                $supplierAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID;
                $supplierAdvancePaymentAccountName = $financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsName'];
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-id',$supplierAdvancePaymentAccountID);
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-value',$supplierAdvancePaymentAccountName);
            }
        }

        $supplierAddView = new ViewModel(array('supplierForm' => $supplierForm,'useAccounting'=>$this->useAccounting));
        $supplierAddView->setTemplate('inventory/supplier/supplier-add-modal');
        $piForm = new GrnForm(array('paymentTerms' => $paymentTerms));
        $piForm->get('discount')->setAttribute('id', 'piDiscount');
        $piForm->get('grnNo')->setAttribute('id', 'piNo')->setAttribute('name', 'piNo');
        $piForm->get('grnSaveButton')->setAttribute('id', 'piSave')->setAttribute('name', 'piSaveButton');
        $piForm->get('grnCancelButton')->setAttribute('id', 'piCancel')->setAttribute('name', 'piCancelButton');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'userLocations',
            'taxes'
        ]);

        $dateFormat = $this->getUserDateFormat();
        $piAddView = new ViewModel(
                array(
                    'piForm' => $piForm,
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,
                    'dateFormat' => $dateFormat,
                    'useAccounting' => $this->useAccounting,
                    'packageID' => $this->packageID,
                )
        );
        $piAddView->addChild($supplierAddView, 'supplierAddView');
        $autoFillView = new ViewModel(array('dateFormat' => $dateFormat));
        $autoFillView->setTemplate('inventory/serialNumberAutoFill');
        $piProductsView = new ViewModel(array('dateFormat' => $dateFormat));
        $piProductsView->setTemplate('inventory/purchase-invoice/pi-add-products');
        $piProductsView->addChild($autoFillView, 'SerialNumberAutoFill');
        $piAddView->addChild($piProductsView, 'piAddProducts');
        $refNotSet = new ViewModel(array(
            'title' => 'Reference Number not set',
            'msg' => $this->getMessage('REQ_OUTPAY_ADD'),
            'controller' => 'company',
            'action' => 'reference'
        ));
        $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
        $piAddView->addChild($refNotSet, 'refNotSet');

        $documenTypes = $this->getDocumentTypes();
        $dtypes = array();
        foreach ($documenTypes as $key => $val) {
            if ($key == 1 || $key == 2 || $key == 3 || $key == 4 || $key == 7 || $key == 10 || $key == 9 || $key == 12 || $key == 15 || $key == 19 || $key == 20 || $key == 22 || $key == 23) {
                $dtypes[$key] = $val;
            }
        }

        $documentReference = new ViewModel(array(
            'title' => 'Document Reference',
            'documentType' => $dtypes,
        ));

        $documentReference->setTemplate('/core/modal/document-reference.phtml');
        $piAddView->addChild($documentReference, 'documentReference');
        
        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $piAddView->addChild($dimensionAddView, 'dimensionAddView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $piAddView->addChild($attachmentsView, 'attachmentsView');

        $this->setLogMessage("Purchase invoice create page accessed.");
        return $piAddView;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function return the viewmodel of list of PVs
     */
    public function listAction() {
        $this->getSideAndUpperMenus('Purchase Invoice', 'View PIs', 'PURCHASING');
        $this->getViewHelper('HeadScript')->prependFile('/js/view.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/pi/viewPi.js');
        $piSearchView = new ViewModel();
        $piSearchView->setTemplate('inventory/purchase-invoice/search-header');
        $userActiveLocation = $this->user_session->userActiveLocation;
        $piList = $this->getPaginatedPurchaseInvoices($userActiveLocation['locationID']);

        $dateFormat = $this->getUserDateFormat();
        $piViewList = new ViewModel(
                array(
            'piList' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat
        ));
        $piViewList->setTemplate('inventory/purchase-invoice/purchase-invoice-list');
        $piView = new ViewModel();
        $piView->addChild($piViewList, 'piList');
        $piView->addChild($piSearchView, 'piSearchHeader');
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/purchase-invoice/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('inventory/purchase-invoice/document-view');
        $piView->addChild($docHistoryView, 'docHistoryView');
        $piView->addChild($documentView, 'documentView');
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $piView->addChild($attachmentsView, 'attachmentsView');

        $this->setLogMessage("Purchase invoice list page accessed.");
        return $piView;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param param1 PV ID
     * This function view's a PV
     */
    public function viewAction() {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $this->getSideAndUpperMenus('Purchase Invoice', 'View PIs', 'PURCHASING');
            $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPIsByPIID($paramIn);

            // 12 = document type for purchase invoice. from documentType table.
            $ref_docs = $this->getDocumentReferenceBySourceData(12, $paramIn);
            $doc_types = $this->getDocumentTypes();

            $document_reference = [];

            foreach ($ref_docs as $doc_id => $type){
                $document_reference[$doc_types[$doc_id]] = $type;
            }

            $piDetails = array();
            $piID;
            foreach ($piData as $row) {
                $tempG = array();
                $piID = $row['purchaseInvoiceID'];
                $tempG['piID'] = $row['purchaseInvoiceID'];
                $tempG['piCd'] = $row['purchaseInvoiceCode'];
                $tempG['piSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempG['piSR'] = $row['purchaseInvoiceSupplierReference'];
                $tempG['piRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempG['piID'] = $this->convertDateToUserFormat($row['purchaseInvoiceIssueDate']);
                $tempG['piDD'] = $this->convertDateToUserFormat($row['purchaseInvoicePaymentDueDate']);
                $tempG['piDC'] = $row['purchaseInvoiceDeliveryCharge'];
                $tempG['piT'] = $row['purchaseInvoiceTotal'];
                $tempG['piC'] = $row['purchaseInvoiceComment'];
                $tempG['piWTD'] = $row['purchaseInvoiceWiseTotalDiscount'];
                $tempG['piWTDT'] = $row['purchaseInvoiceWiseTotalDiscountType'];
                $piProducts = (isset($piDetails[$row['purchaseInvoiceID']]['piProducts'])) ? $piDetails[$row['purchaseInvoiceID']]['piProducts'] : array();
                if ($row['purchaseInvoiceProductID'] != NULL) {
                    if ($row['purchaseInvoiceProductDocumentTypeID'] == "10" && $row['grnID'] != null) {
                        $productKey = $row['productCode'] . '-' . $row['purchaseInvoiceProductPrice'].'-'.$row['purchaseInvoiceProductDiscount']. '-'. $row['grnID'];
                    } else {
                        $productKey = $row['productCode'] . '-' . $row['purchaseInvoiceProductPrice'].'-'.$row['purchaseInvoiceProductDiscount'];
                    }
                    $productTaxes = (isset($piProducts[$productKey]['pT'])) ? $piProducts[$productKey]['pT'] : array();
                    $productBatches = (isset($piProducts[$productKey]['bP'])) ? $piProducts[$productKey]['bP'] : array();
                    $productSerials = (isset($piProducts[$productKey]['sP'])) ? $piProducts[$productKey]['sP'] : array();

                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                    $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['purchaseInvoiceProductPrice'], $productUom);
                    $thisqty = $this->getProductQuantityViaDisplayUom($row['purchaseInvoiceProductTotalQty'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                    $piProducts[$productKey] = array('piPC' => $row['productCode'], 'piPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'piPP' => $unitPrice, 'piPD' => $row['purchaseInvoiceProductDiscount'], 'piPQ' => $thisqty['quantity'], 'piPT' => $row['purchaseInvoiceProductTotal'], 'piPUom' => $thisqty['uomAbbr']);
                    if ($row['productBatchID'] != NULL) {
                        $productBatches[$row['productBatchID']] = array('bC' => $row['productBatchCode'], 'bED' => $this->convertDateToUserFormat($row['productBatchExpiryDate']), 'bW' => $row['productBatchWarrantyPeriod'], 'bMD' => $this->convertDateToUserFormat($row['productBatchManufactureDate']), 'bQ' => $row['purchaseInvoiceProductQuantity']);
                    }
                    if ($row['productSerialID'] != NULL) {
                        $productSerials[$row['productSerialID']] = array('sC' => $row['productSerialCode'], 'sW' => $row['productSerialWarrantyPeriod'],'sWT' => $row['productSerialWarrantyPeriodType'],'sED' => $this->convertDateToUserFormat($row['productSerialExpireDate']), 'sBC' => $row['productBatchCode']);
                    }
                    if ($row['purchaseInvoiceTaxID'] != NULL) {
                        $productTaxes[$row['purchaseInvoiceTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['purchaseInvoiceTaxPrecentage'], 'pTA' => $row['purchaseInvoiceTaxAmount']);
                    }
                    $piProducts[$productKey]['pT'] = $productTaxes;
                    $piProducts[$productKey]['bP'] = $productBatches;
                    $piProducts[$productKey]['sP'] = $productSerials;
                }
                $tempG['piProducts'] = $piProducts;
                $piDetails[$row['purchaseInvoiceID']] = $tempG;
            }
            $piForm = new GrnForm();
            $piForm->get('supplier')->setValue($piDetails[$piID]['piSN']);
            $piForm->get('supplier')->setAttribute('disabled', TRUE);
            $piForm->get('deliveryDate')->setValue($piDetails[$piID]['piDD']);
            $piForm->get('deliveryDate')->setAttribute('disabled', TRUE);
            $piForm->get('deliveryDate')->setAttribute('class', 'form-control');
            $piForm->get('grnNo')->setValue($piDetails[$piID]['piCd']);
            $piForm->get('supplierReference')->setValue($piDetails[$piID]['piSR']);
            $piForm->get('supplierReference')->setAttribute('disabled', TRUE);
            $piForm->get('retrieveLocation')->setValue($piDetails[$piID]['piRL']);
            $piForm->get('retrieveLocation')->setAttribute('disabled', TRUE);
            $piForm->get('comment')->setValue($piDetails[$piID]['piC']);
            $piForm->get('comment')->setAttribute('disabled', TRUE);
            $piForm->get('discount')->setAttribute('id', 'piDiscount');
            $piSubTotal = ((floatval($piDetails[$piID]['piT']) - floatval($piDetails[$piID]['piDC'])));
            $totalProTaxes = array();
            foreach ($piDetails[$piID]['piProducts'] as $product) {
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = (floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA']));
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }
            $piSingleView = new ViewModel(
                    array(
                'piID' => $piDetails[$piID]['piID'],
                'piData' => $piDetails[$piID],
                'piForm' => $piForm,
                'deliCharge' => $piDetails[$piID]['piDC'],
                'piSubTotal' => $piSubTotal,
                'piTotalDiscount' => $piDetails[$piID]['piWTD'],
                'pTotalTax' => $totalProTaxes,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'documentReference' => $document_reference));
            $piSingleView->setTemplate('inventory/purchase-invoice/purchase-invoice-view');
            $this->setLogMessage("View purchase invoice ".$tempG['piCd'].".");
            return $piSingleView;
        } else {
            $this->setLogMessage("Redirect to purchase invoice list.");
            $this->redirect()->toRoute('purchaseInvoice', array('action' => 'list'));
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param param1 PV ID
     * This function provides a print view of a PV
     */
    public function previewAction() {
       
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $data = $this->_getDataForPiView($paramIn);
            $data['pvCode'] = $data['piCd'];
            $data['total'] = ($data['piT']);
            $path = "/pi/document/"; //.$paramIn;
            $translator = new Translator();
            $createNew = $translator->translate('New Purchase Invoice');
//            $createNew = "New Payment Voucher";
            $createPath = "/pi/create";

            $globaldata = $this->getServiceLocator()->get('config');
            $piT = number_format($data['piT'], 2);

            $data["email"] = array(
                "to" => $data['piSEmail'],
                "subject" => "Purchase Voucher from " . $data['companyName'],
                "body" => <<<EMAILBODY

Dear {$data['piSName']}, <br /><br />

Thank you for your inquiry. <br /><br />

An Purchase Voucher has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>Purchase Voucher Number:</strong> {$data['piCd']} <br />
<strong>Purchase Voucher Amount:</strong> {$data['currencySymbol']}{$piT} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
            );

            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountFlag = (count($glAccountExist) > 0)? true :false;
            $journalEntryValues = [];
            if($glAccountFlag){
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('12',$data['piID']);
                $journalEntryID = $journalEntryData['journalEntryID'];
                $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
                $journalEntryValues = $jeResult['JEDATA'];
            }

            $JEntry = new ViewModel(
                array(
                 'journalEntry' => $journalEntryValues,
                 'closeHidden' => true
                )
            );
            $JEntry->setTemplate('accounting/journal-entries/view-modal');

            $documentType = 'Payment Voucher (Purchase Invoice) ';
            $state = $globaldata['statuses'][$data['piSt']];

            $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paramIn, $createPath);
            $additionalButton = new ViewModel(array('state' => $state, 'piCode' => $data['piID'], 'data' => $data, 'glAccountFlag' => $this->useAccounting));
            $additionalButton->setTemplate('inventory/purchase-invoice/additionalButton');
            $preview->addChild($additionalButton, 'additionalButton');
            $preview->addChild($JEntry, 'JEntry');
            $preview->setTerminal(true);

            $this->setLogMessage("Preview purchase invoice ".$data['pvCode']);
            return $preview;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param param1 PV ID
     * This function Provides a data structure view of PV
     */
    public function documentAction() {
     
        $paramIn = $this->params()->fromRoute('param1');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = 'Payment Voucher (Purchase Invoice)';
        $piID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        $paymentData=$this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentIDByPurchaseInvoiceID($piID);
        $paymentIDs = array();

        foreach($paymentData as $key => $value) {
            $paymentIDs[]=$value['paymentID'];
        }


        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $piID, $templateID);
//        if ($document = $tpl->getDocumentFile($filename, $documentType)) {
//            echo $document;
//            exit;
//        }
        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForPiView($piID);
        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);
        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($piID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                $data['data_table_indetail'] = $this->_getDocumentDataTable($piID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])) . '-indetail'));
                $data['payment_table'] = $this->_getDocumentPaymentTable($paymentIDs, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])).'-payment'));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    private function _getDocumentDataTable($piID, $documentSize = 'A4') {
        $data_table_vars = $this->_getDataForPiView($piID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/inventory/purchase-invoice/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    protected function _getDataForPiView($piID) {
       
        if (!empty($this->_piViewDetails)) {
            return $this->_piViewDetails;
        }

        $data = $this->getDataForDocumentView(); // get comapny details

        $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPIsByPIID($piID);
        $piDetails = array();
        foreach ($piData as $row) {
            
            $tempG = array();
            $tempG['piID'] = $row['purchaseInvoiceID'];
            $tempG['piCd'] = $row['purchaseInvoiceCode'];
            $tempG['piSName'] = $row['supplierTitle'] . ' ' . $row['supplierName'];
            $tempG['piSAddress'] = $row['supplierAddress'];
            $tempG['piSEmail'] = $row['supplierEmail'];
            $tempG['piSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
            $tempG['piSR'] = $row['purchaseInvoiceSupplierReference'];
            $tempG['piRL'] = $row['locationCode'] . '-' . $row['locationName'];
            $tempG['piIssueD'] = $this->convertDateToUserFormat($row['purchaseInvoiceIssueDate']);
            $tempG['piDD'] = $this->convertDateToUserFormat($row['purchaseInvoicePaymentDueDate']);
            $tempG['piDC'] = $row['purchaseInvoiceDeliveryCharge'];
            $tempG['piT'] = ($row['purchaseInvoiceTotal']);
            $tempG['piSTax'] = $row['purchaseInvoiceShowTax'];
            $tempG['piC'] = $row['purchaseInvoiceComment'];
            $tempG['piWTD'] = $row['purchaseInvoiceWiseTotalDiscount'];
            $tempG['discountStatement'] = _("Total discount received");
            $tempG['piSt'] = $row['status'];
            $tempG['piPTN'] = $row['paymentTermName'];
            $tempG['userUsername'] = $row['userUsername'];
            $tempG['createdTimeStamp'] = $this->getUserDateTime($row['createdTimeStamp']);
            $piProducts = (isset($piDetails[$row['purchaseInvoiceID']]['piProducts'])) ? $piDetails[$row['purchaseInvoiceID']]['piProducts'] : array();
            if ($row['purchaseInvoiceProductID'] != NULL) {
                if ($row['purchaseInvoiceProductDocumentTypeID'] == "10" && $row['grnID'] != null) {
                    $productKey = $row['productCode'] . '-' . $row['purchaseInvoiceProductPrice'].'-'.$row['purchaseInvoiceProductDiscount']. '-'. $row['grnID'];
                } else {
                    $productKey = $row['productCode'] . '-' . $row['purchaseInvoiceProductPrice'].'-'.$row['purchaseInvoiceProductDiscount'];
                }
                $productTaxes = (isset($piProducts[$productKey]['pT'])) ? $piProducts[$productKey]['pT'] : array();
                $productBatches = (isset($piProducts[$productKey]['bP'])) ? $piProducts[$productKey]['bP'] : array();
                $productSerials = (isset($piProducts[$productKey]['sP'])) ? $piProducts[$productKey]['sP'] : array();

                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['purchaseInvoiceProductPrice'], $productUom);
                $thisqty = $this->getProductQuantityViaDisplayUom($row['purchaseInvoiceProductTotalQty'], $productUom);
                $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];


                if (!empty($row['purchaseInvoiceProductSelectedUomId'])) {

                    $productBaseUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductBaseUomID($row['productID']);
                    if ($productBaseUom == $row['purchaseInvoiceProductSelectedUomId']) {

                        $uomData = $this->CommonTable('Settings/Model/UomTable')->getUom($productBaseUom);
                        $unitPrice = round(($row['purchaseInvoiceProductPrice']), 10);
                        $unitPrice = number_format($unitPrice, 2, '.', '');

                        $thisqty['quantity'] = number_format($row['purchaseInvoiceProductTotalQty'], 2, '.', '');
                        $thisqty['uomAbbr'] = $uomData->uomAbbr;
                        
                    } else {

                        $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                        $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['purchaseInvoiceProductPrice'], $productUom);
                        $thisqty = $this->getProductQuantityViaDisplayUom($row['purchaseInvoiceProductTotalQty'], $productUom);
                        $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];
                    } 
                } 

                $piProducts[$productKey] = array('piPC' => $row['productCode'], 'piPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'piPP' => $unitPrice, 'piPD' => $row['purchaseInvoiceProductDiscount'], 'piPQ' => $thisqty['quantity'], 'piPT' => $row['purchaseInvoiceProductTotal'], 'piPUom' => $thisqty['uomAbbr']);
                if ($row['productBatchID'] != NULL) {
                    $productBatches[$row['productBatchID']] = array('bC' => $row['productBatchCode'], 'bED' => $this->convertDateToUserFormat($row['productBatchExpiryDate']), 'bW' => $row['productBatchWarrantyPeriod'], 'bMD' => $this->convertDateToUserFormat($row['productBatchManufactureDate']), 'bQ' => $row['productBatchQuantity']);
                }
                if ($row['productSerialID'] != NULL) {
                    $productSerials[$row['productSerialID']] = array('sC' => $row['productSerialCode'], 'sW' => $row['productSerialWarrantyPeriod'],'sWT' => $row['productSerialWarrantyPeriodType'],'sED' => $this->convertDateToUserFormat($row['productSerialExpireDate']), 'sBC' => $row['productBatchCode']);
                }
                if ($row['purchaseInvoiceTaxID'] != NULL) {
                    $productTaxes[$row['purchaseInvoiceTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['purchaseInvoiceTaxPrecentage'], 'pTA' => $row['purchaseInvoiceTaxAmount']);
                }
                $piProducts[$productKey]['pT'] = $productTaxes;
                $piProducts[$productKey]['bP'] = $productBatches;
                $piProducts[$productKey]['sP'] = $productSerials;
            }
            $tempG['piProducts'] = $piProducts;
            $piDetails[$row['purchaseInvoiceID']] = $tempG;
        }

        $piSubTotal = (floatval($piDetails[$piID]['piT']) - floatval($piDetails[$piID]['piDC']));
        $totalProTaxes = array();
        foreach ($piDetails[$piID]['piProducts'] as $product) {
            foreach ($product['pT'] as $tKey => $proTax) {
                if (isset($totalProTaxes[$tKey])) {
                    $totalProTaxes[$tKey]['pTA'] = (floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA']));
                } else {
                    $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                }
            }
        }
        $piDetails[$piID]['piSubTotal'] = $piSubTotal;
        $piDetails[$piID]['piTotalTax'] = $totalProTaxes;

        $purchaseInvoiceMethodData = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentsDetailsByPurchaseInvoiceID($piID);
        $pvMethod="";
        foreach ($purchaseInvoiceMethodData as $key => $value) {
            if($value['paymentMethodID']==2){
                $pvMethod.=$value['outGoingPaymentMethodReferenceNumber']."(".$value['bankName'].")"."\r\n";
            }
        }
        $piDetails[$piID]['check_and_bank'] = $pvMethod;

        $data = array_merge($data, $piDetails[$piID]);
        $data['today_date'] = gmdate('Y-m-d');
        $data['currencySymbol'] = $this->companyCurrencySymbol;
        return $this->_piViewDetails = $data;
    }

     private function _getDocumentPaymentTable($paymentIDs, $documentSize = 'A4') {
        $data_table_vars = array();
        $currencySymbol = null;
        $total = 0.0;
        $discountTot = 0.0;
        $i = 0;
        if(count($paymentIDs) > 0 ){
            foreach ($paymentIDs as $pi){


                $data_table_vars[] = $this->_getDataForPaymentView($pi);
                $currencySymbol = $data_table_vars[0]['currencySymbol'];
                $leftToPay = $data_table_vars[$i]['leftToPay'];
                $total += $data_table_vars[$i]['total'];
                $discountTot += $data_table_vars[$i]['cr_discount'];
                $i++;
            }
        }

        $view = new ViewModel(
            array(
                'paymentDetails' => $data_table_vars,
                'currencySymbol' => $currencySymbol,
                'settledTotal' => $total,
                'discountTotal' => $discountTot,
                'leftToPay' => $leftToPay
            ));
        $view->setTemplate('/inventory/purchase-invoice/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    protected function _getDataForPaymentView($paymentID) {

        $data = array();
        $paymentDetails = $this->CommonTable('SupplierPaymentsTable')->getAllPaymentDetailsByID($paymentID)->current();

        $paymentMethodDetails = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPaymentMethodDetailsByPaymentId($paymentID)->current();

        $invoiceDetails = $this->CommonTable('SupplierInvoicePaymentsTable')->getAllSupplierInvoiceDetailsByPaymentID($paymentID)->current();

        $paymentDetails['outgoingPaymentDate'] = $this->convertDateToUserFormat($paymentDetails['outgoingPaymentDate']);
        $paymentDetails['createdTimeStamp'] = $this->getUserDateTime($paymentDetails['createdTimeStamp']);
        $data = array_merge($data, $paymentDetails,$paymentMethodDetails);

        // to be used by below functions - convert to object
        $paymentDetails = (object) $paymentDetails;
        $paymentMethodDetails = (object) $paymentMethodDetails;
        $invoiceDetails = (object) $invoiceDetails;

        // Use core date function
        $data['today_date'] = gmdate('Y-m-d');

        $displaySetup = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();
        $data['currencySymbol'] = $displaySetup['currencySymbol'];


        $paymentmethods = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();


        while ($t = $paymentmethods->current()) {
            $t = (object) $t;
            $paymethod[] = $t;
        }


        if($invoiceDetails->paymentMethodID == NULL){
            $data['paymentMethodName'] = 'Credit Payment';
        }else{
            $data['paymentMethodName'] = $paymethod[$invoiceDetails->paymentMethodID - 1]->paymentMethodName;
        }
        $data['paymentMethodReference'] = $paymentMethodDetails->outGoingPaymentMethodReferenceNumber;
        $data['type'] = "receipt";
        $data['outgoingPaymentDate'] = $paymentDetails->outgoingPaymentDate;
        $data['paymentTermName'] = $paymentDetails->paymentTermName;
        $data['outgoingPaymentCode'] = $paymentDetails->outgoingPaymentCode;
        $data['outgoingPaymentID'] = $paymentDetails->outgoingPaymentID;
        $data['cr_discount'] = $paymentDetails->outgoingPaymentDiscount;
        $data['total'] = $paymentDetails->outgoingPaymentAmount - $paymentDetails->outgoingPaymentDiscount;
        $data['cashPayment'] = $invoiceDetails->outgoingInvoiceCashAmount;
        $data['creditPayment'] = $invoiceDetails->outgoingInvoiceCreditAmount;
        $data['leftToPay'] = $invoiceDetails->left_to_pay;
        $data['cdnUrl'] = $this->cdnUrl;

        return $data;
    }

    private function getPaginatedPurchaseInvoices($userActiveLocationID = NULL) {
        $this->paginator = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoices(true, $userActiveLocationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Purchase Invoice', 'Create PI', 'PURCHASING');
        $pIID = $this->params()->fromRoute('param1');

        $openState = 3;
        $details = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($pIID, $openState)->current();
        $pIEdit = $this->createAction();
        $pIEdit->setTemplate('inventory/purchase-invoice/create');
        $index = new ViewModel(
            array(
                'editMode' => true,
                'pIID' => $pIID,
                ));
        $index->addChild($pIEdit, 'pIEditDetails');
        $invoiceProductsView = new ViewModel();
        $invoiceProductsView->setTemplate('inventory/purchase-invoice/purchase-invoice-add-products');
        $index->addChild($invoiceProductsView, 'invoiceProducts');

        return $index;
    }

    public function documentPdfAction()
    {
        $piID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Payment Voucher (Purchase Invoice)';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/inventory/purchase-invoice/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->_getDataForPiView($piID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($piID, $documentType, $documentData, $templateID);

        return;
    }
}

?>
