<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Form\CustomerPaymentsForm;
use Invoice\Form\AdvancePaymentsForm;
use Zend\I18n\Translator\Translator;
use Zend\Session\Container;

/**
 * @author Ashan madushka <ashan@thinkcube.com>
 */
class OutGoingPaymentController extends CoreController
{

    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'supplierpayments_upper_menu';
    protected $paginator;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'purchasing_side_menu'.$this->packageID;
        }
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * this functoion related to outgoing payment creation
     */
    public function createAction()
    {
        $this->getSideAndUpperMenus('Payments', 'Create Payment', 'PURCHASING');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(13, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $param1 = $this->params()->fromRoute('param1');
        $param2 = $this->params()->fromRoute('param2');

        if ($param1 == 'sup') {
            $piCode = '';
            $supplierID = $param2;
        } else if($param1 == 'pV'){
            $paymentVoucher = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherDetailsByPVID($param2);
            $pV = $paymentVoucher->current();
            if ($pV) {
                $pVID = $pV['paymentVoucherID'];
                $pVCode = $pV['paymentVoucherCode'];
            } else {
                $pVID = '';
                $pVCode = '';
            }
        } else {
            $purchaseInvoice = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($param1);
            $pi = $purchaseInvoice->current();
            if ($pi) {
                $piID = $pi['purchaseInvoiceID'];
                $piCode = $pi['purchaseInvoiceCode'];
            } else {
                $piID = '';
                $piCode = '';
            }
        }

        $paymentterm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        while ($t = $paymentterm->current()) {

            switch ($t->paymentTermName) {
                case 'Cash Only':
                    $paymentTerm = 'Cash';
                    break;
                case 'Cheque Only':
                    $paymentTerm = 'Cheque';
                    break;
                case 'Card Only':
                    $paymentTerm = 'Card';
                    break;
                case 'Loyalty Card Only':
                    $paymentTerm = 'Loyalty Card';
                    break;
                case 'Bank Transfer Only':
                    $paymentTerm = 'Bank Transfer';
                    break;
                case 'Gift Card Only':
                    $paymentTerm = 'Gift Card';
                    break;
                case 'LC Only':
                    $paymentTerm = 'LC';
                    break;
                case 'TT Only':
                    $paymentTerm = 'TT';
                    break;
                case 'Uniform Voucher Only':
                    $paymentTerm = 'Uniform Voucher';
                    break;
                default:
                    $paymentTerm = null;
                    break;
            }


            if (!empty($paymentTerm)) {
                $ptData = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethodByName($paymentTerm);

                if ($ptData->paymentMethodInPurchase) {
                    $payterm[$t->paymentTermID] = $t->paymentTermName;
                }
            } else {

                $payterm[$t->paymentTermID] = $t->paymentTermName;
            }
        }

        $suppliers = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliers();
        while ($t = $suppliers->current()) {
            $t = (object) $t;
            $supl[$t->supplierID] = $t->supplierName;
        }

        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(false, array('bank.bankName' => 'ASC'));
        foreach ($banks as $t) {
            $t = (object) $t;
            $bank[$t->bankId] = $t->bankName;
        }

        // $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        // $payMethod = array();
        // foreach ($paymentMethos as $key => $t) {
        //     $payMethod[$t['paymentMethodID']] = $t;
        // }

        $payMethod = array();
        $payMethodAcc = array();
        $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentMethos as $t) {
            if($t['paymentMethodInSales'] == 1){
                $payMethod[$t['paymentMethodID']] = $t['paymentMethodName'];
                $payMethodAcc[$t['paymentMethodID']] = $t;
            }
        }

        $form2 = new CustomerPaymentsForm(array(
            'suppliers' => $supl,
            'customerID' => 'supplierID',
            'banks' => $bank,
            'paymentTerm' => $payterm,
            'giftCards' => $giftCards,
            'paymentMethod' => $payMethod,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'companyCurrencyId' => $this->companyCurrencyId,
            'customCurrency' => $currency,
        ));
        $form2->removeAttribute('onsubmit');
        $form2->get('save')->setValue('Add Payment');
        $form2->get('InvoiceID')->setAttribute('placeholder', 'Enter PV ID');

        /**
         * create a viewModel with data
         * SupplierPaymentForm,paymentReference Number
         * */
        $dateFormat = $this->getUserDateFormat();
        $todayDateTime = $this->getUserDateTime();
        $todayDate = date('Y-m-d', strtotime($todayDateTime));

        $index = new ViewModel(array(
            'form2' => $form2,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'piID' => $piID,
            'piCode' => $piCode,
            'pVID' => $pVID,
            'pVCode' => $pVCode,
            'supplierID' => $supplierID,
            'referenceNumber' => $rid,
            'sup_list' => $this->getSupplierNameJson(),
            'invoice_list' => $this->getInvoiceIdsJson(),
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'dateFormat' => $dateFormat,
            'todayDate' => $todayDate,
            'banks' => $bank,
            'payMethods' => $payMethod,
            'useAccounting' => $this->useAccounting,
            'payMethodAcc' =>$payMethodAcc,
        ));

        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Supplier Payment Reference Number not set';
                $msg = $this->getMessage('REQ_OUTPAY_ADD');
            } else {
                $title = 'Supplier Payment Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('REQ_OUTPAY_CHANGE');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));

            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $index->addChild($refNotSet, 'refNotSet');
        }

        $this->setLogMessage('Supplier payment create page accessed.');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['location']);
        $this->getViewHelper('HeadScript')->prependFile('/js/supplierPayment/viewPayments.js');

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $index->addChild($dimensionAddView, 'dimensionAddView');
        
        return $index;
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * this functoion related to outgoing payment advance payment creation
     */
    public function advancePaymentAction()
    {
        $this->getSideAndUpperMenus('Payments', 'Create Payment', 'PURCHASING');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(13, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];
        $dateFormat = $this->getUserDateFormat();

        $paymentmethod = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        $payMethod = array();
        $payMethodForAcc = array();
        foreach ($paymentmethod as $t) {
            if($t['paymentMethodInPurchase'] == 1){
                $payMethodForAcc[$t['paymentMethodID']] = $t;
                $paymethod[$t['paymentMethodID']] = $t['paymentMethodName'];
            }
        }

        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";


        $suppliers = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliers();
        while ($t = $suppliers->current()) {
            $t = (object) $t;
            $supl[$t->supplierID] = $t->supplierName;
        }

        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(false, array('bank.bankName' => 'ASC'));
        foreach ($banks as $t) {
            $t = (object) $t;
            $bank[$t->bankId] = $t->bankName;
        }

        $form1 = new AdvancePaymentsForm(array(
            'suppliers' => $supl,
            'paymentMethod' => $paymethod,
            'custName' => 'advanceSupplier',
            'banks' => $bank,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));

        /**
         * create a viewModel with data
         * AdvancePaymentsForm, advanceReference Number
         * */
        $index = new ViewModel(array(
            'form1' => $form1,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'adReferenceNumber' => $rid,
            'sup_list' => $this->getSupplierNameJson(),
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'dateFormat' => $dateFormat,
            'banks' => $bank,
            'payMethods' => $payMethodForAcc,
            'useAccounting' => $this->useAccounting,
        ));

        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Supplier Payment Reference Number not set';
                $msg = $this->getMessage('REQ_OUTPAY_ADD');
            } else {
                $title = 'Supplier Payment Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('REQ_OUTPAY_CHANGE');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));

            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $index->addChild($refNotSet, 'refNotSet');
        }

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $index->addChild($dimensionAddView, 'dimensionAddView');


        $this->setLogMessage('Supplier advance payment create page accessed.');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['location']);
        $this->getViewHelper('HeadScript')->prependFile('/js/supplierPayment/viewPayments.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/supplierPayment/advancePayments.js');

        return $index;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Payments', 'View PI Payments', 'PURCHASING');
        $whichPayment = "PI";
        $this->getPaginatedPayments($whichPayment);

        $dateFormat = $this->getUserDateFormat();
        $payments = new ViewModel(array(
            'payments' => $this->paginator,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat,
            'whichPayment'=> $whichPayment,
        ));

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/out-going-payment/doc-history');
        $payments->addChild($attachmentsView, 'attachmentsView');
        $payments->addChild($docHistoryView, 'docHistoryView');
        $documentView = new ViewModel();
        $documentView->setTemplate('inventory/out-going-payment/document-view');
        $payments->addChild($documentView, 'documentView');

        $this->getViewHelper('HeadScript')->prependFile('/js/supplierPayment/viewPayments.js');
        $this->setLogMessage('Supplier purchase invoice payment list page accessed.');
        return $payments;
    }

    public function viewPVPaymentsAction()
    {
        $this->getSideAndUpperMenus('Payments', 'View PV Payments', 'PURCHASING');
        $whichPayment = "PV";
        $this->getPaginatedPayments($whichPayment);

        $dateFormat = $this->getUserDateFormat();
        $payments = new ViewModel(array(
            'payments' => $this->paginator,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat,
            'whichPayment'=> $whichPayment,
        ));

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $payments->addChild($attachmentsView, 'attachmentsView');
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/out-going-payment/doc-history');
        $payments->addChild($docHistoryView, 'docHistoryView');
        $documentView = new ViewModel();
        $documentView->setTemplate('inventory/out-going-payment/document-view');
        $payments->addChild($documentView, 'documentView');

        $payments->setTemplate('inventory/out-going-payment/view');
        $this->getViewHelper('HeadScript')->prependFile('/js/supplierPayment/viewPayments.js');
        $this->setLogMessage('Supplier payment voucher payment list page accessed.');
        return $payments;
    }

    public function viewPVAdvancePaymentsAction()
    {
        $this->getSideAndUpperMenus('Payments', 'Advance Payments', 'PURCHASING');
        $whichPayment = "PVAdv";
        $this->getPaginatedPayments($whichPayment);

        $dateFormat = $this->getUserDateFormat();
        $payments = new ViewModel(array(
            'payments' => $this->paginator,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat,
            'whichPayment'=> $whichPayment,
        ));

        $payments->setTemplate('inventory/out-going-payment/view');
        $this->getViewHelper('HeadScript')->prependFile('/js/supplierPayment/viewPayments.js');
        $this->setLogMessage('Supplier payment voucher payment list page accessed.');
        return $payments;
    }

    public function viewReceiptAction()
    {
        $paymentID = $this->params()->fromRoute('param1');
        $data = $this->_getDataForPaymentPreview($paymentID);
        $data['paymentID'] = $paymentID;
        $discount = number_format($data['cr_discount'], 2);
        $path = "/supplierPayments/document/"; //.$paymentID;
        $translator = new Translator();
        $createNew = $translator->translate('New Payment');
        $createPath = '/supplierPayments';

        $invoices = '';
        if ($data['invoices']) {
            foreach ($data['invoices'] as $inv) {
                $invPaidAmount = number_format(abs($inv["outgoingInvoiceCashAmount"] + $inv["outgoingInvoiceCreditAmount"]), 2);
                $invoices .= <<<HTML
PV No: {$inv['purchaseInvoiceCode']} <br />
PV Date: {$inv['purchaseInvoiceIssueDate']} <br />
Amount Paid: {$data['currencySymbol']}{$invPaidAmount} <br /><br />
HTML;
            }
        }
        $invoices .= '</ol>';

        $data['total'] = number_format($data['total'], 2);
        $data["email"] = array(
            "to" => $data['supplierEmail'],
            "subject" => "Receipt for your payment to " . $data['companyName'],
            "body" => <<<EMAILBODY
Dear {$data['supplierName']}, <br /><br />

Thank you for the payment. <br /><br />

A payment receipt has been generated for you from {$data['companyName']} based on your payment and attached herewith. <br /><br />

<strong>Payment No.</strong>: {$data['outgoingPaymentCode']} <br />
<strong>Date Paid</strong>: {$data['outgoingPaymentDate']} <br />
<strong>Discount</strong>: {$data['currencySymbol']}{$discount} <br />
<strong>Total Paid Amount</strong>: {$data['currencySymbol']}{$data['total']} <br /><br />

<strong>Payment Voucher(s):</strong> <br /><br />

{$invoices}


Looking forward to working with you again. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $journalEntryValues = [];
        if($this->useAccounting == 1){
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('14',$paymentID);
            $journalEntryID = $journalEntryData['journalEntryID'];
            $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryValues = $jeResult['JEDATA'];
        }

        $JEntry = new ViewModel(
            array(
               'journalEntry' => $journalEntryValues,
               'closeHidden' => true
               )
            );
        $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $documentType = 'Supplier Payment';
        $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paymentID, $createPath);
        $preview->addChild($JEntry, 'JEntry');

        $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
        $additionalButton->setTemplate('additionalButton');
        $preview->addChild($additionalButton, 'additionalButton');

        $this->setLogMessage("Preview supplier payment ".$data['outgoingPaymentCode']);
        return $preview;
    }

    public function documentPdfAction()
    {
        $paymentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Supplier Payment';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/inventory/out-going-payment/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->_getDataForPaymentPreview($paymentID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($paymentID, $documentType, $documentData, $templateID);

        return;
    }

    private function _getDataForPaymentPreview($paymentID)
    {

        if (!empty($this->_paymentViewData)) {
            return $this->_paymentViewData;
        }

        // Get company details
        $data = $this->getDataForDocumentView();


        $paymentDetails = $this->CommonTable('SupplierPaymentsTable')->getAllPaymentDetailsByID($paymentID)->current();
        $supplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($paymentDetails['supplierID']);


        $paymentDetails['outgoingPaymentDate'] = $this->convertDateToUserFormat($paymentDetails['outgoingPaymentDate']);
        $paymentDetails['createdTimeStamp'] = $this->getUserDateTime($paymentDetails['createdTimeStamp']);
        $data = array_merge($data, $paymentDetails, (array) $supplierDetails);

        // to be used by below functions - convert to object
        $paymentDetails = (object) $paymentDetails;

        // Use core date function
        $data['today_date'] = gmdate('Y-m-d');

        $displaySetup = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();
        $data['currencySymbol'] = $displaySetup['currencySymbol'];

        $invoiceDetails = $this->CommonTable('SupplierInvoicePaymentsTable')->getAllSupplierInvoiceDetailsByPaymentID($paymentID);
        $paymentmethods = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();

        while ($row = $paymentTerms->current()) {
            $id = $row->id;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->term;
        }

        $cr_payment = 0;

        while ($t = $paymentmethods->current()) {
            $t = (object) $t;
            $paymethod[] = $t;
        }
        $inv_no_list = null;

        foreach ($invoiceDetails as $row) {
            $data['invoices'][] = $row;

            if ($paymentDetails->outgoingPaymentType == "invoice") {

                $rec[] = array(
                    ($row["purchaseInvoiceCode"])? : $row["paymentVoucherCode"],
                    number_format(abs(($row["purchaseInvoiceTotal"]? : $row["paymentVoucherTotal"])), 2),
                    // $paymethod[$row["paymentMethodID"] - 1]->paymentMethodName,
                    number_format(abs($row["outgoingInvoiceCashAmount"]), 2),
                    number_format(abs($row["outgoingInvoiceCreditAmount"]), 2),
                    number_format(abs(($row["left_to_pay"])? : $row["left_to_pay_pv"]), 2),
                    number_format(abs($row["outgoingInvoiceCashAmount"] + $row["outgoingInvoiceCreditAmount"]), 2)
                );
                $inv_no_list .=$row["invoiceID"] . "&nbsp;";
            }
            $cr_payment += $row["outgoingInvoiceCreditAmount"];
        }

        if ($paymentDetails->outgoingPaymentType == "advance") {
            $rec[] = array(
                $supplierDetails->supplierCode . '-' . $supplierDetails->supplierName,
                // $paymethod[$paymentDetails->paymentMethodID - 1]->paymentMethodName,
                number_format($paymentDetails->outgoingPaymentAmount, 2)
            );
        }


        $data['type'] = "receipt";
        $data['sup_name'] = $supplierDetails->supplierName;
        $data['outgoingPaymentDate'] = $paymentDetails->outgoingPaymentDate;
        $data['paymentTermName'] = $paymentDetails->paymentTermName;
        $data['outgoingPaymentCode'] = $paymentDetails->outgoingPaymentCode;
        $data['outgoingPaymentID'] = $paymentDetails->outgoingPaymentID;
        $data['sup_data'] = array(
            "address" => $supplierDetails->supplierAddress,
        );
        if ($paymentDetails->outgoingPaymentType == "invoice") {
            $data['table'] = array(
                'col_size' => array("5", "10", "10", "10", "10", "10", "10", "10"),
                'headers' => array(
                    "PV No." => 2,
                    "PV Amount </br>" . "(" . $displaySetup['currencySymbol'] . ")" => 1,
                    // "Payment method" => 1,
                    "Amount </br>" . "(" . $displaySetup['currencySymbol'] . ")" => 1,
                    "Credit payments </br>" . "(" . $displaySetup['currencySymbol'] . ")" => 1,
                    "Left to pay </br>" . "(" . $displaySetup['currencySymbol'] . ")" => 1,
                    "total paid amount </br>" . "(" . $displaySetup['currencySymbol'] . ")" => 1,
                ),
                'records' => $rec,
            );
        } else {
            $data['table'] = array(
                'col_size' => array("5", "20", "20", "10"),
                'headers' => array(
                    "Supplier" => 2,
                    // "Payment method" => 1,
                    "Advance amount" => 1,
                ),
                'records' => $rec,
            );
        }

        $data['memo'] = $paymentDetails->outgoingPaymentMemo;
        $data['sub_total'] = $paymentDetails->outgoingPaymentAmount - $cr_payment;
        $data['cr_payment'] = $cr_payment;
        $data['cr_discount'] = $paymentDetails->outgoingPaymentDiscount;
        $data['total'] = $paymentDetails->outgoingPaymentPaidAmount;
        $data['discount'] = null;

        $data['cdnUrl'] = $this->cdnUrl;
        $data['supplierName'] = $data['supplierTitle'] . ' ' . $data['supplierName'];

        return $this->_paymentViewData = $data;
    }

    public function documentAction()
    {
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

        $documentType = 'Supplier Payment';
        $paymentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $paymentID, $templateID);

        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForPaymentPreview($paymentID);

        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);

        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
                $data['cheque_table'] = $this->_getDocumentChequeTable($paymentID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])).'-cheque'));
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($paymentID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                break;
        }

        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    private function _getDocumentDataTable($paymentID, $documentSize = 'A4')
    {

        $data_table_vars = $this->_getDataForPaymentPreview($paymentID);

        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/inventory/out-going-payment/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    private function _getDocumentChequeTable($paymentID, $documentSize = 'A4')
    {
        $data_table_vars = $this->_getDataForChequePreview($paymentID);
        $view = new ViewModel(
            array(
                'chequeDetails' => $data_table_vars
            ));
        $view->setTemplate('/inventory/out-going-payment/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    protected function _getDataForChequePreview($paymentID) {

        $data = array();
        
        $incomingPaymentChequeDetails = $this->CommonTable('SupplierPaymentsTable')->getSupplierChequesByPaymentId($paymentID);

        foreach ($incomingPaymentChequeDetails as $value) {


            if ($value['outGoingPaymentMethodID'] == '2') {
                $data[] = array('chequeNumber' => $value['outGoingPaymentMethodReferenceNumber'],
                            'chequeDate' => $value['postdatedChequeDate'],
                            'chequeAmount' => number_format(round($value['outGoingPaymentMethodPaidAmount']),2),2);
            }
            
        }

        return $data;
    
    }

    public function getPaginatedPayments($whichPayment)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('SupplierPaymentsTable')->getPaginatedPaymentsData($locationID,$whichPayment);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function getSupplierNameJson()
    {
        $sup_obj = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
        while ($row1 = $sup_obj->current()) {
            $sup_list[] = $row1->supplierName;
        }
        return json_encode($sup_list);
    }

    public function getInvoiceIdsJson()
    {
        $ids_obj = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceIds();
        while ($row = $ids_obj->current()) {
            $ids[] = $row['salesInvoiceID'];
        }
        return json_encode($ids);
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Get supplier Payments list for view by ajax
     * @return boolean|\Zend\View\Model\ViewModel
     */
    public function getSupplierPaymentsListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $whichPayment = $request->getPost('whichPayment');
            $this->getPaginatedPayments($whichPayment);
            $dateFormat = $this->getUserDateFormat();
            $paymenteview = new ViewModel(array(
                'payments' => $this->paginator,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat,
                'whichPayment'=> $whichPayment,
                'paginator'=> true
            ));
            $paymenteview->setTerminal(true);
            $paymenteview->setTemplate('inventory/out-going-payment-api/payment-list');
            return $paymenteview;
        }else{
           $this->status = false;
           $this->msg=$this->getMessage('ERR_REQUEST');
           return $this->JSONRespond();
        }
    }

    //payment method edit action in outgoing payment
    public function editAction()
    {
        $this->getSideAndUpperMenus('Payments', 'View Payments', 'PURCHASING');
        $paymentID = $this->params()->fromRoute('param1');
        $paymentData = $this->CommonTable('SupplierPaymentsTable')->getPaymentByID($paymentID);
        $paymentMethods = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPaymentMethodDetailsByPaymentId($paymentID);

        foreach ($paymentMethods as $paymethods) {
            $paymethods = (object) $paymethods;
            $dataOfPaymentMethod[$paymethods->outGoingPaymentMethodsNumbersID] = $paymethods;
        }

        if (floatval($paymentData->outgoingPaymentCreditAmount) > 0) {
            $dataOfPaymentMethod[0]->outGoingPaymentMethodPaidAmount = $paymentData->outgoingPaymentCreditAmount;
            $dataOfPaymentMethod[0]->outGoingPaymentMethodID = 0;
        }

        $paymentInvoiceMethod = $this->CommonTable('SupplierInvoicePaymentsTable')->getAllSupplierInvoiceDetailsByPaymentID($paymentID);

        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(false, array('bank.bankName' => 'ASC'));
        foreach ($banks as $t) {
            $t = (object) $t;
            $bank[$t->bankId] = $t->bankName;
        }

        $paymentEdit = new ViewModel(array(
            'paymentMethods' => $paymentInvoiceMethod,
            'dataOfPaymentMethod' => $dataOfPaymentMethod,
            'banks' => $bank,
            'paymentCode' => $paymentData->outgoingPaymentCode,
            'paymentId' => $paymentID,
            'comment' => $paymentData->outgoingPaymentMemo,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/supplierPayment/editPayments.js');
        $this->setLogMessage("Supplier payment payment method edit page accessed for payment ".$paymentData->outgoingPaymentCode.".");
        return $paymentEdit;
    }

}

?>
