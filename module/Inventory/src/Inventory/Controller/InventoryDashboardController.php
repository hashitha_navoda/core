<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class InventoryDashboardController extends CoreController
{
	protected $sideMenus = 'inventory_side_menu';
    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }


    public function indexAction()
    {
        $this->getSideAndUpperMenus('Dashboard', null, 'INVENTORY');
        
        $locationID = $this->user_session->userActiveLocation['locationID'];

        $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductsFromLocationForInventoryDashbaord($locationID);

        $locationProductData =[];
        foreach ($locationProduct as $value) {
            $temp['productCode'] = $value['productCode'];
            $temp['productName'] = $value['productName'];
            $temp['locationProductID'] = $value['locationProductID'];
            $temp['locationProductQuantity'] = floatval($value['locationProductQuantity']);
            $temp['locationProductMinInventoryLevel'] = floatval($value['locationProductMinInventoryLevel']);
            $temp['locationProductReOrderLevel'] = floatval($value['locationProductReOrderLevel']);
            $temp['qtyDiff'] = floatval($value['locationProductQuantity']) - floatval($value['locationProductMinInventoryLevel']);

            $locationProductData[$value['locationProductID']] = $temp;
        }

        $qtyDiff = array_column($locationProductData, 'qtyDiff');
        array_multisort($qtyDiff, SORT_ASC, $locationProductData);

        $finalProductArray = [];

        foreach ($locationProductData as $key => $value) {
            $poQty = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getPurchaseOrderProductByLocationProductID($value['locationProductID']);
            $finalProductArray[$value['locationProductID']] = $value;
            if ($poQty) {
                $finalProductArray[$value['locationProductID']]['poQty'] = floatval($poQty->totalQty);
                $finalProductArray[$value['locationProductID']]['toBeOrderQty'] = floatval($value['locationProductReOrderLevel']) - floatval($value['locationProductQuantity']) - floatval($poQty->totalQty);
            } else {
                $finalProductArray[$value['locationProductID']]['poQty'] = 0;
                $finalProductArray[$value['locationProductID']]['toBeOrderQty'] = floatval($value['locationProductReOrderLevel']) - floatval($value['locationProductQuantity']);
            }
        }

        $viewmodel = new ViewModel(array(
            'finalProductArray' => $finalProductArray,
        ));

        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory-dashboard.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/inventory-dashboard.js');

        return $viewmodel;
    }
}