<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Inventory Product related controller functions
 */

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Inventory\Form\ProductCategoryForm;
use Inventory\Model\ProductCategory;
use Inventory\Model\Product;
use Inventory\Model\ProductImage;
use Inventory\Model\ProductTax;
use Inventory\Model\ProductUom;
use Inventory\Model\ProductSupplier;
use Inventory\Model\ProductHandeling;
use Inventory\Model\LocationProduct;
use Inventory\Model\GoodsIssue;
use Inventory\Model\GoodsIssueProduct;
use Inventory\Model\PriceList;
use Inventory\Model\PriceListLocations;
use Inventory\Model\PriceListItems;
use Inventory\Form\ProductForm;
use Inventory\Form\UnitMeasureForm;
use Inventory\Form\CategoryForm;
use Zend\View\Model\JsonModel;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemOut;
use Inventory\Model\ItemBarcode;
use Inventory\Model\FreeIssueItems;
use Inventory\Model\FreeIssueDetails;

class ProductAPIController extends CoreController
{

    protected $userID;
    protected $user_session;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function addProductAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $result = $this->storeProduct($request->getPost());
            $this->status = $result['status'];
            $this->data = $result['data'];
            $this->msg = $result['msg'];
            return $this->JSONRespond();
        } else {
            $this->status = false;
            $this->data = null;
            $this->msg = $this->getMessage('ERR_PRDCTAPI_SAVE');
            return $this->JSONRespond();
        }
    }

    /*
    * This function is used to save product
    */
    public function storeProduct($productData, $openingBalanceFlag = false)
    {
        $productDiscountPercentage = $productData['productDiscountPercentage'];
        $productDiscountValue = $productData['productDiscountValue'];

        if ($productDiscountPercentage == "") {
            $productDiscountPercentage = null;
        }

        if ($productDiscountValue == "") {
            $productDiscountValue = null;
        }
        
        $productPurchaseDiscountPercentage = $productData['productPurchaseDiscountPercentage'];
        $productPurchaseDiscountValue = $productData['productPurchaseDiscountValue'];

        if ($productPurchaseDiscountPercentage == "") {
            $productPurchaseDiscountPercentage = null;
        }

        if ($productPurchaseDiscountValue == "") {
            $productPurchaseDiscountValue = null;
        }

        $isUseDiscountScheme = 0;

        $discountSchemeID = (!empty($productData['discountSchemeID'])) ? $productData['discountSchemeID'] : null;
        $schemeData = [];

        if (!empty($productData['useDiscountSchema']) && $productData['useDiscountSchema'] == 'true') {
            $isUseDiscountScheme = 1;
        }


        if ($isUseDiscountScheme) {
            $schemeData = $this->CommonTable('DiscountSchemeTable')->getSchemeBySchemeID($discountSchemeID);

            if ($schemeData['discountType'] == 'percentage') {
               $productDiscountPercentage = 0;
            } elseif ($schemeData['discountType'] == 'value') {
               $productDiscountValue = 0;
            }
        }

        //check accounts are inactive
        if (!empty($productData['productSalesAccountID'])) {
            $accountData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($productData['productSalesAccountID'])->current();
            if ($accountData['financeAccountStatusID'] == 2) {
                return ['status' => false, 'msg'=>$this->getMessage('ERR_SALES_ACCOUNT_INACTIVE'), 'data' => null];

            }
        }
        if (!empty($productData['productInventoryAccountID'])) {
            $accountData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($productData['productInventoryAccountID'])->current();
            if ($accountData['financeAccountStatusID'] == 2) {
                return ['status' => false, 'msg'=>$this->getMessage('ERR_INVENTORY_ACCOUNT_INACTIVE'), 'data' => null];

            }
        }
        if (!empty($productData['productCOGSAccountID'])) {
            $accountData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($productData['productCOGSAccountID'])->current();
            if ($accountData['financeAccountStatusID'] == 2) {
                return ['status' => false, 'msg'=>$this->getMessage('ERR_COST_OF_GOODS_ACCOUNT_INACTIVE'), 'data' => null];

            }
        }
        if (!empty($productData['productAdjusmentAccountID'])) {
            $accountData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($productData['productAdjusmentAccountID'])->current();
            if ($accountData['financeAccountStatusID'] == 2) {
                return ['status' => false, 'msg'=>$this->getMessage('ERR_ADJUSTMENT_ACCOUNT_INACTIVE'), 'data' => null];

            }
        }
        if (!empty($productData['productDeperciationAccountID'])) {
            $accountData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($productData['productDeperciationAccountID'])->current();
            if ($accountData['financeAccountStatusID'] == 2) {
                return ['status' => false, 'msg'=>$this->getMessage('ERR_DEP_ACCOUNT_INACTIVE'), 'data' => null];

            }
        }

        $post = [
            'productCode' => $productData['productCode'],
            'productBarcode' => $productData['productBarcode'],
            'productName' => $productData['productName'],
            'productTypeID' => $productData['productTypeID'],
            'productDescription' => $productData['productDescription'],
            'productDiscountEligible' => filter_var($productData['discountEligible'], FILTER_VALIDATE_BOOLEAN),
            'productDiscountPercentage' => $productDiscountPercentage,
            'productDiscountValue' => $productDiscountValue,
            'productPurchaseDiscountEligible' => filter_var($productData['purchaseDiscountEligible'], FILTER_VALIDATE_BOOLEAN),
            'productPurchaseDiscountPercentage' => $productPurchaseDiscountPercentage,
            'productPurchaseDiscountValue' => $productPurchaseDiscountValue,
            'productImageURL' => $productData['productImageURL'],
            'productState' => $productData['productState'],
            'categoryID' => $productData['categoryID'],
            'productGlobalProduct' => filter_var($productData['productGlobalProduct'], FILTER_VALIDATE_BOOLEAN),
            'batchProduct' => filter_var($productData['batchProduct'], FILTER_VALIDATE_BOOLEAN),
            'serialProduct' => filter_var($productData['serialProduct'], FILTER_VALIDATE_BOOLEAN),
            'hsCode' => $productData['hsCode'],
            'customDutyValue' => $productData['customDutyValue'],
            'customDutyPercentage' => $productData['customDutyPercentage'],
            'productDefaultOpeningQuantity' => $productData['productDefaultOpeningQuantity'],
            'productDefaultSellingPrice' => $productData['productDefaultSellingPrice'],
            'productDefaultMinimumInventoryLevel' => $productData['productDefaultMinimumInventoryLevel'],
            'productDefaultMaximunInventoryLevel' => $productData['productDefaultMaximunInventoryLevel'],
            'productDefaultReorderLevel' => $productData['productDefaultReorderLevel'],
            'productTaxEligible' => filter_var($productData['productTaxEligible'], FILTER_VALIDATE_BOOLEAN),
            'productDefaultPurchasePrice' => $productData['productDefaultPurchasePrice'],
            'rackID' => $productData['rackID'],
            'itemDetail' => $productData['itemDetail'],
            'productSalesAccountID' => $productData['productSalesAccountID'],
            'productInventoryAccountID' => $productData['productInventoryAccountID'],
            'productCOGSAccountID' => $productData['productCOGSAccountID'],
            'productAdjusmentAccountID' => $productData['productAdjusmentAccountID'],
            'productDepreciationAccountID' => $productData['productDeperciationAccountID'],
            'isUseDiscountScheme' => $isUseDiscountScheme,
            'discountSchemID' => $discountSchemeID,
        ];


//          If product having batch or serial product default opening quantity will be zero
        if ($post['batchProduct'] || $post['serialProduct'] || ($post['productTypeID'] == 2)) {
            $post['productDefaultOpeningQuantity'] = 0;
        }

        //get autogenarated reference number and check there availabilty.
        $productReference = $productData['productCode'];
        $refData = $this->getReferenceNoForLocation(33, 1);
        $rid = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        $autogenaratedCode = false;
        if($rid == $productReference){
            $autogenaratedCode = true;
        }

        $this->beginTransaction();

// check if a Product from the same Product Code exists
        if ($this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($productReference)) {
            if($autogenaratedCode){
                while ($productReference) {
                    if ($this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($productReference)) {
                        if ($locationReferenceID) {
                            $newProductCode = $this->getReferenceNumber($locationReferenceID);
                            if ($productReference == $newProductCode) {
                                $this->updateReferenceNumber($locationReferenceID);
                                $productReference = $this->getReferenceNumber($locationReferenceID);
                            } else {
                                $productReference = $newProductCode;
                            }
                        } else {
                            return ['status' => false, 'msg'=>$this->getMessage('ERR_JOB_CODE_EXIST'), 'data' => null];
                        }
                    } else {
                        break;
                    }
                }
            } else {
                return ['status' => false, 'msg'=>$this->getMessage('ERR_PRDCTAPI_ITEM_EXIST'), 'data' => null];
            }
        }
        $post['productCode'] = $productReference;
        if($post['productBarcode'] == ''){
            $post['productBarcode'] = $productReference;
            $productData['itemBarCodes'] = [
                [
                    "itemBarCodeID" => "new",
                    "itemBarCode" => $productReference,
                ]

            ];
        }
        //update productReference
        if($autogenaratedCode){
            $this->updateReferenceNumber($locationReferenceID);
        }

        $product = new Product();

// validate form inputs
        $form = new ProductForm();
        $form->setInputFilter($product->getInputFilter());
        $form->setValidationGroup(array('productCode', 'productBarcode', 'productName', 'productTypeID', 'categoryID', 'productDescription'));
        $form->setData($post);
        if (!$form->isValid()) {
            return ['status' => false, 'msg'=> _(array_values(array_values($form->getMessages())[0])[0]), 'data' => null];
        }

// replace array with new filtered values
        $post = array_replace($post, $form->getData());

// create entity for product
        $entityID = $this->createEntity();
        $post['entityID'] = $entityID;
        $product->exchangeArray($post);

//check if a Product from the same Product Barcode exists
        // if ($this->CommonTable('Inventory\Model\ProductTable')->getProductByBarcode($post['productBarcode'])) {
        //     return ['status' => false, 'msg'=> $this->getMessage('ERR_PRDCTAPI_BARCODE_EXIST'), 'data' => null];
        // }
        
        $addedID = $this->CommonTable('Inventory\Model\ProductTable')->saveProduct($product);
// if product was successfully added
        if ($addedID) {

            $itemBarcode = new ItemBarcode();
            foreach ($productData['itemBarCodes'] as $key => $value) {
                $getbarcodeDetail = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getBarcodeByName($value['itemBarCode'], $addedID);

                if (sizeof($getbarcodeDetail) > 0) {
                    return ['status' => false, 'msg'=> $this->getMessage('ERR_BARCODE_EXIST', array($value['itemBarCode'])), 'data' => null];
                }

                $dataSet = [
                    'productID' => $addedID,
                    'barCode' => $value['itemBarCode']
                ];
                $itemBarcode->exchangeArray($dataSet);

                $addBarCode = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->saveItemBarcode($itemBarcode);
            }

            // add expiry alerts details
            $expiryAlertsDetails = $productData['expiryAlertArray'];
            if (count($expiryAlertsDetails) > 0) {
                foreach ($expiryAlertsDetails as $exAlerts) {
                    $addedExpiryAlertID = $this->CommonTable('Inventory\Model\ExpiryAlertNotificationTable')->addAlerts($addedID, $exAlerts);
                }
                //this use to add zero value to the expireAlertNotificationTable
                $addedExpiryAlertID = $this->CommonTable('Inventory\Model\ExpiryAlertNotificationTable')->addAlerts($addedID, 0);
            }

            $itemAttrValue = $productData['itemAttrValue'];
            $itemAttrValue = ($itemAttrValue) ? $itemAttrValue : [];
            //add item attribute details
            $itemAttrDetails = array_filter($itemAttrValue);

            foreach ($itemAttrDetails as $itemAttrkey => $itemAttrValue) {
                $attrData = [];
                $attrData = array(
                    'itemAttributeID' => $itemAttrkey,
                    'productID' => $addedID,
                    );

                $savedMapID = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->saveItemAttributeMap($attrData);
                if($savedMapID){
                    foreach (array_filter($itemAttrValue) as $key => $itemAttrSubValue) {
                        $attrValueData = [];
                        $attrValueData = array(
                            'itemAttributeMapID' => $savedMapID,
                            'itemAttributeValueID' => $itemAttrSubValue,
                        );
                        $attrValueMapID = $this->CommonTable('Settings\Model\ItemAttributeValueMapTable')->saveItemAttributeValueMap($attrValueData);
                    }
                }
            }


            $handeling = (is_array($productData['handeling'])) ? $productData['handeling'] : array();
            if (count($handeling) > 0) {

                $data = array(
                    'productID' => $addedID,
                    'productHandelingManufactureProduct' => (in_array('productHandelingManufactureProduct', $handeling)) ? 1 : 0,
                    'productHandelingPurchaseProduct' => (in_array('productHandelingPurchaseProduct', $handeling)) ? 1 : 0,
                    'productHandelingSalesProduct' => (in_array('productHandelingSalesProduct', $handeling)) ? 1 : 0,
                    'productHandelingConsumables' => (in_array('productHandelingConsumables', $handeling)) ? 1 : 0,
                    'productHandelingFixedAssets' => (in_array('productHandelingFixedAssets', $handeling)) ? 1 : 0,
                );
                $productHandeling = new ProductHandeling();
                $productHandeling->exchangeArray($data);


                if (!$this->CommonTable('Inventory\Model\ProductHandelingTable')->saveProductHandeling($productHandeling)) {
                    return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMHANDLE'));
                }
            } else {
                return ['status' => false, 'msg'=> $this->getMessage('ERR_PRDCTAPI_TYPE'), 'data' => null];
            }

            $this->CommonTable('Inventory\Model\ProductTaxTable')->deleteProductTax($addedID);
            if ($post['productTaxEligible']) {
                $taxes = (is_array($productData['taxes'])) ? $productData['taxes'] : array();
                foreach ($taxes as $taxID) {
                    $data = array(
                        'taxID' => $taxID,
                        'productID' => $addedID,
                    );
                    $productTax = new ProductTax();
                    $productTax->exchangeArray($data);

                    if (!$this->CommonTable('Inventory\Model\ProductTaxTable')->saveProductTax($productTax)) {
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_NOTSAVE')); //'Item was not saved due to an error which occured while saving Item Taxes. Please try again.');
                    }
                }
            }

            $this->CommonTable('Inventory\Model\ProductSupplierTable')->deleteProductSupplierByProductID($addedID);
            $suppliers = (is_array($productData['suppliers'])) ? $productData['suppliers'] : array();
            foreach ($suppliers as $supplierID) {
                $data = array(
                    'productID' => $addedID,
                    'supplierID' => $supplierID,
                );
                $productSupplier = new ProductSupplier();
                $productSupplier->exchangeArray($data);

                if (!$this->CommonTable('Inventory\Model\ProductSupplierTable')->saveProductSupplier($productSupplier)) {
                    return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_SAVEITEMSUP')); //'Item was not saved due to an error which occured while saving Item Suppliers. Please try again.');
                }
            }

            $locations = array();
// if location type is global, get all locations
            if ($productData['productGlobalProduct'] == 1) {
                $locationsArr = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
                foreach ($locationsArr as $l) {
                    $l = (object) $l;
                    $locations[] = $l->locationID;
                }
            } else {
                $locations = (is_array($productData['locations'])) ? $productData['locations'] : array();
            }
            foreach ($locations as $locationID) {
                $entityID = $this->createEntity();
                $data = array(
                    'productID' => $addedID,
                    'locationID' => $locationID,
                    'entityID' => $entityID,
                );
                $locationIDs[] = $locationID;
// add product discounts to locations
                $data['locationDiscountPercentage'] = $productDiscountPercentage;
                $data['locationDiscountValue'] = $productDiscountValue;
                $data['defaultSellingPrice'] = $post['productDefaultSellingPrice'];
                $data['locationProductMinInventoryLevel'] = $post['productDefaultMinimumInventoryLevel'];
                $data['locationProductMaxInventoryLevel'] = $post['productDefaultMaximunInventoryLevel'];
                $data['locationProductReOrderLevel'] = $post['productDefaultReorderLevel'];
                $data['defaultPurchasePrice'] = $post['productDefaultPurchasePrice'];
                $data['isUseDiscountSchemeForLocation'] = $isUseDiscountScheme;
                $data['locationDiscountSchemID'] = $discountSchemeID;

                $locationProduct = new LocationProduct();
                $locationProduct->exchangeArray($data);

                if (!$this->CommonTable('Inventory\Model\LocationProductTable')->saveLocationProduct($locationProduct)) {
                    return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMLOC'));
                }
            }

            $uom = (is_array($productData['uom'])) ? $productData['uom'] : array();
            $displayUoms = $productData['displayUoms'];
            foreach ($uom as $uomID => $uomValue) {
                $uomValue = (object) $uomValue;
                if ($uomID == $displayUoms) {
                    $displayUom = 1;
                } else {
                    $displayUom = 0;
                }
                $data = array(
                    'productID' => $addedID,
                    'uomID' => $uomID,
                    'productUomConversion' => $uomValue->uomConversion,
                    'productUomDisplay' => $displayUom,
                    'productUomBase' => ($uomValue->baseUom == 'true') ? 1 : 0,
                );
                $productUom = new ProductUom();
                $productUom->exchangeArray($data);
                if (!$this->CommonTable('Inventory\Model\ProductUomTable')->saveProductUom($productUom)) {
                    return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMUOM'));
                }
            }
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $locProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByProIdLocId($addedID, $locationID);

            $this->commit();

            $eventParameter = ['productIDs' => [$addedID], 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            $this->setLogMessage('Item '.$productData['productCode'].' is created');
            if ($productData['flag'] != 'invoice' && $openingBalanceFlag == false) {
                $this->flashMessenger()->addMessage($this->getMessage('SUCC_PRDCTAPI_ADD'));
            }
            return ['status' => true, 'msg'=> $this->getMessage('SUCC_PRDCTAPI_ADD'), 'data' =>  array('productID' => $addedID, 'locationProductData' => $locProductData)];
        } else {
            return ['status' => false, 'msg'=> $this->getMessage('ERR_PRDCTAPI_SAVE'), 'data' => null];
        }
    }

    public function saveProductAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

// check if a Product from the same Product Code exists
            $existingProduct = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($request->getPost('productCode'));
            if ($existingProduct['productTypeID'] == "2") {
                if ($existingProduct['productTypeID'] != $request->getPost('productTypeID')) {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERROR_PRODUCT_TYPE_SAVE_NON_INV');
                    return $this->JSONRespond();
                }
            }

            if ($existingProduct && ($existingProduct['productID'] != $request->getPost('productID'))) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_ITEM_CODE_EXIST');
                return $this->JSONRespond();
            }

//check wheater discount value is more tahan the default selling price
            if ($request->getPost('productDiscountValue') != 0) {
                $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationsByProductID($existingProduct['productID']);
                foreach ($locationProduct as $product) {
                    $product = (object) $product;
                    if ($product->defaultSellingPrice != 0) {
                        if ($request->getPost('productDiscountValue') >= $product->defaultSellingPrice) {
                            $location = $this->CommonTable('Core\Model\LocationTable')->getLocation($product->locationID);
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PRDCTAPI_DISCOUNT', array($location->locationName));
                            return $this->JSONRespond();
                        }
                    }
                }
            }
            
            $productDiscountPercentage = $request->getPost('productDiscountPercentage');
            $productDiscountValue = $request->getPost('productDiscountValue');



            if ($productDiscountPercentage == "") {
                $productDiscountPercentage = null;
            }

            if ($productDiscountValue == "") {
                $productDiscountValue = null;
            }
            
            $productPurchaseDiscountPercentage = $request->getPost('productPurchaseDiscountPercentage');
            $productPurchaseDiscountValue = $request->getPost('productPurchaseDiscountValue');

            if ($productPurchaseDiscountPercentage == "") {
                $productPurchaseDiscountPercentage = null;
            }

            if ($productPurchaseDiscountValue == "") {
                $productPurchaseDiscountValue = null;
            }


            // get current details of product
            $existingProduct = ($existingProduct) ? $existingProduct : $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($request->getPost('productID'));

            $isUseDiscountScheme = 0;
            $discountSchemeID = (!empty($request->getPost('discountSchemeID'))) ? $request->getPost('discountSchemeID') : null;
            $schemeData = [];

            if (!empty($request->getPost('useDiscountSchema')) && $request->getPost('useDiscountSchema') == 'true') {
                $isUseDiscountScheme = 1;
            }


            if ($isUseDiscountScheme) {
                if ($request->getPost('discountSchemID') != $discountSchemeID) {
                    $schemeData = $this->CommonTable('DiscountSchemeTable')->getSchemeBySchemeID($discountSchemeID);

                    if ($schemeData['discountType'] == 'percentage') {
                       $productDiscountPercentage = 0;
                       $productDiscountValue = null;
                    } elseif ($schemeData['discountType'] == 'value') {
                       $productDiscountValue = 0;
                       $productDiscountPercentage = null;
                    }
                }
            }

            $post = [
                'productID' => $request->getPost('productID'),
                'productBarcode' => $request->getPost('productBarcode'),
                'productCode' => $request->getPost('productCode'),
                'productName' => $request->getPost('productName'),
                'productTypeID' => $request->getPost('productTypeID'),
                'productDescription' => $request->getPost('productDescription'),
                'productDiscountEligible' => filter_var($request->getPost('discountEligible'), FILTER_VALIDATE_BOOLEAN),
                'productDiscountPercentage' => $productDiscountPercentage,
                'productDiscountValue' => $productDiscountValue,
                'productPurchaseDiscountEligible' => filter_var($request->getPost('purchaseDiscountEligible'), FILTER_VALIDATE_BOOLEAN),
                'productPurchaseDiscountPercentage' => $productPurchaseDiscountPercentage,
                'productPurchaseDiscountValue' => $productPurchaseDiscountValue,
                'productImageURL' => $request->getPost('productImageURL'),
                'productState' => $request->getPost('productState'),
                'categoryID' => $request->getPost('categoryID'),
                'productGlobalProduct' => filter_var($request->getPost('productGlobalProduct'), FILTER_VALIDATE_BOOLEAN),
                'batchProduct' => filter_var($request->getPost('batchProduct'), FILTER_VALIDATE_BOOLEAN),
                'serialProduct' => filter_var($request->getPost('serialProduct'), FILTER_VALIDATE_BOOLEAN),
                'hsCode' => $request->getPost('hsCode'),
                'customDutyValue' => $request->getPost('customDutyValue'),
                'customDutyPercentage' => $request->getPost('customDutyPercentage'),
                'productDefaultSellingPrice' => $request->getPost('productDefaultSellingPrice'),
                'productDefaultPurchasePrice' => $request->getPost('productDefaultPurchasePrice'),
                'productDefaultMinimumInventoryLevel' => $request->getPost('productDefaultMinimumInventoryLevel'),
                'productDefaultMaximunInventoryLevel' => $request->getPost('productDefaultMaximunInventoryLevel'),
                'productDefaultReorderLevel' => $request->getPost('productDefaultReorderLevel'),
                'productDefaultOpeningQuantity' => $request->getPost('productDefaultOpeningQuantity'),
                'productTaxEligible' => filter_var($request->getPost('productTaxEligible'), FILTER_VALIDATE_BOOLEAN),
                'rackID' => $request->getPost('rackID'),
                'itemDetail' => $request->getPost('itemDetail'),
                'productSalesAccountID' => $request->getPost('productSalesAccountID'),
                'productInventoryAccountID' => $request->getPost('productInventoryAccountID'),
                'productCOGSAccountID' => $request->getPost('productCOGSAccountID'),
                'productAdjusmentAccountID' => $request->getPost('productAdjusmentAccountID'),
                'productDepreciationAccountID' => $request->getPost('productDeperciationAccountID'),
                'isUseDiscountScheme' => $isUseDiscountScheme,
                'discountSchemID' => $discountSchemeID,
            ];

//            If product having batch or serial product default opening quantity will be zero
            if ($post['batchProduct'] || $post['serialProduct'] || ($post['productTypeID'] == 2)) {
                $post['productDefaultOpeningQuantity'] = 0;
            }

            $this->beginTransaction();

            $product = new Product();
// validate form inputs
            $form = new ProductForm();
            $form->setInputFilter($product->getInputFilter());
            $form->setValidationGroup(array('productCode', 'productBarcode', 'productName', 'productTypeID', 'categoryID', 'productDescription'));
            $form->setData($post);
            if (!$form->isValid()) {
                $this->status = false;
                $this->msg = _(array_values(array_values($form->getMessages())[0])[0]);
                return $this->JSONRespond();
            }

// replace array with new filtered values
            $post = array_replace($post, $form->getData());
            $product->exchangeArray($post);

            if(empty($product->productBarcode)){
                $product->productBarcode = $product->productCode;
            }
// check barcode already exists
            // $isExistsbarcode = $this->CommonTable('Inventory\Model\ProductTable')->getProductByBarcode($request->getPost('productBarcode'));
            // if ($isExistsbarcode && ($isExistsbarcode['productCode'] != $request->getPost('productCode'))) {
            //     $this->status = false;
            //     $this->msg = $this->getMessage('ERR_PRDCTAPI_BARCODE_EXIST');
            //     return $this->JSONRespond();
            // }

            $success = $this->CommonTable('Inventory\Model\ProductTable')->updateProduct($product);

// if product was successfully added
            if ($success) {
                $this->updateEntity($existingProduct["entityID"]);
                $itemBarcode = new ItemBarcode();
                $newItemBarcodeIDs = [];
                foreach ($request->getPost('itemBarCodes') as $key => $value) {
                    if ($value['itemBarCodeID'] == 'new') {
                        $getbarcodeDetail = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getBarcodeByName($value['itemBarCode'], $request->getPost('productID'));

                        if (sizeof($getbarcodeDetail) > 0) {
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_BARCODE_EXIST', array($value['itemBarCode']));
                            return $this->JSONRespond();
                        }

                        $dataSet = [
                            'productID' => $request->getPost('productID'),
                            'barCode' => $value['itemBarCode']
                        ];

                        $itemBarcode->exchangeArray($dataSet);

                        $addBarCodeID = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->saveItemBarcode($itemBarcode);
                        $newItemBarcodeIDs[] = $addBarCodeID;
                    } else{
                        $newItemBarcodeIDs[] = $value['itemBarCodeID'];
                    }

                }


                $getProductBarcodes = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getRelatedBarCodesByProductID($request->getPost('productID'));

                foreach ($getProductBarcodes as $key => $val) {
                    if (!in_array($val['itemBarcodeID'], $newItemBarcodeIDs)) {
                        $deleteBarCode = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->deleteBarCodeByItemBarCodeID($val['itemBarcodeID']);
                    }
                }


                // update item attribute and item-attributes values.
                // before update delete all related records from table
                $itemAttrValueMapDetails = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->getDetailsByProductID($request->getPost('productID'));
                foreach ($itemAttrValueMapDetails as $key => $value) {
                    $delAttrMap = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->deleteMapDetailsByID($value['itemAttributeMapID']);
                    $delAttrValueMAp = $this->CommonTable('Settings\Model\ItemAttributeValueMapTable')->deleteMapDetailsByitemAttrValueMapID($value['itemAttributeValueMapID']);
                }

                $itemAttrDetails = array_filter($request->getPost('itemAttrValue'));
                foreach ($itemAttrDetails as $itemAttrkey => $itemAttrValue) {
                    $attrData = [];
                    $attrData = array(
                        'itemAttributeID' => $itemAttrkey,
                        'productID' => $request->getPost('productID'),
                        );

                    $savedMapID = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->saveItemAttributeMap($attrData);
                    if($savedMapID){
                        foreach (array_filter($itemAttrValue) as $key => $itemAttrSubValue) {
                            $attrValueData = [];
                            $attrValueData = array(
                                'itemAttributeMapID' => $savedMapID,
                                'itemAttributeValueID' => $itemAttrSubValue,
                            );
                            $attrValueMapID = $this->CommonTable('Settings\Model\ItemAttributeValueMapTable')->saveItemAttributeValueMap($attrValueData);
                        }
                    }
                }

                $handeling = (is_array($request->getPost('handeling'))) ? $request->getPost('handeling') : array();
                if (count($handeling) > 0) {
                    $data = array(
                        'productID' => $existingProduct['productID'],
                        'productHandelingManufactureProduct' => (in_array('productHandelingManufactureProduct', $handeling)) ? 1 : 0,
                        'productHandelingPurchaseProduct' => (in_array('productHandelingPurchaseProduct', $handeling)) ? 1 : 0,
                        'productHandelingSalesProduct' => (in_array('productHandelingSalesProduct', $handeling)) ? 1 : 0,
                        'productHandelingConsumables' => (in_array('productHandelingConsumables', $handeling)) ? 1 : 0,
                        'productHandelingFixedAssets' => (in_array('productHandelingFixedAssets', $handeling)) ? 1 : 0,
                    );
                    $productHandeling = new ProductHandeling();
                    $productHandeling->exchangeArray($data);

                    if ($this->CommonTable('Inventory\Model\ProductHandelingTable')->checkProductHandeling($existingProduct['productID'])) {
                        if (!$this->CommonTable('Inventory\Model\ProductHandelingTable')->updateProductHandeling($productHandeling)) {
                            return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMHANDLTYPE'));
                        }
                    } else {
                        if (!$this->CommonTable('Inventory\Model\ProductHandelingTable')->saveProductHandeling($productHandeling)) {
                            return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMHANDLTYPE'));
                        }
                    }
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PRDCTAPI_TYPE');
                    return $this->JSONRespond();
                }

//update tax
                if ($post['productTaxEligible']) {
                    $addTaxes = (is_array($request->getPost('taxes'))) ? $request->getPost('taxes') : array();
                    $deleteTaxes = array();

                    $productCurrTaxes = $this->CommonTable('Inventory\Model\ProductTaxTable')->getProductTaxByProductID($existingProduct['productID']);
                    foreach ($productCurrTaxes as $pt) {
                        $pt = (array) $pt;
                        if (in_array($pt['taxID'], $addTaxes)) {
                            unset($addTaxes[array_search($pt['taxID'], $addTaxes)]);
                        } else {
                            $deleteTaxes[] = $pt['taxID'];
                        }
                    }
                    foreach ($addTaxes as $taxID) {
                        $data = array(
                            'productID' => $existingProduct['productID'],
                            'taxID' => $taxID,
                        );
                        $productTax = new ProductTax();
                        $productTax->exchangeArray($data);

                        if (!$this->CommonTable('Inventory\Model\ProductTaxTable')->saveProductTax($productTax)) {
                            return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_NOTSAVE'));
                        }
                    }

                    foreach ($deleteTaxes as $taxID) {

                        if (!$this->CommonTable('Inventory\Model\ProductTaxTable')->deleteProductTaxByProductIDAndTaxID($existingProduct['productID'], $taxID)) {
                            return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMTAX'));
                        }
                    }
                }

// update suppliers
                $addSuppliers = (is_array($request->getPost('suppliers'))) ? $request->getPost('suppliers') : array();
                $deleteSuppliers = array();

                $productCurrSuppliers = $this->CommonTable('Inventory\Model\ProductSupplierTable')->getProductSupplierByProductID($post['productID']);
                foreach ($productCurrSuppliers as $ps) {
                    if (in_array($ps['supplierID'], $addSuppliers)) {
                        unset($addSuppliers[array_search($ps['supplierID'], $addSuppliers)]);
                    } else {
                        $deleteSuppliers[] = $ps['supplierID'];
                    }
                }

                foreach ($addSuppliers as $supplierID) {
                    $data = array(
                        'productID' => $existingProduct['productID'],
                        'supplierID' => $supplierID,
                    );
                    $productSupplier = new ProductSupplier();
                    $productSupplier->exchangeArray($data);

                    if (!$this->CommonTable('Inventory\Model\ProductSupplierTable')->saveProductSupplier($productSupplier)) {
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_SAVEITEMSUP'));
                    }
                }

                foreach ($deleteSuppliers as $supplierID) {

                    if (!$this->CommonTable('Inventory\Model\ProductSupplierTable')->deleteProductSupplierByProductIDAndSupplierID($existingProduct['productID'], $supplierID)) {
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_DELEITEMSUP'));
                    }
                }

// update locations
                $addLocations = array();
                $deleteLocations = array();
                $updateLocations = array();

// if location type is global, get all locations
                if ($request->getPost('productGlobalProduct') == 1) {
                    $locationsArr = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
                    foreach ($locationsArr as $l) {
                        $l = (object) $l;
                        $addLocations[] = $l->locationID;
                    }
                } else {
                    $addLocations = (is_array($request->getPost('locations'))) ? $request->getPost('locations') : array();
                }

                $productCurrLocations = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationsByProductID($existingProduct['productID'], true);
                foreach ($productCurrLocations as $p) {
                    if (in_array($p['locationID'], $addLocations)) {
                        unset($addLocations[array_search($p['locationID'], $addLocations)]);
                        $updateLocations[] = $p;

// if previously deleted, consider it as a 'disabled' locproduct and reenable it
                        if ($p['deleted'] == 1) {
                            $this->revertSoftDelete($p['entityID']);
                        }
                    } else {
                        $deleteLocations[] = $p['entityID'];
                    }
                }

                foreach ($addLocations as $locationID) {
                    $locationEntityID = $this->createEntity();
                    $data = array(
                        'productID' => $existingProduct['productID'],
                        'locationID' => $locationID,
                        'entityID' => $locationEntityID,
                    );

// add product discounts to locations
                    $data['locationDiscountPercentage'] = $post['productDiscountPercentage'];
                    $data['locationDiscountValue'] = $post['productDiscountValue'];
                    $data['defaultSellingPrice'] = $post['productDefaultSellingPrice'];
                    $data['locationProductMinInventoryLevel'] = $post['productDefaultMinimumInventoryLevel'];
                    $data['locationProductMaxInventoryLevel'] = $post['productDefaultMaximunInventoryLevel'];
                    $data['locationProductReOrderLevel'] = $post['productDefaultReorderLevel'];
                    $data['isUseDiscountSchemeForLocation'] = $isUseDiscountScheme;
                    $data['locationDiscountSchemID'] = $discountSchemeID;

                    $locationProduct = new LocationProduct();
                    $locationProduct->exchangeArray($data);

                    if (!$this->CommonTable('Inventory\Model\LocationProductTable')->saveLocationProduct($locationProduct)) {
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMLOC'));
                    }
                }

// if product discount is set, apply it to all its locations as well
                if (isset($post['productDiscountPercentage']) || isset($post['productDiscountValue'])) {
                    foreach ($updateLocations as $lp) {
                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($lp['locationProductID']);
                        $this->updateEntity($currentPQty->entityID);
                        $data = array(
                            'locationProductID' => $lp['locationProductID'],
                            'locationDiscountValue' => $post['productDiscountValue'],
                            'locationDiscountPercentage' => $post['productDiscountPercentage'],
                        );

                        $locationProduct = new LocationProduct();
                        $locationProduct->exchangeArray($data);

                        if (!$this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductDiscount($locationProduct)) {
                            return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMLOC'));
                        }
                    }
                }

                foreach ($deleteLocations as $locationEntityID) {
                    if (!$this->updateDeleteInfoEntity($locationEntityID)) {
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_DELEITEMLOC'));
                    }
                }

// update UOMs
                $addUoms = (is_array($request->getPost('uom'))) ? $request->getPost('uom') : array();
                $deleteUoms = array();
                $updateUoms = array();
                $displayUoms = $request->getPost('displayUoms');

                $productCurrUoms = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomByProductID($existingProduct['productID']);
                foreach ($productCurrUoms as $u) {
                    if (isset($addUoms[$u['uomID']])) {
                        $updateUoms[$u['uomID']] = $addUoms[$u['uomID']];
                        unset($addUoms[$u['uomID']]);
                    } else {
                        $deleteUoms[] = $u['uomID'];
                    }
                }

                foreach ($addUoms as $uomID => $uomValue) {
                    $uomValue = (object) $uomValue;
                    if ($uomID == $displayUoms) {
                        $displayUom = 1;
                    } else {
                        $displayUom = 0;
                    }
                    $data = array(
                        'productID' => $existingProduct['productID'],
                        'uomID' => $uomID,
                        'productUomConversion' => $uomValue->uomConversion,
                        'productUomDisplay' => $displayUom,
                        'productUomBase' => ($uomValue->baseUom == 'true') ? 1 : 0,
                    );
                    $productUom = new ProductUom();
                    $productUom->exchangeArray($data);

                    if (!$this->CommonTable('Inventory\Model\ProductUomTable')->saveProductUom($productUom)) {
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMUOM'));
                    }
                }

                foreach ($updateUoms as $uomID => $uomValue) {
                    $uomValue = (object) $uomValue;
                    if ($uomID == $displayUoms) {
                        $displayUom = 1;
                    } else {
                        $displayUom = 0;
                    }
                    $data = array(
                        'productID' => $existingProduct['productID'],
                        'uomID' => $uomID,
                        'productUomConversion' => $uomValue->uomConversion,
                        'productUomDisplay' => $displayUom,
                        'productUomBase' => ($uomValue->baseUom == 'true') ? 1 : 0,
                    );
                    $productUom = new ProductUom();
                    $productUom->exchangeArray($data);

                    if (!$this->CommonTable('Inventory\Model\ProductUomTable')->updateProductUom($productUom)) {
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_UPADTEUOM'));
                    }
                }

                foreach ($deleteUoms as $uomID) {

                    if (!$this->CommonTable('Inventory\Model\ProductUomTable')->deleteProductUomByUomID($existingProduct['productID'], $uomID)) {
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_DELEITEMUOM'));
                    }
                }

//update expire alerts
                $expiryAlertsDetails = $request->getPost('expiryAlertArray');
                if (count($expiryAlertsDetails) > 0) {
                    //then first need to delete existing records
                    $status = $this->CommonTable('Inventory\Model\ExpiryAlertNotificationTable')->deleteDetailsByProductID($request->getPost('productID'));
                    if ($status) {
                        foreach ($expiryAlertsDetails as $exAlerts) {
                            $addedExpiryAlertID = $this->CommonTable('Inventory\Model\ExpiryAlertNotificationTable')->addAlerts($request->getPost('productID'), $exAlerts);
                        }
                    }
                }



//              call update quantity event
                $productLocations = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsByProductID($existingProduct['productID']);
                $locationIDs = array_map(function($element) {
                    return $element['locationID'];
                }, iterator_to_array($productLocations));

                $previousData = [];
                $previousData['Product Code'] = $existingProduct['productCode'];
                $previousData['Product Barcode'] = $existingProduct['productBarcode'];
                $previousData['Product Name'] = $existingProduct['productName'];
                $previousData['Product Type'] = ($existingProduct['productTypeID'] == 1) ? "Inventory" : "Non Inventory";
                $previousData['Product Description'] = $existingProduct['productDescription'];
                $previousData['Product Sales Discount Eligible'] = ($existingProduct['productDiscountEligible'] == '0') ? 'Non Eligible' : 'Eligible';
                $previousData['Sales Discount Percentage'] = $existingProduct['productDiscountPercentage'];
                $previousData['Sales Discount Value'] = $existingProduct['productDiscountValue'];
                $previousData['Product Purchase Discount Eligible'] = ($existingProduct['productPurchaseDiscountEligible'] == '0') ? 'Non Eligible' : 'Eligible';
                $previousData['Purchase Discount Percentage'] = $existingProduct['productPurchaseDiscountPercentage'];
                $previousData['Purchase Discount Value'] = $existingProduct['productPurchaseDiscountValue'];
                $previousData['Default Opening Quantity'] = $existingProduct['productDefaultOpeningQuantity'];
                $previousData['Default Selling Price'] = $existingProduct['productDefaultSellingPrice'];
                $previousData['Product Default Purchase Price'] = $existingProduct['productDefaultPurchasePrice'];
                $previousData['Product Default Minimum Inventory Level'] = $existingProduct['productDefaultMinimumInventoryLevel'];
                $previousData['Product Default Reorder Level'] = $existingProduct['productDefaultReorderLevel'];
                $previousData['Product Tax Eligible'] = ($existingProduct['productTaxEligible'] == '0') ? 'Non Eligible' : 'Eligible';



                $this->commit();

                $changeArray = $this->getProductChageDetails($request->getPost());
                $pCode = $previousData['Product Code'];
                $previousData = json_encode($previousData);
                $newData = json_encode($changeArray['newData']);

                $this->setLogDetailsArray($previousData,$newData);
                $this->setLogMessage("Product ".$pCode." is updated.");
                $this->status = true;
                $this->data = array('productID' => $existingProduct['productID']);
                $this->msg = $this->getMessage('SUCC_PRDCTAPI_UPDATE');
                $this->flashMessenger()->addMessage($this->msg);
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('SUCC_PRDCTAPI_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

    public function getProductChageDetails($data)
    {
        $existingProduct = ($existingProduct) ? $existingProduct : $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($data['productID']);
        $previousData = [];
        $previousData['Product Code'] = $existingProduct['productCode'];
        $previousData['Product Barcode'] = $existingProduct['productBarcode'];
        $previousData['Product Name'] = $existingProduct['productName'];
        $previousData['Product Type'] = ($existingProduct['productTypeID'] == 1) ? "Inventory" : "Non Inventory";
        $previousData['Product Description'] = $existingProduct['productDescription'];
        $previousData['Product Sales Discount Eligible'] = ($existingProduct['productDiscountEligible'] == '0') ? 'Non Eligible' : 'Eligible';
        $previousData['Sales Discount Percentage'] = $existingProduct['productDiscountPercentage'];
        $previousData['Sales Discount Value'] = $existingProduct['productDiscountValue'];
        $previousData['Product Purchase Discount Eligible'] = ($existingProduct['productPurchaseDiscountEligible'] == '0') ? 'Non Eligible' : 'Eligible';
        $previousData['Purchase Discount Percentage'] = $existingProduct['productPurchaseDiscountPercentage'];
        $previousData['Purchase Discount Value'] = $existingProduct['productPurchaseDiscountValue'];
        $previousData['Default Opening Quantity'] = $existingProduct['productDefaultOpeningQuantity'];
        $previousData['Default Selling Price'] = $existingProduct['productDefaultSellingPrice'];
        $previousData['Product Default Purchase Price'] = $existingProduct['productDefaultPurchasePrice'];
        $previousData['Product Default Minimum Inventory Level'] = $existingProduct['productDefaultMinimumInventoryLevel'];
        $previousData['Product Default Reorder Level'] = $existingProduct['productDefaultReorderLevel'];
        $previousData['Product Tax Eligible'] = ($existingProduct['productTaxEligible'] == '0') ? 'Non Eligible' : 'Eligible';

        $newData = [];
        $newData['Product Code'] = $data['productCode'];
        $newData['Product Barcode'] = $data['productBarcode'];
        $newData['Product Name'] = $data['productName'];
        $newData['Product Type'] = ($data['productTypeID'] == 1) ? "Inventory" : "Non Inventory";
        $newData['Product Description'] = $data['productDescription'];
        $newData['Product Sales Discount Eligible'] = ($data['discountEligible'] == 'true') ? 'Eligible' : 'Non Eligible';
        $newData['Sales Discount Percentage'] = $data['productDiscountPercentage'];
        $newData['Sales Discount Value'] = $data['productDiscountValue'];
        $newData['Product Purchase Discount Eligible'] = ($data['purchaseDiscountEligible'] == 'true') ? 'Eligible' : 'Non Eligible';
        $newData['Purchase Discount Percentage'] = $data['productPurchaseDiscountPercentage'];
        $newData['Purchase Discount Value'] = $data['productPurchaseDiscountValue'];
        $newData['Default Opening Quantity'] = $data['productDefaultOpeningQuantity'];
        $newData['Default Selling Price'] = $data['productDefaultSellingPrice'];
        $newData['Product Default Purchase Price'] = $data['productDefaultPurchasePrice'];
        $newData['Product Default Minimum Inventory Level'] = $data['productDefaultMinimumInventoryLevel'];
        $newData['Product Default Reorder Level'] = $data['productDefaultReorderLevel'];
        $newData['Product Tax Eligible'] = ($data['productTaxEligible'] == 'true') ? 'Eligible' : 'Non Eligible';

        return array('previousData' => $previousData, 'newData' => $newData);
    }

    public function previewAction()
    {
        $result = $this->CommonTable('Inventory\Model\ProductImageTable')->getImageNameByProductID($_POST['ID']);

//remove any image file that already exists with the same name
        $previewImg = $this->getCompanyDataFolder() . "/product/thumbs/" . $result[0]['productImageName']; //get existing preview image  name
        $imageFile = explode("_preview.", $result[0]['productImageName']);
        $originalImage = $this->getCompanyDataFolder() . "/product/" . $imageFile[0] . "." . $imageFile[1]; //get existing original image name
//
//delete existing preview image
        if (file_exists($previewImg)) {
            @unlink($previewImg);
            $this->CommonTable('Inventory\Model\ProductImageTable')->deleteImageDetails($_POST['ID']);
        }
//delete existing original image
        if (file_exists($originalImage)) {
            @unlink($originalImage);
        }

        $imgname = $this->previewImage("/product", $_POST['ID']);
        $data = array(
            'productImageName' => $imgname,
            'productID' => $_POST['ID'],
        );
        $prImage = new ProductImage();
        $prImage->exchangeArray($data);
        $insertedID = $this->CommonTable('Inventory\Model\ProductImageTable')->saveImageDetails($prImage);
        $imgUrl = array(
            'productImageURL' => $imgname,
        );
        $this->CommonTable('Inventory\Model\ProductTable')->updateImageUrl($imgUrl, $_POST['ID']);


        return new JsonModel(array($imgname));
    }

    public function addProductLocationDetailsAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $this->beginTransaction();
            $productIDs = [];
            $locationIDs = [];
            foreach ($request->getPost('productData') as $locationProductID => $p) {

                $data = array(
                    'locationProductID' => $locationProductID,
                    'locationProductMinInventoryLevel' => $p['minInvLevel'],
                    'locationProductReOrderLevel' => $p['reorderLevel'],
                    'defaultSellingPrice' => $p['sellingPrice'],
                    'locationDiscountPercentage' => ($p['discountPercentage'] != "") ? floatval($p['discountPercentage']) : null,
                    'locationDiscountValue' => ($p['discountValue'] != "") ? floatval($p['discountPercentage']) : null
                );

                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);
                $productIDs[] = $locationProductData->productID;
                $locationIDs[] = $locationProductData->locationID;

                $locationProduct = new LocationProduct();
                $locationProduct->exchangeArray($data);

                if (!$this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProduct($locationProduct)) {
                    return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ERRORSAVEITEM'));
                }
            }
//            call product quantity updated  events
            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => $locationIDs];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            $this->commit();

            $this->status = true;
            $this->msg = $this->getMessage('SUCC_PRDCTAPI_DETAILUPDATE');
            return $this->JSONRespond();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_PRDCTAPI_SAVE');
            return $this->JSONRespond();
        }
    }

    public function addLocationAllProductDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productID = $request->getPost('productID');
            $discountPercentages = $request->getPost('discountPercentage');
            $discountValues = $request->getPost('discountValue');
            $minInvLevel = $request->getPost('minInvLevel');
            $maxInvLevel = $request->getPost('maxInvLevel');
            $openingLPQ = $request->getPost('openingLPQ');
            $reorderLevels = $request->getPost('reorderLevel');
            $sellingPrices = $request->getPost('sellingPrice');
            $itemPrices = $request->getPost('itemPrice');
            $sellingPriceUom = $request->getPost('sellingPriceUom');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $uomConversionFactor = $request->getPost('uomConversionFactor');
            $editMode = filter_var($request->getPost('editMode'), FILTER_VALIDATE_BOOLEAN);
            $this->beginTransaction();

            $locationsArr = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationsByProductID($productID);
            $locationIDs = [];
            
            foreach ($locationsArr as $l) {
                $locationIDs[] = $l['locationID'];
                $discpercentage = $discountPercentages[$l['locationID']];
                if ($discpercentage == "") {
                    $discpercentage = null;
                }
                
                $discValue = $discountValues[$l['locationID']];
                if ($discValue == "") {
                    $discValue = null;
                }

                $data = array(
                    'locationProductID' => $l['locationProductID'],
                    'locationProductMinInventoryLevel' => (isset($minInvLevel[$l['locationID']])) ? $minInvLevel[$l['locationID']] : 0,
                    'locationProductMaxInventoryLevel' => (isset($maxInvLevel[$l['locationID']])) ? $maxInvLevel[$l['locationID']] : 0,
                    'locationProductReOrderLevel' => (isset($reorderLevels[$l['locationID']])) ? $reorderLevels[$l['locationID']] : 0,
                    'defaultSellingPrice' => (isset($sellingPrices[$l['locationID']])) ? $sellingPrices[$l['locationID']] : 0,
                    'locationDiscountPercentage' => $discpercentage,
                    'locationDiscountValue' => $discValue,
                    'defaultPurchasePrice' => (isset($itemPrices[$l['locationID']])) ? $itemPrices[$l['locationID']] : 0.00,
                );

                
//              coverting defalt selling price to base quantity price accroding to user selected uom
                if (isset($sellingPriceUom[$l['locationID']])) {
                    $uomID = $sellingPriceUom[$l['locationID']];
                    $cF = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($productID, $uomID);
                    $data['defaultSellingPrice'] = $data['defaultSellingPrice'] / $cF;
                }
                $locationProduct = new LocationProduct();
                $locationProduct->exchangeArray($data);

                if (!$this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProduct($locationProduct)) {
                    return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_SAVELOCITEMD'));
                }

//                Check already used this location product
                $grnProduct = $this->CommonTable('Inventory\Model\GrnProductTable')->CheckGrnProductByLocationProductID($l['locationProductID']);
                $pvProduct = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->CheckPurchaseInvoiceProductByLocationProductID($l['locationProductID']);
                $adjuesmentProduct = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->CheckGoodsIssueProductByLocationProductID($l['locationProductID']);
                $transferProduct = $this->CommonTable('Inventory\Model\TransferTable')->CheckTransferInByLocationProductId($l['productID'], $l['locationID']);

                if (isset($grnProduct->grnProductID) || isset($pvProduct->purchaseInvoiceProductID) || isset($adjuesmentProduct->goodsIssueProductID) || count($transferProduct) > 0) {
                    $isAlreadyUsed = TRUE;
                } else {
                    $isAlreadyUsed = FALSE;
                }

//                check batch product or serial product
                $product = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);
                $isBatOrSerialProduct = false;
                if ($product['batchProduct'] || $product['serialProduct']) {
                    $isBatOrSerialProduct = true;
                }

//          Adding positive adjuestment
                if (isset($openingLPQ[$l['locationID']]) && ($openingLPQ[$l['locationID']] > 0) && !$isAlreadyUsed && !$isBatOrSerialProduct && !($product['productTypeID'] == 2)) {

                    $accountProduct = array();

                    $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($l['locationProductID']);
                    $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                    $locationProductQuantity = $locationProductData->locationProductQuantity;
                    $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

                    $productConversionRate = $uomConversionFactor[$l['locationID']];

                    if($this->useAccounting == 1){
                        if($product['productInventoryAccountID'] == ''){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($product['productCode'].' - '.$product['productName']));
                            return $this->JSONRespond();
                        }
                        if($product['productAdjusmentAccountID'] == ''){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PRPODUCT_ADJUSMENT_ACCOUNT', array($product['productCode'].' - '.$product['productName']));
                            return $this->JSONRespond();
                        }
                    }

                    $locID = $l['locationID'];
                    $result = $this->getLocationReferenceDetails('28', null);
                    $locRefID = $result->locationReferenceID;
                    $adjusmentCode = $this->getReferenceNumber($locRefID);

//insert data in to GoodIssueTable
                    $data['locationID'] = $locID;
                    $data['goodsIssueCode'] = $adjusmentCode;
                    $data['goodsIssueDate'] = $this->getGMTDateTime('Y-m-d');
                    $data['goodsIssueReason'] = 'Adding opening Quantity';
                    $data['goodsIssueTypeID'] = 2; // Positive Adjustment type

                    $goodsIssueModel = new GoodsIssue();
                    $goodsIssueModel->exchangeArray($data);
                    $entityID = $this->createEntity();
                    $goodsIssueModel->entityID = $entityID;

                    $insertedGoodsIssueID = $this->CommonTable('Inventory\Model\GoodsIssueTable')->saveGoodIssue($goodsIssueModel);

                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($l['locationProductID']);
                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($openingLPQ[$l['locationID']])* floatval($productConversionRate);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $l['locationProductID']);
                    $baseUomID = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductBaseUomID($productID);
                    $goodIssueProData = array(
                        'goodsIssueID' => $insertedGoodsIssueID,
                        'locationProductID' => $l['locationProductID'],
                        'goodsIssueProductQuantity' => (floatval($openingLPQ[$l['locationID']])*floatval($productConversionRate)),
                        'unitOfPrice' => (isset($itemPrices[$l['locationID']])) ? $itemPrices[$l['locationID']] : 0.00,
                        'uomID' => $baseUomID,
                        'uomConversionRate' => $uomConversionFactor[$l['locationID']]
                    );
                    $goodsIssueProductModel = new GoodsIssueProduct();
                    $goodsIssueProductModel->exchangeArray($goodIssueProData);
                    if (!$this->CommonTable('Inventory\Model\GoodsIssueProductTable')->saveGoodIssueProduct($goodsIssueProductModel)) {
                        $this->rollback();
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_SAVEPOSIADJ'));
                    } else {
//save itemIn Details
                        $itemInData = array(
                            'itemInDocumentType' => 'Inventory Positive Adjustment',
                            'itemInDocumentID' => $insertedGoodsIssueID,
                            'itemInLocationProductID' => $l['locationProductID'],
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => NULL,
                            'itemInQty' => floatval($openingLPQ[$l['locationID']]* floatval($productConversionRate)),
                            'itemInPrice' => (isset($itemPrices[$l['locationID']])) ? $itemPrices[$l['locationID']] : 0,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
//update auto postive ajusment code
                        if ($locRefID) {
                            $this->updateReferenceNumber($locRefID);
                        }

                        if($this->useAccounting == 1){
                            $unitPrice = (isset($itemPrices[$l['locationID']])) ? $itemPrices[$l['locationID']] : 0;
                            $productTotal = floatval($openingLPQ[$l['locationID']]) * floatval($productConversionRate) * $unitPrice;

                            if(isset($accountProduct[$product['productAdjusmentAccountID']]['creditTotal'])){
                                $accountProduct[$product['productAdjusmentAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$product['productAdjusmentAccountID']]['creditTotal'] = $productTotal;
                                $accountProduct[$product['productAdjusmentAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$product['productAdjusmentAccountID']]['accountID'] = $product['productAdjusmentAccountID'];
                            }

                            if(isset($accountProduct[$product['productInventoryAccountID']]['debitTotal'])){
                                $accountProduct[$product['productInventoryAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$product['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                $accountProduct[$product['productInventoryAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$product['productInventoryAccountID']]['accountID'] = $product['productInventoryAccountID'];
                            }
                        }

                        $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($l['locationProductID']);
                        $locationPrTotalQty = 0;
                        $locationPrTotalPrice = 0;
                        $newItemInIds = array();
                        if(count($itemInData) > 0){
                            foreach ($itemInData as $key => $value) {
                                if($value->itemInID > $locationProductLastItemInID){
                                    $newItemInIds[] = $value->itemInID;
                                    $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                    if($remainQty > 0){
                                        $itemInPrice = $remainQty*$value->itemInPrice;
                                        $itemInDiscount = $remainQty*$value->itemInDiscount;
                                        $locationPrTotalQty += $remainQty;
                                        $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                                    }
                                    $locationProductLastItemInID = $value->itemInID;
                                }
                            }
                        }

                        $locationPrTotalQty += $locationProductQuantity;
                        $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;
                        if($locationPrTotalQty > 0){
                            $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                            $lpdata = array(
                                'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                                'locationProductLastItemInID' => $locationProductLastItemInID,
                                );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $l['locationProductID']);

                            foreach ($newItemInIds as $inID) {
                                $intemInData = array(
                                    'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                    );
                                $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                            }

                        }

                        if ($this->useAccounting == 1) {
                            //create data array for the journal entry save.
                            $i=0;
                            $journalEntryAccounts = array();

                            foreach ($accountProduct as $key => $value) {
                                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Inventroy Positive Adjustments '.$adjusmentCode;
                                $i++;
                            }

            //get journal entry reference number.
                            $jeresult = $this->getReferenceNoForLocation('30', $l['locationID']);
                            $jelocationReferenceID = $jeresult['locRefID'];
                            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                            $journalEntryData = array(
                                'journalEntryAccounts' => $journalEntryAccounts,
                                'journalEntryDate' => $data['goodsIssueDate'],
                                'journalEntryCode' => $JournalEntryCode,
                                'journalEntryTypeID' => '',
                                'journalEntryIsReverse' => 0,
                                'journalEntryComment' => 'Journal Entry is posted when create Positive Adjustment',
                                'documentTypeID' => 16,
                                'journalEntryDocumentID' => $insertedGoodsIssueID,
                                'ignoreBudgetLimit' => $ignoreBudgetLimit,
                                );

                            $resultData = $this->saveJournalEntry($journalEntryData);
                            if(!$resultData['status']){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $resultData['msg'];
                                $this->data = $resultData['data'];
                                return $this->JSONRespond();
                            }
                        }
                    }
                }
            }
            // for update inventory when create item
            $eventParameter = ['productIDs' => [$productID], 'locationIDs' => $locationIDs];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            $this->commit();

            $this->status = true;
            $this->msg = $this->getMessage('SUCC_PRDCTAPI_LOCUPDATE');
            if (!$editMode) {
                $this->flashMessenger()->addMessage($this->msg);
            }
            return $this->JSONRespond();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_PRDCTAPI_SAVE');
            return $this->JSONRespond();
        }
    }

    public function getProductDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productID = $request->getPost('productID');
            $locationID = $request->getPost('locationID');
            $return = $this->CommonTable('Inventory\Model\ProductTable')->getLocationProduct($productID, $locationID);
            if (isset($return->locationProductID)) {
                return new JsonModel(array('state' => true, 'data' => $return));
            } else {
                return new JsonModel(array('state' => "none"));
            }
        } else {
            return new JsonModel(array('state' => false));
        }
    }

    public function getProductUomListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productID = $request->getPost('productID');
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomsListByProductID($productID);
            $this->data = $productUom;
            return $this->JSONRespond();
        }
    }

    public function getProductDetailsToEditAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productID = $request->getPost('productID');
            $productSuppliers = '';
            $productTax = '';
            $product = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);
            $productSupp = $this->CommonTable('Inventory\Model\ProductSupplierTable')->getProductSupplierByProductID($productID);
            while ($row = $productSupp->current()) {
                $productSuppliers[$row['supplierID']] = $row;
            }
            $productT = $this->CommonTable('Inventory\Model\ProductTaxTable')->getProductTaxByProductID($productID);
            while ($row = $productT->current()) {
                $productTax[$row->taxID] = $row;
            }
            $locationPro = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationsByProductID($productID);
            while ($row = $locationPro->current()) {
                $locationProduct[$row['locationID']] = $row;
            }
            if ($product) {
                return new JsonModel(array('state' => true, 'product' => $product, 'productSuppliers' => $productSuppliers, 'productTax' => $productTax, 'locationProduct' => $locationProduct));
            } else {
                return new JsonModel(array('state' => false));
            }
        } else {
            return new JsonModel(array('state' => false));
        }
    }

    public function deleteProductAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productID = $request->getPost('productID');
            $fixedAssetFlag = $request->getPost('fixedAssetFlag');
            $product = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);
            $entityID = $product['entityID'];

            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsByProductID($productID);
            $locationIDs = [];
            $flag = 1;
            foreach ($locationProduct as $val) {
                $locationIDs[] = $val['locationID'];
                $val = (object) $val;
                $locationProductID = $val->locationProductID;
                $grnProduct = $this->CommonTable('Inventory\Model\GrnProductTable')->CheckGrnProductByLocationProductID($locationProductID);
                if (!isset($grnProduct->grnProductID)) {
                    $grnProduct = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->CheckGoodsIssueProductByLocationProductID($locationProductID);
                    if (!isset($grnProduct->locationProductID)) {
                        $purchaseInvoiceProduct = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->CheckPurchaseInvoiceProductByLocationProductID($locationProductID);
                        if (!isset($purchaseInvoiceProduct->locationProductID)) {
                            $purchaseOrderProduct = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->CheckPurchaseOrderProductByLocationProductID($locationProductID);
                            if (!isset($purchaseOrderProduct->locationProductID)) {
                                $transferProduct = $this->CommonTable('Inventory\Model\TransferProductTable')->CheckTransferProductByProductID($productID);
                                if (!isset($transferProduct->transferProductID)) {
                                    $quotationProduct = $this->CommonTable('Invoice\Model\QuotationProductTable')->CheckQuotationProductByLocationProductID($locationProductID);
                                    if (!isset($quotationProduct->locationProductID)) {
                                        $SalesOrderProduct = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->CheckSalesOrderProductByLocationProductID($locationProductID);
                                        if (isset($SalesOrderProduct->locationProductID)) {
                                            $flag = 0;
                                            break;
                                        }
                                    } else {
                                        $flag = 0;
                                        break;
                                    }
                                } else {
                                    $flag = 0;
                                    break;
                                }
                            } else {
                                $flag = 0;
                                break;
                            }
                        } else {
                            $flag = 0;
                            break;
                        }
                    } else {
                        $flag = 0;
                        break;
                    }
                } else {
                    $flag = 0;
                    break;
                }
            }
            if ($flag) {
                $return = $this->updateDeleteInfoEntity($entityID);
                if ($return) {

                    $eventParameter = [
                        'productIDs' => [$productID],
                        'locationIDs' => $locationIDs,
                        'action' => 'deleted'
                    ];
                    $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

                    $barcodeDetails = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getRelatedBarCodesByProductID($productID, true);

                    if (sizeof($barcodeDetails) > 0) {
                        foreach ($barcodeDetails as $key => $barcode) {
                            $deleteBarcode = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->deleteBarCodeByItemBarCodeID($barcode['itemBarcodeID']);
                            
                        }
                    }

                    $view = $this->CommonTable('Inventory\Model\ProductImageTable')->getDataByProductID($productID);
                    $pieces = explode("_preview.", $view['productImageName']);

                    $path = $this->getCompanyDataFolder();
                    $newPath = $path . "/product/";
                    $copypath = $newPath . 'thumbs/';
                    $tempFile = $newPath . $pieces[0] . "." . $pieces[1];
                    $previewFile = $copypath . $view['productImageName'];

                    if (file_exists($previewFile)) {
                        @unlink($previewFile);
                    }
                    if (file_exists($tempFile)) {
                        @unlink($tempFile);
                    }

                    $return = $this->CommonTable('Inventory\Model\ProductImageTable')->deleteImageDetails($productID);
                    $productsList = $this->getPaginatedProducts($fixedAssetFlag);

                    if ($fixedAssetFlag) {
                        $htmlOutput = $this->_getListViewHtmlFixedAsset($productsList, true);
                    } else {
                        $htmlOutput = $this->_getListViewHtml($productsList, true);
                    }
                    $this->setLogMessage('Product '.$product['productCode'].' is deleted');
                    $this->msg = $this->getMessage('SUCC_PRDCTAPI_DEL');
                    $this->status = true;
                    $this->html = $htmlOutput;
                    return $this->JSONRespondHtml();
                } else {
                    $this->msg = $this->getMessage('ERR_PRDCTAPI_DEL');
                    $this->status = false;
                    return $this->JSONRespond();
                }
            } else {
                $this->msg = $this->getMessage('ERR_PRDCTAPI_NODELPRO');
                $this->status = false;
                return $this->JSONRespond();
            }
        }
    }

    public function changeStateAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $product = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($req->getPost('productID'));
            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationsByProductID($req->getPost('productID'));
            $fixedAssetFlag = $req->getPost('fixedAssetFlag');
            if ($product['productHandelingID'] == NULL) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_SELETYPE');
            } else if ($product['categoryID'] == NULL) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_SELECATG');
            } else if (!$locationProduct->current()) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_SELELOC');
            } else {
                $productID = $req->getPost('productID');
                $post = [
                    'productState' => (int) $req->getPost('productState'),
                    'productID' => $productID
                ];

                $product = new Product();
                $product->exchangeArray($post);
                $this->CommonTable('Inventory\Model\ProductTable')->updateProductState($product);
                $verb = ($post['productState']) ? 'activated' : 'deactivated';


                $productLocations = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsByProductID($productID);
                $locationIDs = array_map(function($element) {
                    return $element['locationID'];
                }, iterator_to_array($productLocations));


                if ($req->getPost('productState') == 0) {
                    $eventParameter = [
                        'productIDs' => [$productID],
                        'locationIDs' => $locationIDs,
                        'action' => 'deleted'
                    ];
                } else {
                    $eventParameter = ['productIDs' => [$productID], 'locationIDs' => $locationIDs];
                }

                $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

// get product list to return to view
                $productList = $this->getPaginatedProducts($fixedAssetFlag);
                if ($fixedAssetFlag) {
                    $htmlOutput = $this->_getListViewHtmlFixedAsset($productList, true);
                } else {
                    $htmlOutput = $this->_getListViewHtml($productList, true);
                }
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PRDCTAPI_STATUS', array($verb));
                $this->html = $htmlOutput;
                return $this->JSONRespondHtml();
            }
            return $this->JSONRespond();
        }
    }

    public function getProductsForSearchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $fixedAssetFlag = ($request->getPost('fixedAssetFlag') == "false") ? false : true;
            if (trim($request->getPost('searchProductString')) != "") {
                $products = $this->CommonTable('Inventory\Model\ProductTable')->getProductsforSearch($request->getPost('searchProductString'), $locationID);
                $paginated = NULL;
            } else {
                $products = $this->getPaginatedProducts($fixedAssetFlag);
                $paginated = true;
            }

            if ($fixedAssetFlag) {
                $htmlOutput = $this->_getListViewHtmlFixedAsset($products, $paginated);
            } else {
                $htmlOutput = $this->_getListViewHtml($products, $paginated);
            }

            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedProducts($fixedAssetFlag = false)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Inventory\Model\ProductTable')->fetchAll(true, $locationID, $fixedAssetFlag);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);

        return $this->paginator;
    }

    public function getProductsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductsFromLocation($locationID);
            $LocaProduct = array();
            while ($locPro = $locationProduct->current()) {
                $LocaProduct[] = $locPro;
            }
            if ($locationProduct) {
                $this->status = true;
                $this->msg = $this->getMessage('PRDCTAPI_EMPTY');
                $this->data = $LocaProduct;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_LOC');
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function returns active products related to a
     * specific location
     * input - locationID
     */
    public function getActiveProductsFromLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            //this type send from activity.js for filter some specific data...
            $type = $request->getPost('dataType');
            if ($type == 'activity') {
                $productsList = $this->CommonTable('Inventory\Model\ProductTable')->getActiveProductListByLocation($locationID, null, $type);
            } else {
                $productsList = $this->CommonTable('Inventory\Model\ProductTable')->getActiveProductListByLocation($locationID);
            }
            $products = array();
            foreach ($productsList as $row) {
                $tempP = array();
                $tempP['pC'] = $row['productCode'];
                $tempP['lPID'] = $row['locationProductID'];
                $tempP['pN'] = $row['productName'];
                $tempP['LPQ'] = $row['locationProductQuantity'];
                $tempP['bP'] = $row['batchProduct'];
                $tempP['sP'] = $row['serialProduct'];
                $tempP['dSP'] = $row['defaultSellingPrice'];
                $tempP['dPP'] = $row['defaultPurchasePrice'];
                $tempP['dEL'] = $row['productDiscountEligible'];
                $tempP['dPR'] = $row['locationDiscountPercentage'];
                $tempP['dV'] = $row['locationDiscountValue'];
                $tempP['pT'] = $row['productTypeID'];
                $tempP['sales'] = false;
                $tempP['purchase'] = false;
                $tempP['isRawMaterial'] = false;
                $tempP['isFixedAssets'] = false;


                if ($row['productHandelingManufactureProduct'] == 1 || $row['productHandelingSalesProduct'] == 1) {
                    $tempP['sales'] = true;
                }

                if ($row['productHandelingManufactureProduct'] == 1 || $row['productHandelingPurchaseProduct'] == 1 || $row['productHandelingConsumables'] == 1 || $row['productHandelingFixedAssets'] == 1) {
                    $tempP['purchase'] = true;
                }
//if item is raw material
                if ($row['productHandelingPurchaseProduct'] == 1) {
                    $tempP['isRawMaterial'] = true;
                }
//if item is fixed assets
                if ($row['productHandelingFixedAssets'] == 1) {
                    $tempP['isFixedAssets'] = true;
                }

                $tax = (isset($products[$row['productID']]['tax'])) ? $products[$row['productID']]['tax'] : array();
                if ($row['productTaxEligible'] == 1) {
                    if ($row['taxID'] != NULL) {
                        $tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state']);
                    }
                }
                $uom = (isset($products[$row['productID']]['uom'])) ? $products[$row['productID']]['uom'] : array();
                if ($row['uomID'] != NULL) {
                    $uom[$row['uomID']] = array('uA' => $row['uomAbbr'], 'uN' => $row['uomName'], 'uDP' => $row['uomDecimalPlace'], 'uS' => $row['uomState'], 'uC' => $row['productUomConversion'], 'pUI' => $row['productUomID'], 'uomID' => $row['uomID'], 'pUDisplay' => $row['productUomDisplay']);
                }

                $batchSerial = (isset($products[$row['productID']]['batchSerial'])) ? $products[$row['productID']]['batchSerial'] : array();
                $batchIDsInBatchSerial = (isset($products[$row['productID']]['productIDs'])) ? $products[$row['productID']]['productIDs'] : array();
                if ($row['serialProductBatchID'] != NULL && $row['serialProductBatchID'] == $row['productBatchID'] && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {
                    $batchSerial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold']);
                    if ($row['productBatchID'] != NULL) {
                        $batchIDsInBatchSerial[$row['productBatchID']] = array('PBID' => $row['productBatchID']);
                    }
                }

                $batch = (isset($products[$row['productID']]['batch'])) ? $products[$row['productID']]['batch'] : array();
                if ($row['productBatchID'] != NULL && $row['serialProductBatchID'] != $row['productBatchID']) {
                    $batch[$row['productBatchID']] = array('PBID' => $row['productBatchID'], 'PBC' => $row['productBatchCode'], 'PBQ' => $row['productBatchQuantity'], 'PSID' => $row['productSerialID']);
                }

                $serial = (isset($products[$row['productID']]['serial'])) ? $products[$row['productID']]['serial'] : array();
                if ($row['productSerialID'] != NULL && $row['serialProductBatchID'] == NULL && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {

                    $serial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'],);
                }

                $tempP['tax'] = $tax;
                $tempP['uom'] = $uom;
                $tempP['batch'] = $batch;
                $tempP['serial'] = $serial;
                $tempP['batchSerial'] = $batchSerial;
                $tempP['productIDs'] = $batchIDsInBatchSerial;
                $products[$row['productID']] = $tempP;
            }
            $this->data = $products;
            return $this->JSONRespond();
        }
    }

    public function getProductsForReturnAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $productsList = $this->CommonTable('Inventory\Model\ProductTable')->getActiveProductListByLocation($locationID);
            $products = array();
            foreach ($productsList as $row) {
                if ($row['productSerialReturned'] == 0) {
                    $tempP = array();
                    $tempP['pC'] = $row['productCode'];
                    $tempP['lPID'] = $row['locationProductID'];
                    $tempP['pN'] = $row['productName'];
                    $tempP['LPQ'] = $row['locationProductQuantity'];
                    $tempP['bP'] = $row['batchProduct'];
                    $tempP['sP'] = $row['serialProduct'];
                    $tempP['dSP'] = $row['defaultSellingPrice'];
                    $tempP['dPR'] = $row['locationDiscountPercentage'];
                    $tempP['dV'] = $row['locationDiscountValue'];

                    $tax = (isset($products[$row['productID']]['tax'])) ? $products[$row['productID']]['tax'] : array();
                    if ($row['productTaxEligible'] == 1) {
                        if ($row['taxID'] != NULL) {
                            $tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state']);
                        }
                    }
                    $uom = (isset($products[$row['productID']]['uom'])) ? $products[$row['productID']]['uom'] : array();
                    if ($row['uomID'] != NULL) {
                        $uom[$row['uomID']] = array('uA' => $row['uomAbbr'], 'uN' => $row['uomName'], 'uDP' => $row['uomDecimalPlace'], 'uS' => $row['uomState'], 'uC' => $row['productUomConversion'], 'pUI' => $row['productUomID']);
                    }

                    $batchSerial = (isset($products[$row['productID']]['batchSerial'])) ? $products[$row['productID']]['batchSerial'] : array();
                    $batchIDsInBatchSerial = (isset($products[$row['productID']]['productIDs'])) ? $products[$row['productID']]['productIDs'] : array();
                    if ($row['serialProductBatchID'] != NULL && $row['serialProductBatchID'] == $row['productBatchID']) {
                        $batchSerial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold']);
                        if ($row['productBatchID'] != NULL) {
                            $batchIDsInBatchSerial[$row['productBatchID']] = array('PBID' => $row['productBatchID']);
                        }
                    }

                    $batch = (isset($products[$row['productID']]['batch'])) ? $products[$row['productID']]['batch'] : array();
                    if ($row['productBatchID'] != NULL && $row['serialProductBatchID'] != $row['productBatchID']) {
                        $batch[$row['productBatchID']] = array('PBID' => $row['productBatchID'], 'PBC' => $row['productBatchCode'], 'PBQ' => $row['productBatchQuantity'], 'PSID' => $row['productSerialID']);
                    }

                    $serial = (isset($products[$row['productID']]['serial'])) ? $products[$row['productID']]['serial'] : array();
                    if ($row['productSerialID'] != NULL && $row['serialProductBatchID'] == NULL) {

                        $serial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'],);
                    }

                    $tempP['tax'] = $tax;
                    $tempP['uom'] = $uom;
                    $tempP['batch'] = $batch;
                    $tempP['serial'] = $serial;
                    $tempP['batchSerial'] = $batchSerial;
                    $tempP['productIDs'] = $batchIDsInBatchSerial;
                    $products[$row['productID']] = $tempP;
                }
            }
            $this->data = $products;
            return $this->JSONRespond();
        }
    }

    public function checkSerialProductCodeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $startingNumber = $request->getPost('startingNumber');
            $prefix = $request->getPost('prefix');
            $serialQuntity = $request->getPost('serialQuntity');
            $strNumberLength = $request->getPost('strNumberLength');
            $ProductCode = $request->getPost('productCode');

            if ($prefix) {
                for ($i = $startingNumber; $i < $startingNumber + $serialQuntity; $i++) {
                    $string = sprintf("%0" . $strNumberLength . "d", $i);
                    $serialProductCode = $prefix . $string;
                    $result = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductByCodeAndSerailCode($ProductCode, $serialProductCode);
                    $codeExists = false;
                    if ($result->current()) {
                        $codeExists = true;
                        $this->data = $codeExists;
                        return $this->JSONRespond();
                    }
                }
            } else {
                for ($i = $startingNumber; $i < $startingNumber + $serialQuntity; $i++) {
                    $string = sprintf("%0" . $strNumberLength . "d", $i);
                    $serialProductCode = $string;
                    $result = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductByCodeAndSerailCode($ProductCode, $serialProductCode);
                    $codeExists = false;
                    if ($result->current()) {
                        $codeExists = true;
                        $this->data = $codeExists;
                        return $this->JSONRespond();
                    }
                }
            }

            $this->data = $codeExists;
            return $this->JSONRespond();
        }
    }

    public function checkSerialProductCodeExistsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $bulkFlag = filter_var($request->getPost('bulk'), FILTER_VALIDATE_BOOLEAN);
            $serialProductCode = $request->getPost('serialCode');
            $ProductCode = $request->getPost('productCode');
            if ($bulkFlag) {
                $serialCodeSet = $request->getPost('dataSet');
                $result = $this->CommonTable('Inventory\Model\ProductSerialTable')->getProductByCodeAndSerailCode($ProductCode, $serialProductCode, true, $serialCodeSet);
                //create return data set
                $existingSerialCodes = [];
                foreach ($result as $key => $value) {
                    $existingSerialCodes[] = $value['productSerialCode']; 
                }

                if (!empty($existingSerialCodes)) {
                    $this->status = true;
                    $this->data =  implode(", ",$existingSerialCodes);
                    return $this->JSONRespond();
                } else {
                    $this->status = false;
                    return $this->JSONRespond();
                }
                
            } else {
                $result = $this->CommonTable('Inventory\Model\ProductSerialTable')->getProductByCodeAndSerailCode($ProductCode, $serialProductCode);
                $codeExists = false;
                if ($result->current()) {
                    $codeExists = true;
                }
                $this->data = $codeExists;
                return $this->JSONRespond();

            }

        }
    }

    public function importproductAction()
    {
        $importrequest = $this->getRequest();
        if ($importrequest->isPost()) {
            $delim = $importrequest->getPost('delim');
            $filecontent = fopen('/tmp/ezBiz.productImportData.csv', "r");
            $locationarray = array();
            $replaceCount = 0;
            $writtenrowcount = 0;
            $errorCount = 0;
            $discount = 0;
            $customduty = 0;

            if ($importrequest->getPost('header') == TRUE) {
                $linecontent = fgetcsv($filecontent, 1000, $delim);
            } else {

            }
            $colarray = $importrequest->getPost('col');
            $columncount = $importrequest->getPost('columncount');

            while ($linecontent = fgetcsv($filecontent, 1000, $delim)) {
                $importarray = array();
                $productObject = (object) array('productID' => '', 'globalProduct' => '', 'productName' => '', 'productCode' => '');
                $columns = count($linecontent);
                for ($content = 0; $content < $columns; $content++) {
                    if ($colarray[$content] == 'Item Code') {
                        $importarray['productCode'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Item Name') {
                        $importarray['productName'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Item Type ID') {
                        $importarray['productTypeID'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Item Description') {
                        $importarray['productDescription'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Item Discount Eligible') {
                        $importarray['productDiscountEligible'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Item Discount Value(RS)') {
                        $importarray['productDiscountValue'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Item Discount Percentage(%)') {
                        $importarray['productDiscountPercentage'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Item Tax Eligible') {
                        $importarray['productTaxEligible'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Product Image Url') {
                        $importarray['productImageUrl'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Batch Item') {
                        $importarray['batchProduct'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Serial Item') {
                        $importarray['serialProduct'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Hs Code') {
                        $importarray['hsCode'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Custom Duty Percentage') {
                        $importarray['customDutyPercentage'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Custom Duty Value') {
                        $importarray['customDutyValue'] = $linecontent[$content];
                    }
                }
//if product discount precentage and product discout value is set, then set only precentage
                if (isset($importarray['productDiscountPercentage']) && $importarray['productDiscountPercentage'] != '' && isset($importarray['productDiscountValue']) && $importarray['productDiscountValue'] != '') {
                    $importarray['productDiscountValue'] = '';
                    $discount++;
                }
//if custom duty precentage and custom duty value is set,then set only custom precentage
                if (isset($importarray['customDutyPercentage']) && $importarray['customDutyPercentage'] != '' && isset($importarray['customDutyValue']) && $importarray['customDutyValue'] != '') {
                    $importarray['customDutyValue'] = '';
                    $customduty++;
                }

                $importarray['entityID'] = '';
                $importproduct = new Product();
                $importproduct->exchangeArray($importarray);
                $importproduct->productGlobalProduct = 1;

                $searchProduct = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductCode($importproduct->productCode);
                if (!$searchProduct->productID) {
                    $entityID = $this->createEntity();
                    $importproduct->entityID = $entityID;
                    $importres = $this->CommonTable('Inventory\Model\ProductTable')->saveProduct($importproduct);
                    if ($importres != false) {
                        $writtenrowcount++;
                        $product = new $productObject;
                        $product->productID = $importres;
                        $product->globalProduct = isset($importproduct->productGlobalProduct) ? $importproduct->productGlobalProduct : 0;
                        $product->productCode = $importproduct->productCode;
                        $product->productName = $importproduct->productName;
                        $locationarray[] = $product;
                    } else {
                        $errorCount++;
                    }
                } else {
                    $replaceCount++;
                }
            }

            $this->status = true;
            if ($writtenrowcount != 0) {
                $this->msg = $this->getMessage('SUCC_PRDCTAPI_ROW', array($writtenrowcount));
            }

            if ($errorCount != 0) {
                $this->msg = $this->getMessage('ERR_PRDCTAPI_ROW', array($errorCount));
            }
            if ($replaceCount != 0) {
                $this->msg = $this->getMessage('ERR_PRDCTAPI_REPLACE', array($replaceCount));
            }

            if ($writtenrowcount == 0 && $errorCount == 0) {
                $this->msg = $this->getMessage('ERR_PRDCTAPI_NONEWPRO');
            }

            if ($discount != 0) {
                $this->msg = $this->getMessage('NOTE_PRDCTAPI_DISCPERCENT');
            }
            if ($customduty != 0) {
                $this->msg = $this->getMessage('NOTE_PRDCTAPI_CUSTDUTY');
            }
            $dataArray = $locationarray;
            $this->data = $locationarray;

            $htmlOutput = $this->_getProductLocationViewHtml($dataArray);

            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        } else {
            $this->msg = $this->getMessage('ERR_PRDCTAPI_OCCUR');
            $this->status = false;
            return $this->JSONRespond();
        }
    }

    public function importProductLocationsAction()
    {
        $importrequest = $this->getRequest();
        if ($importrequest->isPost()) {
            $result = 'added';
            $local = $importrequest->getPost('local');
            $this->beginTransaction();
            foreach ($local as $key => $val) {
                foreach ($val['locationArray'] as $lkey => $lval) {
                    if ($lval) {
                        $checkResult = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($val['productID'], $lval);
                        if (!isset($checkResult->locationProductID)) {
                            $entityID = $this->createEntity();
                            $data = array(
                                'productID' => $val['productID'],
                                'locationID' => $lval,
                                'entityID' => $entityID,
                            );
                            $locationProduct = new LocationProduct();
                            $locationProduct->exchangeArray($data);
                            $result = $this->CommonTable('Inventory\Model\LocationProductTable')->saveLocationProduct($locationProduct);
                        }
                    }
                }
                $categoryID = $val['categoryID'];
                $productGlobal = $val['productGlobal'];
                $data = (object) array(
                            'productID' => $val['productID'],
                            'categoryID' => $categoryID,
                            'productGlobalProduct' => $productGlobal,
                );
                $this->CommonTable('Inventory\Model\ProductTable')->updateImportProduct($data);
            }
            $this->commit();
            if ($result == false) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_OCCUR');
            } else if ($result == 'added') {
                $this->status = true;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_LOCADD');
            } else {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PRDCTAPI_IMPLOC');
            }
            return $this->JSONRespond();
        }
    }

    public function getImportConfirmAction()
    {
        $viewmodel = new ViewModel();
        $viewmodel->setTerminal(true);
        return $viewmodel;
    }

    private function _getListViewHtml($productList = array(), $paginated = false)
    {

        $view = new ViewModel(array(
            'cSymbol' => $this->companyCurrencySymbol,
            'products' => $productList,
            'paginated' => $paginated,
            'cSymbol' => $this->companyCurrencySymbol,
        ));

        $view->setTerminal(true);
        $view->setTemplate('inventory/product/product-list');

        return $view;
    }

    private function _getListViewHtmlFixedAsset($productList = array(), $paginated = false)
    {

        $userLocation = $this->user_session->userActiveLocation['locationID'];

        $proIds = [];
        $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown([$userLocation], $productSearchKey = NULL, $isSearch = FALSE, false, null, true);

        foreach ($productData as $value) {
            $proIds[$value['productID']] = $value['productID'];
        }

        if (!empty($proIds)) {
            $globalStockValueData = $this->getService('StockInHandReportService')->getProductsWithGRNCosting($proIds, [$userLocation], false, []);
            $depreciation = $this->CommonTable('Inventory/Model/DepreciationTable')->getTotalDepreciationByProductIDArray($proIds);
        } else {
            $globalStockValueData = 0;
            $depreciation = 0;
        }


        $depreciationData=[];
        foreach ($depreciation as $value) {
            $depreciationData[$value['productID']] = ($value['totalDepreciation'] == null) ? 0 : $value['totalDepreciation'];
        }

        $view = new ViewModel(array(
            'cSymbol' => $this->companyCurrencySymbol,
            'products' => $productList,
            'globalStockValueData' => $globalStockValueData,
            'depreciationData' => $depreciationData,
            'userLocation' => $userLocation,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('inventory/product/fixed-asset-list-table');

        return $view;
    }

    private function _getProductLocationViewHtml($product)
    {
        $userID = $this->user_session->userID;
        $locations = $this->CommonTable('Core\Model\LocationTable')->gerActiveUserLocation($userID);
        $category = (object) array('categoryID' => '', 'categoryName' => '', 'categoryPath' => '');
        $allCategoryPaths = $this->CommonTable('Inventory/Model/CategoryTable')->getHierarchyPaths();
        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $catgoryArray = array();
        foreach ($allCategoryPaths as $key => $value) {
            $path = rtrim($value, ' > ' . $allCategoryList[$key]);
            $categoryies = new $category();
            $categoryies->categoryID = $key;
            $categoryies->categoryName = $allCategoryList[$key];
            $categoryies->categoryPath = $path;
            $catgoryArray[] = $categoryies;
        }
        $locationView = new ViewModel(array(
            'products' => $product,
            'loc' => $locations,
            'categories' => $catgoryArray,
        ));
        $categoryForm = new CategoryForm($allCategoryList);

        $locationView->categoryForm = $categoryForm;

        $locationView->setTerminal(true);
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');
        $locationView->setTemplate('inventory/product/product-locations');

        return $locationView;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getLocationProductDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productID = $request->getPost('productID');
            $locationID = $request->getPost('locationID');
            $locationProduct = $this->getLocationProductDetails($locationID, $productID);
            $this->data = $locationProduct;

            $this->setLogMessage("Retrive ".$locationProduct['pC']." Product Details");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchLocationProductsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $productSearchKey = $searchrequest->getPost('searchKey');
            $documentType = $searchrequest->getPost('documentType');
            $locationID = $searchrequest->getPost('locationID');
            if (!$locationID) {
                $locationID = $this->user_session->userActiveLocation['locationID'];
            }
//addflag is used for check request is coming from invoice or not
//if addflag==1 its mean request coming from invoice
            $addFlag = $searchrequest->getPost('addFlag');
//if addflag==1 below function return all products with gift card othervice only return products.

            $isBom = false;
            // if ($documentType == 'bomDocument') {
            //     $isBom = true;
            // }

            $locationProducts = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationProductsForDropdown($locationID, $productSearchKey, $addFlag, $isBom);

            $otherOptions = array();
            if (isset($addFlag) && $addFlag == 1) {
                $otherOptions[] = ["value" => "add", "text" => "Add New Item", "class" => "addNewItem"];
            }

            $locationProductList = array();
            if ($documentType == 'invoice') {
                $flag = 'sales';
            } else if ($documentType == 'purchaseOrder') {
                $flag = 'purchase';
            } else if ($documentType == 'GRN') {
                $flag = 'purchase';
            } else if ($documentType == 'inventoryAdjusment') {
                $flag = 'purchase';
            } else if ($documentType == 'promotion') {
                $flag = 'sales';
            } else if ($documentType == 'transfer') {
                $flag = 'purchase';
            } else if ($documentType == 'purchaseInvoice') {
                $flag = 'purchase';
            } else if ($documentType == 'deliveryNote') {
                $flag = 'sales';
            } else if ($documentType == 'quotation') {
                $flag = 'sales';
            } else if ($documentType == 'salesOrder') {
                $flag = 'sales';
            } else if ($documentType == 'paymentVoucher') {
                $flag = 'purchase';
            } else if ($documentType == 'bomDocument') {
                $flag = 'purchase';
            } else if ($documentType == 'jobRawMaterials') {
                $flag = 'jobRawMaterials';
            } else if ($documentType == 'jobFixedAssets') {
                $flag = 'jobFixedAssets';
            }  else if ($documentType == 'eventFixedAssets') {
                $flag = 'eventFixedAssets';
            } else if ($documentType == 'jobTradingGoods') {
                $flag = 'jobTradingGoods';
            } else if ($documentType == 'taskCard') {
                $flag = 'taskCard';
            }

            foreach ($locationProducts as $locationProduct) {
                if ($flag == 'sales' && ($locationProduct['productHandelingManufactureProduct'] == 1 || $locationProduct['productHandelingSalesProduct'] == 1 || $locationProduct['productHandelingGiftCard'] == 1)) {

                    $attrID = (isset($_GET['attrID'])) ? $_GET['attrID'] : "null";
                    $attrValID = (isset($_GET['attrValID'])) ? $_GET['attrValID'] : "null";

                    if ($attrID != "null" && $attrValID != "null") {
                        $ruleRelatedProductArr = $this->CommonTable('Inventory\Model\ProductTable')->getProductsByAttributeID($attrID,$attrValID);

                        if(in_array($locationProduct['productID'], $ruleRelatedProductArr)) {
                            $temp['value'] = $locationProduct['productID'];
                            $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'] .' - '. floatval($locationProduct['locationProductQuantity'])/floatval($locationProduct['productUomConversion']) .'Qty';
                        }
                    } else {
                        $temp['value'] = $locationProduct['productID'];
                        $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'] .' - '. floatval($locationProduct['locationProductQuantity'])/floatval($locationProduct['productUomConversion']) .'Qty';
                    }

                } else if ($flag == 'purchase' && ($locationProduct['productHandelingManufactureProduct'] == 1 || $locationProduct['productHandelingPurchaseProduct'] == 1 || $locationProduct['productHandelingConsumables'] == 1 || $locationProduct['productHandelingFixedAssets'] == 1)) {
                    $temp['value'] = $locationProduct['productID'];
                    $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'].' - '. floatval($locationProduct['locationProductQuantity'])/floatval($locationProduct['productUomConversion']).'Qty';;
                } else if ($flag == "jobRawMaterials" && ($locationProduct['productHandelingPurchaseProduct'] == 1) && ($locationProduct['batchProduct'] == "0" && $locationProduct['serialProduct'] == "0")) {
                    $temp['value'] = $locationProduct['productID'];
                    $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'];
                } else if ($flag == "jobFixedAssets" && ($locationProduct['productHandelingFixedAssets'] == 1) && ($locationProduct['batchProduct'] == "0" && $locationProduct['serialProduct'] == "0")) {
                    $temp['value'] = $locationProduct['productID'];
                    $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'];
                }  else if ($flag == "eventFixedAssets" && ($locationProduct['productHandelingFixedAssets'] == 1) && ($locationProduct['serialProduct'] == "1")) {
                    $temp['value'] = $locationProduct['productID'];
                    $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'];
                } else if ($flag == "jobTradingGoods" && ($locationProduct['productHandelingManufactureProduct'] == 1 || $locationProduct['productHandelingSalesProduct'] == 1) && ($locationProduct['batchProduct'] == "0" && $locationProduct['serialProduct'] == "0")) {
                    $temp['value'] = $locationProduct['productID'];
                    $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'];
                } else if ($flag == "taskCard" && ($locationProduct['batchProduct'] == "0" && $locationProduct['serialProduct'] == "0")) {
                    $temp['value'] = $locationProduct['productID'];
                    $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'];
                }
                if (!in_array($temp,$locationProductList)) {
                    $locationProductList[] = $temp;
                }
            }

            // If "param1" set to serial-search, search will run through serial numbers and barcodes.
            // If serial-search request has been made, Sends separate array to map product ID against
            // serial number, under array attribute named "extra"
            if ($this->params('param1') == 'serial-search') {
                $serialProductsList = Array();
                $serialProducts = $this->CommonTable('Inventory\Model\LocationProductTable')->locationProductSerialSearch($locationID, $productSearchKey, 50 - $locationProducts->count());
                foreach ($serialProducts as $serialProduct) {
                    $serialProductsList[] = array(
                        'value' => $serialProduct['productID'],
                        'text' => $serialProduct['productSerialCode'],
                        'srl_id' => $serialProduct['productSerialID'],
                        'entity_type' => 'serial'
                    );
                }

                $allItems = array_merge($locationProductList, $serialProductsList);
                $mapper = Array();

                for ($i = 0; $i < count($allItems); $i++) {
                    $list[] = array(
                        'value' => 'pid_' . $i,
                        'text' => $allItems[$i]['text'],
                    );
                    $mapper['pid_' . $i]['pid'] = $allItems[$i]['value'];

                    if ($allItems[$i]['srl_id']) {
                        $mapper['pid_' . $i]['srl_id'] = $allItems[$i]['srl_id'];
                        $mapper['pid_' . $i]['type'] = $allItems[$i]['entity_type'];
                    }
                }
            } else {
                $list = $locationProductList;
            }

            $this->data = array(
                'list' => $list,
                'otherOptions' => $otherOptions,
                'extra' => $mapper
            );

            $this->setLogMessage("Retrive Product list for dropdown.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * search non inventory active produts for dropdown
     */
    public function searchNonInventoryActiveProductsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $productSearchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            if (!$locationID) {
                $locationID = $this->user_session->userActiveLocation['locationID'];
            }
            $addFlag = $searchrequest->getPost('addFlag');
            $nonInventoryProducts = $this->CommonTable('Inventory\Model\ProductTable')->getActiveNonInventoryItemsBySearchKey($locationID, $productSearchKey);

            $nonInventoryProductList = array();
            if (isset($addFlag) && $addFlag == 1) {
                $otherOptions[] = ["value" => "add", "text" => "Add New Item", "class" => "addNewItem"];
            }

            foreach ($nonInventoryProducts as $locationProduct) {
                $temp['value'] = $locationProduct['productID'];
                $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'];
                $nonInventoryProductList[] = $temp;
            }

            $this->data = array('list' => $nonInventoryProductList, 'otherOptions' => $otherOptions);
            return $this->JSONRespond();
        }
    }

    /**
     * Search User Active locations inventory product list for dropdown
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchUserActiveLocationProductsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $productSearchKey = $searchrequest->getPost('searchKey');
            $activeLocationIDs = array_keys($this->allLocations);
            $locationProducts = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown($activeLocationIDs, $productSearchKey, $isSearch = TRUE, $withNonInventory = TRUE);

            $locationProductList = array();
            foreach ($locationProducts as $locationProduct) {
                $temp['value'] = $locationProduct['productID'];
                $temp['text'] = $locationProduct['productName'] . '-' . $locationProduct['productCode'];
                $locationProductList[] = $temp;
            }

            $this->data = array('list' => $locationProductList);
            $this->setLogMessage("Retrive active loaction product for dropdown.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to find Uom Convertion Rate that given products
     *
     */
    public function getUomConversionValueAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $uomValue = $request->getPost('uomValue');
            $productID = $request->getPost('productID');
            for ($i = 1; $i < sizeof($uomValue) + 1; $i++) {
                $conversionFactor = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($productID, $uomValue[$i]);
                $proConFactor[$i] = $conversionFactor;
            }
            $this->data = $proConFactor;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * Get product name list for a location or location set
     */
    public function getProductsForLocationsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locations = $request->getPost('locationID');
            $productSearchKey = $request->getPost('searchKey');
            $productList = array();
            $products = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown($locations, $productSearchKey, false, true);
            $temp = array();
            foreach ($products as $product) {
                $temp['value'] = $product['productID'];
                $temp['text'] = $product['productName'] . '-' . $product['productCode'];
                $productList[] = $temp;
            }
            $this->data = array('list' => $productList);
            return $this->JSONRespond();
        }
    }

    /**
     * Get serial product list for dropdown
     * @return type
     */
    public function searchSerialProductsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');

            $products = $this->CommonTable('Inventory\Model\ProductTable')->getSerialProductList($searchKey);

//$locationProducts = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown($activeLocationIDs, $productSearchKey, $isSearch = TRUE);

            $productList = array();
            foreach ($products as $product) {
                $temp['value'] = $product['productID'];
                $temp['text'] = $product['productName'].'-'.$product['productCode'];
                $productList[] = $temp;
            }

            $this->data = array('list' => $productList);
            return $this->JSONRespond();
        }
    }

    /**
     * Get batch and serial product list for dropdown
     * @return type
     */
    public function searchBatchSerialProductsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');

            $products = $this->CommonTable('Inventory\Model\ProductTable')->getBatchAndSerialProductListBySearchKey($searchKey);

            $productList = array();
            foreach ($products as $product) {
                $temp['value'] = $product['productID'];
                $temp['text'] = $product['productName'].'-'.$product['productCode'];
                $productList[] = $temp;
            }

            $this->data = array('list' => $productList);
            return $this->JSONRespond();
        }
    }

    /**
     * Get product serial list for dropdown
     * @return JsonModel
     */
    public function searchProductSerialsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $serials = $this->CommonTable('Inventory\Model\ProductSerialTable')->searchProductSreialsForDropdown($searchKey);

            $serialList = array();
            foreach ($serials as $serial) {
                $temp['value'] = $serial['productSerialID'];
                $temp['text'] = $serial['productSerialCode'];
                $serialList[] = $temp;
            }

            $this->data = array('list' => $serialList);
            return $this->JSONRespond();
        }
    }

    //get products by location ID
    public function getProductsByLocationIdsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getActiveProductsFromLocation($locationID);
            $LocaProduct = array();
            foreach ($locationProduct as $locPro) {
                $locPro = (object) $locPro;
                if (!isset($LocaProduct[$locPro->productID])) {
                    $LocaProduct[$locPro->productID] = $locPro;
                }
            }
            if ($locationProduct) {
                $this->status = true;
                $this->msg = $this->getMessage('PRDCTAPI_EMPTY');
                $this->data = $LocaProduct;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_LOC');
            }
            return $this->JSONRespond();
        }
    }

    //save item price List
    public function saveItemPriceListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $priceListName = $request->getPost('priceListName');
            $locations = $request->getPost('locationIDs');
            $productData = json_decode($request->getPost('productData'), true);
            $priceListGlobal = $request->getPost('priceListGlobal');
            $checkPriceList = $this->CommonTable('Inventory\Model\PriceListTable')->getPriceListByPriceListName($priceListName);
            if ($checkPriceList == null) {
                $this->beginTransaction();
                $data = array(
                    'priceListName' => $priceListName,
                    'entityID' => $this->createEntity(),
                    'priceListState' => 1,
                    'priceListGlobal' => $priceListGlobal,
                );
                $priceList = new PriceList();
                $priceList->exchangeArray($data);
                $priceListId = $this->CommonTable('Inventory\Model\PriceListTable')->savePriceList($priceList);

                if ($priceListId) {
                    $flag = 1;
                    foreach ($locations as $loca) {
                        $prlocationData = array(
                            'priceListId' => $priceListId,
                            'locationId' => $loca
                        );
                        $priceListLocations = new PriceListLocations();
                        $priceListLocations->exchangeArray($prlocationData);
                        $result = $this->CommonTable('Inventory\Model\PriceListLocationsTable')->savePriceListLocations($priceListLocations);
                        $flag = ($result == false) ? 0 : $flag;
                    }

                    if ($flag == 1) {
                        $flag1 = 1;
                        foreach ($productData as $prod) {
                            $pro = (object) $prod;
                            $plProductData = array(
                                'priceListId' => $priceListId,
                                'productId' => $pro->productID,
                                'priceListItemsPrice' => $pro->productPrice,
                                'priceListItemsDiscountType' => $pro->productDiscountType,
                                'priceListItemsDiscount' => $pro->productDiscount,
                            );
                            $priceListItems = new PriceListItems();
                            $priceListItems->exchangeArray($plProductData);
                            $result1 = $this->CommonTable('Inventory\Model\PriceListItemsTable')->savePriceListItems($priceListItems);
                            $flag1 = ($result1 == false) ? 0 : $flag1;
                        }
                        if ($flag1 == 1) {
                            $this->commit();
                            $this->status = true;
                            $this->msg = $this->getMessage('SUCC_SAVE_PRICE_LIST');
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PRICE_LIST_SAVE_ITEMS');
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PRICE_LIST_SAVE_LOCATION');
                    }
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PRICE_LIST_SAVE_NAME');
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRICE_LIST_NAME_ALREDY_EXIST');
            }
            return $this->JSONRespond();
        }
    }

    //update item price List
    public function updateItemPriceListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $priceListId = $request->getPost('priceListId');
            $productData = json_decode($request->getPost('productData'), true);

            if ($priceListId) {
                //get price list product ids
                $itemList = $this->CommonTable('Inventory\Model\PriceListItemsTable')->getPriceListProductListByPriceListId($priceListId);
                $this->beginTransaction();
                try {
                    foreach ($productData as $prod) {
                        $pro = (object) $prod;
                        if (in_array($pro->productID, $itemList)) {
                            $plProductData = array(
                                'priceListId' => $priceListId,
                                'productId' => $pro->productID,
                                'priceListItemsPrice' => $pro->productPrice,
                                'priceListItemsDiscountType' => $pro->productDiscountType,
                                'priceListItemsDiscount' => $pro->productDiscount,
                            );
                            $priceListItems = new PriceListItems();
                            $priceListItems->exchangeArray($plProductData);
                            $this->CommonTable('Inventory\Model\PriceListItemsTable')->updatePriceListItems($priceListItems);
                        } else {
                            $plProductData = array(
                                'priceListId' => $priceListId,
                                'productId' => $pro->productID,
                                'priceListItemsPrice' => $pro->productPrice,
                                'priceListItemsDiscountType' => $pro->productDiscountType,
                                'priceListItemsDiscount' => $pro->productDiscount,
                            );
                            $priceListItems = new PriceListItems();
                            $priceListItems->exchangeArray($plProductData);
                            $result = $this->CommonTable('Inventory\Model\PriceListItemsTable')->savePriceListItems($priceListItems);
                        }
                    }
                    $this->commit();
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_UPDATE_PRICE_LIST');

                } catch (Exception $ex) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PRICE_LIST_UPDATE_ITEMS');
                }
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRICE_LIST_ID');
            }
            return $this->JSONRespond();
        }
    }

    //inactive item price List
    public function activeInactivePriceListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $priceListId = $request->getPost('priceListId');
            $priceListState = $request->getPost('priceListState');
            if ($priceListState == 0) {
                $success = $this->getMessage('SUCC_INACTIVE_PRICE_LIST');
                $error = $this->getMessage('ERR_INACTIVE_PRICE_LIST');
            } else {
                $success = $this->getMessage('SUCC_ACTIVE_PRICE_LIST');
                $error = $this->getMessage('ERR_ACTIVE_PRICE_LIST');
            }
            if ($priceListId) {
                $this->beginTransaction();
                $result = $this->CommonTable('Inventory\Model\PriceListTable')->activeInactivePriceList($priceListId, $priceListState);
                if ($result) {
                    $this->commit();
                    $this->status = true;
                    $this->msg = $success;
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $error;
                }
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRICE_LIST_ID');
            }
            return $this->JSONRespond();
        }
    }

    //delete price List
    public function deletePriceListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $priceListId = $request->getPost('priceListId');
            if ($priceListId) {
                $this->beginTransaction();
                $priceListData = (object) $this->CommonTable('Inventory\Model\PriceListTable')->getPriceListByPriceListId($priceListId);
                $entityId = $priceListData->entityID;
                $result = $this->updateDeleteInfoEntity($entityId);
                if ($result) {
                    $this->commit();
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_DELETE_PRICE_LIST');
                    ;
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_DELETE_PRICE_LIST');
                    ;
                }
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRICE_LIST_ID');
            }
            return $this->JSONRespond();
        }
    }

    //get Price List Details By Price List Id
    public function getPriceListDetailsByPriceListIdAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $priceListId = $request->getPost('priceListId');

            //get all active products
            $products = $this->CommonTable('Inventory\Model\ProductTable')->getActiveProductList();
            $productArr = iterator_to_array($products);

            $productIds = array_map(function($product){
                return $product['productID'];
            }, $productArr);

            $productDetails = [];
            foreach ($productArr as $product) {
                $productDetails[$product['productID']] = (object) $product;
            }

            $priceListItemsData = $this->CommonTable('Inventory\Model\PriceListTable')->getPriceListItemsDataByPriceListId($priceListId);
            $itemsData = [];
            foreach ($priceListItemsData as $prItemData) {
                $prItemData = (object) $prItemData;
                $itemsData[] = $prItemData;
                unset($productDetails[$prItemData->productId]);
            }

            $prLocData = [];
            $priceListLocationsData = $this->CommonTable('Inventory\Model\PriceListLocationsTable')->getPriceListLocationsDataByPriceListId($priceListId);
            foreach ($priceListLocationsData as $prLocationData) {
                $prLocationData = (object) $prLocationData;
                $prLocData[] = $prLocationData;
            }

            if (isset($priceListItemsData) && isset($priceListLocationsData)) {
                $data = array(
                    'priceListLocationsData' => $prLocData,
                    'priceListItemsData' => array_merge($itemsData, $productDetails),
                );
                $this->data = $data;
                $this->status = true;
            } else {
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    public function getProductByCatagoriesAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }


        $productSearchKey = $request->getPost('searchKey');
        $categoryID = $request->getPost('documentType');
        $activeLocationIDs = array_keys($this->allLocations);

        if ($categoryID == "") {
            $categoryID = NULL;
        }

        $itemList = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown($activeLocationIDs, $productSearchKey, $isSearch = TRUE, $withNonInventory = TRUE, $categoryID);

        $locationProductList = array();
        foreach ($itemList as $item) {
            $temp['value'] = $item['productID'];
            $temp['text'] = $item['productName'] . '-' . $item['productCode'];
            $locationProductList[] = $temp;
        }

        $this->data = array('list' => $locationProductList);
        $this->setLogMessage("Retrive active loaction product for dropdown.");
        return $this->JSONRespond();


    }
    /**
    * this function use to get existing expire alert details
    **/
    public function getExpireAlertDetalsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $productID = $request->getPost('productID');

        $alerts = $this->CommonTable('Inventory\Model\ExpiryAlertNotificationTable')->getDetailsByProductID($productID);

        $items = array();
        foreach ($alerts['data'] as $item) {
            $items[] = $item['NumberOfDays'];
        }
        $this->data = $items;
        $this->status = $alerts['status'];
        return $this->JSONRespond();       
    }
  
    /**
    * This function use to check that the product has transactions
    **/
    public function getProductTransactionsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $productID = $request->getPost('productID');

        $productTransactions = $this->CommonTable('Inventory\Model\ProductTable')->getProductTransactionByPID($productID);

        $transactions = array();
        foreach ($productTransactions as $transaction) {
            $transactions[] = $transaction;
        }
        $this->data = $transactions;
        $this->status = true;
        return $this->JSONRespond();       
    }
   

   /**
    * This function use to check admin privileges for inventory type change
    **/
    public function getAdminPrivilegeForInventoryTypeChangeAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $username = trim($request->getPost('username'));
        $password = trim($request->getPost('password'));
        $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
        if ($user) {
            if ($user->roleID == '1') {
                $passwordData = explode(':', $user->userPassword);
                $storedPassword = $passwordData[0];
                $checkPassword = md5($password . $passwordData[1]);

                if ($storedPassword == $checkPassword) {
                    $this->status = true;
                    $this->msg = $this->getMessage('INFO_USERAPICONTRO_AUTH');
                    return $this->JSONRespond();

                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_USERAPICONTRO_INCPWD');
                    return $this->JSONRespond();
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_VIEWPAY_USER_PRIVILEGE');
                return $this->JSONRespond();
            }
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_VIEWPAY_INVALID_USRNAME_PWD');
            return $this->JSONRespond();
        }       
    }

    public function getBarCodesAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $productID = $request->getPost('productID');
        $barCodes = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getRelatedBarCodesByProductID($productID);

        // $resultData = [];
        foreach ($barCodes as $key => $value) {
            $resultData[] = [
                'itemBarCodeID' => $value['itemBarcodeID'],
                'itemBarCode' => $value['barCode']
            ];
        }
        $this->data = $resultData;
        $this->status = true;
        return $this->JSONRespond();      

    }

    public function getItemsByCategoryIDAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $catID = $request->getPost('catID');
        $items = $this->CommonTable('Inventory\Model\ProductTable')->getProductListByCategoryId($catID);

        foreach ($items as $key => $product) {
            $resultData[] = $product;
        }
        $this->data = $resultData;
        $this->status = true;
        return $this->JSONRespond();      

    }


    public function getProductBaseUomAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $proID = $request->getPost('productID');
        $uomData = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductBaseUom($proID);
       
        $this->data = $uomData;
        $this->status = true;
        return $this->JSONRespond();      

    }


    public function saveFreeIssueItemDetailsAction() {

        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $data = array(
            'majorProductID' => $request->getPost('majorProductID'),
            'majorQuantity' => floatval($request->getPost('majorQuantity')) ,
            'majorConditionType' => $request->getPost('majorItemCondition'),
            'selectedMajorUom' => $request->getPost('selectedMajorUom'),
            'locationID' => $locationID,
        );
        $freeIssueDetails = new FreeIssueDetails();
        $freeIssueDetails->exchangeArray($data);
        $freeIssueDetailsId = $this->CommonTable('Inventory\Model\FreeIssueDetailsTable')->saveFreeIssueDetails($freeIssueDetails);

    
        if ($freeIssueDetailsId) {
            foreach ($request->getPost('selectedProducts') as $key => $value) {
                $dt = array(
                    'freeIssueDetailsID' => $freeIssueDetailsId ,
                    'productID' => $value['productID'],
                    'quantity' => floatval($value['quantity']),
                    'selectedUom' => $value['selectedUom'],
                );

                $freeIssueItems = new FreeIssueItems();
                $freeIssueItems->exchangeArray($dt);
                $savefreeIssueItemId = $this->CommonTable('Inventory\Model\FreeIssueItemsTable')->saveFreeIssueItems($freeIssueItems);

                if (!$savefreeIssueItemId) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_FREE_ISSUE_ITEM_SAVE');
                    return $this->JSONRespond();

                }
            }
        } else {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_FREE_ISSUE_DETAILS_SAVE');
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_SAVE_FREE_ISSUE_ITEMS');
        return $this->JSONRespond();      

    } 

    public function editFreeIssueItemDetailsAction() {

        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $freeItemIssueDetailsID = $request->getPost('freeItemIssueDetailsID');
        $data = array(
            'majorQuantity' => floatval($request->getPost('majorQuantity')) ,
            'majorConditionType' => $request->getPost('majorItemCondition'),
            'selectedMajorUom' => $request->getPost('selectedMajorUom'),
        );


        $updateData = $this->CommonTable('Inventory\Model\FreeIssueDetailsTable')->updateFreeIssueDetails($freeItemIssueDetailsID, $data);

        $deleteFreeItems = $this->CommonTable('Inventory\Model\FreeIssueItemsTable')->deleteFreeIssueItems($freeItemIssueDetailsID);
    
        if ($deleteFreeItems) {
            foreach ($request->getPost('selectedProducts') as $key => $value) {
                $dt = array(
                    'freeIssueDetailsID' => $freeItemIssueDetailsID ,
                    'productID' => $value['productID'],
                    'quantity' => floatval($value['quantity']),
                    'selectedUom' => $value['selectedUom'],
                );

                $freeIssueItems = new FreeIssueItems();
                $freeIssueItems->exchangeArray($dt);
                $savefreeIssueItemId = $this->CommonTable('Inventory\Model\FreeIssueItemsTable')->saveFreeIssueItems($freeIssueItems);

                if (!$savefreeIssueItemId) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_FREE_ISSUE_ITEM_SAVE');
                    return $this->JSONRespond();

                }
            }
        } else {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_FREE_ISSUE_DETAILS_SAVE');
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_UPDATE_FREE_ISSUE_ITEMS');
        return $this->JSONRespond();      

    }


    public function getFreeItemDetailsAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $productID = $request->getPost('productID');

        $freeIssueDetail =  $this->CommonTable('Inventory\Model\FreeIssueDetailsTable')->getFreeItemDetailsByProductID($productID);

        if (empty($freeIssueDetail)) {
            $this->status = false;
            $this->msg = $this->getMessage('NO_ANY_FREE_ISSUE_ITEMS');
            return $this->JSONRespond();
        }

        $relatedfreeItems =  $this->CommonTable('Inventory\Model\FreeIssueItemsTable')->getRelatedFreeIssuesByFreeIssueDetailsID($freeIssueDetail['freeIssueDetailID']);
        

        $freeIssueDetail['relatedFreeItems'] = $relatedfreeItems;
        $freeIssueDetail['uomList'] = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomsListByProductID($productID);


        $majorProductUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductBaseUomID($productID);

        if ($majorProductUom != $freeIssueDetail['selectedMajorUom']) {
            $freeIssueDetail['displayQty'] = $this->getProductQuantityViaDisplayUom($freeIssueDetail['majorQuantity'], $freeIssueDetail['selectedMajorUom']);
        } else {
            $freeIssueDetail['displayQty'] = $freeIssueDetail['majorQuantity'];
        }


        foreach ($freeIssueDetail['relatedFreeItems'] as $key => $value) {
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductBaseUomID($value['productID']);
            $uomList = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);

            if ($productUom != $value['selectedUom']) {
                $freeIssueDetail['relatedFreeItems'][$key]['displayQty'] =  $this->getProductQuantityViaDisplayUom($value['quantity'], $uomList)['quantity'];

            } else {
                 $freeIssueDetail['relatedFreeItems'][$key]['displayQty'] = $value['quantity'];
            }
        }
      

        $this->status = true;
        $this->data = $freeIssueDetail;
        $this->msg = $this->getMessage('SUCC_SAVE_FREE_ISSUE_ITEMS');
        return $this->JSONRespond();      
    }

    public function getProductsForSearchForFreeIssueAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $fixedAssetFlag = ($request->getPost('fixedAssetFlag') == "false") ? false : true;
            if (trim($request->getPost('searchProductString')) != "") {
                $products = $this->CommonTable('Inventory\Model\ProductTable')->getProductsforSearch($request->getPost('searchProductString'), $locationID);
                $paginated = false;
            } else {
                $products = $this->getPaginatedProducts($fixedAssetFlag);
                $paginated = true;
            }


            //get itemIssueDetails
           $freeIssues =  $this->CommonTable('Inventory\Model\FreeIssueDetailsTable')->fetchAll();
           $freeItemsAddedItems = [];
           foreach ($freeIssues as $key => $value) {
                $freeItems =  $this->CommonTable('Inventory\Model\FreeIssueItemsTable')->getRelatedFreeIssuesByFreeIssueDetailsID($value['freeIssueDetailID']);

                if (sizeof($freeItems) > 0) {
                    $freeItemsAddedItems[] = $value['majorProductID'];
                }

           }

            $view = new ViewModel(array(
                'itemList' => $products,
                'paginated' => $paginated,
                'currentLoc' => $locationID,
                'freeItemsAddedItems' => $freeItemsAddedItems
            ));


            $view->setTerminal(true);
            $view->setTemplate('inventory/product/free-item-list-view');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getFreeIssueItemsForSalesInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productIDs = $request->getPost('productIDs');
            $relatedFreeItems = $request->getPost('relatedFreeItems');
            $locationID = $this->user_session->userActiveLocation['locationID'];

            $freeItemsData = [];
            foreach ($productIDs as $key => $value) {
                $freeItemsData[$value] = $relatedFreeItems[$value];
            }

            $tax = array(
                'freeItemTaxID' => '',
                'freeItemTaxName' => '',
                'freeItemTaxPrecentage' => '',
                'freeItemTaxAmount' => '',
            );
            $subProduct = array(
                'freeItemSubProductID' => '',
                'freeItemBatchID' => '',
                'freeItemBatchCode' => '',
                'freeItemSerialID' => '',
                'freeItemSerialCode' => '',
                'freeItemSubQuantity' => '',
            );

            $Products = array(
                'freeItemProductID' => '',
                'productID' => '',
                'freeItemProductPrice' => '',
                'freeItemProductDiscountEligible' => '',
                'freeItemProductDiscountPercentage' => '',
                'freeItemProductDiscountValue' => '',
                'freeItemProductQuantity' => '',
                'productCode' => '',
                'productName' => '',
                'tax' => '',
                'subProduct' => '',
                'locationID' => '',
                'locationProductID' => '',
            );

            $productIdForTax = null;

            foreach ($freeItemsData as $key => $value) {
                $t = (object) $value;

                    
                $salesReturnProductQty = 0;
                $returnSubProductSerial = array();
                $returnSubProductBatch = array();
                $salesReturnProductIDArray = array();


                $locationProductDetails = $this->getLocationProductDetails($locationID, $t->productID);
                $locationProducts[$t->productID] = $locationProductDetails;


                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID] = (object) $Products;
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->freeItemProductID = $t->freeIssueItemsID;
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->productID = $t->productID;
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->freeItemProductPrice = 0;
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->freeItemProductDiscountEligible = $locationProductDetails['dEL'];
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->freeItemProductDiscountPercentage = $locationProductDetails['dPR'];
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->freeItemProductDiscountValue = $locationProductDetails['dPR'];
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->freeItemProductQuantity = $t->quantity;
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->productCode = $t->productCode;
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->productName = $t->productName;
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->productType = $locationProductDetails['pT'];
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->locationID = $locationID;
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->locationProductID = $locationProductDetails['lPID'];
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->mrpType = $locationProductDetails['mrpType'];
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->mrpValue = $locationProductDetails['mrpValue'];
                $freeItemProduct[$t->freeIssueItemsID."_".$t->productID]->mrpPercentageValue = $locationProductDetails['mrpPercentageValue'];

                
            }
    

            $this->data = array(
                'status' => true,
                'freeItemProduct' => $freeItemProduct,
                'locationProducts' => $locationProducts,
                // 'customCurrencySymbol' => $customCurrencySymbol,
            );
            $this->status = true;

            // $this->setLogMessage("Retrive ".$locationProduct['pC']." Product Details");
            return $this->JSONRespond();
        }
    }
}
