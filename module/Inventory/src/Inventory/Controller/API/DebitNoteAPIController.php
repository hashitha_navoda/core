<?php

/**
 * @authora Ashan Madushka  <ashan@thinkcube.com>
 * This file contains debit Note API related controller functions
 */

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Inventory\Model\Supplier;
use Inventory\Model\DebitNote;
use Inventory\Model\DebitNoteProduct;
use Inventory\Model\DebitNoteSubProduct;
use Inventory\Model\DebitNoteProductTax;
use Inventory\Model\ItemOut;

class DebitNoteAPIController extends CoreController
{

    protected $userID;
    protected $user_session;
    protected $useAccounting;

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function saveDebitNoteDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierID = $request->getPost('supplierID');
            $supplierName = $request->getPost('supplierName');
            $sup = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
            $this->beginTransaction();
            
            if ($sup->supplierCode . '-' . $sup->supplierName == $supplierName) {
                $products = $request->getPost('products');
                $pVID = $request->getPost('pVID');
                $subProducts = $request->getPost('subProducts');
                $debitNoteCode = $request->getPost('debitNoteCode');
                $locationID = $request->getPost('locationID');
                $date = $this->convertDateToStandardFormat($request->getPost('date'));
                $totalPrice = trim($request->getPost('debitNoteTotalPrice'));
                $comment = $request->getPost('debitNoteComment');
                $paymentTermID = $request->getPost('paymentTermID');
                $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
                $debitNotePaymentEligible = 0;
                $debitBalance = 0;
                $oustandingBalance = 0;
                $refData = $this->getReferenceNoForLocation(11, $locationID);
                $refid = $refData["refNo"];
                $locationReferenceID = $refData["locRefID"];
                $grnIDsArray = [];
                //check debit note numer already exist in debit note table if exist give next number for debit  note code

                // check whether pi is edit or not
                $piStatus = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataByID($pVID);

                if (empty($piStatus)) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_DEBIT_PI_CANCELED');
                    return $this->JSONRespond();
                }


                if ($piStatus['status'] == "10") {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_DEBIT_PI_EDIT');
                    return $this->JSONRespond();
                }

                while ($debitNoteCode) {
                    if ($this->CommonTable('Inventory\Model\DebitNoteTable')->checkDebitNoteByCode($debitNoteCode)->current()) {
                        if ($locationReferenceID) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $debitNoteCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $this->setLogMessage("Error occured when create debit note ".$debitNoteCode.'.');
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_DEBIT_NOTE_CODE_EXIST');
                            return $this->JSONRespond();
                        }
                    } else {
                        break;
                    }
                }
                $this->setLogMessage("Error occured when create debit note ".$debitNoteCode.' .');

                $purchaseVoucher = (object) $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseVoucherByID($pVID);
                $debitNotes = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNotesByPvID($pVID);
                $pastDebitNoteTotal = 0;
                foreach ($debitNotes as $values) {
                    $value = (object) $values;
                    $pastDebitNoteTotal+=$value->debitNoteTotal;
                }
                $pvPayedAmount = $purchaseVoucher->purchaseInvoicePayedAmount;
                $pvTotalAmount = $purchaseVoucher->purchaseInvoiceTotal;
                $ammountDifferent = $pvTotalAmount - $pvPayedAmount - $pastDebitNoteTotal;

                if ($ammountDifferent <= $totalPrice) {
                    $debitNotePaymentEligible = 1;
                    if ($ammountDifferent < 0) {
                        $debitBalance = $totalPrice;
                    } else {
                        $debitBalance = $totalPrice - $ammountDifferent;
                        $oustandingBalance = $ammountDifferent;
                    }
                    $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->updateOverDueInvoiceStatus($pVID, 4);
                } else {
                    $oustandingBalance = $totalPrice;
                }

                $supplierCreditBalance = $sup->supplierCreditBalance + $debitBalance;
                $supplierCurrentBalance = $sup->supplierOutstandingBalance - $oustandingBalance;
                $supplierData = array(
                    'supplierID' => $supplierID,
                    'supplierCreditBalance' => $supplierCreditBalance,
                    'supplierOutstandingBalance' => $supplierCurrentBalance,
                );
                $supplier = new Supplier;
                $supplier->exchangeArray($supplierData);
                $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplierCurrentAndCreditBalance($supplier);

                $entityID = $this->createEntity();
                $debitNoteData = array(
                    'debitNoteCode' => $debitNoteCode,
                    'supplierID' => $supplierID,
                    'locationID' => $locationID,
                    'paymentTermID' => $paymentTermID,
                    'purchaseInvoiceID' => $pVID,
                    'debitNoteDate' => $date,
                    'debitNoteTotal' => $totalPrice,
                    'debitNoteComment' => $comment,
                    'statusID' => 3,
                    'debitNotePaymentEligible' => $debitNotePaymentEligible,
                    'debitNotePaymentAmount' => $debitBalance,
                    'entityID' => $entityID
                );

                $debitNote = new DebitNote;
                $debitNote->exchangeArray($debitNoteData);

                $debitNoteID = $this->CommonTable('Inventory\Model\DebitNoteTable')->saveDebitNote($debitNote);

                if(!$debitNoteID){
                   $this->rollback();
                   $this->status = false;
                   $this->msg = $this->getMessage('ERR_DEBIT_PRODUCTDETAIL_ADD');
                   return $this->JSONRespond();
                }

                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }

                foreach ($products as $key => $p) {
                    $batchProducts = $subProducts[$p['productID']];
                    $productID = $p['productIID'];
                    $pvProductID = $p['purchaseVoucherProductID'];
                    $loDebitNoteQty = $debitNoteQty = $p['debitNoteQuantity']['qty'];
                    $productType = $p['productType'];
                    if ($productType == 2) {
                        $loDebitNoteQty = 0;
                    }
                    $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationID);
                    $averageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
                    
                    if ($locationProduct->locationProductQuantity < $loDebitNoteQty && $p['documentTypeID']!= "10") {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DEBIT_NOTE_PRODUCT_SOLD', array($p['productCode']));
                        return $this->JSONRespond();
                    }

                    $locationProductData = array(
                        'locationProductQuantity' => $locationProduct->locationProductQuantity - $loDebitNoteQty,
                    );

                    if ($p['documentTypeID']!= "10") {
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductData, $productID, $locationID);
                    }

                    //calculate discount value
                    $discountValue = 0;
                    if ($p['productDiscountType'] == "precentage") {
                        $discountValue = $p['productPrice'] / 100 * $p['productDiscount'];
                    } else if ($p['productDiscountType'] == "value") {
                        $discountValue = $p['productDiscount'];
                    }

                    $debitNoteProductData = array(
                        'debitNoteID' => $debitNoteID,
                        'purchaseInvoiceProductID' => $pvProductID,
                        'productID' => $productID,
                        'debitNoteProductPrice' => $p['productPrice'],
                        'debitNoteProductDiscount' => $p['productDiscount'],
                        'debitNoteProductDiscountType' => $p['productDiscountType'],
                        'debitNoteProductTotal' => $p['productTotal'],
                        'debitNoteProductQuantity' => $debitNoteQty
                    );
                    $debitNoteProduct = new DebitNoteProduct;
                    $debitNoteProduct->exchangeArray($debitNoteProductData);
                    $debitNoteProductID = $this->CommonTable('Inventory\Model\DebitNoteProductTable')->saveDebitNoteProducts($debitNoteProduct);
                    
                    if(!$debitNoteProductID){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DEBIT_PRODUCTDETAIL_ADD');
                        return $this->JSONRespond();
                    }

                    if ($p['pTax']) {
                        foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                            $debitNoteProductTaxData = array(
                                'debitNoteProductID' => $debitNoteProductID,
                                'taxID' => $taxKey,
                                'debitNoteProductTaxPercentage' => $productTax['tP'],
                                'debitNoteProductTaxAmount' => $productTax['tA']
                            );
                            $debitNoteProductTax = new DebitNoteProductTax();
                            $debitNoteProductTax->exchangeArray($debitNoteProductTaxData);
                            $this->CommonTable('Inventory\Model\DebitNoteProductTaxTable')->saveDebitNoteProductTax($debitNoteProductTax);
                        }
                    }

                    if (!count($batchProducts) > 0) {
                        if ($p['documentTypeID'] == "10") {
                            $grnProductData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductDetailsByGrnProductID($p['documentID'])->current();
                            $newCopiedQty = $grnProductData['grnProductCopiedQuantity'] - $p['debitNoteQuantity']['qty'];
                            $newTotalCopiedQty = NULL;
                            if (!is_null($grnProductData['grnProductTotalCopiedQuantity'])) {
                                $newTotalCopiedQty = $grnProductData['grnProductTotalCopiedQuantity'] - $p['debitNoteQuantity']['qty'];
                            }

                            $grnIDsArray[] = $grnProductData['grnID'];
                            $data = array(
                                    'grnProductCopiedQuantity' => $newCopiedQty,
                                    'grnProductTotalCopiedQuantity' => $newTotalCopiedQty,
                                    'copied' => 0,
                                );
                            $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $p['documentID']);
                        }
                        $sellingQty = $debitNoteQty;
                        while ($sellingQty != 0) {
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($locationProduct->locationProductID);
                            if (!$itemInDetails) {
                                break;
                            }
                            if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Debit Note',
                                    'itemOutDocumentID' => $debitNoteID,
                                    'itemOutLocationProductID' => $locationProduct->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $sellingQty,
                                    'itemOutPrice' => $p['productPrice'],
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                        
                                if ($p['documentTypeID'] != "10") {
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                }
                                $sellingQty = 0;
                                break;
                            } else {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Debit Note',
                                    'itemOutDocumentID' => $debitNoteID,
                                    'itemOutLocationProductID' => $locationProduct->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                    'itemOutPrice' => $p['productPrice'],
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                                if ($p['documentTypeID']!= "10") {
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                }
                                $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                            }
                        }
                    }
                    if (count($batchProducts) > 0) {
                        foreach ($batchProducts as $batchKey => $batchValue) {
                            $result = $this->saveDebitNoteSubProductData($batchValue, $debitNoteProductID);

                            if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {

                                // validate debit note batch quantities against existing purchase invoice batch quantities
                                $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                                if ($batchProduct->productBatchQuantity < $batchValue['qtyByBase']) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_DEBIT_NOTE_PRODUCT_QTY_EXCEED', [$batchProduct->productBatchQuantity, $p['productCode'] . ' (' . $p['productName'] . ')']);
                                    return $this->JSONRespond();
                                }

                                $productBatchQuentity = $batchProduct->productBatchQuantity - $batchValue['qtyByBase'];
                                $productBatchQty = array(
                                    'productBatchQuantity' => $productBatchQuentity,
                                );
                                if ($p['documentTypeID'] != "10") {
                                    $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                                }

                                if ($batchValue['serialID']) {
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProduct->locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                    $itemOutM = new ItemOut();
                                    $itemOutData = array(
                                        'itemOutDocumentType' => 'Debit Note',
                                        'itemOutDocumentID' => $debitNoteID,
                                        'itemOutLocationProductID' => $locationProduct->locationProductID,
                                        'itemOutBatchID' => $batchValue['batchID'],
                                        'itemOutSerialID' => $batchValue['serialID'],
                                        'itemOutItemInID' => $itemInDetails['itemInID'],
                                        'itemOutQty' => $batchValue['qtyByBase'],
                                        'itemOutPrice' => $p['productPrice'],
                                        'itemOutDiscount' => $discountValue,
                                        'itemOutAverageCostingPrice' => $averageCostingPrice,
                                        'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                    $itemOutM->exchangeArray($itemOutData);
                                    if ($p['documentTypeID'] != "10") {
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                        $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    }

                                    if ($p['documentTypeID']== "10") {
                                        $grnProductData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductDetailsByGrnProductID($batchValue['subDocumentTypeID'])->current();
                                        if ($grnProductData['productSerialID'] == $batchValue['serialID']) {
                                            $newSerialCopiedQty = $grnProductData['grnProductCopiedQuantity'] - $batchValue['qtyByBase'];
                                            $newSerialTotalCopiedQty = NULL;
                                            if (!is_null($grnProductData['grnProductTotalCopiedQuantity'])) {
                                                $newSerialTotalCopiedQty = $grnProductData['grnProductTotalCopiedQuantity'] - $p['debitNoteQuantity']['qty'];
                                            }

                                            $grnIDsArray[] = $grnProductData['grnID'];
                                            $data = array(
                                                    'grnProductCopiedQuantity' => $newSerialCopiedQty,
                                                    'grnProductTotalCopiedQuantity' => $newSerialTotalCopiedQty,
                                                    'copied' => 0,
                                                );
                                            $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $batchValue['subDocumentTypeID']);
                                        }
                                    }
                                } else {
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProduct->locationProductID, $batchValue['batchID']);
                                    $itemOutM = new ItemOut();
                                    $itemOutData = array(
                                        'itemOutDocumentType' => 'Debit Note',
                                        'itemOutDocumentID' => $debitNoteID,
                                        'itemOutLocationProductID' => $locationProduct->locationProductID,
                                        'itemOutBatchID' => $batchValue['batchID'],
                                        'itemOutSerialID' => NULL,
                                        'itemOutItemInID' => $itemInDetails['itemInID'],
                                        'itemOutQty' => $batchValue['qtyByBase'],
                                        'itemOutPrice' => $p['productPrice'],
                                        'itemOutDiscount' => $discountValue,
                                        'itemOutAverageCostingPrice' => $averageCostingPrice,
                                        'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                    $itemOutM->exchangeArray($itemOutData);
                                    if ($p['documentTypeID']!= "10") {
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                        $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    }

                                    if ($p['documentTypeID']== "10") {
                                        $grnProductData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductDetailsByGrnProductID($batchValue['subDocumentTypeID'])->current();
                                        if ($grnProductData['productBatchID'] == $batchValue['batchID']) {
                                            $newBatchCopiedQty = $grnProductData['grnProductCopiedQuantity'] - $batchValue['qtyByBase'];
                                            $newBatchTotalCopiedQty = NULL;
                                            if (!is_null($grnProductData['grnProductTotalCopiedQuantity'])) {
                                                $newBatchTotalCopiedQty = $grnProductData['grnProductTotalCopiedQuantity'] - $p['debitNoteQuantity']['qty'];
                                            }

                                            $grnIDsArray[] = $grnProductData['grnID'];
                                            $data = array(
                                                    'grnProductCopiedQuantity' => $newBatchCopiedQty,
                                                    'grnProductTotalCopiedQuantity' => $newBatchTotalCopiedQty,
                                                    'copied' => 0,
                                                );
                                            $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $batchValue['subDocumentTypeID']);
                                        }
                                    }
                                }
                            } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {

                                // check whether serial product exist
                                $result = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductDetails($batchValue['serialID']);
                                if ($result['productSerialSold'] && !$result['productSerialReturned']) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_DEBIT_NOTE_SERIAL_PRODUCT_NOT_EXIST', [$p['productCode'] . ' (' . $p['productName'] . ')', $result['productSerialCode']]);
                                    return $this->JSONRespond();
                                }

                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProduct->locationProductID, $batchValue['serialID']);
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Debit Note',
                                    'itemOutDocumentID' => $debitNoteID,
                                    'itemOutLocationProductID' => $locationProduct->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => $batchValue['serialID'],
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => 1,
                                    'itemOutPrice' => $p['productPrice'],
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );

                                $itemOutM->exchangeArray($itemOutData);
                                if ($p['documentTypeID']!="10") {
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);
                                }

                                if ($p['documentTypeID']== "10") {
                                    $grnProductData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductDetailsByGrnProductID($batchValue['subDocumentTypeID'])->current();
                                    if ($grnProductData['productSerialID'] == $batchValue['serialID']) {
                                        $newSerialCopiedQty = $grnProductData['grnProductCopiedQuantity'] - $batchValue['qtyByBase'];
                                        $newSerialTotalCopiedQty = NULL;
                                        if (!is_null($grnProductData['grnProductTotalCopiedQuantity'])) {
                                            $newSerialTotalCopiedQty = $grnProductData['grnProductTotalCopiedQuantity'] - $p['debitNoteQuantity']['qty'];
                                        }

                                        $grnIDsArray[] = $grnProductData['grnID'];
                                        $data = array(
                                                'grnProductCopiedQuantity' => $newSerialCopiedQty,
                                                'grnProductTotalCopiedQuantity' => $newSerialTotalCopiedQty,
                                                'copied' => 0,
                                            );
                                        $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $batchValue['subDocumentTypeID']);
                                    }
                                }
                            }

                            if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                $serialProductData = array(
                                    'productSerialReturned' => '1',
                                );
                                if ($p['documentTypeID'] != "10") {
                                    $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                                }
                            }
                        }
                    }
                }

                if (!empty($grnIDsArray)) {
                    foreach (array_unique($grnIDsArray) as $key => $value) {
                        $this->checkAndUpdateGrnStatus($value);
                    }
                }

                //      call product updated event
                $productIDs = array_map(function($element) {
                    return $element['productIID'];
                }, $products);
                
                $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
                $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

                if ($this->useAccounting == 1) {
                    //check whether supplier payable account id is set or not
                    if(empty($sup->supplierPayableAccountID)){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PURINV_SUPPLIER_PAYABLE_ACCOUNT', array($sup->supplierName.' - '.$sup->supplierCode));
                        return $this->JSONRespond();
                    }
                    $supplierPayableAccountID = $sup->supplierPayableAccountID;

                    $accountProduct = array();
                    $accountProductByGrn = array();
                    $totalTaxAmount = 0;
                    $allProductTotal = 0;

                    foreach ($products as $key => $pro) {
                        if(isset($pro['pTax']['tL'])){
                            foreach ($pro['pTax']['tL'] as $keyid => $value) {
                                $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($keyid);
                                if(empty($taxData['taxPurchaseAccountID'])){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_TAX_PURCHASE_ACCOUNT', array($taxData['taxName']));
                                    return $this->JSONRespond();
                                }
                                //set tax gl accounts for the journal Entry
                                if(isset($accountTax[$taxData['taxPurchaseAccountID']]['total'])){
                                    $accountTax[$taxData['taxPurchaseAccountID']]['total'] += $value['tA'];
                                }else{
                                    $accountTax[$taxData['taxPurchaseAccountID']]['total'] = $value['tA'];
                                    $accountTax[$taxData['taxPurchaseAccountID']]['taxAccountID'] = $taxData['taxPurchaseAccountID'];
                                }
                            }
                        }

                        if(isset($pro['pTax']['tTA'])){
                            $totalTaxAmount += $pro['pTax']['tTA'];
                        }

                        //check whether Product Inventory Gl account id set or not
                        $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($pro['productIID']);
                        if(empty($pData['productInventoryAccountID'])){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                            return $this->JSONRespond();
                        }

                        //calculate product total without tax potion.
                        $productTotal = $pro['productPrice']*$pro['debitNoteQuantity']['qty'] * ((100 - $pro['productDiscount']) / 100);
                        $allProductTotal +=$productTotal;

                        //set gl accounts for the journal entry
                        if ($pro['documentTypeID'] != 10) {
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['total'])){
                                $accountProduct[$pData['productInventoryAccountID']]['total'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['total'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
                            }
                        } else {
                            if(isset($accountProductByGrn[$sup->supplierGrnClearingAccountID]['total'])){
                                $accountProductByGrn[$sup->supplierGrnClearingAccountID]['total'] += $productTotal;
                            }else{
                                $accountProductByGrn[$sup->supplierGrnClearingAccountID]['total'] = $productTotal;
                                $accountProductByGrn[$sup->supplierGrnClearingAccountID]['supplierGrnClearingAccountID'] = $sup->supplierGrnClearingAccountID;
                            }
                        }
                    }

                    $payableTotalAmount = $allProductTotal+$totalTaxAmount;
                    
                    $i=0;
                    $journalEntryAccounts = array();
                    foreach ($accountProduct as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] =$value['total'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Debit Note '.$debitNoteCode;
                        $i++;
                    }

                    foreach ($accountProductByGrn as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['supplierGrnClearingAccountID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] =$value['total'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Debit Note '.$debitNoteCode;
                        $i++;
                    }

                    if(isset($accountTax)){
                        foreach ($accountTax as $taxkey => $taxVal) {
                            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                            $journalEntryAccounts[$i]['financeAccountsID'] = $taxVal['taxAccountID'];
                            $journalEntryAccounts[$i]['financeGroupsID'] = '';
                            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $taxVal['total'];
                            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Debit Note '.$debitNoteCode;
                            $i++;
                        }
                    }

                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $supplierPayableAccountID;
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $payableTotalAmount;
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Debit Note '.$debitNoteCode.".";

                    //get journal entry reference number.
                    $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                    $jelocationReferenceID = $jeresult['locRefID'];
                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                    $journalEntryData = array(
                        'journalEntryAccounts' => $journalEntryAccounts,
                        'journalEntryDate' => $date,
                        'journalEntryCode' => $JournalEntryCode,
                        'journalEntryTypeID' => '',
                        'journalEntryIsReverse' => 0,
                        'journalEntryComment' => 'Journal Entry is posted when create Debit Note '.$debitNoteCode.".",
                        'documentTypeID' => 13,
                        'journalEntryDocumentID' => $debitNoteID,
                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                        );

                    $resultData = $this->saveJournalEntry($journalEntryData);

                    if($resultData['status']){
                        $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($debitNoteID,13, 3);
                        if(!$jEDocStatusUpdate['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->msg =$jEDocStatusUpdate['msg'];
                            return $this->JSONRespond();
                        }

                        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(12,$pVID);
                        $dimensionData = [];
                        foreach ($jEDimensionData as $value) {
                            if (!is_null($value['journalEntryID'])) {
                                $temp = [];
                                $temp['dimensionTypeId'] = $value['dimensionType'];
                                $temp['dimensionValueId'] = $value['dimensionValueID'];
                                $dimensionData[$debitNoteCode][] = $temp;
                            }
                        }
                        if (!empty($dimensionData)) {
                            $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$debitNoteCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                            if(!$saveRes['status']){
                                $this->rollback();
                                $this->status = false;
                                $this->msg =$saveRes['msg'];
                                $this->data =$saveRes['data'];
                                return $this->JSONRespond();
                            }   
                        }
                        $this->commit();
                        $this->status = true;
                        $this->data = array('debitNoteID' => $debitNoteID);
                        $this->msg = $this->getMessage('SUCC_DEBIT_PRODUCTDETAIL_ADD', array($debitNoteCode));
                        $this->setLogMessage($this->companyCurrencySymbol . $totalPrice . ' amount debit Note successfully created - ' . $debitNoteCode.' againts purchase invoice '.$purchaseVoucher->purchaseInvoiceCode.'.');
                        $this->flashMessenger()->addMessage($this->msg);
                    }else{
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $resultData['msg'];
                        $this->data = $resultData['data'];
                    }
                    return $this->JSONRespond();
                }else{
                    $this->commit();
                    $this->status = true;
                    $this->data = array('debitNoteID' => $debitNoteID);
                    $this->msg = $this->getMessage('SUCC_DEBIT_PRODUCTDETAIL_ADD', array($debitNoteCode));
                    $this->setLogMessage($this->companyCurrencySymbol . $totalPrice . ' amount debit Note successfully created - ' . $debitNoteCode.' againts purchase invoice '.$purchaseVoucher->purchaseInvoiceCode.'.');
                    $this->flashMessenger()->addMessage($this->msg);
                }
                return $this->JSONRespond();
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DEBIT_VALID_SUP');
                return $this->JSONRespond();
            }
        } else {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DEBIT_PRODUCT_SAVE');
            return $this->JSONRespond();
        }
    }

    /*
     * This function is used to save direct debit note
     * @param JSON request
     * @return JSON respond
     */
    public function saveDirectDebitNoteDetailsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DEBIT_PRODUCT_SAVE');
            return $this->JSONRespond();
        }
        $supplierID = $request->getPost('supplierID');
        $totalPrice = trim($request->getPost('debitNoteTotalPrice'));
        $sup = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
        $this->beginTransaction();
        
        if (is_null($sup->supplierCode)) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DEBIT_VALID_SUP');
            return $this->JSONRespond();
        }
        $debitNoteCode = $request->getPost('debitNoteCode');
        $locationID = $request->getPost('locationID');
        $refData = $this->getReferenceNoForLocation(11, $locationID);
        $locationReferenceID = $refData["locRefID"];

        //check debit note numer already exist in debit note table if exist give next number for debit  note code
        while ($debitNoteCode) {
            if (!$this->CommonTable('Inventory\Model\DebitNoteTable')->checkDebitNoteByCode($debitNoteCode)->current()) {
                break;
            }

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
                $debitNoteCode = $this->getReferenceNumber($locationReferenceID);
            } else {
                $this->setLogMessage("Error occured when create debit note ".$debitNoteCode.'.');
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DEBIT_NOTE_CODE_EXIST');
                return $this->JSONRespond();
            }

        }
        $this->setLogMessage("Error occured when create debit note ".$debitNoteCode.' .');

        $supplierCreditBalance = $sup->supplierCreditBalance + $totalPrice;
        $supplierCurrentBalance = $sup->supplierOutstandingBalance;
        
        $supplierData = array(
            'supplierID' => $supplierID,
            'supplierCreditBalance' => $supplierCreditBalance,
            'supplierOutstandingBalance' => $supplierCurrentBalance,
        );
        $supplier = new Supplier;
        $supplier->exchangeArray($supplierData);
        $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplierCurrentAndCreditBalance($supplier);

        $result = $this->storeDirectDebitNote($request->getPost(), $sup->supplierPayableAccountID);
        $this->status = $result['status'];
        $this->data = $result['data'];
        $this->msg = $result['msg'];
        return $this->JSONRespond();
    }

    /*
    * This function is used to save direct return note
    */
    public function storeDirectDebitNote($debitNoteData , $supplierPayableAccountID)
    {
        $products = $debitNoteData['products'];
        $subProducts = $debitNoteData['subProducts'];
        $debitNoteCode = $debitNoteData['debitNoteCode'];
        $locationID = $debitNoteData['locationID'];
        $dimensionData = $debitNoteData['dimensionData'];
        $date = $this->convertDateToStandardFormat($debitNoteData['date']);
        $totalPrice = trim($debitNoteData['debitNoteTotalPrice']);
        $comment = $debitNoteData['debitNoteComment'];
        $supplierID = $debitNoteData['supplierID'];
        $paymentTermID = $debitNoteData['paymentTermID'];
        $ignoreBudgetLimit = $debitNoteData['ignoreBudgetLimit'];
        $debitNotePaymentEligible = 0;
        $refData = $this->getReferenceNoForLocation(11, $locationID);
        $refid = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        
        $entityID = $this->createEntity();
        $debitNoteData = array(
            'debitNoteCode' => $debitNoteCode,
            'supplierID' => $supplierID,
            'locationID' => $locationID,
            'paymentTermID' => $paymentTermID,
            'debitNoteDate' => $date,
            'debitNoteTotal' => $totalPrice,
            'debitNoteComment' => $comment,
            'statusID' => 3,
            'debitNotePaymentEligible' => $debitNotePaymentEligible,
            'debitNotePaymentAmount' => 0,
            'entityID' => $entityID,
            'directDebitNoteFlag' => 1
        );

        $debitNote = new DebitNote;
        $debitNote->exchangeArray($debitNoteData);

        $debitNoteID = $this->CommonTable('Inventory\Model\DebitNoteTable')->saveDebitNote($debitNote);

        if(!$debitNoteID){
           $this->rollback();
           return ['status' => false, 'msg' => $this->getMessage('ERR_DEBIT_PRODUCTDETAIL_ADD'), 'data' => null];
        }

        if ($locationReferenceID) {
            $this->updateReferenceNumber($locationReferenceID);
        }

        //save direct debit note products
        $productResult = $this->saveDirectDebitNoteProducts($products, $subProducts, $locationID, $debitNoteID);
        if (!$productResult['status']) {
           return ['status' => $productResult['status'], 'msg' => $productResult['msg'], 'data' => $productResult['data']]; 
        }

        if ($this->useAccounting == 1) {
            //save journal entry for direct debit note
            $JEResult = $this->saveJournalEntryForDirectReturn($supplierPayableAccountID, $products, $debitNoteCode, $locationID, $date, $debitNoteID, $dimensionData, $ignoreBudgetLimit, $totalPrice);
           return ['status' => $JEResult['status'], 'msg' => $JEResult['msg'], 'data' => $JEResult['data']]; 
        }else{
            $this->commit();
            $this->setLogMessage($this->companyCurrencySymbol . $totalPrice . ' amount direct debit Note successfully created - ' . $debitNoteCode.'.');
            $this->flashMessenger()->addMessage($this->getMessage('SUCC_DEBIT_PRODUCTDETAIL_ADD', array($debitNoteCode)));
            return ['status' => true, 'msg' => $this->getMessage('SUCC_DEBIT_PRODUCTDETAIL_ADD', array($debitNoteCode)), 'data' => array('debitNoteID' => $debitNoteID)];
        }

    }

    /*
    * This function is used to save direct debit note products
    */
    public function saveDirectDebitNoteProducts($products, $subProducts, $locationID, $debitNoteID)
    {
        foreach ($products as $key => $p) {
            $batchProducts = $subProducts[$key];
            $productID = $p['productID'];
            $loDebitNoteQty = $debitNoteQty = $p['deliverQuantity']['qty'];

            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetails($productID, $locationID);

            $productType = $locationProduct->productTypeID;
            if ($productType == 2) {
                $loDebitNoteQty = 0;
            }
            $averageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
            
            if ($locationProduct->locationProductQuantity < $loDebitNoteQty) {
                $this->rollback();
                return ['status' => false, 'msg' => $this->getMessage('ERR_DEBIT_NOTE_PRODUCT_SOLD', array($p['productCode'])), 'data' => null];
            }

            $locationProductData = array(
                'locationProductQuantity' => $locationProduct->locationProductQuantity - $loDebitNoteQty,
            );
            $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductData, $productID, $locationID);

            //calculate discount value
            $discountValue = 0;
            if ($p['productDiscountType'] == "precentage") {
                $discountValue = $p['productPrice'] / 100 * $p['productDiscount'];
            } else if ($p['productDiscountType'] == "value") {
                $discountValue = $p['productDiscount'];
            }

            $debitNoteProductData = array(
                'debitNoteID' => $debitNoteID,
                'purchaseInvoiceProductID' => NULL,
                'productID' => $productID,
                'debitNoteProductPrice' => $p['productPrice'],
                'debitNoteProductDiscount' => $p['productDiscount'],
                'debitNoteProductDiscountType' => $p['productDiscountType'],
                'debitNoteProductTotal' => $p['productTotal'],
                'debitNoteProductQuantity' => $debitNoteQty
            );
            $debitNoteProduct = new DebitNoteProduct;
            $debitNoteProduct->exchangeArray($debitNoteProductData);
            $debitNoteProductID = $this->CommonTable('Inventory\Model\DebitNoteProductTable')->saveDebitNoteProducts($debitNoteProduct);
            
            if(!$debitNoteProductID){
                $this->rollback();
                return ['status' => false, 'msg' => $this->getMessage('ERR_DEBIT_PRODUCTDETAIL_ADD'), 'data' => null];
            }

            if ($p['pTax']) {
                foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                    $debitNoteProductTaxData = array(
                        'debitNoteProductID' => $debitNoteProductID,
                        'taxID' => $taxKey,
                        'debitNoteProductTaxPercentage' => $productTax['tP'],
                        'debitNoteProductTaxAmount' => $productTax['tA']
                    );
                    $debitNoteProductTax = new DebitNoteProductTax();
                    $debitNoteProductTax->exchangeArray($debitNoteProductTaxData);
                    $this->CommonTable('Inventory\Model\DebitNoteProductTaxTable')->saveDebitNoteProductTax($debitNoteProductTax);
                }
            }

            if (!count($batchProducts) > 0) {
                $sellingQty = $debitNoteQty;
                while ($sellingQty != 0) {
                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($locationProduct->locationProductID);
                    if (!$itemInDetails) {
                        break;
                    }
                    if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                        $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Direct Debit Note',
                            'itemOutDocumentID' => $debitNoteID,
                            'itemOutLocationProductID' => $locationProduct->locationProductID,
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => NULL,
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => $sellingQty,
                            'itemOutPrice' => $p['productPrice'],
                            'itemOutDiscount' => $discountValue,
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );
                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        $sellingQty = 0;
                        break;
                    } else {
                        $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Direct Debit Note',
                            'itemOutDocumentID' => $debitNoteID,
                            'itemOutLocationProductID' => $locationProduct->locationProductID,
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => NULL,
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                            'itemOutPrice' => $p['productPrice'],
                            'itemOutDiscount' => $discountValue,
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );
                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                    }
                }
            }
            if (count($batchProducts) > 0) {
                foreach ($batchProducts as $batchKey => $batchValue) {
                    $result = $this->saveDebitNoteSubProductData($batchValue, $debitNoteProductID);

                    if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {

                        // validate debit note batch quantities against existing purchase invoice batch quantities
                        $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                        if ($batchProduct->productBatchQuantity < $batchValue['qtyByBase']) {
                            $this->rollback();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DEBIT_NOTE_PRODUCT_QTY_EXCEED', [$batchProduct->productBatchQuantity, $p['productCode'] . ' (' . $p['productName'] . ')']), 'data' => null];
                        }

                        $productBatchQuentity = $batchProduct->productBatchQuantity - $batchValue['qtyByBase'];
                        $productBatchQty = array(
                            'productBatchQuantity' => $productBatchQuentity,
                        );
                        $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);

                        if ($batchValue['serialID']) {
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProduct->locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Direct Debit Note',
                                'itemOutDocumentID' => $debitNoteID,
                                'itemOutLocationProductID' => $locationProduct->locationProductID,
                                'itemOutBatchID' => $batchValue['batchID'],
                                'itemOutSerialID' => $batchValue['serialID'],
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $batchValue['qtyByBase'],
                                'itemOutPrice' => $p['productPrice'],
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        } else {
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProduct->locationProductID, $batchValue['batchID']);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Direct Debit Note',
                                'itemOutDocumentID' => $debitNoteID,
                                'itemOutLocationProductID' => $locationProduct->locationProductID,
                                'itemOutBatchID' => $batchValue['batchID'],
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $batchValue['qtyByBase'],
                                'itemOutPrice' => $p['productPrice'],
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        }
                    } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {

                        // check whether serial product exist
                        $result = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductDetails($batchValue['serialID']);
                        if ($result['productSerialSold'] && !$result['productSerialReturned']) {
                            $this->rollback();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DEBIT_NOTE_SERIAL_PRODUCT_NOT_EXIST', [$p['productCode'] . ' (' . $p['productName'] . ')', $result['productSerialCode']]), 'data' => null];
                        }

                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProduct->locationProductID, $batchValue['serialID']);
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Direct Debit Note',
                            'itemOutDocumentID' => $debitNoteID,
                            'itemOutLocationProductID' => $locationProduct->locationProductID,
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => $batchValue['serialID'],
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => 1,
                            'itemOutPrice' => $p['productPrice'],
                            'itemOutDiscount' => $discountValue,
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );

                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);
                    }

                    if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                        $serialProductData = array(
                            'productSerialReturned' => '1',
                        );
                        $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                    }
                }
            }
        }

        //      call product updated event
        $productIDs = array_map(function($element) {
            return $element['productID'];
        }, $products);
        
        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        return ['status' => true];
    }

    /*
    * This function is used to save journal entry for direct return
    * @param $products, $supplierpayableAcID
    * @return resultSet
    */
    public function saveJournalEntryForDirectReturn($supplierPayableAccountID, $products, $debitNoteCode, $locationID, $date, $debitNoteID, $dimensionData, $ignoreBudgetLimit, $totalPrice)
    {
        //check whether supplier payable account id is set or not
        if(empty($supplierPayableAccountID)){
            $this->rollback();
            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_SUPPLIER_PAYABLE_ACCOUNT', array($sup->supplierName.' - '.$sup->supplierCode)), 'data' => null];
        }

        $accountProduct = array();
        $totalTaxAmount = 0;
        $allProductTotal = 0;

        foreach ($products as $key => $pro) {
            if(isset($pro['pTax']['tL'])){
                foreach ($pro['pTax']['tL'] as $keyid => $value) {
                    $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($keyid);
                    if(empty($taxData['taxPurchaseAccountID'])){
                        $this->rollback();
                        return ['status' => false, 'msg' => $this->getMessage('ERR_TAX_PURCHASE_ACCOUNT', array($taxData['taxName'])), 'data' => null];
                    }
                    //set tax gl accounts for the journal Entry
                    if(isset($accountTax[$taxData['taxPurchaseAccountID']]['total'])){
                        $accountTax[$taxData['taxPurchaseAccountID']]['total'] += $value['tA'];
                    }else{
                        $accountTax[$taxData['taxPurchaseAccountID']]['total'] = $value['tA'];
                        $accountTax[$taxData['taxPurchaseAccountID']]['taxAccountID'] = $taxData['taxPurchaseAccountID'];
                    }
                }
            }

            if(isset($pro['pTax']['tTA'])){
                $totalTaxAmount += $pro['pTax']['tTA'];
            }

            //check whether Product Inventory Gl account id set or not
            $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($pro['productID']);
            if(empty($pData['productInventoryAccountID'])){
                $this->rollback();
                return ['status' => false, 'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])), 'data' => null];
            }

            //calculate product total without tax potion.
            $productTotal = $pro['productPrice']*$pro['deliverQuantity']['qty'] * ((100 - $pro['productDiscount']) / 100);
            $allProductTotal +=$productTotal;

            //set gl accounts for the journal entry
            if(isset($accountProduct[$pData['productInventoryAccountID']]['total'])){
                $accountProduct[$pData['productInventoryAccountID']]['total'] += $productTotal;
            }else{
                $accountProduct[$pData['productInventoryAccountID']]['total'] = $productTotal;
                $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
            }
        }

        $payableTotalAmount = $allProductTotal+$totalTaxAmount;

        $i=0;
        $journalEntryAccounts = array();
        foreach ($accountProduct as $key => $value) {
            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
            $journalEntryAccounts[$i]['financeGroupsID'] = '';
            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] =$value['total'];
            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Direct Debit Note '.$debitNoteCode;
            $i++;
        }

        if(isset($accountTax)){
            foreach ($accountTax as $taxkey => $taxVal) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $taxVal['taxAccountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $taxVal['total'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Direct Debit Note '.$debitNoteCode;
                $i++;
            }
        }

        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
        $journalEntryAccounts[$i]['financeAccountsID'] = $supplierPayableAccountID;
        $journalEntryAccounts[$i]['financeGroupsID'] = '';
        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $payableTotalAmount;
        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Direct Debit Note '.$debitNoteCode.".";

        //get journal entry reference number.
        $jeresult = $this->getReferenceNoForLocation('30', $locationID);
        $jelocationReferenceID = $jeresult['locRefID'];
        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

        $journalEntryData = array(
            'journalEntryAccounts' => $journalEntryAccounts,
            'journalEntryDate' => $date,
            'journalEntryCode' => $JournalEntryCode,
            'journalEntryTypeID' => '',
            'journalEntryIsReverse' => 0,
            'journalEntryComment' => 'Journal Entry is posted when create Direct Debit Note '.$debitNoteCode.".",
            'documentTypeID' => 13,
            'journalEntryDocumentID' => $debitNoteID,
            'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

        $resultData = $this->saveJournalEntry($journalEntryData);

        if($resultData['status']){
            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($debitNoteID,13, 3);
            if(!$jEDocStatusUpdate['status']){
                $this->rollback();
                return ['status' => false,
                            'msg' => $jEDocStatusUpdate['msg']
                        ];
            }

            if (!empty($dimensionData)) {
                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$debitNoteCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                if(!$saveRes['status']){
                    $this->rollback();
                    return ['status' => false,
                            'msg' => $saveRes['msg'],
                            'data' => $saveRes['data']
                        ];
                }   
            }

            $this->commit();
            $this->setLogMessage($this->companyCurrencySymbol . $totalPrice . ' amount direct debit Note successfully created - ' . $debitNoteCode.'.');
            $this->flashMessenger()->addMessage($this->getMessage('SUCC_DEBIT_PRODUCTDETAIL_ADD', array($debitNoteCode)));
            return ['status' => true, 'msg' => $this->getMessage('SUCC_DEBIT_PRODUCTDETAIL_ADD', array($debitNoteCode)), 'data' => array('debitNoteID' => $debitNoteID)];
        }else{
            $this->rollback();
            return ['status'=>false, 'msg' => $resultData['msg'], 'data' => $resultData['data']];            
        }
    }

    /*
     * This function is used to update grn status
     *
     */
    public function checkAndUpdateGrnStatus($grnID)
    {
        $grnProducts = $this->CommonTable('Inventory\Model\GrnProductTable')->getUnCopiedGrnProducts($grnID);
        if (!empty($grnProducts)) {
            $statusID = 3;
            $this->CommonTable('Inventory\Model\GrnTable')->updateGrnStatus(
                    $grnID, $statusID);
        }
    }

    function saveDebitNoteSubProductData($batchValue, $debitNoteProductID)
    {
        $data = array(
            'debitNoteProductID' => $debitNoteProductID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'debitNoteSubProductQuantity' => $batchValue['qtyByBase'],
        );
        $debitNoteSubProduct = new DebitNoteSubProduct;
        $debitNoteSubProduct->exchangeArray($data);
        $debitNoteSubProductID = $this->CommonTable('Inventory\Model\DebitNoteSubProductTable')->saveDebitNoteSubProduct($debitNoteSubProduct);
        return $debitNoteSubProductID;
    }

    public function retriveDebitNoteFromDebitNoteIDAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $debitNotID = $searchrequest->getPost('DebitNoteID');
            $DNCode = $searchrequest->getPost('DebitNoteCode');
            $debitNote = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteID($debitNotID);

            foreach ($debitNote as $t) {
                $debitNoteArray[$t['debitNoteID']] = (object) $t;
            }
            if ($debitNote->count() == 0) {
                $this->setLogMessage("Error occred when retrive debit note ".$DNCode." details.");
                return new JsonModel(array(FALSE));
            } else {
                $return = new ViewModel(array(
                    'debitNote' => $debitNoteArray,
                    'statuses' => $this->getStatusesList()
                ));
                $return->setTerminal(TRUE);
                $return->setTemplate('inventory/debit-note/debit-note-list');
                $this->setLogMessage("Retrive debit note ".$DNCode." for list view.");
                return $return;
            }
        }
    }

    public function retriveSupplierDebitNoteAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $sup_id = $request->getPost('supplierID');
            $supplierCode = $request->getPost('supplierCode');
            $debitNotes = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteBySupplierIDAndLocationID($sup_id, $locationID);
            foreach ($debitNotes as $t) {
                $debitNoteArray[$t['debitNoteID']] = (object) $t;
            }
            $supplierDebitNote = new ViewModel(array(
                'debitNote' => $debitNoteArray,
                'statuses' => $this->getStatusesList()
            ));
            $supplierDebitNote->setTerminal(TRUE);
            $supplierDebitNote->setTemplate('inventory/debit-note/debit-note-list');
            $this->setLogMessage("Retrive debit notes for list view by supplier ".$supplierCode.'.');
            return $supplierDebitNote;
        }
    }

    public function retriveDebitNoteByDatefilterAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromdate = $request->getPost('fromdate');
            $todate = $request->getPost('todate');
            $supplierID = $request->getPost('supplierID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $filteredDebitNotes = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNotesByDate($fromdate, $todate, $supplierID, $locationID);

            foreach ($filteredDebitNotes as $t) {
                $debitNotesArray[$t['debitNoteID']] = (object) $t;
            }
            $DateFilteredDebitNote = new ViewModel(array(
                'debitNote' => $debitNotesArray,
                'statuses' => $this->getStatusesList()
            ));

            $DateFilteredDebitNote->setTerminal(TRUE);
            $DateFilteredDebitNote->setTemplate('inventory/debit-note/debit-note-list');
            $this->setLogMessage("Retrive debit notes for list view by date filters.");
            return $DateFilteredDebitNote;
        }
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Debit Note';
        if ($request->isPost()) {
            $documentID = $request->getPost('documentID');
            $email = $request->getPost('to_email');
            $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteID($documentID)->current();
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_DEBITAPI_SENT_EMAIL'); //TODO : create messages according to module
            $this->setLogMessage($debitNoteData['debitNoteCode']." debit note document email to ".$email.'.');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_DEBITAPI_SENT_EMAIL');
        $this->setLogMessage("Error occured when email the debit note document.");
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search All Debit Notes for dropdown
     * @return type
     */
    public function searchDebitNoteForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $this->data = array('list' => $this->_searchDebitNoteForDropDown($searchKey));
            $this->setLogMessage("Retrive debit note list for dropdown.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * search Active Debit note for dropdown in debit note payments
     * @return type
     */
    public function searchActiveDebitNoteForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $this->data = array('list' => $this->_searchDebitNoteForDropDown($searchKey, true));
            $this->setLogMessage("Retrive active debit note list for dropdown.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search Debit note for dropdown
     * @param String $searchKey the string to search
     * @param boolean $isActive
     * @return array
     */
    private function _searchDebitNoteForDropDown($searchKey, $isActive = false)
    {
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $debitNotes = $this->CommonTable('Inventory\Model\DebitNoteTable')->searchDebitNoteForDropdown($searchKey, $locationID, $isActive);

        $debitNoteList = array();
        foreach ($debitNotes as $debitNote) {
            $temp['value'] = $debitNote['debitNoteID'];
            $temp['text'] = $debitNote['debitNoteCode'];
            $debitNoteList[] = $temp;
        }
        return $debitNoteList;
    }

    public function getAllRelatedDocumentDetailsByDebitNoteIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $debitNoteID = $request->getPost('debitNoteID');
            $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteDetailsByDebitNoteId($debitNoteID);
            $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataByDebitNoteId($debitNoteID);
            $piRelatedPoData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPiRelatedPODetailsByDebitNoteId($debitNoteID);
            $piRelatedGrnData = $this->CommonTable('Inventory\Model\GrnTable')->getPiRelatedGrnDetailsByDebitNoteId($debitNoteID);
            $grnRelatedPoData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getGrnRelatedPODetailsByDebitNoteId($debitNoteID);
            $grnRelatedPrData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPRDetailsByDebitNoteId($debitNoteID);
            $piPaymentData = $this->CommonTable('SupplierPaymentsTable')->getPiPaymentDetailsByPrDebitNoteId($debitNoteID);
            $debitNotePaymentData = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentDetailsByDebitNoteId($debitNoteID);

            $dataExistsFlag = false;
            if ($debitNoteData) {
                $debitNoteData = array(
                    'type' => 'DebitNote',
                    'documentID' => $debitNoteData['debitNoteID'],
                    'code' => $debitNoteData['debitNoteCode'],
                    'amount' => number_format($debitNoteData['debitNoteTotal'], 2),
                    'issuedDate' => $debitNoteData['debitNoteDate'],
                    'created' => $debitNoteData['createdTimeStamp'],
                );
                $debitNoteDetails[] = $debitNoteData;
                $dataExistsFlag = true;
            }
            if (isset($piData)) {
                foreach ($piData as $piDirectDta) {
                    $piDeta = array(
                        'type' => 'PurchaseInvoice',
                        'documentID' => $piDirectDta['purchaseInvoiceID'],
                        'code' => $piDirectDta['purchaseInvoiceCode'],
                        'amount' => number_format($piDirectDta['purchaseInvoiceTotal'], 2),
                        'issuedDate' => $piDirectDta['purchaseInvoiceIssueDate'],
                        'created' => $piDirectDta['createdTimeStamp'],
                    );
                    $debitNoteDetails[] = $piDeta;
                    if (isset($piDirectDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($piRelatedPoData)) {
                foreach ($piRelatedPoData as $poDta) {
                    $pOrderData = array(
                        'type' => 'PurchaseOrder',
                        'documentID' => $poDta['purchaseOrderID'],
                        'code' => $poDta['purchaseOrderCode'],
                        'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                        'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                        'created' => $poDta['createdTimeStamp'],
                    );
                    $debitNoteDetails[] = $pOrderData;
                    if (isset($prDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($grnRelatedPoData)) {
                foreach ($grnRelatedPoData as $poDta) {
                    $pOrderData = array(
                        'type' => 'PurchaseOrder',
                        'documentID' => $poDta['purchaseOrderID'],
                        'code' => $poDta['purchaseOrderCode'],
                        'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                        'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                        'created' => $poDta['createdTimeStamp'],
                    );
                    $debitNoteDetails[] = $pOrderData;
                    if (isset($prDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($piRelatedGrnData)) {
                foreach ($piRelatedGrnData as $grDta) {
                    $grnDta = array(
                        'type' => 'Grn',
                        'documentID' => $grDta['grnID'],
                        'code' => $grDta['grnCode'],
                        'amount' => number_format($grDta['grnTotal'], 2),
                        'issuedDate' => $grDta['grnDate'],
                        'created' => $grDta['createdTimeStamp'],
                    );
                    $debitNoteDetails[] = $grnDta;
                    if (isset($grDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            
            if (isset($grnRelatedPrData)) {
                foreach ($grnRelatedPrData as $prData) {
                    $prDta = array(
                        'type' => 'purchaseReturn',
                        'documentID' => $prData['purchaseReturnID'],
                        'code' => $prData['purchaseReturnCode'],
                        'amount' => number_format($prData['purchaseReturnTotal'], 2),
                        'issuedDate' => $prData['purchaseReturnDate'],
                        'created' => $prData['createdTimeStamp'],
                    );
                    $debitNoteDetails[] = $prDta;
                    if (isset($prData)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($piPaymentData)) {
                foreach ($piPaymentData as $piPayDta) {
                    $piPaymentData = array(
                        'type' => 'SupplierPayment',
                        'documentID' => $piPayDta['outgoingPaymentID'],
                        'code' => $piPayDta['outgoingPaymentCode'],
                        'amount' => number_format($piPayDta['outgoingPaymentAmount'], 2),
                        'issuedDate' => $piPayDta['outgoingPaymentDate'],
                        'created' => $piPayDta['createdTimeStamp'],
                    );
                    $debitNoteDetails[] = $piPaymentData;
                    if (isset($piPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($debitNotePaymentData)) {
                foreach ($debitNotePaymentData as $dNPayDta) {
                    $dNPaymentData = array(
                        'type' => 'DebitNotePayment',
                        'documentID' => $dNPayDta['debitNotePaymentID'],
                        'code' => $dNPayDta['debitNotePaymentCode'],
                        'amount' => number_format($dNPayDta['debitNotePaymentAmount'], 2),
                        'issuedDate' => $dNPayDta['debitNotePaymentDate'],
                        'created' => $dNPayDta['createdTimeStamp'],
                    );
                    $debitNoteDetails[] = $dNPaymentData;
                    if (isset($dNPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($debitNoteDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $debitNoteDetails);

            $documentDetails = array(
                'debitNoteDetails' => $debitNoteDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

}

/////////////////// END OF CUSTOMER API CONTROLLER \\\\\\\\\\\\\\\\\\\\