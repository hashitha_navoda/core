<?php

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Form\CustomerPaymentsForm;
use Invoice\Form\AdvancePaymentsForm;
use Inventory\Model\SupplierPaymentMethodNumbers;
use Zend\View\Model\JsonModel;
use Inventory\Model\PurchaseInvoice;
use Expenses\Model\PaymentVoucher;
use Expenses\Model\ChequePrint;
use Inventory\Model\Supplier;
use Zend\Session\Container;
use Accounting\Model\JournalEntryAccounts;

/**
 * @author Ashan madushka <ashan@thinkcube.com>
 */
class OutGoingPaymentController extends CoreController
{
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * this functoion related to outgoing payment creation
     */

    /** get supplier Invoices from the database * */
    public function invoiceAction()
    {
        $id = $_GET['id'];
        $arr1 = str_split($id, 8);
        $debitInvoiceAmount = [];

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $dateFormat = $this->getUserDateFormat();
        if ($arr1[0] == "supplie=") {
            $PVvalue = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchersBySupplierID($arr1[1], $locationID);
            $PIvalue = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBySupplierID($arr1[1], $locationID);
            foreach ($PVvalue as $val) {
                $value[] = (object) $val;
            }
            foreach ($PIvalue as $val) {
                $value[] = (object) $val;
            }

            $debitValue = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteBySupplierIDAndLocationID($arr1[1], $locationID);
            foreach ($debitValue as $val) {
                $datta = (object) $val;
                $debitInvoiceAmount[$datta->purchaseInvoiceID]+=$datta->debitNoteTotal;
            }
        } else if ($arr1[0] == "invoice=") {
            $purchaseInvoices = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($arr1[1]);
            $debitValue = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNotesByPvID($arr1[1]);
            foreach ($purchaseInvoices as $purchaseInvoice) {
                $value[] = (object) $purchaseInvoice;
            }
            foreach ($debitValue as $val) {
                $datta = (object) $val;
                $debitInvoiceAmount[$datta->purchaseInvoiceID] += $datta->debitNoteTotal;
            }
        } else if ($arr1[0] == "pVocher=") {
            $PVvalue = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherDetailsByPVID($arr1[1]);
            $pvId = array();
            foreach ($PVvalue as $val) {
                if(!in_array($val['paymentVoucherID'], $pvId)){
                    $value[] = (object) $val;
                    $pvId[] = $val['paymentVoucherID'];
                }
            }
        }

        $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        $payMethod = array();
        foreach ($paymentMethos as $key => $t) {
            if($t['paymentMethodInPurchase'] == 1){
                $payMethod[$t['paymentMethodID']] = $t['paymentMethodName'];
            }
        }

        $paymenteview = new ViewModel(array(
            'payments' => $value,
            'debitInvoice' => $debitInvoiceAmount,
            'paymentMethod' => $payMethod,
            'dateFormat' => $dateFormat
                )
        );

        $paymenteview->setTerminal(true);
        $paymenteview->setTemplate('inventory/out-going-payment-api/addPaymentList');
        if ($arr1[0] == "supplie=") {
            $this->setLogMessage('Supplier Invoice list accessed ');
        } else if ($arr1[0] == "invoice=") {
            $this->setLogMessage('Purchase Invoice accessed ');
        }else if($arr1[0] == "pVocher="){
            $this->setLogMessage('Retrive payment Voucher '.$value[0]->paymentVoucherCode.'.');
        }
        return $paymenteview;
    }

    public function checkInvoiceAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $invID = $request->getPost('invoiceID');
            $check = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($invID)->current();
            
            if ($check) {
                $this->status = true;
                $this->setLogMessage("Check purchase invoice ".$check->purchaseInvoiceCode.' status.');
            } else {
                $this->status = false;
                $this->setLogMessage("Check purchase invoice status.");
            }
            
            $this->data = $check;
            return $this->JSONRespond();
        }
    }

    /** get supplier ADMINCurrent Balance from the database * */
    public function checkCreditAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $id = $request->getPost('supplierID');
            $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($id);
            if ($supplier == null) {
                $this->setLogMessage("Supplier not fount by this id ");
                $this->data = array(null);
            } else {
                $cbalance = $supplier->supplierOutstandingBalance;
                $ccredit = $supplier->supplierCreditBalance;
                $supplierName = $supplier->supplierName . ' - ' . $supplier->supplierCode;
                $paymentTerm = $supplier->supplierPaymentTerm;
                $this->setLogMessage('Retrive supplier details '.$supplier->supplierCode.'.');
                $this->data = array($ccredit, $supplierName, $paymentTerm, $cbalance);
            }
        } else {
            $this->data = array(null);
            $this->setLogMessage("Credit check request is not a post request ");
        }
        $this->status = true;
        return $this->JSONRespond();
    }

    public function addAllPaymentAction()
    {
        $paymentdata = '';
        $invcdata = '';
        $supplierCreditUpdate = '';
        $supplierupdate = '';
        $invoicePayment = '';
        $debitNoteSupplierUpdate = '';
        $debitNoteUpdate = '';
        $refNumber = '';

        $request = $this->getRequest();

        if ($request->isPost()) {
            $supplierID = $request->getPost('supplierID');
            $pvAccountID = $request->getPost('pvAccountID');
            $debitNote = $request->getPost('creditNote');
            $invData = json_decode($request->getPost('invData'));
            $paymentCode = $request->getPost('paymentCode');
            $date = $this->convertDateToStandardFormat($request->getPost('date'));
            $paymentTerm = $request->getPost('paymentTerm');
            $amount = $request->getPost('amount');
            $creditAmount = (!empty($request->getPost('creditAmount'))) ? $request->getPost('creditAmount') : 0.00;
            $discount = $request->getPost('discount');
            $memo = $request->getPost('memo');
            $paymentType = $request->getPost('paymentType');
            $supplierCredit = $request->getPost('supplierCredit');
            $supplierBalance = $request->getPost('supplierBalance');
            $locationID = $request->getPost('locationID');
            $paymentdetails = json_decode($request->getPost('PaymentMethodDetails'));
            $dimensionData = (array) json_decode($request->getPost('dimensionData'));
            $rmCreditAmoFlag = $request->getPost('rmCreditBalance');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $dataSetForSpecialCase = $request->getPost();

            $refData = $this->getReferenceNoForLocation(13, $locationID);
            $locationReferenceID = $refData["locRefID"];
            $rid = $refData["refNo"];

            $autogenaratedCode = false;
            if($rid == $paymentCode){
                $autogenaratedCode = true;
            }
            $this->beginTransaction();

           
            foreach ($invData as $key  => $value) {

                $iddt = $key;
                $document = split('_', $iddt)[1];
                $id = split('_', $iddt)[0];

                if ($document == 'PI') {
                    $value = (object) $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($id)->current();
                    if ($date < $value->purchaseInvoiceIssueDate) {
                        $this->status = false;
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_CAN_NOT_PAY_FUTURE_PI');
                        return $this->JSONRespond();
                    }
                } elseif ($document == 'PV') {
                    $value = (object) $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherDetailsByPVID($id)->current();
                    if ($date < $value->paymentVoucherIssuedDate) {
                        $this->status = false;
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_CAN_NOT_PAY_FUTURE_PV');
                        return $this->JSONRespond();
                    }
                }
            }
         
            // check if a supplier payment from the same supplier payment Code exists if exist add next number to supplier payment code
            // while ($paymentCode) {
            //     if ($this->CommonTable('SupplierPaymentsTable')->checkSupplierPaymentByCode($paymentCode)->current()) {
            //         if ($locationReferenceID) {
            //             $newPaymentCode = $this->getReferenceNumber($locationReferenceID);
            //             if ($newPaymentCode == $paymentCode) {
            //                 $this->updateReferenceNumber($locationReferenceID);
            //                 $paymentCode = $this->getReferenceNumber($locationReferenceID);
            //             } else {
            //                 $paymentCode = $newPaymentCode;
            //             }
            //         } else {
                        // $this->setLogMessage("Error occured when create supplier payment ".$paymentCode.'.');
                        // $this->status = false;
                        // $this->rollback();
                        // $this->msg = $this->getMessage('ERR_OUTGPAYMENT_SCODE_EXIST');
                        // return $this->JSONRespond();
            //         }
            //     } else {
            //         break;
            //     }
            // }

            if ($this->CommonTable('SupplierPaymentsTable')->checkSupplierPaymentByCode($paymentCode)->current()) {
                if($autogenaratedCode){
                    while ($paymentCode) {
                        if ($this->CommonTable('SupplierPaymentsTable')->checkSupplierPaymentByCode($paymentCode)->current()) {
                            if ($locationReferenceID) {
                                $newPaymentCode = $this->getReferenceNumber($locationReferenceID);
                                if ($paymentCode == $newPaymentCode) {
                                    $this->updateReferenceNumber($locationReferenceID);
                                    $paymentCode = $this->getReferenceNumber($locationReferenceID);
                                } else {
                                    $paymentCode = $newPaymentCode;
                                }
                            } else {
                                return ['status' => false, 'msg'=>$this->getMessage('ERR_JOB_CODE_EXIST'), 'data' => null];
                            }
                        } else {
                            break;
                        }
                    }
                } else {
                    $this->rollback();
                    return new JsonModel(array('state' => false, 'msg' => $this->getMessage('ERR_OUTGPAYMENT_SCODE_EXIST')));
                }
            } 

            $this->setLogMessage("Error occured when create supplier payment ".$paymentCode.'.');
            
            $entityID = $this->createEntity();
            $paymentdata = array(
                'paymentID' => $paymentCode,
                'date' => $date,
                'paymentTerm' => $paymentTerm,
                'amount' => $amount,
                'supplierID' => (!empty($supplierID)) ? $supplierID : null,
                'discount' => $discount,
                'memo' => $memo,
                'paymentType' => $paymentType,
                'debitNote' => $debitNote,
                'locationID' => $locationID,
                'entityID' => $entityID,
                'creditAmount' => $creditAmount,
                'totalPaidAmount' => 0,
                'balanceAmount' => 0 
            );
            $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
            $supplierBalancesCheck = $currentbalancess = $supplier->supplierOutstandingBalance;
            $supplierCreditcheck = $currentCredit = $supplier->supplierCreditBalance;
            $totalCreditValue = 0;
            $currentbalance = $currentbalancess;
            $callocation = 0;
            $SBalanceallocation = 0;

            if ($supplierID) {
                if ($supplierCredit != $supplierCreditcheck) {
                    $this->rollback();
                    return new JsonModel(array('state' => false, 'msg' => $this->getMessage('ERR_OUTGPAYMENT_SUPPCEDITCHANGE') /* 'Transaction failed because supplier credit change,Please try again.' */, 'customerID' => $supplierID));
                } else if ($supplierBalance != $supplierBalancesCheck) {
                    $this->rollback();
                    return new JsonModel(array('state' => false, 'msg' => $this->getMessage('ERR_OUTGPAYMENT_FAILSUPPOUT')/* 'Transaction failed because supplier oustanding change,Please try again. */, 'customerID' => $supplierID));
                }
            }

            $locationCashAmount = 0;
            $paymentMethodAmounts = array();
            $totalPaymentMethodsAmount = $amount;
            $supplierTotalAmount = 0;
            $supplierAdvanceTotalAmount = 0;
            $totalPaidAmount = 0;


            // get whole cash payment amounts

            foreach ($paymentdetails as $key2 => $value2) {
                

                $totalPaidAmount += $value2->paidAmount;
                if ($value2->methodID == '1') {
                    $locationCashAmount += $value2->paidAmount;
                }

                if(isset($paymentMethodAmounts[$value2->methodID])){
                    $paymentMethodAmounts[$value2->methodID] += $value2->paidAmount;
                }else{
                    $paymentMethodAmounts[$value2->methodID] = $value2->paidAmount;
                }
            }

            $totalPaidAmount += $creditAmount;


            if (!is_null($creditAmount)) {
                $totalCreditValue = $creditAmount;
                $callocation = $currentCredit - $creditAmount;
                $supplierCreditUpdate[] = array(
                    'supplierID' => $supplierID,
                    'supplierCreditBalance' => $callocation,
                );
            } 

            foreach ($invData as $key77 => $value77) {
                
                if ($key77 != '') {
                    $iddt = $key77;
                    $setallocation = $value77;
                    // $creditallocation = json_decode($key)->creditallocation;
                    $payment = null;
                    $id = split('_', $iddt)[0];
                    $document = split('_', $iddt)[1];
                    if ($document == 'PV') {
                        $value = (object) $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherDetailsByPVID($id)->current();
                        if ($value->paymentVoucherStatus == "10" || $value->paymentVoucherStatus == "4") {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_EXPV_EDIT_OR_CLOSED_PAYMENT', array($value->paymentVoucherCode));
                            return $this->JSONRespond();    
                        }
                        $amount = $value->paymentVoucherTotal;
                        $supplierid = $value->paymentVoucherSupplierID;
                        $oldvalue = $value->paymentVoucherPaidAmount;
                        $pvId = $value->paymentVoucherID;
                        $invoiceId = '';
                    } else {
                        $value = (object) $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($id)->current();
                        $debitNotes = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPiDebitNoteTotalByPiId($id);
                        $amount = floatval($value->purchaseInvoiceTotal) - floatval($debitNotes['total']);
                        $supplierid = $value->purchaseInvoiceSupplierID;
                        $oldvalue = $value->purchaseInvoicePayedAmount;
                        $invoiceId = $value->purchaseInvoiceID;
                        $pvId = '';
                    }
                    $diffvalue = $amount - $oldvalue;

                    $invoicePayment[$iddt] = array(
                        'paymentID' => $paymentCode,
                        'invoiceID' => (!empty($invoiceId)) ? $invoiceId : null,
                        'paymentVoucherId' => (!empty($pvId)) ? $pvId : null,
                        'outgoingInvoiceCashAmount' => $setallocation,
                        'outgoingInvoiceCreditAmount' => null,
                        'paymentMethodID' => $payment,
                    );


                    $allocation = $setallocation;

                    $newvalue = $oldvalue + $allocation;
                    if (round($diffvalue,2) > $allocation) {
                        if ($document == 'PV') {
                            $pVdata[$iddt] = array(
                                'paymentVoucherPaidAmount' => $newvalue,
//                            'status' => 3,
                            );
                        } else {
                            $invcdata[$iddt] = array(
                                'purchaseInvoicePayedAmount' => $newvalue,
//                            'status' => 3,
                            );
                        }
                       
                        $currentbalance = $currentbalance - $allocation;
                        $supplierTotalAmount += $setallocation;
                    } else {
                        if ($document == 'PV') {
                            $pVdata[$iddt] = array(
                                'paymentVoucherPaidAmount' => $amount,
                                'paymentVoucherStatus' => 4,
                            );
                        } else {
                            $invcdata[$iddt] = array(
                                'purchaseInvoicePayedAmount' => $amount,
                                'status' => 4,
                            );
                        }
                        
                        $a = floatval($setallocation);
                        $b = $diffvalue;
                        $epcilan = 0.00001;
 
                        if(abs($a-$b) < $epcilan){
                            $supplierTotalAmount += $diffvalue;
                            // $supplierAdvanceTotalAmount += ($setallocation - $diffvalue);
                        }else{
                            $supplierTotalAmount += $setallocation;
                        }
                        $currentbalance = $currentbalance - $amount + $oldvalue;
                        
                    }
                }
            }

            $supplierAdvanceTotalAmount = floatval($totalPaidAmount) - $supplierTotalAmount;


            if ($locationReferenceID) {
                $refNumber = $this->getReferenceNumber($locationReferenceID);
            }
            //check discount and credit values
            if ($supplierAdvanceTotalAmount > 0) {
                if (!empty($supplierCreditUpdate)) {
                    foreach ($supplierCreditUpdate as $supCreditKey => $sapCreditValue) {
                        $supplierCreditUpdate[$supCreditKey]['supplierCreditBalance'] = $supplierCreditUpdate[$supCreditKey]['supplierCreditBalance'] +  $supplierAdvanceTotalAmount;
                        
                    }
                }
            }
            
            $supplierupdate[0] = array(
                'supplierID' => $supplierID,
                'supplierOutstandingBalance' => $currentbalance,
            );


            $paymentdata['totalPaidAmount'] = $totalPaidAmount;
            $paymentdata['balanceAmount'] = $totalPaidAmount - $paymentdata['amount'];
            $result = $this->CommonTable('SupplierPaymentsTable')->savePayments($paymentdata, $invcdata, $supplierCreditUpdate, $supplierupdate, $invoicePayment, $debitNoteSupplierUpdate, $debitNoteUpdate, $pVdata, $currentCredit);
            
            if ($result['state'] == true) {
           
                if ($locationReferenceID) {
                    if ($autogenaratedCode) {
                        $this->updateReferenceNumber($locationReferenceID);
                    }
                }
//              Decrease the location Cash Amount
                $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($locationID)->current();
                $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] - $locationCashAmount;
                $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($locationID, $newLocationCashInHand);
                if ($this->useAccounting == 1) {
                    $accountPayments = array();
                    //check whether supplier Accounts Payable Accounts is set or not
                    $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                  
                    if($supplierID != '' && empty($supplierData->supplierPayableAccountID)){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PURINV_SUPPLIER_PAYABLE_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                        return $this->JSONRespond();
                    }else if(!empty($supplierAdvanceTotalAmount) && empty($supplierData->supplierAdvancePaymentAccountID)){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PURINV_SUPPLIER_ADV_PAYMENT_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                        return $this->JSONRespond();
                    }else if(!empty($discount) && empty($supplierData->supplierPurchaseDiscountAccountID)){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PURINV_SUPPLIER_PURCHASE_DISCOUNT_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                        return $this->JSONRespond();
                    }

                    if(!empty($supplierID)){
                        $payableAccountID = $supplierData->supplierPayableAccountID;
                    }else{
                        $payableAccountID = $pvAccountID;
                    }

                    foreach ($paymentdetails as $pmkey => $pmvalue) {

                        switch ($pmvalue->methodID) {
                            case '1':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->cashAccountID;
                                break;
                            case '2':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->chequeAccountID;
                                break;
                            case '3':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->creditAccountID;
                                break;
                            case '4':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->creditAccountID;
                                break;
                            case '5':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->bankTransferAccountID;
                                break; 
                            case '6':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->giftCardID;
                                break;
                            case '7':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->lcAccountID;
                                break;
                            case '8':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->ttAccountID;
                                break;
                            default:
                                # code...
                                break;
                        }

                        if($pmvalue->paymentMethodFinanceAccountID != ''){
                            //set gl accounts for the journal entry
                            $paymethodDiscount = 0;
                            // if(!($discount == 0 || $discount == "")){
                            //     $paymethodDiscount = $discount*($paymentMethodAmounts[$pmvalue->methodID]/$totalPaymentMethodsAmount); 
                            // }
                            if(isset($accountPayments[$pmvalue->paymentMethodFinanceAccountID]['total'])){
                                $accountPayments[$pmvalue->paymentMethodFinanceAccountID]['total'] += floatval($pmvalue->paidAmount) - $paymethodDiscount;
                            }else{
                                $accountPayments[$pmvalue->paymentMethodFinanceAccountID]['total'] = floatval($pmvalue->paidAmount) - $paymethodDiscount;
                                $accountPayments[$pmvalue->paymentMethodFinanceAccountID]['accountID'] = $pmvalue->paymentMethodFinanceAccountID;
                            }
                        }else{
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PUR_PAY_PLEASE_SELECT_ACCOUNT_ID_FOR_PAY_METHODS', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                            return $this->JSONRespond();       
                        }
                    }


                    //create data array for the journal entry save.
                    $i=0;
                    $journalEntryAccounts = array();
                    
                    //set flag for find about normal payments
                    $nomalPayments = false;
                    foreach ($accountPayments as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['total'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Payment '.$paymentCode;
                        $i++;
                        $nomalPayments = true;
                    }

                    
                    if(floatval($creditAmount) > 0 ){
                        
                        //set total credited credit value
                        $creditedCredit = $creditAmount;
                        
                        if(empty($supplierData->supplierAdvancePaymentAccountID)){
                            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                            $journalEntryAccounts[$i]['financeAccountsID'] = $payableAccountID;
                            $journalEntryAccounts[$i]['financeGroupsID'] = '';
                            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = floatval($supplierTotalAmount);
                            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $creditedCredit;
                            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Payment '.$paymentCode;
                            $i++;                            
                        } else {
                            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                            $journalEntryAccounts[$i]['financeAccountsID'] = $payableAccountID;
                            $journalEntryAccounts[$i]['financeGroupsID'] = '';
                            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = floatval($supplierTotalAmount);
                            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Payment '.$paymentCode;
                            $i++;
                            
                            if(!empty($supplierAdvanceTotalAmount)){

                                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                                $journalEntryAccounts[$i]['financeAccountsID'] = $supplierData->supplierAdvancePaymentAccountID;
                                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $supplierAdvanceTotalAmount;
                                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $creditedCredit;
                                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Payment '.$paymentCode;
                                $i++;
                                
                            } else {
                                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                                $journalEntryAccounts[$i]['financeAccountsID'] = $supplierData->supplierAdvancePaymentAccountID;
                                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $creditedCredit;
                                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Payment '.$paymentCode;
                                $i++;
                            }
                        }
                      
                    } else {
                        
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $payableAccountID;
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $supplierTotalAmount;
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Payment '.$paymentCode;
                        $i++;
                        
                        if(!empty($supplierAdvanceTotalAmount) && $supplierAdvanceTotalAmount > 0){
                            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                            $journalEntryAccounts[$i]['financeAccountsID'] = $supplierData->supplierAdvancePaymentAccountID;
                            $journalEntryAccounts[$i]['financeGroupsID'] = '';
                            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $supplierAdvanceTotalAmount;
                            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Payment '.$paymentCode;
                            $i++;
                        }
                    }
                
            //////////////////////////

                    

                    if(!empty($discount)){
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $supplierData->supplierPurchaseDiscountAccountID;
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $discount;
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Payment '.$paymentCode.'.';
                    }


                    //get journal entry reference number.
                    $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                    $jelocationReferenceID = $jeresult['locRefID'];
                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                    $journalEntryData = array(
                        'journalEntryAccounts' => $journalEntryAccounts,
                        'journalEntryDate' => $date,
                        'journalEntryCode' => $JournalEntryCode,
                        'journalEntryTypeID' => '',
                        'journalEntryIsReverse' => 0,
                        'journalEntryComment' => 'Journal Entry is posted when create Purchase Payment '.$paymentCode.".",
                        'documentTypeID' => 14,
                        'journalEntryDocumentID' => $result['id'],
                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                    );

                    $resultData = $this->saveJournalEntry($journalEntryData);
                    
                    if($resultData['status']){
                        $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($result['id'],14, 3);
                        if(!$jEDocStatusUpdate['status']){
                            $this->rollback();
                            return new JsonModel(array(
                                'state' => false,
                                'msg' =>  $this->getMessage($jEDocStatusUpdate['msg'])
                                )
                            );
                        }

                        if (!empty($dimensionData)) {
                            $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$paymentCode], $resultData['data']['journalEntryID'],$ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                            if(!$saveRes['status']){
                                $this->rollback();
                                return new JsonModel(array(
                                    'state' => false,
                                    'data' => $saveRes['data'],
                                    'msg' =>  $this->getMessage($saveRes['msg'])
                                    )
                                );
                            }   
                        } else {
                            $invDataForDimension = (array) $invData;
                            $jEDimensionData = [];
                            if (sizeof($invDataForDimension) == 1) {
                                foreach ($invDataForDimension as $key => $value) {
                                    if(explode("_",$key)[1] == "PI") {
                                        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(12,explode("_",$key)[0]);
                                    } else if (explode("_",$key)[1] == "PV") {
                                        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(20,explode("_",$key)[0]);
                                    }
                                    $dimensionData = [];
                                    foreach ($jEDimensionData as $value) {
                                        if (!is_null($value['journalEntryID'])) {
                                            $temp = [];
                                            $temp['dimensionTypeId'] = $value['dimensionType'];
                                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                                            $dimensionData[$paymentCode][] = $temp;
                                        }
                                    }
                                    if (!empty($dimensionData)) {
                                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$paymentCode], $resultData['data']['journalEntryID'],$ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                        if(!$saveRes['status']){
                                            $this->rollback();
                                            return new JsonModel(array(
                                                'state' => false,
                                                'data' => $saveRes['data'],
                                                'msg' =>  $this->getMessage($saveRes['msg'])
                                                )
                                            );
                                        }   
                                    }
                                }
                            }
                        } 

                        $supUpdateStatus = $this->getService('SupplierPaymentService')->currncyGainAndLostHandle( $result['id'], $rmCreditAmoFlag); 

                        if (!$supUpdateStatus['status']) {
                            $this->rollback();
                            return new JsonModel(array(
                                'state' => false,
                                'msg' =>  $this->getMessage($supUpdateStatus['msg']),
                                'supplierID' => $supplierID
                                )
                            );
                        }

                        $savePaymentMethodsNumbers =  $this->savePaymentMethodsNumbersForPayments($paymentdetails, $result['id']);

                        if ($savePaymentMethodsNumbers['status'] != true) {
                            $this->rollback();
                            return new JsonModel(array(
                                'state' => false,
                                'msg' =>  $savePaymentMethodsNumbers['msg'],
                                'supplierID' => $result['id'],
                                )
                            );
                        }

                        $this->setLogMessage("Supplier payment successfully created  ".$paymentCode.'.');
                        $this->commit();
                        return new JsonModel(array(
                            'state' => true,
                            'value' => $result['data'],
                            'id' => $result['id'],
                        ));
                    }else{
                        $this->rollback();
                        return new JsonModel(array(
                            'state' => false,
                            'msg' =>  $resultData['msg']/* 'Error occured when save the payment ,Please try again.' */,
                            'supplierID' => $supplierID,
                            'data' => $resultData['data']
                            )
                        );
                    }
                }else{

                    $savePaymentMethodsNumbers =  $this->savePaymentMethodsNumbersForPayments($paymentdetails, $result['id']);


                    if ($savePaymentMethodsNumbers['status'] != true) {
                        $this->rollback();
                        return new JsonModel(array(
                            'state' => false,
                            'msg' =>  $savePaymentMethodsNumbers['msg'],
                            'supplierID' => $result['id'],
                            )
                        );
                    }

                    $this->setLogMessage("Supplier payment successfully created  ".$paymentCode.'.');
                    $this->commit();
                    return new JsonModel(array(
                        'state' => true,
                        'value' => $result['data'],
                        'id' => $result['id'],
                    ));    
                }
            } else {
                $this->rollback();
                return new JsonModel(array(
                    'state' => false,
                    'msg' => $this->getMessage('ERR_CUSTPAY_API_SAVE')/* 'Error occured when save the payment ,Please try again.' */,
                    'supplierID' => $supplierID
                    )
                );
            }
        }
    }

    /** Update Suplier Advance Payment and Reference * */
    public function advancePaymentAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $result = $this->storeAdvancePayment($request->getPost());
            if ($result['state']) {
                $this->commit();
            } else {
                $this->rollback();
            }
            return new JsonModel(array(
                'state' => $result['state'],
                'msg' => $result['msg'],
                'data' => $result['data'],
                'id' => $result['id']
            ));
        } else {
//            $this->setLogMessage("Advance payment add request is not a post request");
            return new JsonModel(array(
                'state' => false,
                'msg' => $this->getMessage('ERR_OUTGPAYMENT_NOSAVEPAYMENT', array($payId))// $payId . " failed to save supplier advance payment"
            ));
        }
    }

    /*
    * This function is used to save advance payment
    */
    public function storeAdvancePayment($advPaymentDetails, $openingCreditFlag = false)
    {

        $payId = $advPaymentDetails['paymentID'];
        $locationID = $advPaymentDetails['locationID'];
        $ignoreBudgetLimit = $advPaymentDetails['ignoreBudgetLimit'];
        $refData = $this->getReferenceNoForLocation(13, $locationID);
        $locationReferenceID = $refData["locRefID"];

        // check if a supplier advance payment from the same supplier advance  payment Code exists if exist add next number to supplier advance payment code
        while ($payId) {
            if ($this->CommonTable('SupplierPaymentsTable')->checkSupplierPaymentByCode($payId)->current()) {
                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                    $payId = $this->getReferenceNumber($locationReferenceID);
                } else {
                    $this->rollback();
                    $this->setLogMessage("Error occured when create supplier advance payment ".$payId.'.');
                    return [
                        'state' => false,
                        'msg' => $this->getMessage('ERR_OUTGPAYMENT_CODE_EXIST')
                    ];
                }
            } else {
                break;
            }
        }
        
        $this->setLogMessage("Error occured when create supplier advance payment ".$payId.'.');

        $entityID = $this->createEntity();
        $date = $advPaymentDetails['date'];
        $supplierID = trim($advPaymentDetails['supplierID']);
        $advancePaymentMethodID = $advPaymentDetails['advancePaymentMethodID'];
        $amount = $advPaymentDetails['amount'];
        $memo = $advPaymentDetails['memo'];
        $paymentType = $advPaymentDetails['paymentType'];
        $paymentdetails = json_decode($advPaymentDetails['PaymentMethodDetails']);
        $dimensionData = $advPaymentDetails['dimensionData'];

        $sup = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
        $supplierCredit = $sup->supplierCreditBalance;
        $totalCredit = $supplierCredit + $amount;
        $sudata = array(
            'supplierID' => $supplierID,
            'supplierCreditBalance' => $totalCredit,
        );
        $data = array(
            'outgoingPaymentCode' => $payId,
            'outgoingPaymentDate' => $this->convertDateToStandardFormat($date),
            'paymentTermID' => 1,
            'paymentMethodID' => null,
            'outgoingPaymentAmount' => $amount,
            'supplierID' => $supplierID,
            'outgoingPaymentDiscount' => "0.00",
            'outgoingPaymentMemo' => $memo,
            'outgoingPaymentType' => $paymentType,
            'debitNoteID' => null,
            'locationID' => $locationID,
            'outgoingPaymentStatus' => 4,
            'entityID' => $entityID,
            'outgoingPaymentPaidAmount' => $amount,
            'outgoingPaymentBalanceAmount' => 0
        );
        $pay = $this->CommonTable('SupplierPaymentsTable')->saveAdvancePayment($data, $sudata, $openingCreditFlag);

        $locationCashAmount = 0;
        $hasCashPayments = false;
        $paymentMethodAmounts = [];
        foreach ($paymentdetails as $key2 => $value2) {
                
            if ($value2->methodID == '1') {
                $hasCashPayments = true;
                $locationCashAmount += $value2->paidAmount;
            }

            if(isset($paymentMethodAmounts[$value2->methodID])){
                $paymentMethodAmounts[$value2->methodID] += $value2->paidAmount;
            }else{
                $paymentMethodAmounts[$value2->methodID] = $value2->paidAmount;
            }
        }




        // var_dump($locationCashAmount).die();
        if ($pay['state'] == true) {
            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }
            //Decrease location Cash in hand location wise
            if ($hasCashPayments == 1) {
                //if cash payment then decrease the amount
                $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($locationID)->current();
                $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] - $locationCashAmount;
                $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($locationID, $newLocationCashInHand);
            }

            if ($this->useAccounting == 1) {
                $accountPayments = array();
                //check whether supplier Accounts Payable Accounts is set or not
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                if(empty($supplierData->supplierAdvancePaymentAccountID)){
                    $this->rollback();
                    return [
                        'state' => false,
                        'msg' => $this->getMessage('ERR_PURINV_SUPPLIER_ADV_PAYMENT_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode))
                    ];
                }

                if ($openingCreditFlag) {
                    if(!empty($paymentdetails->paymentMethodFinanceAccountID)){
                        //set gl accounts for the journal entry
                        $accountPayments[$paymentdetails->paymentMethodFinanceAccountID]['total'] = $amount;
                        $accountPayments[$paymentdetails->paymentMethodFinanceAccountID]['accountID'] = $paymentdetails->paymentMethodFinanceAccountID;
                    }else{
                        $this->rollback();
                        return [
                            'state' => false,
                            'msg' => $this->getMessage('ERR_PUR_PAY_PLEASE_SELECT_ACCOUNT_ID_FOR_PAY_METHODS', array($supplierData->supplierName.' - '.$supplierData->supplierCode))
                        ];     
                    }
                } else {


                    foreach ($paymentdetails as $pmkey => $pmvalue) {

                        switch ($pmvalue->methodID) {
                            case '1':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->cashAccountID;
                                break;
                            case '2':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->chequeAccountID;
                                break;
                            case '3':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->creditAccountID;
                                break;
                            case '4':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->creditAccountID;
                                break;
                            case '5':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->bankTransferAccountID;
                                break; 
                            case '6':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->giftCardID;
                                break;
                            case '7':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->lcAccountID;
                                break;
                            case '8':
                                $pmvalue->paymentMethodFinanceAccountID = $pmvalue->ttAccountID;
                                break;
                            default:
                                # code...
                                break;
                        }





                        if($pmvalue->paymentMethodFinanceAccountID != ''){
                            //set gl accounts for the journal entry
                            $paymethodDiscount = 0;
                            if(!($discount == 0 || $discount == "")){
                                $paymethodDiscount = $discount*($paymentMethodAmounts[$pmvalue->methodID]/$totalPaymentMethodsAmount); 
                            }
                            if(isset($accountPayments[$pmvalue->paymentMethodFinanceAccountID]['total'])){
                                $accountPayments[$pmvalue->paymentMethodFinanceAccountID]['total'] += floatval($pmvalue->paidAmount) - $paymethodDiscount;
                            }else{
                                $accountPayments[$pmvalue->paymentMethodFinanceAccountID]['total'] = floatval($pmvalue->paidAmount) - $paymethodDiscount;
                                $accountPayments[$pmvalue->paymentMethodFinanceAccountID]['accountID'] = $pmvalue->paymentMethodFinanceAccountID;
                            }
                        }else{
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PUR_PAY_PLEASE_SELECT_ACCOUNT_ID_FOR_PAY_METHODS', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                            return $this->JSONRespond();       
                        }
                    }
                }

                  //create data array for the journal entry save.
                $i=0;
                $journalEntryAccounts = array();

                foreach ($accountPayments as $key => $value) {
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['total'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Advance Payment '.$payId;
                    $i++;
                }

                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $supplierData->supplierAdvancePaymentAccountID;
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $amount;
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Advance Payment '.$payId;

                //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $this->convertDateToStandardFormat($date),
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create Purchase Advance Payment.',
                    'documentTypeID' => 14,
                    'journalEntryDocumentID' => $pay['data'],
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                );


                $resultData = $this->saveJournalEntry($journalEntryData);
                if($resultData['status']){
                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($pay['data'],14, 3);
                    if(!$jEDocStatusUpdate['status']){
                        $this->rollback();
                        return [
                            'state' => false,
                            'msg' =>  $jEDocStatusUpdate['msg']/* 'Error occured when save the payment ,Please try again.' */,
                        ];
                    }


                    if (!empty($dimensionData)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$payId], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);

                        if(!$saveRes['status']){
                            return array('status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']);
                        }   
                    }

                    if ($openingCreditFlag) {
                        $payData[] = $paymentdetails;
                    } else {
                        $payData = $paymentdetails;
                    }
                    $savePaymentMethodsNumbers =  $this->savePaymentMethodsNumbersForPayments($payData, $pay['data']);


                    if ($savePaymentMethodsNumbers['status'] != true) {
                        $this->rollback();
                        return new JsonModel(array(
                            'state' => false,
                            'msg' =>  $savePaymentMethodsNumbers['msg'],
                            )
                        );
                    }


                    $this->setLogMessage("Supplier advance payment successfully created  ".$payId.'.');
                    return [
                        'state' => true,
                        'value' => $payId,
                        'id' => $pay['data'],
                        'msg' => $this->getMessage('ERR_OUTGPAYMENT_SUPPADVPAY', array($payId))
                    ];
                }else{
                    $this->rollback();
                    return [
                        'state' => false,
                        'msg' =>  $resultData['msg']/* 'Error occured when save the payment ,Please try again.' */,
                        'data' => $resultData['data']
                    ];
                }

            }else{


                $savePaymentMethodsNumbers =  $this->savePaymentMethodsNumbersForPayments($paymentdetails, $pay['data']);

                if ($savePaymentMethodsNumbers['status'] != true) {
                    $this->rollback();
                    return new JsonModel(array(
                        'state' => false,
                        'msg' =>  $savePaymentMethodsNumbers['msg'],
                        )
                    );
                }

                $this->setLogMessage("Supplier advance payment successfully created  ".$payId.'.');
                return [
                    'state' => true,
                    'value' => $payId,
                    'id' => $pay['data'],
                    'msg' => $this->getMessage('ERR_OUTGPAYMENT_SUPPADVPAY', array($payId))
                ];
            }

        } else {
            $this->rollback();
            return [
                'state' => false,
                'msg' => $this->getMessage('ERR_OUTGPAYMENT_FAILSAVE', array($payId))//$payId . " failed to save payment"
            ];
        }
    }

    public function addPaymentMethodsNumbersAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paymentID = $request->getPost('paymentID');
            $paymentType = $request->getPost('paymentType');
            $paymentdetails = json_decode($request->getPost('PaymentMethodDetails'));
            //TODO::  when implementing custom currency this should be changed to invoice currency id          
            $customCurrencyId = $this->companyCurrencyId;//get custom currency as company currency
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            foreach ($paymentdetails as $key => $value) {
                $referenceNumber = (isset($value->paymentMethodReferenceNumber)) ? $value->paymentMethodReferenceNumber: '';
                $paymentMethodPostdatedStatus = (isset($value->paymentMethodPostdatedStatus)) ? $value->paymentMethodPostdatedStatus: '';
                $paymentMethodPostdatedDate = (isset($value->paymentMethodPostdatedDate)) ? $value->paymentMethodPostdatedDate: '';
                $paymentMethodChequeReference = (isset($value->paymentMethodChequeReference)) ? $value->paymentMethodChequeReference: '';
                $paymentMethodSupplierBank = (isset($value->paymentMethodSupplierBank)) ? $value->paymentMethodSupplierBank: '';
                $paymentMethodSupplierAccountNumber = (isset($value->paymentMethodSupplierAccountNumber)) ? $value->paymentMethodSupplierAccountNumber: '';
                $data = array(
                    'outGoingPaymentID' => $paymentID,
                    'outGoingPaymentMethodID' => $value->paymentMethodID,
                    'outGoingPaymentMethodFinanceAccountID' => $value->paymentMethodFinanceAccountID,
                    'outGoingPaymentMethodReferenceNumber' => $referenceNumber,
                    'outGoingPaymentMethodChequeReference' => $paymentMethodChequeReference,
                    'outGoingPaymentMethodBankTransferSupplierBankName' => $paymentMethodSupplierBank,
                    'outGoingPaymentMethodBankTransferSupplierAccountNumber' => $paymentMethodSupplierAccountNumber,
                    'postdatedChequeStatus' => $paymentMethodPostdatedStatus,
                    'postdatedChequeDate' => $paymentMethodPostdatedDate,
                );
                $supplierPaymentMethodsNumbers = new SupplierPaymentMethodNumbers();
                $supplierPaymentMethodsNumbers->exchangeArray($data);
                $valuess = $this->CommonTable('SupplierPaymentMethodNumbersTable')->savePaymentMethodsNumbers($supplierPaymentMethodsNumbers);
            }
            $this->setLogMessage('Supplier payment Methods save successfully.');
            return new JsonModel(array(true));
        } else {
            $this->setLogMessage('Error occured when save supplier payment.');
            return new JsonModel(array(false));
        }
    }

    /*
    * This function is used to save outgoing payment method numbers
    */
    public function savePaymentMethodsNumbersForPayments($paymentMethodsNumberDetails, $paymentID)
    {

        $paymentID = $paymentID;
        $paymentType = 'invoice';
        $paymentdetails = $paymentMethodsNumberDetails;
        //TODO::  when implementing custom currency this should be changed to invoice currency id          
        $customCurrencyId = $this->companyCurrencyId;//get custom currency as company currency
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        foreach ($paymentdetails as $key => $value) {
            switch ($value->methodID) {
                case '1':
                    $referenceNumber = '';
                    $paymentMethodPostdatedStatus = (isset($value->postdatedStatus)) ? $value->postdatedStatus: '';
                    $paymentMethodPostdatedDate = (isset($value->postdatedDate)) ? $value->postdatedDate: '';
                    $paymentMethodChequeReference = (isset($value->chequeReference)) ? $value->chequeReference: '';
                    $paymentMethodSupplierBank = (isset($value->paymentMethodSupplierBank)) ? $value->paymentMethodSupplierBank: '';
                    $paymentMethodSupplierAccountNumber = (isset($value->paymentMethodSupplierAccountNumber)) ? $value->paymentMethodSupplierAccountNumber: '';
                    $outGoingPaymentMethodChequeBankAccountID = (isset($value->chequeBankAccID)) ? $value->chequeBankAccID: '';
                    $outGoingPaymentMethodBankTransferBankId = (isset($value->bankTransferBankID)) ? $value->bankTransferBankID: '';
                    $outGoingPaymentMethodBankTransferAccountId = (isset($value->bankTransferBankAccountID)) ? $value->bankTransferBankAccountID: '';
                    break;
                case '2':
                    $referenceNumber = (isset($value->checkNumber)) ? $value->checkNumber: '';
                    $paymentMethodPostdatedStatus = (isset($value->postdatedStatus)) ? $value->postdatedStatus: '';
                    $paymentMethodPostdatedDate = (isset($value->postdatedDate)) ? $value->postdatedDate: '';
                    $paymentMethodChequeReference = (isset($value->chequeReference)) ? $value->chequeReference: '';
                    $paymentMethodSupplierBank = (isset($value->paymentMethodSupplierBank)) ? $value->paymentMethodSupplierBank: '';
                    $paymentMethodSupplierAccountNumber = (isset($value->paymentMethodSupplierAccountNumber)) ? $value->paymentMethodSupplierAccountNumber: '';
                    $outGoingPaymentMethodChequeBankAccountID = (isset($value->chequeBankAccID)) ? $value->chequeBankAccID: '';
                    $outGoingPaymentMethodBankTransferBankId = (isset($value->bankTransferBankID)) ? $value->bankTransferBankID: '';
                    $outGoingPaymentMethodBankTransferAccountId = (isset($value->bankTransferBankAccountID)) ? $value->bankTransferBankAccountID: '';
                    break;
                case '3':
                    $referenceNumber = (isset($value->reciptnumber)) ? $value->reciptnumber: '';
                    $paymentMethodPostdatedStatus = (isset($value->postdatedStatus)) ? $value->postdatedStatus: '';
                    $paymentMethodPostdatedDate = (isset($value->postdatedDate)) ? $value->postdatedDate: '';
                    $paymentMethodChequeReference = (isset($value->chequeReference)) ? $value->chequeReference: '';
                    $paymentMethodSupplierBank = (isset($value->bankID)) ? $value->bankID: '';
                    $paymentMethodSupplierAccountNumber = (isset($value->accountID)) ? $value->accountID: '';
                    $outGoingPaymentMethodChequeBankAccountID = (isset($value->chequeBankAccID)) ? $value->chequeBankAccID: '';
                    $outGoingPaymentMethodBankTransferBankId = (isset($value->bankTransferBankID)) ? $value->bankTransferBankID: '';
                    $outGoingPaymentMethodBankTransferAccountId = (isset($value->bankTransferBankAccountID)) ? $value->bankTransferBankAccountID: '';
                    break;
                case '4':
                    $referenceNumber = (isset($value->paymentMethodReferenceNumber)) ? $value->paymentMethodReferenceNumber: '';
                    $paymentMethodPostdatedStatus = (isset($value->postdatedStatus)) ? $value->postdatedStatus: '';
                    $paymentMethodPostdatedDate = (isset($value->postdatedDate)) ? $value->postdatedDate: '';
                    $paymentMethodChequeReference = (isset($value->chequeReference)) ? $value->chequeReference: '';
                    $paymentMethodSupplierBank = (isset($value->bankID)) ? $value->bankID: '';
                    $paymentMethodSupplierAccountNumber = (isset($value->accountID)) ? $value->accountID: '';
                    $outGoingPaymentMethodChequeBankAccountID = (isset($value->chequeBankAccID)) ? $value->chequeBankAccID: '';
                    $outGoingPaymentMethodBankTransferBankId = (isset($value->bankTransferBankID)) ? $value->bankTransferBankID: '';
                    $outGoingPaymentMethodBankTransferAccountId = (isset($value->bankTransferBankAccountID)) ? $value->bankTransferBankAccountID: '';
                    break;
                case '5':
                    $referenceNumber = (isset($value->bankTransferReferenceNumber)) ? $value->bankTransferReferenceNumber: '';
                    $paymentMethodPostdatedStatus = (isset($value->postdatedStatus)) ? $value->postdatedStatus: '';
                    $paymentMethodPostdatedDate = (isset($value->postdatedDate)) ? $value->postdatedDate: '';
                    $paymentMethodChequeReference = (isset($value->chequeReference)) ? $value->chequeReference: '';
                    $paymentMethodSupplierBank = (isset($value->supplierBank)) ? $value->supplierBank: '';
                    $paymentMethodSupplierAccountNumber = (isset($value->supplierAccountNumber)) ? $value->supplierAccountNumber: '';
                    $outGoingPaymentMethodChequeBankAccountID = (isset($value->chequeBankAccID)) ? $value->chequeBankAccID: '';
                    $outGoingPaymentMethodBankTransferBankId = (isset($value->bankTransferBankID)) ? $value->bankTransferBankID: '';
                    $outGoingPaymentMethodBankTransferAccountId = (isset($value->bankTransferBankAccountID)) ? $value->bankTransferBankAccountID: '';
                    break; 
                case '6':
                    $referenceNumber = (isset($value->giftCardReferenceNumber)) ? $value->giftCardReferenceNumber: '';
                    $paymentMethodPostdatedStatus = (isset($value->postdatedStatus)) ? $value->postdatedStatus: '';
                    $paymentMethodPostdatedDate = (isset($value->postdatedDate)) ? $value->postdatedDate: '';
                    $paymentMethodChequeReference = (isset($value->chequeReference)) ? $value->chequeReference: '';
                    $paymentMethodSupplierBank = (isset($value->paymentMethodSupplierBank)) ? $value->paymentMethodSupplierBank: '';
                    $paymentMethodSupplierAccountNumber = (isset($value->paymentMethodSupplierAccountNumber)) ? $value->paymentMethodSupplierAccountNumber: '';
                    $outGoingPaymentMethodChequeBankAccountID = (isset($value->chequeBankAccID)) ? $value->chequeBankAccID: '';
                    $outGoingPaymentMethodBankTransferBankId = (isset($value->bankTransferBankID)) ? $value->bankTransferBankID: '';
                    $outGoingPaymentMethodBankTransferAccountId = (isset($value->bankTransferBankAccountID)) ? $value->bankTransferBankAccountID: '';
                    break;
                case '7':
                    $referenceNumber = (isset($value->lcPaymentReference)) ? $value->lcPaymentReference: '';
                    $paymentMethodPostdatedStatus = (isset($value->postdatedStatus)) ? $value->postdatedStatus: '';
                    $paymentMethodPostdatedDate = (isset($value->postdatedDate)) ? $value->postdatedDate: '';
                    $paymentMethodChequeReference = (isset($value->chequeReference)) ? $value->chequeReference: '';
                    $paymentMethodSupplierBank = (isset($value->paymentMethodSupplierBank)) ? $value->paymentMethodSupplierBank: '';
                    $paymentMethodSupplierAccountNumber = (isset($value->paymentMethodSupplierAccountNumber)) ? $value->paymentMethodSupplierAccountNumber: '';
                    $outGoingPaymentMethodChequeBankAccountID = (isset($value->chequeBankAccID)) ? $value->chequeBankAccID: '';
                    $outGoingPaymentMethodBankTransferBankId = (isset($value->bankTransferBankID)) ? $value->bankTransferBankID: '';
                    $outGoingPaymentMethodBankTransferAccountId = (isset($value->bankTransferBankAccountID)) ? $value->bankTransferBankAccountID: '';
                    break;
                case '8':
                    $referenceNumber = (isset($value->ttPaymentReference)) ? $value->ttPaymentReference: '';
                    $paymentMethodPostdatedStatus = (isset($value->postdatedStatus)) ? $value->postdatedStatus: '';
                    $paymentMethodPostdatedDate = (isset($value->postdatedDate)) ? $value->postdatedDate: '';
                    $paymentMethodChequeReference = (isset($value->chequeReference)) ? $value->chequeReference: '';
                    $paymentMethodSupplierBank = (isset($value->paymentMethodSupplierBank)) ? $value->paymentMethodSupplierBank: '';
                    $paymentMethodSupplierAccountNumber = (isset($value->paymentMethodSupplierAccountNumber)) ? $value->paymentMethodSupplierAccountNumber: '';
                    $outGoingPaymentMethodChequeBankAccountID = (isset($value->chequeBankAccID)) ? $value->chequeBankAccID: '';
                    $outGoingPaymentMethodBankTransferBankId = (isset($value->bankTransferBankID)) ? $value->bankTransferBankID: '';
                    $outGoingPaymentMethodBankTransferAccountId = (isset($value->bankTransferBankAccountID)) ? $value->bankTransferBankAccountID: '';
                    break;
                default:
                    # code...
                    break;
            }


            $data = array(
                'outGoingPaymentID' => $paymentID,
                'outGoingPaymentMethodID' => $value->methodID,
                'outGoingPaymentMethodFinanceAccountID' => $value->paymentMethodFinanceAccountID,
                'outGoingPaymentMethodReferenceNumber' => $referenceNumber,
                'outGoingPaymentMethodChequeReference' => $paymentMethodChequeReference,
                'outGoingPaymentMethodBankTransferSupplierBankName' => $paymentMethodSupplierBank,
                'outGoingPaymentMethodBankTransferSupplierAccountNumber' => $paymentMethodSupplierAccountNumber,
                'postdatedChequeStatus' => $paymentMethodPostdatedStatus,
                'postdatedChequeDate' => $paymentMethodPostdatedDate,
                'outGoingPaymentMethodChequeBankAccountID' => $outGoingPaymentMethodChequeBankAccountID,
                'outGoingPaymentMethodBankTransferBankId' => $outGoingPaymentMethodBankTransferBankId,
                'outGoingPaymentMethodBankTransferAccountId' => $outGoingPaymentMethodBankTransferAccountId,
                'outGoingPaymentMethodPaidAmount' => floatval($value->paidAmount),
            );

            $supplierPaymentMethodsNumbers = new SupplierPaymentMethodNumbers();
            $supplierPaymentMethodsNumbers->exchangeArray($data);

            $valuess = $this->CommonTable('SupplierPaymentMethodNumbersTable')->savePaymentMethodsNumbers($supplierPaymentMethodsNumbers);

            if ($valuess != true) {
                // $this->setLogMessage('Supplier payment Methods save successfully.');
                // return ['status' => true , 'msg'=> 'Supplier payment Methods save successfully.'];
                return ['status' => false, 'msg' => "Error occur while saving payment method numbers"];
            } 
        }
        $this->setLogMessage('Supplier payment Methods save successfully.');
        return ['status' => true , 'msg'=> 'Supplier payment Methods save successfully.'];
    }



    /*
    * This function is used to save outgoing payment method numbers
    */
    public function savePaymentMethodsNumbers($paymentMethodsNumberDetails)
    {
        $paymentID = $paymentMethodsNumberDetails['paymentID'];
        $paymentType = $paymentMethodsNumberDetails['paymentType'];
        $paymentdetails = json_decode($paymentMethodsNumberDetails['PaymentMethodDetails']);
        $customCurrencyId = $this->companyCurrencyId;//get custom currency as company currency
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $data = array(
            'outGoingPaymentID' => $paymentID,
            'outGoingPaymentMethodID' => $paymentdetails->paymentMethodID,
            'outGoingPaymentMethodFinanceAccountID' => $paymentdetails->paymentMethodFinanceAccountID,
            'outGoingPaymentMethodReferenceNumber' => '',
            'outGoingPaymentMethodChequeReference' => '',
            'outGoingPaymentMethodBankTransferSupplierBankName' => '',
            'outGoingPaymentMethodBankTransferSupplierAccountNumber' => '',
            'postdatedChequeStatus' => '',
            'postdatedChequeDate' => '',
        );
        $supplierPaymentMethodsNumbers = new SupplierPaymentMethodNumbers();
        $supplierPaymentMethodsNumbers->exchangeArray($data);
        $result = $this->CommonTable('SupplierPaymentMethodNumbersTable')->savePaymentMethodsNumbers($supplierPaymentMethodsNumbers);

        if ($result == true) {
            $this->setLogMessage('Supplier payment Methods save successfully.');
            return ['status' => true , 'msg'=> 'Supplier payment Methods save successfully.'];
        } else {
            return ['status' => false, 'msg' => "Error occur while saving payment method numbers"];
        }
    }

    public function deletePaymentAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Paymentno = $request->getPost('payno');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $username = trim($request->getPost('username'));
            $password = trim($request->getPost('password'));
            $outgoingPaymentCancelMessage = $request->getPost('outgoingPaymentCancelMessage');
            $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
            $payment = $this->CommonTable('SupplierPaymentsTable')->getPaymentByID($Paymentno);

            if ($payment->outgoingPaymentStatus == 5) {
                $this->rollback();
                return new JsonModel(array('PAYMENTSTATUSERR',$this->getMessage('ERR_PAYMENT_ALREADY_CANCEL')));
            }
            $this->setLogMessage("Fail to delete supplier payment ".$payment->outgoingPaymentCode);
            if ($user) {
                if ($user->roleID == '1') {
                    $passwordData = explode(':', $user->userPassword);
                    $storedPassword = $passwordData[0];
                    $password = md5($password . $passwordData[1]);
                    if ($storedPassword == $password) {
                        $debitNoteID = $payment->debitNoteID;
                        $supplierID = $payment->supplierID;
                        $paymentType = $payment->outgoingPaymentType;
                        $paymentTotal = $payment->outgoingPaymentAmount;
                        $entityID = $payment->entityID;
                        $bankTransferAmount = 0.00;
                        $this->beginTransaction();
                        if ($paymentType == 'invoice') {
                            
                            if ($supplierID != null) {
                                $invoicepayment = $this->CommonTable('SupplierInvoicePaymentsTable')->getInvoiceByPaymentID($Paymentno);
                                
                                $invoiceTotalCreditPayment = 0;
                                $allRemovedCreditValue = 0;
                                while ($results = $invoicepayment->current()) {
                                    $result = (object) $results;
                                    
//                                  if cash payment increase the location amount
                                    if ($result->paymentMethodID == 1) {
                                        $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($payment->locationID)->current();
                                        $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] + $result->outgoingInvoiceCashAmount;
                                        $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($payment->locationID, $newLocationCashInHand);
                                    }

                                    if ($result->paymentMethodID == 5) {
                                        $bankTransferAmount+=$result->outgoingInvoiceCashAmount;
                                    }
                                    $invoiceID = ($result->purchaseInvoiceID)? : $result->paymentVoucherID;
                                    $invoiceAmount = $result->outgoingInvoiceCashAmount;
                                    
                                    $invoiceCreditAmount = $result->outgoingInvoiceCreditAmount;
                                    $totalInvoicePaymentamount = $invoiceAmount + $invoiceCreditAmount;
                                    $createdCredits = 0;
                                    if($result->invoiceID != 0 && floatval($totalInvoicePaymentamount) > floatval($result->purchaseInvoiceTotal)){
                                        $payedAmount = $invoiceAmount;
                                        $invoiceAmount = floatval($result->purchaseInvoiceTotal) - floatval($result->outgoingInvoiceCreditAmount);
                                        $totalInvoicePaymentamount = $invoiceAmount + $invoiceCreditAmount;
                                        $createdCredits =  floatval($payedAmount) - floatval($invoiceAmount);
                                    } else if($result->paymentVoucherId != 0 && floatval($totalInvoicePaymentamount) > floatval($result->paymentVoucherTotal)){
                                        $payedAmount = $invoiceAmount;
                                        $invoiceAmount = floatval($result->paymentVoucherTotal) - floatval($result->outgoingInvoiceCreditAmount);
                                        $totalInvoicePaymentamount = $invoiceAmount + $invoiceCreditAmount;
                                        $createdCredits =  floatval($payedAmount) - floatval($invoiceAmount);

                                    }

                                    $invoiceOldPayAmount = ($result->purchaseInvoicePayedAmount)? : $result->paymentVoucherPaidAmount;
                                    $invoiceNewPayAmount = $invoiceOldPayAmount - $totalInvoicePaymentamount;
                                    if ($result->purchaseInvoiceID) {
                                        $invoicedata = array(
                                            'purchaseInvoiceID' => $invoiceID,
                                            'purchaseInvoicePayedAmount' => $invoiceNewPayAmount,
                                            'status' => '3',
                                        );
                                        $inv = new PurchaseInvoice();
                                        $inv->exchangeArray($invoicedata);
                                        $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->updatePaymentOfInvoice($inv);
                                    } else {
                                        $invoicedata = array(
                                            'paymentVoucherID' => $invoiceID,
                                            'paymentVoucherPaidAmount' => $invoiceNewPayAmount,
                                            'paymentVoucherStatus' => '3',
                                        );
                                        $inv = new PaymentVoucher();
                                        $inv->exchangeArray($invoicedata);
                                        $this->CommonTable('Expenses\Model\PaymentVoucherTable')->updatePaymentOfPaymentVoucher($inv);
                                    }
                                    $allRemovedCreditValue += $createdCredits;
                                    $invoiceTotalCreditPayment+=$invoiceCreditAmount;
                                    $invoiceTotalCashPayment+=$invoiceAmount;
                                }

                                $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                                $supplierOldBalance = $supplier->supplierOutstandingBalance;
                                $supplierCurrentCredit = $supplier->supplierCreditBalance;
                                $supplierNewBalance = $supplierOldBalance + $payment->outgoingPaymentAmount;

                                $addedCredits = 0;
                                if ($payment->outgoingPaymentPaidAmount > $payment->outgoingPaymentAmount) {
                                    $addedCredits = floatval($payment->outgoingPaymentPaidAmount) - floatval($payment->outgoingPaymentAmount);
                                }
                                
                                $supplierNewCreditBalance = ($supplierCurrentCredit + floatval($payment->outgoingPaymentCreditAmount)) - $addedCredits;

                                $data = array(
                                    'supplierOutstandingBalance' => $supplierNewBalance,
                                    'supplierCreditBalance' => $supplierNewCreditBalance,
                                    'supplierID' => $supplierID,
                                );
                                $sup = new Supplier();
                                $sup->exchangeArray($data);
                                $result = $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplierCurrentAndCreditBalance($sup);
                                $this->updateDeleteInfoEntity($entityID);
                                $this->_updatePaymentStatus($Paymentno, $outgoingPaymentCancelMessage);
                            } else {
                                $invoicepayment = $this->CommonTable('SupplierInvoicePaymentsTable')->getInvoiceByPaymentID($Paymentno);
                                $invoiceTotalCreditPayment = 0;
                                while ($results = $invoicepayment->current()) {
                                    $result = (object) $results;
//                                  if cash payment increase the location amount
                                    if ($result->paymentMethodID == 1) {
                                        $currentLocationDetails = $this->CommonTable('Settings\Model\LocationTable')->getLocationCashInHandAmount($payment->locationID)->current();
                                        $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] + $result->outgoingInvoiceCashAmount;
                                        $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($payment->locationID, $newLocationCashInHand);
                                    }

                                    if ($result->paymentMethodID == 5) {
                                        $bankTransferAmount+=$result->outgoingInvoiceCashAmount;
                                    }
                                    $invoiceID = ($result->purchaseInvoiceID)? : $result->paymentVoucherID;
                                    $invoiceAmount = $result->outgoingInvoiceCashAmount;
                                    $invoiceCreditAmount = $result->outgoingInvoiceCreditAmount;
                                    $totalInvoicePaymentamount = $invoiceAmount + $invoiceCreditAmount;

                                    $createdCredits = 0;
                                    if($result->invoiceID != 0 && floatval($totalInvoicePaymentamount) > floatval($result->purchaseInvoiceTotal)){
                                        $payedAmount = $invoiceAmount;
                                        $invoiceAmount = floatval($result->purchaseInvoiceTotal) - floatval($result->outgoingInvoiceCreditAmount);
                                        $totalInvoicePaymentamount = $invoiceAmount + $invoiceCreditAmount;
                                        $createdCredits =  floatval($payedAmount) - floatval($invoiceAmount);
                                        //after all remove added credits
                                        $invoiceCreditAmount = $invoiceCreditAmount - $createdCredits;
                                    } else if($result->paymentVoucherId != 0 && floatval($totalInvoicePaymentamount) > floatval($result->paymentVoucherTotal)) {
                                        $payedAmount = $invoiceAmount;
                                        $invoiceAmount = floatval($result->paymentVoucherTotal) - floatval($result->outgoingInvoiceCreditAmount);
                                        $totalInvoicePaymentamount = $invoiceAmount + $invoiceCreditAmount;
                                        $createdCredits =  floatval($payedAmount) - floatval($invoiceAmount);
                                    }

                                    $invoiceOldPayAmount = ($result->purchaseInvoicePayedAmount)? : $result->paymentVoucherPaidAmount;
                                    $invoiceNewPayAmount = $invoiceOldPayAmount - $totalInvoicePaymentamount;
                                    if ($result->purchaseInvoiceID) {
                                        $invoicedata = array(
                                            'purchaseInvoiceID' => $invoiceID,
                                            'purchaseInvoicePayedAmount' => $invoiceNewPayAmount,
                                            'status' => '3',
                                        );
                                        $inv = new PurchaseInvoice();
                                        $inv->exchangeArray($invoicedata);
                                        $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->updatePaymentOfInvoice($inv);
                                    } else {
                                        $invoicedata = array(
                                            'paymentVoucherID' => $invoiceID,
                                            'paymentVoucherPaidAmount' => $invoiceNewPayAmount,
                                            'paymentVoucherStatus' => '3',
                                        );
                                        $inv = new PaymentVoucher();
                                        $inv->exchangeArray($invoicedata);
                                        $this->CommonTable('Expenses\Model\PaymentVoucherTable')->updatePaymentOfPaymentVoucher($inv);
                                    }
                                }
                                $this->updateDeleteInfoEntity($entityID);
                                $this->_updatePaymentStatus($Paymentno, $outgoingPaymentCancelMessage);
                            }

                            if ($bankTransferAmount != 0) {
                                $bankTransferDetails = (object) $this->CommonTable('SupplierPaymentMethodNumbersTable')->getOutGoingPaymentMethodBankTransferDetailsByPaymentID($Paymentno)->current();
                                $accountId = $bankTransferDetails->outGoingPaymentMethodBankTransferAccountId;
                                $accountData = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);

                                $newAccountBalance = $accountData['accountBalance'] + $bankTransferAmount;
                                $accountUpdate = array(
                                    'accountBalance' => $newAccountBalance,
                                );
                                $this->CommonTable('Expenses\Model\AccountTable')->updateAccount($accountUpdate, $accountId);
                            }

                            if ($this->useAccounting == 1) {
                                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('14', $Paymentno);
                                
                                $journalEntryID = $journalEntryData['journalEntryID'];
                                if(!empty($journalEntryID)){
                                    $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);                   
                                    $i=0;
                                    $journalEntryAccounts = array();
                                    foreach ($jEAccounts as $key => $value) {
                                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
                                        $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
                                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
                                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
                                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Supplier Payment Delete '.$payment->outgoingPaymentCode.'.';
                                        $i++;
                                    }

                    //get journal entry reference number.
                                    $jeresult = $this->getReferenceNoForLocation('30', $payment->locationID);
                                    $jelocationReferenceID = $jeresult['locRefID'];
                                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                                    $journalEntryData = array(
                                        'journalEntryAccounts' => $journalEntryAccounts,
                                        'journalEntryDate' => $this->convertDateToStandardFormat($journalEntryData['journalEntryDate']),
                                        'journalEntryCode' => $JournalEntryCode,
                                        'journalEntryTypeID' => '',
                                        'journalEntryIsReverse' => 0,
                                        'journalEntryComment' => 'Journal Entry is posted when delete supplier payment '.$payment->outgoingPaymentCode.'.',
                                        'documentTypeID' => 14,
                                        'journalEntryDocumentID' => $Paymentno,
                                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                                        );

                                    $resultData = $this->saveJournalEntry($journalEntryData);

                                    if(!$resultData['status']){
                                        $this->rollback();
                                        return new JsonModel(array(false, $resultData['data'], $resultData['msg']));
                                        // $this->status = false;
                                        // $this->msg = $resultData['msg'];

                                        // return $this->JSONRespond();
                                    }

                                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($Paymentno,14, 5);
                                    if(!$jEDocStatusUpdate['status']){
                                        $this->rollback();
                                        return new JsonModel(array(false));
                                    }

                                    $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(14,$Paymentno);
                                    $dimensionData = [];
                                    foreach ($jEDimensionData as $value) {
                                        if (!is_null($value['journalEntryID'])) {
                                            $temp = [];
                                            $temp['dimensionTypeId'] = $value['dimensionType'];
                                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                                            $dimensionData[$payment->outgoingPaymentCode][] = $temp;
                                        }
                                    }
                                    if (!empty($dimensionData)) {
                                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$payment->outgoingPaymentCode], $resultData['data']['journalEntryID'],$ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                        if(!$saveRes['status']){
                                            return new JsonModel(array(false));
                                        }   
                                    }
                                }
                            }

                            // update currency gain and loss details
                            $currencyGainAnlLossStatus = $this->getService('SupplierPaymentService')->handleCurrencyGainAndLossForDeletePayment($Paymentno);
                            if (!$currencyGainAnlLossStatus['status']) {
                                $this->rollback();
                                return new JsonModel(array(false));
                                
                            }

                            $this->setLogMessage("Supplier payment ".$payment->outgoingPaymentCode." deleted successfully.");
                            $this->commit();
                            return new JsonModel(array(true));
                        } else if ($paymentType == 'advance') {
                            $bankTransferAmount = $paymentTotal;
                            $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                            $supplierOldBalance = $supplier->supplierCreditBalance;
                            $supplierNewBalance = $supplierOldBalance - $paymentTotal;
                            if ($supplierNewBalance < 0) {
                                $this->rollback();
                                return new JsonModel(array('chengeCredit'));
                            } else {
                                $data = array(
                                    'supplierCreditBalance' => $supplierNewBalance,
                                    'supplierID' => $supplierID,
                                );
                                $sup = new Supplier();
                                $sup->exchangeArray($data);
                                $result = $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplierCredit($sup);
                                $this->updateDeleteInfoEntity($entityID);
                                $this->_updatePaymentStatus($Paymentno, $outgoingPaymentCancelMessage);

                                if ($bankTransferAmount != 0) {
                                    $bankTransferDetails = (object) $this->CommonTable('SupplierPaymentMethodNumbersTable')->getOutGoingPaymentMethodBankTransferDetailsByPaymentID($Paymentno)->current();
                                    $accountId = $bankTransferDetails->outGoingPaymentMethodBankTransferAccountId;
                                    $accountData = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);

                                    $newAccountBalance = $accountData['accountBalance'] + $bankTransferAmount;
                                    $accountUpdate = array(
                                        'accountBalance' => $newAccountBalance,
                                    );
                                    $this->CommonTable('Expenses\Model\AccountTable')->updateAccount($accountUpdate, $accountId);
                                }
//                              if cash payment, should increase the location cash amount
                                if ($payment->paymentMethodID == 1) {
                                    $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($payment->locationID)->current();
                                    $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] + $paymentTotal;
                                    $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($payment->locationID, $newLocationCashInHand);
                                }
                                $this->setLogMessage("Supplier payment ".$payment->outgoingPaymentCode." deleted successfully.");

                                if ($this->useAccounting == 1) {
                                    $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('14', $Paymentno);

                                    $journalEntryID = $journalEntryData['journalEntryID'];
                                    if(!empty($journalEntryID)){
                                        $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);                   
                                        $i=0;
                                        $journalEntryAccounts = array();
                                        foreach ($jEAccounts as $key => $value) {
                                            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                                            $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
                                            $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
                                            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
                                            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
                                            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Supplier Payment Delete '.$payment->outgoingPaymentCode.'.';
                                            $i++;
                                        }

                    //get journal entry reference number.
                                        $jeresult = $this->getReferenceNoForLocation('30', $payment->locationID);
                                        $jelocationReferenceID = $jeresult['locRefID'];
                                        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                                        $journalEntryData = array(
                                            'journalEntryAccounts' => $journalEntryAccounts,
                                            'journalEntryDate' => $this->convertDateToStandardFormat($journalEntryData['journalEntryDate']),
                                            'journalEntryCode' => $JournalEntryCode,
                                            'journalEntryTypeID' => '',
                                            'journalEntryIsReverse' => 0,
                                            'journalEntryComment' => 'Journal Entry is posted when delete supplier payment '.$payment->outgoingPaymentCode.'.',
                                            'documentTypeID' => 14,
                                            'journalEntryDocumentID' => $Paymentno,
                                            'ignoreBudgetLimit' => $ignoreBudgetLimit,
                                            );

                                        $resultData = $this->saveJournalEntry($journalEntryData);

                                        if(!$resultData['status']){
                                            $this->rollback();
                                            return new JsonModel(array(false, $resultData['data'], $resultData['msg']));
                                        // $this->status = false;
                                        // $this->msg = $resultData['msg'];

                                        // return $this->JSONRespond();
                                        }
                                        $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($Paymentno,14, 5);
                                        if(!$jEDocStatusUpdate['status']){
                                            $this->rollback();
                                            return new JsonModel(array(false));
                                        }
                                    }
                                }
                                $this->commit();
                                return new JsonModel(array(true));
                            }
                        }
                        $this->rollback();
                        return new JsonModel(array(false));
                    } else {
                        $this->rollback();
                        return new JsonModel(array('pass'));
                    }
                } else {
                    $this->rollback();
                    return new JsonModel(array('admin'));
                }
            } else {
                $this->rollback();
                return new JsonModel(array('user'));
            }
            $this->rollback();
            return new JsonModel(array(false));
        }
        $this->rollback();
        $this->setLogMessage("Supplier payment delete request is not a post request");
        return new JsonModel(array(false));
    }

    private function _updatePaymentStatus($paymentID, $outgoingPaymentCancelMessage)
    {
        $cancelStatusID = $this->getStatusID('cancelled');
        $data = array(
            'outgoingPaymentStatus'
            => $cancelStatusID,
            'outgoingPaymentCancelMessage' => $outgoingPaymentCancelMessage
        );
        $this->CommonTable('SupplierPaymentsTable')->update($data, $paymentID);
    }

    public function RetrivePaymentsAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
            $Payment = trim($request->getPost('PaymentID'));
            $PaymentCode = trim($request->getPost('PaymentCode'));
            $getpayment = $this->CommonTable('SupplierPaymentsTable')->getPaymentsByPaymentID($Payment);

            $paymenteview = new ViewModel(array(
                'payments' => $getpayment,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat
                    )
            );
            $paymenteview->setTerminal(true);
            $paymenteview->setTemplate('inventory/out-going-payment-api/payment-list');
            $this->setLogMessage('Retrive supplier payments '.$PaymentCode.' for list view');
            return $paymenteview;
        }
        $this->setLogMessage("Retrive supplier payments ".$PaymentCode." for list view request is not post request");
        return false;
    }

    public function retriveChequesAction()
    {
        if ($this->request->isPost()) {
            $paymentId = $this->getRequest()->getPost('paymentId');
            $cheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')
                    ->getSupplierChequesByPaymentId($paymentId);

            $chequeView = new ViewModel(array(
                'cheques' => $cheques,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $chequeView->setTerminal(true);
            $chequeView->setTemplate('expenses/cheque-print/cheque-list');
            return $chequeView;
        }
    }

    public function retriveChequesBySupplierAction()
    {
        if ($this->request->isPost()) {
            $supplierID = $this->getRequest()->getPost('supplierID');
            $cheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')
                    ->getSupplierChequesByPaymentId(null, $supplierID);

            $chequeView = new ViewModel(array(
                'cheques' => $cheques,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $chequeView->setTerminal(true);
            $chequeView->setTemplate('expenses/cheque-print/cheque-list');
            return $chequeView;
        }
    }

    public function getSupplierChequesByFilterAction()
    {
        if ($this->request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $cheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')
                    ->getSupplierChequesByPaymentId(null, null, $this->convertDateToStandardFormat($postData['fromdate']), $this->convertDateToStandardFormat($postData['todate']));

            $chequeView = new ViewModel(array(
                'cheques' => $cheques,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $chequeView->setTerminal(true);
            $chequeView->setTemplate('expenses/cheque-print/cheque-list');
            return $chequeView;
        }
    }

    public function printChequeAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $paymentID = $request->getPost('paymentID');
        $comment = $request->getPost('comment');
        $cheque = $this->CommonTable('SupplierPaymentMethodNumbersTable')
                    ->getSupplierChequesByPaymentId($paymentID)->current();

        $chequeValue = floatval($cheque['outGoingPaymentMethodPaidAmount']);
        $chequeValueInWords = $this->convertNumberToWord($chequeValue);

        $entityID = $this->createEntity();
        $data = array(
            'paymentID' => $paymentID,
            'comment' => $comment,
            'entityID' => $entityID,
        );
        $printData = new ChequePrint();
        $printData->exchangeArray($data);
        $res = $this->CommonTable('Expenses\Model\ChequePrintTable')->saveChequePrintDetails($printData);

        if ($res) {
            $chequeDate = explode('/', date("d/m/Y", strtotime($cheque['outgoingPaymentDate'])));

            $chequeDayFisrtDigit = $chequeDate[0][0];
            $chequeDaySecondDigit = $chequeDate[0][1];

            $chequeMonthFisrtDigit = $chequeDate[1][0];
            $chequeMonthSecondDigit = $chequeDate[1][1];

            $chequeYearFisrtDigit = $chequeDate[2][2];
            $chequeYearSecondDigit = $chequeDate[2][3];


            $supplierName = $cheque['supplierName'];
            $paymentAmount = number_format($cheque['outGoingPaymentMethodPaidAmount'],2);
            

            $dataForPrint = [
                'chequeDayFisrtDigit' => $chequeDayFisrtDigit,
                'chequeDaySecondDigit' => $chequeDaySecondDigit,
                'chequeMonthFisrtDigit' => $chequeMonthFisrtDigit,
                'chequeMonthSecondDigit' => $chequeMonthSecondDigit,
                'chequeYearFisrtDigit' => $chequeYearFisrtDigit,
                'chequeYearSecondDigit' => $chequeYearSecondDigit,
                'supplier' => $supplierName,
                'paymentAmount' => "**".$this->companyCurrencySymbol." ".$paymentAmount."**",
                'chequeValueInWords' => "**".ucFirst(trim($chequeValueInWords))."**",
            ];

            $this->commit();
            $this->status = true;
            $this->data = $dataForPrint;
            return $this->JSONRespond();
        } else {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_PRINT_CHEQUE');
            return $this->JSONRespond();
        }

    }

    function convertNumberToWord($num = false)
    {
        $decones = array( 
                    '01' => "One", 
                    '02' => "Two", 
                    '03' => "Three", 
                    '04' => "Four", 
                    '05' => "Five", 
                    '06' => "Six", 
                    '07' => "Seven", 
                    '08' => "Eight", 
                    '09' => "Nine", 
                    10 => "Ten", 
                    11 => "Eleven", 
                    12 => "Twelve", 
                    13 => "Thirteen", 
                    14 => "Fourteen", 
                    15 => "Fifteen", 
                    16 => "Sixteen", 
                    17 => "Seventeen", 
                    18 => "Eighteen", 
                    19 => "Nineteen" 
                    );
        $ones = array( 
                    0 => " ",
                    1 => "One",     
                    2 => "Two", 
                    3 => "Three", 
                    4 => "Four", 
                    5 => "Five", 
                    6 => "Six", 
                    7 => "Seven", 
                    8 => "Eight", 
                    9 => "Nine", 
                    10 => "Ten", 
                    11 => "Eleven", 
                    12 => "Twelve", 
                    13 => "Thirteen", 
                    14 => "Fourteen", 
                    15 => "Fifteen", 
                    16 => "Sixteen", 
                    17 => "Seventeen", 
                    18 => "Eighteen", 
                    19 => "Nineteen" 
                    ); 
        $tens = array( 
                    0 => "",
                    2 => "Twenty", 
                    3 => "Thirty", 
                    4 => "Forty", 
                    5 => "Fifty", 
                    6 => "Sixty", 
                    7 => "Seventy", 
                    8 => "Eighty", 
                    9 => "Ninety" 
                    ); 
        $hundreds = array( 
                    "Hundred", 
                    "Thousand", 
                    "Million", 
                    "Billion", 
                    "Trillion", 
                    "Quadrillion" 
                    ); //limit t quadrillion 
        $num = number_format($num,2,".",","); 
        $num_arr = explode(".",$num); 
        $wholenum = $num_arr[0]; 
        $decnum = $num_arr[1]; 
        $whole_arr = array_reverse(explode(",",$wholenum)); 
        krsort($whole_arr); 
        $rettxt = ""; 
        foreach($whole_arr as $key => $i){ 
            if($i < 20){ 
                $rettxt .= $ones[$i]; 
            }
            elseif($i < 100){ 
                $rettxt .= $tens[substr($i,0,1)]; 
                $rettxt .= " ".$ones[substr($i,1,1)]; 
            }
            else{ 
                $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
                $rettxt .= " ".$tens[substr($i,1,1)]; 
                $rettxt .= " ".$ones[substr($i,2,1)]; 
            } 
            if($key > 0){ 
                $rettxt .= " ".$hundreds[$key]." "; 
            } 

        } 
        $rettxt = $rettxt." rupees";

        if($decnum > 0){ 
            $rettxt .= " and "; 
            if($decnum < 20){ 
                $rettxt .= $decones[$decnum]; 
            }
            elseif($decnum < 100){ 
                $rettxt .= $tens[substr($decnum,0,1)]; 
                $rettxt .= " ".$ones[substr($decnum,1,1)]; 
            }
            $rettxt = $rettxt." cents"; 
        } 
        return $rettxt;
    }

    public function getPaymentsByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($invrequest->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $fromdate = $this->convertDateToStandardFormat($invrequest->getPost('fromdate'));
            $todate = $this->convertDateToStandardFormat($invrequest->getPost('todate'));
            $supplierID = $invrequest->getPost('supplierID');
            $whichPayment = $invrequest->getPost('whichPayment');
            $getpayment = $this->CommonTable('SupplierPaymentsTable')->getPaymentsByDate($fromdate, $todate, $supplierID, $locationID,$whichPayment);

            $paymenteview = new ViewModel(array(
                'payments' => $getpayment,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat
                    )
            );
            $paymenteview->setTerminal(true);
            $paymenteview->setTemplate('inventory/out-going-payment-api/payment-list');
            $this->setLogMessage('Retrive supplier payment list for list view by date filter.');
            return $paymenteview;
        }
    }

    public function RetriveSupplierPaymentsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $supplierID = trim($request->getPost('supplierID'));
            $supplierCode = trim($request->getPost('supplierCode'));
            $whichPayment = $request->getPost('WhichPayment');
            $getpayment = $this->CommonTable('SupplierPaymentsTable')->getPaymentsBySupplierID($supplierID, $locationID, $whichPayment);

            $dateFormat = $this->getUserDateFormat();
            $paymenteview = new ViewModel(array(
                'payments' => $getpayment,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat,
                'paginator'=> false
//                'customer' => $value->customerName . '-' . $customerPayments
                    )
            );
            $paymenteview->setTerminal(true);
            $paymenteview->setTemplate('inventory/out-going-payment-api/payment-list');
            $this->setLogMessage('Retrive supplier payments list for list view by supplier '.$supplierCode);
            return $paymenteview;
        }
        $this->setLogMessage("Retrive supplier payments list for list view by supplier ".$supplierCode." request is not post request.");
        return false;
    }

    public function sendSupplierPaymentEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Supplier Payment';

        if ($request->isPost()) {
            $documentID = $request->getPost('documentID');
            $email = $request->getPost('to_email');
            $spayData = $this->CommonTable('SupplierPaymentsTable')->getPaymentsByPaymentID($documentID)->current();
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_PAY_EMAIL');
            $this->setLogMessage($spayData['outgoingPaymentCode']." supplier payment document email to ".$email.'.');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_GRN_EMAIL', array($documentType));
        $this->setLogMessage("Error occured when email the supplier payment document.");
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * search PVs for supplier paymnets create dropdown list
     * @return type
     */
    public function searchPVForSupplierPaymentsDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $PVs = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getLocationRelatedHalfPurchaseInvoices($locationID, $searchKey);

            $PVList = array();
            foreach ($PVs as $PV) {
                $temp['value'] = $PV['purchaseInvoiceID'];

                $temp['text'] = $PV['purchaseInvoiceCode'];
                $PVList[] = $temp;
            }

            $this->data = array('list' => $PVList);
            $this->setLogMessage("Retrive purchase invoice list for dropdown.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * search Payment vochers for supplier paymnets create dropdown list
     * @return type
     */
    public function searchPaymentVoucherForSupplierPaymentsDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $PVs = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getLocationRelatedHalfPaymentVouchers($locationID, $searchKey);
            $PVList = array();
            foreach ($PVs as $PV) {
                $temp['value'] = $PV['paymentVoucherID'];

                $temp['text'] = $PV['paymentVoucherCode'];
                $PVList[] = $temp;
            }

            $this->data = array('list' => $PVList);
            $this->setLogMessage("Retrive payment voucher list for dropdown.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search Supplier payments Dropdown
     * @return type
     */
    public function searchSupplierPaymentsDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $whichPayment = $searchrequest->getPost('addFlag');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $payments = $this->CommonTable('SupplierPaymentsTable')->getPaymentsByLocatioID($locationID, $searchKey,$whichPayment);

            $paymentList = array();
            foreach ($payments as $payment) {
                $temp['value'] = $payment['outgoingPaymentID'];
                $temp['text'] = $payment['outgoingPaymentCode'];
                $paymentList[] = $temp;
            }

            $this->data = array('list' => $paymentList);
            $this->setLogMessage("Retrive supplier payments list for dropdown.");
            return $this->JSONRespond();
        }
    }

    //edit payment Methods details action
    public function editPaymentMethodsAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $paymentMethods = $request->getPost('paymentMethods');
            $paymentId = $request->getPost('paymentId');
            $paymentComment = $request->getPost('paymentComment');

            $this->beginTransaction();

            $paymentData = $this->CommonTable('SupplierPaymentsTable')->getPaymentByID($paymentId);

            $dataPayment = array(
                'outgoingPaymentMemo' => $paymentComment,
            );

            $this->CommonTable('SupplierPaymentsTable')->update($dataPayment, $paymentId);
            
            $previousPaymentData = array(
                'outgoingPaymentMemo' => $paymentData->outgoingPaymentMemo,
            );
            $previousData = json_encode($previousPaymentData);
            $newData = json_encode($dataPayment);
            $this->setLogDetailsArray($previousData,$newData);

            foreach ($paymentMethods as $value) {


                $value = (object) $value;
                $oldData = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPaymentMethodDetailsById($value->methodTypeID)->current();
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('14', $paymentId);

                $journalEntryAccountData = $this->CommonTable('JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryIDAndAccountID( $journalEntryData['journalEntryID'], $oldData['outGoingPaymentMethodFinanceAccountID'])->current();


                $id = $value->methodTypeID;
                if ($value->methodID == 2) {

                    $accData = $this->CommonTable('AccountTable')->getAccountByAccountId($value->checkAccountId);
                    if (empty($oldData['outGoingPaymentMethodChequeBankAccountID']) && !empty($value->checkAccountId)) {
                        $oldFAData = $this->CommonTable('FinanceAccountsTable')->getAccountsDataByAccountsID($oldData['outGoingPaymentMethodFinanceAccountID']);
                        $newFAData = $this->CommonTable('FinanceAccountsTable')->getAccountsDataByAccountsID($accData['financeAccountID']);

                        if (floatval($journalEntryAccountData['journalEntryAccountsCreditAmount']) == floatval(str_replace(',', '', $value->paidAmount))) {
                            //update jea finance accounts id
                            $jeAccData = [
                                'financeAccountsID' => $accData['financeAccountID']
                            ];

                            $updateJEAData = $this->CommonTable('JournalEntryAccountsTable')->updateJournalEntryAccounts($jeAccData, $journalEntryAccountData['journalEntryAccountsID']);


                            //update old gl account credit runing balance
                            $oldGLACCUpdateData = [
                                'financeAccountsCreditAmount' => floatval($oldFAData['financeAccountsCreditAmount']) - floatval($journalEntryAccountData['journalEntryAccountsCreditAmount'])

                            ];

                            $updateOldFAData = $this->CommonTable('FinanceAccountsTable')->updateFinanceAccounts($oldGLACCUpdateData, $oldFAData['financeAccountsID']);

                            //update new gl account credit runing balance
                            $newGLACCUpdateData = [
                                'financeAccountsCreditAmount' => floatval($newFAData['financeAccountsCreditAmount']) + floatval($journalEntryAccountData['journalEntryAccountsCreditAmount'])

                            ];
                            $updateNewFAData = $this->CommonTable('FinanceAccountsTable')->updateFinanceAccounts($newGLACCUpdateData, $newFAData['financeAccountsID']);

                        } elseif (floatval($journalEntryAccountData['journalEntryAccountsCreditAmount']) > floatval(str_replace(',', '', $value->paidAmount))) {
                            
                            $jeAccData = [
                                'journalEntryAccountsCreditAmount' => floatval($journalEntryAccountData['journalEntryAccountsCreditAmount']) - floatval(str_replace(',', '', $value->paidAmount))
                            ];

                            $updateJEAData = $this->CommonTable('JournalEntryAccountsTable')->updateJournalEntryAccounts($jeAccData, $journalEntryAccountData['journalEntryAccountsID']);


                            //update old gl account credit runing balance
                            $oldGLACCUpdateData = [
                                'financeAccountsCreditAmount' => floatval($oldFAData['financeAccountsCreditAmount']) - floatval(str_replace(',', '', $value->paidAmount))

                            ];

                            $updateOldFAData = $this->CommonTable('FinanceAccountsTable')->updateFinanceAccounts($oldGLACCUpdateData, $oldFAData['financeAccountsID']);


                            $newJEAData = [
                                'journalEntryID' => $journalEntryAccountData['journalEntryID'],
                                'financeAccountsID' => $newFAData['financeAccountsID'],
                                'financeGroupsID' => 0,
                                'journalEntryAccountsDebitAmount' => 0,
                                'journalEntryAccountsCreditAmount' => floatval(str_replace(',', '', $value->paidAmount)),
                                'journalEntryAccountsMemo' => $journalEntryAccountData['journalEntryAccountsMemo'],
                                'yearEndCloseFlag' => $journalEntryAccountData['yearEndCloseFlag'],
                            ];

                            $journalEntryAccounts= new JournalEntryAccounts();
                            $journalEntryAccounts->exchangeArray($newJEAData);

                            $saveNewJEAData = $this->CommonTable('JournalEntryAccountsTable')->saveJournalEntryAccounts($journalEntryAccounts);

                            //update new gl account credit runing balance
                            $newGLACCUpdateData = [
                                'financeAccountsCreditAmount' => floatval($newFAData['financeAccountsCreditAmount']) + floatval(str_replace(',', '', $value->paidAmount))

                            ];
                            $updateNewFAData = $this->CommonTable('FinanceAccountsTable')->updateFinanceAccounts($newGLACCUpdateData, $newFAData['financeAccountsID']);

                        }
                    }

                    $data = array(
                        'outGoingPaymentMethodReferenceNumber' => (!empty($value->checkNumber)) ? $value->checkNumber : null,
                        'outGoingPaymentMethodChequeBankAccountID' => (!empty($value->checkAccountId)) ? $value->checkAccountId : null,
                        'outGoingPaymentMethodReferenceNumber' => (!empty($value->checkNumber)) ? $value->checkNumber : null,
                        'outGoingPaymentMethodFinanceAccountID' => $accData['financeAccountID']
                    );
                } else if ($value->methodID == 3) {
                    $data = array(
                        'outGoingPaymentMethodReferenceNumber' => (!empty($value->reciptnumber)) ? $value->reciptnumber : null,
                        'outGoingPaymentMethodCardID' => (!empty($value->cardnumber)) ? $value->cardnumber : null
                    );
                } else if ($value->methodID == 5) {

                    $accData = $this->CommonTable('AccountTable')->getAccountByAccountId($value->accountID);
                    if (empty($oldData['outGoingPaymentMethodBankTransferAccountId']) && !empty($value->accountID)) {
                        $oldFAData = $this->CommonTable('FinanceAccountsTable')->getAccountsDataByAccountsID($oldData['outGoingPaymentMethodFinanceAccountID']);
                        $newFAData = $this->CommonTable('FinanceAccountsTable')->getAccountsDataByAccountsID($accData['financeAccountID']);

                        if (floatval($journalEntryAccountData['journalEntryAccountsCreditAmount']) == floatval(str_replace(',', '', $value->paidAmount))) {
                            //update jea finance accounts id
                            $jeAccData = [
                                'financeAccountsID' => $accData['financeAccountID']
                            ];

                            $updateJEAData = $this->CommonTable('JournalEntryAccountsTable')->updateJournalEntryAccounts($jeAccData, $journalEntryAccountData['journalEntryAccountsID']);


                            //update old gl account credit runing balance
                            $oldGLACCUpdateData = [
                                'financeAccountsCreditAmount' => floatval($oldFAData['financeAccountsCreditAmount']) - floatval($journalEntryAccountData['journalEntryAccountsCreditAmount'])

                            ];

                            $updateOldFAData = $this->CommonTable('FinanceAccountsTable')->updateFinanceAccounts($oldGLACCUpdateData, $oldFAData['financeAccountsID']);

                            //update new gl account credit runing balance
                            $newGLACCUpdateData = [
                                'financeAccountsCreditAmount' => floatval($newFAData['financeAccountsCreditAmount']) + floatval($journalEntryAccountData['journalEntryAccountsCreditAmount'])

                            ];
                            $updateNewFAData = $this->CommonTable('FinanceAccountsTable')->updateFinanceAccounts($newGLACCUpdateData, $newFAData['financeAccountsID']);

                        } elseif (floatval($journalEntryAccountData['journalEntryAccountsCreditAmount']) > floatval(str_replace(',', '', $value->paidAmount))) {
                            
                            $jeAccData = [
                                'journalEntryAccountsCreditAmount' => floatval($journalEntryAccountData['journalEntryAccountsCreditAmount']) - floatval(str_replace(',', '', $value->paidAmount))
                            ];

                            $updateJEAData = $this->CommonTable('JournalEntryAccountsTable')->updateJournalEntryAccounts($jeAccData, $journalEntryAccountData['journalEntryAccountsID']);


                            //update old gl account credit runing balance
                            $oldGLACCUpdateData = [
                                'financeAccountsCreditAmount' => floatval($oldFAData['financeAccountsCreditAmount']) - floatval(str_replace(',', '', $value->paidAmount))

                            ];

                            $updateOldFAData = $this->CommonTable('FinanceAccountsTable')->updateFinanceAccounts($oldGLACCUpdateData, $oldFAData['financeAccountsID']);


                            $newJEAData = [
                                'journalEntryID' => $journalEntryAccountData['journalEntryID'],
                                'financeAccountsID' => $newFAData['financeAccountsID'],
                                'financeGroupsID' => 0,
                                'journalEntryAccountsDebitAmount' => 0,
                                'journalEntryAccountsCreditAmount' => floatval(str_replace(',', '', $value->paidAmount)),
                                'journalEntryAccountsMemo' => $journalEntryAccountData['journalEntryAccountsMemo'],
                                'yearEndCloseFlag' => $journalEntryAccountData['yearEndCloseFlag'],
                            ];

                            $journalEntryAccounts= new JournalEntryAccounts();
                            $journalEntryAccounts->exchangeArray($newJEAData);

                            $saveNewJEAData = $this->CommonTable('JournalEntryAccountsTable')->saveJournalEntryAccounts($journalEntryAccounts);

                            //update new gl account credit runing balance
                            $newGLACCUpdateData = [
                                'financeAccountsCreditAmount' => floatval($newFAData['financeAccountsCreditAmount']) + floatval(str_replace(',', '', $value->paidAmount))

                            ];
                            $updateNewFAData = $this->CommonTable('FinanceAccountsTable')->updateFinanceAccounts($newGLACCUpdateData, $newFAData['financeAccountsID']);

                        }
                    }

                    $data = array(
                        'outGoingPaymentMethodBankTransferBankId' => (!empty($value->bankID)) ? $value->bankID : null,
                        'outGoingPaymentMethodBankTransferAccountId' => (!empty($value->accountID)) ? $value->accountID : null,
                        'outGoingPaymentMethodBankTransferSupplierBankName' => (!empty($value->supplierBank)) ? $value->supplierBank : null,
                        'outGoingPaymentMethodBankTransferSupplierAccountNumber' => (!empty($value->supplierAccountNumber)) ? $value->supplierAccountNumber : null,
                        'outGoingPaymentMethodFinanceAccountID' => (!empty($accData['financeAccountID'])) ? $accData['financeAccountID'] : null
                    );
                }
                
                if(isset($data)){

                    $updataExData = $this->CommonTable('SupplierPaymentMethodNumbersTable')->updatePaymentMethodsNumbers($data, $id);
                    
                    $prePaymentMethods = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPaymentMethodDetailsByPaymentId($paymentId);

                    foreach ($prePaymentMethods as $pkey => $pvalue) {
                        if($pvalue['outGoingPaymentMethodID'] == $value->methodID){
                            $supplierPaymentMethodReferenceNumber = (isset($data['outGoingPaymentMethodReferenceNumber']))? $data['outGoingPaymentMethodReferenceNumber'] : '';
                            $supplierPaymentMethodChequeBankAccountID = (isset($data['outGoingPaymentMethodChequeBankAccountID']))? $data['outGoingPaymentMethodChequeBankAccountID'] : '';
                            $supplierPaymentMethodCardID = (isset($data['outGoingPaymentMethodCardID']))? $data['outGoingPaymentMethodCardID'] : '';
                            $supplierPaymentMethodBankTransferBankId = (isset($data['outGoingPaymentMethodBankTransferBankId']))? $data['outGoingPaymentMethodBankTransferBankId'] : '';
                            $supplierPaymentMethodBankTransferAccountId = (isset($data['outGoingPaymentMethodBankTransferAccountId']))? $data['outGoingPaymentMethodBankTransferAccountId'] : '';
                            $supplierPaymentMethodBankTransferSupplierBankName = (isset($data['outGoingPaymentMethodBankTransferSupplierBankName']))? $data['outGoingPaymentMethodBankTransferSupplierBankName'] : '';
                            $supplierPaymentMethodBankTransferSupplierAccountNumber = (isset($data['outGoingPaymentMethodBankTransferSupplierAccountNumber']))? $data['outGoingPaymentMethodBankTransferSupplierAccountNumber'] : '';
                            $newPMData = array(
                                'supplierPaymentMethodID' =>$pvalue['outGoingPaymentMethodID'],
                                'supplierPaymentMethodReferenceNumber' =>$supplierPaymentMethodReferenceNumber,
                                'supplierPaymentMethodChequeBankAccountID' =>$supplierPaymentMethodChequeBankAccountID,
                                'supplierPaymentMethodCardID' =>$supplierPaymentMethodCardID,
                                'supplierPaymentMethodBankTransferBankId' =>$supplierPaymentMethodBankTransferBankId,
                                'supplierPaymentMethodBankTransferAccountId' =>$supplierPaymentMethodBankTransferAccountId,
                                'supplierPaymentMethodBankTransferSupplierBankName' =>$supplierPaymentMethodBankTransferSupplierBankName,
                                'supplierPaymentMethodBankTransferSupplierAccountNumber' =>$supplierPaymentMethodBankTransferSupplierAccountNumber,
                            );

                            $previousPMData = array(
                                'supplierPaymentMethodID' => $pvalue['outGoingPaymentMethodID'],
                                'supplierPaymentMethodReferenceNumber' =>$pvalue['outGoingPaymentMethodReferenceNumber'],
                                'supplierPaymentMethodChequeBankAccountID' =>$pvalue['outGoingPaymentMethodChequeBankAccountID'],
                                'supplierPaymentMethodCardID' =>$pvalue['outGoingPaymentMethodCardID'],
                                'supplierPaymentMethodBankTransferBankId' =>$pvalue['outGoingPaymentMethodBankTransferBankId'],
                                'supplierPaymentMethodBankTransferAccountId' =>$pvalue['outGoingPaymentMethodBankTransferAccountId'],
                                'supplierPaymentMethodBankTransferSupplierBankName' =>$pvalue['outGoingPaymentMethodBankTransferSupplierBankName'],
                                'supplierPaymentMethodBankTransferSupplierAccountNumber' =>$pvalue['outGoingPaymentMethodBankTransferSupplierAccountNumber'],
                            );
                            $previousPMetData = json_encode($previousPMData);
                            $newPMetData = json_encode($newPMData);
                            $this->setLogDetailsArray($previousPMetData,$newPMetData);
                        }
                    }
                }
            }

            $this->setLogMessage("Edit supplier payments methods in payment ".$paymentData->outgoingPaymentCode);
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('SUC_PAY_METHOD_UPDATE');
            return $this->JSONRespond();
        }
    }



    public function getAllRelatedDocumentDetailsByPaymentIDAction()
    {
        $request = $this->getRequest();
        // $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $payID = $request->getPost('payID');
            $type = $request->getPost('type');
            $documentTypeID = $request->getPost('documentTypeID');

            if ($type != null && $type != "") {
                return $this->getJEForRelatedDocs($payID, $type, $documentTypeID);
            }

            $purchaseInvoices =  $this->CommonTable('SupplierInvoicePaymentsTable')->getPIDataByPaymentID($payID);
            $pvDataSet =  $this->CommonTable('SupplierInvoicePaymentsTable')->getPVDataByPaymentID($payID);

            $piDetails = [];
            foreach ($purchaseInvoices as $key => $value) {
               $data =  $this->processedIvoiceRelatedDocs($value['purchaseInvoiceID']);

               $piDetails = array_merge($piDetails,$data);
            }

            $payData = $this->CommonTable('SupplierPaymentsTable')->getPaymentsByPaymentID($payID);
            
            if (isset($payData)) {
                foreach ($payData as $payment) {
                    $piPaymentData = array(
                        'type' => 'SupplierPayment',
                        'documentID' => $payment['outgoingPaymentID'],
                        'code' => $payment['outgoingPaymentCode'],
                        'amount' => number_format($payment['outgoingPaymentAmount'], 2),
                        'issuedDate' => $payment['outgoingPaymentDate'],
                        'created' => $payment['createdTimeStamp'],
                    );
                    $piDetails[] = $piPaymentData;
                    if (isset($payment)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($pvDataSet)) {
                foreach ($pvDataSet as $pv) {
                    $pvData = array(
                        'type' => 'PaymentVoucher',
                        'documentID' => $pv['paymentVoucherId'],
                        'code' => $pv['paymentVoucherCode'],
                        'amount' => number_format($pv['paymentVoucherTotal'], 2),
                        'issuedDate' => $pv['paymentVoucherDueDate'],
                        'created' => $pv['createdTimeStamp'],
                    );
                    $piDetails[] = $pvData;
                    if (isset($pv)) {
                        $dataExistsFlag = true;
                    }
                }
            }


            $sortData = Array();
            foreach ($piDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $piDetails);

            $documentDetails = array(
                'piDetails' => $piDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

    public function getJEForRelatedDocs($docID, $type, $documentTypeID = null)
    {
        if ($type == "directJE") {
            $journalEntry = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryID($docID);

            $pvData = array(
                'type' => 'JournalEntry',
                'documentID' => $journalEntry['journalEntryID'],
                'code' => $journalEntry['journalEntryCode'],
                'amount' => '-',
                'issuedDate' => $journalEntry['journalEntryDate'],
                'created' => $journalEntry['createdTimeStamp'],
            );
            $piDetails[] = $pvData;

            $documentDetails = array(
                'piDetails' => $piDetails
            );

            if ($journalEntry) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        } else if ($type == "otherJE") {
            if ($documentTypeID == "16") {
                $adjustmentDataData = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getAdjustmentByAdjustmentIDForRec($docID)->current();

                $pvData = array(
                    'type' => 'GoodIssue',
                    'documentID' => $adjustmentDataData['goodsIssueID'],
                    'code' => $adjustmentDataData['goodsIssueCode'],
                    'amount' => '-',
                    'issuedDate' => $adjustmentDataData['goodsIssueDate'],
                    'created' => $adjustmentDataData['createdTimeStamp'],
                );
                $piDetails[] = $pvData;

                $documentDetails = array(
                    'piDetails' => $piDetails
                );

                if ($adjustmentDataData) {
                    $this->data = $documentDetails;
                    $this->status = true;
                } else {
                    $this->status = false;
                }

                return $this->JSONRespond();
            } else if ($documentTypeID == "28") {
                $pettyCashVoucherData = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCasshVoucherByPettyCasshVoucherIDForRec($docID)->current();

                $pvData = array(
                    'type' => 'PettyCashVoucher',
                    'documentID' => $pettyCashVoucherData['pettyCashVoucherID'],
                    'code' => $pettyCashVoucherData['pettyCashVoucherNo'],
                    'amount' => number_format($pettyCashVoucherData['pettyCashVoucherAmount'], 2),
                    'issuedDate' => $pettyCashVoucherData['pettyCashVoucherDate'],
                    'created' => $pettyCashVoucherData['createdTimeStamp'],
                );
                $piDetails[] = $pvData;

                $documentDetails = array(
                    'piDetails' => $piDetails
                );

                if ($pettyCashVoucherData) {
                    $this->data = $documentDetails;
                    $this->status = true;
                } else {
                    $this->status = false;
                }

                return $this->JSONRespond();
            }
        }
    }


    public function processedIvoiceRelatedDocs($piID) {

        $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataWithTimeStampByID($piID);
        $poData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseOrderDataRelatedToPi($piID);
        $grnData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getGrnDetailsByPiId($piID);
        $grnRelatedPoData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPoDetailsRelatedToGrnByPiId($piID);
        $prDataRelatedToGrn = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPrDetailsRelatedToGrnByPiId($piID);
        $debitNoteDataByPi = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPiRelatedDebitNoteDataByPiId($piID);
        // $piPaymentData = $this->CommonTable('SupplierPaymentsTable')->getPiPaymentDetailsByPiId($piID);
        $debitNotePaymentData = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentDetailsByPiId($piID);
        $piDetails = [];
        $dataExistsFlag = false;
        if ($piData) {
            $currentPiData = array(
                'type' => 'PurchaseInvoice',
                'documentID' => $piData['purchaseInvoiceID'],
                'code' => $piData['purchaseInvoiceCode'],
                'amount' => number_format($piData['purchaseInvoiceTotal'], 2),
                'issuedDate' => $piData['purchaseInvoiceIssueDate'],
                'created' => $piData['createdTimeStamp'],
            );
            $piDetails[] = $currentPiData;
            $dataExistsFlag = true;
        }
        if (isset($poData)) {
            foreach ($poData as $poDta) {
                if (!is_null($poDta['purchaseOrderID'])) {
                    $purchaseOrderDta = array(
                        'type' => 'PurchaseOrder',
                        'documentID' => $poDta['purchaseOrderID'],
                        'code' => $poDta['purchaseOrderCode'],
                        'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                        'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                        'created' => $poDta['createdTimeStamp'],
                    );
                    $piDetails[] = $purchaseOrderDta;
                    if (isset($poDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
        }
        if (isset($grnData)) {
            foreach ($grnData as $grDta) {
                if (!is_null($grDta['grnID'])) {
                    $grnDta = array(
                        'type' => 'Grn',
                        'documentID' => $grDta['grnID'],
                        'code' => $grDta['grnCode'],
                        'amount' => number_format($grDta['grnTotal'], 2),
                        'issuedDate' => $grDta['grnDate'],
                        'created' => $grDta['createdTimeStamp'],
                    );
                    $piDetails[] = $grnDta;
                    if (isset($grDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
        }
        if (isset($grnRelatedPoData)) {
            foreach ($grnRelatedPoData as $poDta) {
                if (!is_null($poDta['purchaseOrderID'])) {
                    $purchaseOrderDta = array(
                        'type' => 'PurchaseOrder',
                        'documentID' => $poDta['purchaseOrderID'],
                        'code' => $poDta['purchaseOrderCode'],
                        'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                        'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                        'created' => $poDta['createdTimeStamp'],
                    );
                    $piDetails[] = $purchaseOrderDta;
                    if (isset($poDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
        }
        if (isset($prDataRelatedToGrn)) {
            foreach ($prDataRelatedToGrn as $prDta) {
                if (!is_null($prDta['purchaseReturnID'])) {
                    $pReturnData = array(
                        'type' => 'PurchaseReturn',
                        'documentID' => $prDta['purchaseReturnID'],
                        'code' => $prDta['purchaseReturnCode'],
                        'amount' => number_format($prDta['purchaseReturnTotal'], 2),
                        'issuedDate' => $prDta['purchaseReturnDate'],
                        'created' => $prDta['createdTimeStamp'],
                    );
                    $piDetails[] = $pReturnData;
                    if (isset($prDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
        }
        if (isset($debitNoteDataByPi)) {
            foreach ($debitNoteDataByPi as $debitNoteDta) {
                $dNData = array(
                    'type' => 'DebitNote',
                    'documentID' => $debitNoteDta['debitNoteID'],
                    'code' => $debitNoteDta['debitNoteCode'],
                    'amount' => number_format($debitNoteDta['debitNoteTotal'], 2),
                    'issuedDate' => $debitNoteDta['debitNoteDate'],
                    'created' => $debitNoteDta['createdTimeStamp'],
                );
                $piDetails[] = $dNData;
                if (isset($debitNoteDta)) {
                    $dataExistsFlag = true;
                }
            }
        }

        if (isset($debitNotePaymentData)) {
            foreach ($debitNotePaymentData as $dNPayDta) {
                $dNPaymentData = array(
                    'type' => 'DebitNotePayment',
                    'documentID' => $dNPayDta['debitNotePaymentID'],
                    'code' => $dNPayDta['debitNotePaymentCode'],
                    'amount' => number_format($dNPayDta['debitNotePaymentAmount'], 2),
                    'issuedDate' => $dNPayDta['debitNotePaymentDate'],
                    'created' => $dNPayDta['createdTimeStamp'],
                );
                $piDetails[] = $dNPaymentData;
                if (isset($dNPayDta)) {
                    $dataExistsFlag = true;
                }
            }
        }

        return $piDetails;


    }

}

?>
