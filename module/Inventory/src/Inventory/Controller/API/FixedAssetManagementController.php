<?php

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Inventory\Model\Depreciation;

class FixedAssetManagementController extends CoreController {

	/**
	 * This function is used to save depreciation
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function saveDepreciationAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$postData = $request->getPost();

		$this->beginTransaction();
		$entityID = $this->createEntity();
		$depData = array(
			'productID' => $postData['productID'],
			'depreciationValue' => $postData['depreciationValue'],
			'depreciationDate' => $this->convertDateToStandardFormat($postData['depreciationDate']),
			'entityID' => $entityID,
		);


		$depreciationData = new Depreciation();
		$depreciationData->exchangeArray($depData);
		$depreciationID = $this->CommonTable('Inventory/Model/DepreciationTable')->saveDepreciation($depreciationData);

		if ($depreciationID) {
			$productsArr = $this->CommonTable('Inventory/Model/ProductTable')->getProductByProductID($postData['productID']);

			$journalEntryAccounts[1]['fAccountsIncID'] = 1;
            $journalEntryAccounts[1]['financeAccountsID'] = $postData['depreciationAcID'];
            $journalEntryAccounts[1]['financeGroupsID'] = '';
            $journalEntryAccounts[1]['journalEntryAccountsDebitAmount'] = $postData['depreciationValue'];
            $journalEntryAccounts[1]['journalEntryAccountsCreditAmount'] = 0;
            $journalEntryAccounts[1]['journalEntryAccountsMemo'] = 'Created by depreciation of fixed asset '.$productsArr['productName'];

            $journalEntryAccounts[2]['fAccountsIncID'] = 2;
            $journalEntryAccounts[2]['financeAccountsID'] = $postData['accumulatedDepreciationAcID'];
            $journalEntryAccounts[2]['financeGroupsID'] = '';
            $journalEntryAccounts[2]['journalEntryAccountsDebitAmount'] = 0;
            $journalEntryAccounts[2]['journalEntryAccountsCreditAmount'] = $postData['depreciationValue'];
            $journalEntryAccounts[2]['journalEntryAccountsMemo'] = 'Created by accumulated depreciation of fixed asset '.$productsArr['productName'];

            $journalEntryComment = 'Created by depreciation of fixed asset '.$productsArr['productName'];

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $this->convertDateToStandardFormat($postData['depreciationDate']),
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => $journalEntryComment,
                'documentTypeID' => 41,
	            'journalEntryDocumentID' => $depreciationID,
	            'ignoreBudgetLimit' => $postData['ignoreBudgetLimit'],
            );
            $resultData = $this->saveJournalEntry($journalEntryData);

            if ($resultData['status']) {
            	$this->commit();
				$this->status = true;
				$this->data = $depreciationID;
				$this->msg = $this->getMessage("SUCC_SAVE_DEPRECIATION");
				return $this->JSONRespond();
            } else {
            	$this->rollback();
				$this->status = false;
				$this->data = $resultData['data'];
				$this->msg = $resultData['msg'];
				return $this->JSONRespond();
            }
		} else {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage("ERR_SAVE_DEPRECIATION");
			return $this->JSONRespond();
		}
	}
}
