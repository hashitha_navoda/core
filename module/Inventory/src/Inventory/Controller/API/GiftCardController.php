<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Gift Card controller API functions
 */

namespace Inventory\Controller\API;

use Zend\View\Model\ViewModel;
use Inventory\Model\Product;
use Inventory\Model\ProductHandeling;
use Inventory\Model\LocationProduct;
use Inventory\Model\ProductUom;
use Inventory\Model\GiftCard;
use Inventory\Model\ItemBarcode;
use Inventory\Form\ProductForm;
use Zend\Session\Container;
use Core\Controller\CoreController;

class GiftCardController extends CoreController
{
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    //gift card save action
    public function saveAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $productName = $request->getPost('giftCardName');
            $startValue = $request->getPost('startValue');
            $endValue = $request->getPost('endValue');
            $prefixValue = $request->getPost('giftCardPrefix');
            $giftCardDuration = $request->getPost('giftCardDurationDays');
            $productUnitPrice = $request->getPost('giftCardPrice');
            $giftCardGlAccountID = $request->getPost('giftCardGlAccountID');
            $ignoreBudgetLimit = $request->getPost('giftCardGlAccountID');

            if($this->useAccounting == 1 && ($giftCardGlAccountID == '' || $giftCardGlAccountID == 0)){
                $this->status = false;
                $this->msg = $this->getMessage('SUCC_GIFTCARD_ACCOUNT_NOT_SET');
                return $this->JSONRespond();
            } 

            $categoryName = 'General';
            $getDefaultCategory = $this->CommonTable('Inventory\Model\CategoryTable')->getCategoryByCategoryName($categoryName);

            $name = str_replace('_', '-', str_replace(' ', '-', $productName));
            $productCode = $name . '-' . $productUnitPrice;
            $post = [
                'productCode' => $productCode,
                'productBarcode' => '',
                'productName' => $productName,
                'productTypeID' => 1,
                'productDescription' => '',
                'productDiscountEligible' => ($request->getPost('giftCardDiscountPrecentage')) ? 1 : 0,
                'productDiscountPercentage' => $request->getPost('giftCardDiscountPrecentage'),
                'productDiscountValue' => '',
                'productImageURL' => '',
                'productState' => 1,
                'categoryID' => $getDefaultCategory->categoryID,
                'productGlobalProduct' => 1,
                'batchProduct' => '',
                'serialProduct' => 1,
                'hsCode' => '',
                'customDutyValue' => '',
                'customDutyPercentage' => '',
                'productDefaultOpeningQuantity' => '',
                'productDefaultSellingPrice' => $productUnitPrice,
                'productDefaultMinimumInventoryLevel' => '',
                'productDefaultReorderLevel' => '',
                'productTaxEligible' => 0,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            ];

//          If product having batch or serial product default opening quantity will be zero
            if ($post['batchProduct'] || $post['serialProduct'] || ($post['productTypeID'] == 2)) {
                $post['productDefaultOpeningQuantity'] = 0;
            }

            $this->beginTransaction();

// check if a Product from the same Product Code exists
            $checkProduct = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($post['productCode']);
            if ($checkProduct) {
                $addedID = $checkProduct['productID'];
            } else {
                $product = new Product();
// validate form inputs
                $form = new ProductForm();
                $form->setInputFilter($product->getInputFilter());
                $form->setValidationGroup(array('productCode', 'productBarcode', 'productName', 'productTypeID', 'categoryID', 'productDescription'));
                $form->setData($post);
                if (!$form->isValid()) {
                    $this->status = false;
                    $this->msg = _(array_values(array_values($form->getMessages())[0])[0]);
                    $this->rollback();
                    return $this->JSONRespond();
                }

// replace array with new filtered values
                $post = array_replace($post, $form->getData());

// create entity for product
                $entityID = $this->createEntity();
                $post['entityID'] = $entityID;

                $productData = [];
                $post['productCode'] = $productCode;
                if($post['productBarcode'] == ''){
                    $post['productBarcode'] = $productCode;
                    $productData['itemBarCodes'] = [
                        [
                            "itemBarCodeID" => "new",
                            "itemBarCode" => $productCode,
                        ]

                    ];
                }

                $product->exchangeArray($post);

                $addedID = $this->CommonTable('Inventory\Model\ProductTable')->saveProduct($product);

                $uomName = 'Unit';
                $getDefaultUom = $this->CommonTable('Settings\Model\UomTable')->getUomByName($uomName);

// if product was successfully added
                if ($addedID) {


                    $itemBarcode = new ItemBarcode();
                    foreach ($productData['itemBarCodes'] as $key => $value) {
                        $getbarcodeDetail = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->getBarcodeByName($value['itemBarCode'], $addedID);

                        if (sizeof($getbarcodeDetail) > 0) {
                            return ['status' => false, 'msg'=> $this->getMessage('ERR_BARCODE_EXIST', array($value['itemBarCode'])), 'data' => null];
                        }

                        $dataSet = [
                            'productID' => $addedID,
                            'barCode' => $value['itemBarCode']
                        ];
                        $itemBarcode->exchangeArray($dataSet);

                        $addBarCode = $this->CommonTable('Inventory\Model\ItemBarcodeTable')->saveItemBarcode($itemBarcode);
                    }



                    $data = array(
                        'productID' => $addedID,
                        'productHandelingManufactureProduct' => 0,
                        'productHandelingPurchaseProduct' => 0,
                        'productHandelingSalesProduct' => 0,
                        'productHandelingConsumables' => 0,
                        'productHandelingFixedAssets' => 0,
                        'productHandelingGiftCard' => 1,
                    );
                    $productHandeling = new ProductHandeling();
                    $productHandeling->exchangeArray($data);


                    if (!$this->CommonTable('Inventory\Model\ProductHandelingTable')->saveProductHandeling($productHandeling)) {
                        $this->rollback();
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMHANDLE'));
                    }

                    $locations = array();
// if location type is global, get all locations
                    if ($post['productGlobalProduct'] == 1) {
                        $locationsArr = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
                        foreach ($locationsArr as $l) {
                            $l = (object) $l;
                            $locationID = $l->locationID;
                            $entityID = $this->createEntity();
                            $data = array(
                                'productID' => $addedID,
                                'locationID' => $locationID,
                                'locationDiscountPercentage' => $post['productDiscountPercentage'],
                                'defaultSellingPrice' => $post['productDefaultSellingPrice'],
                                'entityID' => $entityID,
                            );

                            $locationProduct = new LocationProduct();
                            $locationProduct->exchangeArray($data);

                            if (!$this->CommonTable('Inventory\Model\LocationProductTable')->saveLocationProduct($locationProduct)) {
                                $this->rollback();
                                return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMLOC'));
                            }
                        }
                    }



                    $data = array(
                        'productID' => $addedID,
                        'uomID' => $getDefaultUom->uomID,
                        'productUomConversion' => 1,
                        'productUomBase' => 1,
                        'productUomDisplay' => 1,
                    );
                    $productUom = new ProductUom();
                    $productUom->exchangeArray($data);
                    if (!$this->CommonTable('Inventory\Model\ProductUomTable')->saveProductUom($productUom)) {
                        $this->rollback();
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMUOM'));
                    }
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PRDCTAPI_SAVE');
                    $this->rollback();
                    return $this->JSONRespond();
                }
            }

            $date = date('Y-m-d');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $locationProductID = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($addedID, $locationID)->locationProductID;

            $uomList[$getDefaultUom->uomID] = $getDefaultUom->uomID;
            $increment = 0;
            for ($startValue; $startValue <= $endValue; $startValue++) {
                $giftCardCode = $prefixValue . $startValue;
                $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductsBySerialCodeAndLocationProdutId($giftCardCode, $locationProductID);
                if (!$checkSerial) {
                    //crete serial product array for adjusment
                    $serialProducts[$increment] = array(
                        'sCode' => $giftCardCode,
                        'sEdate' => '',
                        'sWarranty' => '',
                    );
                    $increment++;

                    //insert product serial data in gift card table
                    $giftCardData = array(
                        'giftCardCode' => $giftCardCode,
                        'giftCardDuration' => $giftCardDuration,
                        'giftCardIssueDate' => '',
                        'giftCardExpireDate' => '',
                        'giftCardValue' => $productUnitPrice,
                        'giftCardStatus' => 2,
                        'giftCardGlAccountID' => $giftCardGlAccountID,
                    );

                    $giftCard = new GiftCard();
                    $giftCard->exchangeArray($giftCardData);
                    $this->CommonTable('Inventory\Model\GiftCardTable')->saveGiftCard($giftCard);
                }
            }

            $productQuantity = $increment + 1;
            if ($increment != 0) {
                $totalProductPrice = $productQuantity * $productUnitPrice;
                $item[$addedID] = array(
                    'locationPID' => $locationProductID,
                    'pID' => $addedID,
                    'pCode' => $productCode,
                    'pName' => $productName,
                    'pQuantity' => $productQuantity,
                    'pUnitPrice' => $productUnitPrice,
                    'pUom' => $uomList,
                    'uomID' => 1,
                    'pTotal' => $totalProductPrice,
                    'sProducts' => $serialProducts,
                    'productType' => '',
                    'uomConversionRate' => ''
                );

                $goodsIssueTblData = array(
                    'locRefID' => 'No',
                    'goods_issue_id' => '',
                    'goods_issue_date' => $date,
                    'goods_issue_reason' => "opening balnce from Gift Card",
                    'goods_issue_type_id' => 2,
                );

                $post = array(
                    'locationID' => $locationID,
                    'flag' => 'GiftCard',
                    'goods_issue_tbl_data' => $goodsIssueTblData,
                    'items' => $item,
                );

                $sm = $this->getServiceLocator();
                $invAdj = $sm->get('Inventory\Controller\API\InventoryAdjustment');
                $invAdj->updatePositiveAdjusmentWithoutPost($post);

                $this->commit();

                $this->status = true;
                $this->data = array('productID' => $addedID);
                $this->msg = $this->getMessage('SUCC_GIFT_CARD_ADD');
                $this->setLogMessage('Item was added successfully');
                $this->flashMessenger()->addMessage($this->msg);
            } else {
                $this->rollback();
                $this->status = false;
                $this->data = array('productID' => $addedID);
                $this->msg = $this->getMessage('ALL_SERIAL_CODE_EXIXT_IN_THIS_PRODUCT');
            }
            return $this->JSONRespond();
        } else {

        }
    }

    //get gift Card for search string
    public function getGiftCardForSearchAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            if (trim($request->getPost('searchGiftCardString')) != "") {
                $giftCards = $this->CommonTable('Inventory\Model\ProductTable')->getGiftCardforSearch($request->getPost('searchGiftCardString'), $locationID);
                $paginated = NULL;
            } else {
                $giftCards = $this->getPaginatedGiftCard();
                $paginated = true;
            }

            $htmlOutput = $this->_getListViewHtml($giftCards, $paginated);
            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedGiftCard($locationID)
    {
        $this->paginator = $this->CommonTable('Inventory/Model/ProductTable')->giftCardFetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
//        $this->setLogMessage("Product list accessed");
    }

    private function _getListViewHtml($giftCardList = array(), $paginated = false)
    {

        $view = new ViewModel(array(
            'giftCard' => $giftCardList,
            'paginated' => $paginated,
            'cSymbol' => $this->companyCurrencySymbol,
        ));

        $view->setTerminal(true);
        $view->setTemplate('inventory/gift-card/gift-card-list');

        return $view;
    }

    //delete gift cards
    public function deleteGiftCardsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $giftCardId = $request->getPost('giftCardId');
            $serialData = $request->getPost('serialData');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $count = 0;
            $locationProductID = '';
            $this->beginTransaction();
            foreach ($serialData as $key => $value) {
                $count++;
                $psCode = $value['serialCode'];
                $psId = $value['serialId'];

                $giftData = array(
                    'giftCardStatus' => 5,
                );
                if (!$this->CommonTable('Inventory\Model\GiftCardTable')->updateGiftCardDetailsByGiftCardCode($giftData, $psCode)) {
                    $this->rollback();
                    return;
                }

                $serialProducts[$count] = array(
                    's_code' => $psCode,
                    'sEdate' => '',
                    'sWarranty' => '',
                );

                $sproductData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($psId);
                $locationProductID = $sproductData->locationProductID;
            }

            $uomName = 'Unit';
            $getDefaultUom = $this->CommonTable('Settings\Model\UomTable')->getUomByName($uomName);
            $uomList[$getDefaultUom->uomID] = $getDefaultUom->uomID;
            $product = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductsByLocationProductID($locationProductID);
            $totalProductPrice = $count * $product->productDefaultSellingPrice;
            $locationId = $product->locationID;
            $date = date('Y-m-d');

            $item[$product->productID] = array(
                'locationProductID' => $locationProductID,
                'pID' => $product->productID,
                'pCode' => $product->productCode,
                'pName' => $product->productName,
                'pQuantity' => $count,
                'unit_price' => $product->productDefaultSellingPrice,
                'pUom' => $uomList,
                'uomID' => 1,
                'pTotal' => $totalProductPrice,
                'serial_data' => json_encode($serialProducts),
                'batch_text_data' => '',
                'productType' => 1,
                'uomConversionRate' => ''
            );

            $goodsIssueTblData = array(
                'locRefID' => 'No',
                'goods_issue_id' => '',
                'goods_issue_date' => $date,
                'goods_issue_reason' => "Delete gift Card",
                'goods_issue_type_id' => 1,
                'goods_issue_comment' => '',
            );

            $sm = $this->getServiceLocator();
            $invAdj = $sm->get('Inventory\Controller\API\InventoryAdjustment');
            $invAdj->updateNegativeAdjusment($item, $goodsIssueTblData, $locationId, [], $ignoreBudgetLimit);
            $this->commit();

            $this->status = true;
            $this->msg = $this->getMessage('SUCC_DELETE_GIFT_CARD');
            $this->setLogMessage('Item was deleted successfully');

            return $this->JSONRespond();
        }
    }

    public function changeStateAction()
    {
        $req = $this->getRequest();
        $verb = ($req->getPost('productState')) ? 'activated' : 'deactivated';
        $this->status = false;
        $this->msg = $this->getMessage('ERR_GIFTCARD_STATUS', array($verb));

        if ($req->isPost()) {
            $product = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($req->getPost('productID'));
            $productID = $req->getPost('productID');
            $post = [
                'productState' => (int) $req->getPost('productState'),
                'productID' => $productID
            ];

            $product = new Product();
            $product->exchangeArray($post);
            $this->CommonTable('Inventory\Model\ProductTable')->updateProductState($product);

            $productLocations = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsByProductID($productID);
            $locationIDs = array_map(function($element) {
                return $element['locationID'];
            }, iterator_to_array($productLocations));

            $eventParameter = ['productIDs' => [$productID], 'locationIDs' => $locationIDs];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            $this->status = true;
            $this->msg = $this->getMessage('SUCC_GIFTCARD_STATUS', array($verb));
            $this->flashMessenger()->addMessage($this->msg);
        }
        return $this->JSONRespond();
    }

}
