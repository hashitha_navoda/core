<?php

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Model\PurchaseRequisition;
use Inventory\Model\PurchaseRequisitionProduct;
use Inventory\Model\PurchaseRequisitionProductTax;
use Core\Model\DocumentReference;

class PurchaseRequistionController extends CoreController
{

    protected $paginator;

    public function getPoNoForLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $refData = $this->getReferenceNoForLocation(39, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];
            if ($rid == '' || $rid == NULL) {
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_PURORDER_REFLOC');
                } else {
                    $this->msg = $this->getMessage('ERR_PURORDER_REFMAX');
                }
                $this->data = FALSE;
                $this->status = false;
            } else {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $rid,
                    'locRefID' => $lrefID,
                );
            }

            $this->setLogMessage("Retrive purchase order code.");
            return $this->JSONRespond();
        }
    }

    public function savePurchaseRequisitionAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $purchaseOrderCode = $post['poC'];
            $locationReferenceID = $post['lRID'];
            $documentReference = $post['documentReference'];
            $locationID = $post['rL'];
            $this->setLogMessage("Error occured when create purhcase requisition".$purchaseOrderCode.".");
// check if a purchase order from the same purchase order Code exists if exist add next number to purchase order code
            while ($purchaseOrderCode) {
                if ($this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->checkpurchaseRequisitionByCode($purchaseOrderCode)->current()) {
                    if ($locationReferenceID) {
                        $newPurchaseOrderCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newPurchaseOrderCode == $purchaseOrderCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $purchaseOrderCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $purchaseOrderCode = $newPurchaseOrderCode;
                        }
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PREQ_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }
            $entityID = $this->createEntity();
            $hashValue = hash('md5', $purchaseOrderCode);
            $poData = array(
                'purchaseRequisitionCode' => $purchaseOrderCode,
                'purchaseRequisitionSupplierID' => $post['sID'],
                'purchaseRequisitionSupplierReference' => $post['sR'],
                'purchaseRequisitionRetrieveLocation' => $locationID,
                'purchaseRequisitionEstDelDate' => $this->convertDateToStandardFormat($post['eDD']),
                'purchaseRequisitionDate' => $this->convertDateToStandardFormat($post['poD']),
                'purchaseRequisitionDescription' => $post['cm'],
                'purchaseRequisitionDeliveryCharge' => $post['dC'],
                'purchaseRequisitionTotal' => $post['fT'],
                'purchaseRequisitionShowTax' => $post['sT'],
                'purchaseRequisitionType' => $post['purchaseReqType'],
                'purchaseRequisitionHashValue' => $hashValue,
                'entityID' => $entityID,
            );
            $poM = new PurchaseRequisition();
            $poM->exchangeArray($poData);
            $poID = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->savePReq($poM);
            $this->updateReferenceNumber($locationReferenceID);
            $poProductM = new PurchaseRequisitionProduct();
            foreach ($post['pr'] as $product) {
                if (!empty($product)) {
                    $poProductData = array(
                        'purchaseRequisitionID' => $poID,
                        'locationProductID' => $product['locationPID'],
                        'purchaseRequisitionProductQuantity' => $product['pQuantity'],
                        'purchaseRequisitionProductPrice' => $product['pUnitPrice'],
                        'purchaseRequisitionProductDiscount' => $product['pDiscount'],
                        'purchaseRequisitionProductTotal' => $product['pTotal'],
                    );
                    $poProductM->exchangeArray($poProductData);
                    $insertedPoProductID = $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')->savePurchaseRequisitionProduct($poProductM);
                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $poPTaxesData = array(
                                    'purchaseRequisitionID' => $poID,
                                    'purchaseRequisitionProductID' => $insertedPoProductID,
                                    'purchaseRequisitionTaxID' => $taxKey,
                                    'purchaseRequisitionTaxPrecentage' => $productTax['tP'],
                                    'purchaseRequisitionTaxAmount' => $productTax['tA']
                                );
                                $poPTaxM = new PurchaseRequisitionProductTax();
                                $poPTaxM->exchangeArray($poPTaxesData);
                                $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTaxTable')->savePurchaseRequisitionProductTax($poPTaxM);
                            }
                        }
                    }
                }
            }

            $approvers = $this->getApprovers($post['purchaseReqType'], $post['fT']);
            if (!empty($approvers)) {
                $approversFlag = true;

                $draftData = [
                    'purchaseRequisitionDraftFlag' => 1,
                    'status' => 7,
                    'purchaseRequisitionID' => $poID,
                ];

                $draftRes = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->updateDraftDetails($draftData);
            
                if (!$draftRes) {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PREQ_DRAFT');
                    return $this->JSONRespond();
                }

            } else {
                $approversFlag = false;
            }
            
            //      call product updated event
            $productIDs = array_map(function($element) {
                return $element['pID'];
            }, $post['pr']);

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            $this->status = true;
            $this->data = array('poID' => $poID, 'approversFlag' => $approversFlag, 'purchaseRequisitionCode' => $purchaseOrderCode, 'approver' => $approvers, 'hashValue' => $hashValue);

            $this->msg = $this->getMessage('SUCC_PREQ_CREATE', array($purchaseOrderCode));
            $this->setLogMessage($this->companyCurrencySymbol . $post['fT'] . ' amount purchase requisition successfully created - ' . $purchaseOrderCode.".");
            return $this->JSONRespond();
        }
    }

    public function getApprovers($typeID, $totalPrice)
    {
        $approvers = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeTable')->getApproversByTypeIDAndPriceRange($typeID, $totalPrice);
        $approversArray = array();
        $i = 0;
        foreach ($approvers as $apr) {
            $approversArray[$i]['purchaseRequisitionTypeId'] = $apr['purchaseRequisitionTypeId'];
            $approversArray[$i]['purchaseRequisitionTypeName'] = $apr['purchaseRequisitionTypeName'];
            $approversArray[$i]['purchaseRequisitionTypeCategory'] = $apr['purchaseRequisitionTypeCategory'];
            $approversArray[$i]['purchaseRequisitionTypeLimitApproverID'] = $apr['purchaseRequisitionTypeLimitApprover'];
            $approversArray[$i]['ApproverFirstName'] = $apr['firstName'];
            $approversArray[$i]['ApproverLastName'] = $apr['lastName'];
            $approversArray[$i]['ApproverEmail'] = $apr['email'];
            $i++;
        }
        return $approversArray;
    }

    public function sendApproverEmailAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pReqId = $request->getPost('pReqId');
            $pReqCode = $request->getPost('pReqCode');
            $approvers = $request->getPost('approvers');
            $token = $request->getPost('token');
            if ($pReqId && $pReqCode && $approvers && $token) {
                $this->sendPurchaseRequisitionApproversEmail($pReqId, $pReqCode, $approvers, $token);
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PREQ_SEND_FOR_WF');
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PREQ_SEND_FOR_WF');
            }
        }
        return $this->JSONRespond();
    }

    private function sendPurchaseRequisitionApproversEmail($pReqId, $pReqCode, $approvers, $token)
    {
        foreach ($approvers as $approver) {
            $approveUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/api/purchase-requistion/approve/?pReqId=' . $pReqId . '&action=approve&token=' . $token;
            $rejectUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/api/purchase-requistion/approve/?pReqId=' . $pReqId . '&action=reject&token=' . $token;
            $approvePvEmailView = new ViewModel(array(
                'name' => $approver['ApproverFirstName'] . ' ' . $approver['ApproverLastName'],
                'pReqCode' => $pReqCode,
                'approveLink' => $approveUrl,
                'rejectLink' => $rejectUrl,
            ));
            $approvePvEmailView->setTemplate('/core/email/purchase-requisition-approve');
            $renderedEmailHtml = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($approvePvEmailView);
            $documentType = 'Purchase Requisition';
            $this->sendDocumentEmail($approver['ApproverEmail'], "Request for Approve - Purchase Requisition - $pReqCode", $renderedEmailHtml, $documentType, $pReqId);
        }
    }

    public function approveAction()
    {
        $action = $_GET['action'];
        $pReqId = $_GET['pReqId'];
        $token = $_GET['token'];
        $pReqDetails = $this->CommonTable("Inventory\Model\PurchaseRequisitionTable")->getPurchaseRequisitionByID($pReqId);
        $pReqCode = $pReqDetails->purchaseRequisitionCode;
        $this->beginTransaction();
        if ($pReqDetails->purchaseRequisitionHashValue == $token) {
            if ($action == 'approve') {
                if ($pReqDetails->purchaseRequisitionApproved == 0 && $pReqDetails->status == 13) {
                    $this->commit();
                    echo "Purchase Requisition $pReqCode is already rejected, you cannot approve any more..!";
                    exit();
                }

                if ($pReqDetails->purchaseRequisitionApproved != 1) {
                    $draftData = [
                        'purchaseRequisitionDraftFlag' => 0,
                        'purchaseRequisitionApproved' => 1,
                        'status' => 3,
                        'purchaseRequisitionID' => $pReqDetails->purchaseRequisitionID,
                    ];

                    $draftRes = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->updateDraftDetails($draftData);

                    if(!$draftRes){
                        $this->rollback();
                        echo $this->getMessage('ERR_PREQ_DRAFT_TO_PREQ');
                        exit();
                    }else{
                        $this->commit();
                        echo "Purchase Requisition $pReqCode approved..!";
                        exit();
                    }
                } else {
                    $this->commit();
                    echo "Purchase Requisition $pReqCode is already approved..!";
                    exit();
                }
            } else {
                if ($pReqDetails->purchaseRequisitionApproved == 1) {
                    $this->commit();
                    echo "Purchase Requisition $pReqCode is already approved, you cannot reject any more..!";
                    exit();
                }

                if ($pReqDetails->purchaseRequisitionApproved == 0 && $pReqDetails->status == 13) {
                    $this->commit();
                    echo "Purchase Requisition $pReqCode is already rejected..!";
                    exit();
                } else {
                    $draftData = [
                        'purchaseRequisitionDraftFlag' => 0,
                        'purchaseRequisitionApproved' => 0,
                        'status' => 13,
                        'purchaseRequisitionID' => $pReqDetails->purchaseRequisitionID,
                    ];

                    $draftRes = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->updateDraftDetails($draftData);

                    if(!$draftRes){
                        $this->rollback();
                        echo $this->getMessage('ERR_PREQ_DRAFT_TO_PREQ');
                        exit();
                    }else{
                        $this->commit();
                        echo "Purchase Requisition $pReqCode rejected..!";
                        exit();
                    }
                }
            }
        } else {
            $this->rollback();
            echo 'Token Mismatch';
            exit();
        }
    }

    public function getPreqDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $poID = $request->getPost('poID');
            $toGrn = $request->getPost('toGrn');
            if ($toGrn) {
                $toGrn = TRUE;
            } else {
                $toGrn = FALSE;
            }
            $poData = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getPReqByPoID($poID, $toGrn);
            $poDetails = array();
            $poID;
            while ($row = $poData->current()) {
                $tempPo = array();
                $poID = $row['purchaseRequisitionID'];
                $tempPo['poID'] = $row['purchaseRequisitionID'];
                $tempPo['poC'] = $row['purchaseRequisitionCode'];
                $tempPo['poSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempPo['poSID'] = $row['purchaseRequisitionSupplierID'];
                $tempPo['poSR'] = $row['purchaseRequisitionSupplierReference'];
                $tempPo['purchaseRequisitionType'] = $row['purchaseRequisitionType'];
                $tempPo['poRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempPo['poRLID'] = $row['purchaseRequisitionRetrieveLocation'];
                $tempPo['poD'] = $this->convertDateToUserFormat($row['purchaseRequisitionEstDelDate']);
                $tempPo['purchaseOrderDate'] = $this->convertDateToUserFormat($row['purchaseRequisitionDate']);
                $tempPo['poDC'] = $row['purchaseRequisitionDeliveryCharge'];
                $tempPo['poT'] = $row['purchaseRequisitionTotal'];
                $tempPo['poDes'] = $row['purchaseRequisitionDescription'];
                $tempPo['poSTax'] = $row['purchaseRequisitionShowTax'];
                $tempPo['productType'] = $row['productTypeID'];
                $tempPo['poSOB'] = $row['supplierOutstandingBalance'];
                $tempPo['poSCB'] = $row['supplierCreditBalance'];
                $poProducts = (isset($poDetails[$row['purchaseRequisitionID']]['poProducts'])) ? $poDetails[$row['purchaseRequisitionID']]['poProducts'] : array();
                if ($row['purchaseRequisitionProductID'] != NULL) {
                    $productTaxes = (isset($poProducts[$row['productCode']]['pT'])) ? $poProducts[$row['productCode']]['pT'] : array();
                    $poProducts[$row['productCode']] = array('poPC' => $row['productCode'], 'poPID' => $row['productID'], 'poPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'poPP' => $row['purchaseRequisitionProductPrice'], 'poPD' => $row['purchaseRequisitionProductDiscount'], 'poPQ' => $row['purchaseRequisitionProductQuantity'], 'poPT' => $row['purchaseRequisitionProductTotal'], 'poPUom' => $row['uomAbbr'], 'poPUomID' => $row['uomID'], 'pUDisplay' => $row['productUomDisplay'], 'productType' => $row['productTypeID'], 'copiedQty' => $row['purchaseRequisitionProductCopiedQuantity']);

                    if ($row['purchaseRequisitionTaxID'] != NULL) {
                        $productTaxes[$row['purchaseRequisitionTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['purchaseRequisitionTaxPrecentage'], 'pTA' => $row['purchaseRequisitionTaxAmount']);
                    }
                    $poProducts[$row['productCode']]['pT'] = $productTaxes;
                }
                $tempPo['poProducts'] = $poProducts;
                $poDetails[$row['purchaseRequisitionID']] = $tempPo;
            }
            $poSubTotal = number_format((floatval($poDetails[$poID]['poT']) - floatval($poDetails[$poID]['poDC'])), 2);
            $totalProTaxes = array();
            foreach ($poDetails[$poID]['poProducts'] as $product) {
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = number_format((floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA'])), 2);
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }
            $locationProducts = Array();
            $inactiveItems = Array();
            $locationID = $poDetails[$poID]['poRLID'];
            foreach ($poDetails[$poID]['poProducts'] as $poProducts) {
                $locationProducts[$poProducts['poPID']] = $this->getLocationProductDetails($locationID, $poProducts['poPID']);
                if ($locationProducts[$poProducts['poPID']] == null) {
                    $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($poProducts['poPID']);
                    $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                }
            }

            $inactiveItemFlag = false;
            $errorMsg = null;
            if (!empty($inactiveItems)) {
                $inactiveItemFlag = true;
                $errorItems = implode(",",$inactiveItems);        
                $errorMsg = $this->getMessage('ERR_PREQ_ITM_INACTIVE_COPY', [$errorItems]);
            }

            $uploadedAttachments = [];
            $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($poID, 38);
            $companyName = $this->getSubdomain();
            $path = '/userfiles/' . md5($companyName) . "/attachments";


            foreach ($attachmentData as $value) {
                $temp = [];
                $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
                $temp['documentRealName'] = $value['documentRealName'];
                $temp['docLink'] = $path . "/" .$value['documentName'];
                $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
            }


            $sourceDocumentTypeId=38;
            $sourceDocumentId=$poID;
            $docrefData=$this->getDocumentReferenceBySourceData($sourceDocumentTypeId,$sourceDocumentId);
            $poInfo = array(
                'poData' => $poDetails[$poID],
                'poSubTotal' => $poSubTotal,
                'pTotalTax' => $totalProTaxes,
                'locationProducts' => $locationProducts,
                'docRefData'=>$docrefData,
                'errorMsg'=>$errorMsg,
                'inactiveItemFlag'=>$inactiveItemFlag,
                'uploadedAttachments'=>$uploadedAttachments,
            );

            $this->data = $poInfo;
            $this->status = true;
            return $this->JSONRespond();
        }
    }


    public function changePReqStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $poID = $request->getPost('poID');
            $poData = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getPurchaseRequisitionByID($poID);
            $previouseStatusID = $poData->status;
            $preqCode = $poData->purchaseRequisitionCode;

            $statusList = $this->getStatusesList();
            $previousStatusCode = $statusList[$previouseStatusID];

            $newStatusCode = $request->getPost('statusCode');
            $newStatusID = $this->getStatusID($newStatusCode);
            
            $result = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->updatePReqStatus($poID, $newStatusID);
            if ($result) {
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_PURORDER_STATUS');
                $this->flashMessenger()->addMessage($this->msg);
                $this->setLogMessage($preqCode." Purchase requisition status change ".$previousStatusCode.' to '.$newStatusCode.'.');
            } else {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PURORDER_STATUS');
                $this->setLogMessage("Error occured when change ".$preqCode." purchase requisition status ".$previousStatusCode.' to '.$newStatusCode.'.');
            }
            return $this->JSONRespond();
        }
    }

    public function sendPurchaseRequisitionEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Purchase Requisition';

        if ($request->isPost()) {
            $documentID = $request->getPost('documentID');
            $email = $request->getPost('to_email');
            $preqData = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getPurchaseRequisitionByID($documentID);
            $this->mailDocumentAsTemplate($request, $documentType, true);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_GRN_EMAIL', array($documentType));

            $this->setLogMessage($preqData->purchaseRequisitionCode." purchase requisition document email to ".$email.'.');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_GRN_EMAIL', array($documentType));

        $this->setLogMessage("Error occured when email the purchase requisition document.");
        return $this->JSONRespond();
    }

    public function searchAllPreqForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');

            $this->setLogMessage("Retrive purchase requisition list for dropdown.");
            $this->data = array('list' => $this->_searchPReqForDropdown($searchKey, true, $locationID));
            return $this->JSONRespond();
        }
    }

    private function _searchPReqForDropdown($searchKey, $isAllPRequs = false, $locationID = false)
    {
        if (!$locationID) {
            $locationID = $this->user_session->userActiveLocation["locationID"];
        }
        if ($isAllPRequs) {
            $pos = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->searchPReqForDropdown($searchKey, FALSE, $locationID);
        } else {
            $pos = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->searchPReqForDropdown($searchKey, $this->getStatusID('open'), $locationID, TRUE);
        }

        $poList = array();
        foreach ($pos as $po) {
            $temp['value'] = $po['purchaseRequisitionID'];
            $temp['text'] = $po['purchaseRequisitionCode'];
            $poList[] = $temp;
        }
        return $poList;
    }

    public function getPReqListForSearchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $statuses = $globaldata['statuses'];
            $dateFormat = $this->getUserDateFormat();

            if ($request->getPost('POID') == '' && $request->getPost('supplierID') == '') {
                $userActiveLocation = $this->user_session->userActiveLocation;
                $this->paginator = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getpurchaseRequisitions(true, $userActiveLocation['locationID']);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $poListView = new ViewModel(array('poList' => $this->paginator, 'statuses' => $statuses));
            } else {
                $poList = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getPReqListForSearch($request->getPost('POID'), $request->getPost('supplierID'));
                $poListView = new ViewModel(array('poList' => $poList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
            }

            $poListView->setTemplate('inventory/purchase-requistion/purchase-requistion-list');
            $this->html = $poListView;

            $this->setLogMessage("Purchase requisition list accessed.");
            return $this->JSONRespondHtml();
        }
    }

    public function deletePurchaseRequisitionDraftAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        
        $draftID = $request->getPost('poDraftID');
        // statusID = 5 (canceled)
        $statusID = 5;
        $updateStatus = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->updatePReqStatus($draftID, $statusID);
        if(!$updateStatus){
            $this->status = false;
            return $this->JSONRespond();
        }

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_PREQ_DRAFT_DELETE');
        return $this->JSONRespond();
    }

    /**
    * this function use to update purchase requisition
    * @param json request
    * return json respond
    **/
    public function updatePurchaseRequisitionAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = '';
            return $this->JSONRespond();
        }
        $poDataSet = $request->getPost();
        // validate product qty
        $validateRespond = $this->validateExistingItemQty($poDataSet['poID'],$poDataSet['realProductArray']);
        if(!$validateRespond['status']) {
            $this->status = false;
            $this->msg = $validateRespond['msg'];
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        // update existing po status
        $statusID = 10; // replace
        $updateExistingPo = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->updatePReqStatus($poDataSet['poID'], $statusID);
        
        // create new entity
        $entityID = $this->createEntity();
        $hashValue = hash('md5', $poDataSet['poC']);
        $poData = array(
            'purchaseRequisitionCode' => $poDataSet['poC'],
            'purchaseRequisitionSupplierID' => $poDataSet['sID'],
            'purchaseRequisitionSupplierReference' => $poDataSet['sR'],
            'purchaseRequisitionRetrieveLocation' => $poDataSet['rL'],
            'purchaseRequisitionEstDelDate' => $this->convertDateToStandardFormat($poDataSet['eDD']),
            'purchaseRequisitionDate' => $this->convertDateToStandardFormat($poDataSet['poD']),
            'purchaseRequisitionDescription' => $poDataSet['cm'],
            'purchaseRequisitionDeliveryCharge' => $poDataSet['dC'],
            'purchaseRequisitionTotal' => $poDataSet['fT'],
            'purchaseRequisitionShowTax' => $poDataSet['sT'],
            'purchaseRequisitionType' => $poDataSet['purchaseReqType'],
            'purchaseRequisitionHashValue' => $hashValue,
            'entityID' => $entityID,
        );
        $poM = new PurchaseRequisition();
        $poM->exchangeArray($poData);
        $poID = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->savePReq($poM);
        $poProductM = new PurchaseRequisitionProduct();
        $poSubProduct = [];
        foreach ($poDataSet['pr'] as $product) {
            if (!empty($product)) {
                $cop = $validateRespond['data'][$product['locationPID']]['copied'];
                if ($validateRespond['data'][$product['locationPID']]['copiedQty'] != '' && $validateRespond['data'][$product['locationPID']]['copiedQty'] < $product['pQuantity']) {
                    $cop = false;
                }
                $poProductData = array(
                    'purchaseRequisitionID' => $poID,
                    'locationProductID' => $product['locationPID'],
                    'purchaseRequisitionProductQuantity' => $product['pQuantity'],
                    'purchaseRequisitionProductPrice' => $product['pUnitPrice'],
                    'purchaseRequisitionProductDiscount' => $product['pDiscount'],
                    'purchaseRequisitionProductTotal' => $product['pTotal'],
                    'copied' => $cop,
                    'purchaseRequisitionProductCopiedQuantity' => $validateRespond['data'][$product['locationPID']]['copiedQty']
                );
                
                $poProductM->exchangeArray($poProductData);
                $insertedPoProductID = $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')->savePurchaseRequisitionProduct($poProductM);
                $poSubProduct[$product['locationPID']] = $insertedPoProductID;
                if (array_key_exists('pTax', $product)) {
                    if (array_key_exists('tL', $product['pTax'])) {
                        foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                            $poPTaxesData = array(
                                'purchaseRequisitionID' => $poID,
                                'purchaseRequisitionProductID' => $insertedPoProductID,
                                'purchaseRequisitionTaxID' => $taxKey,
                                'purchaseRequisitionTaxPrecentage' => $productTax['tP'],
                                'purchaseRequisitionTaxAmount' => $productTax['tA']
                            );
                            $poPTaxM = new PurchaseRequisitionProductTax();
                            $poPTaxM->exchangeArray($poPTaxesData);
                            $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTaxTable')->savePurchaseRequisitionProductTax($poPTaxM);
                        }
                    }
                }
            }
        }

        // // after all, need to update grn and purchase invoice copied document references.
        // $updateDocumentReference =  $this->updateGrnAndPIDocumentReferences($poDataSet['poID'],$poID, $poSubProduct,$validateRespond['data']);
        // if (!$updateDocumentReference['status']) {
        //     $this->status = false;
        //     $this->rollback();
        //     $this->msg = $updateDocumentReference['msg'];
        //     return $this->JSONRespond();
        // }

        $approvers = $this->getApprovers($poDataSet['purchaseReqType'], $poDataSet['fT']);
        if (!empty($approvers)) {
            $approversFlag = true;

            $draftData = [
                'purchaseRequisitionDraftFlag' => 1,
                'status' => 7,
                'purchaseRequisitionID' => $poID,
            ];

            $draftRes = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->updateDraftDetails($draftData);
        
            if (!$draftRes) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PREQ_DRAFT');
                return $this->JSONRespond();
            }

        } else {
            $approversFlag = false;
        }

        $changeArray = $this->getPurchaseRequisitionChageDetails($poDataSet);

        //set log details
        $previousData = json_encode($changeArray['previousData']);
        $newData = json_encode($changeArray['newData']);

        $this->setLogDetailsArray($previousData,$newData);
        $this->setLogMessage("Purchase Requisition ".$changeArray['previousData']['Purchase Requisition Code']." is updated.");

        $this->commit();
        $this->status = true;
        $this->data = array('poID' => $poID, 'approversFlag' => $approversFlag, 'purchaseRequisitionCode' => $poDataSet['poC'], 'approver' => $approvers, 'hashValue' => $hashValue);
        $this->msg = $this->getMessage('SUCC_PO_UPDATE');
        return $this->JSONRespond();
    } 
    
    public function getPurchaseRequisitionChageDetails($data)
    {
        $row = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getPReqByPoID($data['poID'])->current();
        $previousData = [];
        $previousData['Purchase Requisition Code'] = $row['purchaseRequisitionCode'];
        $previousData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
        $previousData['Purchase Requisition Supplier Reference'] = $row['purchaseRequisitionSupplierReference'];
        $previousData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $previousData['Purchase Requisition Est Delivery Date'] = $this->convertDateToUserFormat($row['purchaseRequisitionEstDelDate']);
        $previousData['Purchase Requisition Date'] = $this->convertDateToUserFormat($row['purchaseRequisitionDate']);
        $previousData['Purchase Requisition Delivery Charge'] = $row['purchaseRequisitionDeliveryCharge'];
        $previousData['Purchase Requisition Type'] = $row['purchaseRequisitionTypeName'];
        $previousData['Purchase Requisition Total'] = $row['purchaseRequisitionTotal'];
        $previousData['Purchase Requisition Description'] = $row['purchaseRequisitionDescription'];

        $newData = [];
        $newData['Purchase Requisition Code'] = $data['poC'];
        $newData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
        $newData['Purchase Requisition Supplier Reference'] = $data['sR'];
        $newData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $newData['Purchase Requisition Est Delivery Date'] = $this->convertDateToUserFormat($data['eDD']);
        $newData['Purchase Requisition Date'] = $this->convertDateToUserFormat($data['poD']);
        $newData['Purchase Requisition Delivery Charge'] = $data['dC'];
        $newData['Purchase Requisition Type'] = $data['purchaseReqType'];
        $newData['Purchase Requisition Total'] = $data['fT'];
        $newData['Purchase Requisition Description'] = $data['cm'];

        return array('previousData' => $previousData, 'newData' => $newData);
    }


    /**
    * this function use to validate existing qty and edited qty when above po copy to another documents
    * @param int purchaseOrderID
    * @param array editedItemSet
    * @param array
    **/
    public function validateExistingItemQty($purchaseOrderID,$editedItemSet = [])
    {
        // get existing purchase order item details
        $existingPoDetails = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getPReqByPoID($purchaseOrderID);
        $existingItemSet = [];
        $errorProducts = [];
        $deletedProducts = [];
        $noQtyErrorFlag = false;
        $noItemErrorFlag = false;
        foreach ($existingPoDetails as $key => $value) {
            $existingItemSet[$value['locationProductID']] = [
                'qty' => $value['purchaseOrderProductQuantity'],
                'copiedQty' => $value['purchaseOrderProductCopiedQuantity'],
                'copied' => $value['copied'],
                'checked' => false,
                'proName' => $value['productName'],
                'purchaseOderProductID' => $value['purchaseOrderProductID']
            ];
        }
        // validate items
        foreach ($editedItemSet as $key => $newItems) {
            if ($existingItemSet[$newItems['locationPID']]['copiedQty'] != '' && $existingItemSet[$newItems['locationPID']]['copiedQty'] > $newItems['pQuantity']) {
                $errorProducts[] = $newItems['pName'];
                $noQtyErrorFlag = true;
            } else {
                $existingItemSet[$newItems['locationPID']]['checked'] = true;
            }
        }
        foreach ($existingItemSet as $key => $oldItems) {
            if (!$oldItems['checked'] && $oldItems['copiedQty'] != 0) {
                $deletedProducts[] = $oldItems['proName'];
                $noItemErrorFlag = true;
            } 
        }
        
        if ($noItemErrorFlag) {
            return ['status'=> false, 'msg' => $this->getMessage('ERR_NO_ITEMS_FOR_PREQ_EDIT', $deletedProducts)];
        } else if ($noQtyErrorFlag) {
            return ['status'=> false, 'msg' => $this->getMessage('ERR_NO_QTY_FOR_PREQ_EDIT', $errorProducts)];
        } else {
            return ['status'=> true, 'data' => $existingItemSet];
        }
        
    }

    /**
    * this function use to update grn and purchase invoice copied document references
    * @param int $existingID
    * @param int $newID
    * @param array $newPOProductIDs
    * @param array $existingPOIds
    * return array
    **/
    public function updateGrnAndPIDocumentReferences($existingID, $newID, $newPOProductIDs, $existingPOIds)
    {
        if ($existingID == null || $newID == null) {
            return ['ststus' => false, 'msg' => $this->getMessage('ERR_NO_IDS_PASS')];
        }
        
        // first check and update grn Product table
        $grnUpdateData = [
            'grnProductDocumentId' => $newID
        ]; 
        $grnCopyDocID = 9; // purchaseOrder Doc type is 9
        $updateGrn = $this->CommonTable('Inventory\Model\GrnProductTable')->updateCopiedID($grnUpdateData, $grnCopyDocID, $existingID);

        if (!$updateGrn) {
            return ['status' => false, 'msg' => $this->getMessage('ERR_GRN_PRO_UPDATE')];
        }
        // update purchase invoice product table references
        foreach ($existingPOIds as $key => $value) {
            if ($newPOProductIDs[$key] != '') {
                // update purchaseInvoice Product Table
                $piDetails = [
                    'purchaseInvoiceProductDocumentID' => $newPOProductIDs[$key]
                ];
                $updatePi = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->updateCopiedDocumentReference($piDetails, $grnCopyDocID, $value['purchaseOderProductID']);
                if (!$updatePi) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_PI_PRO_UPDATE')];
                }
            }
        }

        return ['status' => true];
    }

    public function searchPrForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $this->data = array('list' => $this->_searchPrForDropdown($searchKey));
            return $this->JSONRespond();
        }
    }

    private function _searchPrForDropdown($searchKey)
    {

        $prs = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->searchPReqForDropdown($searchKey, $this->getStatusID('open'), $locationID, TRUE);

        $prList = array();
        foreach ($prs as $po) {
            $temp['value'] = $po['purchaseRequisitionID'];
            $temp['text'] = $po['purchaseRequisitionCode'];
            $prList[] = $temp;
        }
        return $prList;
    }


    public function getPrDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $prID = $request->getPost('prID');
            $poData = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->getPReqByPoID($prID, true);
            $poDetails = array();
            $prID;
            while ($row = $poData->current()) {
                $tempPo = array();
                $prID = $row['purchaseRequisitionID'];
                $tempPo['prID'] = $row['purchaseRequisitionID'];
                $tempPo['prC'] = $row['purchaseRequisitionCode'];
                $tempPo['purchaseRequisitionApproved'] = $row['purchaseRequisitionApproved'];
                $tempPo['prSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempPo['prSID'] = $row['purchaseRequisitionSupplierID'];
                $tempPo['prSR'] = $row['purchaseRequisitionSupplierReference'];
                $tempPo['prRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempPo['prRLID'] = $row['purchaseRequisitionRetrieveLocation'];
                $tempPo['prD'] = $this->convertDateToUserFormat($row['purchaseRequisitionExpDelDate']);
                $tempPo['purchaseRequisitionDate'] = $this->convertDateToUserFormat($row['purchaseRequisitionDate']);
                $tempPo['prDC'] = $row['purchaseRequisitionDeliveryCharge'];
                $tempPo['prT'] = $row['purchaseRequisitionTotal'];
                $tempPo['prDes'] = $row['purchaseRequisitionDescription'];
                $tempPo['prSTax'] = $row['purchaseRequisitionShowTax'];
                $tempPo['productType'] = $row['productTypeID'];
                $tempPo['prSOB'] = $row['supplierOutstandingBalance'];
                $tempPo['prSCB'] = $row['supplierCreditBalance'];
                $poProducts = (isset($poDetails[$row['purchaseRequisitionID']]['prProducts'])) ? $poDetails[$row['purchaseRequisitionID']]['prProducts'] : array();
                if ($row['purchaseRequisitionProductID'] != NULL) {
                    $productTaxes = (isset($poProducts[$row['productCode']]['pT'])) ? $poProducts[$row['productCode']]['pT'] : array();
                    $poProducts[$row['productCode']] = array('prPC' => $row['productCode'], 'prPID' => $row['productID'], 'prPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'prPP' => $row['purchaseRequisitionProductPrice'], 'prPD' => $row['purchaseRequisitionProductDiscount'], 'prPQ' => $row['purchaseRequisitionProductQuantity'], 'prPT' => $row['purchaseRequisitionProductTotal'], 'prPUom' => $row['uomAbbr'], 'prPUomID' => $row['uomID'], 'pUDisplay' => $row['productUomDisplay'], 'productType' => $row['productTypeID'], 'copiedQty' => $row['purchaseRequisitionProductCopiedQuantity']);

                    if ($row['purchaseRequisitionTaxID'] != NULL) {
                        $productTaxes[$row['purchaseRequisitionTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['purchaseRequisitionTaxPrecentage'], 'pTA' => $row['purchaseRequisitionTaxAmount']);
                    }
                    $poProducts[$row['productCode']]['pT'] = $productTaxes;
                }
                $tempPo['prProducts'] = $poProducts;
                $poDetails[$row['purchaseRequisitionID']] = $tempPo;
            }
            $prSubTotal = number_format((floatval($poDetails[$prID]['prT']) - floatval($poDetails[$prID]['prDC'])), 2);
            $totalProTaxes = array();
            foreach ($poDetails[$prID]['prProducts'] as $product) {
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = number_format((floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA'])), 2);
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }
            $locationProducts = Array();
            $inactiveItems = Array();
            $locationID = $poDetails[$prID]['prRLID'];
            foreach ($poDetails[$prID]['prProducts'] as $poProducts) {
                $locationProducts[$poProducts['prPID']] = $this->getLocationProductDetails($locationID, $poProducts['prPID']);
                if ($locationProducts[$poProducts['prPID']] == null) {
                    $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($poProducts['prPID']);
                    $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                }
            }

            $inactiveItemFlag = false;
            $errorMsg = null;
            if (!empty($inactiveItems)) {
                $inactiveItemFlag = true;
                $errorItems = implode(",",$inactiveItems);        
                $errorMsg = $this->getMessage('ERR_PO_ITM_INACTIVE_COPY', [$errorItems]);
            }

            $poInfo = array(
                'prData' => $poDetails[$prID],
                'prSubTotal' => $prSubTotal,
                'pTotalTax' => $totalProTaxes,
                'locationProducts' => $locationProducts,
                'errorMsg'=>$errorMsg,
                'inactiveItemFlag'=>$inactiveItemFlag,
            );

            $this->data = $poInfo;
            $this->status = true;
            return $this->JSONRespond();
        }
    }
}
?>
