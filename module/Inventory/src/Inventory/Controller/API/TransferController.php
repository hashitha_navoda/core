<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Inventory Product related controller functions
 */

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Inventory\Model\Transfer;
use Inventory\Model\TransferProduct;
use Inventory\Model\ProductBatch;
use Inventory\Model\ItemIn;

class TransferController extends CoreController
{

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
    }

    public function searchAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $keyword = $req->getPost('keyword');

            if (!empty($keyword)) {
                $tbl = $this->CommonTable('Inventory\Model\TransferTable');
                $transfersList = $tbl->getTransfersForSearch($keyword);
                $paginated = false;
            } else {
                $transfersList = $this->getPaginatedTransfers();
                $paginated = true;
            }

            $htmlOutput = $this->_getListViewHtml($transfersList, $paginated);

            $this->status = true;
            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        }
    }

    private function _getListViewHtml($list = array(), $paginated = false)
    {
        $dateFormat = $this->getUserDateFormat();
        $globaldata = $this->getServiceLocator()->get('config');
        $statuses = $globaldata['statuses'];
        $view = new ViewModel(array(
            'statuses' => $statuses,
            'transfer' => $list,
            'paginated' => $paginated,
            'dateFormat' => $dateFormat
        ));

        $view->setTerminal(true);
        $view->setTemplate('inventory/transfer/list-table.phtml');

        return $view;
    }

    public function getPaginatedTransfers()
    {
        $this->paginator = $this->CommonTable('Inventory\Model\TransferTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);

        return $this->paginator;
    }

    public function saveTransferDetailsAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $products = $request->getPost('products');
            $transferCode = $request->getPost('transferCode');
            $locationOut = $request->getPost('locationOutID');
            $locationIn = $request->getPost('locationInID');
            $date = $this->convertDateToStandardFormat($request->getPost('date'));
            $description = $request->getPost('description');
            $locationRefID = $request->getPost('locationRefID');

            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            if($FIFOCostingFlag == 0 && $averageCostingFlag == 0){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE');
                return $this->JSONRespond();
            }

            $this->beginTransaction();
            // if product is not available in the 'transfer to' location, it cannot be transferred
            foreach ($products as $key => $p) {
                $productID = $p['productID'];
                $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationIn);
                $product = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);
                $location = (array) $this->CommonTable('Core\Model\LocationTable')->getLocationById($locationIn);
                if (!isset($locationProduct->locationProductID)) {
                    //if the confirmation message from the front end asking whether you want to add the product has not popped up yet
                    //if (!isset($request->getPost('forceLocationAdd')[$productID])) {
                    $this->status = false;
                    $this->data['NOT_INITIALIZED'] = $product['productID'];
                    //$this->msg = 'Product ' . $product['productName'] . ' (' . $product['productCode'] . ') is not enabled in ' . $location['locationName'] . ' (' . $location['locationCode'] . ').'
                    //<br><br>If you want to continue the transfer, enable the product by going in to the <a target="_blank" href="' . $this->url()->fromRoute('product', array('action' => 'edit', 'param1' => $product['productID'])) . '">product edit page</a> and ticking the desired location.';
                    $this->msg = $this->getMessage('ERR_TRAN_ENABLE', array($product['productName'], $product['productCode'], $location['locationName'], $location['locationCode'], $this->url()->fromRoute('product', array('action' => 'edit', 'param1' => $product['productID']))));
                    return $this->JSONRespond();
//                    } else {
//
//                    }
                }
            }
            // check if a transfer from the same transfer Code exists if exist add next number to transfer code
            while ($transferCode) {
                if ($this->CommonTable('Inventory\Model\TransferTable')->checkTransferByCode($transferCode)->current()) {
                    if ($locationRefID) {
                        $newTransferCode = $this->getReferenceNumber($locationRefID);
                        if ($newTransferCode == $transferCode) {
                            $this->updateReferenceNumber($locationRefID);
                            $transferCode = $this->getReferenceNumber($locationRefID);
                        } else {
                            $transferCode = $newTransferCode;
                        }
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_TRAN_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }

            $entityID = $this->createEntity();
            $transferData = array(
                'transferCode' => $transferCode,
                'locationIDIn' => $locationIn,
                'locationIDOut' => $locationOut,
                'transferDate' => $date,
                'transferDescription' => $description,
                'entityID' => $entityID
            );
            $transfer = new Transfer;
            $transfer->exchangeArray($transferData);

            $trasferID = $this->CommonTable('Inventory\Model\TransferTable')->saveTransfer($transfer);

            foreach ($products as $key => $p) {

                $batchProducts = $request->getPost('subProducts')[$p['productID']];
                $productID = $p['productID'];
                $trasferQty = $p['transferQuantity']['qty'];
                $producctType = $p['productType'];
                $locationProductOUT = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationOut);
                $locationProductIN = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationIn);
                $locationProductInID = $locationProductIN->locationProductID;

                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductInID);
                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

                if (count($batchProducts) > 0) {
                    foreach ($batchProducts as $batchKey => $batchValue) {
                        if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                            $this->saveTransferProductData($batchValue, $trasferID, $productID, $p['selectedUomId']);
                            $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                            if ($batchProduct->productBatchQuantity < $batchValue['qtyByBase']) {
                                $this->status = false;
                                $this->msg = $this->getMessage('REQ_TRAN_REFRESH');
                                return $this->JSONRespond();
                            }
                            $productBatchCode = $batchProduct->productBatchCode;
                            $productBatchQuentity = $batchProduct->productBatchQuantity - $batchValue['qtyByBase'];
                            $productBatchQty = array(
                                'productBatchQuantity' => $productBatchQuentity,
                            );

                            $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);

                            $checkBatchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProductFromLocationProductIdAndProductBatchCode($locationProductInID, $productBatchCode);
                            $newBatchID = 0;

                            // if batch is already available in the location
                            if ($checkBatchProduct) {

                                $newBatchID = $checkBatchProduct->productBatchID;
                                $productBatchQtyIN = array(
                                    'productBatchQuantity' => $checkBatchProduct->productBatchQuantity + $batchValue['qtyByBase'],
                                );
                                $productBatchID = $checkBatchProduct->productBatchID;
                                $checkBatchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($productBatchID, $productBatchQtyIN);

                                // if batch is new to the location
                            } else {
                                $productBatchQtyIN = array(
                                    'locationProductID' => $locationProductInID,
                                    'productBatchCode' => $productBatchCode,
                                    'productBatchExpiryDate' => $batchProduct->productBatchExpiryDate,
                                    'productBatchWarrantyPeriod' => $batchProduct->productBatchWarrantyPeriod,
                                    'productBatchManufactureDate' => $batchProduct->productBatchManufactureDate,
                                    'productBatchQuantity' => $batchValue['qtyByBase'],
                                );
                                $saveBatchProduct = new ProductBatch;
                                $saveBatchProduct->exchangeArray($productBatchQtyIN);
                                $newBatchID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($saveBatchProduct);
                            }
                            $itemInData = Array();
                            
                            if ($batchValue['serialID']) {
                                //insert data to item in table for serial products
                                $serialProduct = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductByBatchIDAndSerialID($batchValue['serialID'], $batchValue['batchID']);
                                
                                // check product has been sold already
                                if(!$serialProduct || $serialProduct->productSerialSold){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('REQ_TRAN_REFRESH');
                                    return $this->JSONRespond();
                                }
                                $serialProduct->locationProductID;
                                $serialProductData = array(
                                    'locationProductID' => $locationProductInID,
                                    'productBatchID' => $newBatchID,
                                );
                                $this->CommonTable('Inventory\Model\ProductSerialTable')->updateLocationProductID($batchValue['serialID'], $serialProductData);

                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProductOUT->locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                
                                $unitPrice = $itemInDetails['itemInPrice'];
                                $unitDiscount = $itemInDetails['itemInDiscount'];
                                if($averageCostingFlag == 1){
                                    $unitPrice = $locationProductOUT->locationProductAverageCostingPrice;
                                    $unitDiscount = 0;
                                }

                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);
                                //save new item in for location in for batch and serial products
                                $itemInData = array(
                                    'itemInDocumentType' => 'Inventory Transfer',
                                    'itemInDocumentID' => $trasferID,
                                    'itemInLocationProductID' => $locationProductIN->locationProductID,
                                    'itemInBatchID' => $newBatchID,
                                    'itemInSerialID' => $batchValue['serialID'],
                                    'itemInQty' => 1,
                                    'itemInPrice' => $unitPrice,
                                    'itemInDiscount' => $unitDiscount,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                );

                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                //////////////////
                            } else {
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProductOUT->locationProductID, $batchValue['batchID']);
                                
                                $unitPrice = $itemInDetails['itemInPrice'];
                                $unitDiscount = $itemInDetails['itemInDiscount'];
                                if($averageCostingFlag == 1){
                                    $unitPrice = $locationProductOUT->locationProductAverageCostingPrice;
                                    $unitDiscount = 0;
                                }

                                $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                /////Add new item in data and update///////
                                //save new item in for location in
                                $itemInData = array(
                                    'itemInDocumentType' => 'Inventory Transfer',
                                    'itemInDocumentID' => $trasferID,
                                    'itemInLocationProductID' => $locationProductIN->locationProductID,
                                    'itemInBatchID' => $newBatchID,
                                    'itemInSerialID' => NULL,
                                    'itemInQty' => $batchValue['qtyByBase'],
                                    'itemInPrice' => $unitPrice,
                                    'itemInDiscount' => $unitDiscount,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                //////////////////////////////////////
                            }
                        } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                            $this->saveTransferProductData($batchValue, $trasferID, $productID,$p['selectedUomId']);
                            $serialProduct = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductByProdctLocationIDAndSerialID($batchValue['serialID'], $locationProductOUT->locationProductID);
                            if (!$serialProduct || $serialProduct->productSerialSold == 1) {
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('REQ_TRAN_REFRESH');
                                return $this->JSONRespond();
                            }
                            $serialProduct->locationProductID;
                            $serialProductData = array(
                                'locationProductID' => $locationProductInID,
                            );
                            
                            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateLocationProductID($batchValue['serialID'], $serialProductData);

                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProductOUT->locationProductID, $batchValue['serialID']);
                            
                            $unitPrice = $itemInDetails['itemInPrice'];
                            $unitDiscount = $itemInDetails['itemInDiscount'];
                            if($averageCostingFlag == 1){
                                $unitPrice = $locationProductOUT->locationProductAverageCostingPrice;
                                $unitDiscount = 0;
                            }

                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);
                            //add serial data for item in table
                            $itemInData = array(
                                'itemInDocumentType' => 'Inventory Transfer',
                                'itemInDocumentID' => $trasferID,
                                'itemInLocationProductID' => $locationProductIN->locationProductID,
                                'itemInBatchID' => NULL,
                                'itemInSerialID' => $batchValue['serialID'],
                                'itemInQty' => 1,
                                'itemInPrice' => $unitPrice,
                                'itemInDiscount' => $unitDiscount,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                        }
                    }
                } else {
                    //update for non serial and non batch products for item in
                    $sellingQty = $p['transferQuantity']['qty'];
                   
                    while ($sellingQty != 0) {

                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($locationProductOUT->locationProductID);
                       
                        // If product have not saved to itemIn table
                        if(!$itemInDetails){
                            $this->rollback();
                            $this->msg = $this->getMessage('ERR_TRAN_SAVE');
                            $this->status = FALSE;
                            return $this->JSONRespond();
                        }

                        if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                            $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                            //for add new record we need to set updateQty
                            $updateQty = $sellingQty;
                            $sellingQty = 0;
                        } else {
                            $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                            $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                            //for add new record we need to set updateQty
                            $updateQty = $itemInDetails['itemInRemainingQty'];
                        }

                        $unitPrice = $itemInDetails['itemInPrice'];
                        $unitDiscount = $itemInDetails['itemInDiscount'];
                        if($averageCostingFlag == 1){
                            $unitPrice = $locationProductOUT->locationProductAverageCostingPrice;
                            $unitDiscount = 0;
                        }
                         //save itemIn Details
                        $itemInData = array(
                            'itemInDocumentType' => 'Inventory Transfer',
                            'itemInDocumentID' => $trasferID,
                            'itemInLocationProductID' => $locationProductIN->locationProductID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => NULL,
                            'itemInQty' => $updateQty,
                            'itemInPrice' =>  $unitPrice,
                            'itemInDiscount' => $unitDiscount,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                    }

                    $this->saveTransferProductData(array('batchID' => 0, 'serialID' => 0, 'qtyByBase' => $p['transferQuantity']['qtyByBase']), $trasferID, $productID, $p['selectedUomId']);
                }

                if ($producctType == 2) {
                    $trasferQty = 0;
                }
                if ($locationProductOUT->locationProductQuantity < $trasferQty) {
                    $this->status = false;
                    $this->msg = $this->getMessage('REQ_TRAN_REFRESH');
                    return $this->JSONRespond();
                }

                $locationProductINData = array(
                    'locationProductQuantity' => $locationProductIN->locationProductQuantity + $trasferQty,
                );
                $locationProductOUTData = array(
                    'locationProductQuantity' => $locationProductOUT->locationProductQuantity - $trasferQty,
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductINData, $productID, $locationIn);
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductOUTData, $productID, $locationOut);
            
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductInID);
                $locationPrTotalQty = 0;
                $locationPrTotalPrice = 0;
                $newItemInIds = array();
                if(count($itemInData) > 0){
                    foreach ($itemInData as $key => $value) {
                        if($value->itemInID > $locationProductLastItemInID){
                            $newItemInIds[] = $value->itemInID; 
                            $remainQty = $value->itemInQty - $value->itemInSoldQty;
                            if($remainQty > 0){
                                $itemInPrice = $remainQty*$value->itemInPrice;
                                $itemInDiscount = $remainQty*$value->itemInDiscount;
                                $locationPrTotalQty += $remainQty;
                                $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                            }
                            $locationProductLastItemInID = $value->itemInID; 
                        }
                    }
                }
                $locationPrTotalQty += $locationProductQuantity;
                $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;
                if($locationPrTotalQty > 0){
                    // $newAverageCostinPrice = $locationProductOUT->locationProductAverageCostingPrice;
                    $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;;
                    $lpdata = array(
                        'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                        'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationProductInID);
                
                    foreach ($newItemInIds as $inID) {
                        $intemInData = array(
                            'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                            );
                        $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                    }
                }

            }

            if ($locationRefID) {
                $this->updateReferenceNumber($locationRefID);
            }

            //  call product updated event
            $productIDs = array_map(function($element) {
                return $element['productID'];
            }, $products);

            $this->commit();

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationOut]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            $this->status = true;
            $this->data = array('transferID' => $trasferID);
            $this->msg = $this->getMessage('SUCC_TRAN_DETAIL_ADD', array($transferCode));
            $this->setLogMessage('Transfer '.$transferCode . ' is created');
            $this->flashMessenger()->addMessage($this->msg);
            return $this->JSONRespond();
        } else {
            $this->rollback();

            $this->status = false;
            $this->msg = $this->getMessage('ERR_TRAN_SAVE');
            return $this->JSONRespond();
        }
    }

    public function saveTransferProductData($batchValue, $trasferID, $productID, $selectedUomId)
    {
        $data = array(
            'transferID' => $trasferID,
            'productID' => $productID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'transferProductQuantity' => $batchValue['qtyByBase'],
            'selectedUomId' => $selectedUomId,
        );
        $transferProduct = new TransferProduct;
        $transferProduct->exchangeArray($data);
        $trasferProductID = $this->CommonTable('Inventory\Model\TransferProductTable')->saveTransferProduct($transferProduct);

        return $trasferProductID;
    }

    /*
     * @author Sandun <sandun@thinkcube.com>
     * @param int $locationProductID
     * calculation of unit price when item is transferring
     * return $unitPrice
     */

    private function _getUnitPrice($locationProductID)
    {
        $productDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductID);
        $totalItemQty = 0;
        $totalItemPrice = 0;
        foreach ($productDetails as $p) {
            $totalItemQty += $p->itemInQty;
            $totalItemPrice += ($p->itemInQty * $p->itemInPrice);
        }
        $unitPrice = $totalItemPrice / $totalItemQty;

        return $unitPrice;
    }

    /**
     * Search Transfer for drop down
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchTransferForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            if ($locationID) {
                $transfers = $this->CommonTable('Inventory\Model\TransferTable')->searchTransferForDocumentDropdown($searchKey, $locationID);
            } else {
                $transfers = $this->CommonTable('Inventory\Model\TransferTable')->searchTransferForDropdown($searchKey);
            }
            $transfersList = array();
            foreach ($transfers as $transfer) {
                $temp['value'] = $transfer['transferID'];
                $temp['text'] = $transfer['transferCode'];
                $transfersList[] = $temp;
            }

            $this->setLogMessage("Retrive transfer list for dropdown.");
            $this->data = array('list' => $transfersList);
            return $this->JSONRespond();
        }
    }

    /**
     * Get transfer view list by transferID
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getTransferByTransferIDAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $transferID = $req->getPost('transferID');

            if (!empty($transferID)) {
                $transfersList = $this->CommonTable('Inventory\Model\TransferTable')->getTransfersByTransferID($transferID);
                $paginated = false;
            } else {
                $transfersList = $this->getPaginatedTransfers();
                $paginated = true;
            }

            $htmlOutput = $this->_getListViewHtml($transfersList, $paginated);

            $this->status = true;
            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     *
     */
    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Inventory Transfer';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_INV_TRAN_EMAIL_SENT');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_INV_TRAN_EMAIL_SENT');
        return $this->JSONRespond();
    }

}
