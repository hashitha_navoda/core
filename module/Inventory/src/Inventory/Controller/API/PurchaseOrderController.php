<?php

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Model\PurchaseOrder;
use Inventory\Model\PurchaseOrderProduct;
use Inventory\Model\PurchaseOrderProductTax;
use Core\Model\DocumentReference;

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */
class PurchaseOrderController extends CoreController
{

    protected $paginator;

    public function getPoNoForLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $refData = $this->getReferenceNoForLocation(9, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];
            if ($rid == '' || $rid == NULL) {
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_PURORDER_REFLOC');
                } else {
                    $this->msg = $this->getMessage('ERR_PURORDER_REFMAX');
                }
                $this->data = FALSE;
                $this->status = false;
            } else {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $rid,
                    'locRefID' => $lrefID,
                );
            }

            $this->setLogMessage("Retrive purchase order code.");
            return $this->JSONRespond();
        }
    }

    public function savePoAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();

            $purchaseOrderCode = $post['poC'];
            $locationReferenceID = $post['lRID'];
            $documentReference = $post['documentReference'];
            $locationID = $post['rL'];
            $this->setLogMessage("Error occured when create purhcase order".$purchaseOrderCode.".");
// check if a purchase order from the same purchase order Code exists if exist add next number to purchase order code
            while ($purchaseOrderCode) {
                if ($this->CommonTable('Inventory\Model\PurchaseOrderTable')->checkPurchaseOrderByCode($purchaseOrderCode)->current()) {
                    if ($locationReferenceID) {
                        $newPurchaseOrderCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newPurchaseOrderCode == $purchaseOrderCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $purchaseOrderCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $purchaseOrderCode = $newPurchaseOrderCode;
                        }
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PURORDER_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }
            $prProductsUncopiedArr = [];
            if ($post['selectedPR'] != '') {
                $copiedFromPr = TRUE;
                //get purchase order product details
                $prProducts = $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')->getUnCopiedPrProducts($post['selectedPR']);
                foreach ($prProducts as $product) {
                    $restQty = $product['purchaseRequisitionProductQuantity'] - $product['purchaseRequisitionProductCopiedQuantity'];
                    $restQty = ($restQty < 0) ? 0 : $restQty;
                    $prProductsUncopiedArr[$product['locationProductID']] = $restQty;
                }
            } else {
                $copiedFromPr = FALSE;
            }

            $this->beginTransaction();

            if ($post['isActivePOWF'] == 1) {
                $limitData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->getLimitsByWFId($post['WFID']);
                $limitID = null;

                foreach ($limitData as $key => $value) {
                    if ($post['fT'] <= floatval($value['approvalWorkflowLimitMax']) && $post['fT'] >= floatval($value['approvalWorkflowLimitMin'])) {
                        $limitID = $value['approvalWorkflowLimitsID'];
                        break;
                    }
                }
                if ($limitID != null) {
                    $limitApproversData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->getLimitApproversByWFLimitId($limitID);
                    $approvers = [];

                    foreach ($limitApproversData as $key => $value) {
                        $approvers[] = $value['workflowLimitApprover'];
                    }


                    $savePODraft = $this->savePurchaseOrderDraft($post);

                    if ($savePODraft) {
                        $this->commit();
                        $this->status = true;
                        $this->data = array('poID' => $savePODraft['poID'], 'approvers' => $approvers, 'hashValue' => $savePODraft['hashValue'], 'poCode' => $savePODraft['poCode']);
                        $this->msg = $this->getMessage('SUCC_PURORDER_GO_THROUGH_WF');
                        return $this->JSONRespond();
                    }
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('SUCC_PURORDER_APP_WF_MISMATCH');
                    return $this->JSONRespond();
                }
            }
            $entityID = $this->createEntity();
            $poData = array(
                'purchaseOrderCode' => $purchaseOrderCode,
                'purchaseOrderSupplierID' => $post['sID'],
                'purchaseOrderSupplierReference' => $post['sR'],
                'purchaseOrderRetrieveLocation' => $locationID,
                'purchaseOrderExpDelDate' => $this->convertDateToStandardFormat($post['eDD']),
                'purchaseOrderDate' => $this->convertDateToStandardFormat($post['poD']),
                'purchaseOrderDescription' => $post['cm'],
                'purchaseOrderDeliveryCharge' => $post['dC'],
                'purchaseOrderTotal' => $post['fT'],
                'purchaseOrderShowTax' => $post['sT'],
                'pucrhaseRequisitionID' => ($post['selectedPR'] == '') ? null : $post['selectedPR'],
                'entityID' => $entityID,
            );
            $poM = new PurchaseOrder();
            $poM->exchangeArray($poData);
            $poID = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->savePo($poM);
            $this->updateReferenceNumber($locationReferenceID);
            $poProductM = new PurchaseOrderProduct();
            foreach ($post['pr'] as $product) {
                if (!empty($product)) {
                    //if copied from PO,update those products as copied.
                    if ($copiedFromPr == TRUE) {
                        $this->updatePrProductCopiedQty($product['locationPID'], $post['selectedPR'], $product['pQuantity']);
                    }
                    $poProductData = array(
                        'purchaseOrderID' => $poID,
                        'locationProductID' => $product['locationPID'],
                        'purchaseOrderProductQuantity' => $product['pQuantity'],
                        'purchaseOrderProductPrice' => $product['pUnitPrice'],
                        'purchaseOrderProductDiscount' => $product['pDiscount'],
                        'purchaseOrderProductTotal' => $product['pTotal'],
                        'purchaseOrderProductSelectedUomId' => $product['pUom'],
                    );

                    if ($copiedFromPr && array_key_exists($product['locationPID'], $prProductsUncopiedArr) && $prProductsUncopiedArr[$product['locationPID']] > 0) {
                        $copiedQty = ($product['pQuantity'] < $prProductsUncopiedArr[$product['locationPID']]) ? $product['pQuantity'] : $prProductsUncopiedArr[$product['locationPID']];
                        $poProductData['purchaseOrderProductDocumentId'] = $post['selectedPR'];
                        $poProductData['purchaseOrderProductDocumentTypeId'] = 38;
                        $poProductData['purchaseOrderProductDoumentTypeCopiedQuantity'] = $copiedQty;
                    }

                    $poProductM->exchangeArray($poProductData);
                    $insertedPoProductID = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->savePurchaseOrderProduct($poProductM);
                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $poPTaxesData = array(
                                    'purchaseOrderID' => $poID,
                                    'purchaseOrderProductID' => $insertedPoProductID,
                                    'purchaseOrderTaxID' => $taxKey,
                                    'purchaseOrderTaxPrecentage' => $productTax['tP'],
                                    'purchaseOrderTaxAmount' => $productTax['tA']
                                );
                                $poPTaxM = new PurchaseOrderProductTax();
                                $poPTaxM->exchangeArray($poPTaxesData);
                                $this->CommonTable('Inventory\Model\PurchaseOrderProductTaxTable')->savePurchaseOrderProductTax($poPTaxM);
                            }
                        }
                    }
                }
            }

            //update PO status if copied from it
            if ($copiedFromPr == TRUE) {
                $this->checkAndUpdatePrStatus($post['selectedPR']);
            }

            if (isset($documentReference)) {
                foreach ($documentReference as $key => $val) {
                    foreach ($val as $key1 => $value) {
                        $data = array(
                            'sourceDocumentTypeId' => 9,
                            'sourceDocumentId' => $poID,
                            'referenceDocumentTypeId' => $key,
                            'referenceDocumentId' => $value,
                        );
                        $documentReference = new DocumentReference();
                        $documentReference->exchangeArray($data);
                        $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                    }
                }
            }

            //      call product updated event
            $productIDs = array_map(function($element) {
                return $element['pID'];
            }, $post['pr']);

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            $this->commit();
            $this->status = true;
            $this->data = array('poID' => $poID, 'approvers' => []);

            $this->msg = $this->getMessage('SUCC_PURORDER_PO', array($purchaseOrderCode));
//            $this->msg = $this->getMessage('SUCC_INVOICE_ADD_INVO_PRODETAILS', array($purchaseOrderCode));
            $this->setLogMessage($this->companyCurrencySymbol . $post['fT'] . ' amount PO successfully created - ' . $purchaseOrderCode.".");
//            $this->flashMessenger()->addMessage($this->msg);
            return $this->JSONRespond();
        }
    }

    public function savePurchaseOrderDraft($dataSet)
    {

        $prProductsUncopiedArr = [];
        if ($dataSet['selectedPR'] != '') {
            $copiedFromPr = TRUE;
            //get purchase order product details
            $prProducts = $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')->getUnCopiedPrProducts($post['selectedPR']);
            foreach ($prProducts as $product) {
                $restQty = $product['purchaseRequisitionProductQuantity'] - $product['purchaseRequisitionProductCopiedQuantity'];
                $restQty = ($restQty < 0) ? 0 : $restQty;
                $prProductsUncopiedArr[$product['locationProductID']] = $restQty;
            }
        } else {
            $copiedFromPr = FALSE;
        }

        $locationID = $dataSet['rL'];
        $refData = $this->getReferenceNoForLocation(32, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];
        $entityID = $this->createEntity();
        $hashValue = hash('md5', $rid);

        $date = date("Y-m-d");
        $poData = array(
            'purchaseOrderCode' => $rid,
            'purchaseOrderSupplierID' => $dataSet['sID'],
            'purchaseOrderSupplierReference' => $dataSet['sR'],
            'purchaseOrderRetrieveLocation' => $dataSet['rL'],
            'purchaseOrderExpDelDate' => $this->convertDateToStandardFormat($dataSet['eDD']),
            'purchaseOrderDate' => $this->convertDateToStandardFormat($dataSet['poD']),
            'purchaseOrderDescription' => $dataSet['cm'],
            'purchaseOrderDeliveryCharge' => $dataSet['dC'],
            'purchaseOrderTotal' => $dataSet['fT'],
            'status' => 7,
            'purchaseOrderDraftFlag' => 1,
            'purchaseOrderHashValue' => $hashValue,
            'purchaseOrderShowTax' => $dataSet['sT'],
            'pucrhaseRequisitionID' => ($dataSet['selectedPR'] == '') ? null : $dataSet['selectedPR'],
            'entityID' => $entityID,
        );

        
        $poM = new PurchaseOrder();
        $poM->exchangeArray($poData);

        $poID = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->savePo($poM);
        // $this->updateReferenceNumber($locationReferenceID);
        
        $poProductM = new PurchaseOrderProduct();
        foreach ($dataSet['pr'] as $product) {
            if (!empty($product)) {
                //if copied from PO,update those products as copied.
                if ($copiedFromPr == TRUE) {
                    $this->updatePrProductCopiedQty($product['locationPID'], $dataSet['selectedPR'], $product['pQuantity']);
                }

                $poProductData = array(
                    'purchaseOrderID' => $poID,
                    'locationProductID' => $product['locationPID'],
                    'purchaseOrderProductQuantity' => $product['pQuantity'],
                    'purchaseOrderProductPrice' => $product['pUnitPrice'],
                    'purchaseOrderProductDiscount' => $product['pDiscount'],
                    'purchaseOrderProductTotal' => $product['pTotal'],
                    'purchaseOrderProductSelectedUomId' => $product['pUom'],
                );

                if ($copiedFromPr && array_key_exists($product['locationPID'], $prProductsUncopiedArr) && $prProductsUncopiedArr[$product['locationPID']] > 0) {
                    $copiedQty = ($product['pQuantity'] < $prProductsUncopiedArr[$product['locationPID']]) ? $product['pQuantity'] : $prProductsUncopiedArr[$product['locationPID']];
                    $poProductData['purchaseOrderProductDocumentId'] = $dataSet['selectedPR'];
                    $poProductData['purchaseOrderProductDocumentTypeId'] = 38;
                    $poProductData['purchaseOrderProductDoumentTypeCopiedQuantity'] = $copiedQty;
                }

                $poProductM->exchangeArray($poProductData);
                $insertedPoProductID = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->savePurchaseOrderProduct($poProductM);
                if (array_key_exists('pTax', $product)) {
                    if (array_key_exists('tL', $product['pTax'])) {
                        foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                            $poPTaxesData = array(
                                'purchaseOrderID' => $poID,
                                'purchaseOrderProductID' => $insertedPoProductID,
                                'purchaseOrderTaxID' => $taxKey,
                                'purchaseOrderTaxPrecentage' => $productTax['tP'],
                                'purchaseOrderTaxAmount' => $productTax['tA']
                            );
                            $poPTaxM = new PurchaseOrderProductTax();
                            $poPTaxM->exchangeArray($poPTaxesData);
                            $this->CommonTable('Inventory\Model\PurchaseOrderProductTaxTable')->savePurchaseOrderProductTax($poPTaxM);
                        }
                    }
                }
            }
        }

        $this->updateReferenceNumber($lrefID);
        $data = [
            'hashValue' => $hashValue,
            'poID' => $poID,
            'poCode' => $rid
        ];
        return $data;
    }


    public function sendApproverEmailAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $poId = $request->getPost('poId');
            $poCode = $request->getPost('poCode');
            $approvers = $request->getPost('approvers');
            $token = $request->getPost('token');

            if ($poId && $poCode && $approvers && $token) {
                $this->sendPurchaseOrderApproversEmail($poId, $poCode, $approvers, $token);
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PO_SEND_FOR_WF');
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PO_SEND_FOR_WF');
            }
        }
        return $this->JSONRespond();
    }

    private function sendPurchaseOrderApproversEmail($poId, $poCode, $approvers, $token)
    {
        foreach ($approvers as $approver) {

            $approverData =  $this->CommonTable('Settings\Model\ApproverTable')->getApproverByID($approver)->current();


            $approveUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/api/po/approve/?pOId=' . $poId . '&action=approve&token=' . $token;
            $rejectUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/api/po/approve/?pOId=' . $poId . '&action=reject&token=' . $token;
            $approvePoEmailView = new ViewModel(array(
                'name' => $approverData['firstName'] . ' ' . $approverData['lastName'],
                'poCode' => $poCode,
                'approveLink' => $approveUrl,
                'rejectLink' => $rejectUrl,
            ));
            $approvePoEmailView->setTemplate('/core/email/po-approve');
            $renderedEmailHtml = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($approvePoEmailView);
            $documentType = 'Purchase Order';
            $this->sendDocumentEmail($approverData['email'], "Request for Approve - Purchase Order - $poCode", $renderedEmailHtml, $documentType, $poId);
        }
    }

    public function approveAction()
    {
        $action = $_GET['action'];
        $pOId = $_GET['pOId'];
        $token = $_GET['token'];
        $pODetails = $this->CommonTable("Inventory\Model\PurchaseOrderTable")->getPurchaseOrderByID($pOId);
        $poCode = $pODetails->purchaseOrderCode;
        $this->beginTransaction();
        if ($pODetails->purchaseOrderHashValue == $token) {
            if ($action == 'approve') {
                if ($pODetails->purchaseOrderApproved == 0 && $pODetails->status == 13) {
                    $this->commit();
                    echo "Purchase Order $poCode is already rejected, you cannot approve any more..!";
                    exit();
                }

                if ($pODetails->purchaseOrderApproved == 0) {

                    $data = $this->getPoDetailsForPODraftApprove($pODetails->purchaseOrderID);
                    $convertDraftToPO = $this->updateDraftToPoForPOApprove($data);

                    if ($convertDraftToPO) {
                         $this->commit();
                        echo "Purchase Order approved..!";
                        exit();
                    } else {
                        $this->rollback();
                        echo "Error Occured while approve purchase order..!";
                        exit();

                    }
                    
                } else {
                    $this->commit();
                    echo "Purchase Order $poCode is already approved..!";
                    exit();
                }
            } else {
                if ($pODetails->purchaseOrderApproved == 1) {
                    $this->commit();
                    echo "Purchase Order $poCode is already approved, you cannot reject any more..!";
                    exit();
                }

                if ($pODetails->purchaseOrderApproved == 0 && $pODetails->status == 13) {
                    $this->commit();
                    echo "Purchase Order $poCode is already rejected..!";
                    exit();
                } else {
                    $draftData = [
                        'purchaseOrderDraftFlag' => 0,
                        'purchaseOrderApproved' => 0,
                        'status' => 13,
                        'purchaseOrderID' => $pODetails->purchaseOrderID,
                    ];

                    $draftRes = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->updateDraftPo($draftData);

                    if(!$draftRes){
                        $this->rollback();
                        echo $this->getMessage('ERR_PREQ_DRAFT_TO_PREQ');
                        exit();
                    }else{
                        $this->commit();
                        echo "Purchase Order $poCode rejected..!";
                        exit();
                    }
                }
            }
        } else {
            $this->rollback();
            echo 'Token Mismatch';
            exit();
        }
    }

    public function updatePrProductCopiedQty($locationPID, $prId, $productQty)
    {
        $oldPrProductData = $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')->getPrProduct($prId, $locationPID);
        $recordedQty = $oldPrProductData['purchaseRequisitionProductQuantity'];
        $currentUpdatedQty = $oldPrProductData['purchaseRequisitionProductCopiedQuantity'];
        $updatingQty = floatval($currentUpdatedQty) + floatval($productQty);
        $allCopied = FALSE;
        if (floatval($updatingQty) >= floatval($recordedQty)) {
            $allCopied = TRUE;
        }

        $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')->updateCopiedPrProductQty($locationPID, $prId, $updatingQty);
        if ($allCopied) {
            $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')->updateCopiedPrProducts($locationPID, $prId);
        }
    }

    public function checkAndUpdatePrStatus($prID)
    {
        $poProducts = $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')->getUnCopiedPrProducts($prID);
        if (empty($poProducts)) {
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')->updatePReqStatus($prID, $closeStatusID);
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function returns po details
     * @param POST purchaseOrderID
     */
    public function getPoDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $poID = $request->getPost('poID');
            $toGrn = $request->getPost('toGrn');
            if ($toGrn) {
                $toGrn = TRUE;
            } else {
                $toGrn = FALSE;
            }
            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPoByPoID($poID, $toGrn);
            $poDetails = array();
            $poID;
            while ($row = $poData->current()) {
                $tempPo = array();
                $poID = $row['purchaseOrderID'];
                $tempPo['poID'] = $row['purchaseOrderID'];
                $tempPo['poC'] = $row['purchaseOrderCode'];
                $tempPo['poSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempPo['poSID'] = $row['purchaseOrderSupplierID'];
                $tempPo['poSR'] = $row['purchaseOrderSupplierReference'];
                $tempPo['poRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempPo['poRLID'] = $row['purchaseOrderRetrieveLocation'];
                $tempPo['poD'] = $this->convertDateToUserFormat($row['purchaseOrderExpDelDate']);
                $tempPo['purchaseOrderDate'] = $this->convertDateToUserFormat($row['purchaseOrderDate']);
                $tempPo['poDC'] = $row['purchaseOrderDeliveryCharge'];
                $tempPo['poT'] = $row['purchaseOrderTotal'];
                $tempPo['poDes'] = $row['purchaseOrderDescription'];
                $tempPo['poSTax'] = $row['purchaseOrderShowTax'];
                $tempPo['productType'] = $row['productTypeID'];
                $tempPo['poSOB'] = $row['supplierOutstandingBalance'];
                $tempPo['poSCB'] = $row['supplierCreditBalance'];
                $tempPo['poPRID'] = $row['pucrhaseRequisitionID'];
                $tempPo['poHash'] = $row['purchaseOrderHashValue'];
                $tempPo['poApproved'] = $row['purchaseOrderApproved'];
                $poProducts = (isset($poDetails[$row['purchaseOrderID']]['poProducts'])) ? $poDetails[$row['purchaseOrderID']]['poProducts'] : array();
                if ($row['purchaseOrderProductID'] != NULL) {
                    $productTaxes = (isset($poProducts[$row['productCode']]['pT'])) ? $poProducts[$row['productCode']]['pT'] : array();
                    $poProducts[$row['productCode']] = array('poPC' => $row['productCode'], 'poPID' => $row['productID'], 'poPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'poPP' => $row['purchaseOrderProductPrice'], 'poPD' => $row['purchaseOrderProductDiscount'], 'poPQ' => $row['purchaseOrderProductQuantity'], 'poPT' => $row['purchaseOrderProductTotal'], 'poPUom' => $row['uomAbbr'], 'poPUomID' => $row['purchaseOrderProductSelectedUomId'], 'pUDisplay' => $row['productUomDisplay'], 'productType' => $row['productTypeID'], 'copiedQty' => $row['purchaseOrderProductCopiedQuantity']);

                    if ($row['purchaseOrderTaxID'] != NULL) {
                        $productTaxes[$row['purchaseOrderTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['purchaseOrderTaxPrecentage'], 'pTA' => $row['purchaseOrderTaxAmount']);
                    }
                    $poProducts[$row['productCode']]['pT'] = $productTaxes;
                }
                $tempPo['poProducts'] = $poProducts;
                $poDetails[$row['purchaseOrderID']] = $tempPo;
            }
            $poSubTotal = number_format((floatval($poDetails[$poID]['poT']) - floatval($poDetails[$poID]['poDC'])), 2);
            $totalProTaxes = array();
            foreach ($poDetails[$poID]['poProducts'] as $product) {
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = number_format((floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA'])), 2);
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }
            $locationProducts = Array();
            $inactiveItems = Array();
            $locationID = $poDetails[$poID]['poRLID'];
            foreach ($poDetails[$poID]['poProducts'] as $poProducts) {
                $locationProducts[$poProducts['poPID']] = $this->getLocationProductDetails($locationID, $poProducts['poPID']);
                if ($locationProducts[$poProducts['poPID']] == null) {
                    $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($poProducts['poPID']);
                    $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                }
            }

            $inactiveItemFlag = false;
            $errorMsg = null;
            if (!empty($inactiveItems)) {
                $inactiveItemFlag = true;
                $errorItems = implode(",",$inactiveItems);        
                $errorMsg = $this->getMessage('ERR_PO_ITM_INACTIVE_COPY', [$errorItems]);
            }

            $uploadedAttachments = [];
            $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($poID, 9);
            $companyName = $this->getSubdomain();
            $path = '/userfiles/' . md5($companyName) . "/attachments";


            foreach ($attachmentData as $value) {
                $temp = [];
                $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
                $temp['documentRealName'] = $value['documentRealName'];
                $temp['docLink'] = $path . "/" .$value['documentName'];
                $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
            }


            $sourceDocumentTypeId=9;
            $sourceDocumentId=$poID;
            $docrefData=$this->getDocumentReferenceBySourceData($sourceDocumentTypeId,$sourceDocumentId);
            $poInfo = array(
                'poData' => $poDetails[$poID],
                'poSubTotal' => $poSubTotal,
                'pTotalTax' => $totalProTaxes,
                'locationProducts' => $locationProducts,
                'docRefData'=>$docrefData,
                'errorMsg'=>$errorMsg,
                'inactiveItemFlag'=>$inactiveItemFlag,
                'uploadedAttachments'=>$uploadedAttachments,
            );

            $this->data = $poInfo;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getPosForSearchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $statuses = $globaldata['statuses'];
            $dateFormat = $this->getUserDateFormat();

            if ($request->getPost('searchKey') != '') {
                $poList = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPosforSearch($request->getPost('searchKey'));
                $poListView = new ViewModel(array('poList' => $poList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
            } else {
                $userActiveLocation = $this->user_session->userActiveLocation;
                $this->paginator = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrders(true, $userActiveLocation['locationID']);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $poListView = new ViewModel(array('poList' => $this->paginator, 'statuses' => $statuses));
            }

            $poListView->setTemplate('inventory/purchase-order/purchase-order-list');
            $this->html = $poListView;
            return $this->JSONRespondHtml();
        }
    }

    public function changePoStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $poID = $request->getPost('poID');
            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrderByID($poID);
            $previouseStatusID = $poData->status;
            $poCode = $poData->purchaseOrderCode;

            $statusList = $this->getStatusesList();
            $previousStatusCode = $statusList[$previouseStatusID];

            $newStatusCode = $request->getPost('statusCode');
            $newStatusID = $this->getStatusID($newStatusCode);
            
            $result = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->updatePoStatus($poID, $newStatusID);
            if ($result) {
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_PURORDER_STATUS');
                $this->flashMessenger()->addMessage($this->msg);
                $this->setLogMessage($poCode." Purchase order status change ".$previousStatusCode.' to '.$newStatusCode.'.');
            } else {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PURORDER_STATUS');
                $this->setLogMessage("Error occured when change ".$poCode." purchase order status ".$previousStatusCode.' to '.$newStatusCode.'.');
            }
            return $this->JSONRespond();
        }
    }

    public function sendPoEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Purchase Order';

        if ($request->isPost()) {
            $documentID = $request->getPost('documentID');
            $email = $request->getPost('to_email');
            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrderByID($documentID);
            $this->mailDocumentAsTemplate($request, $documentType, true);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_GRN_EMAIL', array($documentType));

            $this->setLogMessage($poData->purchaseOrderCode." purchase order document email to ".$email.'.');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_GRN_EMAIL', array($documentType));

        $this->setLogMessage("Error occured when email the purchase order document.");
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * search open po by po code for dropdown
     * @return viewModel
     */
    public function searchPoForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $this->data = array('list' => $this->_searchPoForDropdown($searchKey));
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search All Purchase Orders for Dropdown
     * @return view model
     */
    public function searchAllPoForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');

            $this->setLogMessage("Retrive purchase order list for dropdown.");
            $this->data = array('list' => $this->_searchPoForDropdown($searchKey, true, $locationID));
            return $this->JSONRespond();
        }
    }

    /**
     * @author Hashan <hashan@thinkcube.com>
     * Search All Open Purchase Orders for Dropdown
     * @return view model
     */
    public function searchAllOpenPoForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');

            $this->setLogMessage("Retrive purchase order list for dropdown.");
            $this->data = array('list' => $this->_searchPoForDropdown($searchKey, false, $locationID));
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search PO For Dropdown
     * @param String $searchKey
     * @param boolean $isAllPOs
     */
    private function _searchPoForDropdown($searchKey, $isAllPOs = false, $locationID = false)
    {
        if (!$locationID) {
            $locationID = $this->user_session->userActiveLocation["locationID"];
        }
        if ($isAllPOs) {
            $pos = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->searchPoForDropdown($searchKey, FALSE, $locationID);
        } else {
            $pos = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->searchPoForDropdown($searchKey, $this->getStatusID('open'), $locationID, TRUE);
        }

        $poList = array();
        foreach ($pos as $po) {
            $temp['value'] = $po['purchaseOrderID'];
            $temp['text'] = $po['purchaseOrderCode'];
            $poList[] = $temp;
        }
        return $poList;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Get Purchase order list for view page by Purchase order ID or SupplierID
     * @return ViewModel
     */
    public function getPOListForSearchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $statuses = $globaldata['statuses'];
            $dateFormat = $this->getUserDateFormat();

            if ($request->getPost('POID') == '' && $request->getPost('supplierID') == '') {
                $userActiveLocation = $this->user_session->userActiveLocation;
                $this->paginator = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrders(true, $userActiveLocation['locationID']);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $poListView = new ViewModel(array('poList' => $this->paginator, 'statuses' => $statuses));
            } else {
                $poList = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPOListForSearch($request->getPost('POID'), $request->getPost('supplierID'));
                $poListView = new ViewModel(array('poList' => $poList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
            }

            $poListView->setTemplate('inventory/purchase-order/purchase-order-list');
            $this->html = $poListView;

            $this->setLogMessage("Purchase order list accessed.");
            return $this->JSONRespondHtml();
        }
    }

    public function getDraftPoListByMrpAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        
        $draftID = $request->getPost('id');
        $poDraftDataSet = [];
        $poDraftDetails = $this->CommonTable('Inventory\Model\PoDraftTable')
            ->getDetailsByID($draftID)->current();
        $valueSet = array_filter(json_decode($poDraftDetails['poDraftDetails'], true));
        $locationID = $this->user_session->userActiveLocation['locationID'];
        
        foreach ($valueSet as $key => $value) {
            $locationProduct = $this->getLocationProductDetails($locationID, $value['proID']);
            // $proDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductsByproductId($value['proID'], $locationID);
            $poDraftDataSet[] = array(
                'locProDetails' => $locationProduct,
                'reqQty' => $value['reqQty'],
                );
        }
     
        $this->status = true;
        $this->data = $poDraftDataSet;
        return $this->JSONRespond();
              
    }

    public function deletePurchaseOrderDraftAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        
        $draftID = $request->getPost('poDraftID');
        // statusID = 5 (canceled)
        $statusID = 5;
        $updateStatus = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->updatePoStatus($draftID, $statusID);
        if(!$updateStatus){
            $this->status = false;
            return $this->JSONRespond();
        }

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_PO_DRAFT_DELETE');
        return $this->JSONRespond();
    }

    public function updateDraftToPoAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $post = $request->getPost();
        $purchaseOrderCode = $post['poC'];
        $locationReferenceID = $post['lRID'];
        $documentReference = $post['documentReference'];
        $locationID = $post['rL'];
        $purchaseOrderID = $post['poID'];
        $goThroughWF = false;

        $getDraftPOData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrderByID($purchaseOrderID);
        $poDftCode = $getDraftPOData->purchaseOrderCode;

        // check if a purchase order from the same purchase order Code exists if exist add next number to purchase order code
        while ($purchaseOrderCode) {
            if ($this->CommonTable('Inventory\Model\PurchaseOrderTable')->checkPurchaseOrderByCode($purchaseOrderCode)->current()) {
                if ($locationReferenceID) {
                    $newPurchaseOrderCode = $this->getReferenceNumber($locationReferenceID);
                    if ($newPurchaseOrderCode == $purchaseOrderCode) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $purchaseOrderCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $purchaseOrderCode = $newPurchaseOrderCode;
                    }
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PURORDER_CODE_EXIST');
                    return $this->JSONRespond();
                }
            } else {
                break;
            }
        }
        $this->beginTransaction();

        if ($post['isActivePOWF'] == 1) {
            $limitData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->getLimitsByWFId($post['WFID']);
            $limitID = null;

            foreach ($limitData as $key => $value) {
                if ($post['fT'] <= floatval($value['approvalWorkflowLimitMax']) && $post['fT'] >= floatval($value['approvalWorkflowLimitMin'])) {
                    $limitID = $value['approvalWorkflowLimitsID'];
                    break;
                }
            }
            if ($limitID != null) {
                $limitApproversData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->getLimitApproversByWFLimitId($limitID);
                $approvers = [];

                foreach ($limitApproversData as $key => $value) {
                    $approvers[] = $value['workflowLimitApprover'];
                }
                $goThroughWF = true;

                // $savePODraft = $this->savePurchaseOrderDraft($post);
                $hashValue = hash('md5', $rid);

                // create po update data set array
                $poUpdateData = array(
                    'purchaseOrderID' => $purchaseOrderID,
                    'purchaseOrderSupplierID' => $post['sID'],
                    'purchaseOrderSupplierReference' => $post['sR'],
                    'purchaseOrderRetrieveLocation' => $locationID,
                    'purchaseOrderExpDelDate' => $this->convertDateToStandardFormat($post['eDD']),
                    'purchaseOrderDate' => $this->convertDateToStandardFormat($post['purchaseOrderDate']),
                    'purchaseOrderDescription' => $post['cm'],
                    'purchaseOrderDeliveryCharge' => $post['dC'],
                    'purchaseOrderTotal' => $post['fT'],
                    'purchaseOrderShowTax' => $post['sT'],
                    'purchaseOrderDraftFlag' => 1,
                    'purchaseOrderHashValue' => $hashValue,
                );

            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('SUCC_PURORDER_APP_WF_MISMATCH');
                return $this->JSONRespond();
            }
        } else {
            $poUpdateData = array(
                'purchaseOrderID' => $purchaseOrderID,
                'purchaseOrderCode' => $purchaseOrderCode,
                'purchaseOrderSupplierID' => $post['sID'],
                'purchaseOrderSupplierReference' => $post['sR'],
                'purchaseOrderRetrieveLocation' => $locationID,
                'purchaseOrderExpDelDate' => $this->convertDateToStandardFormat($post['eDD']),
                'purchaseOrderDate' => $this->convertDateToStandardFormat($post['purchaseOrderDate']),
                'purchaseOrderDescription' => $post['cm'],
                'purchaseOrderDeliveryCharge' => $post['dC'],
                'purchaseOrderTotal' => $post['fT'],
                'purchaseOrderShowTax' => $post['sT'],
                'purchaseOrderDraftFlag' => 0, // because this 1 mean draft
                'status' => 3 // open state id is 3
            );
        }

        $status = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->updateDraftToPo($poUpdateData);
        
        if(!$status){
            $this->rollback();
            $this->status = false;
            return $this->JSONRespond();
        } 
        
        if (!$goThroughWF) {
            //update purchase order reference number.
            $this->updateReferenceNumber($locationReferenceID);
        }   
        
        //before update purchase-order-product table should delete existing records that related to the above (exist) purchase order draft.
        $poSubProductTaxDeteteStatus = $this->CommonTable('Inventory\Model\PurchaseOrderProductTaxTable')->deleteDraftRecordTaxesByPoID($purchaseOrderID);
        
      
        $poSubProductDeleteStatus = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')
                                ->deleteDraftRecordsByPoID($purchaseOrderID);
        
        if(!$poSubProductDeleteStatus)
        {
            $this->rollback();
            $this->status = false;
            return $this->JSONRespond();
        }

        $poProductM = new PurchaseOrderProduct();
        foreach ($post['pr'] as $product) {
            $poProductData = array(
                'purchaseOrderID' => $purchaseOrderID,
                'locationProductID' => $product['locationPID'],
                'purchaseOrderProductQuantity' => $product['pQuantity'],
                'purchaseOrderProductPrice' => $product['pUnitPrice'],
                'purchaseOrderProductDiscount' => $product['pDiscount'],
                'purchaseOrderProductTotal' => $product['pTotal'],
            );
            $poProductM->exchangeArray($poProductData);
            $insertedPoProductID = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->savePurchaseOrderProduct($poProductM);
            
            if(!$insertedPoProductID){
                $this->rollback();
                $this->status = false;
                return $this->JSONRespond();
            }
            if (array_key_exists('pTax', $product)) {
                if (array_key_exists('tL', $product['pTax'])) {
                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                        $poPTaxesData = array(
                            'purchaseOrderID' => $purchaseOrderID,
                            'purchaseOrderProductID' => $insertedPoProductID,
                            'purchaseOrderTaxID' => $taxKey,
                            'purchaseOrderTaxPrecentage' => $productTax['tP'],
                            'purchaseOrderTaxAmount' => $productTax['tA']
                        );
                        $poPTaxM = new PurchaseOrderProductTax();
                        $poPTaxM->exchangeArray($poPTaxesData);
                        $this->CommonTable('Inventory\Model\PurchaseOrderProductTaxTable')->savePurchaseOrderProductTax($poPTaxM);

                    }
                }
            }
        }

        if (isset($documentReference)) {
            foreach ($documentReference as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $data = array(
                        'sourceDocumentTypeId' => 9,
                        'sourceDocumentId' => $purchaseOrderID,
                        'referenceDocumentTypeId' => $key,
                        'referenceDocumentId' => $value,
                    );
                    $documentReference = new DocumentReference();
                    $documentReference->exchangeArray($data);
                    $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                }
            }
        }

        if (!$goThroughWF) {
            $this->commit();
            $this->status = true;
            $this->data = array('poID' => $purchaseOrderID, 'approvers' => []);
            $this->msg = $this->getMessage('SUCC_UPDATE_PODRAFT');
            $this->flashMessenger()->addMessage($this->msg);
            return $this->JSONRespond(); 
        } else {

            $this->commit();
            $this->status = true;
            $this->data = array('poID' => $purchaseOrderID, 'approvers' => $approvers, 'hashValue' => $hashValue, 'poCode' => $poDftCode);
            $this->msg = $this->getMessage('SUCC_PURORDER_GO_THROUGH_WF');
            return $this->JSONRespond();       
        }
    }

    public function getAllRelatedDocumentDetailsByPoIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $poID = $request->getPost('poID');
            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPODetailsByPoId($poID);
            $grnData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnDetailsByPoId($poID);
            $piDataByDirectPoId = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataByPoId($poID);
            $piDataByGrn = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataCopiedFromGrnByPoId($poID);
            $purchaseReturnData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnDataByPoId($poID);
            $debitNoteDataByPo = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDirectPoRelatedDebitNoteDataByPoId($poID);
            $debitNoteDataByCopiedPo = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPoRelatedDebitNoteDataByPoId($poID);
            $piPaymentByPo = $this->CommonTable('SupplierPaymentsTable')->getPiPaymentDetailsByPoId($poID);
            $piPaymentDataByCopiedPo = $this->CommonTable('SupplierPaymentsTable')->getPiPaymentDetailsByCopiedPoId($poID);
            $debitNotePaymentDataByPo = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentDetailsByPoId($poID);
            $debitNotePaymentDataByCopiedPo = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentDetailsByCopiedPoId($poID);

            $dataExistsFlag = false;
            if ($poData) {
                $poData = array(
                    'type' => 'PurchaseOrder',
                    'documentID' => $poData['purchaseOrderID'],
                    'code' => $poData['purchaseOrderCode'],
                    'amount' => number_format($poData['purchaseOrderTotal'], 2),
                    'issuedDate' => $poData['purchaseOrderExpDelDate'],
                    'created' => $poData['createdTimeStamp'],
                );
                $poDetails[] = $poData;
                $dataExistsFlag = true;
            }
            if (isset($grnData)) {
                foreach ($grnData as $grDta) {
                    $grnDta = array(
                        'type' => 'Grn',
                        'documentID' => $grDta['grnID'],
                        'code' => $grDta['grnCode'],
                        'amount' => number_format($grDta['grnTotal'], 2),
                        'issuedDate' => $grDta['grnDate'],
                        'created' => $grDta['createdTimeStamp'],
                    );
                    $poDetails[] = $grnDta;
                    if (isset($grDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($piDataByDirectPoId)) {
                foreach ($piDataByDirectPoId as $piDirectDta) {
                    $piDeta = array(
                        'type' => 'PurchaseInvoice',
                        'documentID' => $piDirectDta['purchaseInvoiceID'],
                        'code' => $piDirectDta['purchaseInvoiceCode'],
                        'amount' => number_format($piDirectDta['purchaseInvoiceTotal'], 2),
                        'issuedDate' => $piDirectDta['purchaseInvoiceIssueDate'],
                        'created' => $piDirectDta['createdTimeStamp'],
                    );
                    $poDetails[] = $piDeta;
                    if (isset($piDirectDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($piDataByGrn)) {
                foreach ($piDataByGrn as $piData) {
                    $piDta = array(
                        'type' => 'PurchaseInvoice',
                        'documentID' => $piData['purchaseInvoiceID'],
                        'code' => $piData['purchaseInvoiceCode'],
                        'amount' => number_format($piData['purchaseInvoiceTotal'], 2),
                        'issuedDate' => $piData['purchaseInvoiceIssueDate'],
                        'created' => $piData['createdTimeStamp'],
                    );
                    $poDetails[] = $piDta;
                    if (isset($piData)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($purchaseReturnData)) {
                foreach ($purchaseReturnData as $prDta) {
                    $pReturnData = array(
                        'type' => 'PurchaseReturn',
                        'documentID' => $prDta['purchaseReturnID'],
                        'code' => $prDta['purchaseReturnCode'],
                        'amount' => number_format($prDta['purchaseReturnTotal'], 2),
                        'issuedDate' => $prDta['purchaseReturnDate'],
                        'created' => $prDta['createdTimeStamp'],
                    );
                    $poDetails[] = $pReturnData;
                    if (isset($prDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($debitNoteDataByPo)) {
                foreach ($debitNoteDataByPo as $debitNoteDta) {
                    $dNData = array(
                        'type' => 'DebitNote',
                        'documentID' => $debitNoteDta['debitNoteID'],
                        'code' => $debitNoteDta['debitNoteCode'],
                        'amount' => number_format($debitNoteDta['debitNoteTotal'], 2),
                        'issuedDate' => $debitNoteDta['debitNoteDate'],
                        'created' => $debitNoteDta['createdTimeStamp'],
                    );
                    $poDetails[] = $dNData;
                    if (isset($debitNoteDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($debitNoteDataByCopiedPo)) {
                foreach ($debitNoteDataByCopiedPo as $debitNoteDta) {
                    $dNData = array(
                        'type' => 'DebitNote',
                        'documentID' => $debitNoteDta['debitNoteID'],
                        'code' => $debitNoteDta['debitNoteCode'],
                        'amount' => number_format($debitNoteDta['debitNoteTotal'], 2),
                        'issuedDate' => $debitNoteDta['debitNoteDate'],
                        'created' => $debitNoteDta['createdTimeStamp'],
                    );
                    $poDetails[] = $dNData;
                    if (isset($debitNoteDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($piPaymentByPo)) {
                foreach ($piPaymentByPo as $piPayDta) {
                    $piPaymentData = array(
                        'type' => 'SupplierPayment',
                        'documentID' => $piPayDta['outgoingPaymentID'],
                        'code' => $piPayDta['outgoingPaymentCode'],
                        'amount' => number_format($piPayDta['outgoingPaymentAmount'], 2),
                        'issuedDate' => $piPayDta['outgoingPaymentDate'],
                        'created' => $piPayDta['createdTimeStamp'],
                    );
                    $poDetails[] = $piPaymentData;
                    if (isset($piPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($piPaymentDataByCopiedPo)) {
                foreach ($piPaymentDataByCopiedPo as $piPayDta) {
                    $piPaymentData = array(
                        'type' => 'SupplierPayment',
                        'documentID' => $piPayDta['outgoingPaymentID'],
                        'code' => $piPayDta['outgoingPaymentCode'],
                        'amount' => number_format($piPayDta['outgoingPaymentAmount'], 2),
                        'issuedDate' => $piPayDta['outgoingPaymentDate'],
                        'created' => $piPayDta['createdTimeStamp'],
                    );
                    $poDetails[] = $piPaymentData;
                    if (isset($piPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($debitNotePaymentDataByPo)) {
                foreach ($debitNotePaymentDataByPo as $dNPayDta) {
                    $dNPaymentData = array(
                        'type' => 'DebitNotePayment',
                        'documentID' => $dNPayDta['debitNotePaymentID'],
                        'code' => $dNPayDta['debitNotePaymentCode'],
                        'amount' => number_format($dNPayDta['debitNotePaymentAmount'], 2),
                        'issuedDate' => $dNPayDta['debitNotePaymentDate'],
                        'created' => $dNPayDta['createdTimeStamp'],
                    );
                    $poDetails[] = $dNPaymentData;
                    if (isset($dNPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($debitNotePaymentDataByCopiedPo)) {
                foreach ($debitNotePaymentDataByCopiedPo as $dNPayDta) {
                    $dNPaymentData = array(
                        'type' => 'DebitNotePayment',
                        'documentID' => $dNPayDta['debitNotePaymentID'],
                        'code' => $dNPayDta['debitNotePaymentCode'],
                        'amount' => number_format($dNPayDta['debitNotePaymentAmount'], 2),
                        'issuedDate' => $dNPayDta['debitNotePaymentDate'],
                        'created' => $dNPayDta['createdTimeStamp'],
                    );
                    $poDetails[] = $dNPaymentData;
                    if (isset($dNPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($poDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $poDetails);

            $documentDetails = array(
                'poDetails' => $poDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }


    /**
    * this function use to update purchase orders
    * @param json request
    * return json respond
    **/
    public function updatePoAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = '';
            return $this->JSONRespond();
        }
        $poDataSet = $request->getPost();
        // validate product qty
        $validateRespond = $this->validateExistingItemQty($poDataSet['poID'],$poDataSet['realProductArray']);
        if(!$validateRespond['status']) {
            $this->status = false;
            $this->msg = $validateRespond['msg'];
            return $this->JSONRespond();
        }


        $this->beginTransaction();


        $poPurchaseRequisitionDataSet = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getAllPoProductsByPoID($poDataSet['poID']);

        $prID = null;
        foreach ($poPurchaseRequisitionDataSet as $key => $value) {
            if($value['purchaseOrderProductDocumentTypeId'] == 38 && $value['purchaseOrderProductDocumentId']){
                $prID = $value['purchaseOrderProductDocumentId'];
                //get pr product details
                $prProductDetails = $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')
                    ->getPrProduct($value['purchaseOrderProductDocumentId'], $value['locationProductID']);

                //update purchaseOrder product table
                $currentCopedQty = floatval($prProductDetails['purchaseRequisitionProductCopiedQuantity']) - floatval($value['purchaseOrderProductQuantity']);
                $updatedPoDataSet = array(
                    'purchaseRequisitionProductID' => $prProductDetails['purchaseRequisitionProductID'],
                    'copied' => 0,
                    'purchaseRequisitionProductCopiedQuantity' => $currentCopedQty,
                    );
                $updatePoProduct = $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')
                    ->updatePoCopiedStateAndCopiedQty($updatedPoDataSet);
                if(!$updatePoProduct){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_UPDATE_PO_PRODUCT');
                    return $this->JSONRespond();
                }
            }
        }
        
        if($prID){
            //update purchaseOrder table
            $statID = 3; // open
            $updatePo = $this->CommonTable('Inventory\Model\PurchaseRequisitionTable')
                ->updatePReqStatus($prID, $statID);
            $poDataSet['selectedPR'] = $prID;
        }

        $prProductsUncopiedArr = [];
        if ($poDataSet['selectedPR'] != '') {
            $copiedFromPr = TRUE;
            //get purchase order product details
            $prProducts = $this->CommonTable('Inventory\Model\PurchaseRequisitionProductTable')->getUnCopiedPrProducts($post['selectedPR']);
            foreach ($prProducts as $product) {
                $restQty = $product['purchaseRequisitionProductQuantity'] - $product['purchaseRequisitionProductCopiedQuantity'];
                $restQty = ($restQty < 0) ? 0 : $restQty;
                $prProductsUncopiedArr[$product['locationProductID']] = $restQty;
            }
        } else {
            $copiedFromPr = FALSE;
        }

        // update existing po status
        $statusID = 10; // replace
        $updateExistingPo = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->updatePoStatus($poDataSet['poID'], $statusID);
        
        // create new entity
        $entityID = $this->createEntity();
        $poData = array(
            'purchaseOrderCode' => $poDataSet['poC'],
            'purchaseOrderSupplierID' => $poDataSet['sID'],
            'purchaseOrderSupplierReference' => $poDataSet['sR'],
            'purchaseOrderRetrieveLocation' => $poDataSet['rL'],
            'purchaseOrderExpDelDate' => $this->convertDateToStandardFormat($poDataSet['eDD']),
            'purchaseOrderDate' => $this->convertDateToStandardFormat($poDataSet['poD']),
            'purchaseOrderDescription' => $poDataSet['cm'],
            'purchaseOrderDeliveryCharge' => $poDataSet['dC'],
            'purchaseOrderTotal' => $poDataSet['fT'],
            'purchaseOrderShowTax' => $poDataSet['sT'],
            'pucrhaseRequisitionID' => ($poDataSet['selectedPR'] == '') ? null : $poDataSet['selectedPR'],
            'entityID' => $entityID,
        );
        $poM = new PurchaseOrder();
        $poM->exchangeArray($poData);
        $poID = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->savePo($poM);
        $poProductM = new PurchaseOrderProduct();
        $poSubProduct = [];
        foreach ($poDataSet['pr'] as $product) {
            if (!empty($product)) {
                $cop = $validateRespond['data'][$product['locationPID']]['copied'];
                if ($validateRespond['data'][$product['locationPID']]['copiedQty'] != '' && $validateRespond['data'][$product['locationPID']]['copiedQty'] < $product['pQuantity']) {
                    $cop = false;
                }

                if ($copiedFromPr == TRUE) {
                    $this->updatePrProductCopiedQty($product['locationPID'], $poDataSet['selectedPR'], $product['pQuantity']);
                }

                $poProductData = array(
                    'purchaseOrderID' => $poID,
                    'locationProductID' => $product['locationPID'],
                    'purchaseOrderProductQuantity' => $product['pQuantity'],
                    'purchaseOrderProductPrice' => $product['pUnitPrice'],
                    'purchaseOrderProductDiscount' => $product['pDiscount'],
                    'purchaseOrderProductTotal' => $product['pTotal'],
                    'copied' => $cop,
                    'purchaseOrderProductCopiedQuantity' => $validateRespond['data'][$product['locationPID']]['copiedQty'],
                    'purchaseOrderProductSelectedUomId' => $product['pUom'],
                );

                if ($copiedFromPr && array_key_exists($product['locationPID'], $prProductsUncopiedArr) && $prProductsUncopiedArr[$product['locationPID']] > 0) {
                    $copiedQty = ($product['pQuantity'] < $prProductsUncopiedArr[$product['locationPID']]) ? $product['pQuantity'] : $prProductsUncopiedArr[$product['locationPID']];
                    $poProductData['purchaseOrderProductDocumentId'] = $poDataSet['selectedPR'];
                    $poProductData['purchaseOrderProductDocumentTypeId'] = 38;
                    $poProductData['purchaseOrderProductDoumentTypeCopiedQuantity'] = $copiedQty;
                }
                
                $poProductM->exchangeArray($poProductData);
                $insertedPoProductID = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->savePurchaseOrderProduct($poProductM);
                $poSubProduct[$product['locationPID']] = $insertedPoProductID;
                if (array_key_exists('pTax', $product)) {
                    if (array_key_exists('tL', $product['pTax'])) {
                        foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                            $poPTaxesData = array(
                                'purchaseOrderID' => $poID,
                                'purchaseOrderProductID' => $insertedPoProductID,
                                'purchaseOrderTaxID' => $taxKey,
                                'purchaseOrderTaxPrecentage' => $productTax['tP'],
                                'purchaseOrderTaxAmount' => $productTax['tA']
                            );
                            $poPTaxM = new PurchaseOrderProductTax();
                            $poPTaxM->exchangeArray($poPTaxesData);
                            $this->CommonTable('Inventory\Model\PurchaseOrderProductTaxTable')->savePurchaseOrderProductTax($poPTaxM);
                        }
                    }
                }
            }
        }

        //update PO status if copied from it
        if ($copiedFromPr == TRUE) {
            $this->checkAndUpdatePrStatus($poDataSet['selectedPR']);
        }

        $documentReference = $poDataSet['documentReference'];
        if (isset($documentReference)) {
            foreach ($documentReference as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $data = array(
                        'sourceDocumentTypeId' => 9,
                        'sourceDocumentId' => $poID,
                        'referenceDocumentTypeId' => $key,
                        'referenceDocumentId' => $value,
                    );
                    $documentReference = new DocumentReference();
                    $documentReference->exchangeArray($data);
                    $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                }
            }
        }
        // after all, need to update grn and purchase invoice copied document references.
        $updateDocumentReference =  $this->updateGrnAndPIDocumentReferences($poDataSet['poID'],$poID, $poSubProduct,$validateRespond['data']);
        if (!$updateDocumentReference['status']) {
            $this->status = false;
            $this->rollback();
            $this->msg = $updateDocumentReference['msg'];
            return $this->JSONRespond();
        }

        $changeArray = $this->getPurchaseOrderChageDetails($poDataSet);

        //set log details
        $previousData = json_encode($changeArray['previousData']);
        $newData = json_encode($changeArray['newData']);

        $this->setLogDetailsArray($previousData,$newData);
        $this->setLogMessage("Purchase Order ".$changeArray['previousData']['Purchase Order Code']." is updated.");

        $this->commit();
        $this->status = true;
        $this->data = $poID;
        $this->msg = $this->getMessage('SUCC_PO_UPDATE');
        return $this->JSONRespond();
    } 
    
    public function getPurchaseOrderChageDetails($data)
    {
        $row = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPoByPoID($data['poID'])->current();
        $previousData = [];
        $previousData['Purchase Order Code'] = $row['purchaseOrderCode'];
        $previousData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
        $previousData['Purchase Order Supplier Reference'] = $row['purchaseOrderSupplierReference'];
        $previousData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $previousData['Purchase Order Est Delivery Date'] = $this->convertDateToUserFormat($row['purchaseOrderExpDelDate']);
        $previousData['Purchase Order Date'] = $this->convertDateToUserFormat($row['purchaseOrderDate']);
        $previousData['Purchase Order Delivery Charge'] = $row['purchaseOrderDeliveryCharge'];
        $previousData['Purchase Order Total'] = number_format($row['purchaseOrderTotal'],2);
        $previousData['Purchase Order Description'] = $row['purchaseOrderDescription'];

        $newData = [];
        $newData['Purchase Order Code'] = $data['poC'];
        $newData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
        $newData['Purchase Order Supplier Reference'] = $data['sR'];
        $newData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $newData['Purchase Order Est Delivery Date'] = $this->convertDateToUserFormat($data['eDD']);
        $newData['Purchase Order Date'] = $this->convertDateToUserFormat($data['poD']);
        $newData['Purchase Order Delivery Charge'] = $data['dC'];
        $newData['Purchase Order Total'] = number_format($data['fT'],2);
        $newData['Purchase Order Description'] = $data['cm'];

        return array('previousData' => $previousData, 'newData' => $newData);
    }


    /**
    * this function use to validate existing qty and edited qty when above po copy to another documents
    * @param int purchaseOrderID
    * @param array editedItemSet
    * @param array
    **/
    public function validateExistingItemQty($purchaseOrderID,$editedItemSet = [])
    {
        // get existing purchase order item details
        $existingPoDetails = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPoByPoID($purchaseOrderID);
        $existingItemSet = [];
        $errorProducts = [];
        $deletedProducts = [];
        $noQtyErrorFlag = false;
        $noItemErrorFlag = false;
        foreach ($existingPoDetails as $key => $value) {
            $existingItemSet[$value['locationProductID']] = [
                'qty' => $value['purchaseOrderProductQuantity'],
                'copiedQty' => $value['purchaseOrderProductCopiedQuantity'],
                'copied' => $value['copied'],
                'checked' => false,
                'proName' => $value['productName'],
                'purchaseOderProductID' => $value['purchaseOrderProductID']
            ];
        }
        // validate items
        foreach ($editedItemSet as $key => $newItems) {
            if ($existingItemSet[$newItems['locationPID']]['copiedQty'] != '' && $existingItemSet[$newItems['locationPID']]['copiedQty'] > $newItems['pQuantity']) {
                $errorProducts[] = $newItems['pName'];
                $noQtyErrorFlag = true;
            } else {
                $existingItemSet[$newItems['locationPID']]['checked'] = true;
            }
        }
        foreach ($existingItemSet as $key => $oldItems) {
            if (!$oldItems['checked'] && $oldItems['copiedQty'] != 0) {
                $deletedProducts[] = $oldItems['proName'];
                $noItemErrorFlag = true;
            } 
        }
        
        if ($noItemErrorFlag) {
            return ['status'=> false, 'msg' => $this->getMessage('ERR_NO_ITEMS_FOR_PO_EDIT', $deletedProducts)];
        } else if ($noQtyErrorFlag) {
            return ['status'=> false, 'msg' => $this->getMessage('ERR_NO_QTY_FOR_PO_EDIT', $errorProducts)];
        } else {
            return ['status'=> true, 'data' => $existingItemSet];
        }
        
    }

    /**
    * this function use to update grn and purchase invoice copied document references
    * @param int $existingID
    * @param int $newID
    * @param array $newPOProductIDs
    * @param array $existingPOIds
    * return array
    **/
    public function updateGrnAndPIDocumentReferences($existingID, $newID, $newPOProductIDs, $existingPOIds)
    {
        if ($existingID == null || $newID == null) {
            return ['ststus' => false, 'msg' => $this->getMessage('ERR_NO_IDS_PASS')];
        }
        
        // first check and update grn Product table
        $grnUpdateData = [
            'grnProductDocumentId' => $newID
        ]; 
        $grnCopyDocID = 9; // purchaseOrder Doc type is 9
        $updateGrn = $this->CommonTable('Inventory\Model\GrnProductTable')->updateCopiedID($grnUpdateData, $grnCopyDocID, $existingID);

        if (!$updateGrn) {
            return ['status' => false, 'msg' => $this->getMessage('ERR_GRN_PRO_UPDATE')];
        }
        // update purchase invoice product table references
        foreach ($existingPOIds as $key => $value) {
            if ($newPOProductIDs[$key] != '') {
                // update purchaseInvoice Product Table
                $piDetails = [
                    'purchaseInvoiceProductDocumentID' => $newPOProductIDs[$key]
                ];
                $updatePi = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->updateCopiedDocumentReference($piDetails, $grnCopyDocID, $value['purchaseOderProductID']);
                if (!$updatePi) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_PI_PRO_UPDATE')];
                }
            }
        }

        return ['status' => true];
    }


    public function getPoDetailsForPODraftApprove ($poID) {
        
            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPoByPoID($poID, false);
            $poDetails = array();
            $poID;
            while ($row = $poData->current()) {
                $tempPo = array();
                $poID = $row['purchaseOrderID'];
                $tempPo['poID'] = $row['purchaseOrderID'];
                $tempPo['poC'] = $row['purchaseOrderCode'];
                $tempPo['poSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempPo['poSID'] = $row['purchaseOrderSupplierID'];
                $tempPo['poSR'] = $row['purchaseOrderSupplierReference'];
                $tempPo['poRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempPo['poRLID'] = $row['purchaseOrderRetrieveLocation'];
                $tempPo['poD'] = $this->convertDateToUserFormat($row['purchaseOrderExpDelDate']);
                $tempPo['purchaseOrderDate'] = $this->convertDateToUserFormat($row['purchaseOrderDate']);
                $tempPo['poDC'] = $row['purchaseOrderDeliveryCharge'];
                $tempPo['poT'] = $row['purchaseOrderTotal'];
                $tempPo['poDes'] = $row['purchaseOrderDescription'];
                $tempPo['poSTax'] = $row['purchaseOrderShowTax'];
                $tempPo['productType'] = $row['productTypeID'];
                $tempPo['poSOB'] = $row['supplierOutstandingBalance'];
                $tempPo['poSCB'] = $row['supplierCreditBalance'];
                $tempPo['poPRID'] = $row['pucrhaseRequisitionID'];
                $poProducts = (isset($poDetails[$row['purchaseOrderID']]['poProducts'])) ? $poDetails[$row['purchaseOrderID']]['poProducts'] : array();
                if ($row['purchaseOrderProductID'] != NULL) {
                    $productTaxes = (isset($poProducts[$row['productCode']]['pT'])) ? $poProducts[$row['productCode']]['pT'] : array();
                    $poProducts[$row['productCode']] = array('poPC' => $row['productCode'], 'poPID' => $row['productID'], 'poPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'poPP' => $row['purchaseOrderProductPrice'], 'poPD' => $row['purchaseOrderProductDiscount'], 'poPQ' => $row['purchaseOrderProductQuantity'], 'poPT' => $row['purchaseOrderProductTotal'], 'poPUom' => $row['uomAbbr'], 'poPUomID' => $row['uomID'], 'pUDisplay' => $row['productUomDisplay'], 'productType' => $row['productTypeID'], 'copiedQty' => $row['purchaseOrderProductCopiedQuantity'], 'selectedUom' => $row['purchaseOrderProductSelectedUomId']);

                    if ($row['purchaseOrderTaxID'] != NULL) {
                        $productTaxes[$row['purchaseOrderTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['purchaseOrderTaxPrecentage'], 'pTA' => $row['purchaseOrderTaxAmount']);
                    }
                    $poProducts[$row['productCode']]['pT'] = $productTaxes;
                }
                $tempPo['poProducts'] = $poProducts;
                $poDetails[$row['purchaseOrderID']] = $tempPo;
            }
            $poSubTotal = number_format((floatval($poDetails[$poID]['poT']) - floatval($poDetails[$poID]['poDC'])), 2);
            $totalProTaxes = array();
            foreach ($poDetails[$poID]['poProducts'] as $product) {
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = number_format((floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA'])), 2);
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }
            $locationProducts = Array();
            $inactiveItems = Array();
            $locationID = $poDetails[$poID]['poRLID'];
            foreach ($poDetails[$poID]['poProducts'] as $poProducts) {
                $locationProducts[$poProducts['poPID']] = $this->getLocationProductDetails($locationID, $poProducts['poPID']);
                if ($locationProducts[$poProducts['poPID']] == null) {
                    $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($poProducts['poPID']);
                    $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                }
            }

            $inactiveItemFlag = false;
            $errorMsg = null;
            if (!empty($inactiveItems)) {
                $inactiveItemFlag = true;
                $errorItems = implode(",",$inactiveItems);        
                $errorMsg = $this->getMessage('ERR_PO_ITM_INACTIVE_COPY', [$errorItems]);
            }

            $uploadedAttachments = [];
            $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($poID, 9);
            $companyName = $this->getSubdomain();
            $path = '/userfiles/' . md5($companyName) . "/attachments";


            foreach ($attachmentData as $value) {
                $temp = [];
                $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
                $temp['documentRealName'] = $value['documentRealName'];
                $temp['docLink'] = $path . "/" .$value['documentName'];
                $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
            }


            $sourceDocumentTypeId=9;
            $sourceDocumentId=$poID;
            $docrefData=$this->getDocumentReferenceBySourceData($sourceDocumentTypeId,$sourceDocumentId);
            $poInfo = array(
                'poData' => $poDetails[$poID],
                'poSubTotal' => $poSubTotal,
                'pTotalTax' => $totalProTaxes,
                'locationProducts' => $locationProducts,
                'docRefData'=>$docrefData,
                'errorMsg'=>$errorMsg,
                'inactiveItemFlag'=>$inactiveItemFlag,
                'uploadedAttachments'=>$uploadedAttachments,
            );

            return $poInfo;
    }

    public function updateDraftToPoForPOApprove($post)
    {
        $refData = $this->getReferenceNoForLocation(9, $post['poData']['poRLID']);
        $purchaseOrderCode = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        $documentReference = $post['docRefData'];
        $purchaseOrderID = $post['poData']['poID'];

        // check if a purchase order from the same purchase order Code exists if exist add next number to purchase order code
        while ($purchaseOrderCode) {
            if ($this->CommonTable('Inventory\Model\PurchaseOrderTable')->checkPurchaseOrderByCode($purchaseOrderCode)->current()) {
                if ($locationReferenceID) {
                    $newPurchaseOrderCode = $this->getReferenceNumber($locationReferenceID);
                    if ($newPurchaseOrderCode == $purchaseOrderCode) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $purchaseOrderCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $purchaseOrderCode = $newPurchaseOrderCode;
                    }
                } else {
                    // $this->status = false;
                    // $this->msg = $this->getMessage('ERR_PURORDER_CODE_EXIST');
                    return false;
                }
            } else {
                break;
            }
        }
        // create po update data set array
        $poUpdateData = array(
            'purchaseOrderID' => $purchaseOrderID,
            'purchaseOrderCode' => $purchaseOrderCode,
            'purchaseOrderSupplierID' => $post['poData']['poSID'],
            'purchaseOrderSupplierReference' => $post['poData']['poSR'],
            'purchaseOrderRetrieveLocation' => $post['poData']['poRLID'],
            'purchaseOrderExpDelDate' => $this->convertDateToStandardFormat($post['poData']['poD']),
            'purchaseOrderDate' => $this->convertDateToStandardFormat($post['poData']['poD']),
            'purchaseOrderDescription' => $post['poData']['poDes'],
            'purchaseOrderDeliveryCharge' => $post['poData']['poDC'],
            'purchaseOrderTotal' => $post['poData']['poT'],
            'purchaseOrderShowTax' => $post['poData']['poSTax'],
            'purchaseOrderDraftFlag' => 0, // because this 1 mean draft
            'purchaseOrderApproved' => 1, // because this 1 mean draft
            'status' => 3 // open state id is 3
        );
       
        $status = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->updateDraftToPo($poUpdateData);
        
        if(!$status){
            // $this->rollback();
            // $this->status = false;
            return false;
        } 
        
        //update purchase order reference number.
        $this->updateReferenceNumber($locationReferenceID);
        
        //before update purchase-order-product table should delete existing records that related to the above (exist) purchase order draft.
       
        $poSubProductTaxDeteteStatus = $this->CommonTable('Inventory\Model\PurchaseOrderProductTaxTable')->deleteDraftRecordTaxesByPoID($purchaseOrderID);
        
      
        $poSubProductDeleteStatus = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')
                                ->deleteDraftRecordsByPoID($purchaseOrderID);
        
        if(!$poSubProductDeleteStatus)
        {
            // $this->rollback();
            // $this->status = false;
            return false;
        }

        $poProductM = new PurchaseOrderProduct();
        foreach ($post['poData']['poProducts'] as $product) {
            $poProductData = array(
                'purchaseOrderID' => $purchaseOrderID,
                'locationProductID' => $product['lPID'],
                'purchaseOrderProductQuantity' => $product['poPQ'],
                'purchaseOrderProductPrice' => $product['poPP'],
                'purchaseOrderProductDiscount' => $product['poPD'],
                'purchaseOrderProductTotal' => $product['poPT'],
                'purchaseOrderProductSelectedUomId' => $product['selectedUom'],
            );
            $poProductM->exchangeArray($poProductData);
            $insertedPoProductID = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->savePurchaseOrderProduct($poProductM);
            
            if(!$insertedPoProductID){
                // $this->rollback();
                // $this->status = false;
                return false;
            }
            if (array_key_exists('pT', $product)) {
                
                foreach ($product['pT'] as $taxKey => $productTax) {
                    $poPTaxesData = array(
                        'purchaseOrderID' => $purchaseOrderID,
                        'purchaseOrderProductID' => $insertedPoProductID,
                        'purchaseOrderTaxID' => $taxKey,
                        'purchaseOrderTaxPrecentage' => $productTax['pTP'],
                        'purchaseOrderTaxAmount' => $productTax['pTA']
                    );
                    $poPTaxM = new PurchaseOrderProductTax();
                    $poPTaxM->exchangeArray($poPTaxesData);
                    $this->CommonTable('Inventory\Model\PurchaseOrderProductTaxTable')->savePurchaseOrderProductTax($poPTaxM);

                }
                
            }
        }

        if (isset($documentReference)) {
            foreach ($documentReference as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $data = array(
                        'sourceDocumentTypeId' => 9,
                        'sourceDocumentId' => $purchaseOrderID,
                        'referenceDocumentTypeId' => $key,
                        'referenceDocumentId' => $value,
                    );
                    $documentReference = new DocumentReference();
                    $documentReference->exchangeArray($data);
                    $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                }
            }
        }
        
        return true; 
        
    }

}

?>
