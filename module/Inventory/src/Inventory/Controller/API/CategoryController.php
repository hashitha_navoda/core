<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * This file contains Units of Measure related controller functions
 */

namespace Inventory\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Inventory\Model\Category;
use Inventory\Form\CategoryForm;
use Zend\View\Model\JsonModel;

class CategoryController extends CoreController
{

    public function addAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $post = [
                'categoryName' => $req->getPost('categoryName'),
                'categoryPrefix' => $req->getPost('categoryPrefix'),
                'categoryParentID' => $req->getPost('categoryParentID'),
                'categoryState' => $req->getPost('categoryState'),
                'isWizard' => $req->getPost('isWizard')
            ];
            $isWizard = $req->getPost('isWizard');
            $category = new Category();
            $form = new CategoryForm();

            $form->setInputFilter($category->getInputFilter());
            $form->setData($post);

            if ($form->isValid()) {
                $post = $form->getData();

                // check if category with the same name exists in the same level
                $checkCategory = $this->CommonTable('Inventory/Model/CategoryTable')->getCategoryByNameAndParentID($post['categoryName'], $post['categoryParentID']);
                if (isset($checkCategory->categoryName)) {
                    $this->status = false;
                    $this->msg = _($this->getMessage('ERR_CATG_NAME_EXIST'));
                    return $this->JSONRespond();
                }

                // check if category with the same prefix exists in the same level
                if (isset($post['categoryPrefix']) && $post['categoryPrefix'] != '') {
                    $checkCategoryPrefix = $this->CommonTable('Inventory/Model/CategoryTable')->getCategoryByPrefixAndParentID($post['categoryPrefix'], $post['categoryParentID']);
                    if (isset($checkCategoryPrefix->categoryName)) {
                        $this->status = false;
                        $this->msg = _($this->getMessage('ERR_CATG_PREFIX_EXIST'));
                        return $this->JSONRespond();
                    }
                }

                $category->exchangeArray($form->getData());
                $entityID = $this->createEntity();

                $category->entityID = $entityID;
                $insertedID = $this->CommonTable('Inventory/Model/CategoryTable')->saveCategory($category);

                // get category list to return to view
                if ($isWizard == 1) {
                    $categoryList = $this->CommonTable('Inventory/Model/CategoryTable')->fetchAll();
                    $htmlOutput = $this->_getListViewHtml($categoryList, false);
                } else {
                    $categoryList = $this->getPaginatedCategory();
                    $htmlOutput = $this->_getListViewHtml($categoryList, true);
                }

                $this->status = true;
                $this->msg = $this->getMessage('SUCC_CATG');
                // get updated path list for typeahead
                $allCategoryPaths = $this->CommonTable('Inventory/Model/CategoryTable')->getHierarchyPaths();
                $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
                $this->data = array('paths' => $allCategoryPaths, 'categories' => $allCategoryList, 'categoryID' => $insertedID);

                $this->html = $htmlOutput;
                return $this->JSONRespondHtml();
            } else {
                // validation error
                $this->status = false;
                $this->msg = _($this->getMessage('ERR_CATG'));
                return $this->JSONRespond();
            }
        }
    }

    public function updateAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $post = [
                'categoryName' => $req->getPost('categoryName'),
                'categoryParentID' => $req->getPost('categoryParentID'),
                'categoryPrefix' => $req->getPost('categoryPrefix'),
                'categoryState' => $req->getPost('categoryState'),
                'categoryID' => $req->getPost('categoryID')
            ];

            $category = new Category();
            $form = new CategoryForm();

            $form->setInputFilter($category->getInputFilter());
            $form->setData($post);

            if ($form->isValid()) {

                $post = $form->getData();

                // check if category with the same name exists in the same level
                $currCat = $this->CommonTable('Inventory/Model/CategoryTable')->getCategoryByNameAndParentID($post['categoryName'], $post['categoryParentID']);
                if (isset($currCat->categoryID) && $currCat->categoryID != $post['categoryID']) {
                    $this->status = false;
                    $this->msg = _($this->getMessage('ERR_CATG_NAME_EXIST'));
                    return $this->JSONRespond();
                }

                // check if category with the same prefix exists in the same level
                if (isset($post['categoryPrefix']) && $post['categoryPrefix'] != '') {
                    $checkCategoryPrefix = $this->CommonTable('Inventory/Model/CategoryTable')->getCategoryByPrefixAndParentID($post['categoryPrefix'], $post['categoryParentID']);
                    if (isset($checkCategoryPrefix->categoryName) && $checkCategoryPrefix->categoryID != $post['categoryID']) {
                        $this->status = false;
                        $this->msg = _($this->getMessage('ERR_CATG_PREFIX_EXIST'));
                        return $this->JSONRespond();
                    }
                }

                $category->exchangeArray($post);
                $this->CommonTable('Inventory/Model/CategoryTable')->updateCategory($category);

                // get category list to return to view
                $categoryList = $this->getPaginatedCategory();
                $htmlOutput = $this->_getListViewHtml($categoryList, true);

                $this->status = true;

                // get updated path list for typeahead
                $allCategoryPaths = $this->CommonTable('Inventory/Model/CategoryTable')->getHierarchyPaths();
                $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
                $this->data = array('paths' => $allCategoryPaths, 'categories' => $allCategoryList);

                $this->msg = $this->getMessage('SUCC_CATG_UPDATE');
                $this->html = $htmlOutput;
                return $this->JSONRespondHtml();
            } else {
                // validation error
                $this->status = false;
                $this->msg = _($this->getMessage('ERR_CATG'));
                return $this->JSONRespond();
            }
        }
    }

    public function changeStateAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $categoryID = $req->getPost('categoryID');
            $categorystate = (int) $req->getPost('categoryState');
            $checkParent = $this->CommonTable('Inventory/Model/CategoryTable')->checkParent($categoryID, 1);

            if ($checkParent && !($categorystate)) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CATG_PARENTINACTIVE');
                return $this->JSONRespond();
            }

            if ($categorystate) {
                $category = $this->CommonTable('Inventory/Model/CategoryTable')->getCategory($categoryID);
                $parentID = $category->categoryParentID;
                if ($parentID != 0) {
                    $checkParentCat = $this->CommonTable('Inventory/Model/CategoryTable')->checkCategory($parentID, 1);
                } else {
                    $checkParentCat = true;
                }
                if (!$checkParentCat) {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CATG_NOPARENT');
                    return $this->JSONRespond();
                }
            }

            $post = [
                'categoryState' => $categorystate,
                'categoryID' => $categoryID
            ];

            $category = new Category();
            $category->exchangeArray($post);
            $this->CommonTable('Inventory/Model/CategoryTable')->updateCategoryState($category);

            // get category list to return to view
            $categoryList = $this->getPaginatedCategory();
            $htmlOutput = $this->_getListViewHtml($categoryList, true);

            $verb = ($post['categoryState']) ? 'activated' : 'deactivated';
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_CATG_STATUS', array($verb));
            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        }
    }

    public function deleteAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $categoryID = $req->getPost('categoryID');
            $checkParent = $this->CommonTable('Inventory/Model/CategoryTable')->isParent($categoryID);
            if ($checkParent) {
                $this->status = false;
                $this->msg = _($this->getMessage('REQ_CATG_DELCHILD'));
                return $this->JSONRespond();
            }

            $checkProduct = $this->CommonTable('Inventory/Model/ProductTable')->useCategory($categoryID);
            if ($checkProduct) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CATG_INACTIVE');
                return $this->JSONRespond();
            }

            $result = $this->CommonTable('Inventory/Model/CategoryTable')->getCategory($categoryID);
            $this->updateDeleteInfoEntity($result->entityID);

            // get category list to return to view
            $categoryList = $this->getPaginatedCategory();
            $htmlOutput = $this->_getListViewHtml($categoryList, true);

            $this->status = true;
            $this->msg = $this->getMessage('SUCC_CATG_DEL');

            // get updated path list for typeahead
            $allCategoryPaths = $this->CommonTable('Inventory/Model/CategoryTable')->getHierarchyPaths();
            $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
            $this->data = array('paths' => $allCategoryPaths, 'categories' => $allCategoryList);

            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        }
    }

    public function searchAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $keyword = $req->getPost('keyword');

            if (!empty($keyword)) {
                $tbl = $this->CommonTable('Inventory/Model/CategoryTable');
                $categoryList = $tbl->getCategoryforSearch($keyword);
                $paginated = false;
            } else {
                $categoryList = $this->getPaginatedCategory();
                $paginated = true;
            }

            $htmlOutput = $this->_getListViewHtml($categoryList, $paginated);

            $this->status = true;
            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        }
    }

    private function _getListViewHtml($categoryList = array(), $paginated = false)
    {
        $view = new ViewModel(array(
            'categoryList' => $categoryList,
            'paginated' => $paginated
        ));

        $view->setTerminal(true);
        $view->setTemplate('inventory/category/list.phtml');

        return $view;
    }

    public function getAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $categoryID = $req->getPost('categoryID');

            $tbl = $this->CommonTable('Inventory/Model/CategoryTable');
            $categoryData = $tbl->getCategory($categoryID);

            $this->status = true;
            $this->data = $categoryData;
            return $this->JSONRespond();
        }
    }

    protected function getPaginatedCategory($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Inventory/Model/CategoryTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Category list accessed');

        return $this->paginator;
    }
    
    public function searchCategoriesForDropdownAction() 
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $searchKey = $request->getPost('searchKey');
            $categoryList = array();
            $categories = $this->CommonTable('Inventory/Model/CategoryTable')->getActiveCategories($searchKey);
            $temp = array();
            foreach ($categories as $category) {
                $temp['value'] = $category['categoryID'];
                $temp['text']  = $category['categoryName'];
                $categoryList[] = $temp;
            }
            $this->data = array('list' => $categoryList);
            return $this->JSONRespond();
        }
    }

}
