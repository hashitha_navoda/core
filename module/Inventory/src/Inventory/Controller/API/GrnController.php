<?php

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Inventory\Model\Grn;
use Inventory\Model\DraftGrn;
use Inventory\Model\GrnProduct;
use Inventory\Model\DraftGrnProduct;
use Inventory\Model\GrnProductTax;
use Inventory\Model\DraftGrnProductTax;
use Inventory\Model\ProductBatch;
use Inventory\Model\DraftProductBatch;
use Inventory\Model\ProductSerial;
use Inventory\Model\DraftProductSerial;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemOut;
use Inventory\Model\CompoundCostTemplate;
use Inventory\Model\CompoundCostType;
use Inventory\Model\CompoundCostValue;
use Inventory\Model\CompoundCostItem;
use Inventory\Model\CompoundCostGrn;
use Core\Model\DocumentReference;

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */
class GrnController extends CoreController
{

    const ITEM_IMPORT_DIR = "/tmp/";
    const ITEM_IMPORT_FILE_PREFIX = "ez-biz-grn-item-import-";
    const PO_DOCUMENT_TYPE_ID = 9;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion save a GRN to the DB.
     * If success, Saved GRN ID and true status are returned.
     */
    public function saveGrnAction()
    {
        /**
         * This action will update several tables.
         * Including grn,grnProduct,productBatch,productSerial,grnProductTax,locationProduct
         */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountsData = $glAccountExist->current();
            
//check whether GRN is created via a Purchase Order
            $poProductsUncopiedArr = [];
            $poProductsUncopiedArrForGrnApproval = [];
            if ($post['startByPoID'] != '') {
                $copiedFromPo = TRUE;
                //get purchase order product details
                $poProducts = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getUnCopiedPoProducts($post['startByPoID']);
                foreach ($poProducts as $product) {
                    $restQty = $product['purchaseOrderProductQuantity'] - $product['purchaseOrderProductCopiedQuantity'];
                    $restQty = ($restQty < 0) ? 0 : $restQty;
                    $poProductsUncopiedArr[$product['locationProductID']] = $restQty;
                    $poProductsUncopiedArrForGrnApproval[$product['locationProductID']] = [
                        'restQty' => $restQty,
                        'price' => $product['purchaseOrderProductPrice']
                    ];
                }
            } else {
                $copiedFromPo = FALSE;
            }
            $grnCode = $post['gC'];
            $locationReferenceID = $post['lRID'];
            $locationID = $post['rL'];
            $documentReference = $post['documentReference'];
            $dimensionData = $post['dimensionData'];
            $ignoreBudgetLimit = $post['ignoreBudgetLimit'];
            $isFromApprovedPo = false;

            $this->beginTransaction();

            if ($post['isActiveWF'] == 1) {
                if(!$copiedFromPo && !empty($post['wfID']) ) {
                    //check whether grn has to through the approval workflow or not
                    $limitData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->getLimitsByWFId($post['wfID']);
                    $limitID = null;

                    foreach ($limitData as $key => $value) {
                        if (floatval($post['fT']) <= floatval($value['approvalWorkflowLimitMax']) && floatval($post['fT']) >= floatval($value['approvalWorkflowLimitMin'])) {
                            $limitID = $value['approvalWorkflowLimitsID'];
                            break;
                        }
                    }
                    if ($limitID != null) {
                        $limitApproversData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->getLimitApproversByWFLimitId($limitID);
                        $approvers = [];

                        foreach ($limitApproversData as $key => $value) {
                            $approvers[] = $value['workflowLimitApprover'];
                        }

                        $saveGRNDraft = $this->saveGRNDraft($post);

                        if (!$saveGRNDraft['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $saveGRNDraft['msg'];
                            return $this->JSONRespond();
                        } else {
                            $this->commit();
                            $this->status = true;
                            $this->data = array('grnID' => $saveGRNDraft['data']['grnID'], 'approvers' => $approvers, 'hashValue' => $saveGRNDraft['data']['hashValue'], 'grnCode' => $saveGRNDraft['data']['grnCode'], 'isDraft' => true);
                            $this->msg = $this->getMessage('SUCC_GRN_WF');
                            return $this->JSONRespond();
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('SELECTED_APPROVAL_WF_RANGES_MISMATCH');
                        return $this->JSONRespond();
                    }
                } elseif($copiedFromPo && !empty($post['wfID']) )  {

                    $checkCopiedPoForApprovalWF = $this->checkCopiedPoForApprovalWF($poProductsUncopiedArrForGrnApproval, array_filter(json_decode($post['pr'], true)));


                    if ($checkCopiedPoForApprovalWF) {
                        if (!empty($post['wfID'])) {
                            $limitData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->getLimitsByWFId($post['wfID']);
                            $limitID = null;

                            foreach ($limitData as $key => $value) {
                                if (floatval($post['fT']) <= floatval($value['approvalWorkflowLimitMax']) && floatval($post['fT']) >= floatval($value['approvalWorkflowLimitMin'])) {
                                    $limitID = $value['approvalWorkflowLimitsID'];
                                    break;
                                }
                            }
                            
                            if ($limitID != null) {
                                $limitApproversData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->getLimitApproversByWFLimitId($limitID);
                                $approvers = [];

                                foreach ($limitApproversData as $key => $value) {
                                    $approvers[] = $value['workflowLimitApprover'];
                                }

                                $saveGRNDraft = $this->saveGRNDraft($post);

                                if (!$saveGRNDraft['status']) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $saveGRNDraft['msg'];
                                    return $this->JSONRespond();
                                } else {
                                    $this->commit();
                                    $this->status = true;
                                    $this->data = array('grnID' => $saveGRNDraft['data']['grnID'], 'approvers' => $approvers, 'hashValue' => $saveGRNDraft['data']['hashValue'], 'grnCode' => $saveGRNDraft['data']['grnCode'], 'isDraft' => true);
                                    $this->msg = $this->getMessage('SUCC_GRN_WF');
                                    return $this->JSONRespond();
                                }
                            } else {
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('COPIED_PO_HAS_CHANGES');
                                return $this->JSONRespond();
                            }
                        }
                    } else {
                        $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrderByID($post['startByPoID']);

                        if (!$poData->purchaseOrderApproved) {
                            $limitData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->getLimitsByWFId($post['wfID']);
                            $limitID = null;

                            foreach ($limitData as $key => $value) {
                                if (floatval($post['fT']) <= floatval($value['approvalWorkflowLimitMax']) && floatval($post['fT']) >= floatval($value['approvalWorkflowLimitMin'])) {
                                    $limitID = $value['approvalWorkflowLimitsID'];
                                    break;
                                }
                            }
                            
                            if ($limitID != null) {
                                $limitApproversData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->getLimitApproversByWFLimitId($limitID);
                                $approvers = [];

                                foreach ($limitApproversData as $key => $value) {
                                    $approvers[] = $value['workflowLimitApprover'];
                                }

                                $saveGRNDraft = $this->saveGRNDraft($post);

                                if (!$saveGRNDraft['status']) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $saveGRNDraft['msg'];
                                    return $this->JSONRespond();
                                } else {
                                    $this->commit();
                                    $this->status = true;
                                    $this->data = array('grnID' => $saveGRNDraft['data']['grnID'], 'approvers' => $approvers, 'hashValue' => $saveGRNDraft['data']['hashValue'], 'grnCode' => $saveGRNDraft['data']['grnCode'], 'isDraft' => true);
                                    $this->msg = $this->getMessage('SUCC_GRN_WF');
                                    return $this->JSONRespond();
                                }
                            } else {
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('COPIED_PO_HAS_CHANGES');
                                return $this->JSONRespond();
                            }
                        } else {
                            $isFromApprovedPo = true;
                        }
                    }
                }
            }
// check if a grn from the same grn Code exists if exist add next number to grn code
            while ($grnCode) {
                if ($this->CommonTable('Inventory\Model\GrnTable')->checkGrnByCode($grnCode)->current()) {
                    if ($locationReferenceID) {
                        $newGrnCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newGrnCode == $grnCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $grnCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $grnCode = $newGrnCode;
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_GRN_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }
            $entityID = $this->createEntity();
            $grnData = array(
                'grnSupplierID' => $post['sID'],
                'grnSupplierReference' => $post['sR'],
                'grnCode' => $grnCode,
                'grnRetrieveLocation' => $locationID,
                'grnDate' => $this->convertDateToStandardFormat($post['dD']),
                'grnDeliveryCharge' => $post['dC'],
                'grnTotal' => $post['fT'],
                'grnComment' => $post['cm'],
                'grnShowTax' => $post['sT'],
                'entityID' => $entityID,
                'grnUploadFlag' => filter_var($post['uploadDocFlag'], FILTER_VALIDATE_BOOLEAN),
            );
            $grnM = new Grn();
            $grnM->exchangeArray($grnData);

//save the grn details to the grn table
            $grnID = $this->CommonTable('Inventory\Model\GrnTable')->saveGrn($grnM);

            if ($isFromApprovedPo) {
                $dataSetGrn = [
                    'isApprovedThroughWF' => 1
                ];

               $updateGrnFromWFStatus =  $this->CommonTable('Inventory\Model\GrnTable')->updateGrnRecord($grnID,$dataSetGrn);
            }
            //check whether grn save or not
            if(!$grnID){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_GRN_CREATE');
                return $this->JSONRespond();
            }
            // save Compound Costing Grn 
            if ($post['compoundCostID'] != '') {
                $compCostGrnData = [
                    'compoundCostTemplateID' => $post['compoundCostID'],
                    'grnID' => $grnID
                ];
                $costGrn = new CompoundCostGrn();
                $costGrn->exchangeArray($compCostGrnData);
                $saveCostGrn = $this->CommonTable('Inventory\Model\CompoundCostGrnTable')->saveCompoundCostGrn($costGrn);
            }
//update the reference number
            $this->updateReferenceNumber($locationReferenceID);
//loop through the products

            $allProductTotal = 0;
            // remove empty values and decord data
            $proList = array_filter(json_decode($post['pr'], true));

            foreach ($proList as $product) {
                $locationPID = $product['locationPID'];

                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

                $productType = $product['productType'];
                //use to calculate product discount via value or percentage
                if($product['pDType'] == 'value'){
                    $discountValue = $product['pDiscount'];
                    $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
                } else {
                    $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                    $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                }

                $allProductTotal+=$productTotal;
//if copied from PO,update those products as copied.
                if ($copiedFromPo == TRUE) {
                    $this->updatePoProductCopiedQty($locationPID, $post['startByPoID'], $product['pQuantity']);
                }
                $productBaseQty = $product['pQuantity'];
                $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);

//if product is a batch Product

                // if (array_key_exists('bProducts', $product)) {
                if (sizeof($product['bProducts']) > 0) {
                    $batchProductCount = 0;
                    foreach ($product['bProducts'] as $bProduct) {

                        $batchProductCount++;
                        $bPData = array(
                            'locationProductID' => $locationPID,
                            'productBatchCode' => $bProduct['bCode'],
                            'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                            'productBatchWarrantyPeriod' => $bProduct['warnty'],
                            'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                            'productBatchQuantity' => $bProduct['bQty'] * $productConversionRate,
                            'productBatchPrice' => ($bProduct['price'] / $productConversionRate),
                        );
                        $batchProduct = new ProductBatch();
                        $batchProduct->exchangeArray($bPData);

                        $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);

//if the product is batch and serial
                        // if (array_key_exists('sProducts', $product)) {
                        if(sizeof($product['sProducts']) > 0){
                            foreach ($product['sProducts'] as $sProduct) {
                                if (isset($sProduct['sBCode'])) {
                                    if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                        $sPData = array(
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialCode' => $sProduct['sCode'],
                                            'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                            'productSerialWarrantyPeriodType' =>(int)$sProduct['sWarrantyType'],
                                        );
                                        $serialPr = new ProductSerial();
                                        $serialPr->exchangeArray($sPData);
                                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                        $grnProductM = new GrnProduct();
                                        $grnProductData = array(
                                            'grnID' => $grnID,
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialID' => $insertedSID,
                                            'grnProductPrice' => $product['pUnitPrice'],
                                            'grnProductDiscountType' => $product['pDType'],
                                            'grnProductDiscount' => $product['pDiscount'],
                                            'grnProductTotalQty' => $productBaseQty,
                                            'grnProductQuantity' => $bProduct['bQty'],
                                            'grnProductUom' => $product['pUom'],
                                            'grnProductTotal' => $product['pTotal'],
                                            'rackID' => ''
                                        );

                                        if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > $serialProductCount) {
                                            $copiedQty = 1;
                                            $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                                            $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                                            $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                                        }

                                        $grnProductM->exchangeArray($grnProductData);
                                        $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);
//save the product details to the itemInTable

                                        $itemInData = array(
                                            'itemInDocumentType' => 'Goods Received Note',
                                            'itemInDocumentID' => $grnID,
                                            'itemInLocationProductID' => $locationPID,
                                            'itemInBatchID' => $insertedBID,
                                            'itemInSerialID' => $insertedSID,
                                            'itemInQty' => 1,
                                            'itemInPrice' => $product['pUnitPrice'],
                                            'itemInDiscount' => $discountValue,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                        if (array_key_exists('pTax', $product)) {
                                            if (array_key_exists('tL', $product['pTax'])) {
                                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                    $grnPTaxesData = array(
                                                        'grnID' => $grnID,
                                                        'grnProductID' => $insertedGrnProductID,
                                                        'grnTaxID' => $taxKey,
                                                        'grnTaxPrecentage' => $productTax['tP'],
                                                        'grnTaxAmount' => $productTax['tA']
                                                    );
                                                    $grnPTaxM = new GrnProductTax();
                                                    $grnPTaxM->exchangeArray($grnPTaxesData);
                                                    $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                                                }
                                            }
                                        }
                                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                        $newPQtyData = array(
                                            'locationProductQuantity' => $newPQty
                                        );
//Update location Product Quantity
                                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                    }
                                }
                            }
                        } else {
                            $grnProductM = new GrnProduct();
                            $grnProductData = array(
                                'grnID' => $grnID,
                                'locationProductID' => $locationPID,
                                'productBatchID' => $insertedBID,
                                'grnProductPrice' => $product['pUnitPrice'],
                                'grnProductDiscountType' => $product['pDType'],
                                'grnProductDiscount' => $product['pDiscount'],
                                'grnProductTotalQty' => $productBaseQty,
                                'grnProductQuantity' => $bProduct['bQty'] * $productConversionRate,
                                'grnProductUom' => $product['pUom'],
                                'grnProductTotal' => $product['pTotal'],
                                'rackID' => ''
                            );

                            if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > 0) {
                                $copiedQty = ($bProduct['bQty'] < $poProductsUncopiedArr[$locationPID]) ? $bProduct['bQty'] : $poProductsUncopiedArr[$locationPID];
                                $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                                $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                                $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                            }

                            $grnProductM->exchangeArray($grnProductData);
                            $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);
//save itemIn
                            $itemInData = array(
                                'itemInDocumentType' => 'Goods Received Note',
                                'itemInDocumentID' => $grnID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => $insertedBID,
                                'itemInSerialID' => NULL,
                                'itemInQty' => $bProduct['bQty'] * $productConversionRate,
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if (array_key_exists('pTax', $product) && $product['pTax'] != '') {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $grnPTaxesData = array(
                                            'grnID' => $grnID,
                                            'grnProductID' => $insertedGrnProductID,
                                            'grnTaxID' => $taxKey,
                                            'grnTaxPrecentage' => $productTax['tP'],
                                            'grnTaxAmount' => $productTax['tA']
                                        );
                                        $grnPTaxM = new GrnProductTax();
                                        $grnPTaxM->exchangeArray($grnPTaxesData);
                                        $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                                    }
                                }
                            }
                            if ($batchProductCount == 1) {
                                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                                $newPQtyData = array(
                                    'locationProductQuantity' => $newPQty
                                );
                                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                            }
                        }
                    }

                // } elseif (array_key_exists('sProducts', $product)) {
                } elseif (sizeof($product['sProducts']) > 0) {
                //if the product is a serial Product             
                    $serialProductCount = 0;
                    foreach ($product['sProducts'] as $sProduct) {
                        $sPData = array(
                            'locationProductID' => $locationPID,
                            'productSerialCode' => $sProduct['sCode'],
                            'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                            'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                        );            
                        $serialPr = new ProductSerial();
                        $serialPr->exchangeArray($sPData);
                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                        $grnProductM = new GrnProduct();
                        $grnProductData = array(
                            'grnID' => $grnID,
                            'locationProductID' => $locationPID,
                            'productSerialID' => $insertedSID,
                            'grnProductPrice' => $product['pUnitPrice'],
                            'grnProductDiscountType' => $product['pDType'],
                            'grnProductDiscount' => $product['pDiscount'],
                            'grnProductTotalQty' => $productBaseQty,
                            'grnProductQuantity' => 1,
                            'grnProductUom' => $product['pUom'],
                            'grnProductTotal' => $product['pTotal'],
                            'rackID' => ''
                        );

                        if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > $serialProductCount) {
                            $copiedQty = 1;
                            $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                            $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                            $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                        }

                        $grnProductM->exchangeArray($grnProductData);
                        $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);
//save serial product itemIn
                        $itemInData = array(
                            'itemInDocumentType' => 'Goods Received Note',
                            'itemInDocumentID' => $grnID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => $insertedSID,
                            'itemInQty' => 1,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                        if (array_key_exists('pTax', $product)) {
                            if (array_key_exists('tL', $product['pTax'])) {
                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                    $grnPTaxesData = array(
                                        'grnID' => $grnID,
                                        'grnProductID' => $insertedGrnProductID,
                                        'grnTaxID' => $taxKey,
                                        'grnTaxPrecentage' => $productTax['tP'],
                                        'grnTaxAmount' => $productTax['tA']
                                    );
                                    $grnPTaxM = new GrnProductTax();
                                    $grnPTaxM->exchangeArray($grnPTaxesData);
                                    $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                                }
                            }
                        }
                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                        );
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        $serialProductCount++;
                    }
                } else {
//If the product is a non batch and non serial product
                    $grnProductM = new GrnProduct();
                    $grnProductData = array(
                        'grnID' => $grnID,
                        'locationProductID' => $locationPID,
                        'grnProductPrice' => $product['pUnitPrice'],
                        'grnProductTotalQty' => $productBaseQty,
                        'grnProductQuantity' => $productBaseQty,
                        'grnProductUom' => $product['pUom'],
                        'grnProductDiscountType' => $product['pDType'],
                        'grnProductDiscount' => $product['pDiscount'],
                        'grnProductTotal' => $product['pTotal']
                    );

                    if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > 0) {
                        $copiedQty = ($productBaseQty < $poProductsUncopiedArr[$locationPID]) ? $productBaseQty : $poProductsUncopiedArr[$locationPID];
                        $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                        $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                        $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                    }

                    $grnProductM->exchangeArray($grnProductData);
                    $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);


                    //save itemIn Details
                    if ($productType != 2) {
                        $itemInData = array(
                            'itemInDocumentType' => 'Goods Received Note',
                            'itemInDocumentID' => $grnID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => NULL,
                            'itemInQty' => $productBaseQty,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                    }

                    if (array_key_exists('pTax', $product) && $product['pTax'] != '') {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $grnPTaxesData = array(
                                    'grnID' => $grnID,
                                    'grnProductID' => $insertedGrnProductID,
                                    'grnTaxID' => $taxKey,
                                    'grnTaxPrecentage' => $productTax['tP'],
                                    'grnTaxAmount' => $productTax['tA']
                                );
                                $grnPTaxM = new GrnProductTax();
                                $grnPTaxM->exchangeArray($grnPTaxesData);
                                $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                            }
                        }
                    }
                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    if ($productType == 2) {
                        $productBaseQty = 0;
                    }
                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                }

                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
                $locationPrTotalQty = 0;
                $locationPrTotalPrice = 0;
                $newItemInIds = array();
                if(count($itemInData) > 0){
                    foreach ($itemInData as $key => $value) {
                        if($value->itemInID > $locationProductLastItemInID){
                            $newItemInIds[] = $value->itemInID;
                            $remainQty = $value->itemInQty - $value->itemInSoldQty;
                            if($remainQty > 0){
                                $itemInPrice = $remainQty*$value->itemInPrice;
                                $itemInDiscount = $remainQty*$value->itemInDiscount;
                                $locationPrTotalQty += $remainQty;
                                $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                            }
                            $locationProductLastItemInID = $value->itemInID;
                        }
                    }
                }
                $locationPrTotalQty += $locationProductQuantity;
                $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                if($locationPrTotalQty > 0){
                    $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                    $lpdata = array(
                        'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                        'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                    foreach ($newItemInIds as $inID) {
                        $intemInData = array(
                            'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                        );
                        $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                    }
                }
            }

            //add document reference for document reference table
            if (count($documentReference) > 0) {
                foreach ($documentReference as $key => $val) {
                    foreach ($val as $key1 => $value) {
                        $data = array(
                            'sourceDocumentTypeId' => 10,
                            'sourceDocumentId' => $grnID,
                            'referenceDocumentTypeId' => $key,
                            'referenceDocumentId' => $value,
                        );
                        $documentReference = new DocumentReference();
                        $documentReference->exchangeArray($data);
                        $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                    }
                }
            }

            //update PO status if copied from it
            if ($copiedFromPo == TRUE) {
                $this->checkAndUpdatePoStatus($post['startByPoID']);
            }

            // call product updated event
            $productIDs = array_map(function($element) {
                return $element['pID'];
            }, json_decode($post['pr'], true));

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            if ($this->useAccounting == 1) {
                //check whether supplier GRN Clearing account is set or not
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($post['sID']);
                if(empty($supplierData->supplierGrnClearingAccountID)){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_GRN_SUPPLIER_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                    return $this->JSONRespond();
                }
                $GRNClearingAccountID = $supplierData->supplierGrnClearingAccountID;

                $accountProduct = array();
                foreach ($proList as $product) {
                    //check whether Product Inventory Gl account id set or not
                    $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($product['pID']);
                    if(empty($pData['productInventoryAccountID'])){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                        return $this->JSONRespond();
                    }
                    //calculate product total without tax potion.
                    //use to calculate product discount via value or percentage
                    if($product['pDType'] == 'value'){
                        $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
                    } else {
                        $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                    }

                    //add grn deliveryCharge to product according to price ratio.
                    // if($post['dC'] >0){
                    //     $productTotal += ($post['dC']*$productTotal)/ $allProductTotal;
                    // }
                    //set gl accounts for the journal entry
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['total'])){
                        $accountProduct[$pData['productInventoryAccountID']]['total'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['total'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
                    }
                }

                //create data array for the journal entry save.
                $i=0;
                $journalEntryAccounts = array();
                $totalValueForSupplierAccount = 0;

                foreach ($accountProduct as $key => $value) {
                    $totalValueForSupplierAccount+=$value['total'];
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['total'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode;
                    $i++;
                }

                if($post['dC'] > 0){
                    if (!empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)) {
                        if(isset($accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'])){
                            $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'] += $post['dC'];
                        }else{
                            $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'] = $post['dC'];
                            $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['accountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                        }


                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;;
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'];
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode;

                        $totalValueForSupplierAccount+= $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'];
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DELI_CHARGE_ACCOUNT');
                        return $this->JSONRespond();
                    }

                }


                $inc = (!empty($post['dC'])) ?  $i +1 : $i;

                $journalEntryAccounts[$inc]['fAccountsIncID'] = $inc;
                $journalEntryAccounts[$inc]['financeAccountsID'] = $GRNClearingAccountID;
                $journalEntryAccounts[$inc]['financeGroupsID'] = '';
                $journalEntryAccounts[$inc]['journalEntryAccountsDebitAmount'] = 0.00;
                $journalEntryAccounts[$inc]['journalEntryAccountsCreditAmount'] = $totalValueForSupplierAccount;
                $journalEntryAccounts[$inc]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode.".";

                //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $grnData['grnDate'],
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create GRN '.$grnCode.".",
                    'documentTypeID' => 10,
                    'journalEntryDocumentID' => $grnID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                );

                $resultData = $this->saveJournalEntry($journalEntryData);

                if($resultData['status']){
                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($grnID,10, 3);
                    if(!$jEDocStatusUpdate['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $jEDocStatusUpdate['msg'];
                        return $this->JSONRespond();
                    }

                    if (!empty($dimensionData)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$grnCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->msg =$saveRes['msg'];
                            $this->data =$saveRes['data'];
                            return $this->JSONRespond();
                        }   
                    }                          

                    $this->commit();
                    $this->status = true;
                    $this->data = array('grnID' => $grnID, 'isDraft' => false);
                    $this->msg = $this->getMessage('SUCC_GRN_CREATE', array($grnCode));
                    $this->setLogMessage($this->companyCurrencySymbol . $product['pTotal'] . ' GRN successfully created -' . $grnCode);
                }else{
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $resultData['msg'];
                    $this->data = $resultData['data'];
                }
            }else{
                $this->commit();
                $this->status = true;
                $this->data = array('grnID' => $grnID, 'isDraft' => false);
                $this->msg = $this->getMessage('SUCC_GRN_CREATE', array($grnCode));
                $this->setLogMessage($this->companyCurrencySymbol . $product['pTotal'] . ' GRN successfully created -' . $grnCode);
            //$this->flashMessenger()->addMessage($this->msg);
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function returns a GRN code for a url
     * LocationID must passed with POST
     * @return type
     */
    public function getGrnNoForLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
//Reference NO 10 is for the GRN
            $refData = $this->getReferenceNoForLocation(10, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];
            if ($rid == '' || $rid == NULL) {
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_GRN_NO_NUM');
                } else {
                    $this->msg = $this->getMessage('ERR_GRN_REFMAX');
                }
                $this->data = FALSE;
                $this->status = false;
            } else {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $rid,
                    'locRefID' => $lrefID,
                );
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param string GRN code or supplier name or GRN ID as searchkey
     * @return GRN list viewmodel
     */
    public function getGrnsForSearchAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
//if Null passed return the default grn list page
            if ($request->getPost('searchKey') != '') {
                $grnList = $this->CommonTable('Inventory\Model\GrnTable')->getGrnsforSearch($request->getPost('searchKey'));
                $grnListView = new ViewModel(array('grnList' => $grnList, 'statuses' => $this->getStatusesList(), 'paginated' => false, 'dateFormat' => $dateFormat));
            } else {
                $this->paginator = $this->CommonTable('Inventory\Model\GrnTable')->getGrns(true);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $grnListView = new ViewModel(array('grnList' => $this->paginator, 'statuses' => $this->getStatusesList(), 'dateFormat' => $dateFormat));
            }

            $grnListView->setTemplate('inventory/grn/grn-list');
            $this->html = $grnListView;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function returns grn details
     * @param POST grnID
     */
    public function getGrnDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $getGrnID = $request->getPost('grnID');
            /**
             * Same functions is used to get details to the pv.
             * $toPv flag was added to get details to the pv
             * by $toPv flag,it means don't get copied grn products
             */
            $toPv = $request->getPost('toPv');
            if ($toPv) {
                $toPv = TRUE;
            } else {
                $toPv = FALSE;
            }
            $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnID($getGrnID, $toPv ,$checkItemIn = TRUE);
            $grnDetails = array();
            $grnID;
            $counter = 0;
            foreach ($grnData as $row) {
                $counter++;
                $tempG = array();
                $grnID = $row['grnID'];
                $tempG['gID'] = $row['grnID'];
                $tempG['gCd'] = $row['grnCode'];
                $tempG['gSID'] = $row['grnSupplierID'];
                $tempG['gSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempG['gSR'] = $row['grnSupplierReference'];
                $tempG['gRLID'] = $row['grnRetrieveLocation'];
                $tempG['gRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempG['gD'] = $this->convertDateToUserFormat($row['grnDate']);
                $tempG['gDC'] = $row['grnDeliveryCharge'];
                $tempG['gT'] = $row['grnTotal'];
                $tempG['gC'] = $row['grnComment'];
                $tempG['productType'] = $row['productTypeID'];
                $tempG['gSOB'] = $row['supplierOutstandingBalance'];
                $tempG['gSCB'] = $row['supplierCreditBalance'];
                $grnProducts = (isset($grnDetails[$row['grnID']]['gProducts'])) ? $grnDetails[$row['grnID']]['gProducts'] : array();

                /////////////////////
                if($counter == 1){
                //this condition use to set indexkey value for first time when run this main foreach loop
                    $indexKey = $row['grnProductID'];

                }
                if(isset($grnDetails[$row['grnID']]['gProducts']['uniqKey']) &&
                    $grnDetails[$row['grnID']]['gProducts']['uniqKey'] != $row['locationProductID']){
                    $indexKey = $row['grnProductID'];
                } else if(isset($grnDetails[$row['grnID']]['gProducts']['uniqKey']) &&
                    $grnDetails[$row['grnID']]['gProducts']['uniqKey'] == $row['locationProductID'] &&
                        $grnProducts[$indexKey]['gPP'] != $row['grnProductPrice']){
                    $indexKey = $row['grnProductID'];
                }

                /////////////////////

                if ($row['grnProductID'] != NULL) {
                    $productTaxes = (isset($grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['pT'])) ? $grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['pT'] : array();
                    $productBatches = (isset($grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['bP'])) ? $grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['bP'] : array();
                    $productSerials = (isset($grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['sP'])) ? $grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['sP'] : array();
                    $productUoms = (isset($grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['pUom'])) ? $grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['pUom'] : array();

                    $grnProducts[$row['locationProductID'].round($row['grnProductPrice'])] = array(
                            'gPC' => $row['productCode'],
                            'gPID' => $row['productID'],
                            'gPN' => $row['productName'],
                            'lPID' => $row['locationProductID'],
                            'gPP' => $row['grnProductPrice'],
                            'gPD' => $row['grnProductDiscount'],
                            'gPQ' => $row['grnProductTotalQty'] - $row['grnProductTotalCopiedQuantity'],
                            'gPT' => $row['grnProductTotal'],
                            'gProId' => $row['grnProductID'],
                            'productType' => $row['productTypeID'],
                            'grnIndexKey' => $row['locationProductID'].round($row['grnProductPrice']),
                        );
                    if ($row['productBatchID'] != NULL) {
                        $productBatches[$row['productBatchID']] = array(
                        'bC' => $row['productBatchCode'],
                        'bED' => $this->convertDateToUserFormat($row['productBatchExpiryDate']),
                        'bW' => $row['productBatchWarrantyPeriod'],
                        'bMD' => $this->convertDateToUserFormat($row['productBatchManufactureDate']),
                        'bQ' => $row['grnProductQuantity']- $row['grnProductCopiedQuantity']); ///////////////////date-format
                    }
                    if ($row['productSerialID'] != NULL) {
                        $productSerials[$row['productSerialID']] = array(
                        'sC' => $row['productSerialCode'],
                        'sW' => $row['productSerialWarrantyPeriod'],
                        'sWT' => $row['productSerialWarrantyPeriodType'],
                        'sED' => $this->convertDateToUserFormat($row['productSerialExpireDate']),
                        'sBC' => $row['productBatchCode']); ///////////////////date-format
                    }
                    if ($row['grnTaxID'] != NULL) {
                        $productTaxes[$row['grnTaxID']] = array(
                            'pTN' => $row['taxName'],
                            'pTP' => $row['grnTaxPrecentage'],
                            'pTA' => $row['grnTaxAmount']);
                    }
                    if ($row['uomID'] != NULL) {
                        $productUoms[$row['uomID']] = array(
                            'gPUom' => $row['uomAbbr'],
                            'gPUomID' => $row['uomID'],
                            'gPUomCon' => $row['productUomConversion']);
                    }
                    $grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['pT'] = $productTaxes;
                    $grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['bP'] = $productBatches;
                    $grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['sP'] = $productSerials;
                    $grnProducts[$row['locationProductID'].round($row['grnProductPrice'])]['pUom'] = $productUoms;
                }
                $tempG['gProducts'] = $grnProducts;
                $tempG['gProducts']['uniqKey'] = $row['locationProductID'];
                $grnDetails[$row['grnID']] = $tempG;
            }
            unset($grnDetails[$row['grnID']]['gProducts']['uniqKey']);

//get details of purchase invoice if any returns were made to the current GRN
            $prRelatedDetails = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnDetailsByGrnID($grnID);
            $prID;
            $prDetails = array();
            foreach ($prRelatedDetails as $prData) {
                $tempPr = array();
                $prID = $prData['purchaseReturnID'];
                $tempPr['prID'] = $prData['purchaseReturnID'];
                $tempPr['prCd'] = $prData['purchaseReturnCode'];
                $tempPr['prSID'] = $prData['grnSupplierID'];
                $tempPr['prSName'] = $prData['supplierTitle'] . ' ' . $prData['supplierName'];
                $tempPr['prSR'] = $prData['grnSupplierReference'];
                $tempPr['prRLID'] = $prData['grnRetrieveLocation'];
                $tempPr['prRL'] = $prData['locationCode'] . ' - ' . $prData['locationName'];
                $tempPr['prD'] = $prData['purchaseReturnDate'];
                $tempPr['prT'] = $prData['purchaseReturnTotal'];
                $tempPr['prC'] = $prData['purchaseReturnComment'];
                $prProducts = (isset($prDetails[$prID]['prProducts'])) ? $prDetails[$prID]['prProducts'] : array();
                if ($prData['productID'] != NULL) {
                    $productTaxes = (isset($prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['pT'])) ?
                        $prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['pT'] : array();
                    $productBatches = (isset($prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['bP'])) ?
                        $prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['bP'] : array();
                    $productSerials = (isset($prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['sP'])) ?
                        $prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['sP'] : array();
                    $productUoms = (isset($prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['pUom'])) ?
                        $prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['pUom'] : array();

                    $prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])] = array(
                        'prPC' => $prData['productCode'],
                        'prPID' => $prData['productID'],
                        'prPN' => $prData['productName'],
                        'lPID' => $prData['purchaseReturnLocationProductID'],
                        'prPP' => $prData['purchaseReturnProductPrice'],
                        'prPD' => $prData['purchaseReturnProductDiscount'],
                        'prRPQ' => $prData['purchaseReturnProductReturnedQty'],
                        'prSRPQ' => $prData['purchaseReturnSubProductReturnedQty'],
                        'prPT' => $prData['purchaseReturnProductTotal']);
                    if ($prData['productBatchID'] != NULL) {
                        $productBatches[$prData['productBatchID']] = array(
                            'bC' => $prData['productBatchCode'],
                            'bED' => $prData['productBatchExpiryDate'],
                            'bW' => $prData['productBatchWarrantyPeriod'],
                            'bMD' => $prData['productBatchManufactureDate'],
                            'bRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                    }
                    if ($prData['productSerialID'] != NULL) {
                        $productSerials[$prData['productSerialID']] = array(
                            'sC' => $prData['productSerialCode'],
                            'sW' => $prData['productSerialWarrantyPeriod'],
                            'sWT' => $row['productSerialWarrantyPeriodType'],
                            'sED' => $prData['productSerialExpireDate'],
                            'sBC' => $prData['productBatchCode'],
                            'sRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                    }
                    if ($prData['purchaseReturnTaxID'] != NULL) {
                        $productTaxes[$prData['purchaseReturnTaxID']] = array(
                            'pTN' => $prData['taxName'],
                            'pTP' => $prData['purchaseReturnTaxPrecentage'],
                            'pTA' => $prData['purchaseReturnTaxAmount']);
                    }
                    if ($prData['uomID'] != NULL) {
                        $productUoms[$prData['uomID']] = array(
                            'pUom' => $prData['uomAbbr'],
                            'pUomID' => $prData['uomID'],
                            'pUomCon' => $prData['productUomConversion']);
                    }
                    $prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['pT'] = $productTaxes;
                    $prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['bP'] = $productBatches;
                    $prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['sP'] = $productSerials;
                    $prProducts[$prData['purchaseReturnLocationProductID'].round($prData['purchaseReturnProductPrice'])]['pUom'] = $productUoms;
                }
                $tempPr['prProducts'] = $prProducts;
                $prDetails[$prID] = $tempPr;
            }

            $grnForm = new GrnForm();
            $grnForm->get('supplier')->setValue($grnDetails[$grnID]['gSN']);
            $grnForm->get('supplier')->setAttribute('disabled', TRUE);
            $grnForm->get('deliveryDate')->setValue($grnDetails[$grnID]['gD']);
            $grnForm->get('deliveryDate')->setAttribute('disabled', TRUE);
            $grnForm->get('grnNo')->setValue($grnDetails[$grnID]['gCd']);
            $grnForm->get('supplierReference')->setValue($grnDetails[$grnID]['gSR']);
            $grnForm->get('supplierReference')->setAttribute('disabled', TRUE);
            $grnForm->get('retrieveLocation')->setValue($grnDetails[$grnID]['gRL']);
            $grnForm->get('retrieveLocation')->setAttribute('disabled', TRUE);
            $grnForm->get('comment')->setValue($grnDetails[$grnID]['gC']);
            $grnForm->get('comment')->setAttribute('disabled', TRUE);
            $grnForm->get('discount')->setAttribute('id', 'grnDiscount');
            $grnSubTotal = number_format((floatval($grnDetails[$grnID]['gT']) - floatval($grnDetails[$grnID]['gDC'])), 2);
            $totalProTaxes = array();
            foreach ($grnDetails[$grnID]['gProducts'] as $product) {
                foreach ($product['pT'] as $tKey => $proTax) {
                    if (isset($totalProTaxes[$tKey])) {
                        $totalProTaxes[$tKey]['pTA'] = number_format((floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA'])), 2);
                    } else {
                        $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                    }
                }
            }

            foreach ($prDetails as $prIDKey => $prDataArray) {
                $prProductArray = isset($prDataArray['prProducts']) ? $prDataArray['prProducts'] : array();

//Update the product qtys of GRN if products were previously returned
                foreach ($grnDetails[$grnID]['gProducts'] as &$grnCheckProduct) {
                    $grnProductPurQty = $grnCheckProduct['gPQ'];
//check the current product was returned
                    if (array_key_exists($grnCheckProduct['grnIndexKey'], $prProductArray)) {
                        // $productKey = $grnCheckProduct['gPC'];
                        $productKey = $grnCheckProduct['grnIndexKey'];
                        $grnCheckProduct['gPQ'] = floatval($grnCheckProduct['gPQ']) - floatval($prProductArray[$productKey]['prRPQ']);

//if batch products available,update the returned qty details

                        if (!empty($grnCheckProduct['bP'])) {
                            foreach ($grnCheckProduct['bP'] as $batchKey => &$grnBatchPro) {
                                if (array_key_exists($batchKey, $prProductArray[$productKey]['bP'])) {
                                    if ($grnBatchPro['bQ'] == $prProductArray[$productKey]['bP'][$batchKey]['bRQ']) {
                                        unset($grnCheckProduct['bP'][$batchKey]);
                                    } else {
                                        $grnBatchPro['bQ'] = floatval($grnBatchPro['bQ']) - floatval($prProductArray[$productKey]['bP'][$batchKey]['bRQ']);
                                    }
                                }

                            }
                        }

//if serial products available,update the returned qty details
                        if (!empty($grnCheckProduct['sP'])) {
                            foreach ($grnCheckProduct['sP'] as $serialKey => &$serialPro) {
                                if (array_key_exists($serialKey, $prProductArray[$productKey]['sP'])) {
                                    unset($grnCheckProduct['sP'][$serialKey]);
                                }

                            }
                        }
                    }


//reset Total of the product
                    $grnCheckProduct['gPT'] = number_format((floatval($grnCheckProduct['gPT']) / floatval($grnProductPurQty)) * floatval($grnCheckProduct['gPQ']), 2, '.', '');

//reset the tax values of the product
                    foreach ($grnCheckProduct['pT'] as &$productTax) {
                        $productTax['pTA'] = number_format((floatval($productTax['pTA']) / floatval($grnProductPurQty)) * floatval($grnCheckProduct['gPQ']), 2, '.', '');
                    }

//if all items were returned, Unset the product from the list
                    if ($grnCheckProduct['gPQ'] == 0) {
                        unset($grnDetails[$grnID]['gProducts'][$productKey]);
                    }
                }
            }
//////////////////////////////////
            foreach ($grnDetails[$grnID]['gProducts'] as &$grnCheckProduct) {

                $grnProductPurQty = $grnCheckProduct['gPQ'];
                if (!empty($grnCheckProduct['bP'])) {
                    foreach ($grnCheckProduct['bP'] as $batchKey => &$grnBatchPro) {
                            $batchSerialcurrentQty += $grnBatchPro['bQ'];
                    }
                    $grnCheckProduct['gPQ'] = $batchSerialcurrentQty;
                    $batchSerialcurrentQty = 0;
                }
                if (!empty($grnCheckProduct['sP'])) {
                    foreach ($grnCheckProduct['sP'] as $serialKey => &$serialPro) {
                        $batchSerialcurrentQty ++;
                    }
                    $grnCheckProduct['gPQ'] = $batchSerialcurrentQty;

                    $batchSerialcurrentQty = 0;

                }

                //reset Total of the product
                $grnCheckProduct['gPT'] = number_format((floatval($grnCheckProduct['gPT']) / floatval($grnProductPurQty)) *
                    floatval($grnCheckProduct['gPQ']), 2, '.', '');

//reset the tax values of the product
                    foreach ($grnCheckProduct['pT'] as &$productTax) {
                        $productTax['pTA'] = number_format((floatval($productTax['pTA']) / floatval($grnProductPurQty)) *
                            floatval($grnCheckProduct['gPQ']), 2, '.', '');
                    }

            }
//////////////////////////////////


            $locationProducts = Array();
            $inactiveItems = Array();
            $locationID = $grnDetails[$grnID]['gRLID'];
            foreach ($grnDetails[$grnID]['gProducts'] as $gProducts) {
                $locationProducts[$gProducts['gPID']] = $this->getLocationProductDetails($locationID, $gProducts['gPID']);
                if ($locationProducts[$gProducts['gPID']] == null) {
                    $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($gProducts['gPID']);
                    $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                }
            }

            $inactiveItemFlag = false;
            $errorMsg = null;
            if (!empty($inactiveItems)) {
                $inactiveItemFlag = true;
                $errorItems = implode(",",$inactiveItems);        
                $errorMsg = $this->getMessage('ERR_GRN_ITM_INACTIVE_COPY', [$errorItems]);
            }

            $sourceDocumentTypeId=10;
            $sourceDocumentId=$getGrnID;
            $docrefData=$this->getDocumentReferenceBySourceData($sourceDocumentTypeId,$sourceDocumentId);

            $grnInfo = array(
                'grnData' => $grnDetails[$grnID],
                'grnSubTotal' => $grnSubTotal,
                'pTotalTax' => $totalProTaxes,
                'locationProducts' => $locationProducts,
                'docRefData'=>$docrefData,
                'errorMsg'=>$errorMsg,
                'inactiveItemFlag'=>$inactiveItemFlag,
            );

            $this->setLogMessage("Retrive ".$tempG['gCd']." GRN details.");
            $this->data = $grnInfo;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function returns grn details for the purchase return
     * @param POST grnID
     */
    public function getGrnDetailsForPrAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $grnID = $request->getPost('grnID');
            $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnID($grnID, FALSE);

            $grnDetails = array();
            $grnDetails = $this->getGrnDetailsForReturnAndPurchaseInvoice($grnData);
            //get details of purchase return if any returns were made to the current GRN
            $prRelatedDetails = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnDetailsByGrnID($grnID);
            $prID;
            $prDetails = array();

            foreach ($prRelatedDetails as $prData) {
                $tempPr = array();
                $prID = $prData['purchaseReturnID'];
                $tempPr['prID'] = $prData['purchaseReturnID'];
                $tempPr['prCd'] = $prData['purchaseReturnCode'];
                $tempPr['prSID'] = $prData['grnSupplierID'];
                $tempPr['prSName'] = $prData['supplierTitle'] . ' ' . $prData['supplierName'];
                $tempPr['prSR'] = $prData['grnSupplierReference'];
                $tempPr['prRLID'] = $prData['grnRetrieveLocation'];
                $tempPr['prRL'] = $prData['locationCode'] . ' - ' . $prData['locationName'];
                $tempPr['prD'] = $prData['purchaseReturnDate'];
                $tempPr['prT'] = $prData['purchaseReturnTotal'];
                $tempPr['prC'] = $prData['purchaseReturnComment'];
                $prProducts = (isset($prDetails[$prID]['prProducts'])) ? $prDetails[$prID]['prProducts'] : array();
                if ($prData['productID'] != NULL) {
                    $productTaxes = (isset($prProducts[$prData['purchaseReturnProductGrnProductID']]['pT'])) ?
                        $prProducts[$prData['purchaseReturnProductGrnProductID']]['pT'] : array();
                    $productBatches = (isset($prProducts[$prData['purchaseReturnProductGrnProductID']]['bP'])) ?
                        $prProducts[$prData['purchaseReturnProductGrnProductID']]['bP'] : array();
                    $productSerials = (isset($prProducts[$prData['purchaseReturnProductGrnProductID']]['sP'])) ?
                        $prProducts[$prData['purchaseReturnProductGrnProductID']]['sP'] : array();

                    $prProducts[$prData['purchaseReturnProductGrnProductID']] = array(
                        'prPC' => $prData['productCode'],
                        'prPID' => $prData['productID'],
                        'prPN' => $prData['productName'],
                        'lPID' => $prData['purchaseReturnLocationProductID'],
                        'prPP' => $prData['purchaseReturnProductPrice'],
                        'prPD' => $prData['purchaseReturnProductDiscount'],
                        'prRPQ' => $prData['purchaseReturnProductReturnedQty'],
                        'prSRPQ' => $prData['purchaseReturnSubProductReturnedQty'],
                        'prPT' => $prData['purchaseReturnProductTotal'],
                        'prGrnProdctID' => $prData['purchaseReturnProductGrnProductID']);
                    if ($prData['productBatchID'] != NULL) {
                        $productBatches[$prData['productBatchID']] = array(
                            'bC' => $prData['productBatchCode'],
                            'bED' => $prData['productBatchExpiryDate'],
                            'bW' => $prData['productBatchWarrantyPeriod'],
                            'bMD' => $prData['productBatchManufactureDate'],
                            'bRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                    }
                    if ($prData['productSerialID'] != NULL) {
                        $productSerials[$prData['productSerialID']] = array(
                            'sC' => $prData['productSerialCode'],
                            'sW' => $prData['productSerialWarrantyPeriod'],
                            'sWT' => $row['productSerialWarrantyPeriodType'],
                            'sED' => $prData['productSerialExpireDate'],
                            'sBC' => $prData['productBatchCode'],
                            'sRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                    }
                    if ($prData['purchaseReturnTaxID'] != NULL) {
                        $productTaxes[$prData['purchaseReturnTaxID']] = array(
                            'pTN' => $prData['taxName'],
                            'pTP' => $prData['purchaseReturnTaxPrecentage'],
                            'pTA' => $prData['purchaseReturnTaxAmount']);
                    }

                    $uoms = $this->getProductRelatedUomForUomPlugin($prData['productID']);

                    $prProducts[$prData['purchaseReturnProductGrnProductID']]['pT'] = $productTaxes;
                    $prProducts[$prData['purchaseReturnProductGrnProductID']]['bP'] = $productBatches;
                    $prProducts[$prData['purchaseReturnProductGrnProductID']]['sP'] = $productSerials;
                    $prProducts[$prData['purchaseReturnProductGrnProductID']]['pUom'] = $uoms;
                }
                $tempPr['prProducts'] = $prProducts;
                $prDetails[$prID] = $tempPr;
            }


//check and update qty for batch and serial products

            foreach ($grnDetails[$grnID]['gProducts'] as &$grnProduct) {
                $grnProductPurQty = $grnProduct['gPQ'];
                $productKey = $grnProduct['gPC'];
                if (isset($grnProduct['batchProduct']) && isset($grnProduct['serialProduct'])) {
                    $grnProduct['gPQ'] = count($grnProduct['sP']);
                } else if (isset($grnProduct['batchProduct'])) {
                    $totalBatchQty = 0;
                    if (!empty($grnProduct['bP'])) {
                        foreach ($grnProduct['bP'] as &$grnBatchPro) {
                            $totalBatchQty += $grnBatchPro['bQ'];
                        }
                        $grnProduct['gPQ'] = $totalBatchQty;
                    } else {
                        $grnProduct['gPQ'] = 0;
                    }
                } else if (isset($grnProduct['serialProduct'])) {
                    $grnProduct['gPQ'] = count($grnProduct['sP']);
                } else {
                    $grnProduct['gPQ'] = $grnProduct['gPQ'] - $grnProduct['gCPQ'];
                }

//reset Total of the product
                $grnProduct['gPT'] = number_format((floatval($grnProduct['gPT']) / floatval($grnProductPurQty)) * floatval($grnProduct['gPQ']), 2, '.', '');

//reset the tax values of the product
                foreach ($grnProduct['pT'] as &$productTax) {
                    $productTax['pTA'] = number_format((floatval($productTax['pTA']) / floatval($grnProductPurQty)) * floatval($grnProduct['gPQ']), 2, '.', '');
                }

                if ($grnProduct['gPQ'] == 0) {
                    unset($grnDetails[$grnID]['gProducts'][$productKey]);
                }
            }
            foreach ($prDetails as $prIDKey => $prDataArray) {
                $prProductArray = isset($prDataArray['prProducts']) ? $prDataArray['prProducts'] : array();
//Update the product qtys of GRN if products were previously returned
                ////////////////////////////
                foreach ($prProductArray as $key => $purchaseReturnSingleRow) {

                    if (array_key_exists($purchaseReturnSingleRow['prGrnProdctID'], $grnDetails[$grnID]['gProducts'])) {
                        $productKey = $grnCheckProduct['gPC'];
                        $grnProductKey = $purchaseReturnSingleRow['prGrnProdctID'];
                        $grnProPureQty = $grnDetails[$grnID]['gProducts'][$grnProductKey]['gPQ'];

                        if (!isset($grnDetails[$grnID]['gProducts'][$grnProductKey]['batchProduct']) &&
                                !isset($grnDetails[$grnID]['gProducts'][$grnProductKey]['serialProduct'])) {
                            $grnDetails[$grnID]['gProducts'][$grnProductKey]['gPQ'] = floatval($grnDetails[$grnID]['gProducts'][$grnProductKey]['gPQ']) - floatval($purchaseReturnSingleRow['prRPQ']);
//reset Total of the product
                            $grnDetails[$grnID]['gProducts'][$grnProductKey]['gPT'] =
                                number_format((floatval($grnDetails[$grnID]['gProducts'][$grnProductKey]['gPT']) / floatval($grnProPureQty)) * floatval($grnDetails[$grnID]['gProducts'][$grnProductKey]['gPQ']), 2, '.', '');

//reset the tax values of the product
                            foreach ($grnDetails[$grnID]['gProducts'][$grnProductKey]['pT'] as &$productTax) {
                                $productTax['pTA'] = number_format((floatval($productTax['pTA']) / floatval($grnProPureQty)) * floatval($grnDetails[$grnID]['gProducts'][$grnProductKey]['gPQ']), 2, '.', '');
                            }
                        }
                    }
                    //if all items were returned, Unset the product from the list
                    if ($grnDetails[$grnID]['gProducts'][$grnProductKey]['gPQ'] == 0) {
                        unset($grnDetails[$grnID]['gProducts'][$grnProductKey]);
                    }
                }
                ////////////////////////////
            }

            foreach ($grnDetails[$grnID]['gProducts'] as $key => $value) {
                if ($value['gPQ'] == 0) {
                    unset($grnDetails[$grnID]['gProducts'][$key]);
                }
            }

            $locationPro = Array();
            $inactiveItems = Array();
            $locationID = $grnDetails[$grnID]['gRLID'];
            foreach ($grnDetails[$grnID]['gProducts'] as $grnProducts) {
                $locationPro[$grnProducts['gPID']] = $this->getLocationProductDetails($locationID, $grnProducts['gPID']);
                if ($locationPro[$grnProducts['gPID']] == null) {
                    $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($grnProducts['gPID']);
                    $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                }
            }

            $inactiveItemFlag = false;
            $errorMsg = null;
            if (!empty($inactiveItems)) {
                $inactiveItemFlag = true;
                $errorItems = implode(",",$inactiveItems);        
                $errorMsg = $this->getMessage('ERR_GRN_ITM_INACTIVE_COPY', [$errorItems]);
            }

            if (empty($grnDetails[$grnID]['gProducts'])) {
                $msg = $this->getMessage('ERR_PURRETURN_CREATE');
            }
            $grnInfo = array(
                'grnData' => $grnDetails[$grnID],
                'errorMsg' => $errorMsg,
                'inactiveItemFlag' => $inactiveItemFlag
            );

            $this->data = $grnInfo;
            $this->msg = $msg;

            $this->setLogMessage("Retrive GRN ".$grnDetails[$grnID]['gCd']." details.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param type $poID
     * This function update the po states if all items were copied to the grn
     */
    public function checkAndUpdatePoStatus($poID)
    {
        $poProducts = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getUnCopiedPoProducts($poID);
        if (empty($poProducts)) {
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Inventory\Model\PurchaseOrderTable')->updatePoStatus($poID, $closeStatusID);
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param type $grnID
     * This function update the grn status
     */
    public function changeGrnStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $grnID = $request->getPost('grnID');
            $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnDetailsByGrnId($grnID);
            $previouseStatusID = $grnData->status;
            $grnCode = $grnData->grnCode;

            $statusList = $this->getStatusesList();
            $previousStatusCode = $statusList[$previouseStatusID];

            $newStatusCode = $request->getPost('statusCode');
            $newStatusID = $this->getStatusID($newStatusCode);
            $result = $this->CommonTable('Inventory\Model\GrnTable')->updateGrnStatus($grnID, $newStatusID);
            if ($result) {
                $this->setLogMessage($grnCode." GRN status change ".$previousStatusCode.' to '.$newStatusCode.'.');
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_GRN_STATUS');
                $this->flashMessenger()->addMessage($this->msg);
            } else {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_GRN_STATUS');
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param string toEmail,string subject,string body,string documentID
     * This function send A email to the passed address
     */
    public function sendGrnEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Goods Received Note';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
//            $this->msg = $this->getMessage('SUCC_GRN_EMAIL', array($documentType));
            $this->msg = $this->getMessage('SUCC_PAY_EMAIL');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_GRN_EMAIL', array($documentType));
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search open grn for drop down
     * @return type
     */
    public function searchOpenGrnForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $supplierID = $_GET['supplierID'];
            $openGrns = $this->CommonTable('Inventory\Model\GrnTable')->searchGrnForDropdown($searchKey, $this->getStatusID('open'), $locationID, $supplierID);
            $openGrnList = array();
            foreach ($openGrns as $openGrn) {
                $temp['value'] = $openGrn['grnID'];
                $temp['text'] = $openGrn['grnCode'] . '-' . $openGrn['locationCode'];
                $openGrnList[] = $temp;
            }

            $this->setLogMessage("Retrive open GRN list for dropdown.");
            $this->data = array('list' => $openGrnList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search all grn for drop down
     * @return type
     */
    public function searchAllGrnForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            if ($locationID == '' || $locationID == null) {
                $locationID = $this->getActiveAllLocationsIds();
            }
            $allGrns = $this->CommonTable('Inventory\Model\GrnTable')->searchGrnForDropdown($searchKey, false, $locationID);
            $allGrnList = array();
            foreach ($allGrns as $allGrn) {
                $temp['value'] = $allGrn['grnID'];
                $temp['text'] = $allGrn['grnCode'] . '-' . $allGrn['locationCode'];
                $allGrnList[] = $temp;
            }

            $this->setLogMessage("Retrive GRN list for dropdown.");
            $this->data = array('list' => $allGrnList);
            return $this->JSONRespond();
        }
    }

    /**
     * Get Grns By search list
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getGrnsBySearchAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
            if ($request->getPost('GRNID') == '' && $request->getPost('supplierID') == '') {
                $userActiveLocation = $this->user_session->userActiveLocation;
                $this->paginator = $this->CommonTable('Inventory\Model\GrnTable')->getGrns(true, $userActiveLocation['locationID']);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $grnListView = new ViewModel(array('grnList' => $this->paginator, 'statuses' => $this->getStatusesList(), 'dateFormat' => $dateFormat));
            } else {
                $grnList = $this->CommonTable('Inventory\Model\GrnTable')->getGrnsBySearch($request->getPost('GRNID'), $request->getPost('supplierID'));
                $grnListView = new ViewModel(array('grnList' => $grnList, 'statuses' => $this->getStatusesList(), 'paginated' => false, 'dateFormat' => $dateFormat));
            }

            $grnListView->setTemplate('inventory/grn/grn-list');
            $this->html = $grnListView;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * Get Grns By search list
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getGrnsBySerialIdSearchAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
            $grnList = $this->CommonTable('Inventory\Model\GrnTable')->getGrnsBySerialCodeSearch($request->getPost('searchString'));
            $grnListView = new ViewModel(array('grnList' => $grnList, 'statuses' => $this->getStatusesList(), 'paginated' => false, 'dateFormat' => $dateFormat));

            $grnListView->setTemplate('inventory/grn/grn-list');
            $this->html = $grnListView;
            return $this->JSONRespondHtml();
        }
    }

    public function updatePoProductCopiedQty($locationPID, $poId, $productQty)
    {
        $oldPoProductData = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getPoProduct($poId, $locationPID);
        $recordedQty = $oldPoProductData['purchaseOrderProductQuantity'];
        $currentUpdatedQty = $oldPoProductData['purchaseOrderProductCopiedQuantity'];
        $updatingQty = floatval($currentUpdatedQty) + floatval($productQty);
        $allCopied = FALSE;
        if (floatval($updatingQty) >= floatval($recordedQty)) {
            $allCopied = TRUE;
        }

        $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->updateCopiedPoProductQty($locationPID, $poId, $updatingQty);
        if ($allCopied) {
            $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->updateCopiedPoProducts($locationPID, $poId);
        }
    }

    public function getBatchNumbersForTypeaheadAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $batchCodeSearchKey = $request->getPost('query');
        $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->searchBatchProductsForDropDown($batchCodeSearchKey);

        $batchCodeList = [];
        foreach ($batchDetails as $batchData) {
            $batchCodeList[] = $batchData['productBatchCode'];
        }

        $this->data = $batchCodeList;
        $this->status = true;
        return $this->JSONRespond();

    }

    public function getManDateAndExpDateForBatchItemAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $batchCode = $request->getPost('batchCode');
        $batchDetais = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProductDetailsByCode($batchCode)->current();

        $batchData = [];
        $batchData['manDate'] = $this->convertDateToUserFormat($batchDetais['productBatchManufactureDate']);
        $batchData['expDate'] = $this->convertDateToUserFormat($batchDetais['productBatchExpiryDate']);

        if(!count($batchData) > 0){
            $this->status = false;
            return $this->JSONRespond();
        }

        $this->data = $batchData;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function grnItemImportAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $lineSet = [];
        $headerData = [];
        $rowData = [];
        $prototype = [];
        $dataRow = [];
        $lineByLineRecords = [];

        $dataSet = $request->getPost('dataSet');
        $delimiter = $request->getPost('productDelimiter');
        $dataType = $request->getPost('dataType');
        $batchSerialType = $request->getPost('batchSerialType');
        $proHeader = $request->getPost('productHeader');

        if($dataSet){
            $lineRecords = explode("\n", $dataSet);
            $headerData = explode($delimiter, $lineRecords[0]);
            $rowData = explode($delimiter, $lineRecords[1]);

            foreach (array_filter($lineRecords) as $key => $value) {
                if(sizeof($headerData) != sizeof(array_filter(explode($delimiter, $value)))){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_GRN_ITEM_LIST_NOT_VALID');
                    return $this->JSONRespond();
                }else {
                    $dataRow[$key] = explode($delimiter, $lineRecords[$key]);
                }
            }

            for($i = 0; $i < sizeof($lineRecords); $i++){
                for($j = 0; $j < sizeof($dataRow[$i]); $j++){
                    $lineByLineRecords[$j][] = $dataRow[$i][$j];
                }
            }

            $returnData = array(
                'headerFlag' => $proHeader,
                'dataSet' => $lineByLineRecords,
                'hiddenDataSet' => base64_encode(serialize($lineByLineRecords)),
                );

            $this->status = true;
            $this->data = $returnData;
            return $this->JSONRespond();

        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GRN_REQ_DATA_NOT_SET');
            return $this->JSONRespond();
        }

    }

    public function loadImportedDataToGrnCreateAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
     
        $dataSet = $request->getPost('dataArray');
        $batchSerialType = $request->getPost('batchSerialType');
        $productDelimiter = $request->getPost('productDelimiter');
        $dataType = $request->getPost('dataType');
        $fileData = $request->getPost('dataSet');
        $header = $request->getPost('productHeader');
        $uomMapArray = $request->getPost('uomMapArray');
        $discTypeArray = $request->getPost('discTypeArray');
        
        $batchData = [];
        $serialData = [];
        $fileDetails = unserialize(base64_decode($fileData));
        $productDetails = [];
        $rowDetails = [];
        $inValidUploadCount = 0;
        $invalidDetails = [];

        if($batchSerialType == 'batchItem'){
            $rowCounter = 0;
            foreach ($fileDetails[$dataSet['itemCode']] as $key => $value) {
                $rowCounter++;
                if($header == 'true' && $key == 0){
                    continue;
                } else{
                    $productID = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($value, false, true);
                    if($productID){
                        $locationID = $request->getPost('locationID');
                        $locationProduct = $this->getLocationProductDetails($locationID, $productID['productID']);
                        $productDetails['locProduct'] = $locationProduct;

                        if (!empty($fileDetails[$dataSet['exDate']][$key])) {
                            if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$fileDetails[$dataSet['exDate']][$key])) {
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_GRN_IMPORT_DATE_FORMAT');
                                return $this->JSONRespond();
                            }
                        }

                        if (!empty($fileDetails[$dataSet['manDate']][$key])) {
                            if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$fileDetails[$dataSet['manDate']][$key])) {
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_GRN_IMPORT_DATE_MANU_FORMAT');
                                return $this->JSONRespond();
                            }
                        }

                        $batchData = array(
                            'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                            'newBQty' => $fileDetails[$dataSet['quantity']][$key],
                            'newBMDate' => (!empty($fileDetails[$dataSet['manDate']][$key])) ?$this->convertDateToGivenFormat($fileDetails[$dataSet['manDate']][$key], 'Y-m-d') : null,
                            'newBEDate' => (!empty($fileDetails[$dataSet['exDate']][$key])) ? $this->convertDateToGivenFormat($fileDetails[$dataSet['exDate']][$key], 'Y-m-d') : null,
                            'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                            'newBPrice' => $fileDetails[$dataSet['batchSalePrice']][$key],
                        );
                        $productDetails['batchData'][$fileDetails[$dataSet['batchCode']][$key]] = $batchData;
                        $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key];
                        $productDetails['qnty'] = $fileDetails[$dataSet['quantity']][$key];
                        $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                        $productDetails['discValue'] = $fileDetails[$dataSet['discValue']][$key];
                        $productDetails['discType'] = $discTypeArray[$fileDetails[$dataSet['discType']][$key]];
                        $productDetails['serialCode'] = null;

                        $rowDetails[] = $productDetails;
                        $productDetails = [];
                    } else {
                        $inValidUploadCount++;
                        $invalidDetails[] = $rowCounter;
                    }


                }
            }

        } else if($batchSerialType == 'serialItem'){
            $rowCounter = 0;
            foreach ($fileDetails[$dataSet['itemCode']] as $key => $value) {
                $rowCounter++;
                if($header == 'true' && $key == 0){
                    continue;
                } else{
                    $currentProduct = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($value, true, false);

                    if (!empty($fileDetails[$dataSet['exDate']][$key])) {
                        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$fileDetails[$dataSet['exDate']][$key])) {
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_GRN_IMPORT_DATE_FORMAT');
                            return $this->JSONRespond();
                        }
                    }

                    if(array_key_exists($value, $rowDetails)){
                        $validateSerialFlag = $this->validateSerialItems($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'], $value, null, null);
                        if($validateSerialFlag){
                            $rowDetails[$value]['qnty']++;
                            $serialData = array(
                                'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                            );
                            $rowDetails[$value]['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                        } else {
                            $inValidUploadCount++;
                            $invalidDetails[] = $rowCounter;
                        }

                    } else{
                        if($currentProduct){
                            $locationID = $request->getPost('locationID');
                            $locationProduct = $this->getLocationProductDetails($locationID, $currentProduct['productID']);
                            $validateSerialFlag = $this->validateSerialItems($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'],$value, null, 1);

                            if($validateSerialFlag){
                                $productDetails['locProduct'] = $locationProduct;

                                $serialData = array(
                                    'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                    'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                    'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                    'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                );
                                $productDetails['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                                $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key];
                                // $productDetails['qnty'] = (!empty($fileDetails[$dataSet['quantity']][$key])) ? $fileDetails[$dataSet['quantity']][$key] : 1;
                                $productDetails['qnty'] = 1;
                                $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                                $productDetails['discValue'] = $fileDetails[$dataSet['discValue']][$key];
                                $productDetails['discType'] = $discTypeArray[$fileDetails[$dataSet['discType']][$key]];
                                $productDetails['batchData'] = null;

                                $rowDetails[$value] = $productDetails;
                                $productDetails = [];
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }
                        } else {
                            $inValidUploadCount++;
                            $invalidDetails[] = $rowCounter;
                        }
                    }

                }

            }

        } else if ($batchSerialType == 'batchSerialItem'){
            $countKey = 0;
            $rowCounter = 0;
            foreach ($fileDetails[$dataSet['itemCode']] as $key => $value) {
                $rowCounter++;
                if($header == 'true' && $key == 0){
                    continue;
                } else{
                    $currentProduct = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($value, true, true);

                    if(array_key_exists($value, $rowDetails) && array_key_exists($fileDetails[$dataSet['batchCode']][$key], $rowDetails[$value][$countKey]['batchData'])){
                        $validateSerialFlag = $this->validateSerialItems($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'], $value, $fileDetails[$dataSet['batchCode']][$key], null);
                        if($validateSerialFlag){
                            $rowDetails[$value][$countKey]['batchData'][$fileDetails[$dataSet['batchCode']][$key]]['newBQty']++;
                            $rowDetails[$value][$countKey]['qnty']++;
                            $serialData = array(
                                'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                            );
                            $rowDetails[$value][$countKey]['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                        } else {
                            $inValidUploadCount++;
                            $invalidDetails[] = $rowCounter;
                        }
                    } else{
                        if($currentProduct){
                            $locationID = $request->getPost('locationID');
                            $locationProduct = $this->getLocationProductDetails($locationID, $currentProduct['productID']);
                            $validateSerialFlag = $this->validateSerialItems($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'], $value, $fileDetails[$dataSet['batchCode']][$key], 1);
                            if($validateSerialFlag){
                                $productDetails['locProduct'] = $locationProduct;

                                $batchData = array(
                                    'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                    'newBQty' => (!empty($fileDetails[$dataSet['quantity']][$key])) ? $fileDetails[$dataSet['quantity']][$key] : 1,
                                    'newBMDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['manDate']][$key]),
                                    'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                    'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                    'newBPrice' => $fileDetails[$dataSet['unitPrice']][$key],
                                );

                                $serialData = array(
                                    'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                    'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                    'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                    'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                );
                                $productDetails['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                                $productDetails['batchData'][$fileDetails[$dataSet['batchCode']][$key]] = $batchData;
                                $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key];
                                $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                                $productDetails['discValue'] = $fileDetails[$dataSet['discValue']][$key];
                                $productDetails['discType'] = $discTypeArray[$fileDetails[$dataSet['discType']][$key]];

                                // $productDetails['qnty'] = (!empty($fileDetails[$dataSet['quantity']][$key])) ? $fileDetails[$dataSet['quantity']][$key] : 1;
                                $productDetails['qnty'] = 1;

                                $countKey++;
                                $rowDetails[$value][$countKey] = $productDetails;
                                $productDetails = [];
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }
                        } else{
                            $inValidUploadCount++;
                            $invalidDetails[] = $rowCounter;
                        }

                    }


                }

            }

        } else if($batchSerialType == 'normalItem'){
            $rowCounter = 0;
            foreach ($fileDetails[$dataSet['itemCode']] as $key => $value) {
                $rowCounter++;
                if($header == 'true' && $key == 0){
                    continue;
                } else{
                    $productID = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($value, false, false);
                    if($productID){
                        $locationID = $request->getPost('locationID');
                        $locationProduct = $this->getLocationProductDetails($locationID, $productID['productID']);
                        $productDetails['locProduct'] = $locationProduct;
                        $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key];
                        $productDetails['qnty'] = $fileDetails[$dataSet['quantity']][$key];
                        $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                        $productDetails['discValue'] = $fileDetails[$dataSet['discValue']][$key];
                        $productDetails['discType'] = $discTypeArray[$fileDetails[$dataSet['discType']][$key]];
                        $productDetails['serialCode'] = null;
                        $productDetails['batchData'] = null;

                        $rowDetails[] = $productDetails;
                        $productDetails = [];
                    } else {
                        $inValidUploadCount++;
                        $invalidDetails[] = $rowCounter;
                    }


                }
            }
        }
            
        if($inValidUploadCount > 0){
            $invalidDataSet = array_unique($invalidDetails);
            if($inValidUploadCount == 1){
                $errorMsg = $inValidUploadCount.' Record with row number '.implode(", ", $invalidDataSet).' is';
            } else {
                $errorMsg = $inValidUploadCount.' Records with row numbers '.implode(", ", $invalidDataSet).' are';
            }
            $this->msg = $this->getMessage('ERR_GRN_INVALID_DATA_ROWS', [$errorMsg]);
            
        }
        $this->status = true;
        $this->data = $rowDetails;
        return $this->JSONRespond();

    }

    public function uomMappingAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }

        $dataSet = unserialize(base64_decode($request->getPost('dataArray')));
        $id = explode("_", $request->getPost('id'));

        $uomDataSet = array_unique($dataSet[$id[1]]);
        $currentUom = [];
        $systemUomList = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        foreach ($systemUomList as $key => $value) {
            $currentUom[] = array(
                'uomName' => $value->uomName,
                'uomID' => $value->uomID,
                );
        }
        $returnDataSet = array(
            'systemUomList' => $currentUom,
            'uploadedUomList' => $uomDataSet,
            );

        $this->data = $returnDataSet;
        $this->status = true;
        return $this->JSONRespond();

    }

    public function validateSerialItems($serialCode, $uploadedData, $productID, $itemCode, $batchCode = null, $stepOne = null)
    {
        if($productID){
            if($batchCode){
                $batchSerialDetails = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialDetailsByProductID($productID);
                foreach ($batchSerialDetails as $key => $bSValue) {
                    if($bSValue['productSerialCode'] == $serialCode && $bSValue['productBatchCode'] == $batchCode){
                        return false;
                    }
                }

                if(!$stepOne){
                    foreach ($uploadedData[$itemCode] as $key => $value) {
                        foreach ($value['serilaData'] as $key => $serialValue) {
                            if($serialValue['newSCode'] == $serialCode && $serialValue['newBCode'] == $batchCode){
                                return false;
                            }
                        }
                    }
                }


            } else {
                $batchSerialDetails = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialDetailsByProductID($productID);
                    foreach ($batchSerialDetails as $key => $bSValue) {
                        if($bSValue['productSerialCode'] == $serialCode){
                            return false;
                    }
                }

                if(!$stepOne) {
                    foreach ($uploadedData[$itemCode]['serilaData'] as $key => $value) {
                        if($value['newSCode'] == $serialCode){
                            return false;
                        }
                    }
                }
            }
            return true;
        } else {
            return false;
        }

    }
    /**
    ** set all grn details into the array for copy option of purchaseInvoice and PurchaseReturns
    **
    **/
    public function getGrnDetailsForReturnAndPurchaseInvoice($grnData)
    {
        $grnDetails = array();
        $counter = 0;
        foreach ($grnData as $key => $row) {
            $counter++;
            $tempG = array();
            $tempG['gID'] = $row['grnID'];
            $tempG['gCd'] = $row['grnCode'];
            $tempG['gSID'] = $row['grnSupplierID'];
            $tempG['gSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
            $tempG['gSR'] = $row['grnSupplierReference'];
            $tempG['gRLID'] = $row['grnRetrieveLocation'];
            $tempG['gRL'] = $row['locationCode'] . '-' . $row['locationName'];
            $tempG['gD'] = $this->convertDateToUserFormat($row['grnDate']);
            $tempG['gDC'] = $row['grnDeliveryCharge'];
            $tempG['gT'] = $row['grnTotal'];
            $tempG['gC'] = $row['grnComment'];
            $tempG['lPID'] = $row['locationProductID'];
            $grnProducts = (isset($grnDetails[$row['grnID']]['gProducts'])) ?
                $grnDetails[$row['grnID']]['gProducts'] : array();
            ///////////////////////////////
            if($counter == 1){
                //this condition use to set indexkey value for first time when run this main foreach loop
                $indexKey = $row['grnProductID'];

            }
            if(isset($grnDetails[$row['grnID']]['gProducts']['uniqKey']) &&
                    $grnDetails[$row['grnID']]['gProducts']['uniqKey'] != $row['locationProductID']){
                $indexKey = $row['grnProductID'];
            } else if(isset($grnDetails[$row['grnID']]['gProducts']['uniqKey']) &&
                    $grnDetails[$row['grnID']]['gProducts']['uniqKey'] == $row['locationProductID'] &&
                        $grnProducts[$indexKey]['gPP'] != $row['grnProductPrice']){
                $indexKey = $row['grnProductID'];
            }

            ///////////////////////////////
            if ($row['grnProductID'] != NULL) {
                $productTaxes = (isset($grnProducts[$indexKey]['pT'])) ? $grnProducts[$indexKey]['pT'] : array();
                $productBatches = (isset($grnProducts[$indexKey]['bP'])) ? $grnProducts[$indexKey]['bP'] : array();
                $productSerials = (isset($grnProducts[$indexKey]['sP'])) ? $grnProducts[$indexKey]['sP'] : array();
                $row['grnProductTotalCopiedQuantity'] = (!is_null($row['grnProductTotalCopiedQuantity'])) ? $row['grnProductTotalCopiedQuantity']: 0;

                $grnProducts[$indexKey] = array(
                    'gPC' => $row['productCode'],
                    'gPID' => $row['productID'],
                    'gPN' => $row['productName'],
                    'lPID' => $row['locationProductID'],
                    'gPP' => $row['grnProductPrice'],
                    'gPD' => $row['grnProductDiscount'],
                    'gPQ' => $row['grnProductTotalQty'],
                    'gCPQ' => $row['grnProductTotalCopiedQuantity'],
                    'gLPQ' => $row['locationProductQuantity'],
                    'gPT' => $row['grnProductTotal'],
                    'productType' => $row['productTypeID'],
                    'grnProductID' => $indexKey);
//Actual batch qty is taken
                if ($row['productBatchID'] != NULL) {
                    $grnProducts[$indexKey]['batchProduct'] = TRUE;
                    if ($row['productBatchQuantity'] > 0) {
                        $productBatches[$row['productBatchID']] = array(
                            'bC' => $row['productBatchCode'],
                            'bED' => $row['productBatchExpiryDate'],
                            'bW' => $row['productBatchWarrantyPeriod'],
                            'bMD' => $row['productBatchManufactureDate'],
                            'bQ' => $row['productBatchQuantity'] - $row['grnProductCopiedQuantity']);
                    }
                }
                if ($row['productSerialID'] != NULL) {
                    $grnProducts[$indexKey]['serialProduct'] = TRUE;
//check serial product is sold or returned
                    if ($row['productSerialReturned'] != 1 && $row['productSerialSold'] != 1 && $row['locationProductID'] == $row['serialLocationId'] && $row['copied'] == "0") {
                        $productSerials[$row['productSerialID']] = array(
                            'sC' => $row['productSerialCode'],
                            'sW' => $row['productSerialWarrantyPeriod'],
                            'sWT' => $row['productSerialWarrantyPeriodType'],
                            'sED' => $row['productSerialExpireDate'],
                            'sGrnProductID' => $row['grnProductID'],
                            'sBC' => $row['productBatchCode']);
                    }
                }
                if ($row['grnTaxID'] != NULL) {
                    $productTaxes[$row['grnTaxID']] = array(
                        'pTN' => $row['taxName'],
                        'pTP' => $row['grnTaxPrecentage'],
                        'pTA' => $row['grnTaxAmount']);
                }

                $uoms = $this->getProductRelatedUomForUomPlugin($row['productID']);

                $grnProducts[$indexKey]['pT'] = $productTaxes;
                $grnProducts[$indexKey]['bP'] = $productBatches;
                $grnProducts[$indexKey]['sP'] = $productSerials;
                $grnProducts[$indexKey]['pUom'] = $uoms;

            }
            $tempG['gProducts'] = $grnProducts;
            $tempG['gProducts']['uniqKey'] = $row['locationProductID'];
            $grnDetails[$row['grnID']] = $tempG;

        }
        unset($grnDetails[$row['grnID']]['gProducts']['uniqKey']);

        return $grnDetails;

    }

    public function discountTypeMappingAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }

        $dataSet = unserialize(base64_decode($request->getPost('dataArray')));
        $id = explode("_", $request->getPost('id'));

        $discTypeDataSet = array_unique($dataSet[$id[1]]);
        $currentDiscType[0] = array(
                'DiskType' => 'percentage',
                'DiscID' => 1,
                );
        $currentDiscType[1] = array(
                'DiskType' => 'value',
                'DiscID' => 2,
                );

        $returnDataSet = array(
            'systemDataList' => $currentDiscType,
            'uploadedDataList' => $discTypeDataSet,
            );

        $this->data = $returnDataSet;
        $this->status = true;
        return $this->JSONRespond();

    }

    public function updateGrnAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }

        $dataSet = $request->getPost();

        $updateFlag = false;
        //check grn status is closed or not
        $grnStatus = $this->CommonTable('Inventory\Model\GrnTable')->grnDetailsByGrnId($dataSet['grnID']);
        if ($grnStatus['status'] == "4") {
            $this->status = false;
            $this->msg = "The requested GRN is closed, Therefore can not edit.";
            return $this->JSONRespond();
        }
        
        $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnID($dataSet['grnID']);
        $grnArr[] = $this->getGrnProductSet($grnData);

        foreach ($grnArr as $value) {
            $sizeOfOldGrnProductArray = sizeof($value['gProducts']);
            $oldGrnTotal = $value['gT'];

            if ($sizeOfOldGrnProductArray != sizeof(json_decode($dataSet['pr']))) {
                $updateFlag = true;
            }

            if (floatval($dataSet['fT']) != floatval($oldGrnTotal)) {
                $updateFlag = true;
            }
        }
        //before do any transaction , need to check items that related to this grn are sold or not
        $createdDataSet = $this->createDataSetForReverse($dataSet['grnID']);

        //reverse grn Items
        $this->beginTransaction();
        if ($updateFlag) {
            $reverseStatusDetails = $this->reverseGrnProducts($dataSet['grnID'], $createdDataSet);

            if(!$reverseStatusDetails['status']){
                $this->rollback();
                if($reverseStatusDetails['data'] == 'transfer'){
                    $this->status = false;
                    $this->msg = $reverseStatusDetails['msg'];
                    return $this->JSONRespond();
                }
                
                // if this product in use then we need to update existing grn records
               $returnValue =  $this->updateSoldGrn($dataSet);
          
               if(!$returnValue['status']){
                    $this->status = false;
                    $this->msg = $returnValue['msg'];
                    return $this->JSONRespond();
               }

               //update journal entries
               $returnJEState = $this->createDataSetForJournalEntry($dataSet, $returnValue['data'], $dataSet['ignoreBudgetLimit']);
               if(!$returnJEState['status']){
                    $this->status = false;
                    $this->msg = $returnJEState['msg'];
                    $this->data = $returnJEState['data'];
                    return $this->JSONRespond();
               }

                $this->status = true;
                $this->msg = $this->getMessage('SUCC_GRN_UPDATE');
                return $this->JSONRespond();

            }
            //reverse journal entries
            if($this->useAccounting){
                $journalEntryReverseStatus = $this->journalEntryreverseByGrn($dataSet['grnID'], $dataSet['gC'], $dataSet['rL'], $dataSet['dimensionData'], $dataSet['ignoreBudgetLimit']);
                if(!$journalEntryReverseStatus['status']){
                    $this->rollback();
                    $this->status = false;
                    if ($journalEntryReverseStatus['data'] == "NotifyBudgetLimit" || $journalEntryReverseStatus['data'] == "BlockBudgetLimit") {
                        $this->msg = $journalEntryReverseStatus['msg'];                        
                    } else {
                        $this->msg = $this->getMessage('ERR_UPDATE_ACCOUNTS');
                    }
                    $this->data = $journalEntryReverseStatus['data'];                        
                    return $this->JSONRespond();
                }
            }

            // check whether its come from po, then update po and grnPo
            $grnPurchaseOrderDataSet = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnAllBatchSerialProductDetailsByGrnId($dataSet['grnID']);

            $poID = null;
            foreach ($grnPurchaseOrderDataSet as $key => $value) {
                if($value['grnProductDocumentTypeId'] == 9 && $value['grnProductDocumentId']){
                    $poID = $value['grnProductDocumentId'];
                    //get po product details
                    $poProductDetails = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')
                        ->getPoProduct($value['grnProductDocumentId'], $value['locationProductID']);

                    //update purchaseOrder product table
                        $currentCopedQty = floatval($poProductDetails['purchaseOrderProductCopiedQuantity']) - floatval($value['grnProductQuantity']);
                    $updatedPoDataSet = array(
                        'purchaseOrderProductID' => $poProductDetails['purchaseOrderProductID'],
                        'copied' => 0,
                        'purchaseOrderProductCopiedQuantity' => $currentCopedQty,
                        );
                    $updatePoProduct = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')
                        ->updatePoCopiedStateAndCopiedQty($updatedPoDataSet);
                    if(!$updatePoProduct){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_UPDATE_PO_PRODUCT');
                        return $this->JSONRespond();
                    }
                }
            }
            if($poID){
                //update purchaseOrder table
                $statID = 3; // open
                $updatePo = $this->CommonTable('Inventory\Model\PurchaseOrderTable')
                    ->updatePoStatus($poID, $statID);
                $dataSet['startByPoID'] = $poID;
            }

            //update existing GRN as a replaced
            $grnStatID = 10; // replaced
            $updateGRn = $this->CommonTable('Inventory\Model\GrnTable')
                ->updateGrnStatus($dataSet['grnID'], $grnStatID);

            //add new records to the grn
            $createdNewGrnStatus = $this->updateExistingGrnRecord($dataSet, $dataSet['grnID'], $updateFlag, $dataSet['ignoreBudgetLimit']);
            
            if(!$createdNewGrnStatus['status']){
                $this->rollback();
                $this->status = false;
                $this->msg = $createdNewGrnStatus['msg'];
                $this->data = $createdNewGrnStatus['data'];
                return $this->JSONRespond();
            } else {
                $this->commit();

                $changeArray = $this->getGRNChageDetails($dataSet);

                //set log details
                $previousData = json_encode($changeArray['previousData']);
                $newData = json_encode($changeArray['newData']);

                $this->setLogDetailsArray($previousData,$newData);
                $this->setLogMessage("GRN ".$changeArray['previousData']['GRN Code']." is updated with product change.");

                $this->status = true;
                $this->data = $createdNewGrnStatus['data'];
                $this->msg = $createdNewGrnStatus['msg'];
                return $this->JSONRespond();
            }

        } else {
            //update only grn comment
            $this->beginTransaction();

            $grnData = array(
                'grnComment' => $dataSet['cm'],
                'grnSupplierID' => $dataSet['sID'],
                'grnDate' => $this->convertDateToStandardFormat($dataSet['dD']),
            );

            $grnDate = $grnData['grnDate'];

            //reverse journal entries
            if($this->useAccounting){

                /**
                * get journal entry details by given id and document id = 10
                * because grn document id is 10
                */

                $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();

                $checkFiscalPeriod = false;
                if(count($fiscalPeriod) > 0){
                    foreach ($fiscalPeriod as $key => $value) {
                        if($value['fiscalPeriodStatusID'] == 14){
                            if(($value['fiscalPeriodStartDate'] <= $grnDate && $grnDate <= $value['fiscalPeriodEndDate'])){
                                $checkFiscalPeriod = true;
                            }
                        }
                    }
                }
                if (!$checkFiscalPeriod) {
                    $this->status = false;
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_DATE_IS_NOTIN_FISCALPERIOD_RANGE');
                    return $this->JSONRespond();
                }

                $jeData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('10', $dataSet['grnID']);

                $jeNewData['journalEntryDate'] = $this->convertDateToStandardFormat($dataSet['dD']);


                $jeUpdated = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntry($jeNewData, $jeData['journalEntryID']);

                if ($grnStatus['grnSupplierID'] != $dataSet['sID']) {
                    $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($grnStatus['grnSupplierID']);
                    if(empty($supplierData->supplierGrnClearingAccountID)){
                        $this->status = false;
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_GRN_SUPPLIER_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                        return $this->JSONRespond();
                    }
                    $GRNClearingAccountID = $supplierData->supplierGrnClearingAccountID;


                    $newSupplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($dataSet['sID']);
                    if(empty($newSupplierData->supplierGrnClearingAccountID)){
                        $this->status = false;
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_GRN_SUPPLIER_ACCOUNT', array($newSupplierData->supplierName.' - '.$newSupplierData->supplierCode));
                        return $this->JSONRespond();
                    }
                    $GRNClearingAccountIDNew = $newSupplierData->supplierGrnClearingAccountID;

                    $updateJEData['financeAccountsID'] = $GRNClearingAccountIDNew;
                    $jeUpdatedNew = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryAccountByJeAndFinanceAccount($updateJEData, $GRNClearingAccountID, $jeData['journalEntryID']);
                }
            }
            $row = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnID($dataSet['grnID'], false, false, true)->current();
            $previousData = [];
            $previousData['GRN Code'] = $row['grnCode'];
            $previousData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
            $previousData['GRN Supplier Reference'] = $row['grnSupplierReference'];
            $previousData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
            $previousData['GRN Date'] = $this->convertDateToUserFormat($row['grnDate']);
            $previousData['GRN Delivery Charge'] = $row['grnDeliveryCharge'];
            $previousData['GRN Total'] = $row['grnTotal'];
            $previousData['GRN Comment'] = $row['grnComment'];

            $updateGrnTable = $this->CommonTable('Inventory\Model\GrnTable')->updateGrnRecord($dataSet['grnID'],$grnData);
            if(!$updateGrnTable || !$jeUpdated){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_GRN_UPDATE');
                return $this->JSONRespond();
            }

            $documentReference = $dataSet['documentReference'];
            $grnID = $dataSet['grnID'];

            $this->CommonTable('Core\model\DocumentReferenceTable')->deleteDocumentReference(10, $grnID);
            if (isset($documentReference)) {
                foreach ($documentReference as $key => $val) {
                    foreach ($val as $key1 => $value) {
                        $data = array(
                            'sourceDocumentTypeId' => 10,
                            'sourceDocumentId' => $grnID,
                            'referenceDocumentTypeId' => $key,
                            'referenceDocumentId' => $value,
                        );
                        $documentReference = new DocumentReference();
                        $documentReference->exchangeArray($data);
                        $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                    }
                }
            }

            $this->commit();

            $changeArray = $this->getGRNChageDetails($dataSet);

            //set log details
            $previousData = json_encode($previousData);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("GRN ".$row['grnCode']." is updated without product change.");
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_GRN_UPDATE');
            return $this->JSONRespond();
        }

    }

    public function getGRNChageDetails($data)
    {
        $row = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnID($data['grnID'], false, false, true)->current();
        $previousData = [];
        $previousData['GRN Code'] = $row['grnCode'];
        $previousData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
        $previousData['GRN Supplier Reference'] = $row['grnSupplierReference'];
        $previousData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $previousData['GRN Date'] = $this->convertDateToUserFormat($row['grnDate']);
        $previousData['GRN Delivery Charge'] = $row['grnDeliveryCharge'];
        $previousData['GRN Total'] = $row['grnTotal'];
        $previousData['GRN Comment'] = $row['grnComment'];

        $newData = [];
        $newData['GRN Code'] = $data['gC'];
        $newData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
        $newData['GRN Supplier Reference'] = $data['sR'];
        $newData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $newData['GRN Date'] = $this->convertDateToUserFormat($data['dD']);
        $newData['GRN Delivery Charge'] = $data['dC'];
        $newData['GRN Total'] = $data['fT'];
        $newData['GRN Comment'] = $data['cm'];

        return array('previousData' => $previousData, 'newData' => $newData);
    }


    private function createDataSetForReverse($grnID)
    {
        $grnProductDetails = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnAllBatchSerialProductDetailsByGrnId($grnID);
        $grnBatchDetails = [];
        $grnSerialDetails = [];
        $grnNormalItems = [];

        foreach ($grnProductDetails as $key => $grnDetails) {
            if($grnDetails['productSerialID'] != ''){
                $grnSerialDetails[$grnDetails['productSerialID']] = $grnDetails;

            } else if($grnDetails['productBatchID'] != ''){
                $grnBatchDetails[$grnDetails['productBatchID']] = $grnDetails;

            } else {
                $grnNormalItems[$grnDetails['locationProductID']][] = $grnDetails;

            }
        }


        $returnDataSet = array(
            'grnBatch' => $grnBatchDetails,
            'grnSerial' => $grnSerialDetails,
            'grnNormal' => $grnNormalItems,
            );
        return $returnDataSet;
    }

   private function reverseGrnProducts($grnID, $dataSet)
    {
        $docType = 'Goods Received Note';

        // reverse serial products
        if(sizeof($dataSet['grnSerial']) > 0){
            $batchSerialQty = [];
            foreach ($dataSet['grnSerial'] as $key => $serialDataSet) {
                $selectedSerialLocProID  = $serialDataSet['locationProductID'];
                            
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID(NULL, $serialDataSet['productSerialID']);
                $serialSoldFlag = false;

                $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($serialDataSet['productID'], $serialDataSet['locationID'])->current();
                
                foreach ($itemInData as $key => $serialDta) {
                    
                    if($serialDta['itemInSoldQty'] == 0){
                        if(floatval($serialDta['itemInLocationProductID']) != floatval($selectedSerialLocProID)){
                            $errorData = array(
                                'msg' => $this->getMessage('ERR_SERIAL_ITEM_TRANSFER_TO_ANOTHER_LOCATION'),
                                'status' => false,
                                'data' => 'transfer',
                            );
                            return $errorData;
                        }
                        if($serialDta['itemInBatchID'] != ''){
                            $batchSerialQty[$serialDta['itemInBatchID']] ++;
                        }
                        $returnQty = '1';
                        $serialSoldFlag = true;
                        //update locationproduct ItemIn and ItemOut
                        $locProStatue = $this->updateLocationProductAndItemInAndItemOut($grnID, $locationProductDetails, $serialDta, $returnQty,$returnQty);
                        if($locProStatue['status'] != 'true'){
                            $errorData = array(
                                'msg' => $locProStatue['msg'],
                                'status' => false,
                            );
                            return $errorData;
                        }

                        //update serial table
                        $data = array(
                            'productSerialReturned' => '1',
                            );

                        $updateSerialTable = $this->CommonTable('Inventory\Model\ProductSerialTable')
                                ->updateProductSerialData($data, $serialDta['itemInSerialID']);


                    }
                }
                if(!$serialSoldFlag){
                    $errorData = array(
                        'msg' => $this->getMessage('ERR_SERIAL_PRODUCT_IN_USE_GRN'),
                        'status' => false,
                        );
                    return $errorData;
                }

            };
            //update batch-serilaItem batch table
            if(!empty($batchSerialQty)){
                foreach ($batchSerialQty as $key => $value) {
                    $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($key);
                    $newBatchQty = floatval($batchDetails->productBatchQuantity) - floatval($value);

                    //update batchTable
                    $data = array(
                        'productBatchQuantity' => $newBatchQty,
                        );
                    $update = $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($key, $data);
                    if(!$update){
                        $errorData = array(
                            'msg' => $this->getMessage('ERR_SERIAL_BATCH_PRODUCT_UPDATE'),
                            'status' => false,
                        );
                        return $errorData;
                    }
                }
            }

        }
        //reverse batch products
        if(sizeof($dataSet['grnBatch']) > 0) {
            foreach ($dataSet['grnBatch'] as $key => $batchValue) {

                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID($batchValue['productBatchID'],null);

                $totalUpdatedQty = 0;

                foreach ($itemInData as $key => $itemInBatchValue) {

                    $returnQty = floatval($itemInBatchValue['itemInQty']) - floatval($itemInBatchValue['itemInSoldQty']);

                    $totalUpdatedQty += $returnQty;
                    $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($batchValue['productID'], $batchValue['locationID'])->current();
                    //update locationproduct ItemIn and ItemOut
                    $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($grnID, $locationProductDetails, $itemInBatchValue, $returnQty, floatval($itemInBatchValue['itemInQty']));
                    if($locUpdateStatus['status'] != 'true'){
                        $errorData = array(
                            'msg' => $locUpdateStatus['msg'],
                            'status' => false,
                        );
                        return $errorData;
                    }

                    //update batch table
                    if($batchValue['productSerialID'] == ''){
                        $data = array(
                            'productBatchQuantity' => '0',
                        );
                        $updateBatchTable = $this->CommonTable('Inventory\Model\ProductBatchTable')
                                   ->updateProductBatchQuantity($itemInBatchValue['itemInBatchID'], $data);
                    }

                }

                if(floatval($totalUpdatedQty) != floatval($batchValue['grnProductQuantity'])){
                    $errorData = array(
                        'msg' => $this->getMessage('ERR_BATCH_PRODUCT_IN_USE_GRN'),
                        'status' => false,
                        );
                    return $errorData;
                }

            }

        }
        //reverse normal products
        if(sizeof($dataSet['grnNormal']) > 0){
            $exisistGrnDetails = [];
            foreach ($dataSet['grnNormal'] as $key => $detailsByLocID) {

                foreach ($detailsByLocID as $key => $normalDataSet) {
                    if(empty($exisistGrnDetails[$normalDataSet['locationProductID']])){
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getProductDetailsBylocationProductIDDocTypeAndDocumentID($normalDataSet['locationProductID'], $docType, $normalDataSet['grnID']);

                        foreach ($itemInDetails as $key => $itemInSingleValue) {
                            if($itemInSingleValue['itemInSoldQty'] == 0){

                                $returnQty = floatval($itemInSingleValue['itemInQty']);
                                $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($normalDataSet['productID'], $normalDataSet['locationID'])->current();
                                //update locationproduct ItemIn and ItemOut

                                $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($grnID, $locationProductDetails, $itemInSingleValue, $returnQty,floatval($itemInSingleValue['itemInQty']));
                

                                if($locUpdateStatus['status'] != 'true'){
                                    $errorData = array(
                                        'msg' => $locUpdateStatus['msg'],
                                        'status' => false,
                                    );
                                    return $errorData;
                                }
                            } else {
                               $errorData = array(
                                    'msg' => $this->getMessage('ERR_NORMAL_PRODUCT_IN_USE_GRN'),
                                    'status' => false,
                                );
                                return $errorData;
                            }


                        }
                        // validate location product update or not
                        $exisistGrnDetails[$normalDataSet['locationProductID']] = $normalDataSet;

                    }
                }
            }
        }
        $errorData = array(
            'status' => true,
        );
        return $errorData;


    }

    private function updateLocationProductAndItemInAndItemOut($grnID, $locationProductDetails, $productDataSet, $returnQty, $finalReturnQty)
    {
        if($returnQty != 0){

            $outData = array(
                'itemOutDocumentType' => 'Goods Received Note Cancel',
                'itemOutDocumentID' => $grnID,
                'itemOutLocationProductID' => $locationProductDetails['locationProductID'],
                'itemOutSerialID' => $productDataSet['itemInSerialID'],
                'itemOutItemInID' => $productDataSet['itemInID'],
                'itemOutQty' => $returnQty,
                'itemOutPrice' => $productDataSet['itemInPrice'],
                'itemOutAverageCostingPrice' => $locationProductDetails['locationProductAverageCostingPrice'],
                'itemOutDiscount' => $productDataSet['itemInDiscount'],
                'itemOutDateAndTime' => $this->getGMTDateTime(),
                'itemOutDeletedFlag' => 1,
            );
            //add record to the item out
            $itemOutM = new ItemOut();
            $itemOutM->exchangeArray($outData);

            $itemOutState = $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
            if($itemOutState == ''){
                $updateLocRespondData = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_ADD_ITEM_OUT'),
                    );
                return $updateLocRespondData;
            }
            //update itemIn table as all items sold.
            $updatedItemInDetails = array(
                'itemInSoldQty' => $finalReturnQty,
                'itemInDeletedFlag' => 1,
                );
            $itemInUpdateStatue = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($updatedItemInDetails, $productDataSet['itemInID']);

            if(!$itemInUpdateStatue){
                $updateLocRespondData = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_UPDATE_ITEM_IN'),
                    );
                return $updateLocRespondData;
            }

             // calculate new avarageCosting
            $totalStockValue = floatval($locationProductDetails['locationProductQuantity'])
                    * floatval($locationProductDetails['locationProductAverageCostingPrice']);

            $canceledProductTotalValue = floatval($returnQty) * floatval($productDataSet['itemInPrice']);
            $existItemTotal = floatval($locationProductDetails['locationProductQuantity'])
                    - floatval($returnQty);

            if($existItemTotal != 0){
                $newLocationProdcutAvgCosting = ($totalStockValue - $canceledProductTotalValue)/ $existItemTotal;
            } else {
                $newLocationProdcutAvgCosting = 0;
            }

            //update locationProductAvgCosting
            $data = array(
                // 'locationProductAverageCostingPrice' => $newLocationProdcutAvgCosting,
                'locationProductQuantity' => $existItemTotal
            );



            $udateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')
                                ->updateAvgCostingByLocationProductID($data, $locationProductDetails['locationProductID']);

            if(!$udateLocPro){
                $updateLocRespondData = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_UPDATE_LOCATION_PRO_AVG_COST'),
                    );
                return $updateLocRespondData;
            }
            $updateAverageCosting = $this->calculateAvgCostAndOtherDetailsFornonSoldGRN($locationProductDetails, $productDataSet);
            if(!$updateAverageCosting['status']){
                $this->rollback();
                $returnBatchSerialState = array(
                    'msg' => $this->getMessage('ERR_UPDATE_AVERAGE_COSTING'),
                    'status' => false, 
                );
                return $returnBatchSerialState;
               
            } 

        }

        $updateLocRespondData = array(
            'status' => true,
        );
        return $updateLocRespondData;

    }


    private function calculateAvgCostAndOtherDetailsFornonSoldGRN($locationProductDetails, $productDataSet = null)
    {
        //get all iteminDetails
        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductIDInDecendingOrder($productDataSet['itemInLocationProductID'],$productDataSet['itemInID']);
        $initialState = 0;
        $locationProductQuantity = floatval($locationProductDetails['locationProductQuantity']);

        foreach ($itemInDetails as $key => $itemInSingleValue) {
            if ($initialState == 0) {
                //get sum of qty from item out for calculate costing
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
            } else {
                $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();

                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();

            }
            $initialState++;

            $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQty['totalQty'])) - floatval($itemInSingleValue['itemInQty']);

        }

        $nextInRecordFromCancelGrn = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();

        $outDetailsQtyForGrn = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($productDataSet['itemInLocationProductID'], $productDataSet['itemInDateAndTime'], $nextInRecordFromCancelGrn['itemInDateAndTime'])->current();  


        $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQtyForGrn['totalQty'])) - floatval($productDataSet['itemInQty']);

        $previousItemInFromCancelGrn = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryPreviousRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();


        $outDetailsQtyForPreviousDoc = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($previousItemInFromCancelGrn['itemInLocationProductID'], $previousItemInFromCancelGrn['itemInDateAndTime'], $productDataSet['itemInDateAndTime'])->current();

        $locationProductQuantity = floatval($locationProductQuantity) + floatval($outDetailsQtyForPreviousDoc['totalQty']);

        if ($previousItemInFromCancelGrn) {
            //get all iteminDetails after cancel
            $itemInDetailsAfterCancel = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductID($previousItemInFromCancelGrn['itemInLocationProductID'],$previousItemInFromCancelGrn['itemInID']);
            $avarageCosting = floatval($previousItemInFromCancelGrn['itemInAverageCostingPrice']);
        } else {
            $itemInDetailsAfterCancel = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID']);
            $avarageCosting = 0;
        }
        $initialState = 0;
        $locationUpdateFlag = false;
        foreach ($itemInDetailsAfterCancel as $key => $itemInSingleValue) {
            $locationUpdateFlag = true;
            //get very Next record in itemIn Table
            $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();
            
            if($nextInRecord){
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();
                
            } else {
                if (sizeof($itemInDetails) != 0) {
                    $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
                }
            }
            
            if ($initialState == 0 && $previousItemInFromCancelGrn) {
                $newAvgCosting = $avarageCosting;
            } else {
                $newAvgCosting = (($locationProductQuantity * $avarageCosting) + (floatval($itemInSingleValue['itemInQty']) * floatval($itemInSingleValue['itemInPrice'])))/(floatval($itemInSingleValue['itemInQty']) + $locationProductQuantity);
                $avarageCosting = $newAvgCosting;
                $locationProductQuantity = $locationProductQuantity + floatval($itemInSingleValue['itemInQty']);
            }
            
            //set data to update itemIn Table
            $itemInUpdatedDataSet = array(
                'itemInAverageCostingPrice' => $newAvgCosting,
            );

            //update item in table
            $itemInUpdateFlag = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($itemInUpdatedDataSet, $itemInSingleValue['itemInID']);
            if(!$itemInUpdateFlag){
                $returnCostingDetails = array(
                    'status' => false,
                    );
                return $returnCostingDetails;
            }
            
            $locationProductQuantity = $locationProductQuantity - floatval($outDetailsQty['totalQty']);
            //set data to update locationProduct
            $locationProductUpdatedData = array(
                'locationProductAverageCostingPrice' => $newAvgCosting
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $itemInSingleValue['itemInLocationProductID']);


            if($nextInRecord){
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime'],$nextInRecord['itemInDateAndTime']);
        
            } else {
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime']);

            }

           
            foreach ($itemOutDetails as $key => $itemOutSingleValue) {
                //set Data to update itemOut table
                $updateItemOutCValue = array(
                    'itemOutAverageCostingPrice' => $newAvgCosting,
                );

                //update costing value in itemOutRecords
                $updateItemOut = $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutTable($itemOutSingleValue['itemOutID'],$updateItemOutCValue);

                $itemOutIds = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDataByItemOutID($itemOutSingleValue['itemOutID'])->current();

                $journalEntryData = array(
                    'journalEntryAccountsDebitAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    'journalEntryAccountsCreditAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    );

                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryByDocumentId($journalEntryData, $itemOutIds['journalEntryID']);
                
                if(!$updateItemOut){
                    
                    $returnCalculateData = array(
                        'status' => false,
                    );
                
                    return $returnCalculateData;
                }
            }
            $initialState++;
        }

        if (!$locationUpdateFlag) {
            $locationProductUpdatedData = array(
                'locationProductAverageCostingPrice' => 0
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $productDataSet['itemInLocationProductID']);

        }

        $returnCostingDetails = array(
            'status' => true,
            );
        return $returnCostingDetails;
    }

    private function journalEntryreverseByGrn($grnID, $grnCode, $locationID, $dimensionData, $ignoreBudgetLimit)
    {
        /**
        * get journal entry details by given id and document id = 10
        * because grn document id is 10
        */

        $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('10', $grnID);
        $journalEntryID = $journalEntryData['journalEntryID'];
        $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);

        $journalEntryDate = $journalEntryData['journalEntryDate'];
        $i=0;
        $journalEntryAccounts = array();
        foreach ($jEAccounts as $key => $value) {
            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
            $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN Delete '.$grnCode.'.';
            $i++;
        }

        //get journal entry reference number.
        $jeresult = $this->getReferenceNoForLocation('30', $locationID);
        $jelocationReferenceID = $jeresult['locRefID'];
        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

        $journalEntryData = array(
            'journalEntryAccounts' => $journalEntryAccounts,
            'journalEntryDate' => $this->convertDateToStandardFormat($journalEntryDate),
            'journalEntryCode' => $JournalEntryCode,
            'journalEntryTypeID' => '',
            'journalEntryIsReverse' => 0,
            'journalEntryComment' => 'Journal Entry is posted when cancel GRN '.$grnCode.'.',
            'documentTypeID' => 10,
            'journalEntryDocumentID' => $grnID,
            'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

        $resultData = $this->saveJournalEntry($journalEntryData);

        if(!$resultData['status']){
            return ['status' => false, 'msg' => $resultData['msg'], 'data' => $resultData['data']];
        }

        $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($grnID,10, 5);
        if(!$jEDocStatusUpdate['status']){
            return ['status' => false, 'msg' => $jEDocStatusUpdate['msg'], 'data' => $jEDocStatusUpdate['data']];
        }

        foreach ($dimensionData as $value) {
            if (!empty($value)) {
                $saveRes = $this->saveDimensionsForJournalEntry($value, $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                if(!$saveRes['status']){
                    return ['status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']];
                }   
            }
        }

        return ['status' => true];
    }


    public function updateExistingGrnRecord($post ,$cancelGrnId, $updateFlag, $ignoreBudgetLimit)
    {
        //check whether GRN is created via a Purchase Order
        $poProductsUncopiedArr = [];
        if ($post['startByPoID'] != '') {
            $copiedFromPo = TRUE;
            //get purchase order product details
            $poProducts = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getUnCopiedPoProducts($post['startByPoID']);
            foreach ($poProducts as $product) {
                $restQty = $product['purchaseOrderProductQuantity'] - $product['purchaseOrderProductCopiedQuantity'];
                $restQty = ($restQty < 0) ? 0 : $restQty;
                $poProductsUncopiedArr[$product['locationProductID']] = $restQty;
            }
        } else {
            $copiedFromPo = FALSE;
        }
        $grnCode = $post['gC'];
        $locationReferenceID = $post['lRID'];
        $locationID = $post['rL'];
        $documentReference = $post['documentReference'];

        $entityID = $this->createEntity();
        $grnData = array(
            'grnSupplierID' => $post['sID'],
            'grnSupplierReference' => $post['sR'],
            'grnCode' => $grnCode,
            'grnRetrieveLocation' => $locationID,
            'grnDate' => $post['dD'],
            'grnDate' => $this->convertDateToStandardFormat($post['dD']),
            'grnDeliveryCharge' => $post['dC'],
            'grnTotal' => $post['fT'],
            'grnComment' => $post['cm'],
            'grnShowTax' => $post['sT'],
            'entityID' => $entityID,
            'grnUploadFlag' => filter_var($post['uploadDocFlag'], FILTER_VALIDATE_BOOLEAN),
        );
        $grnM = new Grn();
        $grnM->exchangeArray($grnData);

//save the grn details to the grn table
        $grnID = $this->CommonTable('Inventory\Model\GrnTable')->saveGrn($grnM);
        //check whether grn save or not
        if(!$grnID){
            $retunArray = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_GRN_CREATE'),
                );
            return $retunArray;

        }
//loop through the products

        $allProductTotal = 0;
        // remove empty values and decord data
        $proList = array_filter(json_decode($post['pr'], true));

        foreach ($proList as $product) {
            $locationPID = $product['locationPID'];

            $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
            $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
            $locationProductQuantity = $locationProductData->locationProductQuantity;
            $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

            $productType = $product['productType'];
            //use to calculate product discount via value or percentage
            if($product['pDType'] == 'value'){
                $discountValue = $product['pDiscount'];
                $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
            } else {
                $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
            }

            $allProductTotal+=$productTotal;
//if copied from PO,update those products as copied.
            if ($copiedFromPo == TRUE && $product['poID'] != '' && $updateFlag == TRUE) {

                $this->updatePoProductCopiedQty($locationPID, $post['startByPoID'], $product['pQuantity']);
            }
            $productBaseQty = $product['pQuantity'];
            $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);

//if product is a batch Product

            // if (array_key_exists('bProducts', $product)) {
            if (sizeof($product['bProducts']) > 0) {
                $itemData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsForAvgCosting($locationPID);
                $batchProductCount = 0;
                foreach ($product['bProducts'] as $bProduct) {

                    $batchProductCount++;
                    $bPData = array(
                        'locationProductID' => $locationPID,
                        'productBatchCode' => $bProduct['bCode'],
                        'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                        'productBatchWarrantyPeriod' => $bProduct['warnty'],
                        'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                        'productBatchQuantity' => $bProduct['bQty']*$productConversionRate,
                        'productBatchPrice' => ($bProduct['price']*$productConversionRate),
                    );
                    $batchProduct = new ProductBatch();
                    $batchProduct->exchangeArray($bPData);

                    $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);

//if the product is batch and serial
                    // if (array_key_exists('sProducts', $product)) {
                    if(sizeof($product['sProducts']) > 0){
                        foreach ($product['sProducts'] as $sProduct) {
                            if (isset($sProduct['sBCode'])) {
                                if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                    $sPData = array(
                                        'locationProductID' => $locationPID,
                                        'productBatchID' => $insertedBID,
                                        'productSerialCode' => $sProduct['sCode'],
                                        'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                                        'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                        'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                                    );
                                    $serialPr = new ProductSerial();
                                    $serialPr->exchangeArray($sPData);
                                    $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                    $grnProductM = new GrnProduct();
                                    $grnProductData = array(
                                        'grnID' => $grnID,
                                        'locationProductID' => $locationPID,
                                        'productBatchID' => $insertedBID,
                                        'productSerialID' => $insertedSID,
                                        'grnProductPrice' => $product['pUnitPrice'],
                                        'grnProductDiscountType' => $product['pDType'],
                                        'grnProductDiscount' => $product['pDiscount'],
                                        'grnProductTotalQty' => $productBaseQty,
                                        'grnProductQuantity' => $bProduct['bQty'],
                                        'grnProductUom' => $product['pUom'],
                                        'grnProductTotal' => $product['pTotal'],
                                        'rackID' => ''
                                    );

                                    if ($copiedFromPo && $product['poID'] != '' && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > $serialProductCount) {

                                        $copiedQty = 1;
                                        $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                                        $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                                        $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                                    }

                                    $grnProductM->exchangeArray($grnProductData);
                                    $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);
//save the product details to the itemInTable

                                    $itemInData = array(
                                        'itemInDocumentType' => 'Goods Received Note',
                                        'itemInDocumentID' => $grnID,
                                        'itemInLocationProductID' => $locationPID,
                                        'itemInBatchID' => $insertedBID,
                                        'itemInSerialID' => $insertedSID,
                                        'itemInQty' => 1,
                                        'itemInPrice' => $product['pUnitPrice'],
                                        'itemInDiscount' => $discountValue,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                    if (array_key_exists('pTax', $product)) {
                                        if (array_key_exists('tL', $product['pTax'])) {
                                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                $grnPTaxesData = array(
                                                    'grnID' => $grnID,
                                                    'grnProductID' => $insertedGrnProductID,
                                                    'grnTaxID' => $taxKey,
                                                    'grnTaxPrecentage' => $productTax['tP'],
                                                    'grnTaxAmount' => $productTax['tA']
                                                );
                                                $grnPTaxM = new GrnProductTax();
                                                $grnPTaxM->exchangeArray($grnPTaxesData);
                                                $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                                            }
                                        }
                                    }
                                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                    $newPQtyData = array(
                                        'locationProductQuantity' => $newPQty
                                    );
//Update location Product Quantity
                                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                }
                            }
                        }
                    } else {
                        $grnProductM = new GrnProduct();
                        $grnProductData = array(
                            'grnID' => $grnID,
                            'locationProductID' => $locationPID,
                            'productBatchID' => $insertedBID,
                            'grnProductPrice' => $product['pUnitPrice'],
                            'grnProductDiscountType' => $product['pDType'],
                            'grnProductDiscount' => $product['pDiscount'],
                            'grnProductTotalQty' => $productBaseQty,
                            'grnProductQuantity' => $bProduct['bQty'] * $productConversionRate,
                            'grnProductUom' => $product['pUom'],
                            'grnProductTotal' => $product['pTotal'],
                            'rackID' => ''
                        );

                        if ($copiedFromPo && $product['poID'] != '' && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > 0) {

                            $copiedQty = ($bProduct['bQty'] < $poProductsUncopiedArr[$locationPID]) ? $bProduct['bQty'] : $poProductsUncopiedArr[$locationPID];
                            $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                            $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                            $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                        }

                        $grnProductM->exchangeArray($grnProductData);
                        $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);
//save itemIn
                        $itemInData = array(
                            'itemInDocumentType' => 'Goods Received Note',
                            'itemInDocumentID' => $grnID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => $insertedBID,
                            'itemInSerialID' => NULL,
                            'itemInQty' => $bProduct['bQty'] * $productConversionRate,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if (array_key_exists('pTax', $product) && $product['pTax'] != '') {
                            if (array_key_exists('tL', $product['pTax'])) {
                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                    $grnPTaxesData = array(
                                        'grnID' => $grnID,
                                        'grnProductID' => $insertedGrnProductID,
                                        'grnTaxID' => $taxKey,
                                        'grnTaxPrecentage' => $productTax['tP'],
                                        'grnTaxAmount' => $productTax['tA']
                                    );
                                    $grnPTaxM = new GrnProductTax();
                                    $grnPTaxM->exchangeArray($grnPTaxesData);
                                    $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                                }
                            }
                        }
                        if ($batchProductCount == 1) {
                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                            );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        }
                    }
                }

            // } elseif (array_key_exists('sProducts', $product)) {
            } elseif (sizeof($product['sProducts']) > 0) {
               
            //if the product is a serial Product
                $itemData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsForAvgCosting($locationPID);
                $serialProductCount = 0;
                foreach ($product['sProducts'] as $sProduct) {
                    $sPData = array(
                        'locationProductID' => $locationPID,
                        'productSerialCode' => $sProduct['sCode'],
                        'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                        'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                        'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                    );
                    $serialPr = new ProductSerial();
                    $serialPr->exchangeArray($sPData);
                    $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                    $grnProductM = new GrnProduct();
                    $grnProductData = array(
                        'grnID' => $grnID,
                        'locationProductID' => $locationPID,
                        'productSerialID' => $insertedSID,
                        'grnProductPrice' => $product['pUnitPrice'],
                        'grnProductDiscountType' => $product['pDType'],
                        'grnProductDiscount' => $product['pDiscount'],
                        'grnProductTotalQty' => $productBaseQty,
                        'grnProductQuantity' => 1,
                        'grnProductUom' => $product['pUom'],
                        'grnProductTotal' => $product['pTotal'],
                        'rackID' => ''
                    );

                    if ($copiedFromPo && $product['poID'] != '' && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > $serialProductCount) {

                        $copiedQty = 1;
                        $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                        $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                        $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                    }

                    $grnProductM->exchangeArray($grnProductData);
                    $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);
//save serial product itemIn
                    $itemInData = array(
                        'itemInDocumentType' => 'Goods Received Note',
                        'itemInDocumentID' => $grnID,
                        'itemInLocationProductID' => $locationPID,
                        'itemInBatchID' => NULL,
                        'itemInSerialID' => $insertedSID,
                        'itemInQty' => 1,
                        'itemInPrice' => $product['pUnitPrice'],
                        'itemInDiscount' => $discountValue,
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                    );
                    $itemInModel = new ItemIn();
                    $itemInModel->exchangeArray($itemInData);
                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $grnPTaxesData = array(
                                    'grnID' => $grnID,
                                    'grnProductID' => $insertedGrnProductID,
                                    'grnTaxID' => $taxKey,
                                    'grnTaxPrecentage' => $productTax['tP'],
                                    'grnTaxAmount' => $productTax['tA']
                                );
                                $grnPTaxM = new GrnProductTax();
                                $grnPTaxM->exchangeArray($grnPTaxesData);
                                $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                            }
                        }
                    }
                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                    $serialProductCount++;
                }
            } else {
//If the product is a non batch and non serial product
                $itemData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsForAvgCosting($locationPID);
                $grnProductM = new GrnProduct();
                $grnProductData = array(
                    'grnID' => $grnID,
                    'locationProductID' => $locationPID,
                    'grnProductPrice' => $product['pUnitPrice'],
                    'grnProductTotalQty' => $productBaseQty,
                    'grnProductQuantity' => $productBaseQty,
                    'grnProductUom' => $product['pUom'],
                    'grnProductDiscountType' => $product['pDType'],
                    'grnProductDiscount' => $product['pDiscount'],
                    'grnProductTotal' => $product['pTotal']
                );

                if ($copiedFromPo && $product['poID'] != '' && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > 0) {

                    $copiedQty = ($productBaseQty < $poProductsUncopiedArr[$locationPID]) ? $productBaseQty : $poProductsUncopiedArr[$locationPID];
                    $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                    $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                    $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                }

                $grnProductM->exchangeArray($grnProductData);
                $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);


                //save itemIn Details
                if ($productType != 2) {
                    $itemInData = array(
                        'itemInDocumentType' => 'Goods Received Note',
                        'itemInDocumentID' => $grnID,
                        'itemInLocationProductID' => $locationPID,
                        'itemInBatchID' => NULL,
                        'itemInSerialID' => NULL,
                        'itemInQty' => $productBaseQty,
                        'itemInPrice' => $product['pUnitPrice'],
                        'itemInDiscount' => $discountValue,
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                    );
                    $itemInModel = new ItemIn();
                    $itemInModel->exchangeArray($itemInData);
                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                }

                if (array_key_exists('pTax', $product) && $product['pTax'] != '') {
                    if (array_key_exists('tL', $product['pTax'])) {
                        foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                            $grnPTaxesData = array(
                                'grnID' => $grnID,
                                'grnProductID' => $insertedGrnProductID,
                                'grnTaxID' => $taxKey,
                                'grnTaxPrecentage' => $productTax['tP'],
                                'grnTaxAmount' => $productTax['tA']
                            );
                            $grnPTaxM = new GrnProductTax();
                            $grnPTaxM->exchangeArray($grnPTaxesData);
                            $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                        }
                    }
                }
                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                if ($productType == 2) {
                    $productBaseQty = 0;
                }
                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                $newPQtyData = array(
                    'locationProductQuantity' => $newPQty
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
            }

            $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
            $locationPrTotalQty = 0;
            $locationPrTotalPrice = 0;
            $newItemInIds = array();
            if(count($itemInData) > 0){
                foreach ($itemInData as $key => $value) {
                    if($value->itemInID > $locationProductLastItemInID){
                        $newItemInIds[] = $value->itemInID;
                        $remainQty = $value->itemInQty - $value->itemInSoldQty;
                        if($remainQty > 0){
                            $itemInPrice = $remainQty*$value->itemInPrice;
                            $itemInDiscount = $remainQty*$value->itemInDiscount;
                            $locationPrTotalQty += $remainQty;
                            $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                        }
                        $locationProductLastItemInID = $value->itemInID;
                    }
                }
            }
            foreach ($itemData as $value) {
                $availableStockQty = $value['totalAvilableStockQty'];
                $availableStockValue = $value['totalAvilableStockValue'];
            }

            $itemInCanceledData = $this->CommonTable('Inventory\Model\ItemInTable')->getCanceledItemInProductDetails($cancelGrnId, $locationPID)->current();
               
            $canceledItemInDate = $itemInCanceledData['itemInDateAndTime'];
                
            $locationPrTotalQty += $availableStockQty;
            $locationPrTotalPrice += $availableStockValue;

            if($locationPrTotalQty > 0){
                $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                $lpdata = array(
                    'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                    'locationProductLastItemInID' => $locationProductLastItemInID,
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                foreach ($newItemInIds as $inID) {
                    $intemInData = array(
                        'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                    );
                    $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                }


                $outData = array(
                    'itemOutAverageCostingPrice' => $newAverageCostinPrice
                );

                $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutByArray($outData, $locationPID, $canceledItemInDate);

                $itemOutIds = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDocumentIdsByCancelledDate($locationPID, $canceledItemInDate);

                $itemOutDocumentIDs = [];
                foreach ($itemOutIds as $value) {
                    
                    $journalEntryData = array(
                    'journalEntryAccountsDebitAmount' => $newAverageCostinPrice * $value['itemOutQty'],
                    'journalEntryAccountsCreditAmount' => $newAverageCostinPrice * $value['itemOutQty'],
                    );

                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryByDocumentId($journalEntryData, $value['journalEntryID']);   
                }
            }
        }

        //add document reference for document reference table
        if (count($documentReference) > 0) {
            foreach ($documentReference as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $data = array(
                        'sourceDocumentTypeId' => 10,
                        'sourceDocumentId' => $grnID,
                        'referenceDocumentTypeId' => $key,
                        'referenceDocumentId' => $value,
                    );
                    $documentReference = new DocumentReference();
                    $documentReference->exchangeArray($data);
                    $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                }
            }
        }

        //update PO status if copied from it
        if ($copiedFromPo == TRUE) {
            $this->checkAndUpdatePoStatus($post['startByPoID']);
        }

        // call product updated event
        $productIDs = array_map(function($element) {
            return $element['pID'];
        }, json_decode($post['pr'], true));

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        if ($this->useAccounting == 1) {
            //check whether supplier GRN Clearing account is set or not
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($post['sID']);
            if(empty($supplierData->supplierGrnClearingAccountID)){

                $returnArray = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_GRN_SUPPLIER_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode)),
                );
                return $returnArray;
            }
            $GRNClearingAccountID = $supplierData->supplierGrnClearingAccountID;

            $accountProduct = array();
            foreach ($proList as $product) {
                //check whether Product Inventory Gl account id set or not
                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($product['pID']);
                if(empty($pData['productInventoryAccountID'])){

                    $returnArray = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                    );
                    return $returnArray;
                }
                //calculate product total without tax potion.
                //use to calculate product discount via value or percentage
                if($product['pDType'] == 'value'){
                    $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
                } else {
                    $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                }

                //add grn deliveryCharge to product according to price ratio.
                // if($post['dC'] >0){
                //     $productTotal += ($post['dC']*$productTotal)/ $allProductTotal;
                // }
                //set gl accounts for the journal entry
                if(isset($accountProduct[$pData['productInventoryAccountID']]['total'])){
                    $accountProduct[$pData['productInventoryAccountID']]['total'] += $productTotal;
                }else{
                    $accountProduct[$pData['productInventoryAccountID']]['total'] = $productTotal;
                    $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
                }
            }

            //create data array for the journal entry save.
            $i=0;
            $journalEntryAccounts = array();
            $totalValueForSupplierAccount = 0;

            foreach ($accountProduct as $key => $value) {
                $totalValueForSupplierAccount+=$value['total'];
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['total'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode;
                $i++;
            }

            // $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            // $journalEntryAccounts[$i]['financeAccountsID'] = $GRNClearingAccountID;
            // $journalEntryAccounts[$i]['financeGroupsID'] = '';
            // $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
            // $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $totalValueForSupplierAccount;
            // $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode.".";

            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountsData = $glAccountExist->current();

            if($post['dC'] > 0){
                if (!empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)) {
                    if(isset($accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'])){
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'] += $post['dC'];
                    }else{
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'] = $post['dC'];
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['accountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                    }


                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode;

                    $totalValueForSupplierAccount+= $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'];
                } else {
                    $returnArray = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_DELI_CHARGE_ACCOUNT'),
                    );
                    return $returnArray;
                }
            }


            $inc = (!empty($post['dC'])) ?  $i +1 : $i;

            $journalEntryAccounts[$inc]['fAccountsIncID'] = $inc;
            $journalEntryAccounts[$inc]['financeAccountsID'] = $GRNClearingAccountID;
            $journalEntryAccounts[$inc]['financeGroupsID'] = '';
            $journalEntryAccounts[$inc]['journalEntryAccountsDebitAmount'] = 0.00;
            $journalEntryAccounts[$inc]['journalEntryAccountsCreditAmount'] = $totalValueForSupplierAccount;
            $journalEntryAccounts[$inc]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode.".";


            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $grnData['grnDate'],
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create GRN '.$grnCode.".",
                'documentTypeID' => 10,
                'journalEntryDocumentID' => $grnID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

            $resultData = $this->saveJournalEntry($journalEntryData);

            if($resultData['status']){
                $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($grnID,10, 3);
                if(!$jEDocStatusUpdate['status']){
                    return ['status' => false, 'msg' => $jEDocStatusUpdate['msg']];
                }
                
                foreach ($post['dimensionData'] as $value) {
                    if (!empty($value)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($value, $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            return ['status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']];
                        }   
                    }
                }

                $returnArray = array(
                    'status' => true,
                    'data' => $grnID,
                    'msg' => $this->getMessage('SUCC_GRN_UPDATE')
                );
                $this->setLogMessage($this->companyCurrencySymbol . $product['pTotal'] . ' GRN successfully updated -' . $grnCode);
                return $returnArray;
            }else{

                $returnArray = array(
                    'status' => false,
                    'msg' => $resultData['msg'],
                    'data' => $resultData['data'],
                );
                return $returnArray;
            }
        }else{
            $returnArray = array(
                'status' => true,
                'data' => $grnID,
                'msg' => $this->getMessage('SUCC_GRN_UPDATE')
                );

            $this->setLogMessage($this->companyCurrencySymbol . $product['pTotal'] . ' GRN successfully updates -' . $grnCode);
            return $returnArray;
        }
    }

    public function updateSoldGrn($grnDataSet)
    {
        //first update grn table with new values
        $grnBasicUpdateSet = array(
            'grnComment' => $grnDataSet['cm'],
            'grnTotal' => $grnDataSet['fT'],
            'grnDeliveryCharge' => $grnDataSet['dC'],
            'grnShowTax' => $grnDataSet['sT'],
            'grnDate' => $this->convertDateToStandardFormat($grnDataSet['dD']),
        );       
        //update grn table
        $this->beginTransaction();
        $updateGrnRecord = $this->CommonTable('Inventory\Model\GrnTable')->updateGrnRecord($grnDataSet['grnID'],$grnBasicUpdateSet);
        if(!$updateGrnRecord){
            $returnCalculateData = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_UPDATE_GRN_PRO'),
            );
            return $returnCalculateData;
        } 
        $allProductTotal = 0;
        $validateState = $this->validateEditedGrn(json_decode($grnDataSet['pr'], true), $grnDataSet['grnID']);
        if(!$validateState['status']){
            $this->rollback();
            return $validateState;  
        }
        
        // check grn items, one by one
        foreach (json_decode($grnDataSet['pr'], true) as $key => $grnProductLine) {
            //get details by grnProductTable
            $updateExistingItemFlag = true;
            if($grnProductLine != null){

                $productType = $grnProductLine['productType'];
                //use to calculate product discount via value or percentage
                if($grnProductLine['pDType'] == 'value'){
                    $discountValue = $grnProductLine['pDiscount'];
                    $productTotal = ($grnProductLine['pUnitPrice'] - $grnProductLine['pDiscount'])*$grnProductLine['pQuantity'];
                } else {
                    $discountValue = $grnProductLine['pUnitPrice'] * $grnProductLine['pDiscount'] / 100;
                    $productTotal = $grnProductLine['pUnitPrice']*$grnProductLine['pQuantity'] * ((100 - $grnProductLine['pDiscount']) / 100);
                }

                $allProductTotal+=$productTotal;
                                
                if(sizeof($grnProductLine['bProducts']) == 0 && sizeof($grnProductLine['sProducts']) == 0){
                    
                    $grnProdutDetails = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductDetailsByGrnProductID($grnProductLine['grnProductID'])->current();
                    
                    $crntGrnProPrice = ($grnProdutDetails['grnProductPrice'] != '') ? $grnProdutDetails['grnProductPrice'] : 0;
                    $crntGrnProQty =  ($grnProdutDetails['grnProductQuantity'] != '') ? $grnProdutDetails['grnProductQuantity'] : 0;
                    //get item in details
                    $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsBydocumentIDLocationProdcutIDQtyAndAvgCosting($grnProdutDetails['locationProductID'],'Goods Received Note',$grnProdutDetails['grnID'],$crntGrnProQty,$crntGrnProPrice)->current();
                                        
                    //create data set for update grn product table for normal items
                    $updatedGrnProductDataSet = array(
                        'grnProductDiscountType' => $grnProductLine['pDType'],
                        'grnProductDiscount' => $grnProductLine['pDiscount'],
                        'grnProductTotalQty' => $grnProductLine['pQuantity'],
                        'grnProductQuantity' => $grnProductLine['pQuantity'],
                        'grnProductTotal'=> $grnProductLine['pTotal'],
                        'grnProductPrice' => $grnProductLine['pUnitPrice']
                        );
                    //update grn product table
                    $updateFlag = $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($updatedGrnProductDataSet, $grnProductLine['grnProductID']);

                    //set data to update grnProduct tax table
                    if (array_key_exists('pTax', $grnProductLine)) {
                        if (array_key_exists('tL', $grnProductLine['pTax'])) {
                            
                            // set current tax data to the array
                            $currentTaxData = [];
                            foreach ($grnProductLine['pTax']['tL'] as $taxKey => $productTax) {
                                $currentTaxData[$taxKey] = array(
                                    'grnTaxID' => $taxKey,
                                    'grnTaxPrecentage' => $productTax['tP'],
                                    'grnTaxAmount' => $productTax['tA']
                                );
                            }
                            //get existing grn product tax details
                            $taxDetails = $this->CommonTable('Inventory\Model\GrnProductTaxTable')->getGrnProductTaxDetailByGrnIDAndGrnProductID($grnDataSet['grnID'],$grnProductLine['grnProductID']);
                            foreach ($taxDetails as $key => $singleTaxValue) {

                                //update grnProductTax table total tax column
                                if($currentTaxData[$singleTaxValue['grnTaxID']]){
                                    $taxUpdateData = array(
                                        'grnTaxAmount' => floatval($currentTaxData[$singleTaxValue['grnTaxID']]['grnTaxAmount']),
                                        );
                                    $updategrnTax = $this->CommonTable('Inventory\Model\GrnProductTaxTable')->updateGrnProductTax($taxUpdateData,$singleTaxValue['grnProductTaxID']);

                                    if(!$updategrnTax){
                                        $returnCalculateData = array(
                                            'status' => false,
                                            'msg' => $this->getMessage('ERR_UPDATE_GRN_PRO_TAX'),
                                        );
                                        return $returnCalculateData;
                                    }
                                } else {
                                    $taxDeleRes = $this->CommonTable('Inventory\Model\GrnProductTaxTable')->deleteGrnProductTaxByGrnProductTaxID($singleTaxValue['grnProductTaxID']);
                                    if(!$taxDeleRes){
                                        $data = array(
                                            'status' => false,
                                            'msg' => $this->getMessage('ERR_UPDATE_GRN_PRO_TAX'),
                                        );
                                        return $data;
                                    }
                                }
                            }
                            
                        } else {
                            $existingTaxDetails = $this->CommonTable('Inventory\Model\GrnProductTaxTable')->getGrnProductTaxDetailByGrnIDAndGrnProductID($grnDataSet['grnID'],$grnProductLine['grnProductID'])->current();
                            if ($existingTaxDetails) {
                                $taxDeleRes = $this->CommonTable('Inventory\Model\GrnProductTaxTable')->deleteGrnProductTax($grnDataSet['grnID'],$grnProductLine['grnProductID']);
                                if(!$taxDeleRes){
                                    $data = array(
                                        'status' => false,
                                        'msg' => $this->getMessage('ERR_UPDATE_GRN_PRO_TAX'),
                                    );
                                    return $data;
                                }
                            }
                        }
                    }
                                        
                    $updateStateForNormal = $this->calculateAvgCostAndOtherDetails($itemInData, $grnProductLine); 
                    if(!$updateStateForNormal['status']){
                        $this->rollback();
                        $returnBatchSerialState = array(
                            'msg' => $this->getMessage('ERR_NORMAL_PRODUCT_UPDATE'),
                            'status' => false, 
                        );
                        return $returnBatchSerialState;
                       
                    }

                             
                } else if(sizeof($grnProductLine['bProducts']) != 0 && sizeof($grnProductLine['sProducts']) == 0){
                    //get Batch products one by one create by next step
                    $this->rollback();
                    $returnBatchSerialState = array(
                        'msg' => $this->getMessage('ERR_BATCH_PRODUCT_CANNOT'),
                        'status' => false, 
                        );
                    return $returnBatchSerialState;
                    
                } else if(sizeof($grnProductLine['bProducts']) == 0 && sizeof($grnProductLine['sProducts']) != 0) {
                    $this->rollback();
                    $returnBatchSerialState = array(
                        'msg' => $this->getMessage('ERR_SERIAL_PRODUCT_CANNOT'),
                        'status' => false, 
                        );
                    return $returnBatchSerialState;
                } else {
                    $this->rollback();
                    $returnBatchSerialState = array(
                        'msg' => $this->getMessage('ERR_BATCH_SERIAL_PRODUCT_CANNOT'),
                        'status' => false, 
                        );
                    return $returnBatchSerialState;
                }

            }
        }
        $this->commit();
        $returnBatchSerialState = array(
            'msg' => $this->getMessage('SUCC_UPDATE_GRN_ITEM'),
            'status' => true, 
            'data' => $allProductTotal,
        );
        return $returnBatchSerialState;
    }

    private function calculateAvgCostAndOtherDetails($itemInData, $grnProductLine = null)
    {
        //get all iteminDetails
        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductID($itemInData['itemInLocationProductID'],$itemInData['itemInID']);
            
        $initialState = 0;
        foreach ($itemInDetails as $key => $itemInSingleValue) {
            $initialState++;
            //get current locationProduct Detials
            $locationProDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductsByLocationProductID($itemInSingleValue['itemInLocationProductID']);
            
            //get previous item in details
            $pastDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getAvgCostingValueForSelectedID($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInID'])->current();
            
            //get sum of qty from item out for calculate costing
            $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();

            //get related location product id's for this product id
            $locProductIds = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsByProductID($grnProductLine['pID']);

            foreach ($locProductIds as $key => $locvalue) {
                $outDetailsQtyByTransfer = $this->CommonTable('Inventory\Model\ItemInTable')->getTransferOutDataByLocationProductIDAndDate($locvalue['locationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();

                if ($outDetailsQtyByTransfer) {
                    $outDetailsQty['totalQty'] = floatval($outDetailsQty['totalQty']) + floatval($outDetailsQtyByTransfer['totalQty']);
                }
            }

            //get sum of productCost after above grn
            $totalCost = $this->CommonTable('Inventory\Model\ItemInTable')->getTotalCostByLocationProductIDAndDateRange($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInID'])->current();
            
            //calculate previos locationProductQty
            $prvLocProQty = (floatval($locationProDetails->locationProductQuantity) + floatval($outDetailsQty['totalQty']))-(floatval($totalCost['totalQty']) + floatval($itemInSingleValue['itemInQty']));

            // calculate new avg costing value for initial state
            if($initialState == 1){
                $existingTotal = $grnProductLine['pUnitPrice']*(100- floatval($grnProductLine['pDiscount']))/100;
                $newAvgCosting = ((floatval($pastDetails['itemInAverageCostingPrice'])*floatval($prvLocProQty))+ (floatval($existingTotal)* floatval($grnProductLine['pQuantity'])))/(floatval($prvLocProQty) + floatval($grnProductLine['pQuantity']));
                
                //calculate Discount value
                $discountValue = $grnProductLine['pUnitPrice']*(floatval($grnProductLine['pDiscount']))/100;

                //validate itemInQty
                if(floatval($itemInData['itemInQty']) <= floatval($grnProductLine['pQuantity'])){
                
                    //set data to update itemIn Table
                    $itemInUpdatedDataSet = array(
                        'itemInQty' => floatval($grnProductLine['pQuantity']),
                        'itemInPrice' => floatval($grnProductLine['pUnitPrice']), 
                        'itemInAverageCostingPrice' => $newAvgCosting,
                        'itemInDiscount' => $discountValue, 
                        );
                } else {
                    $returnCostingDetails = array(
                        'status' => false,
                    );
                    return $returnCostingDetails;
                }
        
                
            } else {

                $existingTotal = ((floatval($itemInSingleValue['itemInPrice'])* floatval($itemInSingleValue['itemInQty']))-floatval($itemInSingleValue['itemInDiscount']))/floatval($itemInSingleValue['itemInQty']);
                $newAvgCosting = ((floatval($pastDetails['itemInAverageCostingPrice'])*floatval($prvLocProQty))+ (floatval($existingTotal)* floatval($itemInSingleValue['itemInQty'])))/(floatval($prvLocProQty) + floatval($itemInSingleValue['itemInQty']));

                //set data to update itemIn Table
                $itemInUpdatedDataSet = array(
                    'itemInAverageCostingPrice' => $newAvgCosting,
                );
               
            }
            //update item in table
            $itemInUpdateFlag = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($itemInUpdatedDataSet, $itemInSingleValue['itemInID']);
            if(!$itemInUpdateFlag){
                $returnCostingDetails = array(
                    'status' => false,
                    );
                return $returnCostingDetails;
            }
            
            //set data to update locationProduct
            $locationProductUpdatedData = array(
                'locationProductAverageCostingPrice' => $newAvgCosting,
                );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $itemInSingleValue['itemInLocationProductID']);


            //get very Next record in itemIn Table
            $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();
                
            if(sizeof($nextInRecord) != 0){
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime'],$nextInRecord['itemInDateAndTime']);
                
            } else {
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime']);
            }

           
            foreach ($itemOutDetails as $key => $itemOutSingleValue) {

                //set Data to update itemOut table
                $updateItemOutCValue = array(
                    'itemOutAverageCostingPrice' => $newAvgCosting,
                );

                //update costing value in itemOutRecords
                $updateItemOut = $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutTable($itemOutSingleValue['itemOutID'],$updateItemOutCValue);

                $itemOutIds = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDataByItemOutID($itemOutSingleValue['itemOutID'])->current();

                $journalEntryData = array(
                    'journalEntryAccountsDebitAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    'journalEntryAccountsCreditAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    );

                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryByDocumentId($journalEntryData, $itemOutIds['journalEntryID']);
                
                if(!$updateItemOut){
                    
                    $returnCalculateData = array(
                        'status' => false,
                    );
                
                    return $returnCalculateData;
                }
            }

            //get related location product id's for this product id
            $locProductIdSet = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsByProductID($grnProductLine['pID']);

            foreach ($locProductIdSet as $key => $locvalue) {
                if($nextInRecord){
                    $transferDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getTransferDetailsByGivenDateRangeAndLOcationProductID($locvalue['locationProductID'], $itemInSingleValue['itemInDateAndTime'],$nextInRecord['itemInDateAndTime']);

                } else {
                    $transferDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getTransferDetailsByGivenDateRangeAndLOcationProductID($locvalue['locationProductID'], $itemInSingleValue['itemInDateAndTime']);
                }

                foreach ($transferDetails as $key => $transferSingleData) {
                    //set data to update itemIn Table
                    $itemInUpdatedDataSet = array(
                        'itemInAverageCostingPrice' => $newAvgCosting,
                    );
                
                    //update item in table
                    $itemInUpdateFlag = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($itemInUpdatedDataSet, $transferSingleData['itemInID']);
                    if(!$itemInUpdateFlag){
                        $returnCostingDetails = array(
                            'status' => false,
                            );
                        return $returnCostingDetails;
                    }

                    //set data to update locationProduct
                    $locationProductUpdatedData = array(
                        'locationProductAverageCostingPrice' => $newAvgCosting,
                        );

                    //update locationProduct table
                    $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $transferSingleData['itemInLocationProductID']);
                }
            }

        }
                       
        $returnCostingDetails = array(
            'status' => true,
            );
        return $returnCostingDetails;
    }

    private function createDataSetForJournalEntry($dataSet, $allProductTotal, $ignoreBudgetLimit)
    {
        $documentTypeID = 10;
        $existingJournalEntry = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithAccountDetailsByDocumentTypeIDAndDocumentID($documentTypeID, $dataSet['grnID']);
        $jEExistingID;
        foreach ($existingJournalEntry as $key => $jEntrySingleValue) {
            $jEExistingID = $jEntrySingleValue['journalEntryID'];
            //reverse existing journal Entry values
           
            //get finance accounts details
            $financeAccountDetails = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($jEntrySingleValue['financeAccountsID']);
           
            //reverse debit amount
            if(floatval($jEntrySingleValue['journalEntryAccountsDebitAmount']) > 0){
                
                $updateFinanceAcc = array(
                    'financeAccountsDebitAmount' => floatval($financeAccountDetails['financeAccountsDebitAmount']) - floatval($jEntrySingleValue['journalEntryAccountsDebitAmount']), 
                );
                
                //update finance account
                $updateFA = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->updateFinanceAccounts($updateFinanceAcc, $jEntrySingleValue['financeAccountsID']);

                //set data to update journalEntryAccount
                $updateJEAcc = array(
                    'journalEntryAccountsDebitAmount' => 0,
                    );
                
                //update journalEntryAccount
                $updateJEA = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryAccounts($updateJEAcc, $jEntrySingleValue['journalEntryAccountsID']);
            } 
            //reverse credit ammount
            if(floatval($jEntrySingleValue['journalEntryAccountsCreditAmount']) > 0){

                $updateFinanceAcc = array(
                    'financeAccountsCreditAmount' => floatval($financeAccountDetails['financeAccountsCreditAmount']) - floatval($jEntrySingleValue['journalEntryAccountsCreditAmount']),
                    );
                
                
                //update finance account
                $updateFA = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->updateFinanceAccounts($updateFinanceAcc, $jEntrySingleValue['financeAccountsID']);
                
                
                //set data to update journalEntryAccount
                $updateJEAcc = array(
                    'journalEntryAccountsCreditAmount' => 0,
                    );
                
                //update journalEntryAccount
                $updateJEA = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryAccounts($updateJEAcc, $jEntrySingleValue['journalEntryAccountsID']);
            }
        }
        $updatedDataSet = array(
            'documentTypeID' => '',
            'journalEntryDocumentID' => ''
            );
        //remove JE reference
        $updateJE = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntry($updatedDataSet,$jEExistingID);
        /////////////////////////////////



        //check whether supplier GRN Clearing account is set or not
        $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($dataSet['sID']);
        if(empty($supplierData->supplierGrnClearingAccountID)){
            $returnDataArray = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_GRN_SUPPLIER_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode)), 
                );
            return $returnDataArray;
        }
        $GRNClearingAccountID = $supplierData->supplierGrnClearingAccountID;

        $accountProduct = array();
        foreach (json_decode($dataSet['pr'], true) as $product) {
            if($product != null){
                //check whether Product Inventory Gl account id set or not
                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($product['pID']);
                if(empty($pData['productInventoryAccountID'])){
                    $returnDataArray = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])), 
                    );
                    return $returnDataArray;
                }
                
                //calculate product total without tax potion.
                //use to calculate product discount via value or percentage
                if($product['pDType'] == 'value'){
                    $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
                } else {
                    $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                }

                //add grn deliveryCharge to product according to price ratio.
                // if($dataSet['dC'] >0){
                //     $productTotal += ($dataSet['dC']*$productTotal)/ $allProductTotal;
                // }
                //set gl accounts for the journal entry
                if(isset($accountProduct[$pData['productInventoryAccountID']]['total'])){
                    $accountProduct[$pData['productInventoryAccountID']]['total'] += $productTotal;
                }else{
                    $accountProduct[$pData['productInventoryAccountID']]['total'] = $productTotal;
                    $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
                }
                
            }
        }

        $i=0;
        $journalEntryAccounts = array();
        $totalValueForSupplierAccount = 0;

        foreach ($accountProduct as $key => $value) {
                $totalValueForSupplierAccount+=$value['total'];
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['total'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$dataSet['gC'];
                $i++;
        }


        // $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
        // $journalEntryAccounts[$i]['financeAccountsID'] = $GRNClearingAccountID;
        // $journalEntryAccounts[$i]['financeGroupsID'] = '';
        // $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
        // $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $totalValueForSupplierAccount;
        // $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$dataSet['gC'].".";

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountsData = $glAccountExist->current();

        if($dataSet['dC'] > 0){
            if (!empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)) {
                if(isset($accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'])){
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'] += $dataSet['dC'];
                }else{
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'] = $dataSet['dC'];
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['accountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                }


                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;;
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode;

                $totalValueForSupplierAccount+= $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'];
            } else {
                $returnDataArray = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_DELI_CHARGE_ACCOUNT'), 
                );
                return $returnDataArray;
            }
        }


        $inc = (!empty($dataSet['dC'])) ?  $i +1 : $i;

        $journalEntryAccounts[$inc]['fAccountsIncID'] = $inc;
        $journalEntryAccounts[$inc]['financeAccountsID'] = $GRNClearingAccountID;
        $journalEntryAccounts[$inc]['financeGroupsID'] = '';
        $journalEntryAccounts[$inc]['journalEntryAccountsDebitAmount'] = 0.00;
        $journalEntryAccounts[$inc]['journalEntryAccountsCreditAmount'] = $totalValueForSupplierAccount;
        $journalEntryAccounts[$inc]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode.".";


        //get journal entry reference number.
        $jeresult = $this->getReferenceNoForLocation('30', $locationID);
        $jelocationReferenceID = $jeresult['locRefID'];
        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

        $journalEntryData = array(
            'journalEntryAccounts' => $journalEntryAccounts,
            'journalEntryDate' => $dataSet['dD'],
            'journalEntryCode' => $JournalEntryCode,
            'journalEntryTypeID' => '',
            'journalEntryIsReverse' => 0,
            'journalEntryComment' => 'Journal Entry is posted when create GRN '.$dataSet['gC'].".",
            'documentTypeID' => 10,
            'journalEntryDocumentID' => $dataSet['grnID'],
            'ignoreBudgetLimit' => $ignoreBudgetLimit,
        );

        $resultData = $this->saveJournalEntry($journalEntryData);
        
        if($resultData['status']){
            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($dataSet['grnID'],10, 3);
            if(!$jEDocStatusUpdate['status']){
                return ['status' => false, 'msg' => $jEDocStatusUpdate['msg']];
            }

            foreach ($dataSet['dimensionData'] as $value) {
                if (!empty($value)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($value, $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                    if(!$saveRes['status']){
                        return ['status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']];
                    }   
                }
            }

            return array(
                'status' => true,
                'msg' => $this->getMessage('SUCC_JE_UPDATE'),
                );
        } else {
            return array(
                'status' => true,
                'msg' => $resultData['msg'],
                'data' => $resultData['data'],
                );
        }
    }

    public function validateEditedGrn($newDataSet, $grnID)
    {
        //this validation is temporary. not for all items. only for non-batch serials
        $existingProDetails = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductLocationProductDetailsByGrnID($grnID);
        $locationProductsIDs = [];
        $productCount = 0;
        foreach ($existingProDetails as $key => $value) {
            if(sizeof($locationProductsIDs) == 0 ){
                $locationProductsIDs[$value['locationProductID']] = 1;
            }else if(array_key_exists($value['locationProductID'], $locationProductsIDs)){
                $locationProductsIDs[$value['locationProductID']]++;
            } else {
                $locationProductsIDs[$value['locationProductID']] = 1;
            }
            
        }
        
        foreach ($newDataSet as $key => $value) {
            if($value != null){
                if($locationProductsIDs[$value['locationPID']]){
                    $locationProductsIDs[$value['locationPID']]--;
                } else {
                    $locationProductsIDs[$value['locationPID']] = 1;
                }
                
            }
        }
       
        foreach ($locationProductsIDs as $key => $value) {
            if($value > 0 ){
                $returnDataSet = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CANNOT_DELETE_ITEMS'),
                    );
                return $returnDataSet;
            } else if($value < 0){
                $returnDataSet = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CANNOT_ADD_ITEMS'),
                );
                return $returnDataSet;
            }
        }

        $returnDataSet = array(
            'status' => true,
        );
        return $returnDataSet;

    }

    private function getGrnProductSet($grnData)
    {
        $rowCount = 0;
        $tempProduct = [];
        $btchData = [];
        $serialData = [];
        $tempG = array();
        $taxDetails = [];

        foreach ($grnData as $key => $row) {

                $grnID = $row['grnID'];
                $tempG['gID'] = $row['grnID'];
                $tempG['gCd'] = $row['grnCode'];
                $tempG['gSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempG['gSID'] = $row['grnSupplierID'];
                $tempG['gSR'] = $row['grnSupplierReference'];
                $tempG['gRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempG['retriveLocation'] = $row['grnRetrieveLocation'];
                $tempG['gD'] = $this->convertDateToUserFormat($row['grnDate']);
                $tempG['gDC'] = $row['grnDeliveryCharge'];
                $tempG['gT'] = $row['grnTotal'];
                $tempG['gC'] = $row['grnComment'];
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);
                $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['grnProductPrice'], $productUom);
                $productQtyDetails = $this->getProductQuantityViaDisplayUom($row['grnProductTotalQty'], $productUom);

                if($row['grnUploadFlag']){
                    if(isset($tempProduct[$rowCount]['bP'][$row['productBatchID']]) && $row['productSerialID']){

                    } else if(!$row['productBatchID'] && $row['productSerialID'] && $tempProduct[$rowCount]['lPID'] == $row['locationProductID']){

                    } else {
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    }
                } else {
                    if($tempProduct[$rowCount]['lPID'] != $row['locationProductID']){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    } else if($tempProduct[$rowCount]['gPP'] != $unitPrice){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    }

                }
                $discSymbol = null;
                if($row['grnProductDiscountType'] == 'value'){
                    $discSymbol = $this->companyCurrencySymbol;
                } else if($row['grnProductDiscountType'] == 'percentage') {
                    $discSymbol = '%';
                }

                $tempProduct[$rowCount] = array(
                    'gPC' => $row['productCode'],
                    'gPN' => $row['productName'],
                    'lPID' => $row['locationProductID'],
                    'gPP' => $unitPrice,
                    'gPD' => $row['grnProductDiscount'],
                    'gPQ' => $productQtyDetails['quantity'],
                    'gPT' => $row['grnProductTotal'],
                    'gPUom' => $productQtyDetails['uomAbbr'],
                    'gPUomDecimal' => $row['uomDecimalPlace'],
                    'gPDT' => $discSymbol,
                    'productID' => $row['productID'],
                    'productUomID' => $row['uomID'],
                    'productPoID' => $row['grnProductDocumentId'],
                    'productType' => $row['productTypeID'],
                    'grnProductID' => $row['grnProductID'],
                    );

                if ($row['grnTaxID'] != NULL) {

                    $taxDetails[$row['grnTaxID']] = array(
                        'pTN' => $row['taxName'],
                        'pTP' => $row['grnTaxPrecentage'],
                        'pTA' => $row['grnTaxAmount'],
                        'TXID' => $row['grnTaxID'],

                        );
                    $tempProduct[$rowCount]['pT'] = $taxDetails;
                }

                if($row['productBatchID']){

                    $btchData[$row['productBatchID']] = array(
                        'bC' => $row['productBatchCode'],
                        'bED' => $row['productBatchExpiryDate'],
                        'bW' => $row['productBatchWarrantyPeriod'],
                        'bMD' => $row['productBatchManufactureDate'],
                        'bQ' => $row['grnProductQuantity'],
                        'bPrice' => $row['productBatchPrice'],
                        );
                    $tempProduct[$rowCount]['bP'] = $btchData;

                    if($row['productSerialID']){
                        $serialData[$row['productSerialID']] = array(
                            'sC' => $row['productSerialCode'],
                            'sW' => $row['productSerialWarrantyPeriod'],
                            'sWT' => $row['productSerialWarrantyPeriodType'],
                            'sED' => $row['productSerialExpireDate'],
                            'sBC' => $row['productBatchCode']
                            );
                        $tempProduct[$rowCount]['sP'] = $serialData;

                    }

                } else if($row['productSerialID']){
                    $serialData[$row['productSerialID']] = array(
                        'sC' => $row['productSerialCode'],
                        'sW' => $row['productSerialWarrantyPeriod'],
                        'sWT' => $row['productSerialWarrantyPeriodType'],
                        'sED' => $row['productSerialExpireDate'],
                        'sBC' => $row['productBatchCode']
                        );
                    $tempProduct[$rowCount]['sP'] = $serialData;
                }
                $tempG['gProducts'] = $tempProduct;
        }
        return $tempG;
    }

    public function getAllRelatedDocumentDetailsByGrnIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $grnID = $request->getPost('grnID');
            $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnBasicDetailsByGrnId($grnID);
            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPODetailsByPrId(null,$grnID);
            $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataCopiedFromGrnByPoId(null,$grnID);
            $purchaseReturnData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnDataByPoId(null,$grnID);
            $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDirectPoRelatedDebitNoteDataByPoId(null,$grnID);
            $piPaymentData = $this->CommonTable('SupplierPaymentsTable')->getPiPaymentDetailsByPoId(null, $grnID);
            $debitNotePaymentData = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentDetailsByPoId(null,$grnID);

            $dataExistsFlag = false;
            if ($grnData) {
                $grnData = array(
                    'type' => 'Grn',
                    'documentID' => $grnData['grnID'],
                    'code' => $grnData['grnCode'],
                    'amount' => number_format($grnData['grnTotal'], 2),
                    'issuedDate' => $grnData['grnDate'],
                    'created' => $grnData['createdTimeStamp'],
                );
                $grnDetails[] = $grnData;
                $dataExistsFlag = true;
            }

            if (isset($poData)) {
                foreach ($poData as $poDta) {
                    $purcOrderDta = array(
                        'type' => 'PurchaseOrder',
                        'documentID' => $poDta['purchaseOrderID'],
                        'code' => $poDta['purchaseOrderCode'],
                        'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                        'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                        'created' => $poDta['createdTimeStamp'],
                    );
                    $grnDetails[] = $purcOrderDta;
                    if (isset($poDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($piData)) {
                foreach ($piData as $piDirectDta) {
                    $piDeta = array(
                        'type' => 'PurchaseInvoice',
                        'documentID' => $piDirectDta['purchaseInvoiceID'],
                        'code' => $piDirectDta['purchaseInvoiceCode'],
                        'amount' => number_format($piDirectDta['purchaseInvoiceTotal'], 2),
                        'issuedDate' => $piDirectDta['purchaseInvoiceIssueDate'],
                        'created' => $piDirectDta['createdTimeStamp'],
                    );
                    $grnDetails[] = $piDeta;
                    if (isset($piDirectDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
           
            if (isset($purchaseReturnData)) {
                foreach ($purchaseReturnData as $prDta) {
                    $pReturnData = array(
                        'type' => 'PurchaseReturn',
                        'documentID' => $prDta['purchaseReturnID'],
                        'code' => $prDta['purchaseReturnCode'],
                        'amount' => number_format($prDta['purchaseReturnTotal'], 2),
                        'issuedDate' => $prDta['purchaseReturnDate'],
                        'created' => $prDta['createdTimeStamp'],
                    );
                    $grnDetails[] = $pReturnData;
                    if (isset($prDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($debitNoteData)) {
                foreach ($debitNoteData as $debitNoteDta) {
                    $dNData = array(
                        'type' => 'DebitNote',
                        'documentID' => $debitNoteDta['debitNoteID'],
                        'code' => $debitNoteDta['debitNoteCode'],
                        'amount' => number_format($debitNoteDta['debitNoteTotal'], 2),
                        'issuedDate' => $debitNoteDta['debitNoteDate'],
                        'created' => $debitNoteDta['createdTimeStamp'],
                    );
                    $grnDetails[] = $dNData;
                    if (isset($debitNoteDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($piPaymentData)) {
                foreach ($piPaymentData as $piPayDta) {
                    $piPayData = array(
                        'type' => 'SupplierPayment',
                        'documentID' => $piPayDta['outgoingPaymentID'],
                        'code' => $piPayDta['outgoingPaymentCode'],
                        'amount' => number_format($piPayDta['outgoingPaymentAmount'], 2),
                        'issuedDate' => $piPayDta['outgoingPaymentDate'],
                        'created' => $piPayDta['createdTimeStamp'],
                    );
                    $grnDetails[] = $piPayData;
                    if (isset($piPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($debitNotePaymentData)) {
                foreach ($debitNotePaymentData as $dNPayDta) {
                    $dNPaymentData = array(
                        'type' => 'DebitNotePayment',
                        'documentID' => $dNPayDta['debitNotePaymentID'],
                        'code' => $dNPayDta['debitNotePaymentCode'],
                        'amount' => number_format($dNPayDta['debitNotePaymentAmount'], 2),
                        'issuedDate' => $dNPayDta['debitNotePaymentDate'],
                        'created' => $dNPayDta['createdTimeStamp'],
                    );
                    $grnDetails[] = $dNPaymentData;
                    if (isset($dNPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($grnDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $grnDetails);

            $documentDetails = array(
                'grnDetails' => $grnDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

    /**
     * search non inventory active produts for dropdown
     */
    public function searchCompoundCostingTypesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $typeSearchKey = $searchrequest->getPost('searchKey');
            
            $costTypes = $this->CommonTable('Inventory\Model\CompoundCostTypeTable')->compoundCostTypeSearchByKey($typeSearchKey);

            $costTypesList = array();
            
            foreach ($costTypes as $costType) {
                $temp['value'] = $costType['compoundCostTypeID'];
                $temp['text'] = $costType['compoundCostTypeName'] . '-' . $costType['compoundCostTypeCode'];
                $costTypesList[] = $temp;
            }

            $this->data = array('list' => $costTypesList);
            return $this->JSONRespond();
        }
    }

    public function saveCompoundCostingAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_COMP_DATA_ERR');
            return $this->JSONRespond();
        }
        $data = $request->getPost();
        $this->beginTransaction();
        
        // get entity ID
        $entityID = $this->createEntity();
        // get reference number
        $refData = $this->getReferenceNoForLocation(35, $dataSet['location']);
        $rid = $refData["refNo"];
        $costingRefID = $refData["locRefID"];

        // set compound Costing template details
        $compoundCostingDataSet = [
            'compoundCostTemplateDescription' => $data['comment'],
            'compoundCostTemplateName' => $data['name'],
            'locationID'=> $data['location'],
            'entityID' => $entityID,
            'status' => 4,  // close status
            'compoundCostTemplateCostCalType' => $data['costType'],
            'compoundCostTemplateCode' => $rid
        ];

        $compoundCostTemplate = new CompoundCostTemplate();
        $compoundCostTemplate->exchangeArray($compoundCostingDataSet);
        // save compound Cost 
        $saveCompCostTemp = $this->CommonTable('Inventory\Model\CompoundCostTemplateTable')->compoundTemplateSave($compoundCostTemplate);
        
        if (!$saveCompCostTemp) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_COMP_TEMP_SAVE');
            return $this->JSONRespond();
        }

        // save cost Details
        foreach ($data['supplierCostSet'] as $key => $supplierCostValue) {
                $compoundCostDetails = [
                    'compoundCostTypeID' => $supplierCostValue['CostID'],
                    'amount' => floatval($supplierCostValue['Cost']),
                    'compoundCostTemplateID' => $saveCompCostTemp,
                    'supplierID' => $supplierCostValue['supID']
                ];
                $compCostData = new CompoundCostValue();
                $compCostData->exchangeArray($compoundCostDetails);
                // save to the CompoundCost value table
                $saveCompCostVal = $this->CommonTable('Inventory\Model\CompoundCostValueTable')->saveCompCostValue($compCostData);
                if (!$saveCompCostVal) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_COMP_VALUE_SAVE');
                    return $this->JSONRespond();
                }
                        
        }
        
        // calculate unit cost by selected cost calculate type
        
        // save item details
        $newDataArray = [];
        foreach ($data['products'] as $key => $itemValue) {
            $costDetails = $this->calculateUnitCostBySelectedType($data, $itemValue);
            // set calculated details to the product array
            $costItem = [
                'productID'=> $itemValue['pID'], 
                'locationProductID' => $itemValue['locationPID'],
                'compoundCostItemQty' => floatval($itemValue['pQuantity']),
                'compoundCostItemPurchasePrice' => floatval($itemValue['pUnitPrice']),
                'compoundCostItemCostAmount' => floatval($costDetails['unitCost']),
                'compoundCostItemTotalUnitCost' => floatval($costDetails['unitPriceWithCost']),
                'compoundCostItemWiseTotalCost' => floatval($costDetails['itemWiseTotalCost']),
                'compoundCostTemplateID' => $saveCompCostTemp,
                'supplierID' => $itemValue['supID'],
                 
            ];

            $costItemExchange = new CompoundCostItem();
            $costItemExchange->exchangeArray($costItem);
            
            // save to the compound item table
            $saveItem = $this->CommonTable('Inventory\Model\CompoundCostItemTable')->saveCompoundItem($costItemExchange);
            if (!$saveItem) {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_COMP_ITEM_SAVE');
                return $this->JSONRespond();
            } 
            $newDataArray[] = array_merge($data['products'][$key], $costDetails); 
        
        }
        $data['products'] = $newDataArray;
        
        $calRes = $this->calcutateGrnDetailsByCosting($data, $saveCompCostTemp, $rid);
        if (!$calRes['status']) {
            $this->rollback();
            $this->status = false;
            if ($calRes['data'] == "NotifyBudgetLimit" || $calRes['data'] == "BlockBudgetLimit") {
                $this->msg = $calRes['msg'];                        
            } else {
                $this->msg = $this->getMessage('ERR_COMP_ITEM_SAVE');
            }
            $this->data = $calRes['data'];
            return $this->JSONRespond();
        } 
        $this->updateReferenceNumber($costingRefID);
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_COMP_SAVE');
        $this->data = $saveCompCostTemp;
        return $this->JSONRespond();
    }

    public function calculateUnitCostBySelectedType($costData, $itemData)
    {   
        // if cost calculate type 1 mean quantity wise calculation
        if ($costData['costType'] == 1) {
            $perUnitCost = floatval($costData['totalCost'])/ floatval($costData['totalQty']);
            $itemTotalCost = floatval($perUnitCost)* floatval($itemData['pQuantity']);
            
        } else {
            $perUnitCost = ((floatval($costData['totalCost'])/ floatval($costData['totalItemPrice']))*floatval($itemData['productTotal']))/floatval($itemData['pQuantity']);
            $itemTotalCost = (floatval($costData['totalCost'])/ floatval($costData['totalItemPrice']))*floatval($itemData['productTotal']);
            
        }
        $itemTotalUnitCost = floatval($perUnitCost) + $itemData['pUnitPrice'];
        return [
            'unitCost' => $perUnitCost,
            'unitPriceWithCost' => $itemTotalUnitCost,
            'itemWiseTotalCost' => $itemTotalCost,  
        ];
    }

    public function calcutateGrnDetailsByCosting($dataSet, $savedID, $referenceCode)
    {
        // calculate per unit cost
        $perUnitCost = floatval($dataSet['totalCost'])/floatval($dataSet['totalQty']);
        $dateFormat = $this->getUserDateFormat();
        $todayDateTime = $this->getUserDateTime();
        $todayDate = date('Y-m-d', strtotime($todayDateTime));

        // get item details for supplier
        foreach (array_filter($dataSet['itemSuppliers']) as $key => $value) {
            $grnDataSet = [];
            $productSet = [];
            $totalValue = 0;
            $supWiseQty = 0;
            $supWiseTotalCost = 0;
            foreach ($dataSet['products'] as $key => $proValue) {
                if ($value == $proValue['supID']) {
                    $productSet[] = [
                        'locationPID' => $proValue['locationPID'],
                        'pID' => $proValue['pID'],
                        'pCode' => $proValue['pCode'],
                        'pName' => $proValue['pName'],
                        'pQuantity' => floatval($proValue['pQuantity']),
                        'pDiscount' => 0,
                        'pUnitPrice' => floatval($proValue['productTotal'])/floatval($proValue['pQuantity']),
                        'pUom' => $proValue['uomID'],
                        'pTotal' => floatval($proValue['productTotal']),
                        'pTax' => [],
                        'bProducts' => [],
                        'sProducts' => [],
                        'productType' => $proValue['proType'],
                        'pDType' => null,
                        'poID' => (!empty($dataSet['selectedPO'])) ? $dataSet['selectedPO'] : null,
                        'grnProductID' => null,
                        'unitCost' => floatval($proValue['unitCost']),
                        'unitPriceWithCost' => floatval($proValue['unitPriceWithCost']),
                    ];
                    $totalValue += floatval($proValue['productTotal']);
                    $supWiseQty += floatval($proValue['pQuantity']);
                    $supWiseTotalCost += floatval($proValue['itemWiseTotalCost']);
                }
            }
                
            // get grn Reference number
            $refData = $this->getReferenceNoForLocation(10, $dataSet['location']);
            $rid = $refData["refNo"];
            $locationReferenceID = $refData["locRefID"];
            $grnDataSet = [
                'sID' => $value,
                'gC' => $rid,
                'dD' => $todayDate,
                'sR' => null,
                'rL' => $dataSet['location'],
                'cm' => 'Document Genarated by landing cost # '.$referenceCode,
                'pr' => json_encode($productSet),
                'dC' => null,
                'fT' => $totalValue,
                'sT' => 0,
                'lRID' => $locationReferenceID,
                'uploadDocFlag' => false,
                'compoundCostID' => null,
                'landingCost' => floatval($itemWiseTotalCost),
                'landedPerUnitCost' => $perUnitCost,
                'startByPoID' =>(!empty($dataSet['selectedPO'])) ? $dataSet['selectedPO'] : null
            ];
            
            $savedGrnDetails = $this->createGrnByCompoundCost($grnDataSet, $dataSet['ignoreBudgetLimit']);
            // save grn and Landing Costing Details 
            if ($savedGrnDetails['status']) {
                $compCostGrnData = [
                    'compoundCostTemplateID' => $savedID,
                    'grnID' => $savedGrnDetails['data']['grnID']
                ];
                $costGrn = new CompoundCostGrn();
                $costGrn->exchangeArray($compCostGrnData);
                $saveCostGrn = $this->CommonTable('Inventory\Model\CompoundCostGrnTable')->saveCompoundCostGrn($costGrn);
            } else {
                return ['status' => false, 'msg' => $savedGrnDetails['msg'], 'data' => $savedGrnDetails['data']];
            }

                    
        }

        // after all we need to create purchase invoices.

        $pI = $this->getServiceLocator()->get("PurchaseInvoiceController");
        $piStatus =  $pI->savePiByLandedCost($dataSet, $savedID, $referenceCode);

        if ($piStatus) {
            return ['status' => true];
        } else {
            return ['status' => false];
        }        
            
    }

    public function getCompoundCostDetailsAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_COMP_DATA_ERR');
            return $this->JSONRespond();
        }

        // get compoundCosting Details for given id
        $compCostDetails = $this->CommonTable('Inventory\Model\CompoundCostTemplateTable')->getCompoundCostDetails($request->getPost('id'));
        $locationID;
        $compCostItemDetails = [];
        foreach ($compCostDetails as $key => $compValue) {
            // get locationProductDetails
            $locationProducts[$compValue['productID']] = $this->getLocationProductDetails($compValue['locationID'], $compValue['productID']);
            $compCostItemDetails[$compValue['productID']] = $compValue;
            $locationID = $compValue['locationID'];
        }
        // get location Details by locationID
        $locationDetails = $this->CommonTable('Settings\Model\LocationTable')->getLocation($locationID);
        $retrunArray = [
            'costingDetails' => $compCostItemDetails,
            'locationProductDetails' => $locationProducts,
            'locationDetails' => $locationDetails,
        ];

        $this->status = true;
        $this->data = $retrunArray;
        return $this->JSONRespond();
    }

    public function createGrnByCompoundCost($post, $ignoreBudgetLimit)
    {
        
        $poProductsUncopiedArr = [];
        
        $grnCode = $post['gC'];
        $locationReferenceID = $post['lRID'];
        $locationID = $post['rL'];
        $documentReference = $post['documentReference'];

        $copiedFromPo = FALSE;

        $poProductsUncopiedArr = [];
        $poProductsUncopiedArrForGrnApproval = [];
        if ($post['startByPoID'] != '') {
            $copiedFromPo = TRUE;
            //get purchase order product details
            $poProducts = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getUnCopiedPoProducts($post['startByPoID']);
            foreach ($poProducts as $product) {
                $restQty = $product['purchaseOrderProductQuantity'] - $product['purchaseOrderProductCopiedQuantity'];
                $restQty = ($restQty < 0) ? 0 : $restQty;
                $poProductsUncopiedArr[$product['locationProductID']] = $restQty;
                $poProductsUncopiedArrForGrnApproval[$product['locationProductID']] = [
                    'restQty' => $restQty,
                    'price' => $product['purchaseOrderProductPrice']
                ];
            }
        }

        $this->beginTransaction();
// check if a grn from the same grn Code exists if exist add next number to grn code
        while ($grnCode) {
            if ($this->CommonTable('Inventory\Model\GrnTable')->checkGrnByCode($grnCode)->current()) {
                if ($locationReferenceID) {
                    $newGrnCode = $this->getReferenceNumber($locationReferenceID);
                    if ($newGrnCode == $grnCode) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $grnCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $grnCode = $newGrnCode;
                    }
                } else {
                    $this->rollback();
                    return $dataSet = [
                        'status' => false,
                        'msg' => $this->getMessage('ERR_GRN_EXIST'),
                    ];
                    
                }
            } else {
                break;
            }
        }
        $entityID = $this->createEntity();
        $grnData = array(
            'grnSupplierID' => $post['sID'],
            'grnSupplierReference' => $post['sR'],
            'grnCode' => $grnCode,
            'grnRetrieveLocation' => $locationID,
            'grnDate' => $this->convertDateToStandardFormat($post['dD']),
            'grnDeliveryCharge' => $post['dC'],
            'grnTotal' => $post['fT'],
            'grnComment' => $post['cm'],
            'grnShowTax' => $post['sT'],
            'entityID' => $entityID,
            'grnUploadFlag' => filter_var($post['uploadDocFlag'], FILTER_VALIDATE_BOOLEAN),
        );
        $grnM = new Grn();
        $grnM->exchangeArray($grnData);

//save the grn details to the grn table
        $grnID = $this->CommonTable('Inventory\Model\GrnTable')->saveGrn($grnM);

        //check whether grn save or not
        if(!$grnID){
            $this->rollback();
            return $dataSet = [
                'status' => false,
                'msg' => $this->getMessage('ERR_GRN_CREATE'),
            ];
        }
        
//update the reference number
        $this->updateReferenceNumber($locationReferenceID);
//loop through the products

        $allProductTotal = 0;
        // remove empty values and decord data
        $proList = array_filter(json_decode($post['pr'], true));

        foreach ($proList as $product) {
            $locationPID = $product['locationPID'];

            $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
            $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
            $locationProductQuantity = $locationProductData->locationProductQuantity;
            $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

            $productType = $product['productType'];
            //use to calculate product discount via value or percentage
            if($product['pDType'] == 'value'){
                $discountValue = $product['pDiscount'];
                $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
            } else {
                $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
            }

            if ($copiedFromPo == TRUE) {
                $this->updatePoProductCopiedQty($locationPID, $post['startByPoID'], $product['pQuantity']);
            }

            $allProductTotal+=$productTotal;
            $productBaseQty = $product['pQuantity'];
            $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);

                $grnProductM = new GrnProduct();
                $grnProductData = array(
                    'grnID' => $grnID,
                    'locationProductID' => $locationPID,
                    'grnProductPrice' => $product['pUnitPrice'],
                    'grnProductTotalQty' => $productBaseQty,
                    'grnProductQuantity' => $productBaseQty,
                    'grnProductUom' => $product['pUom'],
                    'grnProductDiscountType' => $product['pDType'],
                    'grnProductDiscount' => $product['pDiscount'],
                    'grnProductTotal' => $product['pTotal']
                );

                if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > 0) {
                    $copiedQty = ($productBaseQty < $poProductsUncopiedArr[$locationPID]) ? $productBaseQty : $poProductsUncopiedArr[$locationPID];
                    $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                    $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                    $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                }

                $grnProductM->exchangeArray($grnProductData);
                $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);

                //save itemIn Details
                if ($productType != 2) {
                    $itemInData = array(
                        'itemInDocumentType' => 'Goods Received Note',
                        'itemInDocumentID' => $grnID,
                        'itemInLocationProductID' => $locationPID,
                        'itemInBatchID' => NULL,
                        'itemInSerialID' => NULL,
                        'itemInQty' => $productBaseQty,
                        'itemInPrice' => $product['unitPriceWithCost'],
                        'itemInDiscount' => $discountValue,
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                    );
                    $itemInModel = new ItemIn();
                    $itemInModel->exchangeArray($itemInData);
                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                }

                if (array_key_exists('pTax', $product) && $product['pTax'] != '') {
                    if (array_key_exists('tL', $product['pTax'])) {
                        foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                            $grnPTaxesData = array(
                                'grnID' => $grnID,
                                'grnProductID' => $insertedGrnProductID,
                                'grnTaxID' => $taxKey,
                                'grnTaxPrecentage' => $productTax['tP'],
                                'grnTaxAmount' => $productTax['tA']
                            );
                            $grnPTaxM = new GrnProductTax();
                            $grnPTaxM->exchangeArray($grnPTaxesData);
                            $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                        }
                    }
                }
                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                if ($productType == 2) {
                    $productBaseQty = 0;
                }
                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                $newPQtyData = array(
                    'locationProductQuantity' => $newPQty
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
            ///////////////////////////////////////////////////////////////////
            

            $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
            $locationPrTotalQty = 0;
            $locationPrTotalPrice = 0;
            $newItemInIds = array();
            if(count($itemInData) > 0){
                foreach ($itemInData as $key => $value) {
                    if($value->itemInID > $locationProductLastItemInID){
                        $newItemInIds[] = $value->itemInID;
                        $remainQty = $value->itemInQty - $value->itemInSoldQty;
                        if($remainQty > 0){
                            $itemInPrice = $remainQty*$value->itemInPrice;
                            $itemInDiscount = $remainQty*$value->itemInDiscount;
                            $locationPrTotalQty += $remainQty;
                            $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                        }
                        $locationProductLastItemInID = $value->itemInID;
                    }
                }
            }
            $locationPrTotalQty += $locationProductQuantity;
            $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

            if($locationPrTotalQty > 0){
                $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                $lpdata = array(
                    'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                    'locationProductLastItemInID' => $locationProductLastItemInID,
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                foreach ($newItemInIds as $inID) {
                    $intemInData = array(
                        'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                    );
                    $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                }
            }
        }

        //update PO status if copied from it
        if ($copiedFromPo == TRUE) {
            $this->checkAndUpdatePoStatus($post['startByPoID']);
        }

        //add document reference for document reference table
        if (count($documentReference) > 0) {
            foreach ($documentReference as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $data = array(
                        'sourceDocumentTypeId' => 10,
                        'sourceDocumentId' => $grnID,
                        'referenceDocumentTypeId' => $key,
                        'referenceDocumentId' => $value,
                    );
                    $documentReference = new DocumentReference();
                    $documentReference->exchangeArray($data);
                    $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                }
            }
        }
        // call product updated event
        $productIDs = array_map(function($element) {
            return $element['pID'];
        }, json_decode($post['pr'], true));

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];

        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        if ($this->useAccounting == 1) {
            //check whether supplier GRN Clearing account is set or not
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($post['sID']);
            if(empty($supplierData->supplierGrnClearingAccountID)){
                $this->rollback();
                return $dataSet = [
                    'status' => false,
                    'msg' => $this->getMessage('ERR_GRN_SUPPLIER_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode)),
                ];
                
            }
            $GRNClearingAccountID = $supplierData->supplierGrnClearingAccountID;

            $accountProduct = array();
            foreach ($proList as $product) {
                $cost = 0;
                //check whether Product Inventory Gl account id set or not
                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($product['pID']);
                if(empty($pData['productInventoryAccountID'])){
                    $this->rollback();
                    return $dataSet = [
                        'status' => false,
                        'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                    ];
                    
                }
                //calculate product total without tax potion.
                //use to calculate product discount via value or percentage
                if($product['pDType'] == 'value'){
                    $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
                } else {
                    $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                }

                //add grn deliveryCharge to product according to price ratio.
                // if($post['dC'] >0){
                //     $productTotal += ($post['dC']*$productTotal)/ $allProductTotal;
                // }
                // calculate landed cost for this item

                // $cost = floatval($product['unitCost'])*floatval($product['pQuantity']);
                // $productTotal += $cost;
                //set gl accounts for the journal entry
                if(isset($accountProduct[$pData['productInventoryAccountID']]['total'])){
                    $accountProduct[$pData['productInventoryAccountID']]['total'] += $productTotal;
                }else{
                    $accountProduct[$pData['productInventoryAccountID']]['total'] = $productTotal;
                    $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
                }
            }

            //create data array for the journal entry save.
            $i=0;
            $journalEntryAccounts = array();
            $totalValueForSupplierAccount = 0;

            foreach ($accountProduct as $key => $value) {
                $totalValueForSupplierAccount+=$value['total'];
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['total'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode;
                $i++;
            }

            // $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            // $journalEntryAccounts[$i]['financeAccountsID'] = $GRNClearingAccountID;
            // $journalEntryAccounts[$i]['financeGroupsID'] = '';
            // $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
            // $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $totalValueForSupplierAccount;
            // $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode.".";

            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountsData = $glAccountExist->current();

            if($post['dC'] > 0){
                if (!empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)) {
                    if(isset($accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'])){
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'] += $post['dC'];
                    }else{
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'] = $post['dC'];
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['accountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                    }


                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;;
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode;

                    $totalValueForSupplierAccount+= $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['total'];
                } else {
                    $this->rollback();
                    return $dataSet = [
                        'status' => false,
                        'msg' => $this->getMessage('ERR_DELI_CHARGE_ACCOUNT'),
                    ];
                }
            }


            $inc = (!empty($post['dC'])) ?  $i +1 : $i;

            $journalEntryAccounts[$inc]['fAccountsIncID'] = $inc;
            $journalEntryAccounts[$inc]['financeAccountsID'] = $GRNClearingAccountID;
            $journalEntryAccounts[$inc]['financeGroupsID'] = '';
            $journalEntryAccounts[$inc]['journalEntryAccountsDebitAmount'] = 0.00;
            $journalEntryAccounts[$inc]['journalEntryAccountsCreditAmount'] = $totalValueForSupplierAccount;
            $journalEntryAccounts[$inc]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode.".";


            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $grnData['grnDate'],
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create GRN '.$grnCode.".",
                'documentTypeID' => 10,
                'journalEntryDocumentID' => $grnID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

            $resultData = $this->saveJournalEntry($journalEntryData);
            
            if($resultData['status']){
                $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($grnID,10, 3);
                if(!$jEDocStatusUpdate['status']){
                    $this->rollback();
                    return $dataSet = [
                        'status' => false,
                        'msg' => $jEDocStatusUpdate['msg'],
                    ];
                }

                $this->commit();
                return $dataSet = [
                    'status' => true,
                    'data' => array('grnID' => $grnID),
                    'msg' => $this->getMessage('SUCC_GRN_CREATE', array($grnCode)),
                ];
                
            }else{
                $this->rollback();
                return $dataSet = [
                    'status' => false,
                    'msg' => $resultData['msg'],
                    'data' => $resultData['data'],
                ];
                
            }
        }else{
            $this->commit();
            return $dataSet = [
                'status' => true,
                'data' => array('grnID' => $grnID),
                'msg' => $this->getMessage('SUCC_GRN_CREATE', array($grnCode)),
            ];
        }

        
    }


    public function saveGRNDraft($post) {

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(43, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $grnCode = $rid;
        $locationReferenceID = $lrefID;
        $documentReference = $post['documentReference'];
        $dimensionData = $post['dimensionData'];
        $ignoreBudgetLimit = $post['ignoreBudgetLimit'];

        // $this->beginTransaction();
// check if a grn from the same grn Code exists if exist add next number to grn code
        while ($grnCode) {
            if ($this->CommonTable('Inventory\Model\DraftGrnTable')->checkGrnByCode($grnCode)->current()) {
                if ($locationReferenceID) {
                    $newGrnCode = $this->getReferenceNumber($locationReferenceID);
                    if ($newGrnCode == $grnCode) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $grnCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $grnCode = $newGrnCode;
                    }
                } else {
                    
                    $statusData = [
                        "status" => false,
                        "msg" => $this->getMessage('ERR_GRN_EXIST')
                    ];
                    return $statusData;
                }
            } else {
                break;
            }
        }
        $entityID = $this->createEntity();
        $post['isUseAccounting'] = $this->useAccounting;
        $actGrnData = json_encode($post);
        $hashValue = hash('md5', $rid);
        $grnData = array(
            'grnSupplierID' => $post['sID'],
            'grnSupplierReference' => $post['sR'],
            'grnCode' => $grnCode,
            'grnRetrieveLocation' => $post['rL'],
            'grnDate' => $this->convertDateToStandardFormat($post['dD']),
            'grnDeliveryCharge' => $post['dC'],
            'grnTotal' => $post['fT'],
            'grnComment' => $post['cm'],
            'grnShowTax' => $post['sT'],
            'entityID' => $entityID,
            'grnUploadFlag' => filter_var($post['uploadDocFlag'], FILTER_VALIDATE_BOOLEAN),
            'grnActGrnData' => $actGrnData,
            'grnHashValue' => $hashValue,
            'isGrnApproved' => 0
        );
        $grnM = new DraftGrn();
        $grnM->exchangeArray($grnData);

        //save the grn details to the grn table
        $grnID = $this->CommonTable('Inventory\Model\DraftGrnTable')->saveGrn($grnM);
        //check whether grn save or not
        if(!$grnID){
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GRN_CREATE');
            return $this->JSONRespond();
        }
        
        //update the reference number
        $this->updateReferenceNumber($locationReferenceID);
        //loop through the products

        $allProductTotal = 0;
        // remove empty values and decord data
        $proList = array_filter(json_decode($post['pr'], true));
        // var_dump($post['pr']).die();

        foreach ($proList as $product) {
            $locationPID = $product['locationPID'];

            $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
            $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
            $locationProductQuantity = $locationProductData->locationProductQuantity;
            $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

            $productType = $product['productType'];
            //use to calculate product discount via value or percentage
            if($product['pDType'] == 'value'){
                $discountValue = $product['pDiscount'];
                $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
            } else {
                $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
            }

            $allProductTotal+=$productTotal;
        //if copied from PO,update those products as copied.
            if ($copiedFromPo == TRUE) {
                $this->updatePoProductCopiedQty($locationPID, $post['startByPoID'], $product['pQuantity']);
            }
            $productBaseQty = $product['pQuantity'];
            $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);

        //if product is a batch Product

            // if (array_key_exists('bProducts', $product)) {
            if (sizeof($product['bProducts']) > 0) {
                $batchProductCount = 0;
                foreach ($product['bProducts'] as $bProduct) {

                    $batchProductCount++;
                    $bPData = array(
                        'locationProductID' => $locationPID,
                        'productBatchCode' => $bProduct['bCode'],
                        'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                        'productBatchWarrantyPeriod' => $bProduct['warnty'],
                        'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                        'productBatchQuantity' => $bProduct['bQty'] * $productConversionRate,
                        'productBatchPrice' => ($bProduct['price']*$productConversionRate),
                    );
                    $batchProduct = new DraftProductBatch();
                    $batchProduct->exchangeArray($bPData);

                    $insertedBID = $this->CommonTable('Inventory\Model\DraftProductBatchTable')->saveBatchProduct($batchProduct);

                    //if the product is batch and serial
                    // if (array_key_exists('sProducts', $product)) {
                    if(sizeof($product['sProducts']) > 0){
                        foreach ($product['sProducts'] as $sProduct) {
                            if (isset($sProduct['sBCode'])) {
                                if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                    $sPData = array(
                                        'locationProductID' => $locationPID,
                                        'productBatchID' => $insertedBID,
                                        'productSerialCode' => $sProduct['sCode'],
                                        'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                                        'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                        'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                                    );
                                    $serialPr = new DraftProductSerial();
                                    $serialPr->exchangeArray($sPData);
                                    $insertedSID = $this->CommonTable('Inventory\Model\DraftProductSerialTable')->saveSerialProduct($serialPr);
                                    $grnProductM = new DraftGrnProduct();
                                    $grnProductData = array(
                                        'grnID' => $grnID,
                                        'locationProductID' => $locationPID,
                                        'productBatchID' => $insertedBID,
                                        'productSerialID' => $insertedSID,
                                        'grnProductPrice' => $product['pUnitPrice'],
                                        'grnProductDiscountType' => $product['pDType'],
                                        'grnProductDiscount' => $product['pDiscount'],
                                        'grnProductTotalQty' => $productBaseQty,
                                        'grnProductQuantity' => $bProduct['bQty'],
                                        'grnProductUom' => $product['pUom'],
                                        'grnProductTotal' => $product['pTotal'],
                                        'rackID' => ''
                                    );

                                    if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > $serialProductCount) {
                                        $copiedQty = 1;
                                        $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                                        $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                                        $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                                    }

                                    $grnProductM->exchangeArray($grnProductData);
                                    $insertedGrnProductID = $this->CommonTable('Inventory\Model\DraftGrnProductTable')->saveGrnProduct($grnProductM);

                                    if (array_key_exists('pTax', $product)) {
                                        if (array_key_exists('tL', $product['pTax'])) {
                                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                $grnPTaxesData = array(
                                                    'grnID' => $grnID,
                                                    'grnProductID' => $insertedGrnProductID,
                                                    'grnTaxID' => $taxKey,
                                                    'grnTaxPrecentage' => $productTax['tP'],
                                                    'grnTaxAmount' => $productTax['tA']
                                                );
                                                $grnPTaxM = new DraftGrnProductTax();
                                                $grnPTaxM->exchangeArray($grnPTaxesData);
                                                $this->CommonTable('Inventory\Model\DraftGrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $grnProductM = new DraftGrnProduct();
                        $grnProductData = array(
                            'grnID' => $grnID,
                            'locationProductID' => $locationPID,
                            'productBatchID' => $insertedBID,
                            'grnProductPrice' => $product['pUnitPrice'],
                            'grnProductDiscountType' => $product['pDType'],
                            'grnProductDiscount' => $product['pDiscount'],
                            'grnProductTotalQty' => $productBaseQty,
                            'grnProductQuantity' => $bProduct['bQty'],
                            'grnProductUom' => $product['pUom'],
                            'grnProductTotal' => $product['pTotal'],
                            'rackID' => ''
                        );

                        if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > 0) {
                            $copiedQty = ($bProduct['bQty'] < $poProductsUncopiedArr[$locationPID]) ? $bProduct['bQty'] : $poProductsUncopiedArr[$locationPID];
                            $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                            $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                            $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                        }

                        $grnProductM->exchangeArray($grnProductData);
                        $insertedGrnProductID = $this->CommonTable('Inventory\Model\DraftGrnProductTable')->saveGrnProduct($grnProductM);
//save itemIn
                       
                        if (array_key_exists('pTax', $product) && $product['pTax'] != '') {
                            if (array_key_exists('tL', $product['pTax'])) {
                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                    $grnPTaxesData = array(
                                        'grnID' => $grnID,
                                        'grnProductID' => $insertedGrnProductID,
                                        'grnTaxID' => $taxKey,
                                        'grnTaxPrecentage' => $productTax['tP'],
                                        'grnTaxAmount' => $productTax['tA']
                                    );
                                    $grnPTaxM = new DraftGrnProductTax();
                                    $grnPTaxM->exchangeArray($grnPTaxesData);
                                    $this->CommonTable('Inventory\Model\DraftGrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                                }
                            }
                        }
                       

                    }
                }

            // } elseif (array_key_exists('sProducts', $product)) {
            } elseif (sizeof($product['sProducts']) > 0) {
            //if the product is a serial Product
                $serialProductCount = 0;
                foreach ($product['sProducts'] as $sProduct) {
                    $sPData = array(
                        'locationProductID' => $locationPID,
                        'productSerialCode' => $sProduct['sCode'],
                        'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                        'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                        'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                    );
                    $serialPr = new DraftProductSerial();
                    $serialPr->exchangeArray($sPData);
                    $insertedSID = $this->CommonTable('Inventory\Model\DraftProductSerialTable')->saveSerialProduct($serialPr);
                    $grnProductM = new DraftGrnProduct();
                    $grnProductData = array(
                        'grnID' => $grnID,
                        'locationProductID' => $locationPID,
                        'productSerialID' => $insertedSID,
                        'grnProductPrice' => $product['pUnitPrice'],
                        'grnProductDiscountType' => $product['pDType'],
                        'grnProductDiscount' => $product['pDiscount'],
                        'grnProductTotalQty' => $productBaseQty,
                        'grnProductQuantity' => 1,
                        'grnProductUom' => $product['pUom'],
                        'grnProductTotal' => $product['pTotal'],
                        'rackID' => ''
                    );

                    if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > $serialProductCount) {
                        $copiedQty = 1;
                        $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                        $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                        $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                    }

                    $grnProductM->exchangeArray($grnProductData);
                    $insertedGrnProductID = $this->CommonTable('Inventory\Model\DraftGrnProductTable')->saveGrnProduct($grnProductM);


                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $grnPTaxesData = array(
                                    'grnID' => $grnID,
                                    'grnProductID' => $insertedGrnProductID,
                                    'grnTaxID' => $taxKey,
                                    'grnTaxPrecentage' => $productTax['tP'],
                                    'grnTaxAmount' => $productTax['tA']
                                );
                                $grnPTaxM = new DraftGrnProductTax();
                                $grnPTaxM->exchangeArray($grnPTaxesData);
                                $this->CommonTable('Inventory\Model\DraftGrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                            }
                        }
                    }
                   
                    $serialProductCount++;
                }
            } else {
//If the product is a non batch and non serial product
                $grnProductM = new DraftGrnProduct();
                $grnProductData = array(
                    'grnID' => $grnID,
                    'locationProductID' => $locationPID,
                    'grnProductPrice' => $product['pUnitPrice'],
                    'grnProductTotalQty' => $productBaseQty,
                    'grnProductQuantity' => $productBaseQty,
                    'grnProductUom' => $product['pUom'],
                    'grnProductDiscountType' => $product['pDType'],
                    'grnProductDiscount' => $product['pDiscount'],
                    'grnProductTotal' => $product['pTotal']
                );

                if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > 0) {
                    $copiedQty = ($productBaseQty < $poProductsUncopiedArr[$locationPID]) ? $productBaseQty : $poProductsUncopiedArr[$locationPID];
                    $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                    $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                    $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                }

                $grnProductM->exchangeArray($grnProductData);
                $insertedGrnProductID = $this->CommonTable('Inventory\Model\DraftGrnProductTable')->saveGrnProduct($grnProductM);



                if (array_key_exists('pTax', $product) && $product['pTax'] != '') {
                    if (array_key_exists('tL', $product['pTax'])) {
                        foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                            $grnPTaxesData = array(
                                'grnID' => $grnID,
                                'grnProductID' => $insertedGrnProductID,
                                'grnTaxID' => $taxKey,
                                'grnTaxPrecentage' => $productTax['tP'],
                                'grnTaxAmount' => $productTax['tA']
                            );
                            $grnPTaxM = new GrnProductTax();
                            $grnPTaxM->exchangeArray($grnPTaxesData);
                            $this->CommonTable('Inventory\Model\DraftGrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                        }
                    }
                }
               
            }
        }

        $statusData = [
            "status" => true,
            "data" => ['grnID' => $grnID, 'grnCode' => $rid, 'hashValue' => $hashValue],
            "msg" => $this->getMessage('SUCC_GRN_CREATE', array($grnCode)),
        ];

        return $statusData;
    }


    public function checkCopiedPoForApprovalWF($poProductsData, $grnProducts) {

        if (sizeof($poProductsData) == sizeof($grnProducts)) {


            foreach ($grnProducts as $key => $p) {
                
                if (array_key_exists($p['locationPID'], $poProductsData)) {
                    if (floatval($p['pQuantity']) <= floatval($poProductsData[$p['locationPID']]['restQty']) && floatval($p['pUnitPrice']) == floatval($poProductsData[$p['locationPID']]['price'])) {

                        continue;
                    } else {
                        return true;

                    }

                } else {
                    return true;

                }
                
            }

            return false;
        } else {
            return true;

        }
    }

    public function sendApproverEmailAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $grnId = $request->getPost('grnId');
            $grnCode = $request->getPost('grnCode');
            $approvers = $request->getPost('approvers');
            $token = $request->getPost('token');

            if ($grnId && $grnCode && $approvers && $token) {
                $this->sendGrnApproversEmail($grnId, $grnCode, $approvers, $token);
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_GRN_SEND_FOR_WF');
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_GRN_SEND_FOR_WF');
                return $this->JSONRespond();
            }
        }
    }

    private function sendGrnApproversEmail($grnId, $grnCode, $approvers, $token)
    {   
        foreach ($approvers as $approver) {

            $approverData =  $this->CommonTable('Settings\Model\ApproverTable')->getApproverByID($approver)->current();

            $approveUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/api/grn/approve/?grnId=' . $grnId . '&action=approve&token=' . $token;
            $rejectUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/api/grn/approve/?grnId=' . $grnId . '&action=reject&token=' . $token;
            $approveGrnEmailView = new ViewModel(array(
                'name' => $approverData['firstName'] . ' ' . $approverData['lastName'],
                'grnCode' => $grnCode,
                'approveLink' => $approveUrl,
                'rejectLink' => $rejectUrl,
            ));
            $approveGrnEmailView->setTemplate('/core/email/grn-approve');
            $renderedEmailHtml = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($approveGrnEmailView);
            $documentType = 'Goods Received Note';
            $this->sendDocumentEmail($approverData['email'], "Request for Approve - Goods Received Note - $grnCode", $renderedEmailHtml, $documentType, $grnId, null, false, true);
        }
    }


    public function approveAction()
    {
        
        $action = $_GET['action'];
        $grnId = $_GET['grnId'];
        $token = $_GET['token'];
        $grnDetails = $this->CommonTable("Inventory\Model\DraftGrnTable")->getDraftGrnByGrnID($grnId)->current();
        $grnCode = $grnDetails['grnCode'];
        $this->beginTransaction();
        if ($grnDetails['grnHashValue'] == $token) {
            if ($action == 'approve') {
                if ($grnDetails['isGrnApproved'] == 0 && $grnDetails['status'] == 13) {
                    $this->commit();
                    echo "Grn $grnCode is already rejected, you cannot approve any more..!";
                    exit();
                }

                if ($grnDetails['isGrnApproved'] == 0) {

                    $data = json_decode($grnDetails['grnActGrnData']);
                    $convertDraftToGrn = $this->updateDraftGrnToGrn($data);

                    if ($convertDraftToGrn) {

                        $draftData = [
                            'isGrnApproved' => 1,
                            'status' => 16,
                            'grnID' => $grnDetails['grnID'],
                        ];

                        $draftRes = $this->CommonTable('Inventory\Model\DraftGrnTable')->updateDraftGrn($draftData);


                        $this->commit();
                        echo "Good Receive Note approved..!";
                        exit();
                    } else {
                        $this->rollback();
                        echo "Error Occured while approve Good Receive Note..!";
                        exit();

                    }
                    
                } else {
                    $this->commit();
                    echo "Good Receive Note $grnCode is already approved..!";
                    exit();
                }
            } else {
                if ($grnDetails['isGrnApproved'] == 1) {
                    $this->commit();
                    echo "Good Receive Note $grnCode is already approved, you cannot reject any more..!";
                    exit();
                }

                if ($grnDetails['isGrnApproved'] == 0 && $grnDetails['status'] == 13) {
                    $this->commit();
                    echo "Good Receive Note $grnCode is already rejected..!";
                    exit();
                } else {
                    $draftData = [
                        'isGrnApproved' => 0,
                        'status' => 13,
                        'grnID' => $grnDetails['grnID'],
                    ];

                    $draftRes = $this->CommonTable('Inventory\Model\DraftGrnTable')->updateDraftGrn($draftData);

                    if (!$draftRes){
                        $this->rollback();
                        echo $this->getMessage('ERR_PREQ_DRAFT_TO_PREQ');
                        exit();
                    } else{
                        $this->commit();
                        echo "Good Receive Note $grnCode rejected..!";
                        exit();
                    }
                }
            }
        } else {
            $this->rollback();
            echo 'Token Mismatch';
            exit();
        }
    }


    public function updateDraftGrnToGrn($post) {
        $post = (array) $post;
    
        //check whether GRN is created via a Purchase Order
        $poProductsUncopiedArr = [];
        if (!empty($post['startByPoID'])) {
            $copiedFromPo = TRUE;
            //get purchase order product details
            $poProducts = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getUnCopiedPoProducts($post['startByPoID']);
            foreach ($poProducts as $product) {
                $restQty = $product['purchaseOrderProductQuantity'] - $product['purchaseOrderProductCopiedQuantity'];
                $restQty = ($restQty < 0) ? 0 : $restQty;
                $poProductsUncopiedArr[$product['locationProductID']] = $restQty;
            }
        } else {
            $copiedFromPo = FALSE;
        }

        $locationID = $post['rL'];
        $refData = $this->getReferenceNoForLocation(10, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];


        
        $grnCode = $rid;
        $locationReferenceID = $lrefID;
        $documentReference = (!empty($post['documentReference'])) ? $post['documentReference'] : [];
        $dimensionData = (!empty($post['dimensionData'])) ? $post['dimensionData'] : [];
        $ignoreBudgetLimit = $post['ignoreBudgetLimit'];

        $this->beginTransaction();
        // check if a grn from the same grn Code exists if exist add next number to grn code
        while ($grnCode) {
            if ($this->CommonTable('Inventory\Model\GrnTable')->checkGrnByCode($grnCode)->current()) {
                if ($locationReferenceID) {
                    $newGrnCode = $this->getReferenceNumber($locationReferenceID);
                    if ($newGrnCode == $grnCode) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $grnCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $grnCode = $newGrnCode;
                    }
                } else {
                    error_log($this->getMessage('ERR_GRN_EXIST'));
                    return false;
                }
            } else {
                break;
            }
        }
        $entityID = $this->createEntity();
        $grnData = array(
            'grnSupplierID' => $post['sID'],
            'grnSupplierReference' => $post['sR'],
            'grnCode' => $grnCode,
            'grnRetrieveLocation' => $locationID,
            'grnDate' => $this->convertDateToStandardFormat($post['dD']),
            'grnDeliveryCharge' => $post['dC'],
            'grnTotal' => $post['fT'],
            'grnComment' => $post['cm'],
            'grnShowTax' => $post['sT'],
            'entityID' => $entityID,
            'grnUploadFlag' => filter_var($post['uploadDocFlag'], FILTER_VALIDATE_BOOLEAN),
            'isApprovedThroughWF' => 1,
        );
        $grnM = new Grn();
        $grnM->exchangeArray($grnData);

        //save the grn details to the grn table
        $grnID = $this->CommonTable('Inventory\Model\GrnTable')->saveGrn($grnM);
        //check whether grn save or not
        if(!$grnID){
            error_log($this->getMessage('ERR_GRN_CREATE'));
            return false;
        }
        // save Compound Costing Grn 
        if ($post['compoundCostID'] != '') {
            $compCostGrnData = [
                'compoundCostTemplateID' => $post['compoundCostID'],
                'grnID' => $grnID
            ];
            $costGrn = new CompoundCostGrn();
            $costGrn->exchangeArray($compCostGrnData);
            $saveCostGrn = $this->CommonTable('Inventory\Model\CompoundCostGrnTable')->saveCompoundCostGrn($costGrn);
        }
        //update the reference number
        $this->updateReferenceNumber($locationReferenceID);
        //loop through the products

        $allProductTotal = 0;
        // remove empty values and decord data
        $proList = array_filter(json_decode($post['pr'], true));

        foreach ($proList as $product) {
            $locationPID = $product['locationPID'];

            $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
            $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
            $locationProductQuantity = $locationProductData->locationProductQuantity;
            $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

            $productType = $product['productType'];
            //use to calculate product discount via value or percentage
            if($product['pDType'] == 'value'){
                $discountValue = $product['pDiscount'];
                $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
            } else {
                $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
            }

            $allProductTotal+=$productTotal;
            //if copied from PO,update those products as copied.
            if ($copiedFromPo == TRUE) {
                $this->updatePoProductCopiedQty($locationPID, $post['startByPoID'], $product['pQuantity']);
            }
            $productBaseQty = $product['pQuantity'];
            $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);

            //if product is a batch Product

            // if (array_key_exists('bProducts', $product)) {
            if (sizeof($product['bProducts']) > 0) {
                $batchProductCount = 0;
                foreach ($product['bProducts'] as $bProduct) {

                    $batchProductCount++;
                    $bPData = array(
                        'locationProductID' => $locationPID,
                        'productBatchCode' => $bProduct['bCode'],
                        'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                        'productBatchWarrantyPeriod' => $bProduct['warnty'],
                        'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                        'productBatchQuantity' => $bProduct['bQty'] * $productConversionRate,
                        'productBatchPrice' => ($bProduct['price']*$productConversionRate),
                    );
                    $batchProduct = new ProductBatch();
                    $batchProduct->exchangeArray($bPData);

                    $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);

//if the product is batch and serial
                    // if (array_key_exists('sProducts', $product)) {
                    if(sizeof($product['sProducts']) > 0){
                        foreach ($product['sProducts'] as $sProduct) {
                            if (isset($sProduct['sBCode'])) {
                                if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                    $sPData = array(
                                        'locationProductID' => $locationPID,
                                        'productBatchID' => $insertedBID,
                                        'productSerialCode' => $sProduct['sCode'],
                                        'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                                        'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                        'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                                    );
                                    $serialPr = new ProductSerial();
                                    $serialPr->exchangeArray($sPData);
                                    $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                    $grnProductM = new GrnProduct();
                                    $grnProductData = array(
                                        'grnID' => $grnID,
                                        'locationProductID' => $locationPID,
                                        'productBatchID' => $insertedBID,
                                        'productSerialID' => $insertedSID,
                                        'grnProductPrice' => $product['pUnitPrice'],
                                        'grnProductDiscountType' => $product['pDType'],
                                        'grnProductDiscount' => $product['pDiscount'],
                                        'grnProductTotalQty' => $productBaseQty,
                                        'grnProductQuantity' => $bProduct['bQty'],
                                        'grnProductUom' => $product['pUom'],
                                        'grnProductTotal' => $product['pTotal'],
                                        'rackID' => ''
                                    );

                                    if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > $serialProductCount) {
                                        $copiedQty = 1;
                                        $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                                        $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                                        $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                                    }

                                    $grnProductM->exchangeArray($grnProductData);
                                    $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);
//save the product details to the itemInTable

                                    $itemInData = array(
                                        'itemInDocumentType' => 'Goods Received Note',
                                        'itemInDocumentID' => $grnID,
                                        'itemInLocationProductID' => $locationPID,
                                        'itemInBatchID' => $insertedBID,
                                        'itemInSerialID' => $insertedSID,
                                        'itemInQty' => 1,
                                        'itemInPrice' => $product['pUnitPrice'],
                                        'itemInDiscount' => $discountValue,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                    if (array_key_exists('pTax', $product)) {
                                        if (array_key_exists('tL', $product['pTax'])) {
                                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                $grnPTaxesData = array(
                                                    'grnID' => $grnID,
                                                    'grnProductID' => $insertedGrnProductID,
                                                    'grnTaxID' => $taxKey,
                                                    'grnTaxPrecentage' => $productTax['tP'],
                                                    'grnTaxAmount' => $productTax['tA']
                                                );
                                                $grnPTaxM = new GrnProductTax();
                                                $grnPTaxM->exchangeArray($grnPTaxesData);
                                                $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                                            }
                                        }
                                    }
                                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                    $newPQtyData = array(
                                        'locationProductQuantity' => $newPQty
                                    );
//Update location Product Quantity
                                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                }
                            }
                        }
                    } else {
                        $grnProductM = new GrnProduct();
                        $grnProductData = array(
                            'grnID' => $grnID,
                            'locationProductID' => $locationPID,
                            'productBatchID' => $insertedBID,
                            'grnProductPrice' => $product['pUnitPrice'],
                            'grnProductDiscountType' => $product['pDType'],
                            'grnProductDiscount' => $product['pDiscount'],
                            'grnProductTotalQty' => $productBaseQty,
                            'grnProductQuantity' => $bProduct['bQty'],
                            'grnProductUom' => $product['pUom'],
                            'grnProductTotal' => $product['pTotal'],
                            'rackID' => ''
                        );

                        if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > 0) {
                            $copiedQty = ($bProduct['bQty'] < $poProductsUncopiedArr[$locationPID]) ? $bProduct['bQty'] : $poProductsUncopiedArr[$locationPID];
                            $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                            $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                            $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                        }

                        $grnProductM->exchangeArray($grnProductData);
                        $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);
//save itemIn
                        $itemInData = array(
                            'itemInDocumentType' => 'Goods Received Note',
                            'itemInDocumentID' => $grnID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => $insertedBID,
                            'itemInSerialID' => NULL,
                            'itemInQty' => $bProduct['bQty'],
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if (array_key_exists('pTax', $product) && $product['pTax'] != '') {
                            if (array_key_exists('tL', $product['pTax'])) {
                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                    $grnPTaxesData = array(
                                        'grnID' => $grnID,
                                        'grnProductID' => $insertedGrnProductID,
                                        'grnTaxID' => $taxKey,
                                        'grnTaxPrecentage' => $productTax['tP'],
                                        'grnTaxAmount' => $productTax['tA']
                                    );
                                    $grnPTaxM = new GrnProductTax();
                                    $grnPTaxM->exchangeArray($grnPTaxesData);
                                    $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                                }
                            }
                        }
                        if ($batchProductCount == 1) {
                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                            );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        }
                    }
                }

            // } elseif (array_key_exists('sProducts', $product)) {
            } elseif (sizeof($product['sProducts']) > 0) {
            //if the product is a serial Product
                $serialProductCount = 0;
                foreach ($product['sProducts'] as $sProduct) {
                    $sPData = array(
                        'locationProductID' => $locationPID,
                        'productSerialCode' => $sProduct['sCode'],
                        'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                        'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                        'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                    );
                    $serialPr = new ProductSerial();
                    $serialPr->exchangeArray($sPData);
                    $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                    $grnProductM = new GrnProduct();
                    $grnProductData = array(
                        'grnID' => $grnID,
                        'locationProductID' => $locationPID,
                        'productSerialID' => $insertedSID,
                        'grnProductPrice' => $product['pUnitPrice'],
                        'grnProductDiscountType' => $product['pDType'],
                        'grnProductDiscount' => $product['pDiscount'],
                        'grnProductTotalQty' => $productBaseQty,
                        'grnProductQuantity' => 1,
                        'grnProductUom' => $product['pUom'],
                        'grnProductTotal' => $product['pTotal'],
                        'rackID' => ''
                    );

                    if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > $serialProductCount) {
                        $copiedQty = 1;
                        $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                        $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                        $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                    }

                    $grnProductM->exchangeArray($grnProductData);
                    $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);
//save serial product itemIn
                    $itemInData = array(
                        'itemInDocumentType' => 'Goods Received Note',
                        'itemInDocumentID' => $grnID,
                        'itemInLocationProductID' => $locationPID,
                        'itemInBatchID' => NULL,
                        'itemInSerialID' => $insertedSID,
                        'itemInQty' => 1,
                        'itemInPrice' => $product['pUnitPrice'],
                        'itemInDiscount' => $discountValue,
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                    );
                    $itemInModel = new ItemIn();
                    $itemInModel->exchangeArray($itemInData);
                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $grnPTaxesData = array(
                                    'grnID' => $grnID,
                                    'grnProductID' => $insertedGrnProductID,
                                    'grnTaxID' => $taxKey,
                                    'grnTaxPrecentage' => $productTax['tP'],
                                    'grnTaxAmount' => $productTax['tA']
                                );
                                $grnPTaxM = new GrnProductTax();
                                $grnPTaxM->exchangeArray($grnPTaxesData);
                                $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                            }
                        }
                    }
                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                    $serialProductCount++;
                }
            } else {
//If the product is a non batch and non serial product
                $grnProductM = new GrnProduct();
                $grnProductData = array(
                    'grnID' => $grnID,
                    'locationProductID' => $locationPID,
                    'grnProductPrice' => $product['pUnitPrice'],
                    'grnProductTotalQty' => $productBaseQty,
                    'grnProductQuantity' => $productBaseQty,
                    'grnProductUom' => $product['pUom'],
                    'grnProductDiscountType' => $product['pDType'],
                    'grnProductDiscount' => $product['pDiscount'],
                    'grnProductTotal' => $product['pTotal']
                );

                if ($copiedFromPo && array_key_exists($locationPID, $poProductsUncopiedArr) && $poProductsUncopiedArr[$locationPID] > 0) {
                    $copiedQty = ($productBaseQty < $poProductsUncopiedArr[$locationPID]) ? $productBaseQty : $poProductsUncopiedArr[$locationPID];
                    $grnProductData['grnProductDocumentId'] = $post['startByPoID'];
                    $grnProductData['grnProductDocumentTypeId'] = self::PO_DOCUMENT_TYPE_ID;
                    $grnProductData['grnProductDoumentTypeCopiedQuantity'] = $copiedQty;
                }

                $grnProductM->exchangeArray($grnProductData);
                $insertedGrnProductID = $this->CommonTable('Inventory\Model\GrnProductTable')->saveGrnProduct($grnProductM);


                //save itemIn Details
                if ($productType != 2) {
                    $itemInData = array(
                        'itemInDocumentType' => 'Goods Received Note',
                        'itemInDocumentID' => $grnID,
                        'itemInLocationProductID' => $locationPID,
                        'itemInBatchID' => NULL,
                        'itemInSerialID' => NULL,
                        'itemInQty' => $productBaseQty,
                        'itemInPrice' => $product['pUnitPrice'],
                        'itemInDiscount' => $discountValue,
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                    );
                    $itemInModel = new ItemIn();
                    $itemInModel->exchangeArray($itemInData);
                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                }

                if (array_key_exists('pTax', $product) && $product['pTax'] != '') {
                    if (array_key_exists('tL', $product['pTax'])) {
                        foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                            $grnPTaxesData = array(
                                'grnID' => $grnID,
                                'grnProductID' => $insertedGrnProductID,
                                'grnTaxID' => $taxKey,
                                'grnTaxPrecentage' => $productTax['tP'],
                                'grnTaxAmount' => $productTax['tA']
                            );
                            $grnPTaxM = new GrnProductTax();
                            $grnPTaxM->exchangeArray($grnPTaxesData);
                            $this->CommonTable('Inventory\Model\GrnProductTaxTable')->saveGrnProductTax($grnPTaxM);
                        }
                    }
                }
                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                if ($productType == 2) {
                    $productBaseQty = 0;
                }
                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                $newPQtyData = array(
                    'locationProductQuantity' => $newPQty
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
            }

            $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
            $locationPrTotalQty = 0;
            $locationPrTotalPrice = 0;
            $newItemInIds = array();
            if(count($itemInData) > 0){
                foreach ($itemInData as $key => $value) {
                    if($value->itemInID > $locationProductLastItemInID){
                        $newItemInIds[] = $value->itemInID;
                        $remainQty = $value->itemInQty - $value->itemInSoldQty;
                        if($remainQty > 0){
                            $itemInPrice = $remainQty*$value->itemInPrice;
                            $itemInDiscount = $remainQty*$value->itemInDiscount;
                            $locationPrTotalQty += $remainQty;
                            $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                        }
                        $locationProductLastItemInID = $value->itemInID;
                    }
                }
            }
            $locationPrTotalQty += $locationProductQuantity;
            $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

            if($locationPrTotalQty > 0){
                $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                $lpdata = array(
                    'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                    'locationProductLastItemInID' => $locationProductLastItemInID,
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                foreach ($newItemInIds as $inID) {
                    $intemInData = array(
                        'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                    );
                    $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                }
            }
        }

        //add document reference for document reference table
        if (count($documentReference) > 0) {
            foreach ($documentReference as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $data = array(
                        'sourceDocumentTypeId' => 10,
                        'sourceDocumentId' => $grnID,
                        'referenceDocumentTypeId' => $key,
                        'referenceDocumentId' => $value,
                    );
                    $documentReference = new DocumentReference();
                    $documentReference->exchangeArray($data);
                    $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                }
            }
        }

        //update PO status if copied from it
        if ($copiedFromPo == TRUE) {
            $this->checkAndUpdatePoStatus($post['startByPoID']);
        }

        // call product updated event
        $productIDs = array_map(function($element) {
            return $element['pID'];
        }, json_decode($post['pr'], true));

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
        // var_dump($eventParameter).die();
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        if ($post['isUseAccounting'] == 1) {
            //check whether supplier GRN Clearing account is set or not
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($post['sID']);
            if(empty($supplierData->supplierGrnClearingAccountID)){
                
                return false;
            }
            $GRNClearingAccountID = $supplierData->supplierGrnClearingAccountID;

            $accountProduct = array();
            foreach ($proList as $product) {
                //check whether Product Inventory Gl account id set or not
                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($product['pID']);
                if(empty($pData['productInventoryAccountID'])){
                    
                    return false;
                }
                //calculate product total without tax potion.
                //use to calculate product discount via value or percentage
                if($product['pDType'] == 'value'){
                    $productTotal = ($product['pUnitPrice'] - $product['pDiscount'])*$product['pQuantity'];
                } else {
                    $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                }

                //add grn deliveryCharge to product according to price ratio.
                if($post['dC'] >0){
                    $productTotal += ($post['dC']*$productTotal)/ $allProductTotal;
                }
                //set gl accounts for the journal entry
                if(isset($accountProduct[$pData['productInventoryAccountID']]['total'])){
                    $accountProduct[$pData['productInventoryAccountID']]['total'] += $productTotal;
                }else{
                    $accountProduct[$pData['productInventoryAccountID']]['total'] = $productTotal;
                    $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
                }
            }

            //create data array for the journal entry save.
            $i=0;
            $journalEntryAccounts = array();
            $totalValueForSupplierAccount = 0;

            foreach ($accountProduct as $key => $value) {
                $totalValueForSupplierAccount+=$value['total'];
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['total'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode;
                $i++;
            }

            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            $journalEntryAccounts[$i]['financeAccountsID'] = $GRNClearingAccountID;
            $journalEntryAccounts[$i]['financeGroupsID'] = '';
            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $totalValueForSupplierAccount;
            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By GRN '.$grnCode.".";

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $grnData['grnDate'],
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create GRN '.$grnCode.".",
                'documentTypeID' => 10,
                'journalEntryDocumentID' => $grnID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

            $resultData = $this->saveJournalEntry($journalEntryData, null, null, false, null, $locationID);

            if($resultData['status']){
                $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($grnID,10, 3);
                if(!$jEDocStatusUpdate['status']){
                    return false;
                }

                if (!empty($dimensionData)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$grnCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                    if(!$saveRes['status']){
                        
                        error_log($saveRes['msg']);
                        return false;
                    }   
                }                   
                return true;       
            }else{
                error_log($resultData['msg']);
                return false;
            }
        }else{
            return true;
        }
        return true;
    }
}

?>
