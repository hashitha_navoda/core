<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains Inventory Product related controller functions
 */

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Inventory\Model\GoodsIssue;
use Inventory\Model\GoodsIssueProduct;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Inventory\Model\ProductBatch;
use Inventory\Model\ProductSerial;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemOut;
use Zend\Session\Container;

class InventoryAdjustmentController extends CoreController
{

    protected $paginator;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param  $location_code
     * @param  $locationID
     * @return \Zend\View\Model\JsonModel
     */
    public function getProductCodeByLocationCodeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $productLists = $this->CommonTable('Inventory\Model\ProductTable')->getProductList($locationID);

            foreach ($productLists as $p) {
                if ($p['productHandelingManufactureProduct'] == 1 || $p['productHandelingPurchaseProduct'] == 1 || $p['productHandelingConsumables'] == 1 || $p['productHandelingFixedAssets'] == 1) {
                    $productCodeList[$p['productID']] = $p['productCode'];
                    $productList[$p['productID']] = $p['productName'];
                }
            }

            $uomLists = $this->CommonTable('Inventory\Model\ProductTable')->getActiveProductListByLocation($locationID);
            $uomList = array();

            foreach ($uomLists as $u) {
                $temp = array();
                $uom = (isset($uomList[$u['productCode']]['uom'])) ? $uomList[$u['productCode']]['uom'] : array();
                if ($u['uomID'] != NULL) {
                    $uom[$u['uomID']] = array('uA' => $u['uomAbbr'], 'uN' => $u['uomName'], 'uDP' => $u['uomDecimalPlace'], 'uS' => $u['uomState'], 'uC' => $u['productUomConversion'], 'pUI' => $u['productUomID'], 'uomID' => $u['uomID'],);
                }
                $temp['uom'] = $uom;
                $uomList[$u['productCode']] = $temp;
            }

            return new JsonModel(array('pcl' => $productCodeList, 'pl' => $productList, 'uoml' => $uomList));
        } else {
            $this->msg = $this->getMessage('ERR_INVADJ_NO_POST');
            $this->data = '';
            $this->status = false;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return \Zend\View\Model\JsonModel
     * @param $locationID Location id
     * @param $serialProID Serial Product id
     * @param $batchProID Batch Product id
     * @param $sBProID Product id(Serial and Batch)
     */
    public function getSerialOrBatchDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->getPost()) {
            $productID = $request->getPost('productID');
            $locationID = $request->getPost('locationID');

            $serialDataByProTbl = $this->CommonTable('Inventory\Model\ProductTable')->getProductSerialByProducttID($productID);
            $serialProID = $serialDataByProTbl->productID;

            $serialList = Array();
            if ($serialProID != null && $serialDataByProTbl->productTypeID != 2) {
                $proLocTblData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByProIdLocId($serialProID, $locationID);
                $locProID = $proLocTblData->locationProductID;
                $proSerialTblData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductsByLocProductId($locProID);
                if ($proSerialTblData != null) {
                    foreach ($proSerialTblData as $s) {
                        $serialList[] = $s;
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_INVADJ_NOSERIAL_PRODUCTS');
                    $this->data = '';
                    $this->status = false;
                    return $this->JSONRespond();
                }
            }

            $batchList = Array();
            $batchByProTbl = $this->CommonTable('Inventory\Model\ProductTable')->getProductBatchByProductID($productID);
            $batchProID = $batchByProTbl->productID;

            if ($batchProID != null && $batchByProTbl->productTypeID != 2) {
                $batchDataByProLocTbl = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByProIdLocId($batchProID, $locationID);
                $batchLocProID = $batchDataByProLocTbl->locationProductID;

                $batchDataByProBatchTbl = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProductsByLocProductId($batchLocProID);

                if ($batchDataByProBatchTbl != null) {
                    foreach ($batchDataByProBatchTbl as $b) {
                        $batchList[] = $b;
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_INVADJ_NO_QUALITY');
                    $this->data = '';
                    $this->status = false;
                    return $this->JSONRespond();
                }
            }

            $seriaAndBatchlSerialList = array();
            $sBDataByProTbl = $this->CommonTable('Inventory\Model\ProductTable')->getProductBatchAndSerialByProductID($productID);
            $sBProID = $sBDataByProTbl->productID;
            $locProTblData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($sBProID, $locationID);

            if ($sBProID != null && $sBDataByProTbl->productTypeID != 2) {
                $serialBatchData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialAndBatchProductsByProductId($locProTblData->locationProductID);

                $seriaAndBatchlSerialList = [];
                if ($serialBatchData != NULL) {
                    foreach ($serialBatchData as $sB) {
                        $seriaAndBatchlSerialList[] = $sB;
                    }
                }
            }

            $proOnlyProduct = array();
            $productData = $this->CommonTable('Inventory\Model\ProductTable')->getProductOnlyProductByProductID($productID);
            if ($productData->productID != null) {
                $flag = true;
                if ($productData->productTypeID != 2 && ($productData->serialProduct == 1 || $productData->batchProduct == 1)) {
                    $flag = false;
                }
                if ($flag) {
                    foreach ($productData as $p) {
                        $proOnlyProduct[] = $p;
                    }
                }
            }

            return new JsonModel(array(
                'serialProductData' => $serialList,
                'batchProductData' => $batchList,
                'seriaAndBatchlSerialListData' => $seriaAndBatchlSerialList,
                'proOnlyProductData' => $proOnlyProduct));
        } else {
            $this->setLogMessage("Did not get any POST data");
            return new JsonModel(array('result' => false));
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * update functions of Negative adjustment
     */
    public function updateNegativeAdjustmentAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $items = $request->getPost('items');
            $postGoodsIssueData = $request->getPost('goods_issue_tbl_objt');
            $locationID = $request->getPost('locationID');
            $dimensionData = $request->getPost('dimensionData');
            $ignoreBudgetLimit = $postGoodsIssueData['ignoreBudgetLimit'];
            $this->updateNegativeAdjusment($items, $postGoodsIssueData, $locationID, $dimensionData, $ignoreBudgetLimit);
            return $this->JSONRespond();
        }
    }

    //update negative adjusment
    public function updateNegativeAdjusment($items, $postGoodsIssueData, $locationID, $dimensionData, $ignoreBudgetLimit)
    {
        $serialData = null;
        $batchTextData = null;
        $batchSerialData = null;

        $this->beginTransaction();
        if ($postGoodsIssueData != null) {
            $locRefID = $postGoodsIssueData['locRefID'];
            $adjusmentCode = $postGoodsIssueData['goods_issue_id'];

            // check if a adjusment from the same adjusment Code exists if exist add next number to adjusment code
            if ($locRefID != 'No') {

                while ($adjusmentCode) {
                    if ($this->CommonTable('Inventory\Model\GoodsIssueTable')->checkAdjusmentByCode($adjusmentCode)->current()) {
                        if ($locRefID) {
                            $newAdjusmentCode = $this->getReferenceNumber($locRefID);
                            if ($newAdjusmentCode == $adjusmentCode) {
                                $this->updateReferenceNumber($locRefID);
                                $adjusmentCode = $this->getReferenceNumber($locRefID);
                            } else {
                                $adjusmentCode = $newAdjusmentCode;
                            }
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_INVADJ_CODE_EXIST');
                            return $this->JSONRespond();;
                        }
                    } else {
                        break;
                    }
                }
            } else {
                $result = $this->getLocationReferenceDetails('28', null);
                $locRefID = $result->locationReferenceID;
                $adjusmentCode = $this->getReferenceNumber($locRefID);
            }

            if ($locRefID) {
                $this->updateReferenceNumber($locRefID);
            }

            $goodIssueData['locationID'] = $locationID;
            $goodIssueData['goodsIssueCode'] = $adjusmentCode;
            $date = $this->convertDateToStandardFormat($postGoodsIssueData['goods_issue_date']);
            $goodIssueData['goodsIssueDate'] = $date;
            $goodIssueData['goodsIssueReason'] = $postGoodsIssueData['goods_issue_reason'];
            $goodIssueData['goodsIssueTypeID'] = $postGoodsIssueData['goods_issue_type_id'];
            $goodIssueData['goodsIssueComment'] = $postGoodsIssueData['goods_issue_comment'];

            //save data in to goodsIssue table
            $goodsIssueModel = new GoodsIssue();
            $goodsIssueModel->exchangeArray($goodIssueData);
            $entityID = $this->createEntity();
            $goodsIssueModel->entityID = $entityID;

            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            $accountProduct = array();

            $insertedID = $this->CommonTable('Inventory\Model\GoodsIssueTable')->saveGoodIssue($goodsIssueModel);
            if ($insertedID != null) {
                foreach ($items as $ke => $p) {
                    $serialData = json_decode($p['serial_data']);
                    $batchTextData = json_decode($p['batch_text_data']);
                    $bSBatchData = json_decode($p['batch_serial_batch_data']);
                    $bSSerialData = json_decode($p['batch_serial_serial_data']);
                    $locationProductID = $p['locationProductID'];
                    $locationProData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);
                    $averageCostingPrice = $locationProData->locationProductAverageCostingPrice;
//                        $goodsIssueUomID = $p['uomID'];
//                        $uomConversionRate = $p['uomConversionRate'];
                    $unitOfPrice = $p['unit_price'];
                    $productType = $p['productType'];
                    $baseUomID = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductBaseUomID($locationProData->productID);
//                        $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($p['p_id'], $goodsIssueUomID);

                    $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($locationProData->productID);

                    if($this->useAccounting == 1){
                        if(empty($pData['productInventoryAccountID'])){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                            return $this->JSONRespond();
                        }
                        if(empty($pData['productAdjusmentAccountID'])){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PRPODUCT_ADJUSMENT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                            return $this->JSONRespond();
                        }
                    }

                    if ($serialData != null && $batchTextData == null && $bSBatchData == null) {
                        foreach ($serialData as $key => $value) {
                            $sCode = $value->s_code;
                            $sData = array(
                                'productSerialSold' => 1
                            );
                            $proSerialTblDataa = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductDetailsBySerialCodeAndLocId($sCode, $locationProductID);

                            $proSerialTblData = (object) $proSerialTblDataa;
                            if(!$proSerialTblData){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('REQ_TRAN_REFRESH');
                                return $this->JSONRespond();
                            }
                            $proSerialID = $proSerialTblData->productSerialID;

                            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateSerial($proSerialID, $sCode, $sData);
                            //update item out table for serial products only
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProductID, $proSerialID);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Inventory Negative Adjustment',
                                'itemOutDocumentID' => $insertedID,
                                'itemOutLocationProductID' => $locationProductID,
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => $proSerialID,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => 1,
                                'itemOutPrice' => $unitOfPrice,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime(),
                            );

                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);

                            $serialSaveData['goodsIssueID'] = $insertedID;
                            $serialSaveData['productBatchID'] = NULL;
                            $serialSaveData['productSerialID'] = $proSerialID;
                            $serialSaveData['locationProductID'] = $locationProductID;
                            $serialSaveData['goodsIssueProductQuantity'] = 1;
                            $serialSaveData['uomID'] = $baseUomID;
                            $serialSaveData['unitOfPrice'] = $unitOfPrice;
//                                $serialSaveData['goodsIssueUomID'] = $goodsIssueUomID;
//                                $serialSaveData['uomConversionRate'] = $uomConversionRate;
                            $this->saveGoodIssueProduct($serialSaveData);

                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) - floatval(1);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                            );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationProductID);
                            
                            $itemInUnitPrice = $itemInDetails['itemInPrice'];
                            if($this->useAccounting == 1){
                                if($averageCostingFlag == 1){
                                    $productTotal = 1 * $averageCostingPrice;
                                }else{
                                    $productTotal = 1 * $itemInUnitPrice;
                                }

                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'] = $productTotal;
                                    $accountProduct[$pData['productAdjusmentAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productAdjusmentAccountID']]['accountID'] = $pData['productAdjusmentAccountID'];
                                }
                            }
                        }
                    } else if ($serialData == null && $batchTextData != null && $bSBatchData == null) {
                        foreach ($batchTextData as $key => $b) {
                            $reduceQty = $b->value;
                            $id = $b->id;
                            $bID = split("_", $id);
                            $proBatchTblData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($bID[1]);
                            $dbBatchQty = $proBatchTblData->productBatchQuantity;
                            // check product is avaialable for this adjustment
                            if($dbBatchQty < $reduceQty){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('REQ_TRAN_REFRESH');
                                return $this->JSONRespond();
                            }

                            $newBatchQty = $this->getSubQty($dbBatchQty, $reduceQty);
                            $serialSaveData = array(
                                'productBatchQuantity' => $newBatchQty,
                                'productBatchID' => $bID[1]
                            );
                            $this->CommonTable('Inventory\Model\ProductBatchTable')->updateBatchByID($bID[1], $serialSaveData);
                            //update item out table for batch products
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProductID, $bID[1]);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Inventory Negative Adjustment',
                                'itemOutDocumentID' => $insertedID,
                                'itemOutLocationProductID' => $locationProductID,
                                'itemOutBatchID' => $bID[1],
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $reduceQty,
                                'itemOutPrice' => $unitOfPrice,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + $reduceQty;
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                            $batchSaveData['productBatchID'] = $proBatchTblData->productBatchID;
                            $batchSaveData['goodsIssueID'] = $insertedID;
                            $batchSaveData['locationProductID'] = $locationProductID;
                            $batchSaveData['goodsIssueProductQuantity'] = $reduceQty;
                            $batchSaveData['unitOfPrice'] = $unitOfPrice;
                            $batchSaveData['uomID'] = $baseUomID;
//                                $batchSaveData['goodsIssueUomID'] = $goodsIssueUomID;
//                                $batchSaveData['uomConversionRate'] = $uomConversionRate;
                            $this->saveGoodIssueProduct($batchSaveData);

                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) - floatval($reduceQty);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                            );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationProductID);
                            $itemInUnitPrice = $itemInDetails['itemInPrice'];
                            if($this->useAccounting == 1){
                                if($averageCostingFlag == 1){
                                    $productTotal = $reduceQty * $averageCostingPrice;
                                }else{
                                    $productTotal = $reduceQty * $itemInUnitPrice;
                                }
                                
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'] = $productTotal;
                                    $accountProduct[$pData['productAdjusmentAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productAdjusmentAccountID']]['accountID'] = $pData['productAdjusmentAccountID'];
                                }
                            }
                        }
                    } else if (($serialData == null && $batchTextData == null) && $bSBatchData != null || $bSSerialData != null) {
                        // batch and serial product
                        foreach ($bSSerialData as $key => $value) {
                            $sCode = $value->s_code;
                            $proSerialTblData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getAvailableSerialProductsByLocIdSerialCode($sCode, $locationProductID);

                            // check product is available to do this adjustment
                            if (!$proSerialTblData) {
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('REQ_TRAN_REFRESH');
                                return $this->JSONRespond();
                            }

                            $proSerialID = $proSerialTblData->productSerialID;
                            $batchID = $proSerialTblData->productBatchID;
                            //update serial quantity
                            $serialSaveData = array(
                                'productSerialSold' => 1
                            );
                            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateBatchSerial($sCode, $batchID, $serialSaveData);
                            //add item details to item out table
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProductID, $batchID, $proSerialID);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Inventory Negative Adjustment',
                                'itemOutDocumentID' => $insertedID,
                                'itemOutLocationProductID' => $locationProductID,
                                'itemOutBatchID' => $batchID,
                                'itemOutSerialID' => $proSerialID,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => 1,
                                'itemOutPrice' => $unitOfPrice,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + 1;
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                            //update batch quantity
                            $proBatchTblData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchID);
                            $dbBatchQty = $proBatchTblData->productBatchQuantity;
                            $newBatchQty = $this->getSubQty($dbBatchQty, 1);
                            $batchSaveData = array(
                                'productBatchQuantity' => $newBatchQty,
                                'productBatchID' => $batchID
                            );
                            $this->CommonTable('Inventory\Model\ProductBatchTable')->updateBatchByID($batchID, $batchSaveData);

                            $batchSerialData['productBatchID'] = $batchID;
                            $batchSerialData['productSerialID'] = $proSerialID;
                            $batchSerialData['goodsIssueID'] = $insertedID;
                            $batchSerialData['locationProductID'] = $locationProductID;
                            $batchSerialData['goodsIssueProductQuantity'] = 1;
                            $batchSerialData['unitOfPrice'] = $unitOfPrice;
                            $batchSerialData['uomID'] = $baseUomID;
//                                $batchSerialData['goodsIssueUomID'] = $goodsIssueUomID;
//                                $batchSerialData['uomConversionRate'] = $uomConversionRate;
                            $this->saveGoodIssueProduct($batchSerialData);

                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) - floatval(1);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                            );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationProductID);
                            
                            $itemInUnitPrice = $itemInDetails['itemInPrice'];
                            if($this->useAccounting == 1){
                                if($averageCostingFlag == 1){
                                    $productTotal = 1 * $averageCostingPrice;
                                }else{
                                    $productTotal = 1 * $itemInUnitPrice;
                                }
                                
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'] = $productTotal;
                                    $accountProduct[$pData['productAdjusmentAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productAdjusmentAccountID']]['accountID'] = $pData['productAdjusmentAccountID'];
                                }
                            }
                        }
                        foreach ($bSBatchData as $key => $value) {
                            $reduceQty = $value->value;
                            $batchID = $value->id;
                            $proBatchTblData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchID);
                            $dbBatchQty = $proBatchTblData->productBatchQuantity;

                            $newBatchQty = $this->getSubQty($dbBatchQty, $reduceQty);

                            $serialSaveData = array(
                                'productBatchQuantity' => $newBatchQty,
                                'productBatchID' => $batchID
                            );
                            $this->CommonTable('Inventory\Model\ProductBatchTable')->updateBatchByID($batchID, $serialSaveData);
                            $batchSaveData['productBatchID'] = $proBatchTblData->productBatchID;
                            $batchSaveData['goodsIssueID'] = $insertedID;
                            $batchSaveData['locationProductID'] = $locationProductID;
                            $batchSaveData['goodsIssueProductQuantity'] = $reduceQty;
                            $batchSaveData['unitOfPrice'] = $unitOfPrice;
                            $batchSaveData['uomID'] = $baseUomID;
//                                $batchSaveData['goodsIssueUomID'] = $goodsIssueUomID;
//                                $batchSaveData['uomConversionRate'] = $uomConversionRate;
                            $this->saveGoodIssueProduct($batchSaveData);

                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) - floatval($newBatchQty);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                            );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationProductID);
                        }
                    } else {
                        // non batch or serial product
                        $proID = $p['p_id'];
                        $proLocTblDataByProIDLocID = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByProIdLocId($proID, $locationID);
                        $dbBatchQty = $proLocTblDataByProIDLocID->locationProductQuantity;
                        $newBatchQty = $this->getSubQty($dbBatchQty, $p['quantity'], $productType);

                        $serialSaveData = array(
                            'locationProductQuantity' => $newBatchQty
                        );
                        // check product is available to do this adjustment
                        if($proLocTblDataByProIDLocID->locationProductQuantity < $p['quantity']){
                            $this->rollback();
                            $this->msg = $this->getMessage('REQ_TRAN_REFRESH');
                            $this->status = FALSE;
                            return $this->JSONRespond();
                        }

                        //insert details to itemout table
                        $sellingQty = $p['quantity'];
                        $itemInDetails = [];
                        while ($sellingQty != 0) {
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($locationProductID);
                            if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Inventory Negative Adjustment',
                                    'itemOutDocumentID' => $insertedID,
                                    'itemOutLocationProductID' => $locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $sellingQty,
                                    'itemOutPrice' => $unitOfPrice,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                $sellingQty = 0;
                                break;
                            } else {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Inventory Negative Adjustment',
                                    'itemOutDocumentID' => $insertedID,
                                    'itemOutLocationProductID' => $locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                    'itemOutPrice' => $unitOfPrice,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                            }
                        }
                        $sellingQty = $p['quantity'];
                        $itemInUnitPrice = $itemInDetails['itemInPrice'];
                        if($this->useAccounting == 1){
                            if($averageCostingFlag == 1){
                                $productTotal = $sellingQty * $averageCostingPrice;
                            }else{
                                $productTotal = $sellingQty * $itemInUnitPrice;
                            }
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                            }

                            if(isset($accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'] = $productTotal;
                                $accountProduct[$pData['productAdjusmentAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productAdjusmentAccountID']]['accountID'] = $pData['productAdjusmentAccountID'];
                            }
                        }

                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($serialSaveData, $proID, $locationID);
                        $saveProTbldata['goodsIssueID'] = $insertedID;
                        $saveProTbldata['locationProductID'] = $proLocTblDataByProIDLocID->locationProductID;
                        $saveProTbldata['goodsIssueProductQuantity'] = $p['quantity'];
                        $saveProTbldata['unitOfPrice'] = $unitOfPrice;
                        $saveProTbldata['uomID'] = $baseUomID;
//                            $saveProTbldata['goodsIssueUomID'] = $goodsIssueUomID;
//                            $saveProTbldata['uomConversionRate'] = $uomConversionRate;
                        $this->saveGoodIssueProduct($saveProTbldata);
                    }
                }
                if ($this->useAccounting == 1) {
                            //create data array for the journal entry save.
                    $i=0;
                    $journalEntryAccounts = array();

                    foreach ($accountProduct as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Inventroy Negative Adjustment '.$adjusmentCode.'.';
                        $i++;
                    }

                            //get journal entry reference number.
                    $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                    $jelocationReferenceID = $jeresult['locRefID'];
                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                    $journalEntryData = array(
                        'journalEntryAccounts' => $journalEntryAccounts,
                        'journalEntryDate' => $date,
                        'journalEntryCode' => $JournalEntryCode,
                        'journalEntryTypeID' => '',
                        'journalEntryIsReverse' => 0,
                        'journalEntryComment' => 'Journal Entry is posted when create Negative Adjustment '.$adjusmentCode.'.',
                        'documentTypeID' => 16,
                        'journalEntryDocumentID' => $insertedID,
                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                        );

                    $resultData = $this->saveJournalEntry($journalEntryData);
                    if(!$resultData['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $resultData['msg'];
                        $this->data = $resultData['data'];
                        return $this->JSONRespond();;
                    }

                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($insertedID,16, 3);
                    if(!$jEDocStatusUpdate['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg =$jEDocStatusUpdate['msg'];
                        return $this->JSONRespond();
                    }

                    if (!empty($dimensionData)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$adjusmentCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->msg =$saveRes['msg'];
                            $this->data =$saveRes['data'];
                            return $this->JSONRespond();
                        }   
                    }
                }
            } else {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_INVADJ_SAVING_ISSUE');
                $this->status = false;
                return $this->JSONRespond();
            }
        } else {
            $this->rollback();
            $this->msg = $this->getMessage('ERR_INVADJ_NO_POST');
            $this->status = false;
            return $this->JSONRespond();
        }
        $this->commit();
        // call product updated event
        $productIDs = array_map(function($element) {
            return $element['p_id'];
        }, $items);

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        $this->msg = $this->getMessage('SUCC_INVADJ_ADJSAVED', array($adjusmentCode));
        $this->setLogMessage($adjusmentCode . ' successfully saved negative Inventory Adjustment');
        $this->data = array('adjusmentID' => $insertedID);
        $this->status = true;
//        $this->flashMessenger()->addMessage($this->msg);
        return;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return type
     */
    public function updatePositiveAdjustmentAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $this->beginTransaction();
            $result = $this->updatePositiveAdjusmentWithoutPost($post);
            if($result['status']){
                $this->commit();
            }else{
                $this->rollback();
            }
            $this->data = $result['data'];
            $this->status = $result['status'];
            $this->msg = $result['msg'];
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan Madushka<ashan@thinkcube.com>
     * @return type
     * this function for save positive ajusment without a post request
     */
    public function updatePositiveAdjusmentWithoutPost($post)
    {
        $locID = $post['locationID'];
        $dimensionData = $post['dimensionData'];
        $flag = $post['flag'];
        $goodsIssueTblData = $post['goods_issue_tbl_data'];
        if (isset($goodsIssueTblData['type'])) {
            $type = $goodsIssueTblData['type'];
        }

        //update Reference table
        $locRefID = $goodsIssueTblData['locRefID'];
        $adjusmentCode = $goodsIssueTblData['goods_issue_id'];
        if ($locRefID != 'No') {
            // check if a adjusment from the same adjusment Code exists if exist add next number to adjusment code
            while ($adjusmentCode) {
                if ($this->CommonTable('Inventory\Model\GoodsIssueTable')->checkAdjusmentByCode($adjusmentCode)->current()) {
                    if ($locRefID) {
                        $newAdjusmentCode = $this->getReferenceNumber($locRefID);
                        if ($newAdjusmentCode == $adjusmentCode) {
                            $this->updateReferenceNumber($locRefID);
                            $adjusmentCode = $this->getReferenceNumber($locRefID);
                        } else {
                            $adjusmentCode = $newAdjusmentCode;
                        }
                    } else {
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_INVADJ_CODE_EXIST'),
                        );
                    }
                } else {
                    break;
                }
            }
        } else {
            $result = $this->getLocationReferenceDetails('28', null);
            $locRefID = $result->locationReferenceID;
            $adjusmentCode = $this->getReferenceNumber($locRefID);
        }

        if ($locRefID) {
            $this->updateReferenceNumber($locRefID);
        }

        //if action is adding product activity by raw material product
        //then data set to goodsIssue table
        if ($type === 'Activity') {
            $date = new \DateTime("NOW");
            $adjusmentCode = 'ACT_' . $date->getTimestamp();
            $goodsIssueTblData['goods_issue_date'] = $this->getGMTDateTime();
        }

        //insert data in to GoodIssueTable
        $data['locationID'] = $locID;
        $data['goodsIssueCode'] = $adjusmentCode;
        $date = $this->convertDateToStandardFormat($goodsIssueTblData['goods_issue_date']);
        $data['goodsIssueDate'] = $date;
        $data['goodsIssueReason'] = $goodsIssueTblData['goods_issue_reason'];
        $data['goodsIssueTypeID'] = $goodsIssueTblData['goods_issue_type_id'];
        $data['goodsIssueComment'] = $goodsIssueTblData['goods_issue_comment'];

        $goodsIssueModel = new GoodsIssue();
        $goodsIssueModel->exchangeArray($data);
        $entityID = $this->createEntity();
        $goodsIssueModel->entityID = $entityID;

        $accountProduct = array();

        $insertedGoodsIssueID = $this->CommonTable('Inventory\Model\GoodsIssueTable')->saveGoodIssue($goodsIssueModel);
        //Update items
        foreach ($post['items'] as $product) {
//                $uomConversionRate = $product['uomConversionRate'];
            $unitPrice = isset($product['pUnitPrice']) ? $product['pUnitPrice'] : 0;
            $locationPID = $product['locationPID'];
            $uomID = $product['uomID'];
            $baseUomID = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductBaseUomID($product['pID']);

            $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
            $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
            $locationProductQuantity = $locationProductData->locationProductQuantity;
            $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

            $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($locationProductData->productID);
            
            if($this->useAccounting == 1 && $flag != 'GiftCard'){
                if(empty($pData['productInventoryAccountID'])){
                     return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                        );
                }
                if(empty($pData['productAdjusmentAccountID'])){
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_PRPODUCT_ADJUSMENT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                    );
                }
            }

            //$productBaseQty = $this->CommonTable('Inventory\Model\ProductUomTable')->convertProductQtyForBaseQty($product['pID'], $product['uomID'], $product['pQuantity']);
            //assume to 1, because of I already convert it into base value in inventory-adjustment.js
//                $productBaseQty = 1;
            $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $uomID);
            if (array_key_exists('bProducts', $product)) {
                $batchProductCount = 0;
                foreach ($product['bProducts'] as $bProduct) {
                    $batchProductCount++;
                    $bPData = array(
                        'locationProductID' => $locationPID,
                        'productBatchCode' => $bProduct['bCode'],
                        'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                        'productBatchWarrantyPeriod' => $bProduct['warnty'],
                        'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                        'productBatchQuantity' => (floatval($bProduct['bQty']) * floatval($productConversionRate))
                    );
                    $batchProduct = new ProductBatch();
                    $batchProduct->exchangeArray($bPData);
                    $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                    if (array_key_exists('sProducts', $product)) {
                        foreach ($product['sProducts'] as $sProduct) {
                            if (isset($sProduct['sBCode'])) {
                                if ($sProduct['sBCode'] == $bProduct['bCode']) {

                                    $sPData = array(
                                        'locationProductID' => $locationPID,
                                        'productBatchID' => $insertedBID,
                                        'productSerialCode' => $sProduct['sCode'],
                                        'productSerialExpireDate' => $this->convertDateToStandardFormat($sProduct['sEdate']),
                                        'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                        'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                                    );
                                    $serialPr = new ProductSerial();
                                    $serialPr->exchangeArray($sPData);
                                    $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);

                                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                    $newPQtyData = array(
                                        'locationProductQuantity' => $newPQty
                                    );
                                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                }
                            }
                            $goodIssueProData = array(
                                'productBatchID' => $insertedBID,
                                'productSerialID' => $insertedSID,
                                'goodsIssueID' => $insertedGoodsIssueID,
                                'locationProductID' => $locationPID,
                                'goodsIssueProductQuantity' => (floatval($product['pQuantity']) * floatval($productConversionRate)),
                                'unitOfPrice' => $unitPrice,
                                'uomID' => $baseUomID
                            );
                            $this->saveGoodIssueProduct($goodIssueProData);
                            //save the product details to the itemInTable

                            $itemInData = array(
                                'itemInDocumentType' => 'Inventory Positive Adjustment',
                                'itemInDocumentID' => $insertedGoodsIssueID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => $insertedBID,
                                'itemInSerialID' => $insertedSID,
                                'itemInQty' => 1,
                                'itemInPrice' => $unitPrice,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        }
                    } else {
                        //save itemIn
                        $itemInData = array(
                            'itemInDocumentType' => 'Inventory Positive Adjustment',
                            'itemInDocumentID' => $insertedGoodsIssueID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => $insertedBID,
                            'itemInSerialID' => NULL,
                            'itemInQty' => (floatval($product['pQuantity'])),
                            'itemInPrice' => $unitPrice,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                        if ($batchProductCount == 1) {
                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($product['pQuantity']);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                            );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        }
                    }
                    $goodIssueProData = array(
                        'productBatchID' => $insertedBID,
                        'goodsIssueID' => $insertedGoodsIssueID,
                        'locationProductID' => $locationPID,
                        'goodsIssueProductQuantity' => (floatval($bProduct['bQty']) * floatval($productConversionRate)),
                        'unitOfPrice' => $unitPrice,
                        'uomID' => $baseUomID
                    );
                    $this->saveGoodIssueProduct($goodIssueProData);
                }
            } elseif (array_key_exists('sProducts', $product)) {
                foreach ($product['sProducts'] as $sProduct) {
                    $sPData = array(
                        'locationProductID' => $locationPID,
                        'productSerialCode' => $sProduct['sCode'],
                        'productSerialExpireDate' => $sProduct['sEdate'],
                        'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                        'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                    );
                    $serialPr = new ProductSerial();
                    $serialPr->exchangeArray($sPData);
                    $serialID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);


                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);

                    $goodIssueProData = array(
                        'productSerialID' => $serialID,
                        'goodsIssueID' => $insertedGoodsIssueID,
                        'locationProductID' => $locationPID,
                        'goodsIssueProductQuantity' => (1 * floatval($productConversionRate)),
                        'unitOfPrice' => $unitPrice,
                        'uomID' => $baseUomID
                    );
                    $this->saveGoodIssueProduct($goodIssueProData);

                    //save serial product itemIn
                    $itemInData = array(
                        'itemInDocumentType' => 'Inventory Positive Adjustment',
                        'itemInDocumentID' => $insertedGoodsIssueID,
                        'itemInLocationProductID' => $locationPID,
                        'itemInBatchID' => NULL,
                        'itemInSerialID' => $serialID,
                        'itemInQty' => 1,
                        'itemInPrice' => $unitPrice,
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                    );
                    $itemInModel = new ItemIn();
                    $itemInModel->exchangeArray($itemInData);
                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                }
            } else {

                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                // $newPQty = floatval($currentPQty->locationProductQuantity) + (floatval($product['pQuantity']) * floatval($productConversionRate));
                $newPQty = floatval($currentPQty->locationProductQuantity) + (floatval($product['pQuantity']));
                $newPQtyData = array(
                    'locationProductQuantity' => $newPQty
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);

                $goodIssueProData = array(
                    'goodsIssueID' => $insertedGoodsIssueID,
                    'locationProductID' => $locationPID,
                    // 'goodsIssueProductQuantity' => (floatval($product['pQuantity']) * floatval($productConversionRate)),
                    'goodsIssueProductQuantity' => (floatval($product['pQuantity'])),
                    'unitOfPrice' => $unitPrice,
                    'uomID' => $baseUomID
                );
                $this->saveGoodIssueProduct($goodIssueProData);

                //save itemIn Details
                $itemInData = array(
                    'itemInDocumentType' => 'Inventory Positive Adjustment',
                    'itemInDocumentID' => $insertedGoodsIssueID,
                    'itemInLocationProductID' => $locationPID,
                    'itemInBatchID' => NULL,
                    'itemInSerialID' => NULL,
                    // 'itemInQty' => floatval($product['pQuantity']) * floatval($productConversionRate),
                    'itemInQty' => floatval($product['pQuantity']),
                    'itemInPrice' => $unitPrice,
                    'itemInDateAndTime' => $this->getGMTDateTime(),
                );
                $itemInModel = new ItemIn();
                $itemInModel->exchangeArray($itemInData);
                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
            }

            if($this->useAccounting == 1 && $flag != 'GiftCard'){
                // $productTotal = floatval($product['pQuantity']) * floatval($productConversionRate) * $unitPrice;
                $productTotal = floatval($product['pQuantity'])  * $unitPrice;

                if(isset($accountProduct[$pData['productAdjusmentAccountID']]['creditTotal'])){
                    $accountProduct[$pData['productAdjusmentAccountID']]['creditTotal'] += $productTotal;
                }else{
                    $accountProduct[$pData['productAdjusmentAccountID']]['creditTotal'] = $productTotal;
                    $accountProduct[$pData['productAdjusmentAccountID']]['debitTotal'] = 0.00;
                    $accountProduct[$pData['productAdjusmentAccountID']]['accountID'] = $pData['productAdjusmentAccountID'];
                }

                if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                }else{
                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                }
            }

            $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
            $locationPrTotalQty = 0;
            $locationPrTotalPrice = 0;
            $newItemInIds = array();
            if(count($itemInData) > 0){
                foreach ($itemInData as $key => $value) {
                    if($value->itemInID > $locationProductLastItemInID){
                        $newItemInIds[] = $value->itemInID; 
                        $remainQty = $value->itemInQty - $value->itemInSoldQty;
                        if($remainQty > 0){
                            $itemInPrice = $remainQty*$value->itemInPrice;
                            $itemInDiscount = $remainQty*$value->itemInDiscount;
                            $locationPrTotalQty += $remainQty;
                            $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                        }
                        $locationProductLastItemInID = $value->itemInID; 
                    }
                }
            }

            $locationPrTotalQty += $locationProductQuantity;
            $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;
            if($locationPrTotalQty > 0){
                $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                $lpdata = array(
                    'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                    'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);
                
                foreach ($newItemInIds as $inID) {
                    $intemInData = array(
                        'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                    );
                    $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                }

            }
        }

        if ($this->useAccounting == 1 && $flag != 'GiftCard') {
                            //create data array for the journal entry save.
            $i=0;
            $journalEntryAccounts = array();

            foreach ($accountProduct as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Inventroy Positive Adjustments '.$adjusmentCode.'.';
                $i++;
            }

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $date,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Positive Adjustment '.$adjusmentCode.'.',
                'documentTypeID' => 16,
                'journalEntryDocumentID' => $insertedGoodsIssueID,
                'ignoreBudgetLimit' => $goodsIssueTblData['ignoreBudgetLimit'],
                );

            $resultData = $this->saveJournalEntry($journalEntryData);
            if(!$resultData['status']){
                return array(
                    'status' => false,
                    'msg' => $resultData['msg'],
                    'data' => $resultData['data'],
                );
            }

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($insertedGoodsIssueID,16, 3);
            if(!$jEDocStatusUpdate['status']){
                return array(
                        'status' => false,
                        'msg' => $jEDocStatusUpdate['msg'],
                    );
            }

            if (!empty($dimensionData)) {
                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$adjusmentCode], $resultData['data']['journalEntryID'], $goodsIssueTblData['ignoreBudgetLimit'], $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                if(!$saveRes['status']){
                    return array(
                        'status' => false,
                        'msg' => $saveRes['msg'],
                        'data' => $saveRes['data'],
                    );
                    
                }   
            }   
        }

        //      call product updated event
        $productIDs = array_map(function($element) {
            return $element['pID'];
        }, $post['items']);

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locID]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        $this->setLogMessage($adjusmentCode . ' successfully saved positive Inventory Adjustment');
        
        $msg = $this->getMessage('SUCC_INVADJ_POSSITIVE_ADJEST', array($adjusmentCode));
        if ($flag != 'invoice' && $flag != 'GiftCard') {
            $this->flashMessenger()->addMessage($msg);
        }
        return array(
            'status' => true,
            'msg' => $msg,
            'data' => array('adjusmnetID' => $insertedGoodsIssueID),
        );
        return $this->JSONRespond();
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return return row in GoodsIssuetable according to goodIssueID
     */
    public function getGoodsIssueDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->getPost()) {
            $goodIssueID = $request->getPost('giID');
            $result = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getGoodsIssue($goodIssueID);

            $this->data = $result;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @return type
     */
    public function updateGoodsIssueReasonAction()
    {
        $request = $this->getRequest();
        if ($request->getPost()) {
            $goodIssueID = $request->getPost('goodIssueID');
            $reason = $request->getPost('reason');
            $saveData = array(
                'goodsIssueReason' => $reason
            );

            $result = $this->CommonTable('Inventory\Model\GoodsIssueTable')->update($saveData, $goodIssueID);
            if ($result) {
                $this->data = $result;
                $this->msg = $this->getMessage('SUCC_INVADJ_UPDATE');
                $this->status = true;

                return $this->JSONRespond();
            } else {
                $this->data = $result;
                $this->msg = $this->getMessage('ERR_INVADJ_UPDATE');
                $this->status = true;

                return $this->JSONRespond();
            }
        } else {
            $this->data = 'There have no any post data';
            $this->msg = $this->getMessage('ERR_INVADJ_UPDATE'); //'Adjustment upadated fail..'
            $this->status = FALSE;

            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $data
     * @return type
     */
    public function saveGoodIssueProduct($data)
    {
        $goodsIssueProductModel = new GoodsIssueProduct();
        $goodsIssueProductModel->exchangeArray($data);
        $result = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->saveGoodIssueProduct($goodsIssueProductModel);
        return $result;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return type
     */
    public function updateGoodsIssueByIdAction()
    {
        $request = $this->getRequest();
        if ($request->getPost()) {
            $goodsIssueID = $request->getPost('goodIssueID');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');

            $goodIssueStatusData = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getGoodIssueByID($goodsIssueID)->current();
            // Check goodsIssueStatus is canceled 
            if($goodIssueStatusData['goodsIssueStatus'] == '5'){
                $this->getPaginatedGoodsIssue();
                $adjustmentListView = new ViewModel(array('adjustment' => $this->paginator, 'statuses' => $this->getStatusesList()));
                $adjustmentListView->setTerminal(true);
                $adjustmentListView->setTemplate('inventory/inventory-adjustment/adjustment-list');

                $this->msg = $this->getMessage('ERR_ALREADY_DELETED');
                $this->data = 'AlReadyDeleted';
                $this->status = false;
                $this->html = $adjustmentListView;
                return $this->JSONRespondHtml();
            }

            $goodIssueData = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getGoodsIssue($goodsIssueID);
            $goodIssueType = $goodIssueData->goodsIssueTypeID;
            $goodsIssueCode = $goodIssueData->goodsIssueCode;
            $locationID = $goodIssueData->locationID;

            $goodIssueProductData = [];
            $this->beginTransaction();
            //negative adjustment (1) and positive adjustment (2)
            if ($goodIssueType == 1) {
                //get all data(batch,serial) which related to delete goodIssueID
                //get products details by related invoice ID
                $getGoodIssueProductData = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->getGoodIssueProducts($goodsIssueID, FALSE, NULL, 'negative');
                foreach ($getGoodIssueProductData as $key => $g) {
                    $goodIssueProductData['products'][$g['productID']] = array(
                        'productID' => $g['productID'],
                        'goodsIssueID' => $goodsIssueID,
                        'goodsIssueProductQuantity' => $g['totalQty'],
                        'locationProductID' => $g['locationProductID']
                    );
                    if ($g['productTypeID'] === '1') {
                        $inventoryProductIDs[] = $g['productID'];
                    }

                    $adjustmentSubProducts = [];
                    $productData = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->getGoodIssueProducts($goodsIssueID, TRUE, $g['locationProductID'], NULL);

                    foreach ($productData as $key => $s) {
                        if ($s['productBatchID'] != NULL || $s['productSerialID'] != NULL) {

                            $adjustmentSubProducts[] = array(
                                'batchID' => $s['productBatchID'],
                                'serialID' => $s['productSerialID'],
                                'qtyByBase' => $s['goodsIssueProductQuantity'],
                                'productPrice' => $s['unitOfPrice'],
                                'locationProductID' => $s['locationProductID'],
                            );
                        }
                    }
                    $goodIssueProductData['subProducts'][$g['productID']] = $adjustmentSubProducts;
                }

                foreach ($goodIssueProductData['products'] as $key => $p) {
                    $batchProducts = $goodIssueProductData['subProducts'][$p['productID']];
                    $loreturnQty = $returnQty = $p['goodsIssueProductQuantity'];
                    $locationProductID = $p['locationProductID'];

                    $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);

                    $locationProductData = array(
                        'locationProductQuantity' => $locationProduct->locationProductQuantity + $loreturnQty,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateQuantityByID($locationProductData, $locationProductID);

                    if (!count($batchProducts) > 0) {
                        $itemReturnQty = $returnQty;
                        //add item in details for non serial and batch products
                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($locationProduct->locationProductID, 'Inventory Negative Adjustment', $goodsIssueID);

                        foreach (array_reverse($itemOutDetails) as $itemOutData) {
                            if ($itemReturnQty != 0) {
                                if ($itemOutData->itemOutQty > $itemOutData->itemOutReturnQty) {
                                    $itemInPreDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetails($itemOutData->itemOutItemInID);
                                    $leftQty = floatval($itemOutData->itemOutQty) - floatval($itemOutData->itemOutReturnQty);
                                    $itemInInsertQty = 0;
                                    $itemOutUpdateReturnQty = 0;
                                    if ($leftQty >= $itemReturnQty) {
                                        $itemInInsertQty = $itemReturnQty;
                                        $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) + floatval($itemReturnQty);
                                        $itemReturnQty = 0;
                                    } else {
                                        $itemInInsertQty = $leftQty;
                                        $itemOutUpdateReturnQty = $itemOutData->itemOutQty;
                                        $itemReturnQty = floatval($itemReturnQty) - floatval($leftQty);
                                    }
                                    //hit data to the item in table with left qty
                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInPreDetails['itemInIndexID']);
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Delete Negative Adjustment',
                                        'itemInDocumentID' => $goodsIssueID,
                                        'itemInLocationProductID' => $locationProduct->locationProductID,
                                        'itemInBatchID' => NULL,
                                        'itemInSerialID' => NULL,
                                        'itemInQty' => $itemInInsertQty,
                                        'itemInPrice' => $itemInPreDetails['itemInPrice'],
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                    //update item out return qty
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                                }
                            } else {
                                break;
                            }
                        }
                    }

                    if (count($batchProducts) > 0) {
                        foreach ($batchProducts as $batchKey => $batchValue) {
                            $locationProductID = $batchValue['locationProductID'];
                            if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {

                                $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                                $productBatchQuentity = $batchProduct->productBatchQuantity + $batchValue['qtyByBase'];
                                $productBatchQty = array(
                                    'productBatchQuantity' => $productBatchQuentity,
                                );
                                $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                                if ($batchValue['serialID']) {
                                    //Add details to item in table for batch and serial products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchSerialProductDetails($locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Delete Negative Adjustment',
                                        'itemInDocumentID' => $goodsIssueID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => $batchValue['batchID'],
                                        'itemInSerialID' => $batchValue['serialID'],
                                        'itemInQty' => 1,
                                        'itemInPrice' => $batchValue['productPrice'],
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                } else {
                                    //Add details to item in table for batch products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchProductDetails($locationProductID, $batchValue['batchID']);
                                    if ($itemInDetails['itemInID'] != NULL) {
                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                        $itemInData = array(
                                            'itemInIndex' => $newItemInIndex,
                                            'itemInDocumentType' => 'Delete Negative Adjustment',
                                            'itemInDocumentID' => $goodsIssueID,
                                            'itemInLocationProductID' => $locationProductID,
                                            'itemInBatchID' => $batchValue['batchID'],
                                            'itemInSerialID' => NULL,
                                            'itemInQty' => $batchValue['qtyByBase'],
                                            'itemInPrice' => $batchValue['productPrice'],
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                    }
                                }
                            } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                //Add details to item in table for serial products
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getSerialProductDetails($locationProductID, $batchValue['serialID']);
                                $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                $itemInData = array(
                                    'itemInIndex' => $newItemInIndex,
                                    'itemInDocumentType' => 'Delete Negative Adjustment',
                                    'itemInDocumentID' => $goodsIssueID,
                                    'itemInLocationProductID' => $locationProductID,
                                    'itemInBatchID' => NULL,
                                    'itemInSerialID' => $batchValue['serialID'],
                                    'itemInQty' => 1,
                                    'itemInPrice' => $batchValue['productPrice'],
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            }
                            if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                $serialProductData = array(
                                    'productSerialSold' => '0',
                                );
                                $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                            }
                        }
                    }
                }
            } else if ($goodIssueType == 2) {
                //get all data(batch,serial) which related to delete goodIssueID
                //get products details by related invoice ID
                $getGoodIssueProductData = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->getGoodIssueProducts($goodsIssueID, FALSE, NULL, 'positive');
                foreach ($getGoodIssueProductData as $key => $g) {
                    $goodIssueProductData[$g['productCode']] = array(
                        'pID' => $g['productID'],
                        'pCode' => $g['productCode'],
                        'pName' => $g['productName'],
                        'goodsIssueID' => $goodsIssueID,
                        'pQuantity' => $g['goodsIssueProductQuantity'],
                        'pReturnQuantity' => $g['goodsIssueProductQuantity'],
                        'pReturnTotalQuantity' => $g['totalQty'],
                        'pUnitPrice' => $g['unitOfPrice'],
                        'locationPID' => $g['locationProductID']
                    );

                    $productData = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->getGoodIssueProducts($goodsIssueID, TRUE, $g['locationProductID'], NULL);
                    if ($productData != NULL) {
                        foreach ($productData as $key => $s) {
                            $serialProducts = [];
                            $batchProducts = [];
                            if ($s['productSerialID'] != NULL && $s['productBatchID'] == NULL) {
                                $getSerialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProduct($s['productSerialID'], NULL);
                                foreach ($getSerialData as $v) {
                                    $goodIssueProductData[$g['productCode']]['sProducts'][$v['productSerialCode']] = [
                                        'sID' => $s['productSerialID'],
                                        'sCode' => $v['productSerialCode'],
                                        'sBCode' => NULL,
                                        'sRQty' => 1
                                    ];
                                }
                            } else if ($s['productSerialID'] != NULL && $s['productBatchID'] != NULL) {
                                $getBatchData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($s['productBatchID']);
                                $goodIssueProductData[$g['productCode']]['bProducts'][$getBatchData->productBatchCode] = [
                                    'bID' => $s['productBatchID'],
                                    'bCode' => $getBatchData->productBatchCode,
                                    'bQty' => $getBatchData->productBatchQuantity,
                                    'bRQty' => $s['goodsIssueProductQuantity']
                                ];

                                $getSerialBatchData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProduct($s['productSerialID'], $s['productBatchID']);
                                foreach ($getSerialBatchData as $v) {
                                    $goodIssueProductData[$g['productCode']]['sProducts'][$v['productSerialCode']] = [
                                        'sID' => $s['productSerialID'],
                                        'sCode' => $v['productSerialCode'],
                                        'sBCode' => $getBatchData->productBatchCode,
                                        'sRQty' => 1
                                    ];
                                }
                            } else if ($s['productSerialID'] == NULL && $s['productBatchID'] != NULL) {
                                $getBatchData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($s['productBatchID']);
                                $goodIssueProductData[$g['productCode']]['bProducts'][$getBatchData->productBatchCode] = [
                                    'bID' => $s['productBatchID'],
                                    'bCode' => $getBatchData->productBatchCode,
                                    'bQty' => $getBatchData->productBatchQuantity,
                                    'bRQty' => $s['goodsIssueProductQuantity']
                                ];
                            }
                        }
                    }
                }

                foreach ($goodIssueProductData as $returnProduct) {
                    if (array_key_exists('bProducts', $returnProduct) && array_key_exists('sProducts', $returnProduct)) {
                        //Create Adjustment Product for products that have both batch and serial
                        foreach ($returnProduct['sProducts'] as $serialProduct) {
                            $thisSerialBatchCode = $serialProduct['sBCode'];
                            $thisSerialBatchID = $returnProduct['bProducts'][$thisSerialBatchCode]['bID'];

                            //update batch and serial products
                            $serialUpdateData = array(
                                'productSerialReturned' => '1'
                            );
                            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialUpdateData, $serialProduct['sID']);
                            $this->updateBatchProductQuantity($thisSerialBatchID, 1);
                            //Add items to item out table ,Serial and batch
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($returnProduct['locationPID'], $thisSerialBatchID, $serialProduct['sID']);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Delete Positive Adjustment',
                                'itemOutDocumentID' => $goodsIssueID,
                                'itemOutLocationProductID' => $returnProduct['locationPID'],
                                'itemOutBatchID' => $thisSerialBatchID,
                                'itemOutSerialID' => $serialProduct['sID'],
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => 1,
                                'itemOutPrice' => $returnProduct['pUnitPrice'],
                                'itemOutDiscount' => NULL,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + 1;

                            $updatedItemInDetails = array(
                                'itemInSoldQty' => $updateQty,
                                'itemInDeletedFlag' => 1,
                            );
                            $itemInUpdateStatue = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($updatedItemInDetails, $itemInDetails['itemInID']);
                            // $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        }
                    } else if (array_key_exists('bProducts', $returnProduct)) {
                        //Create Adjustment Product for products that have batch products only
                        foreach ($returnProduct['bProducts'] as $batchProduct) {
                            //update batch products
                            $this->updateBatchProductQuantity($batchProduct['bID'], $batchProduct['bRQty']);
                            //add details for item out batch products
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($returnProduct['locationPID'], $batchProduct['bID']);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Delete Positive Adjustment',
                                'itemOutDocumentID' => $goodsIssueID,
                                'itemOutLocationProductID' => $returnProduct['locationPID'],
                                'itemOutBatchID' => $batchProduct['bID'],
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $batchProduct['bRQty'],
                                'itemOutPrice' => $returnProduct['pUnitPrice'],
                                'itemOutDiscount' => NULL,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + $batchProduct['bRQty'];

                            $updatedItemInDetails = array(
                                'itemInSoldQty' => $updateQty,
                                'itemInDeletedFlag' => 1,
                            );
                            $itemInUpdateStatue = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($updatedItemInDetails, $itemInDetails['itemInID']);
                            // $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        }
                    } else if (array_key_exists('sProducts', $returnProduct)) {
                        //Create Adjustment Product for products that have serial products only
                        foreach ($returnProduct['sProducts'] as $serialProduct) {
                            //update Serial Products
                            $serialUpdateData = array(
                                'productSerialReturned' => '1'
                            );
                            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialUpdateData, $serialProduct['sID']);
                            //add details for item out table serieal products only///////////
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($returnProduct['locationPID'], $serialProduct['sID']);

                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Delete Positive Adjustment',
                                'itemOutDocumentID' => $goodsIssueID,
                                'itemOutLocationProductID' => $returnProduct['locationPID'],
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => $serialProduct['sID'],
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => 1,
                                'itemOutPrice' => $returnProduct['pUnitPrice'],
                                'itemOutDiscount' => NULL,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);

                            $updatedItemInDetails = array(
                                'itemInSoldQty' => $updateQty,
                                'itemInDeletedFlag' => 1,
                            );
                            $itemInUpdateStatue = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($updatedItemInDetails, $itemInDetails['itemInID']);

                            // $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);
                        }
                    } else {
                        //Create Adjustment Product for products that don't have either batch or serial products
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetailsByDocument($returnProduct['locationPID'], 'Inventory Positive Adjustment', $goodsIssueID);
                        //check whether items were sold or enough to return

                        if ($itemInDetails['itemInRemainingQty'] >= $returnProduct['pReturnQuantity']) {
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Delete Positive Adjustment',
                                'itemOutDocumentID' => $goodsIssueID,
                                'itemOutLocationProductID' => $returnProduct['locationPID'],
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $returnProduct['pReturnQuantity'],
                                'itemOutPrice' => $returnProduct['pUnitPrice'],
                                'itemOutDiscount' => NULL,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $updateQty = $itemInDetails['itemInSoldQty'] + $returnProduct['pReturnQuantity'];
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);

                            $updatedItemInDetails = array(
                                'itemInSoldQty' => $updateQty,
                                'itemInDeletedFlag' => 1,
                            );
                            $itemInUpdateStatue = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($updatedItemInDetails, $itemInDetails['itemInID']);
                            // $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        } else {
                            $sellingQty = $returnProduct['pReturnQuantity'];
                            while ($sellingQty != 0) {
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($returnProduct['locationPID']);
                                if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                                    $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                                    $itemOutM = new ItemOut();
                                    $itemOutData = array(
                                        'itemOutDocumentType' => 'Delete Positive Adjustment',
                                        'itemOutDocumentID' => $goodsIssueID,
                                        'itemOutLocationProductID' => $returnProduct['locationPID'],
                                        'itemOutBatchID' => NULL,
                                        'itemOutSerialID' => NULL,
                                        'itemOutItemInID' => $itemInDetails['itemInID'],
                                        'itemOutQty' => $sellingQty,
                                        'itemOutPrice' => $itemInDetails['itemInPrice'],
                                        'itemOutDiscount' => $itemInDetails['itemInDiscount'],
                                        'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                    $itemOutM->exchangeArray($itemOutData);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    $sellingQty = 0;
                                    break;
                                } else {
                                    $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                                    $itemOutM = new ItemOut();
                                    $itemOutData = array(
                                        'itemOutDocumentType' => 'Delete Positive Adjustment',
                                        'itemOutDocumentID' => $goodsIssueID,
                                        'itemOutLocationProductID' => $returnProduct['locationPID'],
                                        'itemOutBatchID' => NULL,
                                        'itemOutSerialID' => NULL,
                                        'itemOutItemInID' => $itemInDetails['itemInID'],
                                        'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                        'itemOutPrice' => $itemInDetails['itemInPrice'],
                                        'itemOutDiscount' => $itemInDetails['itemInDiscount'],
                                        'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                    $itemOutM->exchangeArray($itemOutData);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                                }
                            }
                        }
                    }

                    $this->updateLocationProductQty($returnProduct['locationPID'], $returnProduct['pReturnTotalQuantity']);
                   
                    $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($returnProduct['pID'], $locationID)->current();


                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getProductDetailsBylocationProductIDDocTypeAndDocumentID($returnProduct['locationPID'], "Inventory Positive Adjustment", $returnProduct['goodsIssueID']);

                    foreach ($itemInDetails as $key => $value) {
                        
                        $updateAverageCosting = $this->calculateAvgCostAndOtherDetails($locationProductDetails, $value);
                    }
                }
                    // die();
            }

            if ($this->useAccounting == 1) {
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('16', $goodsIssueID);

                $journalEntryID = $journalEntryData['journalEntryID'];
                if(!empty($journalEntryID)){
                    $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);                   
                    $i=0;
                    $journalEntryAccounts = array();
                    foreach ($jEAccounts as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Inventory Adjustment Delete '.$goodsIssueCode.'.';
                        $i++;
                    }

                    //get journal entry reference number.
                    $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                    $jelocationReferenceID = $jeresult['locRefID'];
                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                    $journalEntryData = array(
                        'journalEntryAccounts' => $journalEntryAccounts,
                        'journalEntryDate' => $this->convertDateToStandardFormat($this->getUserDateTime()),
                        'journalEntryCode' => $JournalEntryCode,
                        'journalEntryTypeID' => '',
                        'journalEntryIsReverse' => 0,
                        'journalEntryComment' => 'Journal Entry is posted when delete inventory adjustment '.$goodsIssueCode.'.',
                        'documentTypeID' => 16,
                        'journalEntryDocumentID' => $goodsIssueID,
                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                        );

                    $resultData = $this->saveJournalEntry($journalEntryData);

                    if(!$resultData['status']){
                        $this->status = false;
                        $this->msg = $resultData['msg'];
                        $this->data = $resultData['data'];
                        return $this->JSONRespond();
                    }

                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($goodsIssueID,16, 5);
                    if(!$jEDocStatusUpdate['status']){
                        $this->status = false;
                        $this->msg = $jEDocStatusUpdate['msg'];
                        return $this->JSONRespond();
                    }

                    $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(16,$goodsIssueID);
                    $dimensionData = [];
                    foreach ($jEDimensionData as $value) {
                        if (!is_null($value['journalEntryID'])) {
                            $temp = [];
                            $temp['dimensionTypeId'] = $value['dimensionType'];
                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                            $dimensionData[$goodsIssueCode][] = $temp;
                        }
                    }
                    if (!empty($dimensionData)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$goodsIssueCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->data = $saveRes['data'];
                            $this->msg = $saveRes['msg'];
                            return $this->JSONRespond();
                        }   
                    }
                }
            }

            //get data related to goodIssue table
            $goodIssue = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getGoodsIssue($goodsIssueID);
            $entityID = $goodIssue->entityID;
            //update entity table when adjustment deleted
            $this->updateDeleteInfoEntity($entityID);

            //update goodIssue Table status as it is cancelled
            $closeStatusID = $this->getStatusID('cancelled');
            $data = array(
                'goodsIssueStatus' => $closeStatusID
            );
            $this->CommonTable('Inventory\Model\GoodsIssueTable')->update($data, $goodsIssueID);
            $this->commit();

            $this->getPaginatedGoodsIssue();
            $adjustmentListView = new ViewModel(array('adjustment' => $this->paginator, 'statuses' => $this->getStatusesList()));
            $adjustmentListView->setTerminal(true);
            $adjustmentListView->setTemplate('inventory/inventory-adjustment/adjustment-list');

            $this->msg = $this->getMessage('SUCC_INVADJ_DELETE');
            $this->setLogMessage($goodsIssueCode.' is deleted.');
            //$this->msg = 'Successfully deleted Adjustment .!!';
            $this->status = true;
            $this->html = $adjustmentListView;
            return $this->JSONRespondHtml();
        } else {
            $this->data = 'There have no any post data';
            $this->msg = $this->getMessage('ERR_INVADJ_DELETE');
            $this->status = FALSE;
            return $this->JSONRespond();
        }
    }

    private function calculateAvgCostAndOtherDetails($locationProductDetails, $productDataSet = null)
    {
        //get all iteminDetails
        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductIDInDecendingOrder($productDataSet['itemInLocationProductID'],$productDataSet['itemInID']);

        $initialState = 0;
        $locationProductQuantity = floatval($locationProductDetails['locationProductQuantity']);

        foreach ($itemInDetails as $key => $itemInSingleValue) {
            if ($initialState == 0) {
                //get sum of qty from item out for calculate costing
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
            } else {
                $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();

                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();

            }
            $initialState++;

            $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQty['totalQty'])) - floatval($itemInSingleValue['itemInQty']);

        }
        

        $nextInRecordFromCancelPi = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();


        $outDetailsQtyForPi = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($productDataSet['itemInLocationProductID'], $productDataSet['itemInDateAndTime'], $nextInRecordFromCancelPi['itemInDateAndTime'])->current();  


        $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQtyForPi['totalQty'])) - floatval($productDataSet['itemInQty']);

        $previousItemInFromCancelPi = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryPreviousRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();


        $outDetailsQtyForPreviousDoc = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($previousItemInFromCancelPi['itemInLocationProductID'], $previousItemInFromCancelPi['itemInDateAndTime'], $productDataSet['itemInDateAndTime'])->current();

        $locationProductQuantity = floatval($locationProductQuantity) + floatval($outDetailsQtyForPreviousDoc['totalQty']);

        if ($previousItemInFromCancelPi) {
            //get all iteminDetails after cancel
            $itemInDetailsAfterCancel = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductID($previousItemInFromCancelPi['itemInLocationProductID'],$previousItemInFromCancelPi['itemInID']);
            $avarageCosting = floatval($previousItemInFromCancelPi['itemInAverageCostingPrice']);
        
        } else {
            $itemInDetailsAfterCancel = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID']);
            $avarageCosting = 0;
        }

        $initialState = 0;
        $locationUpdateFlag = false;
        foreach ($itemInDetailsAfterCancel as $key => $itemInSingleValue) {

            $locationUpdateFlag = true;
            //get very Next record in itemIn Table
            $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();
        
            
            if($nextInRecord){
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();
                
            } else {
                if (sizeof($itemInDetails) != 0) {
                    $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
                }
            }
            
            if ($initialState == 0 && $previousItemInFromCancelPi) {
                $newAvgCosting = $avarageCosting;
            } else {
                $newAvgCosting = (($locationProductQuantity * $avarageCosting) + (floatval($itemInSingleValue['itemInQty']) * floatval($itemInSingleValue['itemInPrice'])))/(floatval($itemInSingleValue['itemInQty']) + $locationProductQuantity);
                $avarageCosting = $newAvgCosting;
                $locationProductQuantity = $locationProductQuantity + floatval($itemInSingleValue['itemInQty']);
            }
            
            //set data to update itemIn Table
            $itemInUpdatedDataSet = array(
                'itemInAverageCostingPrice' => $newAvgCosting,
            );

            // var_dump($newAvgCosting);

            //update item in table
            $itemInUpdateFlag = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($itemInUpdatedDataSet, $itemInSingleValue['itemInID']);
            if(!$itemInUpdateFlag){
                $returnCostingDetails = array(
                    'status' => false,
                    );
                return $returnCostingDetails;
            }
            
            $locationProductQuantity = $locationProductQuantity - floatval($outDetailsQty['totalQty']);
            //set data to update locationProduct
            $locationProductUpdatedData = array(
                'locationProductAverageCostingPrice' => $newAvgCosting
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $itemInSingleValue['itemInLocationProductID']);


            if($nextInRecord){
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime'],$nextInRecord['itemInDateAndTime']);
        
            } else {
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime']);

            }

           
            foreach ($itemOutDetails as $key => $itemOutSingleValue) {
                //set Data to update itemOut table
                $updateItemOutCValue = array(
                    'itemOutAverageCostingPrice' => $newAvgCosting,
                );

                //update costing value in itemOutRecords
                $updateItemOut = $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutTable($itemOutSingleValue['itemOutID'],$updateItemOutCValue);

                $itemOutIds = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDataByItemOutID($itemOutSingleValue['itemOutID'])->current();

                $journalEntryData = array(
                    'journalEntryAccountsDebitAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    'journalEntryAccountsCreditAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    );

                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryByDocumentId($journalEntryData, $itemOutIds['journalEntryID']);
                
                if(!$updateItemOut){
                    
                    $returnCalculateData = array(
                        'status' => false,
                    );
                
                    return $returnCalculateData;
                }
            }
            $initialState++;
        }

        if (!$locationUpdateFlag) {
            $locationProductUpdatedData = array(
                'locationProductAverageCostingPrice' => 0
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $productDataSet['itemInLocationProductID']);

        }

        $returnCostingDetails = array(
            'status' => true,
            );
        return $returnCostingDetails;
    }





    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function getAdjustmentForSearchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $statuses = $globaldata['statuses'];
            $dateFormat = $this->getUserDateFormat();

            if ($request->getPost('searchAdjustmentString') == '') {
                $this->getPaginatedGoodsIssue();
                $adjustmentListView = new ViewModel(array('adjustment' => $this->paginator,
                    'statuses' => $statuses, 'dateFormat' => $dateFormat));
                $adjustmentListView->setTerminal(true);
                $adjustmentListView->setTemplate('inventory/inventory-adjustment/adjustment-list');
                $this->html = $adjustmentListView;
                return $this->JSONRespondHtml();
            } else {
                $adjustment = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getAdjustmentforSearch($request->getPost('searchAdjustmentString'), true);
                $adjustmentListView = new ViewModel(array('adjustment' => $adjustment, 'statuses' => $statuses, 'dateFormat' => $dateFormat));
                $adjustmentListView->setTemplate('inventory/inventory-adjustment/adjustment-list');
                $this->html = $adjustmentListView;
                return $this->JSONRespondHtml();
            }
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return JSONRespond
     */
    public function getAdjustmentReferenceNoAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $refData = $this->getReferenceNoForLocation(12, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];
            if ($rid == '' || $rid == NULL) {
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_INVADJ_REFLOCATION');
                    //$this->msg = 'No Adjusment reference no for this location';
                } else {
                    $this->msg = $this->getMessage('ERR_INVADJ_REFMAX');
                }
                $this->data = FALSE;
                $this->status = false;
            } else {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $rid,
                    'locRefID' => $lrefID,
                );
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * set paginator
     */
    protected function getPaginatedGoodsIssue()
    {
        $this->paginator = $this->CommonTable('Inventory\Model\GoodsIssueTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $total1
     * @param type $total2
     * @return Subtration of two variable
     */
    public function getSubQty($total1, $total2, $productType = false)
    {
        if ($productType == 2) {
            $total2 = 0;
        }
        $total = $total1 - $total2;
        return $total;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $total1
     * @param type $total2
     * @return sum of two variable
     */
    public function getSumQty($total1, $total2)
    {
        $total = $total1 + $total2;
        return $total;
    }

    /**
     * Search Adjustments for dropdown
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchAdjstmentForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $adjustments = $this->CommonTable('Inventory\Model\GoodsIssueTable')->searchAdjustmentForDropDown($searchKey);

            $adjustmentList = array();
            foreach ($adjustments as $adjustment) {
                $temp['value'] = $adjustment['goodsIssueID'];
                $temp['text'] = $adjustment['goodsIssueCode'];
                $adjustmentList[] = $temp;
            }

            $this->data = array('list' => $adjustmentList);
            return $this->JSONRespond();
        }
    }

    /**
     * Get adjustment By Adjustment ID for view page
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getAdjustmentByAdjustmentIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $globaldata = $this->getServiceLocator()->get('config');
            $statuses = $globaldata['statuses'];
            $dateFormat = $this->getUserDateFormat();

            if ($request->getPost('adjustmentID') == '') {
                $this->getPaginatedGoodsIssue();
                $adjustmentListView = new ViewModel(array('adjustment' => $this->paginator,
                    'statuses' => $statuses, 'dateFormat' => $dateFormat));
            } else {
                $adjustment = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getAdjustmentForSearchByAdjustmentsID($request->getPost('adjustmentID'));
                $adjustmentListView = new ViewModel(array('adjustment' => $adjustment, 'statuses' => $statuses, 'dateFormat' => $dateFormat, 'paginated' => true));
            }

            $adjustmentListView->setTerminal(true);
            $adjustmentListView->setTemplate('inventory/inventory-adjustment/adjustment-list');
            $this->html = $adjustmentListView;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     *
     */
    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Inventory Adjustment';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_INV_ADJ_EMAIL_SENT');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_INV_ADJ_EMAIL_SENT');
        return $this->JSONRespond();
    }

    //Update returned location Products
    public function updateLocationProductQty($locationPID, $returnedQty)
    {
        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
        $newPQty = floatval($currentPQty->locationProductQuantity) - floatval($returnedQty);
        $newPQtyData = array(
            'locationProductQuantity' => $newPQty
        );
        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
    }

    //Update returned Batch Quantities
    public function updateBatchProductQuantity($batchID, $returnQty)
    {

        $batchData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchID);
        $batchOldQty = $batchData->productBatchQuantity;
        $batchNewQty = $batchOldQty - floatval($returnQty);
        $batchUpdateData = array(
            'productBatchQuantity' => $batchNewQty
        );
        $this->CommonTable('Inventory\Model\ProductBatchTable')->updateBatchByID($batchID, $batchUpdateData);
    }

    public function adjustmentItemImportAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $lineSet = [];
        $headerData = [];
        $rowData = [];
        $prototype = [];
        $dataRow = [];
        $lineByLineRecords = [];

        $dataSet = $request->getPost('dataSet');
        $delimiter = $request->getPost('productDelimiter');
        $dataType = $request->getPost('dataType');
        $batchSerialType = $request->getPost('batchSerialType');
        $proHeader = $request->getPost('productHeader');

        if($dataSet){
            $lineRecords = explode("\n", $dataSet);
            $headerData = explode($delimiter, $lineRecords[0]);
            $rowData = explode($delimiter, $lineRecords[1]);

            foreach (array_filter($lineRecords) as $key => $value) {
                if(sizeof($headerData) != sizeof(array_filter(explode($delimiter, $value)))){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_ADJUSTMENT_ITEM_LIST_NOT_VALID');
                    return $this->JSONRespond();
                }else {
                    $dataRow[$key] = explode($delimiter, $lineRecords[$key]);
                }
            }

            for($i = 0; $i < sizeof($lineRecords); $i++){
                for($j = 0; $j < sizeof($dataRow[$i]); $j++){
                    $lineByLineRecords[$j][] = $dataRow[$i][$j];
                }
            }

            $returnData = array(
                'headerFlag' => $proHeader,
                'dataSet' => $lineByLineRecords,
                'hiddenDataSet' => base64_encode(serialize($lineByLineRecords)),
                );

            $this->status = true;
            $this->data = $returnData;
            return $this->JSONRespond();

        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ADJUSTMENT_REQ_DATA_NOT_SET');
            return $this->JSONRespond();
        }

    }

    public function uomMappingAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }

        $dataSet = unserialize(base64_decode($request->getPost('dataArray')));
        $id = explode("_", $request->getPost('id'));

        $uomDataSet = array_unique($dataSet[$id[1]]);
        $currentUom = [];
        $systemUomList = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        foreach ($systemUomList as $key => $value) {
            $currentUom[] = array(
                'uomName' => $value->uomName,
                'uomID' => $value->uomID,
                );
        }
        $returnDataSet = array(
            'systemUomList' => $currentUom,
            'uploadedUomList' => $uomDataSet,
            );

        $this->data = $returnDataSet;
        $this->status = true;
        return $this->JSONRespond();

    }

    public function loadImportedDataToAdjustmentCreateAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }

        $dataSet = $request->getPost('dataArray');
        $batchSerialType = $request->getPost('batchSerialType');
        $productDelimiter = $request->getPost('productDelimiter');
        $dataType = $request->getPost('dataType');
        $fileData = $request->getPost('dataSet');
        $header = $request->getPost('productHeader');
        $uomMapArray = $request->getPost('uomMapArray');
        $adjustmentType = $request->getPost('adjustmentType');
        $batchData = [];
        $serialData = [];
        $fileDetails = unserialize(base64_decode($fileData));
        $productDetails = [];
        $rowDetails = [];
        $inValidUploadCount = 0;
        $invalidDetails = [];
        $arrayDup = [];

        if($batchSerialType == 'batchItem'){
            $rowCounter = 0;
            foreach ($fileDetails[$dataSet['itemCode']] as $key => $value) {
                $rowCounter++;
                if($header == 'true' && $key == 0){
                    continue;
                } else {
                    $productID = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($value, false, true);

                    $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($productID['productID'], $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]]);

                    if ($adjustmentType == "2") {
                        if($productID){
                            $locationID = $request->getPost('locationID');
                            $locationProduct = $this->getLocationProductDetails($locationID, $productID['productID']);
                            $productDetails['locProduct'] = $locationProduct;

                            $batchData = array(
                                'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                'newBQty' => $fileDetails[$dataSet['quantity']][$key],
                                'newBMDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['manDate']][$key]),
                                'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                'newBPrice' => $fileDetails[$dataSet['unitPrice']][$key],
                            );
                            $productDetails['batchData'][$fileDetails[$dataSet['batchCode']][$key]] = $batchData;
                            $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key] / floatval($productConversionRate);
                            $productDetails['qnty'] = $fileDetails[$dataSet['quantity']][$key] * floatval($productConversionRate);
                            $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                            $productDetails['serialCode'] = null;

                            $rowDetails[] = $productDetails;
                            $productDetails = [];
                        } else {
                            $inValidUploadCount++;
                            $invalidDetails[] = $rowCounter;
                        }
                    } else {
                        if($productID){
                            $locationID = $request->getPost('locationID');
                            $locationProduct = $this->getLocationProductDetails($locationID, $productID['productID']);
                            $productDetails[$locationProduct['lPID']]['locProduct'] = $locationProduct;

                            $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProductsByCodeAndLocationProductId($fileDetails[$dataSet['batchCode']][$key], $locationProduct['lPID'],$fileDetails[$dataSet['quantity']][$key])->current();

                            if (!array_key_exists($batchDetails['productBatchID'], $arrayDup)) {

                                $batchData = array(
                                    'bID' => 'bID_'.$batchDetails['productBatchID'],
                                    'qty' => $fileDetails[$dataSet['quantity']][$key] * floatval($productConversionRate),
                                );

                                $productDetails[$locationProduct['lPID']]['batchData'][$batchDetails['productBatchID']] = $batchData;
                                $productDetails[$locationProduct['lPID']]['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key] / floatval($productConversionRate);
                                $productDetails[$locationProduct['lPID']]['availableQty'] = $locationProduct['LPQ'];
                                $productDetails[$locationProduct['lPID']]['qnty'] += $fileDetails[$dataSet['quantity']][$key] * floatval($productConversionRate);
                                $productDetails[$locationProduct['lPID']]['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                                $productDetails[$locationProduct['lPID']]['serialCode'] = null;
                                $arrayDup[$batchDetails['productBatchID']] = $batchDetails['productBatchCode'];
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;    
                            }
                        } else {
                            $inValidUploadCount++;
                            $invalidDetails[] = $rowCounter;
                        }
                    }
                }
                    
            }
            if ($adjustmentType == "1") {
                foreach ($productDetails as $key => $value) {
                    $rowDetails[] = $value;
                }
            }

        } else if($batchSerialType == 'serialItem'){
            $rowCounter = 0;
            foreach ($fileDetails[$dataSet['itemCode']] as $key => $value) {
                $rowCounter++;
                if($header == 'true' && $key == 0){
                    continue;
                } else{
                    $currentProduct = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($value, true, false);

                    if ($adjustmentType == "2") {
                        if(array_key_exists($value, $rowDetails)){
                            $validateSerialFlag = $this->validateSerialItems($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'], $value, null, null);
                            if($validateSerialFlag){
                                $rowDetails[$value]['qnty']++;
                                $serialData = array(
                                    'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                    'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                    'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                    'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                );
                                $rowDetails[$value]['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }

                        } else{
                            if($currentProduct){
                                $locationID = $request->getPost('locationID');
                                $locationProduct = $this->getLocationProductDetails($locationID, $currentProduct['productID']);
                                $validateSerialFlag = $this->validateSerialItems($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'],$value, null, 1);

                                if($validateSerialFlag){
                                    $productDetails['locProduct'] = $locationProduct;

                                    $serialData = array(
                                        'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                        'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                        'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                        'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                    );
                                    $productDetails['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                                    $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key];
                                    // $productDetails['qnty'] = (!empty($fileDetails[$dataSet['quantity']][$key])) ? $fileDetails[$dataSet['quantity']][$key] : 1;
                                    $productDetails['qnty'] = 1;
                                    $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                                    $productDetails['batchData'] = null;

                                    $rowDetails[$value] = $productDetails;
                                    $productDetails = [];
                                } else {
                                    $inValidUploadCount++;
                                    $invalidDetails[] = $rowCounter;
                                }
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }
                        }
                    } else {
                        if(array_key_exists($value, $rowDetails)){
                            $validateSerialFlag = $this->validateSerialItemForNegativeAdjustment($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'], $value, null);
                            if($validateSerialFlag){
                                $rowDetails[$value]['qnty']++;
                                $serialData = array(
                                    's_code' => $fileDetails[$dataSet['serialCode']][$key],
                                    'baseQty' => 1,
                                );
                                $rowDetails[$value]['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }

                        } else{
                            if($currentProduct){
                                $locationID = $request->getPost('locationID');
                                $locationProduct = $this->getLocationProductDetails($locationID, $currentProduct['productID']);
                                $validateSerialFlag = $this->validateSerialItemForNegativeAdjustment($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'],$value, null);

                                if($validateSerialFlag){
                                    $productDetails['locProduct'] = $locationProduct;

                                    $serialData = array(
                                        's_code' => $fileDetails[$dataSet['serialCode']][$key],
                                        'baseQty' => 1,
                                    );
                                    $productDetails['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                                    $productDetails['availableQty'] = $locationProduct['LPQ'];
                                    $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key];
                                    $productDetails['qnty'] = 1;
                                    $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                                    $productDetails['batchData'] = null;

                                    $rowDetails[$value] = $productDetails;
                                    $productDetails = [];
                                } else {
                                    $inValidUploadCount++;
                                    $invalidDetails[] = $rowCounter;
                                }
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }
                        }
                    }

                }

            }

        } else if ($batchSerialType == 'batchSerialItem'){
            $countKey = 0;
            $rowCounter = 0;
            foreach ($fileDetails[$dataSet['itemCode']] as $key => $value) {
                $rowCounter++;
                if($header == 'true' && $key == 0){
                    continue;
                } else{
                    $currentProduct = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($value, true, true);

                    if ($adjustmentType == "2") {
                        if(array_key_exists($value, $rowDetails) && array_key_exists($fileDetails[$dataSet['batchCode']][$key], $rowDetails[$value][$countKey]['batchData'])){
                            $validateSerialFlag = $this->validateSerialItems($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'], $value, $fileDetails[$dataSet['batchCode']][$key], null);
                            if($validateSerialFlag){
                                $rowDetails[$value][$countKey]['batchData'][$fileDetails[$dataSet['batchCode']][$key]]['newBQty']++;
                                $rowDetails[$value][$countKey]['qnty']++;
                                $serialData = array(
                                    'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                    'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                    'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                    'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                );
                                $rowDetails[$value][$countKey]['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }
                        } else{
                            if($currentProduct){
                                $locationID = $request->getPost('locationID');
                                $locationProduct = $this->getLocationProductDetails($locationID, $currentProduct['productID']);
                                $validateSerialFlag = $this->validateSerialItems($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'], $value, $fileDetails[$dataSet['batchCode']][$key], 1);
                                if($validateSerialFlag){
                                    $productDetails['locProduct'] = $locationProduct;

                                    $batchData = array(
                                        'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                        'newBQty' => (!empty($fileDetails[$dataSet['quantity']][$key])) ? $fileDetails[$dataSet['quantity']][$key] : 1,
                                        'newBMDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['manDate']][$key]),
                                        'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                        'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                        'newBPrice' => $fileDetails[$dataSet['unitPrice']][$key],
                                    );

                                    $serialData = array(
                                        'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                        'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                        'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                        'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                    );
                                    $productDetails['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                                    $productDetails['batchData'][$fileDetails[$dataSet['batchCode']][$key]] = $batchData;
                                    $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key];
                                    $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];

                                    $productDetails['qnty'] = 1;

                                    $countKey++;
                                    $rowDetails[$value][$countKey] = $productDetails;
                                    $productDetails = [];
                                } else {
                                    $inValidUploadCount++;
                                    $invalidDetails[] = $rowCounter;
                                }
                            } else{
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }
                        }
                    } else {
                        if(array_key_exists($value, $rowDetails) && array_key_exists($fileDetails[$dataSet['batchCode']][$key], $rowDetails[$value][$countKey]['batchData'])){
                            $validateSerialFlag = $this->validateSerialItemForNegativeAdjustment($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'], $value, $fileDetails[$dataSet['batchCode']][$key], null);
                            if($validateSerialFlag){
                                $rowDetails[$value][$countKey]['batchData'][$fileDetails[$dataSet['batchCode']][$key]]['newBQty']++;
                                $rowDetails[$value][$countKey]['qnty']++;
                                $serialData = array(
                                    'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                    'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                    'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                    'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                );
                                $rowDetails[$value][$countKey]['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }
                        } else{
                            if($currentProduct){
                                $locationID = $request->getPost('locationID');
                                $locationProduct = $this->getLocationProductDetails($locationID, $currentProduct['productID']);
                                $validateSerialFlag = $this->validateSerialItemForNegativeAdjustment($fileDetails[$dataSet['serialCode']][$key], $rowDetails, $currentProduct['productID'], $value, $fileDetails[$dataSet['batchCode']][$key], 1);
                                if($validateSerialFlag){
                                    $productDetails['locProduct'] = $locationProduct;

                                    $batchData = array(
                                        'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                        'newBQty' => (!empty($fileDetails[$dataSet['quantity']][$key])) ? $fileDetails[$dataSet['quantity']][$key] : 1,
                                        'newBMDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['manDate']][$key]),
                                        'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                        'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                        'newBPrice' => $fileDetails[$dataSet['unitPrice']][$key],
                                    );

                                    $serialData = array(
                                        'newBCode' => $fileDetails[$dataSet['batchCode']][$key],
                                        'newSCode' => $fileDetails[$dataSet['serialCode']][$key],
                                        'newBEDate' => $this->convertDateToStandardFormat($fileDetails[$dataSet['exDate']][$key]),
                                        'newBWrnty' => $fileDetails[$dataSet['wDays']][$key],
                                    );
                                    $productDetails['serilaData'][$fileDetails[$dataSet['serialCode']][$key]] = $serialData;
                                    $productDetails['batchData'][$fileDetails[$dataSet['batchCode']][$key]] = $batchData;
                                    $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key];
                                    $productDetails['availableQty'] = $locationProduct['LPQ'];
                                    $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];

                                    $productDetails['qnty'] = 1;

                                    $countKey++;
                                    $rowDetails[$value][$countKey] = $productDetails;
                                    $productDetails = [];
                                } else {
                                    $inValidUploadCount++;
                                    $invalidDetails[] = $rowCounter;
                                }
                            } else{
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;
                            }

                        }
                    }


                }

            }

        } else if($batchSerialType == 'normalItem'){
            $rowCounter = 0;
            foreach ($fileDetails[$dataSet['itemCode']] as $key => $value) {
                $rowCounter++;
                if($header == 'true' && $key == 0){
                    continue;
                } else{
                    $productID = $this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($value, false, false);

                    $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($productID['productID'], $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]]);

                    if($productID){
                        $locationID = $request->getPost('locationID');
                        $locationProduct = $this->getLocationProductDetails($locationID, $productID['productID']);
                        if ($adjustmentType == "2") {
                            $productDetails['locProduct'] = $locationProduct;
                            $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key] / floatval($productConversionRate);
                            $productDetails['qnty'] = $fileDetails[$dataSet['quantity']][$key] * floatval($productConversionRate);
                            $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                            $productDetails['serialCode'] = null;
                            $productDetails['batchData'] = null;

                            $rowDetails[] = $productDetails;
                            $productDetails = [];
                        } else {
                            if (floatval($locationProduct['LPQ']) >= $fileDetails[$dataSet['quantity']][$key]) {
                                $productDetails['locProduct'] = $locationProduct;
                                $productDetails['availableQty'] = $locationProduct['LPQ'];
                                $productDetails['uPrice'] = $fileDetails[$dataSet['unitPrice']][$key];
                                $productDetails['qnty'] = $fileDetails[$dataSet['quantity']][$key];
                                $productDetails['uomID'] = $uomMapArray[$fileDetails[$dataSet['uomVal']][$key]];
                                $productDetails['serilaData'] = null;
                                $productDetails['batchData'] = null;

                                $rowDetails[] = $productDetails;
                                $productDetails = [];
                            } else {
                                $inValidUploadCount++;
                                $invalidDetails[] = $rowCounter;        
                            }
                        }
                    } else {
                        $inValidUploadCount++;
                        $invalidDetails[] = $rowCounter;
                    }


                }
            }
        }

        if($inValidUploadCount > 0){
            $invalidDataSet = array_unique($invalidDetails);
            if($inValidUploadCount == 1){
                $errorMsg = $inValidUploadCount.' Record with row number '.implode(", ", $invalidDataSet).' is';
            } else {
                $errorMsg = $inValidUploadCount.' Records with row numbers '.implode(", ", $invalidDataSet).' are';
            }
            $this->msg = $this->getMessage('ERR_ADJUSTMENT_INVALID_DATA_ROWS', [$errorMsg]);
            
        }

        $this->status = true;
        $this->data = $rowDetails;
        return $this->JSONRespond();

    }

    public function validateSerialItems($serialCode, $uploadedData, $productID, $itemCode, $batchCode = null, $stepOne = null)
    {
        if($productID){
            if($batchCode){
                $batchSerialDetails = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialDetailsByProductID($productID);
                foreach ($batchSerialDetails as $key => $bSValue) {
                    if($bSValue['productSerialCode'] == $serialCode && $bSValue['productBatchCode'] == $batchCode){
                        return false;
                    }
                }

                if(!$stepOne){
                    foreach ($uploadedData[$itemCode] as $key => $value) {
                        foreach ($value['serilaData'] as $key => $serialValue) {
                            if($serialValue['newSCode'] == $serialCode && $serialValue['newBCode'] == $batchCode){
                                return false;
                            }
                        }
                    }
                }


            } else {
                $batchSerialDetails = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialDetailsByProductID($productID);
                    foreach ($batchSerialDetails as $key => $bSValue) {
                        if($bSValue['productSerialCode'] == $serialCode){
                            return false;
                    }
                }

                if(!$stepOne) {
                    foreach ($uploadedData[$itemCode]['serilaData'] as $key => $value) {
                        if($value['newSCode'] == $serialCode){
                            return false;
                        }
                    }
                }
            }
            return true;
        } else {
            return false;
        }

    }

    /**
     * This function is used to validate serial and batch serial item in negative adjustment
     */
    public function validateSerialItemForNegativeAdjustment($serialCode, $uploadedData, $productID, $itemCode, $batchCode = null, $stepOne = null)
    {
        if($productID){
            if($batchCode){
                $batchSerialDetails = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialDetailsByProductID($productID);
                foreach ($batchSerialDetails as $key => $bSValue) {
                    if($bSValue['productSerialCode'] == $serialCode && $bSValue['productBatchCode'] == $batchCode && $bSValue['productSerialSold'] == "0"){
                        return true;
                    }
                }
            } else {
                $batchSerialDetails = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialDetailsByProductID($productID);
                foreach ($batchSerialDetails as $key => $bSValue) {
                    if($bSValue['productSerialCode'] == $serialCode && $bSValue['productSerialSold'] == "0"){
                        return true;
                    } 
                }
            }
            return false;
        } else {
            return false;
        }

    }

}
