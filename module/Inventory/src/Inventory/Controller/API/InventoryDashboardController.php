<?php

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;

class InventoryDashboardController extends CoreController
{
	protected $userID;
    protected $cdnUrl;
    protected $user_session;
    protected $paginator;
    protected $company;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

	public function getWidgetDataAction()
	{
		$request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                	$lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                 	$lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                 	$lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $locIds = $this->getActiveAllLocationsIds();
            $proIds = [];
            $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown([$locationID], $productSearchKey = NULL, $isSearch = FALSE);

            foreach ($productData as $value) {
                $proIds[$value['productID']] = $value['productID'];
            }
            $globalStockValueData = $this->getService('StockInHandReportService')->_getGlobalWiseStockValueData($proIds, [$locationID], "1", "");

            $totalStockValue = 0;
            $totalStockSellingValue = 0;
            foreach ($globalStockValueData as $key => $data) {
                $availableQty = 0;
                $totalCostValue = 0;
                $uPrice = 0;
                $availableQtyAmount = 0;
                $expectSellValue = 0;
                foreach ($data['proD'] as $key => $value) {
                    $aQty = $value['availableQty'] ? $value['availableQty'] : 0.00;
                    $expectSellValue += ($aQty * floatval($value['productDefaultSellingPrice']));
                    $availableQty += $aQty;
                    $costValue = $value['totalCostValue'] ? $value['totalCostValue'] : 0.00;
                    $discount = isset($value['discountValue']) ? $value['discountValue'] : 0;
                    $tax = isset($value['taxValue']) ? $value['taxValue'] : 0;
                    $totalCostValue += (($costValue + $tax) - $discount);
                }
                if ($availableQty != 0 && $availableQty != NULL) {
                    $uPrice = ($totalCostValue) / $availableQty;
                }
                $availableQtyAmount = $uPrice * $availableQty;
                $totalStockValue += $availableQtyAmount;
                $totalStockSellingValue += $expectSellValue;
            }

            $milReachedItems = $this->getItemWiseMinimumInventoryLevelDetails("", "1", [$locationID]);
            $reOrderReachedItems = $this->CommonTable('Inventory\Model\LocationProductTable')->getOverdueReorderLevelItem($locationID);
            $stockAgedDetails = $this->getStockAgingDetails($toDate, $period, $locationID);
            // $heigstSalesItemData = $this->_getDailySalesItemsDetails($fromDate,$toDate,$locationID);

            $bestMovingItems = $this->CommonTable('Inventory\Model\ItemOutTable')->getBestMovingItemByDateRange($fromDate, $toDate,$locationID);

            $movingCount = array_column($bestMovingItems, 'movingCount');

            array_multisort($movingCount, SORT_DESC, $bestMovingItems);

            header('Content-Type: application/json');
            $widgetData = array(
                'totalStockValue' => round($totalStockValue,2),
                'totalStockSellingValue' => round($totalStockSellingValue,2),
                'stockAgedDetails' => $stockAgedDetails,
                'bestMovingItems' => $bestMovingItems,
                'heigstSalesItemData' => $heigstSalesItemData,
                'totalMinInventoryItems' => count($milReachedItems[$locationID]['pD']),
                'totalActiveItems' => count($proIds),
                'reOrderReachedItems' => count($reOrderReachedItems),
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
	}

    public function getSalesGrossProfitDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $heigstSalesItemData = $this->_getDailySalesItemsDetails($fromDate,$toDate,$locationID);

            header('Content-Type: application/json');
            $widgetData = array(
                'heigstSalesItemData' => $heigstSalesItemData,
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    private function _getDailySalesItemsDetails($fromDate, $toDate, $locationID)
    {
        ini_set('memory_limit','-1');
        ini_set('max_execution_time','0');
        if (isset($fromDate) && isset($toDate)) {
            $dailySalesItemData = array();
            //because of mysql BETWEEN function
//            $toDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));

            $getInvoiceItemDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSalesInvoiceDetails($fromDate, $toDate,null,  $locationID);
            $getDlnWiseInvoiceItemDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getDlnWiseSalesInvoiceDetails($fromDate, $toDate, null, $locationID);

            $getItemDetails = array_merge($getInvoiceItemDetails, $getDlnWiseInvoiceItemDetails);
            if (!empty($getItemDetails)) {
                foreach ($getItemDetails as $row) {
                    $getLocationProduct = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProducts($row['itemOutLocationProductID'], $row['locationID']);

                    $productID = $getLocationProduct->productID;
                    $productName = $getLocationProduct->productName;
                    $productCode = $getLocationProduct->productCode;

                    if ($productID) {
                        //set quantity and uomAbbr according to display UOM
                        $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productID);
                        $thisqty = $this->getProductQuantityViaDisplayUom($row['itemOutQty'], $productUom);
                        $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                        $itemInUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemInPrice'], $productUom);
                        $itemOutUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemOutPrice'], $productUom);

                        $itemOutAvergeCosting = $row['itemOutAverageCostingPrice']; 
                        $itemInAverageCosting = $row['itemInAverageCostingPrice'];

                        if ($row['itemOutDocumentType'] == "Sales Invoice") {
                            //get product credit note details
                            $creditNoteProductDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceProductCreditNoteDetails($productID, $fromDate, $toDate);
                            $creditNoteCostDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getCreditNoteCostByCreditNoteIDAndProductId($productID, $fromDate, $toDate);
                            $creditNoteProductQty = $creditNoteProductDetails['creditNoteProductQty'];
                            $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal'];
                            $creditNoteCost = $creditNoteCostDetails['creditNoteCost'];
                        } else {
                            //if any return made to dln in this date range
                            $creditNoteProductDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceProductSalesReturnDetails($productID, $fromDate, $toDate, $row['salesInvoiceID']);

                            if (!is_null($row['itemOutSerialID'])) {
                                $creditNoteProductQty = sqrt($creditNoteProductDetails['creditNoteProductQty']);
                                $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal']/$creditNoteProductQty;
                                $creditNoteCost = ($creditNoteProductDetails['creditNoteCost']/$creditNoteProductQty) * $creditNoteProductQty;
                            } else {
                                $creditNoteProductQty = $creditNoteProductDetails['creditNoteProductQty'];
                                $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal'];
                                $creditNoteCost = $creditNoteProductDetails['creditNoteCost'] * $creditNoteProductQty;
                            }
                        }
                           
                        if ($row['itemOutDocumentType'] == "Delivery Note") {
                            $deliveryNoteID = $row['itemOutDocumentID'];
                            $getDeliveryNoteProductTax = $this->CommonTable("Invoice\Model\DeliveryNoteProductTaxTable")->getProductTax($deliveryNoteID, $productID);
                            $itemOutTaxAmount = 0;
                            if (!empty($getDeliveryNoteProductTax)) {
                                foreach ($getDeliveryNoteProductTax as $t) {
                                    $qty = ($t['deliveryNoteProductQuantity'] == NULL) ? 1 : $t['deliveryNoteProductQuantity'];
                                    $tax = $t['deliveryNoteTaxAmount'] / $qty;
                                    $itemOutTaxAmount += $tax;
                                }
                            }
                        } else {
                            $invoiceID = $row['itemOutDocumentID'];
                            $getInvoiceProductTax = $this->CommonTable("Invoice\Model\InvoiceProductTaxTable")->getProductTax($invoiceID, $productID);
                            $itemOutTaxAmount = 0;
                            if (!empty($getInvoiceProductTax)) {
                                foreach ($getInvoiceProductTax as $t) {
                                    $qty = ($t['salesInvoiceProductQuantity'] == NULL) ? 1 : $t['salesInvoiceProductQuantity'];
                                    $tax = $t['salesInvoiceProductTaxAmount'] / $qty;
                                    $itemOutTaxAmount += $tax;
                                }
                            }
                        }

                        $itemInTaxAmount = 0;
                        if ($row['itemInDocumentType'] == "Goods Received Note") {
                            $grnID = $row['itemInDocumentID'];
                            $getGrnTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($grnID, $row['itemInLocationProductID']);
                            foreach ($getGrnTax as $g) {
                                $grnQty = ($g['grnProductTotalQty'] == NULL) ? 1 : $g['grnProductTotalQty'];
                                $itemInTaxAmount += $g['grnTaxAmount'] / $grnQty;
                            }
                        } else if ($row['itemInDocumentType'] == "Payment Voucher") {
                            $purchaseInvoiceID = $row['itemInDocumentID'];
                            $purchaseInvoiceTax = $this->CommonTable("Inventory\Model\PurchaseInvoiceProductTaxTable")->getPurchaseInvoiceProductTax($purchaseInvoiceID, $row['itemInLocationProductID']);
                            foreach ($purchaseInvoiceTax as $p) {
                                $purchaseInvoiceQty = ($p['purchaseInvoiceProductTotalQty'] == NULL) ? 1 : $p['purchaseInvoiceProductTotalQty'];
                                $itemInTaxAmount += $p['purchaseInvoiceTaxAmount'] / $purchaseInvoiceQty;
                            }
                        }

                        // check costing method
                        $setup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

        
                        if (!$setup['averageCostingFlag']) {
                            $dailySalesItemData[$productID]['costingMethod'] = 'FIFO';
                        } else {
                            $dailySalesItemData[$productID]['costingMethod'] = "Average";
                        }

                        $dailySalesItemData[$productID]['productName'] = $productName;
                        $dailySalesItemData[$productID]['productCode'] = $productCode;
                        $dailySalesItemData[$productID]['creditNoteProductTotal'] = $creditNoteProductTotal;
                        $dailySalesItemData[$productID]['creditNoteProductQty'] = $creditNoteProductQty;
                        $dailySalesItemData[$productID]['creditNoteCost'] = $creditNoteCost;
                        $dailySalesItemData[$productID]['data'][$row['itemOutID']] = array(
                            'itemOutQty' => $quantity,
                            'itemOutPrice' => $itemOutUnitPrice,
                            'itemOutAvergeCosting' => $itemOutAvergeCosting,
                            'itemOutDiscount' => $row['itemOutDiscount'],
                            'itemInPrice' => $itemInUnitPrice,
                            'itemInAverageCosting' => $itemInAverageCosting,
                            'itemInDiscount' => $row['itemInDiscount'],
                            'uomAbbr' => $thisqty['uomAbbr'],
                            'itemOutTaxAmount' => $itemOutTaxAmount,
                            'itemInTaxAmount' => $itemInTaxAmount
                        );

                    }
                }
            }

            $finalData = [];
            $netTotalCost = 0;
            $netTotalSales = 0;
            $isDataExist = false;
            foreach ($dailySalesItemData as $data) {
                $totalCost = 0;
                $totalSales = 0;
                $totalQty = 0;
                $uomAbbr = '';
                $itemCreditNoteQty = ($data['creditNoteProductQty']) ? $data['creditNoteProductQty'] : 0;
                $itemCreditNoteTotal = ($data['creditNoteProductTotal']) ? $data['creditNoteProductTotal'] : 0;
                $itemCreditNoteCost = ($data['creditNoteCost']) ? $data['creditNoteCost'] : 0;
                foreach ($data['data'] as $value) {
                    $qty = ($value['itemOutQty'] != NULL) ? $value['itemOutQty'] : 0;
                    $totalQty+= $qty;
                    //item in calculations
                    $itemInPrice = ($value['itemInPrice'] != NULL) ? $value['itemInPrice'] : 0;
                    $itemInDiscount = ($value['itemInDiscount'] != NULL) ? $value['itemInDiscount'] : 0;
                    $itemInTaxAmount = $value['itemInTaxAmount'];
                    $itemInActualPrice = ($itemInPrice + $itemInTaxAmount) - $itemInDiscount;
                    $totalCost+= $itemInActualPrice * $qty;
                    $netTotalCost += $itemInActualPrice * $qty;
                    //item out calculations
                    $itemOutPrice = ($value['itemOutPrice'] != NULL) ? $value['itemOutPrice'] : 0;
                    $itemOutDiscount = ($value['itemOutDiscount'] != NULL) ? $value['itemOutDiscount'] : 0;
                    $itemOutTaxAmount = $value['itemOutTaxAmount'];
                    $itemOutActualPrice = ($itemOutPrice + $itemOutTaxAmount) - $itemOutDiscount;
                    $totalSales+= $itemOutActualPrice * $qty;
                    $netTotalSales += $itemOutActualPrice * $qty;
                    $uomAbbr = $value['uomAbbr'];
                }
                //item credit note calculations
                $totalQty = $totalQty - $itemCreditNoteQty;
                $totalSales = $totalSales - $itemCreditNoteTotal;
                $netTotalSales = $netTotalSales - $itemCreditNoteTotal;
                $totalCost = $totalCost - $itemCreditNoteCost;
                $netTotalCost = $netTotalCost - $itemCreditNoteCost;
                $profit = $totalSales - $totalCost;

                $temp['productName'] = $data['productName'];
                $temp['productCode'] = $data['productCode'];
                $temp['totalQty'] = round($totalQty);
                $temp['totalSales'] = $totalSales;
                $temp['totalCost'] = $totalCost;
                $temp['profit'] = $profit;

                $finalData[] = $temp;
            }

            $totalSales = array_column($finalData, 'totalSales');

            array_multisort($totalSales, SORT_DESC, $finalData);

            return $finalData;
        }
    }

    private function getStockAgingDetails($date,$period,$locationID)
    {
        $dataList = [];
        $finalData = [];

        switch ($period) {
            case 'thisYear':
                $results = $this->CommonTable('Inventory\Model\ItemInTable')->getStockAgeDetailsForDashbaordByYear($date);
                foreach ($results as $result) {
                    if (!array_key_exists($result['productID'], $dataList)) {
                        $dataList[$result['productID']] = [
                            'productName' => $result['productName'],
                            'productCode' => $result['productCode'],
                            'Over360StockValue' => $result['Over360StockValue'],
                            'WithIn360StockValue' => $result['WithIn360StockValue'],
                            'WithIn270StockValue' => $result['WithIn270StockValue'],
                            'WithIn180StockValue' => $result['WithIn180StockValue'],
                            'WithIn90StockValue' => $result['WithIn90StockValue'],
                            'WithIn30StockValue' => $result['WithIn30StockValue'],
                        ];
                    } else {
                        $dataList[$result['productID']]['Over360StockValue'] += $result['Over360StockValue'];
                        $dataList[$result['productID']]['WithIn360StockValue'] += $result['WithIn360StockValue'];
                        $dataList[$result['productID']]['WithIn270StockValue'] += $result['WithIn270StockValue'];
                        $dataList[$result['productID']]['WithIn180StockValue'] += $result['WithIn180StockValue'];
                        $dataList[$result['productID']]['WithIn90StockValue'] += $result['WithIn90StockValue'];
                        $dataList[$result['productID']]['WithIn30StockValue'] += $result['WithIn30StockValue'];
                    }
                }

                $over360StockValue = 0;
                $withIn360StockValue = 0;                        
                $withIn270StockValue = 0;                        
                $withIn180StockValue = 0;
                $withIn90StockValue = 0;
                $withIn30StockValue = 0;
                $tottalStockValue = 0;

                foreach ($dataList as $row) {
                    $over360StockValue += $row['Over360StockValue'];
                    $withIn360StockValue += $row['WithIn360StockValue'];
                    $withIn270StockValue += $row['WithIn270StockValue'];
                    $withIn180StockValue += $row['WithIn180StockValue'];
                    $withIn90StockValue += $row['WithIn90StockValue'];
                    $withIn30StockValue += $row['WithIn30StockValue'];
                }

                $finalData['0-30'] = $withIn30StockValue;
                $finalData['30-90'] = $withIn90StockValue;
                $finalData['90-180'] = $withIn180StockValue;
                $finalData['180-270'] = $withIn270StockValue;
                $finalData['270-360'] = $withIn360StockValue;
                $finalData['360+'] = $over360StockValue;
                break;
            case 'thisMonth':
                $results = $this->CommonTable('Inventory\Model\ItemInTable')->getStockAgeDetailsForDashbaordByMonth($date);
                foreach ($results as $result) {
                    if (!array_key_exists($result['productID'], $dataList)) {
                        $dataList[$result['productID']] = [
                            'productName' => $result['productName'],
                            'productCode' => $result['productCode'],
                            'WithIn30StockValue' => $result['WithIn30StockValue'],
                            'WithIn25StockValue' => $result['WithIn25StockValue'],
                            'WithIn20StockValue' => $result['WithIn20StockValue'],
                            'WithIn15StockValue' => $result['WithIn15StockValue'],
                            'WithIn10StockValue' => $result['WithIn10StockValue'],
                            'WithIn5StockValue' => $result['WithIn5StockValue'],
                        ];
                    } else {
                        $dataList[$result['productID']]['WithIn30StockValue'] += $result['WithIn30StockValue'];
                        $dataList[$result['productID']]['WithIn25StockValue'] += $result['WithIn25StockValue'];
                        $dataList[$result['productID']]['WithIn20StockValue'] += $result['WithIn20StockValue'];
                        $dataList[$result['productID']]['WithIn15StockValue'] += $result['WithIn15StockValue'];
                        $dataList[$result['productID']]['WithIn10StockValue'] += $result['WithIn10StockValue'];
                        $dataList[$result['productID']]['WithIn5StockValue'] += $result['WithIn5StockValue'];
                    }
                }

                $withIn30StockValue = 0;                        
                $withIn25StockValue = 0;                        
                $withIn20StockValue = 0;                        
                $withIn15StockValue = 0;
                $withIn10StockValue = 0;
                $withIn5StockValue = 0;

                foreach ($dataList as $row) {
                    $withIn30StockValue += $row['WithIn30StockValue'];
                    $withIn25StockValue += $row['WithIn25StockValue'];
                    $withIn20StockValue += $row['WithIn20StockValue'];
                    $withIn15StockValue += $row['WithIn15StockValue'];
                    $withIn10StockValue += $row['WithIn10StockValue'];
                    $withIn5StockValue += $row['WithIn5StockValue'];
                }

                $finalData['0-5'] = $withIn5StockValue;
                $finalData['5-10'] = $withIn10StockValue;
                $finalData['10-15'] = $withIn15StockValue;
                $finalData['15-20'] = $withIn20StockValue;
                $finalData['20-25'] = $withIn25StockValue;
                $finalData['25-30'] = $withIn30StockValue;
                break;

            case 'thisWeek':
            case 'thisDay':
                $results = $this->CommonTable('Inventory\Model\ItemInTable')->getStockAgeDetailsForDashbaordByWeek($date);
                foreach ($results as $result) {
                    if (!array_key_exists($result['productID'], $dataList)) {
                        $dataList[$result['productID']] = [
                            'productName' => $result['productName'],
                            'productCode' => $result['productCode'],
                            'WithIn8StockValue' => $result['WithIn8StockValue'],
                            'WithIn6StockValue' => $result['WithIn6StockValue'],
                            'WithIn4StockValue' => $result['WithIn4StockValue'],
                            'WithIn2StockValue' => $result['WithIn2StockValue'],
                        ];
                    } else {
                        $dataList[$result['productID']]['WithIn8StockValue'] += $result['WithIn8StockValue'];
                        $dataList[$result['productID']]['WithIn6StockValue'] += $result['WithIn6StockValue'];
                        $dataList[$result['productID']]['WithIn4StockValue'] += $result['WithIn4StockValue'];
                        $dataList[$result['productID']]['WithIn2StockValue'] += $result['WithIn2StockValue'];
                    }
                }

                $withIn8StockValue = 0;                        
                $withIn6StockValue = 0;
                $withIn4StockValue = 0;
                $withIn2StockValue = 0;

                foreach ($dataList as $row) {
                    $withIn8StockValue += $row['WithIn8StockValue'];
                    $withIn6StockValue += $row['WithIn6StockValue'];
                    $withIn4StockValue += $row['WithIn4StockValue'];
                    $withIn2StockValue += $row['WithIn2StockValue'];
                }

                if ($period == "thisWeek") {
                    $finalData['0-2'] = $withIn2StockValue;
                    $finalData['2-4'] = $withIn4StockValue;
                    $finalData['4-6'] = $withIn6StockValue;
                    $finalData['6-8'] = $withIn8StockValue;
                } else {
                    $finalData['0-2'] = $withIn2StockValue;
                    $finalData['2-4'] = $withIn4StockValue;
                }
                    
                break;
            
            default:
                // code...
                break;
        }


        

        return $finalData;
    }


    public function getItemWiseMinimumInventoryLevelDetails($proIDs = null, $isAllProducts = false ,$locationIDs)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        if (isset($proIDs) || $isAllProducts) {
            $allLocations = $this->allLocations;
            foreach ($allLocations as $key => $value) {
                $locIDs[] = $key;
            }
            $locationWiseData = array_fill_keys($locIDs, '');

            $resPro = $this->CommonTable('Inventory\Model\LocationProductTable')->getItemWiseMinimumInvenLevelDetailsForDahbaord($proIDs, $locationIDs, $isAllProducts);
            foreach ($resPro as $row) {
                if (!is_null($row['qty'])) {
                    if (floatval($row['qty']) <= floatval($row['miL'])) {
                        $locationWiseData[$row['locID']]['locNameCD'] = $row['locName'] . '-' . $row['locationCode'];
                        $locationWiseData[$row['locID']]['pD'][$row['pID']]['uomAbbr'] = '';
                        $locationWiseData[$row['locID']]['pD'][$row['pID']]['pCD'] = $row['pCD'];
                        $locationWiseData[$row['locID']]['pD'][$row['pID']]['pName'] = $row['pName'];

                        //set quantity and uomAbbr according to display UOM
                        $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['pID']);
                        $quantity = $this->getProductQuantityViaDisplayUom($row['qty'], $productUom);
                        $quantity['quantity'] = ($quantity['quantity'] == 0) ? 0 : $quantity['quantity'];

                        $miLQty = $this->getProductQuantityViaDisplayUom($row['miL'], $productUom);
                        $miLQty['quantity'] = ($miLQty['quantity'] == 0) ? 0 : $miLQty['quantity'];

                        $locationWiseData[$row['locID']]['pD'][$row['pID']]['qty'] = $quantity['quantity'];
                        $locationWiseData[$row['locID']]['pD'][$row['pID']]['miL'] = $miLQty['quantity'];
                        $locationWiseData[$row['locID']]['pD'][$row['pID']] ['uomAbbr'] = $quantity['uomAbbr'];
                    }
                }
            }

            $pLists = array_filter($locationWiseData);
            return $pLists;
        }
    }

    public function getReorderItemDataAction() { 
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            
            $reOrderReachedItems = $this->CommonTable('Inventory\Model\LocationProductTable')->getOverdueReorderLevelItem($locationID);

            header('Content-Type: application/json');
            $widgetData = array(
                'reOrderReachedItems' => count($reOrderReachedItems),
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }

    }




    public function getMinInventoryItemDataAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            
            $milReachedItems = $this->getItemWiseMinimumInventoryLevelDetails("", "1", [$locationID]);

            header('Content-Type: application/json');
            $widgetData = array(
                'totalMinInventoryItems' => count($milReachedItems[$locationID]['pD']),
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }


    }


    public function updateFooterChartDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

           

            $bestMovingItems = $this->CommonTable('Inventory\Model\ItemOutTable')->getBestMovingItemByDateRange($fromDate, $toDate,$locationID);


            $movingCount = array_column($bestMovingItems, 'movingCount');

            array_multisort($movingCount, SORT_DESC, $bestMovingItems);

            header('Content-Type: application/json');
            $widgetData = array(
                'bestMovingItems' => $bestMovingItems,
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }

        
    }

    public function updateChartDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

           
            $stockAgedDetails = $this->getStockAgingDetails($toDate, $period, $locationID);
            

            header('Content-Type: application/json');
            $widgetData = array(
                'stockAgedDetails' => $stockAgedDetails,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }

        
    }

    public function getStockValuesDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $locationID = $this->user_session->userActiveLocation['locationID'];
            
            $locIds = $this->getActiveAllLocationsIds();
            $proIds = [];
            $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDashboard([$locationID], $productSearchKey = NULL, $isSearch = FALSE);

            foreach ($productData as $value) {
                $proIds[$value['productID']] = $value['productID'];
            }
            $globalStockValueData = $this->getService('StockInHandReportService')->_getGlobalWiseStockValueData($proIds, [$locationID], "1", "");

            $totalStockValue = 0;
            $totalStockSellingValue = 0;
            foreach ($globalStockValueData as $key => $data) {
                $availableQty = 0;
                $totalCostValue = 0;
                $uPrice = 0;
                $availableQtyAmount = 0;
                $expectSellValue = 0;
                foreach ($data['proD'] as $key => $value) {
                    $aQty = $value['availableQty'] ? $value['availableQty'] : 0.00;
                    $expectSellValue += ($aQty * floatval($value['productDefaultSellingPrice']));
                    $availableQty += $aQty;
                    $costValue = $value['totalCostValue'] ? $value['totalCostValue'] : 0.00;
                    $discount = isset($value['discountValue']) ? $value['discountValue'] : 0;
                    $tax = isset($value['taxValue']) ? $value['taxValue'] : 0;
                    $totalCostValue += (($costValue + $tax) - $discount);
                }
                if ($availableQty != 0 && $availableQty != NULL) {
                    $uPrice = ($totalCostValue) / $availableQty;
                }
                $availableQtyAmount = $uPrice * $availableQty;
                $totalStockValue += $availableQtyAmount;
                $totalStockSellingValue += $expectSellValue;
            }

            header('Content-Type: application/json');
            $widgetData = array(
                'totalStockValue' => round($totalStockValue,2),
                'totalStockSellingValue' => round($totalStockSellingValue,2),
                'totalActiveItems' => count($proIds),
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }
}