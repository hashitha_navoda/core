<?php

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;

class PurchaseDashboardController extends CoreController
{
	protected $userID;
    protected $cdnUrl;
    protected $user_session;
    protected $paginator;
    protected $company;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

	public function getWidgetDataAction()
	{
		$request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                	$lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                 	$lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                 	$lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliers();

            $lastPeriodPiData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getTotalPurchaseByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getTotalPurchaseByDateRangeAndLocationID($fromDate,$toDate,$locationID);


            $lastPeriodDNData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteTotalByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $dnData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteTotalByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $totalPurchase = floatval($piData['invTotal']) - floatval($dnData['total']);
            $totalPurchaseLastYear = floatval($lastPeriodPiData['invTotal']) - floatval($lastPeriodDNData['total']);

            $totalPurchaseProfitLoss = (($totalPurchase - $totalPurchaseLastYear) / $totalPurchase) * 100;

            $prData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getTotalPurchaseReturnByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $lastPeriodPrData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getTotalPurchaseReturnByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $totalPurchaseReturnProfitLoss = ((floatval($prData['prTotal']) - floatval($lastPeriodPrData['prTotal'])) / floatval($prData['prTotal'])) * 100;

            $totalPayments = $this->CommonTable('SupplierPaymentsTable')->getTotalPaymentByGivenDateRangeAndLocationID($fromDate,$toDate,$locationID);
            $totalPaymentsLastPeriod = $this->CommonTable('SupplierPaymentsTable')->getTotalPaymentByGivenDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);
            $totalPaymentProfitLoss = ((floatval($totalPayments['paymentTotal']) - floatval($totalPaymentsLastPeriod['paymentTotal'])) / floatval($totalPayments['paymentTotal'])) * 100;


            $mostPurchaseBySupplierData = $this->getMostPurchaseSupplierData($fromDate,$toDate,$locationID);
            
            $mostPurchaseReturnBySupplierData = $this->getMostPurchaseReturnSupplierData($fromDate,$toDate,$locationID);

            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPoStatus($fromDate,$toDate,$locationID);

            $poStatusData = [];
            foreach ($poData as $value) {
                if ($value['statusName'] != "Replace" && $value['statusName'] != "Cancel") {
                    $poStatusData[] = $value;
                }
            }

            $payData = $this->CommonTable('SupplierPaymentsTable')->getPaymentsWithMethodsByGivenDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $paymentMethodData = [];
            foreach ($payData as $value) {
                if (!is_null($value['paymentMethodName'])) {
                    $paymentMethodData[] = $value;
                }
            }

            $piStatusData = $this->getPurchaseInvoiceStatusData($fromDate,$toDate,$locationID);

            header('Content-Type: application/json');
            $widgetData = array(
                'activeSuppliers' => count($supplierData),
                'totalPurchase' => $totalPurchase,
                'poStatusData' => $poStatusData,
                'paymentMethodData' => $paymentMethodData,
                'piStatusData' => $piStatusData,
                'mostPurchaseBySupplierData' => $mostPurchaseBySupplierData,
                'mostPurchaseReturnBySupplierData' => $mostPurchaseReturnBySupplierData,
                'totalPurchaseReturn' => floatval($prData['prTotal']),
                'totalPayments' => floatval($totalPayments['paymentTotal']),
                'totalPaymentsLastPeriod' => floatval($totalPaymentsLastPeriod['paymentTotal']),
                'totalPurchaseReturnLastPeriod' => floatval($lastPeriodPrData['prTotal']),
                'totalPurchaseLastYear' => $totalPurchaseLastYear,
                'totalPurchaseProfitLoss' => round($totalPurchaseProfitLoss,2),
                'totalPurchaseReturnProfitLoss' => round($totalPurchaseReturnProfitLoss,2),
                'totalPaymentProfitLoss' => round($totalPaymentProfitLoss,2),
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
	}

    public function getPurchaseInvoiceStatusData($fromDate,$toDate,$locationID)
    {
        //get open invoices
        $openInvoiceData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getTotalPurchaseByDateRangeAndLocationIDAndStatus($fromDate,$toDate,$locationID, 3);

        $openInvoiceIDs = [];
        $openInvoiceTotal = 0;
        foreach ($openInvoiceData as $value) {
            $openInvoiceIDs[] = $value['purchaseInvoiceID'];
            $openInvoiceTotal += (floatval($value['invAmount']) - floatval($value['invPayedAmountAmount']));
        }

        if (!empty($openInvoiceIDs)) {
            $openInvoiceCreditnoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromDate,$toDate,$locationID, $openInvoiceIDs);
        }

        $openInvoiceTotal = $openInvoiceTotal - floatval($openInvoiceCreditnoteData['total']);

        //overdueInvoice
        //get open invoices
        $overdueInvoiceData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getTotalPurchaseByDateRangeAndLocationIDAndStatus($fromDate,$toDate,$locationID, 6);

        $overdueInvoiceIDs = [];
        $overdueInvoiceTotal = 0;
        foreach ($overdueInvoiceData as $value) {
            $overdueInvoiceIDs[] = $value['purchaseInvoiceID'];
            $overdueInvoiceTotal += (floatval($value['invAmount']) - floatval($value['invPayedAmountAmount']));
        }

        if (!empty($overdueInvoiceIDs)) {
            $overdueInvoiceCreditnoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromDate,$toDate,$locationID, $overdueInvoiceIDs);
        }

        $overdueInvoiceTotal = $overdueInvoiceTotal - floatval($overdueInvoiceCreditnoteData['total']);                    

        // closed invoice total
        $closedInvoiceData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getTotalPurchaseByDateRangeAndLocationIDAndStatus($fromDate,$toDate,$locationID, 4);

        $closedInvoiceIDs = [];
        $closedInvoiceTotal = 0;
        foreach ($closedInvoiceData as $value) {
            $closedInvoiceIDs[] = $value['purchaseInvoiceID'];
            $closedInvoiceTotal += floatval($value['invAmount']);
        }
        if (!empty($closedInvoiceIDs)) {
            $closedInvoiceCreditnoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromDate,$toDate,$locationID, $closedInvoiceIDs);
        }

        $closedInvoiceTotal = $closedInvoiceTotal - floatval($closedInvoiceCreditnoteData['total']); 

        return [
            'openInvoiceTotal' => round($openInvoiceTotal,2),
            'overdueInvoiceTotal' => round($overdueInvoiceTotal,2),
            'closedInvoiceTotal' => round($closedInvoiceTotal,2)
        ];
    }


    public function getMostPurchaseSupplierData($fromDate,$toDate,$locationID)
    {
        $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getTotalPurchaseByDateRangeAndLocationID($fromDate,$toDate,$locationID, true);
        $supplierPurchases = [];
        foreach ($piData as $value) {
            $supplierPurchases[$value['purchaseInvoiceSupplierID']]['purchase'] = $value['invTotal'];
            $supplierPurchases[$value['purchaseInvoiceSupplierID']]['supplier'] = $value['supplierName'];
        }

        $prData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getTotalPurchaseReturnByDateRangeAndLocationID($fromDate,$toDate,$locationID, true);

        $supplierReturns = [];
        foreach ($prData as $value) {
            $supplierReturns[$value['supplierID']]['return'] = $value['prTotal'];   
            $supplierReturns[$value['supplierID']]['supplier'] = $value['supplierName'];   
        }

        $finalData = [];
        foreach ($supplierPurchases as $key => $value) {
            $finalData[$key] = $value;
            if (!is_null($supplierReturns[$key])) {
                $finalData[$key]['return'] = $supplierReturns[$key]['return'];
            } else {
                $finalData[$key]['return'] = 0;
            }
        }

        if (empty($supplierPurchases)) {
            foreach ($supplierReturns as $key => $value) {
                $finalData[$key] = $value;
                $finalData[$key]['purchase'] = 0;
            }
        }

        $purchase = array_column($finalData, 'purchase');

        array_multisort($purchase, SORT_DESC, $finalData);

        return $finalData;
    }

    public function getMostPurchaseReturnSupplierData($fromDate,$toDate,$locationID)
    {
        $prData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getTotalPurchaseReturnByDateRangeAndLocationID($fromDate,$toDate,$locationID, true);

        $supplierReturns = [];
        foreach ($prData as $value) {
            $supplierReturns[$value['supplierID']]['return'] = $value['prTotal'];   
            $supplierReturns[$value['supplierID']]['supplier'] = $value['supplierName'];   
        }

        $return = array_column($supplierReturns, 'return');

        array_multisort($return, SORT_DESC, $supplierReturns);

        return $supplierReturns;
    }


    public function getTotalPurchasesDataAction () {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliersCountForDashboard();

            $lastPeriodPiData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getTotalPurchaseByDateRangeAndLocationIDForDashboard($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getTotalPurchaseByDateRangeAndLocationIDForDashboard($fromDate,$toDate,$locationID);


            $lastPeriodDNData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteTotalByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $dnData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteTotalByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $totalPurchase = floatval($piData['invTotal']) - floatval($dnData['total']);
            $totalPurchaseLastYear = floatval($lastPeriodPiData['invTotal']) - floatval($lastPeriodDNData['total']);

            $totalPurchaseProfitLoss = (($totalPurchase - $totalPurchaseLastYear) / $totalPurchase) * 100;

            

            header('Content-Type: application/json');
            $widgetData = array(
                'activeSuppliers' => count($supplierData),
                'totalPurchase' => $totalPurchase,
                'totalPurchaseLastYear' => $totalPurchaseLastYear,
                'totalPurchaseProfitLoss' => round($totalPurchaseProfitLoss,2),
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function getTotalReturnsDataAction () {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $prData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getTotalPurchaseReturnByDateRangeAndLocationIDDashboard($fromDate,$toDate,$locationID);

            $lastPeriodPrData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getTotalPurchaseReturnByDateRangeAndLocationIDDashboard($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $totalPurchaseReturnProfitLoss = ((floatval($prData['prTotal']) - floatval($lastPeriodPrData['prTotal'])) / floatval($prData['prTotal'])) * 100;

            header('Content-Type: application/json');
            $widgetData = array(
                'totalPurchaseReturn' => floatval($prData['prTotal']),
                'totalPurchaseReturnLastPeriod' => floatval($lastPeriodPrData['prTotal']),
                'totalPurchaseReturnProfitLoss' => round($totalPurchaseReturnProfitLoss,2),
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function getPaymentDataAction () {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }


            $totalPayments = $this->CommonTable('SupplierPaymentsTable')->getTotalPaymentByGivenDateRangeAndLocationID($fromDate,$toDate,$locationID);
            $totalPaymentsLastPeriod = $this->CommonTable('SupplierPaymentsTable')->getTotalPaymentByGivenDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);
            $totalPaymentProfitLoss = ((floatval($totalPayments['paymentTotal']) - floatval($totalPaymentsLastPeriod['paymentTotal'])) / floatval($totalPayments['paymentTotal'])) * 100;


            header('Content-Type: application/json');
            $widgetData = array(
                'totalPayments' => floatval($totalPayments['paymentTotal']),
                'totalPaymentsLastPeriod' => floatval($totalPaymentsLastPeriod['paymentTotal']),
                'totalPaymentProfitLoss' => round($totalPaymentProfitLoss,2),
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function updateChartDataAction () {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $mostPurchaseBySupplierData = $this->getMostPurchaseSupplierData($fromDate,$toDate,$locationID);
            

            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPoStatus($fromDate,$toDate,$locationID);

            $poStatusData = [];
            foreach ($poData as $value) {
                if ($value['statusName'] != "Replace" && $value['statusName'] != "Cancel") {
                    $poStatusData[] = $value;
                }
            }

            header('Content-Type: application/json');
            $widgetData = array(
                'poStatusData' => $poStatusData,
                'mostPurchaseBySupplierData' => $mostPurchaseBySupplierData,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }


    }

    public function updateFooterChartDataAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $mostPurchaseReturnBySupplierData = $this->getMostPurchaseReturnSupplierData($fromDate,$toDate,$locationID);

            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPoStatus($fromDate,$toDate,$locationID);

            $payData = $this->CommonTable('SupplierPaymentsTable')->getPaymentsWithMethodsByGivenDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $paymentMethodData = [];
            foreach ($payData as $value) {
                if (!is_null($value['paymentMethodName'])) {
                    $paymentMethodData[] = $value;
                }
            }

            $piStatusData = $this->getPurchaseInvoiceStatusData($fromDate,$toDate,$locationID);

            header('Content-Type: application/json');
            $widgetData = array(
                'paymentMethodData' => $paymentMethodData,
                'piStatusData' => $piStatusData,
                'mostPurchaseReturnBySupplierData' => $mostPurchaseReturnBySupplierData,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }
}