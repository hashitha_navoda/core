<?php

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Inventory\Form\SupplierForm;
use Inventory\Model\ProductBatch;
use Inventory\Model\ProductSerial;
use Inventory\Model\PurchaseInvoiceProduct;
use Inventory\Model\PurchaseInvoice;
use Inventory\Model\PurchaseInvoiceProductTax;
use Inventory\Model\ItemIn;
use Core\Model\DocumentReference;
use Accounting\Model\JournalEntryAccounts;
use Inventory\Model\ItemOut;
use Inventory\Model\CompoundCostPurchaseInvoice;
use Accounting\Model\JournalEntry;

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */
class PurchaseInvoiceController extends CoreController
{
    protected $useAccounting;

    private $productIds = [];

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this functoion related to pi reation
     */
    public function savePiAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invoiceData = $request->getPost();
            $this->beginTransaction();
            $result = $this->storePi($invoiceData);
            if ($result['status']) {
                $this->commit();
            } else {
                $this->rollback();
            }
            $this->status = $result['status'];
            $this->msg = $result['msg'];
            $this->data = $result['data'];
            return $this->JSONRespond();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DEBIT_PRODUCT_SAVE');
            return $this->JSONRespond();
        }
    }

    public function storePi($invoiceData, $supOpeningBalance = false)
    {
    
        $post = $invoiceData;
        $multipleGrnFlag = false;
        $copyFromGrn = FALSE;
        $copyFromPo = FALSE;
        $copyingPoId = NULL;
        $copyingGrnId = NULL;
        $copiedDocumentType = null;
        $copiedDocumentID = null;
        if ($post['startByGrnID'] != '') {
            $copyFromGrn = TRUE;
            $copyingGrnId = $post['startByGrnID'];
        }
        if ($post['startByPoID'] != '') {
            $copyFromPo = TRUE;
            $copyingPoId = $post['startByPoID'];
        }
        if ($post['multipleGrnCopyFlag'] == "true") {
            $multipleGrnFlag = true;
            $copyFromGrn = true;
        }
        $purchaseInvoiceCode = $post['piC'];
        $locationReferenceID = $post['lRID'];
        $dimensionData = $post['dimensionData'];
        $documentReference = $post['documentReference'];
        $locationID = $post['rL'];
        $journalEntryAccountsData = $post['journalEntryData'];
        $ignoreBudgetLimit = $post['ignoreBudgetLimit'];

        if ($copyingGrnId != NULL && $multipleGrnFlag == false) {
            $grnStatus = $this->CommonTable('Inventory\Model\GrnTable')->checkGrnnByCode($grnCode,$copyingGrnId)->current();

            if (!$grnStatus) {
                $this->setLogMessage("Error occured when create purchase return ".$prCode.'.');
                $this->rollback();
                return ['status' => false, 'msg' => $this->getMessage('ERR_GRN_CODE_CLOSE'), 'data' => null];
            }
        }

        $this->setLogMessage("Error occured when create ".$this->companyCurrencySymbol . $post['fT'] . ' amount purchase invoice - ' . $purchaseInvoiceCode);
        // check if a purchase invoice from the same purchase invoice Code exists if exist add next number to purchase invoice code
        while ($purchaseInvoiceCode) {
            if ($this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->checkPurchaseInvoiceByCode($purchaseInvoiceCode)->current()) {
                if ($locationReferenceID) {
                    $newPurchaseInvoiceCode = $this->getReferenceNumber($locationReferenceID);
                    if ($newPurchaseInvoiceCode == $purchaseInvoiceCode) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $purchaseInvoiceCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $purchaseInvoiceCode = $newPurchaseInvoiceCode;
                    }
                } else {
                    $this->rollBack();
                    return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CODE_EXIST'), 'data' => null];
                }
            } else {
                break;
            }
        }
        $entityID = $this->createEntity();

        $purchaseInvoiceWiseTotalDiscount = 0;
        if ($post['purchaseInvoiceWiseTotalDiscountType'] == 'presentage') {
            $totalInvoiceDiscount = ($post['fT'] * $post['purchaseInvoiceWiseTotalDiscountRate'] / (100 - $post['purchaseInvoiceWiseTotalDiscountRate']));
        } else {
            $totalInvoiceDiscount = $post['purchaseInvoiceWiseTotalDiscountRate'] ;
        }

        $piData = array(
            'purchaseInvoiceCode' => $purchaseInvoiceCode,
            'purchaseInvoiceSupplierID' => $post['sID'],
            'purchaseInvoiceSupplierReference' => $post['sR'],
            'purchaseInvoiceRetrieveLocation' => $locationID,
            'purchaseInvoicePaymentDueDate' => $this->convertDateToStandardFormat($post['dD']),
            'purchaseInvoiceIssueDate' => $this->convertDateToStandardFormat($post['iD']),
            'purchaseInvoicePoID' => $copyingPoId,
            'purchaseInvoiceGrnID' => $copyingGrnId,
            'purchaseInvoiceComment' => $post['cm'],
            'purchaseInvoiceDeliveryCharge' => $post['dC'],
            'purchaseInvoiceShowTax' => $post['sT'],
            'purchaseInvoiceTotal' => $post['fT'],
            'paymentTermID' => $post['pT'],
            'entityID' => $entityID,
            'purchaseInvoiceWiseTotalDiscountRate' => $post['purchaseInvoiceWiseTotalDiscountRate'],
            'purchaseInvoiceWiseTotalDiscountType' => $post['purchaseInvoiceWiseTotalDiscountType'],
            'purchaseInvoiceWiseTotalDiscount' => $totalInvoiceDiscount,
        );

        $piM = new PurchaseInvoice();
        $piM->exchangeArray($piData);
        $piID = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->savePi($piM);
        $issueDate = $this->convertDateToStandardFormat($post['iD']);
        if(!$piID){
            $this->rollback();
            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_INV_CREATE'), 'data' => null];
        }

        $supplierCurrentOutstndBalance = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($post['sID'])->supplierOutstandingBalance;
        $supplierNewOutstandingBalance = floatval($supplierCurrentOutstndBalance) + floatval($post['fT']);
        $supplierData = array(
            'supplierOutstandingBalance' => $supplierNewOutstandingBalance
        );
        if (!$supOpeningBalance) {
            $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplier($supplierData, $post['sID']);
        }
        $this->updateReferenceNumber($locationReferenceID);

        //save document Reference Data

        if($documentReference != ''){
            foreach ($documentReference as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $data = array(
                        'sourceDocumentTypeId' => 12,
                        'sourceDocumentId' => $piID,
                        'referenceDocumentTypeId' => $key,
                        'referenceDocumentId' => $value,
                        );
                    $documentReference = new DocumentReference();
                    $documentReference->exchangeArray($data);
                    $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                }
            }
        }

//if products were copied from GRN. No need to update Batch and series products
        if ($copyFromGrn) {
            $allProductTotal = 0;
            foreach ($post['pr'] as $product) {
                $copiedDocumentType = null;
                $copiedDocumentID = null;
                $locationPID = $product['locationPID'];
                //get location product data for update average costing.
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

                $product['stockUpdate'] =  filter_var($product['stockUpdate'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

                $grnProductQty = $this->CommonTable('Inventory\Model\GrnProductTable')
                    ->getgrnProductQtyBylocationIDAndGrnID($product['grnDocId'],$locationPID,$product['pUnitPrice']);

                if ($product['grnFlag'] == "true") {
                    //if all product quantities are returned then, update grnProductTable
                    if($product['pQuantity'] == $grnProductQty[0]['grnProductTotalQty']){
                        $this->CommonTable('Inventory\Model\GrnProductTable')->updateCopiedGrnProductsByGrnProductIdAndLocationProductId($locationPID, $grnProductQty[0]['grnProductID']);
                    }
                }
                $productBaseQty = $product['pQuantity'];
                $productType = $product['productType'];
                $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);
                $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                // $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);

                if ($product['dType'] == 'precentage') {
                    $productTotal = $product['pUnitPrice'] * $product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                } elseif ($product['dType'] == 'value') {
                    $productTotal = $product['pQuantity'] * (($product['pUnitPrice'] - $product['pDiscount']));
                } else {
                    $productTotal = $product['pUnitPrice'] * $product['pQuantity'];
                }



                $allProductTotal += $productTotal;
                //If product is a batch product
                $totalCopiedQty = 0;
                if (array_key_exists('bProducts', $product)) {
                    $batchProductCount = 0;
                    foreach ($product['bProducts'] as $bProduct) {
                        $totalCopiedQty += $bProduct['bQty'];
                        $batchProductCount++;

                        if($product['stockUpdate']){
                            $bPData = array(
                                'locationProductID' => $locationPID,
                                'productBatchCode' => $bProduct['bCode'],
                                'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                                'productBatchWarrantyPeriod' => $bProduct['warnty'],
                                'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                                'productBatchQuantity' => $bProduct['bQty']
                                );

                            $batchProduct = new ProductBatch();
                            $batchProduct->exchangeArray($bPData);
                            $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                            $bProduct['bID']=$insertedBID;
                            if(!$insertedBID){
                                $this->rollBack();
                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                            }
                        }
                        //if product is a batch and serial product
                        if (array_key_exists('sProducts', $product)) {
                            foreach ($product['sProducts'] as $sProduct) {
                                if (isset($sProduct['sBCode'])) {
                                    if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                        if(!$product['stockUpdate']){
                                            $grnPData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($product['grnDocId'], $locationPID, $sProduct['sID'], $bProduct['bID'])->current();
                                            $grnProductID = $grnPData['grnProductID'];
                                            $data = array(
                                                'grnProductCopiedQuantity' => 1,
                                                'copied' => 1,
                                            );
                                            $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $grnProductID);
                                            $copiedDocumentType = 10;
                                            $copiedDocumentID = $grnPData['grnProductID'];


                                        }else{
                                            $sPData = array(
                                                'locationProductID' => $locationPID,
                                                'productBatchID' => $insertedBID,
                                                'productSerialCode' => $sProduct['sCode'],
                                                'productSerialExpireDate' => $this->convertDateToStandardFormat($sProduct['sEdate']),
                                                'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                                'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                                            );
                                            $serialPr = new ProductSerial();
                                            $serialPr->exchangeArray($sPData);
                                            $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                            $sProduct['sID'] =$insertedSID;
                                            if(!$insertedSID){
                                                $this->rollBack();
                                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                            }
                                            //save item in details
                                            $itemInData = array(
                                                'itemInDocumentType' => 'Payment Voucher',
                                                'itemInDocumentID' => $piID,
                                                'itemInLocationProductID' => $locationPID,
                                                'itemInBatchID' => $insertedBID,
                                                'itemInSerialID' => $insertedSID,
                                                'itemInQty' => 1,
                                                'itemInPrice' => $product['pUnitPrice'],
                                                'itemInDiscount' => $discountValue,
                                                'itemInDateAndTime' => $this->getGMTDateTime(),
                                                );
                                            $itemInModel = new ItemIn();
                                            $itemInModel->exchangeArray($itemInData);
                                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                            if(!$itemInID){
                                                $this->rollBack();
                                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                            }
                                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                            $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                            $newPQtyData = array(
                                                'locationProductQuantity' => $newPQty
                                                );
                                            $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);

                                            if(!$status){
                                                $this->rollBack();
                                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                            }
                                        }

                                        $piProductM = new PurchaseInvoiceProduct();
                                        $piProductData = array(
                                            'purchaseInvoiceID' => $piID,
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $bProduct['bID'],
                                            'productSerialID' => $sProduct['sID'],
                                            'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                                            'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                                            'purchaseInvoiceProductQuantity' => 1,
                                            'purchaseInvoiceProductTotalQty' => $productBaseQty,
                                            'purchaseInvoiceProductTotal' => $product['pTotal'],
                                            'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                                            'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                                            'purchaseInvoiceProductSelectedUomId' => $product['pUom'],
                                            'rackID' => ''
                                        );
                                        $piProductM->exchangeArray($piProductData);
                                        $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                                        if(!$insertedPiProductID){
                                            $this->rollBack();
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                        }
                                        if (array_key_exists('pTax', $product)) {
                                            if (array_key_exists('tL', $product['pTax'])) {
                                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                    $piPTaxesData = array(
                                                        'purchaseInvoiceID' => $piID,
                                                        'purchaseInvoiceProductID' => $insertedPiProductID,
                                                        'purchaseInvoiceTaxID' => $taxKey,
                                                        'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                                        'purchaseInvoiceTaxAmount' => $productTax['tA']
                                                    );
                                                    $piPTaxM = new PurchaseInvoiceProductTax();
                                                    $piPTaxM->exchangeArray($piPTaxesData);
                                                    $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {

                            if(!$product['stockUpdate']){
                                $grnPData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($product['grnDocId'], $locationPID, '', $bProduct['bID'])->current();
                                $grnProductID = $grnPData['grnProductID'];
                                $grnQty = ($grnPData['grnProductCopiedQuantity'] == '') ? 0: $grnPData['grnProductCopiedQuantity'];
                                $newQty = $grnQty + $bProduct['bQty'];
                                $copiedFlag = 0;
                                if($newQty == $grnPData['grnProductQuantity']){
                                    $copiedFlag = 1;
                                }
                                $data = array(
                                    'grnProductCopiedQuantity' => $newQty,
                                    'copied' => $copiedFlag,
                                );
                                $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $grnProductID);
                                $copiedDocumentType = 10;
                                $copiedDocumentID = $grnProductID;

                            }else{
                                 //save item in details
                                $itemInData = array(
                                    'itemInDocumentType' => 'Payment Voucher',
                                    'itemInDocumentID' => $piID,
                                    'itemInLocationProductID' => $locationPID,
                                    'itemInBatchID' => $insertedBID,
                                    'itemInSerialID' => NULL,
                                    'itemInQty' => $bProduct['bQty'],
                                    'itemInPrice' => $product['pUnitPrice'],
                                    'itemInDiscount' => $discountValue,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                if(!$itemInID){
                                    $this->rollBack();
                                    return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                }
                                if ($batchProductCount == 1) {
                                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                                    $newPQtyData = array(
                                        'locationProductQuantity' => $newPQty
                                        );
                                    $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                    if(!$status){
                                        $this->rollBack();
                                        return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                    }
                                }
                            }

                            $piProductM = new PurchaseInvoiceProduct();
                            $piProductData = array(
                                'purchaseInvoiceID' => $piID,
                                'locationProductID' => $locationPID,
                                'productBatchID' => $bProduct['bID'],
                                'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                                'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                                'purchaseInvoiceProductQuantity' => $bProduct['bQty'],
                                'purchaseInvoiceProductTotalQty' => $productBaseQty,
                                'purchaseInvoiceProductTotal' => $product['pTotal'],
                                'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                                'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                                'purchaseInvoiceProductSelectedUomId' => $product['pUom'],
                                'rackID' => ''
                            );

                            $piProductM->exchangeArray($piProductData);
                            $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                            if(!$insertedPiProductID){
                                $this->rollBack();
                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                            }

                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $piPTaxesData = array(
                                            'purchaseInvoiceID' => $piID,
                                            'purchaseInvoiceProductID' => $insertedPiProductID,
                                            'purchaseInvoiceTaxID' => $taxKey,
                                            'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                            'purchaseInvoiceTaxAmount' => $productTax['tA']
                                        );
                                        $piPTaxM = new PurchaseInvoiceProductTax();
                                        $piPTaxM->exchangeArray($piPTaxesData);
                                        $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                    }
                                }
                            }
                        }
                    }

                } elseif (array_key_exists('sProducts', $product)) {

                    //If the product is a serial product
                    foreach ($product['sProducts'] as $sProduct) {
                        $totalCopiedQty++;
                        if(!$product['stockUpdate']){
                            $grnPData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($product['grnDocId'], $locationPID, $sProduct['sID'], '')->current();
                            $grnProductID = $grnPData['grnProductID'];
                            $data = array(
                                'grnProductCopiedQuantity' => 1,
                                'copied' => 1,
                                );
                            $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $grnProductID);
                            $copiedDocumentType = 10;
                            $copiedDocumentID = $grnProductID;

                        }else{
                            $sPData = array(
                                'locationProductID' => $locationPID,
                                'productSerialCode' => $sProduct['sCode'],
                                'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                                'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                                );
                            $serialPr = new ProductSerial();
                            $serialPr->exchangeArray($sPData);     
                            $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                            $sProduct['sID'] = $insertedSID;
                            if(!$insertedSID){
                                $this->rollBack();
                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                            }
                         //save item in details
                            $itemInData = array(
                                'itemInDocumentType' => 'Payment Voucher',
                                'itemInDocumentID' => $piID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => NULL,
                                'itemInSerialID' => $insertedSID,
                                'itemInQty' => 1,
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if(!$itemInID){
                                $this->rollBack();
                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                            }

                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                                );
                            $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                            if(!$status){
                                $this->rollBack();
                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                            }
                        }

                        $piProductM = new PurchaseInvoiceProduct();
                        $piProductData = array(
                            'purchaseInvoiceID' => $piID,
                            'locationProductID' => $locationPID,
                            'productSerialID' => $sProduct['sID'],
                            'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                            'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                            'purchaseInvoiceProductQuantity' => 1,
                            'purchaseInvoiceProductTotalQty' => $productBaseQty,
                            'purchaseInvoiceProductTotal' => $product['pTotal'],
                            'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                            'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                            'purchaseInvoiceProductSelectedUomId' => $product['pUom'],
                            'rackID' => ''
                        );

                        $piProductM->exchangeArray($piProductData);
                        $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                        if(!$insertedPiProductID){
                            $this->rollBack();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                        }
                        if (array_key_exists('pTax', $product)) {
                            if (array_key_exists('tL', $product['pTax'])) {
                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                    $piPTaxesData = array(
                                        'purchaseInvoiceID' => $piID,
                                        'purchaseInvoiceProductID' => $insertedPiProductID,
                                        'purchaseInvoiceTaxID' => $taxKey,
                                        'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                        'purchaseInvoiceTaxAmount' => $productTax['tA']
                                    );
                                    $piPTaxM = new PurchaseInvoiceProductTax();
                                    $piPTaxM->exchangeArray($piPTaxesData);
                                    $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                }
                            }
                        }
                    }
                } else {
                    $totalCopiedQty +=$productBaseQty;
                    if(!$product['stockUpdate']){
                        $grnPData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($product['grnDocId'], $locationPID, '', '',$product['productindexID'])->current();
                        $grnProductID = $grnPData['grnProductID'];
                        $grnQty = ($grnPData['grnProductCopiedQuantity'] == '') ? 0: $grnPData['grnProductCopiedQuantity'];
                        $newQty = $grnQty + $productBaseQty;
                        $copiedFlag = 0;
                        if($newQty == $grnPData['grnProductQuantity']){
                            $copiedFlag = 1;
                        }
                        $data = array(
                            'grnProductCopiedQuantity' => $newQty,
                            'copied' => $copiedFlag,
                            );
                        $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $grnProductID);
                        $copiedDocumentType = 10;
                        $copiedDocumentID = $grnProductID;

                    }else{
                        if ($productType != 2) {
                            $itemInData = array(
                                'itemInDocumentType' => 'Payment Voucher',
                                'itemInDocumentID' => $piID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => NULL,
                                'itemInSerialID' => NULL,
                                'itemInQty' => $productBaseQty,
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if(!$itemInID){
                                $this->rollBack();
                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                            }
                        }

                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                        if ($productType == 2) {
                            $productBaseQty = 0;
                        }
                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                            );
                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        if(!$status){
                            $this->rollBack();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                        }
                    }
                    //if the product is non serial and non batch
                    $piProductM = new PurchaseInvoiceProduct();
                    $piProductData = array(
                        'purchaseInvoiceID' => $piID,
                        'locationProductID' => $locationPID,
                        'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                        'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                        'purchaseInvoiceProductQuantity' => $productBaseQty,
                        'purchaseInvoiceProductTotalQty' => $productBaseQty,
                        'purchaseInvoiceProductTotal' => $product['pTotal'],
                        'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                        'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                        'purchaseInvoiceProductSelectedUomId' => $product['pUom']

                    );

                    $piProductM->exchangeArray($piProductData);
                    $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                    if(!$insertedPiProductID){
                        $this->rollBack();
                        return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                    }

                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $piPTaxesData = array(
                                    'purchaseInvoiceID' => $piID,
                                    'purchaseInvoiceProductID' => $insertedPiProductID,
                                    'purchaseInvoiceTaxID' => $taxKey,
                                    'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                    'purchaseInvoiceTaxAmount' => $productTax['tA']
                                );
                                $piPTaxM = new PurchaseInvoiceProductTax();
                                $piPTaxM->exchangeArray($piPTaxesData);
                                $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                            }
                        }
                    }
                }
                if(!$product['stockUpdate']){
                    $pdata = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($product['grnDocId'], $locationPID, '', '','',$product['pUnitPrice'])->current();

                    $pdata['grnProductTotalCopiedQuantity'] = ($pdata['grnProductTotalCopiedQuantity'] == '')? 0:$pdata['grnProductTotalCopiedQuantity'];
                    $newTotalQty = $pdata['grnProductTotalCopiedQuantity'] + $totalCopiedQty;
                    $copiedData = array(
                        'grnProductTotalCopiedQuantity' => $newTotalQty
                    );
                    $status = $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByLocationProductIDAndGrnID($copiedData, "" , $product['grnDocId'],$pdata['grnProductID']);
                    if(!$status){
                        $this->rollBack();
                        return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                    }
                }else{
                    $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
                    $locationPrTotalQty = 0;
                    $locationPrTotalPrice = 0;
                    $newItemInIds = array();
                    if(count($itemInData) > 0){
                        foreach ($itemInData as $key => $value) {
                            if($value->itemInID > $locationProductLastItemInID){
                                $newItemInIds[] = $value->itemInID;
                                $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                if($remainQty > 0){
                                    $itemInPrice = $remainQty*$value->itemInPrice;
                                    $itemInDiscount = $remainQty*$value->itemInDiscount;
                                    $locationPrTotalQty += $remainQty;
                                    $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                                }
                                $locationProductLastItemInID = $value->itemInID;
                            }
                        }
                    }
                    $locationPrTotalQty += $locationProductQuantity;
                    $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                    if($locationPrTotalQty > 0){
                        $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                        $lpdata = array(
                            'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                            'locationProductLastItemInID' => $locationProductLastItemInID,
                        );

                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                        foreach ($newItemInIds as $inID) {
                            $intemInData = array(
                                'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                );
                            $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                        }
                    }
                }
                
                $this->checkAndUpdateGrnStatus($product['grnDocId']);
            }

            $jeResult = $this->saveJournalEntryDataForPI($piID, $purchaseInvoiceCode, $post['sID'], $post['pr'], $post['dC'], $allProductTotal, $issueDate, $post['startByGrnID'], true, $journalEntryAccountsData, $flag = 1, $dimensionData, $ignoreBudgetLimit);

            if($jeResult['status']){
                $this->setLogMessage($this->companyCurrencySymbol . $post['fT'] . ' amount purchase invoice successfully created - ' . $purchaseInvoiceCode);
                $this->flashMessenger()->addMessage($jeResult['msg']);
            }else{
                $this->rollback();
            }

            return ['status' => $jeResult['status'], 'msg' => $jeResult['msg'], 'data' => $jeResult['data']];

        } else {
            $allProductTotal = 0;
            foreach ($post['pr'] as $product) {
                $copiedDocumentType = null;
                $copiedDocumentID = null;
                $locationPID = $product['locationPID'];
                //get location product data for update average costing.
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

                $product['stockUpdate'] =  filter_var($product['stockUpdate'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

                if(!$product['stockUpdate']){
                    $copiedDocumentType = 9;
                    $poProductDetails = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getPoProduct($post['startByPoID'], $locationPID);
                    $copiedDocumentID = $poProductDetails['purchaseOrderProductID'];

                }

                if ($copyFromPo == TRUE) {
                    $this->updatePoProductCopiedQty($locationPID, $post['startByPoID'], $product['pQuantity']);
                }
             
                $productBaseQty = $product['pQuantity'];
                $productType = $product['productType'];
                $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);
                $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                // $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);

                if ($product['dType'] == 'precentage') {
                    $productTotal = $product['pUnitPrice'] * $product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                } elseif ($product['dType'] == 'value') {
                    $productTotal = $product['pQuantity'] * (($product['pUnitPrice'] - $product['pDiscount']));
                } else {
                    $productTotal = $product['pUnitPrice'] * $product['pQuantity'];
                }

                $allProductTotal+=$productTotal;

                //If product is a batch product
                if (array_key_exists('bProducts', $product)) {
                    $batchProductCount = 0;
                    foreach ($product['bProducts'] as $bProduct) {

                        $batchProductCount++;
                        $bPData = array(
                            'locationProductID' => $locationPID,
                            'productBatchCode' => $bProduct['bCode'],
                            'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                            'productBatchWarrantyPeriod' => $bProduct['warnty'],
                            'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                            'productBatchQuantity' => $bProduct['bQty'] * $productConversionRate
                        );

                        $batchProduct = new ProductBatch();
                        $batchProduct->exchangeArray($bPData);
                        $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                        if(!$insertedBID){
                            $this->rollBack();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                        }
                        //if product is a batch and serial product
                        if (array_key_exists('sProducts', $product)) {
                            foreach ($product['sProducts'] as $sProduct) {
                                if (isset($sProduct['sBCode'])) {

                                    if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                        $sPData = array(
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialCode' => $sProduct['sCode'],
                                            'productSerialExpireDate' => $this->convertDateToStandardFormat($sProduct['sEdate']),
                                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                            'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                                        );
                                        $serialPr = new ProductSerial();
                                        $serialPr->exchangeArray($sPData);
                                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                        if(!$insertedSID){
                                            $this->rollBack();
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                        }
                                        $piProductM = new PurchaseInvoiceProduct();
                                        $piProductData = array(
                                            'purchaseInvoiceID' => $piID,
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialID' => $insertedSID,
                                            'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                                            'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                                            'purchaseInvoiceProductQuantity' => $bProduct['bQty'],
                                            'purchaseInvoiceProductTotalQty' => $productBaseQty,
                                            'purchaseInvoiceProductTotal' => $product['pTotal'],
                                            'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                                            'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                                            'rackID' => '',
                                            'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                                        );
                                        $piProductM->exchangeArray($piProductData);
                                        $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                                        if(!$insertedPiProductID){
                                            $this->rollBack();
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                        }
                                        //save item in details
                                        $itemInData = array(
                                            'itemInDocumentType' => 'Payment Voucher',
                                            'itemInDocumentID' => $piID,
                                            'itemInLocationProductID' => $locationPID,
                                            'itemInBatchID' => $insertedBID,
                                            'itemInSerialID' => $insertedSID,
                                            'itemInQty' => 1,
                                            'itemInPrice' => $product['pUnitPrice'],
                                            'itemInDiscount' => $discountValue,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                        if(!$itemInID){
                                            $this->rollBack();
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                        }
                                        if (array_key_exists('pTax', $product)) {
                                            if (array_key_exists('tL', $product['pTax'])) {
                                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                    $piPTaxesData = array(
                                                        'purchaseInvoiceID' => $piID,
                                                        'purchaseInvoiceProductID' => $insertedPiProductID,
                                                        'purchaseInvoiceTaxID' => $taxKey,
                                                        'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                                        'purchaseInvoiceTaxAmount' => $productTax['tA']
                                                    );
                                                    $piPTaxM = new PurchaseInvoiceProductTax();
                                                    $piPTaxM->exchangeArray($piPTaxesData);
                                                    $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                                }
                                            }
                                        }
                                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                        $newPQtyData = array(
                                            'locationProductQuantity' => $newPQty
                                        );
                                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                        if(!$status){
                                            $this->rollBack();
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                        }
                                    }
                                }
                            }
                        } else {

                            $piProductM = new PurchaseInvoiceProduct();
                            $piProductData = array(
                                'purchaseInvoiceID' => $piID,
                                'locationProductID' => $locationPID,
                                'productBatchID' => $insertedBID,
                                'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                                'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                                'purchaseInvoiceProductQuantity' => $bProduct['bQty'] * $productConversionRate,
                                'purchaseInvoiceProductTotalQty' => $productBaseQty,
                                'purchaseInvoiceProductTotal' => $product['pTotal'],
                                'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                                'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                                'rackID' => '',
                                'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                            );

                            $piProductM->exchangeArray($piProductData);
                            $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                            if(!$insertedPiProductID){
                                $this->rollBack();
                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                            }
                            //save item in details
                            $itemInData = array(
                                'itemInDocumentType' => 'Payment Voucher',
                                'itemInDocumentID' => $piID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => $insertedBID,
                                'itemInSerialID' => NULL,
                                'itemInQty' => $bProduct['bQty'] * $productConversionRate,
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if(!$itemInID){
                                $this->rollBack();
                                return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                            }
                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $piPTaxesData = array(
                                            'purchaseInvoiceID' => $piID,
                                            'purchaseInvoiceProductID' => $insertedPiProductID,
                                            'purchaseInvoiceTaxID' => $taxKey,
                                            'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                            'purchaseInvoiceTaxAmount' => $productTax['tA']
                                        );
                                        $piPTaxM = new PurchaseInvoiceProductTax();
                                        $piPTaxM->exchangeArray($piPTaxesData);
                                        $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                    }
                                }
                            }
                            if ($batchProductCount == 1) {
                                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                                $newPQtyData = array(
                                    'locationProductQuantity' => $newPQty
                                );
                                $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                if(!$status){
                                    $this->rollBack();
                                    return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                                }
                            }
                        }
                    }
                } elseif (array_key_exists('sProducts', $product)) {
                    //If the product is a serial product
                    foreach ($product['sProducts'] as $sProduct) {
                        $sPData = array('locationProductID' => $locationPID,
                            'productSerialCode' => $sProduct['sCode'],
                            'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                            'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                        );
                        $serialPr = new ProductSerial();
                        $serialPr->exchangeArray($sPData);
                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                        if(!$insertedSID){
                            $this->rollBack();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                        }
                        $piProductM = new PurchaseInvoiceProduct();
                        $piProductData = array(
                            'purchaseInvoiceID' => $piID,
                            'locationProductID' => $locationPID,
                            'productSerialID' => $insertedSID,
                            'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                            'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                            'purchaseInvoiceProductQuantity' => 1,
                            'purchaseInvoiceProductTotalQty' => $productBaseQty,
                            'purchaseInvoiceProductTotal' => $product['pTotal'],
                            'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                            'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                            'rackID' => '',
                            'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                        );

                        $piProductM->exchangeArray($piProductData);
                        $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                        if(!$insertedPiProductID){
                            $this->rollBack();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                        }
                        //save item in details
                        $itemInData = array(
                            'itemInDocumentType' => 'Payment Voucher',
                            'itemInDocumentID' => $piID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => $insertedSID,
                            'itemInQty' => 1,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            $this->rollBack();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                        }
                        if (array_key_exists('pTax', $product)) {
                            if (array_key_exists('tL', $product['pTax'])) {
                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                    $piPTaxesData = array(
                                        'purchaseInvoiceID' => $piID,
                                        'purchaseInvoiceProductID' => $insertedPiProductID,
                                        'purchaseInvoiceTaxID' => $taxKey,
                                        'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                        'purchaseInvoiceTaxAmount' => $productTax['tA']
                                    );
                                    $piPTaxM = new PurchaseInvoiceProductTax();
                                    $piPTaxM->exchangeArray($piPTaxesData);
                                    $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                }
                            }
                        }
                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                        );
                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        if(!$status){
                            $this->rollBack();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                        }
                    }
                } else {
                    //if the product is non serial and non batch
                    $piProductM = new PurchaseInvoiceProduct();
                    $piProductData = array(
                        'purchaseInvoiceID' => $piID,
                        'locationProductID' => $locationPID,
                        'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                        'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                        'purchaseInvoiceProductQuantity' => $productBaseQty,
                        'purchaseInvoiceProductTotalQty' => $productBaseQty,
                        'purchaseInvoiceProductTotal' => $product['pTotal'],
                        'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                        'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                        'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                    );

                    $piProductM->exchangeArray($piProductData);
                    $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                    if(!$insertedPiProductID){
                        $this->rollBack();
                        return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                    }

                    if ($productType != 2) {
                        $itemInData = array(
                            'itemInDocumentType' => 'Payment Voucher',
                            'itemInDocumentID' => $piID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => NULL,
                            'itemInQty' => $productBaseQty,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            $this->rollBack();
                            return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                        }
                    }
                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $piPTaxesData = array(
                                    'purchaseInvoiceID' => $piID,
                                    'purchaseInvoiceProductID' => $insertedPiProductID,
                                    'purchaseInvoiceTaxID' => $taxKey,
                                    'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                    'purchaseInvoiceTaxAmount' => $productTax['tA']
                                );
                                $piPTaxM = new PurchaseInvoiceProductTax();
                                $piPTaxM->exchangeArray($piPTaxesData);
                                $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                            }
                        }
                    }
                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    if ($productType == 2) {
                        $productBaseQty = 0;
                    }

                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                    if(!$status){
                        $this->rollBack();
                        return ['status' => false, 'msg' => $this->getMessage('ERR_PURINV_CREATE'), 'data' => null];
                    }
                }
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
                $locationPrTotalQty = 0;
                $locationPrTotalPrice = 0;
                $newItemInIds = array();
                if(count($itemInData) > 0){
                    foreach ($itemInData as $key => $value) {
                        if($value->itemInID > $locationProductLastItemInID){
                            $newItemInIds[] = $value->itemInID;
                            $remainQty = $value->itemInQty - $value->itemInSoldQty;
                            if($remainQty > 0){
                                $itemInPrice = $remainQty*$value->itemInPrice;
                                $itemInDiscount = $remainQty*$value->itemInDiscount;
                                $locationPrTotalQty += $remainQty;
                                $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                            }
                            $locationProductLastItemInID = $value->itemInID;
                        }
                    }
                }
                $locationPrTotalQty += $locationProductQuantity;
                $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                if($locationPrTotalQty > 0){
                    $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                    $lpdata = array(
                        'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                        'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                    foreach ($newItemInIds as $inID) {
                        $intemInData = array(
                            'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                            );
                        $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                    }
                }
            }
            //      call product updated event
            $productIDs = array_map(function($element) {
                return $element['pID'];
            }, $post['pr']);

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            if ($copyFromPo == TRUE) {
                $this->checkAndUpdatePoStatus($post['startByPoID']);
            }

            $jeResult = $this->saveJournalEntryDataForPI($piID, $purchaseInvoiceCode, $post['sID'], $post['pr'], $post['dC'], $allProductTotal, $issueDate, '', true, $journalEntryAccountsData, $flag = 2, $dimensionData, $ignoreBudgetLimit);

            if($jeResult['status']){
                $this->setLogMessage($this->companyCurrencySymbol . $post['fT'] . ' amount purchase invoice successfully created - ' . $purchaseInvoiceCode);
                $this->flashMessenger()->addMessage($jeResult['msg']);
            }else{
                $this->rollback();
            }

            return ['status' => $jeResult['status'], 'msg' => $jeResult['msg'], 'data' => $jeResult['data']];
        }
    }

    
    private function saveJournalEntryDataForPI($piID, $purchaseInvoiceCode, $supplierID, $productsData, $deliveryCharge, $allProductTotal, $issueDate, $grnID, $journalEntrySaveFlag = true, $journalEntryAccountsData = [], $flagForGrnID, $dimensionData, $ignoreBudgetLimit = true)
    {
        if ($this->useAccounting == 1) {
            if (empty($journalEntryAccountsData)) {
                //check whether supplier payable account id is set or not
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                if(empty($supplierData->supplierPayableAccountID)){
                    $status = false;
                    $msg = $this->getMessage('ERR_PURINV_SUPPLIER_PAYABLE_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                    return array('status' => $status, 'msg' => $msg, 'data' => '');
                }
                $supplierPayableAccountID = $supplierData->supplierPayableAccountID;

                $accountProduct = array();
                $totalTaxAmount = 0;

                $grnDeliverCharge = 0;

                
                $grnIdsArr = [];
                foreach ($productsData as $value) {
                    if ($flagForGrnID == 1 || $flagForGrnID == 2) {
                        if (!empty($value['grnDocId'])) {
                            $grnIdsArr[] = $value['grnDocId'];
                        }

                    } else if ($flagForGrnID == 4 || $flagForGrnID == 3) {
                        $piGrnData = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPurchaseInvoiceProductDetailsByPurchaseInvoiceProductID($value['pisubProID'])->current();
                        if ($piGrnData['purchaseInvoiceProductDocumentTypeID'] == "10") {
                            $grnIdsArr[] = $piGrnData['grnID'];
                        }
                    }
                }

                foreach (array_unique($grnIdsArr) as $key => $value) {
                    $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnDetailsByGrnId($value);
                    $grnDeliverCharge += floatval($grnData->grnDeliveryCharge);

                }
                $financeAccounts = $this->getFinanceAccounts();
                $updateDeliveryCharge = false;
                foreach ($productsData as $product) {
                    //check whether tax purchase accout is set or not
                    $product['stockUpdate'] =  filter_var($product['stockUpdate'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                    if(isset($product['pTax']['tL'])){
                        foreach ($product['pTax']['tL'] as $key => $value) {
                            $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($key);
                            if(empty($taxData['taxPurchaseAccountID'])){
                                $status = false;
                                $msg = $this->getMessage('ERR_TAX_PURCHASE_ACCOUNT', array($taxData['taxName']));
                                return array('status' => $status, 'msg' => $msg, 'data' => '');
                            }
                        //set tax gl accounts for the journal Entry
                            if(isset($accountTax[$taxData['taxPurchaseAccountID']]['total'])){
                                $accountTax[$taxData['taxPurchaseAccountID']]['total'] += $value['tA'];
                            }else{
                                $accountTax[$taxData['taxPurchaseAccountID']]['total'] = $value['tA'];
                                $accountTax[$taxData['taxPurchaseAccountID']]['taxAccountID'] = $taxData['taxPurchaseAccountID'];
                            }
                        }
                    }
                    $totalTaxAmount += $product['pTax']['tTA'];

                    if($product['stockUpdate']){

                        //check whether Product Inventory Gl account id set or not
                        $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($product['pID']);
                        if(empty($pData['productInventoryAccountID'])){
                            $status = false;
                            $msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                            return array('status' => $status, 'msg' => $msg, 'data' => '');
                        }
                        //calculate product total without tax potion.

                        if ($product['dType'] == 'precentage'){
                            $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                        } elseif ($product['dType'] == 'value') {
                            $productTotal = $product['pQuantity'] * (($product['pUnitPrice'] - $product['pDiscount']));
                        } else {
                            $productTotal = $product['pUnitPrice']*$product['pQuantity'];

                        }
                        //add deliveryCharge to product according to price ratio.
                        $productCreditTotal = 0;
                        if($grnDeliverCharge > 0){
                            if($grnDeliverCharge > $deliveryCharge){
                                $productCreditTotal = (($grnDeliverCharge - $deliveryCharge)*$productTotal)/ $allProductTotal;
                            }else if($deliveryCharge > $grnDeliverCharge){
                                $productTotal += (($deliveryCharge - $grnDeliverCharge)*$productTotal)/ $allProductTotal;
                            }
                        }else{
                            if($deliveryCharge >0){
                                if ($productTotal != 0) {
                                    // $productTotal += $productTotal;
                                } else {
                                    $productTotal = $deliveryCharge;
                                }
                            }
                        }
                        //set gl accounts for the journal entry
                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productCreditTotal;
                        }else{
                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productCreditTotal;
                            $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
                        }

                    }else{
                        if (!empty($product['locationPID'])) {
                            
                            if(empty($supplierData->supplierGrnClearingAccountID)){
                                $status = false;
                                $msg = $this->getMessage('ERR_PURINV_SUPPLIER_GRN_CLEARING_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                                return array('status' => $status, 'msg' => $msg, 'data' => '');
                            }
                            $grnClearingAccountID = $supplierData->supplierGrnClearingAccountID;

                            if ($flagForGrnID == 1 || $flagForGrnID == 2) {
                                $grnProductData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($product['grnDocId'], $product['locationPID'], '', '')->current();
                            } else if ($flagForGrnID == 4 || $flagForGrnID == 3) {
                                if ($product['pisubProID'] != '') {
                                    // use to purchase invoice edit 
                                    $piGrnData = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPurchaseInvoiceProductDetailsByPurchaseInvoiceProductID($product['pisubProID'])->current();
                                    if ($piGrnData['purchaseInvoiceProductDocumentTypeID'] == "10") {
                                        $grnProductData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($piGrnData['grnID'], $product['locationPID'], '', '')->current();
                                    
                                    }
                                    
                                } else if($product['grnDocId'] != null) {
                                    // use to grn copy to purchase invoice
                                        $grnProductData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($product['grnDocId'], $product['locationPID'], '', '')->current();
                                    
                                }
                            }

                            //calculate grn product total without tax potion.
                            // $grnProductTotal = $grnProductData['grnProductPrice']*$product['pQuantity'] * ((100 - $grnProductData['grnProductDiscount']) / 100);
                             //calculate pi product total without tax potion.
                            // $piProductTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);

                            if ($grnProductData['grnProductDiscountType'] == 'percentage'){
                                $grnProductTotal = $grnProductData['grnProductPrice']*$product['pQuantity'] * ((100 - $grnProductData['grnProductDiscount']) / 100);
                            } elseif ($grnProductData['grnProductDiscountType'] == 'value') {
                                $grnProductTotal = $product['pQuantity'] * (($grnProductData['grnProductPrice'] - $grnProductData['grnProductDiscount']));
                            } else {
                                $grnProductTotal = $grnProductData['grnProductPrice']*$product['pQuantity'];

                            }


                            if ($product['dType'] == 'precentage'){
                                $piProductTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                            } elseif ($product['dType'] == 'value') {
                                $piProductTotal = $product['pQuantity'] * (($product['pUnitPrice'] - $product['pDiscount']));
                            } else {
                                $piProductTotal = $product['pUnitPrice']*$product['pQuantity'];

                            }

                            //set gl accounts for the journal entry
                            if(isset($accountProduct[$grnClearingAccountID]['debitTotal'])){
                                $accountProduct[$grnClearingAccountID]['debitTotal'] += $grnProductTotal;
                            }else{
                                $accountProduct[$grnClearingAccountID]['debitTotal'] = $grnProductTotal;
                                $accountProduct[$grnClearingAccountID]['creditTotal'] = 0;
                                $accountProduct[$grnClearingAccountID]['inventoryAccountID'] = $grnClearingAccountID;
                            }

                            if($deliveryCharge > 0 || $grnProductTotal != $piProductTotal){

                                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($product['pID']);
                                if(empty($pData['productInventoryAccountID'])){
                                    $status = false;
                                    $msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                                    return array('status' => $status, 'msg' => $msg, 'data' => '');
                                }

                                $inventoryAccountID = $pData['productInventoryAccountID'];


                                // if ($grnDeliverCharge == $deliveryCharge && !$updateDeliveryCharge) {
                                //     $accountProduct[$grnClearingAccountID]['debitTotal'] += $grnDeliverCharge;
                                //     $updateDeliveryCharge = true;
                                // }

                                //add grn deliveryCharge to product according to price ratio.
                                // $deliveryChargeTotal = 0;
                                // $deliveryChargeCreditTotal = 0;
                                // if($grnDeliverCharge > 0){
                                //     if($grnDeliverCharge > $deliveryCharge){
                                //         $deliveryChargeCreditTotal = (($grnDeliverCharge - $deliveryCharge)*$piProductTotal)/ $allProductTotal;
                                //     }else if($deliveryCharge > $grnDeliverCharge){
                                //         $deliveryChargeTotal = (($deliveryCharge - $grnDeliverCharge)*$piProductTotal)/ $allProductTotal;
                                //     }
                                // }else{
                                //     if($deliveryCharge >0){
                                //         $deliveryChargeTotal = ($deliveryCharge*$piProductTotal)/ $allProductTotal;
                                //     }
                                // }
                                // if(!($deliveryChargeTotal == 0 && $deliveryChargeCreditTotal == 0)){
                                // //set gl accounts for the journal entry
                                //     if(isset($accountProduct[$inventoryAccountID]['debitTotal'])){
                                //         $accountProduct[$inventoryAccountID]['debitTotal'] += $deliveryChargeTotal;
                                //         $accountProduct[$inventoryAccountID]['creditTotal'] += $deliveryChargeCreditTotal;
                                //     }else{
                                //         $accountProduct[$inventoryAccountID]['debitTotal'] = $deliveryChargeTotal;
                                //         $accountProduct[$inventoryAccountID]['creditTotal'] = $deliveryChargeCreditTotal;
                                //         $accountProduct[$inventoryAccountID]['inventoryAccountID'] = $inventoryAccountID;
                                //     }

                                // }

                            //check whether GRN Product Total Equal To The PiProductTotal

                                if($grnProductTotal != $piProductTotal){
                                    if($grnProductTotal > $piProductTotal){

                                        $lessPrice = $grnProductTotal - $piProductTotal;
                                         //set gl accounts for the journal entry
                                        if(isset($accountProduct[$inventoryAccountID]['creditTotal'])){
                                            $accountProduct[$inventoryAccountID]['creditTotal'] += $lessPrice;
                                        }else{
                                            $accountProduct[$inventoryAccountID]['debitTotal'] = 0;
                                            $accountProduct[$inventoryAccountID]['creditTotal'] = $lessPrice;
                                            $accountProduct[$inventoryAccountID]['inventoryAccountID'] = $inventoryAccountID;
                                        }
                                    }else if($grnProductTotal < $piProductTotal){

                                        $heigherPrice =  $piProductTotal - $grnProductTotal;
                                        //set gl accounts for the journal entry
                                        if(isset($accountProduct[$inventoryAccountID]['debitTotal'])){
                                            $accountProduct[$inventoryAccountID]['debitTotal'] += $heigherPrice;
                                        }else{
                                            $accountProduct[$inventoryAccountID]['debitTotal'] = $heigherPrice;
                                            $accountProduct[$inventoryAccountID]['creditTotal'] = 0;
                                            $accountProduct[$inventoryAccountID]['inventoryAccountID'] = $inventoryAccountID;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $grnClearingAccountID = $supplierData->supplierGrnClearingAccountID;
                $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
                $glAccountsData = $glAccountExist->current();

                if ($deliveryCharge == $grnDeliverCharge && (!empty($grnID) || !empty($grnIdsArr))) {
                    $accountProduct[$grnClearingAccountID]['debitTotal'] += $grnDeliverCharge;
                    $payableTotalAmount = $allProductTotal+$totalTaxAmount+($grnDeliverCharge);
                }elseif ($deliveryCharge > $grnDeliverCharge && (!empty($grnID) || !empty($grnIdsArr))) {
                    if  (!empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)) {
                        $accountProduct[$grnClearingAccountID]['debitTotal'] += $grnDeliverCharge;
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['debitTotal'] = $deliveryCharge - $grnDeliverCharge;
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] = 0;
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['inventoryAccountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                        $payableTotalAmount = $allProductTotal+$totalTaxAmount+($deliveryCharge);
                    } else {
                        $status = false;
                        $msg = $this->getMessage('ERR_DELI_CHARGE_ACCOUNT');
                        return array('status' => $status, 'msg' => $msg, 'data' => '');
                    }

                }elseif ($deliveryCharge < $grnDeliverCharge && (!empty($grnID) || !empty($grnIdsArr))) {
                    if  (!empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)) {
                        $accountProduct[$grnClearingAccountID]['debitTotal'] += $grnDeliverCharge;
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] = $grnDeliverCharge - $deliveryCharge;
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['debitTotal'] = 0;
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['inventoryAccountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                        $payableTotalAmount = $allProductTotal+$totalTaxAmount+($deliveryCharge);
                    } else {
                        $status = false;
                        $msg = $this->getMessage('ERR_DELI_CHARGE_ACCOUNT');
                        return array('status' => $status, 'msg' => $msg, 'data' => '');
                    }
                } elseif ($deliveryCharge > $grnDeliverCharge && (empty($grnID) && empty($grnIdsArr))) {
                    if  (!empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)) {
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['debitTotal'] = $deliveryCharge - $grnDeliverCharge;
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] = 0;
                        $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['inventoryAccountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                        $payableTotalAmount = $allProductTotal+$totalTaxAmount+($deliveryCharge);
                    } else {
                        $status = false;
                        $msg = $this->getMessage('ERR_DELI_CHARGE_ACCOUNT');
                        return array('status' => $status, 'msg' => $msg, 'data' => '');
                    }
                } else {                    
                    $payableTotalAmount = $allProductTotal+$totalTaxAmount+($deliveryCharge - $grnDeliverCharge);
                }

                $payableDebitTotalAmount = 0;
                if($payableTotalAmount < 0){
                    $payableDebitTotalAmount = -1*$payableTotalAmount;
                    $payableTotalAmount = 0.00;
                }

                $i=0;
                $journalEntryAccounts = array();

                foreach ($accountProduct as $key => $value) {
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
                    $journalEntryAccounts[$i]['financeAccountsName'] = $financeAccounts[$value['inventoryAccountID']]['financeAccountsCode']."_".$financeAccounts[$value['inventoryAccountID']]['financeAccountsName'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] =$value['creditTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By PI '.$purchaseInvoiceCode;
                    $i++;
                }

                if(isset($accountTax)){
                    foreach ($accountTax as $taxkey => $taxVal) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $taxVal['taxAccountID'];
                        $journalEntryAccounts[$i]['financeAccountsName'] = $financeAccounts[$taxVal['taxAccountID']]['financeAccountsCode']."_".$financeAccounts[$taxVal['taxAccountID']]['financeAccountsName'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $taxVal['total'];
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By PI '.$purchaseInvoiceCode;
                        $i++;
                    }
                }


                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $supplierPayableAccountID;
                $journalEntryAccounts[$i]['financeAccountsName'] = $financeAccounts[$supplierPayableAccountID]['financeAccountsCode']."_".$financeAccounts[$supplierPayableAccountID]['financeAccountsName'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $payableDebitTotalAmount;
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $payableTotalAmount;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By PI '.$purchaseInvoiceCode.".";

                $journalEntryComment = 'Journal Entry is posted when create Purchase Invoice '.$purchaseInvoiceCode.'.';
            } else {

                $journalEntryAccounts = $journalEntryAccountsData['journalEntryAccounts'];
                $journalEntryComment = $journalEntryAccountsData['journalEntryComment'];
            }

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);
            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $issueDate,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => $journalEntryComment,
                'documentTypeID' => 12,
                'journalEntryDocumentID' => $piID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );
            if (!$journalEntrySaveFlag) {
                return array('status' => true, 'msg' => $this->getMessage('SUCC_PURINV_JOURNAL_ENTRY_RETRIVED'), 'data' => $journalEntryData);
            }

            $resultData = $this->saveJournalEntry($journalEntryData);

            if($resultData['status']){
                foreach ($dimensionData as $value) {
                    if (!empty($value)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($value, $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            return ['status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']];
                        }   
                    }
                }

                $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($piID,12, 3);
                if(!$jEDocStatusUpdate['status']){
                    return array(
                        'status'=> false,
                        'msg'=>$jEDocStatusUpdate['msg'],
                        );
                }

                $status = true;
                $data = array('piID' => $piID);
                $msg = $this->getMessage('SUCC_PURINV_CREATE', array($purchaseInvoiceCode));
            }else{
                $status = false;
                $msg = $resultData['msg'];
                $data = $resultData['data'];
            }
        } else {
            $status = true;
            $data = array('piID' => $piID);
            $msg = $this->getMessage('SUCC_PURINV_CREATE', array($purchaseInvoiceCode));
        }
        return array('status' => $status, 'msg' => $msg, 'data' => $data);
    }

    public function getPurchaseInvoiceJournalEntriesAction()
    {
        $this->status = true;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespond();
        }

        $postData = $request->getPost()->toArray();
        //get pi existing dataset for checking
        $dataSet = $this->getPiDataSet($postData['efectedPiID']);

        $purchaseInvoiceData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataByID($postData['efectedPiID']);

        $locationId = $dataSet['piDetails']['locID']; // get location

        $updateFlag = false;

        if(sizeof(array_filter($dataSet['piDetails']['piProducts'])) != sizeof(array_filter($postData['pr']))){
            $updateFlag = true;
        } else {

            $locationProMatchFlag = false;
            $itemCounter = 0;

            foreach ($dataSet['piDetails']['piProducts'] as $key => $existingvalue) {
                foreach (array_filter($postData['pr']) as $key => $newValue) {
                    if($newValue['locationPID'] == $existingvalue['lPID']){
                        $locationProMatchFlag = true;
                        if($newValue['pQuantity'] == $existingvalue['piPQ'] && $newValue['pUnitPrice'] == $existingvalue['piPP'] &&
                            $newValue['pDiscount'] == $existingvalue['piPD'] && $newValue['pTotal'] == $existingvalue['piPT']){
                            $itemCounter++;
                        } else {
                            $updateFlag = true;

                        }
                    }
                }
            }

        }
        if(!$locationProMatchFlag){
            $updateFlag = true;
        }
        if(sizeof(array_filter($dataSet['piDetails']['piProducts'])) != $itemCounter){
            $updateFlag = true;
        }

        if (floatval($postData['dC']) != floatval($purchaseInvoiceData['purchaseInvoiceDeliveryCharge'])) {
            $updateFlag = true;
        }

        $issueDate = $this->convertDateToStandardFormat($postData['iD']);
        $allProductTotal = 0;
        foreach ($postData['pr'] as $product) {
            if ($product['dType'] == 'precentage') {
                $productTotal = $product['pUnitPrice'] * $product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
            } elseif ($product['dType'] == 'value') {
                $productTotal = $product['pQuantity'] * (($product['pUnitPrice'] - $product['pDiscount']));
            } else {
                $productTotal = $product['pUnitPrice'] * $product['pQuantity'];
            }

            $allProductTotal += $productTotal;
        }

        if ($updateFlag) {
            $piJournalEntryData = $this->saveJournalEntryDataForPI(null, $postData['piC'], $postData['sID'], $postData['pr'], $postData['dC'], $allProductTotal, $issueDate, $postData['startByGrnID'], false, [], $flag = 3,[], $postData['ignoreBudgetLimit']);
        } else {
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID(12,$postData['efectedPiID']);
            
            $journalEntryAccountDetails = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryData['journalEntryID']);

            $i=0;
            $journalEntryAccounts = array();
            foreach ($journalEntryAccountDetails as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
                $journalEntryAccounts[$i]['financeAccountsName'] = $value['financeAccountsCode']."_".$value['financeAccountsName'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsDebitAmount'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] =$value['journalEntryAccountsCreditAmount'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = $value['journalEntryAccountsMemo'];
                $i++;
            }

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $journalEntryData['journalEntryDate'],
                'journalEntryCode' => $journalEntryData['journalEntryCode'],
                'journalEntryTypeID' => $journalEntryData['journalEntryTypeID'],
                'journalEntryIsReverse' => $journalEntryData['journalEntryIsReverse'],
                'journalEntryComment' => $journalEntryData['journalEntryComment'],
                'documentTypeID' => $journalEntryData['documentTypeID'],
                'journalEntryDocumentID' => $journalEntryData['journalEntryDocumentID'],
            );
            $piJournalEntryData = array('status' => true, 'msg' => $this->getMessage('SUCC_PURINV_JOURNAL_ENTRY_RETRIVED'), 'data' => $journalEntryData);
        }
              

        $this->status = $piJournalEntryData['status'];
        $this->msg = $piJournalEntryData['msg'];
        $this->data = $piJournalEntryData['data'];

        return $this->JSONRespond();
    }

    /**
     * @author Damith Thamara<damith@thinkcube.com>
     * @param String  $locationID
     * @return string PI number for passed locationID
     */
    public function getPiNoForLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $refData = $this->getReferenceNoForLocation(14, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];
            if ($rid == '' || $rid == NULL) {
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_PURINV_REFLOCATION');
                } else {
                    $this->msg = $this->getMessage('ERR_PURINV_REFMAX');
                }
                $this->data = FALSE;
                $this->status = false;
            } else {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $rid,
                    'locRefID' => $lrefID,
                );
            }
            $this->setLogMessage("Retrive purchase invoice code.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Damith Thamara<damith@thinkcube.com>
     * @param String  PI no or supplier name as searchkey
     * @return viewmodel PI list
     */
    public function getPisForSearchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $dateFormat = $this->getUserDateFormat();
            $statuses = $globaldata[
                    'statuses'];
            if ($request->getPost('searchKey') != '') {
                $piList = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPisforSearch($request->getPost('searchKey'));
                $piListView = new ViewModel(array('piList' => $piList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
            } else {
                $this->paginator = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoices(true);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $piListView = new ViewModel(array('piList' => $this->paginator, 'statuses' => $statuses));
            }

            $piListView->setTemplate('inventory/purchase-invoice/purchase-invoice-list');
            $this->html = $piListView;
            return $this->JSONRespondHtml();
        }
    }

    public function checkAndUpdateGrnStatus($grnID)
    {
        $grnProducts = $this->CommonTable('Inventory\Model\GrnProductTable')->getUnCopiedGrnProducts($grnID);
        if (empty($grnProducts)) {
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Inventory\Model\GrnTable')->updateGrnStatus($grnID, $closeStatusID);
        } else {
            $getUnCopiedGrnProductQty = 0;
            $prReturnQty = 0;
            foreach ($grnProducts as $key => $value) {
                if (is_null($value['productBatchID']) && is_null($value['productSerialID'])) {
                    $getUnCopiedGrnProductQty += $value['grnProductQuantity'] - $value['grnProductTotalCopiedQuantity'];
                } else if (!is_null($value['productBatchID'])) {
                    $getUnCopiedGrnProductQty += $value['grnProductQuantity'] - $value['grnProductCopiedQuantity'];
                } else {
                    $getUnCopiedGrnProductQty += $value['grnProductQuantity'];
                }

                $prProducts = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getGrnProductReturnDetailsByGrnIdAndGrnProductId($value['grnID'],$value['grnProductID']);

                foreach ($prProducts as $key => $val) {
                    if (is_null($val['purchaseReturnProductSerialID']) && is_null($val['purchaseReturnProductBatchID'])) {
                        $prReturnQty += $val['purchaseReturnProductReturnedQty'];
                    } else {
                        $prReturnQty += $val['purchaseReturnSubProductReturnedQty'];
                    }
                }
            }
            if ($getUnCopiedGrnProductQty == $prReturnQty) {
                $closeStatusID = $this->getStatusID('closed');
                $this->CommonTable('Inventory\Model\GrnTable')->updateGrnStatus($grnID, $closeStatusID);
            }
        }
    }

    public function checkAndUpdatePoStatus($poID)
    {
        $poProducts = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getUnCopiedPoProducts($poID);
        if (empty($poProducts)) {
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Inventory\Model\PurchaseOrderTable')->updatePoStatus($poID, $closeStatusID);
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param string toEmail,string subject,string body,string documentID
     * This function send a email with attached PI to the passed address
     */
    public function sendPiEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Payment Voucher (Purchase Invoice)';

        if ($request->isPost()) {
            $documentID = $request->getPost('documentID');
            $email = $request->getPost('to_email');
            $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($documentID)->current();

            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_PURINV_VOUCHER');
            $this->setLogMessage($piData['purchaseInvoiceCode']." purchase invoice document email to ".$email.'.');
            return $this->JSONRespond();
        }

        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_GRN_EMAIL', array($documentType));
        $this->setLogMessage("Error occured when email the purchase invoice document.");
        return $this->JSONRespond();
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * this function return details of pv
     */
    public function getPurchaseVoucherDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $PVID = $request->getPost('PVID');
            $pVProductsArray = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPVProductByPVIDForDebitNoteCreate($PVID);
            $dparray;
            $dparrayQty;
            $iparray;
            $iparrayQty;
            $realproduct;
            $checkProduct = 0;
            $unitPrice = 0;
            $pId = 0;

            foreach ($pVProductsArray as $dp) {
                $dp = (object) $dp;
                    $dparray[$dp->purchaseInvoiceProductID] = $dp->productID;
                    if ($dp->purchaseInvoiceProductDocumentTypeID == "10" && $dp->grnID != null) {
                        $dparrayQty[$dp->productID."_".$dp->purchaseInvoiceProductPrice."_".$dp->grnID] = $dp->purchaseInvoiceProductTotalQty;
                    } else {
                        $dparrayQty[$dp->productID."_".$dp->purchaseInvoiceProductPrice] = $dp->purchaseInvoiceProductTotalQty;
                    }
            }

            $data = $this->CommonTable('Inventory\Model\DebitNoteProductTable')->getDebitNoteProductsByInvoiceID($PVID);
            foreach ($data as $ip) {
                $ip = (object) $ip;
                $iparray[$ip->purchaseInvoiceProductID] = $ip->productID;
                if ($ip->purchaseInvoiceProductDocumentTypeID == "10" && $ip->grnID != null) {
                    $iparrayQty[$ip->productID."_".$ip->purchaseInvoiceProductPrice."_".$ip->grnID]+=$ip->debitNoteProductQuantity;
                } else {
                    $iparrayQty[$ip->productID."_".$ip->purchaseInvoiceProductPrice]+=$ip->debitNoteProductQuantity;
                }
            }

            foreach ($dparrayQty as $key => $Pdata) {
                if ($iparrayQty[$key] != $Pdata) {
                    $checkProduct = 1;
                    $realproduct[$key] = $key;
                }
            }

            $purchaseVoucher = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseVoucherByID($PVID);
            if ($checkProduct) {
                $pVProduct = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPVProductByPVIDForDebitNoteCreate($PVID);
                $tax = array(
                    'pVTaxID' => '',
                    'pVTaxName' => '',
                    'pVTaxPrecentage' => '',
                    'pVTaxAmount' => '',
                );
                $subProduct = array(
                    'pVSubProductID' => '',
                    'productBatchID' => '',
                    'productSerialID' => '',
                    'pVSubProductQuantity' => '',
                );

                $Products = array(
                    'pVProductID' => '',
                    'pVID' => '',
                    'productID' => '',
                    'pVProductPrice' => '',
                    'pVProductDiscount' => '',
                    'pVProductDiscountType' => '',
                    'pVProductQuantity' => '',
                    'productCode' => '',
                    'productName' => '',
                    'productType' => '',
                    'tax' => '',
                    'subProduct' => '',
                );
                $debitNoteProductQty = array();
                $debitNoteSubProductSerial = array();
                $debitNoteSubProductBatch = array();
                $debitNoteProductIDArray = array();
                $debitNoteSubProductIDArray = array();

                foreach ($pVProduct as $data) {
                    $tt = (object) $data;
                    if ($tt->purchaseInvoiceProductDocumentTypeID == "10" && $tt->grnID != null) {
                        if ($realproduct[$tt->productID."_".$tt->purchaseInvoiceProductPrice."_".$tt->grnID] == $tt->productID."_".$tt->purchaseInvoiceProductPrice."_".$tt->grnID) {
                            $debitNoteProductData = $this->CommonTable('Inventory\Model\DebitNoteProductTable')->getDebitNoteProductAndSubProductDataByPvProductID($tt->purchaseInvoiceProductID);
                            foreach ($debitNoteProductData as $prValue) {
                                $prValue = (object) $prValue;
                                if ($prValue->debitNoteProductID) {
    //use this if statement for ignore repeating debitNoteProducts
                                    if (!$debitNoteProductIDArray[$prValue->debitNoteProductID] == 1) {
                                        $debitNoteProductQty[$tt->productID."_".$tt->purchaseInvoiceProductPrice.'_'.$tt->grnID]+=$prValue->debitNoteProductQuantity;
                                        $debitNoteProductIDArray[$prValue->debitNoteProductID] = 1;
                                    }
                                    if ($prValue->productSerialID) {
                                        if (!$debitNoteSubProductIDArray[$prValue->debitNoteProductID][$prValue->productSerialID] == 1) {
                                            $debitNoteSubProductSerial[$tt->productID."_".$tt->purchaseInvoiceProductPrice.'_'.$tt->grnID][$prValue->productSerialID] += $prValue->debitNoteSubProductQuantity;
                                            $debitNoteSubProductIDArray[$prValue->debitNoteProductID][$prValue->productSerialID] = 1;
                                        }
                                    } else if ($prValue->productBatchID) {
                                        if (!$debitNoteSubProductIDArray[$prValue->debitNoteProductID][$prValue->productBatchID] == 1) {
                                            $debitNoteSubProductBatch[$tt->productID."_".$tt->purchaseInvoiceProductPrice.'_'.$tt->grnID][$prValue->productBatchID] += $prValue->debitNoteSubProductQuantity;
                                            $debitNoteSubProductIDArray[$prValue->debitNoteProductID][$prValue->productBatchID] = 1;
                                        }
                                    }
                                }
                            }
                        }

                    } else {
                        if ($realproduct[$tt->productID."_".$tt->purchaseInvoiceProductPrice] == $tt->productID."_".$tt->purchaseInvoiceProductPrice) {
                            $debitNoteProductData = $this->CommonTable('Inventory\Model\DebitNoteProductTable')->getDebitNoteProductAndSubProductDataByPvProductID($tt->purchaseInvoiceProductID);
                            foreach ($debitNoteProductData as $prValue) {
                                $prValue = (object) $prValue;
                                if ($prValue->debitNoteProductID) {
    //use this if statement for ignore repeating debitNoteProducts
                                    if (!$debitNoteProductIDArray[$prValue->debitNoteProductID] == 1) {
                                        $debitNoteProductQty[$tt->productID."_".$tt->purchaseInvoiceProductPrice]+=$prValue->debitNoteProductQuantity;
                                        $debitNoteProductIDArray[$prValue->debitNoteProductID] = 1;
                                    }
                                    if ($prValue->productSerialID) {
                                        if (!$debitNoteSubProductIDArray[$prValue->debitNoteProductID][$prValue->productSerialID] == 1) {
                                            $debitNoteSubProductSerial[$tt->productID."_".$tt->purchaseInvoiceProductPrice][$prValue->productSerialID] += $prValue->debitNoteSubProductQuantity;
                                            $debitNoteSubProductIDArray[$prValue->debitNoteProductID][$prValue->productSerialID] = 1;
                                        }
                                    } else if ($prValue->productBatchID) {
                                        if (!$debitNoteSubProductIDArray[$prValue->debitNoteProductID][$prValue->productBatchID] == 1) {
                                            $debitNoteSubProductBatch[$tt->productID."_".$tt->purchaseInvoiceProductPrice][$prValue->productBatchID] += $prValue->debitNoteSubProductQuantity;
                                            $debitNoteSubProductIDArray[$prValue->debitNoteProductID][$prValue->productBatchID] = 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $pVProduct2 = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPVProductByPVIDForDebitNoteCreate($PVID);
                $unitPrice = 0;
                while ($tt = $pVProduct2->current()) {
                    $t = (object) $tt;
                    if ($t->purchaseInvoiceProductDocumentTypeID == "10" && $t->grnID != null) {
                        if ($realproduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID] == $t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID) {
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID] = (object) $Products;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->pVProductID = $t->purchaseInvoiceProductID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->pVID = $t->purchaseInvoiceID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->productID = $t->productID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->pVProductPrice = $t->purchaseInvoiceProductPrice;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->pVProductDiscount = $t->purchaseInvoiceProductDiscount;
                            if ($t->productPurchaseDiscountValue != null) {
                                $discountType = 'value';
                            } else if ($t->productPurchaseDiscountPercentage != null) {
                                $discountType = 'precentage';
                            } else {
                                $discountType = 'Non';
                            }
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->pVProductDiscountType = $discountType;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->pVProductQuantity = $t->purchaseInvoiceProductTotalQty - $debitNoteProductQty[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID];
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->productCode = $t->productCode;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->productName = $t->productName;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->productType = $t->productTypeID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->purchaseInvoiceProductDocumentTypeID = $t->purchaseInvoiceProductDocumentTypeID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->purchaseInvoiceProductDocumentID = $t->purchaseInvoiceProductDocumentID;

                            if ($t->productBatchID != '' || $t->productSerialID != '') {
                                $debitNoteQuantity = 0;
                                $Quantity = 0;
                                if ($debitNoteSubProductSerial[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->productSerialID]) {
                                    $debitNoteQuantity = $debitNoteSubProductSerial[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->productSerialID];
                                } else if ($debitNoteSubProductBatch[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->productBatchID]) {
                                    $debitNoteQuantity = $debitNoteSubProductBatch[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->productBatchID];
                                }

                                if ($t->productSerialID != '') {
                                    $Quantity = 1;
                                } else {
                                    $Quantity = $t->purchaseInvoiceProductQuantity;
                                }

                                if (!is_null($t->productBatchID)) {
                                    $batchData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($t->productBatchID);
                                    $batchCode = $batchData->productBatchCode;
                                }
                                if (!is_null($t->productSerialID)) {
                                    $serialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($t->productSerialID);
                                    $serialCode = $serialData->productSerialCode;
                                }
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceProductID] = (object) $subProduct;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceProductID]->pVSubProductID = $t->purchaseInvoiceProductID;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceProductID]->pvProductDocumentID = $t->purchaseInvoiceProductDocumentID;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceProductID]->productBatchID = $t->productBatchID;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceProductID]->productBatchCode = $batchCode;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceProductID]->productSerialID = $t->productSerialID;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceProductID]->productSerialCode = $serialCode;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceProductID]->pVSubProductQuantity = $Quantity - $debitNoteQuantity;
                            }
                            if ($purchaseVoucherSubProduct) {
                                $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->subProduct = $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID];
                            }

                            if ($t->purchaseInvoiceTaxID) {
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceTaxID] = (object) $tax;
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceTaxID]->pVTaxID = $t->purchaseInvoiceTaxID;
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceTaxID]->pVTaxName = $t->taxName;
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceTaxID]->pVTaxPrecentage = $t->purchaseInvoiceTaxPrecentage;
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID][$t->purchaseInvoiceTaxID]->pVTaxAmount = $t->purchaseInvoiceTaxAmount;
                                $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID]->tax = $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice.'_'.$t->grnID];
                            }
                        }
                    } else {
                        if ($realproduct[$t->productID."_".$t->purchaseInvoiceProductPrice] == $t->productID."_".$t->purchaseInvoiceProductPrice) {
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice] = (object) $Products;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->pVProductID = $t->purchaseInvoiceProductID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->pVID = $t->purchaseInvoiceID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->productID = $t->productID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->pVProductPrice = $t->purchaseInvoiceProductPrice;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->pVProductDiscount = $t->purchaseInvoiceProductDiscount;
                            if ($t->productPurchaseDiscountValue != null) {
                                $discountType = 'value';
                            } else if ($t->productPurchaseDiscountPercentage != null) {
                                $discountType = 'precentage';
                            } else {
                                $discountType = 'Non';
                            }
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->pVProductDiscountType = $discountType;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->pVProductQuantity = $t->purchaseInvoiceProductTotalQty - $debitNoteProductQty[$t->productID."_".$t->purchaseInvoiceProductPrice];
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->productCode = $t->productCode;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->productName = $t->productName;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->productType = $t->productTypeID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->purchaseInvoiceProductDocumentTypeID = $t->purchaseInvoiceProductDocumentTypeID;
                            $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->purchaseInvoiceProductDocumentID = $t->purchaseInvoiceProductDocumentID;

                            if ($t->productBatchID != '' || $t->productSerialID != '') {
                                $debitNoteQuantity = 0;
                                $Quantity = 0;
                                if ($debitNoteSubProductSerial[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->productSerialID]) {
                                    $debitNoteQuantity = $debitNoteSubProductSerial[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->productSerialID];
                                } else if ($debitNoteSubProductBatch[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->productBatchID]) {
                                    $debitNoteQuantity = $debitNoteSubProductBatch[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->productBatchID];
                                }

                                if ($t->productSerialID != '') {
                                    $Quantity = 1;
                                } else {
                                    $Quantity = $t->purchaseInvoiceProductQuantity;
                                }

                                if (!is_null($t->productBatchID)) {
                                    $batchData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($t->productBatchID);
                                    $batchCode = $batchData->productBatchCode;
                                }
                                if (!is_null($t->productSerialID)) {
                                    $serialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($t->productSerialID);
                                    $serialCode = $serialData->productSerialCode;
                                }
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceProductID] = (object) $subProduct;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceProductID]->pVSubProductID = $t->purchaseInvoiceProductID;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceProductID]->pvProductDocumentID = $t->purchaseInvoiceProductDocumentID;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceProductID]->productBatchID = $t->productBatchID;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceProductID]->productBatchCode = $batchCode;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceProductID]->productSerialID = $t->productSerialID;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceProductID]->productSerialCode = $serialCode;
                                $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceProductID]->pVSubProductQuantity = $Quantity - $debitNoteQuantity;
                            }
                            if ($purchaseVoucherSubProduct) {
                                $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->subProduct = $purchaseVoucherSubProduct[$t->productID."_".$t->purchaseInvoiceProductPrice];
                            }

                            if ($t->purchaseInvoiceTaxID) {
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceTaxID] = (object) $tax;
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceTaxID]->pVTaxID = $t->purchaseInvoiceTaxID;
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceTaxID]->pVTaxName = $t->taxName;
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceTaxID]->pVTaxPrecentage = $t->purchaseInvoiceTaxPrecentage;
                                $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice][$t->purchaseInvoiceTaxID]->pVTaxAmount = $t->purchaseInvoiceTaxAmount;
                                $pVProducts[$t->productID."_".$t->purchaseInvoiceProductPrice]->tax = $taxes[$t->productID."_".$t->purchaseInvoiceProductPrice];
                            }
                        }
                    }
                }

                $locationID = $request->getPost('locationID');
                $productsDetails = Array();
                foreach ($pVProducts as $key => $value) {
                    $productsDetails[$key] = $this->getLocationProductDetailsForReturn($locationID, $value->productID);
                }
                

                $locationPro = Array();
                $inactiveItems = Array();
                foreach ($pVProducts as $value) {
                    $locationPro[$value->productID] = $this->getLocationProductDetails($locationID, $value->productID);
                    if ($locationPro[$value->productID] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($value->productID);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }
                }

                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_PI_ITM_INACTIVE_COPY', [$errorItems]);
                }

                $this->status = true;
                $this->data = array(
                    'status' => true,
                    'purchaseVoucher' => $purchaseVoucher,
                    'pVProduct' => $pVProducts,
                    'productDetails' => $productsDetails,
                    'errorMsg' => $errorMsg,
                    'inactiveItemFlag' => $inactiveItemFlag
                );
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PURINV_USED_FOR_CN');
            }
            $this->setLogMessage("Retrive purchase invoice ".$purchaseVoucher['purchaseInvoiceCode']." details.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * search All PV for drop down
     * @return ViewModel dropdown list
     */
    public function searchAllPVForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            if ($locationID == "" || $locationID == null) {
                $locationID = false;
            }

            $this->setLogMessage("Retrive purhcase invoice list for dropdown.");
            $this->data = array('list' => $this->_searchPVForDropdown($searchKey, $locationID));
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search Location wise all PVs
     * @return ViewModel for dropdown list
     */
    public function searchLocationWisePVForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $this->data = array('list' => $this->_searchPVForDropdown($searchKey, $locationID));
            $this->setLogMessage("Retrive purchase invoice list for dropdown.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param string $searchKey
     * @param string $locationID, not mentotary
     * @return array
     */
    private function _searchPVForDropdown($searchKey, $locationID = false)
    {
        $pis = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->searchPIForDropdown($searchKey, $locationID);

        $piList = array();
        foreach ($pis as $pi) {
            $temp['value'] = $pi['purchaseInvoiceID'];
            $temp['text'] = $pi['purchaseInvoiceCode'];
            $piList[] = $temp;
        }

        return $piList;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search Purchasing voucher By Purchasing Voucher ID for view page
     * @return viewModel
     */
    public function searchPVByPVIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $dateFormat = $this->getUserDateFormat();
            $statuses = $globaldata['statuses'];
            $userActiveLocation = $this->user_session->userActiveLocation;
            if ($request->getPost('PVID') == '' && $request->getPost('supplierID') == '') {
                $this->paginator = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoices(true, $userActiveLocation['locationID']);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $piListView = new ViewModel(array('piList' => $this->paginator, 'statuses' => $statuses));
                $this->setLogMessage("Retrive purchase invoices.");
            } else {
                $piList = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPVsForSearch($request->getPost('PVID'), $request->getPost('supplierID'), $userActiveLocation['locationID']);
                $piListView = new ViewModel(array('piList' => $piList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
                $this->setLogMessage("Retrive purchase invoice.");
            }

            $piListView->setTemplate('inventory/purchase-invoice/purchase-invoice-list');
            $this->html = $piListView;
            return $this->JSONRespondHtml();
        }
    }


    public function searchPVBySerialCodeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $dateFormat = $this->getUserDateFormat();
            $statuses = $globaldata['statuses'];
            $userActiveLocation = $this->user_session->userActiveLocation;
            $piList = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPVsBySerialCode($request->getPost('searchString'), $userActiveLocation['locationID']);
            $piListView = new ViewModel(array('piList' => $piList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
            $this->setLogMessage("Retrive purchase invoice.");

            $piListView->setTemplate('inventory/purchase-invoice/purchase-invoice-list');
            $this->html = $piListView;
            return $this->JSONRespondHtml();
        }
    }

    public function updatePoProductCopiedQty($locationPID, $poId, $productQty)
    {

        $oldPoProductData = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getPoProduct($poId, $locationPID);
        $recordedQty = $oldPoProductData['purchaseOrderProductQuantity'];
        $currentUpdatedQty = $oldPoProductData['purchaseOrderProductCopiedQuantity'];
        $updatingQty = floatval($currentUpdatedQty) + floatval($productQty);
        $allCopied = FALSE;
        if (floatval($updatingQty) >= floatval($recordedQty)) {
            $allCopied = TRUE;
        }
        $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->updateCopiedPoProductQty($locationPID, $poId, $updatingQty);
        if ($allCopied) {
            $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->updateCopiedPoProducts($locationPID, $poId);
        }
    }

    public function piCancelAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $pIID = $request->getPost('pIID');
        $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
        $checkPIForCancel = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->checkPIhasAnyPayments($pIID);

        if (!is_null($checkPIForCancel)) {
            $this->setLogMessage("Error occured when deleting PI");
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_PURINV_INV_DEL');
            return $this->JSONRespond();
        }


        // get purchase invoice details that related to the given purchase invoice id
        $openState = [3,6];
        $details = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByIDAndStatus($pIID, $openState);

        $grnFlag = false;
        $poFlag = false;
        $poID = null;
        $grnID = null;
        $basicPiDetails = $details;
        $defaultDataSet = iterator_to_array($basicPiDetails);

        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(12,$pIID);
        $dimensionData = [];
        foreach ($jEDimensionData as $value) {
            if (!is_null($value['journalEntryID'])) {
                $temp = [];
                $temp['dimensionTypeId'] = $value['dimensionType'];
                $temp['dimensionValueId'] = $value['dimensionValueID'];
                $dimensionData[$details->current()['purchaseInvoiceCode']][] = $temp;
            }
        }

        if(empty($defaultDataSet)){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CANNOT_DELETE_PI');
            return $this->JSONRespond();
        }

        $locationId = $defaultDataSet[0]['purchaseInvoiceRetrieveLocation'];

        //set grn flag and po flag
        if($defaultDataSet[0]['purchaseInvoiceGrnID'] != ''){
            $grnFlag = true;
            $grnID = $defaultDataSet[0]['purchaseInvoiceGrnID'];
        } else if ($defaultDataSet[0]['purchaseInvoicePoID'] != ''){
            $poFlag = true;
            $poID = $defaultDataSet[0]['purchaseInvoicePoID'];
        }

        $piProDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPurchaseInvoiceProductDetailsByPurchaseInvoiceID($pIID)->current();

        if ($piProDetails) {
            $grnFlag = true;
        }

        $this->beginTransaction();

        //if this pi come from grn then no need to reverse grn items. but need to check about new items that insert throuht the purchase invoice.

        if($grnFlag){
            $grnDataSet = $this->checkPiByOnlyGrn($pIID);
            $updateGrn = $this->updateGrnRelatedDetails($grnDataSet);
            if(!$updateGrn['status']){

                $this->rollback();
                $this->status = false;
                $this->msg = $updateGrn['msg'];
                return $this->JSONRespond();

            }
            //reverse existing items
            $reverseExistsItems = $this->reversePiProducts($pIID, $grnDataSet);
            if(!$reverseExistsItems['status']){
                $this->rollback();
                $this->status = false;
                $this->msg = $reverseExistsItems['msg'];
                return $this->JSONRespond();
            }

        } else {
            if($poFlag) {
                $poDataSet = $this->checkPIDataByPO($poID, $pIID);
                if(!$poDataSet['status']){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $poDataSet['msg'];
                    return $this->JSONRespond();
                }
            }
            $createdDataSet = $this->createDataSetForReverse($pIID);
            $reverseItems = $this->reversePiProducts($pIID, $createdDataSet);

            if(!$reverseItems['status']){
                $this->rollback();
                $this->status = false;
                $this->msg = $reverseItems['msg'];
                return $this->JSONRespond();
            }
        }

        // for update juornal entry records first we need to check that use accounting
            if($this->useAccounting){
                $updateJournalAccount = $this->journalEntryreverseByPI(
                        $pIID,
                        $defaultDataSet[0]['purchaseInvoiceCode'],
                        $defaultDataSet[0]['purchaseInvoiceRetrieveLocation'],
                        $dimensionData,
                        $ignoreBudgetLimit
                    );

                if(!$updateJournalAccount['status']){
                    $this->rollback();
                    $this->status =  false;
                    $this->data = $updateJournalAccount['data'];
                    if ($updateJournalAccount['data'] == "BlockBudgetLimit" || $updateJournalAccount['data'] == "NotifyBudgetLimit") {
                        $this->msg = $updateJournalAccount['msg'];                      
                    } else {
                        $this->msg = $this->getMessage('ERR_UPDATE_ACCOUNTS');
                    }
                    return $this->JSONRespond();
                }
            }

        // update supplier outstanding

            $supplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($defaultDataSet[0]['purchaseInvoiceSupplierID']);

            $currentOutStanding = floatval($supplierDetails->supplierOutstandingBalance) - floatval($defaultDataSet[0]['purchaseInvoiceTotal']);

            //update supplier
            $data = array(
                'supplierOutstandingBalance' => $currentOutStanding,
                );
            $updateSupplier = $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplier($data, $defaultDataSet[0]['purchaseInvoiceSupplierID']);

            if(!$updateSupplier && floatval($defaultDataSet[0]['purchaseInvoiceTotal']) != 0){
                $this->rollback();
                $this->status =  false;
                $this->msg =  $this->getMessage('ERR_UPDATE_SUPPLIER_OUTSTANDING_PI');
                return $this->JSONRespond();
            }

        // update pv status as a canceled. add 5 as a status id
        $statusID = 5;
        $updatePI = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->updatePIStatusForCancel($pIID, $statusID);

        if(!$updatePI){
            $this->rollback();
            $this->status =  false;
            $this->msg =  $this->getMessage('ERR_UPDATE_PI_STATE');
            return $this->JSONRespond();
        }
        //update entityTable as deleted.
        $updateEntity = $this->updateDeleteInfoEntity($defaultDataSet[0]['entityID']);
        $this->setLogMessage("Purchase Invoice ".$defaultDataSet[0]['purchaseInvoiceCode'] ." is deleted");
        $this->commit();

        $eventParameter = ['productIDs' => $this->productIds, 'locationIDs' => [$locationId]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        $this->status =  true;
        $this->msg =  $this->getMessage('SUCC_PI_CANCELED');
        return $this->JSONRespond();
    }

    private function checkPiByOnlyGrn($pIID)
    {
        $piProDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPurchaseInvoiceProductDetailsByPurchaseInvoiceID($pIID);
        $grnProductDetails = [];
        foreach ($piProDetails as $key => $value) {
            $grnProDetails = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductDetailsByGrnProductID($value['purchaseInvoiceProductDocumentID'])->current();
            $grnProductDetails[] = $grnProDetails;
        }
        $piProductDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPVProductByPVIDForDebitNote($pIID, $documentType = "Grn");
        $grnBatch = [];
        $grnSerial = [];
        $grnNormalItem = [];
        $piBatchDetails = [];
        $piSerialDetails = [];
        $piNormalItems = [];

        foreach ($grnProductDetails as $key => $grnSubValue) {
            if($grnSubValue['productSerialID'] != ''){
                $grnSerial[$grnSubValue['productSerialID']] = $grnSubValue;
            } else if($grnSubValue['productBatchID'] != '') {
                $grnBatch[$grnSubValue['productBatchID']] = $grnSubValue;
            } else {
                $grnNormalItem[$grnSubValue['grnProductID']] = $grnSubValue;
            }
        }

        foreach ($piProductDetails as $key => $piDetails) {
            if($piDetails['productSerialID'] != ''){
                if(!empty($grnSerial[$piDetails['productSerialID']])){
                    $grnSerial[$piDetails['productSerialID']]['piCanceled'] = 1;

                } else {
                    $piSerialDetails[$piDetails['productSerialID']] = $piDetails;
                }
            } else if($piDetails['productBatchID'] != ''){
                if(!empty($grnBatch[$piDetails['productBatchID']]) && $piDetails['purchaseInvoiceProductDocumentTypeID'] == 10){
                    $grnBatch[$piDetails['productBatchID']]['piCanceled'] = 1;
                    $grnBatch[$piDetails['productBatchID']]['CanceledQty'] = $piDetails['purchaseInvoiceProductQuantity'];

                } else {
                    $piBatchDetails[$piDetails['productBatchID']] = $piDetails;
                }
            } else {
                if(!empty($grnNormalItem[$piDetails['grnProductID']]) && $piDetails['purchaseInvoiceProductDocumentTypeID'] == 10 && $piDetails['purchaseInvoiceProductDocumentID'] != ''){
                    $grnNormalItem[$piDetails['grnProductID']]['piCanceled'] = 1;
                    $grnNormalItem[$piDetails['grnProductID']]['CanceledQty'] = $piDetails['purchaseInvoiceProductQuantity'];
                } else {
                    $piNormalItems[$piDetails['locationProductID']] = $piDetails;
                }
            }

        }

        $returnDataSet = array(
            'grnBatch' => $grnBatch,
            'grnSerial' => $grnSerial,
            'grnNormal' => $grnNormalItem,
            'piBatch' => $piBatchDetails,
            'piSerial' => $piSerialDetails,
            'piNormal' => $piNormalItems,
            );

        return $returnDataSet;
    }

    private function updateGrnRelatedDetails($grnDataSet)
    {
        //update serial item copy falg and copy qty
        $serialLocationProductQty = [];
        $serialLocationProductCopiedQty = [];
        $batchLoactionProductQty = [];
        $serialIDs = [];
        $batchDetails = [];
        $normalItemDetails = [];
        //set serial data set
        foreach ($grnDataSet['grnSerial'] as $key => $serialValue) {
            $this->productIds[] = $serialValue['productID'];
            if($serialValue['piCanceled'] == '1'){
                $serialIDs[] = $serialValue['productSerialID'];
                $serialLocationProductQty[$serialValue['locationProductID']] += 1;
                if ($serialValue['grnProductTotalCopiedQuantity'] != null || $serialValue['grnProductTotalCopiedQuantity'] != 0) {
                    $serialLocationProductCopiedQty[$serialValue['locationProductID']] = $serialValue['grnProductTotalCopiedQuantity'];
                }
            }
        }

        // set batch data set
        foreach ($grnDataSet['grnBatch'] as $key => $batchValue) {
            $this->productIds[] = $batchValue['productID'];
            if($batchValue['piCanceled'] == '1'){
                $batchDetails[] = $batchValue;
                $batchLoactionProductQty[$batchValue['locationProductID']] += $batchValue['CanceledQty'];
            }
        }
        //update normal item data set
        if(!empty($grnDataSet['grnNormal'])){
            foreach ($grnDataSet['grnNormal'] as $key => $normalValue) {
                $this->productIds[] = $normalValue['productID'];
                if($normalValue['piCanceled'] == '1'){
                    $data = array(
                        'grnProductCopiedQuantity' => floatval($normalValue['grnProductCopiedQuantity']) - floatval($normalValue['CanceledQty']),
                        'grnProductTotalCopiedQuantity' => $normalValue['grnProductTotalCopiedQuantity'] - $normalValue['CanceledQty'],
                        'copied' => 0
                        );
                    //update grn product table
                    $updateNormalItem = $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $normalValue['grnProductID']);
                    if(!$updateNormalItem) {
                        $respondSet = array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_GRN_PRODUCT_UPDATE'),
                            );
                        return $respondSet;
                    }
                    $openState = 3;
                    $updateGrnState = $this->CommonTable('Inventory\Model\GrnTable')->updateGrnStatus($normalValue['grnID'], $openState);

                    if(!$updateGrnState){
                        $respondSet = array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_GRN_STATUS_UPDATE'),
                        );
                        return $respondSet;
                    }
                }
            }

        }
        //update batch grn batch details
        if(!empty($batchDetails)){
            foreach ($batchDetails as $key => $batchUpdateValue) {
                $data = array(
                    'grnProductCopiedQuantity' => floatval($batchUpdateValue['grnProductCopiedQuantity']) - floatval($batchUpdateValue['CanceledQty']),
                    'grnProductTotalCopiedQuantity' =>$batchUpdateValue['grnProductTotalCopiedQuantity'] - $batchLoactionProductQty[$batchUpdateValue['locationProductID']],
                    'copied' => 0
                );
                //update grn product table
                $updateBatchItem = $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $batchUpdateValue['grnProductID']);
                if(!$updateBatchItem) {
                    $respondSet = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_GRN_PRODUCT_UPDATE'),
                        );
                    return $respondSet;
                }
                $openState = 3;
                $updateGrnState = $this->CommonTable('Inventory\Model\GrnTable')->updateGrnStatus($batchUpdateValue['grnID'], $openState);

                if(!$updateGrnState){
                    $respondSet = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_GRN_STATUS_UPDATE'),
                    );
                    return $respondSet;
                }

            }

        }

        //update serial grn data set
         if(!empty($serialIDs)){
                $updateSerialItems = $this->CommonTable('Inventory\Model\GrnProductTable')->updateSerilaProductCopyStateByCanceledPI($serialIDs);

                //update serila grn item total copied qty
                $updatedSerials = [];
                foreach ($grnDataSet['grnSerial'] as $key => $serialValue) {
                    $grnProDetails = $this->CommonTable('Inventory\Model\GrnProductTable')->getgrnProductDetailsBylocationIDAndGrnIDAndUnitPrice($serialValue['grnID'], $serialValue['locationProductID'], $serialValue['grnProductPrice']);

                    foreach ($grnProDetails as $key => $value) {
                        $newQty = $value['grnProductTotalCopiedQuantity'] - $serialLocationProductQty[$value['locationProductID']];

                        if ($newQty >= 0) {
                            if (!array_key_exists($value['grnProductID'], $updatedSerials)) {
                                $data =array(
                                    'grnProductTotalCopiedQuantity' => $newQty,
                                );
                                $updatedSerials[$value['grnProductID']] = $value['productSerialID'];
                            }
                            $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $value['grnProductID']);
                        }
                    }
                    if($serialValue['piCanceled'] == '1'){
                        $openState = 3;
                        $updateGrnState = $this->CommonTable('Inventory\Model\GrnTable')->updateGrnStatus($serialValue['grnID'], $openState);
                        if(!$updateGrnState){
                            $respondSet = array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_GRN_STATUS_UPDATE'),
                            );
                            return $respondSet;
                        }
                    }
                }
         }

        $respondSet = array(
            'status' => true,
        );

        return $respondSet;
    }

    private function checkPIDataByPO($poID, $piID)
    {
        $poDetails = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPoProductDetails($poID);
        $piProductDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPVProductByPVIDForDebitNote($piID);

        //set po details as array against location product ID
        $resultsPOArray = [];
        foreach ($poDetails as $result) {
            $resultsPOArray[$result['locationProductID']] = $result;
        }

        //set pi details to the array
        $locationProductReturnQty = [];
        $purchaseInvoiceProductIDArray = [];
        foreach ($piProductDetails as $piValue) {
            if($resultsPOArray[$piValue['locationProductID']] && $piValue['purchaseInvoiceProductDocumentTypeID'] == '9'){
                
                if(!in_array($piValue['purchaseInvoiceProductID'], $purchaseInvoiceProductIDArray)){
                    $purchaseInvoiceProductIDArray[] = $piValue['purchaseInvoiceProductID'];
                    $resultsPOArray[$piValue['locationProductID']]['canceled'] = 1;
                    $locationProductReturnQty[$piValue['locationProductID']] += $piValue['purchaseInvoiceProductQuantity'];
                }
            }
        }
        $poOpenFlag = false;
        // update po details
        foreach ($resultsPOArray as $singleDataSet) {
            
            if($singleDataSet['canceled'] == '1'){
                $data = array(
                    'purchaseOrderProductID' => $singleDataSet['purchaseOrderProductID'],
                    'copied' => '0',
                    'purchaseOrderProductCopiedQuantity' => $singleDataSet['purchaseOrderProductCopiedQuantity'] - $locationProductReturnQty[$singleDataSet['locationProductID']],
                    );
                $updatePo = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->updatePoCopiedStateAndCopiedQty($data);

                if(!$updatePo){
                    $poOpenFlag = false;
                    $returnPoState = array(
                        'msg' => $this->getMessage('ERR_PO_UPDATE'),
                        'status' => false,
                        );
                    return $returnPoState;
                } else {
                    $poOpenFlag = true;
                }
            }

        }
        //update PO state
        $poStatus = 3; // open state
        $updateState = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->updatePoStatus($poID, $poStatus);

        $returnPoState = array(
            'status' => true,
            );
        return $returnPoState;
    }

    private function createDataSetForReverse($pIID)
    {
        $piProductDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPVProductByPVIDForDebitNote($pIID);
        $piBatchDetails = [];
        $piSerialDetails = [];
        $piNormalItems = [];
        foreach ($piProductDetails as $key => $piDetails) {
           if($piDetails['productSerialID'] != ''){
                $piSerialDetails[$piDetails['productSerialID']] = $piDetails;

            } else if($piDetails['productBatchID'] != ''){
                $piBatchDetails[$piDetails['productBatchID']] = $piDetails;

            } else {
                $piNormalItems[$piDetails['purchaseInvoiceProductID']] = $piDetails;

            }
        }
      
        $returnDataSet = array(
            'piBatch' => $piBatchDetails,
            'piSerial' => $piSerialDetails,
            'piNormal' => $piNormalItems,
            );
        return $returnDataSet;
    }

    private function reversePiProducts($pIID, $dataSet)
    {
        $docType = 'Payment Voucher';
        // reverse serial products
        if(sizeof($dataSet['piSerial']) > 0){
            $batchSerialQty = [];
            foreach ($dataSet['piSerial'] as $key => $serialDataSet) {
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID(NULL, $serialDataSet['productSerialID']);
                $serialSoldFlag = false;

                $this->productIds[] = $serialDataSet['productID'];

                foreach ($itemInData as $key => $serialDta) {
                    if($serialDta['itemInSoldQty'] == 0){
                        if($serialDta['itemInBatchID'] != ''){
                            $batchSerialQty[$serialDta['itemInBatchID']] ++;
                        }
                        $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($serialDataSet['productID'], $serialDataSet['locationID'])->current();
                        $returnQty = '1';
                        $serialSoldFlag = true;
                        //update locationproduct ItemIn and ItemOut
                        $locProStatue = $this->updateLocationProductAndItemInAndItemOut($pIID, $locationProductDetails, $serialDta, $returnQty);
                        if($locProStatue['status'] != 'true'){
                            $errorData = array(
                                'msg' => $locProStatue['msg'],
                                'status' => false,
                            );
                            return $errorData;
                        }

                        //update serial table
                        $data = array(
                            'productSerialReturned' => '1',
                            );

                        $updateSerialTable = $this->CommonTable('Inventory\Model\ProductSerialTable')
                                ->updateProductSerialData($data, $serialDta['itemInSerialID']);


                    }
                }
                if(!$serialSoldFlag){
                    $errorData = array(
                        'msg' => $this->getMessage('ERR_SERIAL_PRODUCT_IN_USE'),
                        'status' => false,
                        );
                    return $errorData;
                }

            }
            //update batch-serilaItem batch table
            if(!empty($batchSerialQty)){
                foreach ($batchSerialQty as $key => $value) {
                    $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($key);
                    $newBatchQty = floatval($batchDetails->productBatchQuantity) - floatval($value);

                    //update batchTable
                    $data = array(
                        'productBatchQuantity' => $newBatchQty,
                        );
                    $update = $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($key, $data);
                    if(!$update){
                        $errorData = array(
                            'msg' => $this->getMessage('ERR_SERIAL_BATCH_PRODUCT_UPDATE'),
                            'status' => false,
                        );
                        return $errorData;
                    }
                }
            }

        }
        //reverse batch products
        if(sizeof($dataSet['piBatch']) > 0) {

            foreach ($dataSet['piBatch'] as $key => $batchValue) {
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID($batchValue['productBatchID'],null);

                $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($batchValue['productID'], $batchValue['locationID'])->current();
                $totalUpdatedQty = 0;

                $this->productIds[] = $batchValue['productID'];

                foreach ($itemInData as $key => $itemInBatchValue) {

                    $returnQty = floatval($itemInBatchValue['itemInQty']) - floatval($itemInBatchValue['itemInSoldQty']);
                    $totalUpdatedQty += $returnQty;
                    //update locationproduct ItemIn and ItemOut
                    $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($pIID, $locationProductDetails, $itemInBatchValue, $returnQty);
                    if($locUpdateStatus['status'] != 'true'){
                        $errorData = array(
                            'msg' => $locUpdateStatus['msg'],
                            'status' => false,
                        );
                        return $errorData;
                    }

                    //update batch table
                    if($batchValue['itemInSerialID'] == ''){
                        $data = array(
                            'productBatchQuantity' => '0',
                        );
                        $updateBatchTable = $this->CommonTable('Inventory\Model\ProductBatchTable')
                                   ->updateProductBatchQuantity($itemInBatchValue['itemInBatchID'], $data);
                    }

                }
                if(floatval($totalUpdatedQty) != floatval($batchValue['purchaseInvoiceProductQuantity'])){
                    $errorData = array(
                        'msg' => $this->getMessage('ERR_BATCH_PRODUCT_IN_USE'),
                        'status' => false,
                        );
                    return $errorData;

                }

            }

        }
        //reverse normal products
        if(sizeof($dataSet['piNormal']) > 0){

            $exisistPiDetails = [];
            $itemInIds =[];
            foreach ($dataSet['piNormal'] as $key => $normalDataSet) {
                if(empty($exisistPiDetails[$normalDataSet['purchaseInvoiceProductID']])){
                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getProductDetailsBylocationProductIDDocTypeAndDocumentID($normalDataSet['locationProductID'], $docType, $normalDataSet['purchaseInvoiceID']);

                    $this->productIds[] = $normalDataSet['productID'];

                    foreach ($itemInDetails as $key => $itemInSingleValue) {
                        if(empty($itemInIds[$itemInSingleValue['itemInID']])) {
                            if($itemInSingleValue['itemInSoldQty'] == 0){
                                $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($normalDataSet['productID'], $normalDataSet['locationID'])->current();

                                $returnQty = floatval($itemInSingleValue['itemInQty']);
                                //update locationproduct ItemIn and ItemOut
                                $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($pIID, $locationProductDetails, $itemInSingleValue, $returnQty);
                                if($locUpdateStatus['status'] != 'true'){
                                    $errorData = array(
                                        'msg' => $locUpdateStatus['msg'],
                                        'status' => false,
                                    );
                                    return $errorData;
                                }
                            } else {
                               $errorData = array(
                                    'msg' => $this->getMessage('ERR_NORMAL_PRODUCT_IN_USE'),
                                    'status' => false,
                                );
                                return $errorData;
                            }
                        }
                        $itemInIds[$itemInSingleValue['itemInID']] = $itemInSingleValue;
                    }
                    // die();
                    // validate location product update or not
                    $exisistPiDetails[$normalDataSet['purchaseInvoiceProductID']] = $normalDataSet;

                }
            }
        }
        $errorData = array(
            'status' => true,
        );
        return $errorData;


    }

    private function updateLocationProductAndItemInAndItemOut($pIID, $locationProductDetails, $productDataSet, $returnQty)
    {
        if($returnQty != 0){
            $outData = array(
                'itemOutDocumentType' => 'purchase Invoice cancel',
                'itemOutDocumentID' => $pIID,
                'itemOutLocationProductID' => $locationProductDetails['locationProductID'],
                'itemOutSerialID' => $productDataSet['itemInSerialID'],
                'itemOutItemInID' => $productDataSet['itemInID'],
                'itemOutQty' => $returnQty,
                'itemOutPrice' => $productDataSet['itemInPrice'],
                'itemOutAverageCostingPrice' => $locationProductDetails['locationProductAverageCostingPrice'],
                'itemOutDiscount' => $productDataSet['itemInDiscount'],
                'itemOutDateAndTime' => $this->getGMTDateTime(),
                'itemOutDeletedFlag' => 1,
            );
            //add record to the item out
            $itemOutM = new ItemOut();
            $itemOutM->exchangeArray($outData);

            $itemOutState = $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
            if($itemOutState == ''){
                $updateLocRespondData = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_ADD_ITEM_OUT'),
                    );
                return $updateLocRespondData;
            }
            //update itemIn table as all items sold.
            $updatedItemInDetails = array(
                'itemInSoldQty' => $returnQty,
                'itemInDeletedFlag' => 1,
            );
            $itemInUpdateStatue = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($updatedItemInDetails, $productDataSet['itemInID']);

            if(!$itemInUpdateStatue){
                $updateLocRespondData = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_UPDATE_ITEM_IN'),
                    );
                return $updateLocRespondData;
            }

            $locationProductQuantity = floatval($locationProductDetails['locationProductQuantity']) - floatval($returnQty);
            //set data to update locationProduct
            $locationProductUpdatedData = array(
                'locationProductQuantity' => $locationProductQuantity
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $productDataSet['itemInLocationProductID']);

            if(!$updateLocPro){
                $updateResData = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_UPDATE_ITEM_IN'),
                    );
                return $updateResData;
            }

             // calculate new avarageCosting
            $updateAverageCosting = $this->calculateAvgCostAndOtherDetails($locationProductDetails, $productDataSet);
            if(!$updateAverageCosting['status']){
                $this->rollback();
                $returnBatchSerialState = array(
                    'msg' => $this->getMessage('ERR_UPDATE_AVERAGE_COSTING'),
                    'status' => false, 
                );
                return $returnBatchSerialState;
               
            } 
        }

        $updateLocRespondData = array(
            'status' => true,
        );
        return $updateLocRespondData;

    }

    private function journalEntryreverseByPI($purchaseInvoiceID, $pICode, $locationID, $dimensionData, $ignoreBudgetLimit = true)
    {
        /**
        * get journal entry details by given id and document id =12
        * because purchase invoice document id is 12
        */

        $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('12', $purchaseInvoiceID);
        $journalEntryID = $journalEntryData['journalEntryID'];
       
        $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);


        $i=0;
        $journalEntryAccounts = array();
        foreach ($jEAccounts as $key => $value) {
            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
            $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Invoice Delete '.$pICode.'.';
            $i++;
        }

        //get journal entry reference number.
        $jeresult = $this->getReferenceNoForLocation('30', $locationID);
        $jelocationReferenceID = $jeresult['locRefID'];
        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

        $journalEntryData = array(
            'journalEntryAccounts' => $journalEntryAccounts,
            'journalEntryDate' => $this->convertDateToStandardFormat($journalEntryData['journalEntryDate']),
            'journalEntryCode' => $JournalEntryCode,
            'journalEntryTypeID' => '',
            'journalEntryIsReverse' => 0,
            'journalEntryComment' => 'Journal Entry is posted when cancel Purchase Invoice '.$pICode.'.',
            'documentTypeID' => 12,
            'journalEntryDocumentID' => $purchaseInvoiceID,
            'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

        $resultData = $this->saveJournalEntry($journalEntryData);

        if(!$resultData['status']){
           return ['status' => false, 'msg' => $resultData['msg'], 'data' => $resultData['data']];

        }

        $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($purchaseInvoiceID,12, 5);
        if(!$jEDocStatusUpdate['status']){
            return ['status' => false, 'msg' => $jEDocStatusUpdate['msg'], 'data' => $jEDocStatusUpdate['data']];
        }

        if (!is_null($dimensionData)) {
            foreach ($dimensionData as $value) {
                if (!empty($value)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($value, $resultData['data']['journalEntryID'],$ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                    if(!$saveRes['status']){
                        return ['status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']];
                    }   
                }
            }
        }

        return [
            'status' => true,
            'oldJEID' => $journalEntryID,
            'newJEID' => $resultData['data']['journalEntryID']
        ];

    }

    public function getDataForUpdateAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $pIID = $request->getPost('pIID');
        $dataSet = $this->getPiDataSet($pIID, $isUnconvertedUom = true);
        $this->data = $dataSet;
        $this->status = true;
        return $this->JSONRespond();
    }


    public function getPiDataSet($pIID, $isUnconvertedUom = false)
    {
        $data = $this->getDataForDocumentView(); // get comapny details
        $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPIsByPIID($pIID);
        $piDetails = array();
        $piProductArray = [];
        $piCode = "";
        foreach ($piData as $row) {
            $piProductArray[] = $row['productID'];
            $tempG = array();
            $tempG['piID'] = $row['purchaseInvoiceID'];
            $tempG['piCd'] = $row['purchaseInvoiceCode'];
            $piCode = $row['purchaseInvoiceCode'];
            $tempG['piSupID'] = $row['purchaseInvoiceSupplierID'];
            $tempG['locID'] = $row['purchaseInvoiceRetrieveLocation'];
            $tempG['piSName'] = $row['supplierTitle'] . ' ' . $row['supplierName'];
            $tempG['piSAddress'] = $row['supplierAddress'];
            $tempG['piSEmail'] = $row['supplierEmail'];
            $tempG['piSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
            $tempG['piSR'] = $row['purchaseInvoiceSupplierReference'];
            $tempG['piRL'] = $row['locationCode'] . '-' . $row['locationName'];
            $tempG['piIssueD'] = $this->convertDateToUserFormat($row['purchaseInvoiceIssueDate']);
            $tempG['piDD'] = $this->convertDateToUserFormat($row['purchaseInvoicePaymentDueDate']);
            $tempG['piDC'] = $row['purchaseInvoiceDeliveryCharge'];
            $tempG['piT'] = ($row['purchaseInvoiceTotal']);
            $tempG['piPayedAmo'] = $row['purchaseInvoicePayedAmount'];
            $tempG['piSTax'] = $row['purchaseInvoiceShowTax'];
            $tempG['piC'] = $row['purchaseInvoiceComment'];
            $tempG['piSt'] = $row['status'];
            $tempG['piPTN'] = $row['paymentTermName'];
            $tempG['userUsername'] = $row['userUsername'];
            $tempG['createdTimeStamp'] = $this->getUserDateTime($row['createdTimeStamp']);
            $tempG['purchaseInvoiceProductDocumentTypeID'] = $row['purchaseInvoiceProductDocumentTypeID'];
            $tempG['purchaseInvoiceProductDocumentID'] = $row['purchaseInvoiceProductDocumentID'];
            $tempG['grnID'] = $row['purchaseInvoiceGrnID'];
            $tempG['poID'] = $row['purchaseInvoicePoID'];
            $tempG['piWTD'] = $row['purchaseInvoiceWiseTotalDiscount'];
            $tempG['piWTDT'] = $row['purchaseInvoiceWiseTotalDiscountType'];
            $tempG['piWTDR'] = $row['purchaseInvoiceWiseTotalDiscountRate'];
            
            $piProducts = (isset($piDetails[$row['purchaseInvoiceID']]['piProducts'])) ? $piDetails[$row['purchaseInvoiceID']]['piProducts'] : array();
            if ($row['purchaseInvoiceProductID'] != NULL) {
                if ($row['purchaseInvoiceProductDocumentTypeID'] == "10" && $row['grnID'] != null) {
                    $productKey = $row['productCode'] . '-' . $row['purchaseInvoiceProductPrice'].'-'.$row['purchaseInvoiceProductDiscount']. '-'. $row['grnID'];
                } else {
                    $productKey = $row['productCode'] . '-' . $row['purchaseInvoiceProductPrice'].'-'.$row['purchaseInvoiceProductDiscount'];
                }
                $productTaxes = (isset($piProducts[$productKey]['pT'])) ? $piProducts[$productKey]['pT'] : array();
                $productBatches = (isset($piProducts[$productKey]['bP'])) ? $piProducts[$productKey]['bP'] : array();
                $productSerials = (isset($piProducts[$productKey]['sP'])) ? $piProducts[$productKey]['sP'] : array();
                $productBatchSerials = (isset($piProducts[$productKey]['sP']) && isset($piProducts[$productKey]['bP'])) ? $piProducts[$productKey]['bSP'] : array();

                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                // $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['purchaseInvoiceProductPrice'], $productUom);
                // $thisqty = $this->getProductQuantityViaDisplayUom($row['purchaseInvoiceProductTotalQty'], $productUom);
                // $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                $unitPrice = $row['purchaseInvoiceProductPrice'];
                $thisqty = [];
                $thisqty['quantity'] = ($row['purchaseInvoiceProductTotalQty'] == 0) ? '-' : $row['purchaseInvoiceProductTotalQty'];

                if (!empty($row['purchaseInvoiceProductSelectedUomId']) && $isUnconvertedUom) {
                    $selectedUomData = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomByProductID($row['productID'], $row['purchaseInvoiceProductSelectedUomId'])->current();

                    $selectedUom = $row['purchaseInvoiceProductSelectedUomId'];
                    $selectedUomAbbr = $selectedUomData['uomAbbr'];

                    $qty = round((floatval($row['purchaseInvoiceProductTotalQty'])), 10);
                    $qty = number_format($qty, $this->getNumberOfDecimalPoints($qty), '.', '');

                    $price = round((floatval($row['purchaseInvoiceProductPrice'])), 10);
                    $price = number_format($price, $this->getNumberOfDecimalPoints($price), '.', '');
                } else {
                    $selectedUom = $thisqty['uomID'];
                    $selectedUomAbbr = $thisqty['uomAbbr'];
                    $qty = $thisqty['quantity'];
                    $price = $unitPrice;
                }

                // if ($isUnconvertedUom) {
                //     $qty = round((floatval($row['purchaseInvoiceProductTotalQty'])), 10);
                //     $qty = number_format($qty, $this->getNumberOfDecimalPoints($qty), '.', '');

                //     $price = round((floatval($row['purchaseInvoiceProductPrice'])), 10);
                //     $price = number_format($price, $this->getNumberOfDecimalPoints($price), '.', '');
                // } else {
                //     $qty = $thisqty['quantity'];
                //     $price = $unitPrice;
                // }



                $piProducts[$productKey] = array('piPC' => $row['productCode'], 'piPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'piPP' => $price, 'piPD' => $row['purchaseInvoiceProductDiscount'], 'piPQ' => $qty, 'piPT' => $row['purchaseInvoiceProductTotal'], 'piUomID' => $selectedUom, 'piPUom' => $selectedUomAbbr,'piPID' => $row['productID'], 'piPTypeID' => $row['productTypeID'], 'pIProductID' => $row['purchaseInvoiceProductID'], 'copyDocType' => $row['purchaseInvoiceProductDocumentTypeID'], 'copyDocID' => $row['purchaseInvoiceProductDocumentID']);
                if ($row['productBatchID'] != NULL && $row['productSerialID'] == NULL) {
                    $productBatches[$row['productBatchID']] = array('bID' => $row['productBatchID'], 'bC' => $row['productBatchCode'], 'bED' => $this->convertDateToUserFormat($row['productBatchExpiryDate']), 'bW' => $row['productBatchWarrantyPeriod'], 'bMD' => $this->convertDateToUserFormat($row['productBatchManufactureDate']), 'bQ' => $row['productBatchQuantity']);
                }
                if ($row['productSerialID'] != NULL && $row['productBatchID'] == NULL) {
                    $productSerials[$row['productSerialID']] = array('sID' => $row['productSerialID'], 'sC' => $row['productSerialCode'], 'sW' => $row['productSerialWarrantyPeriod'], 'sED' => $this->convertDateToUserFormat($row['productSerialExpireDate']), 'sBC' => $row['productBatchCode'],'sW' => $row['productSerialWarrantyPeriod'],'sWT' => $row['productSerialWarrantyPeriodType']);
                }
                if ($row['productSerialID'] != NULL && $row['productBatchID'] != NULL) {
                    $productBatchSerials[$row['productSerialID']] = array('sID' => $row['productSerialID'], 'sC' => $row['productSerialCode'], 'sW' => $row['productSerialWarrantyPeriod'], 'sED' => $this->convertDateToUserFormat($row['productSerialExpireDate']), 'sBC' => $row['productBatchCode'], 'bC' => $row['productBatchCode'], 'bID'=> $row['productBatchID']);
                }
                if ($row['purchaseInvoiceTaxID'] != NULL) {
                    $productTaxes[$row['purchaseInvoiceTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['purchaseInvoiceTaxPrecentage'], 'pTA' => $row['purchaseInvoiceTaxAmount'], 'oTID' => $row['purchaseInvoiceTaxID']);
                }
                $piProducts[$productKey]['pT'] = $productTaxes;
                $piProducts[$productKey]['bP'] = $productBatches;
                $piProducts[$productKey]['sP'] = $productSerials;
                $piProducts[$productKey]['bSP'] = $productBatchSerials;
            }
            $tempG['piProducts'] = $piProducts;
            $piDetails[$row['purchaseInvoiceID']] = $tempG;
        }
        $originalTaxDetails = null;
        if (!empty($piProductArray)) {
            $originalTaxDetails = $this->getSelectedItemTaxDetails($piProductArray);
        }
        $piSubTotal = (floatval($piDetails[$pIID]['piT']) - floatval($piDetails[$pIID]['piDC']));
        $totalProTaxes = array();
        foreach ($piDetails[$pIID]['piProducts'] as $product) {
            foreach ($product['pT'] as $tKey => $proTax) {
                if (isset($totalProTaxes[$tKey])) {
                    $totalProTaxes[$tKey]['pTA'] = (floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA']));
                } else {
                    $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                }
            }
        }
        $piDetails[$pIID]['piSubTotal'] = $piSubTotal;
        $piDetails[$pIID]['piTotalTax'] = $totalProTaxes;

        $purchaseInvoiceMethodData = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentsDetailsByPurchaseInvoiceID($piID);
        $pvMethod="";
        foreach ($purchaseInvoiceMethodData as $key => $value) {
            if($value['paymentMethodID']==2){
                $pvMethod.=$value['outGoingPaymentMethodReferenceNumber']."(".$value['bankName'].")"."\r\n";
            }
        }
        $piDetails[$piID]['check_and_bank'] = $pvMethod;

        $locationProducts = Array();
        $inactiveItems = Array();
        $locationID = $piDetails[$pIID]['locID'];
        foreach ($piDetails[$pIID]['piProducts'] as $piProducts) {
            $locationProducts[$piProducts['piPID']] = $this->getLocationProductDetails($locationID, $piProducts['piPID']);
            if ($locationProducts[$piProducts['piPID']] == null) {
                $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($piProducts['piPID']);
                $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
            }
        }
        $inactiveItemFlag = false;
        $errorMsg = null;
        if (!empty($inactiveItems)) {
            $inactiveItemFlag = true;
            $errorItems = implode(",",$inactiveItems);        
            $errorMsg = $this->getMessage('ERR_PI_ITM_INACTIVE', [$errorItems]);
        }
        
        $data = array_merge($data, $piDetails[$pIID]);
        $data['today_date'] = gmdate('Y-m-d');
        $data['currencySymbol'] = $this->companyCurrencySymbol;
        $docRefData = $this->getDocumentReferenceBySourceData(12, $pIID);

        $dimensionData = [];
        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(12,$pIID);
        foreach ($jEDimensionData as $value) {
            if (!is_null($value['journalEntryID'])) {
                $temp = [];
                if($value['dimensionType'] == "job" || $value['dimensionType'] == "project") {
                    if ($value['dimensionType'] == "job") {
                        $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($value['dimensionValueID']);
                        $valueTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                        $dimensionTxt = "Job";
                    } else if ($value['dimensionType'] == "project") {
                        $proData = $this->CommonTable('Jobs\Model\ProjectTable')->getProjectByProjectId($value['dimensionValueID']);
                        $valueTxt = $proData->projectCode.'-'.$proData->projectName;
                        $dimensionTxt = "Project";
                    }

                    $temp['dimensionTypeId'] = $value['dimensionType'];
                    $temp['dimensionTypeTxt'] = $dimensionTxt;
                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                    $temp['dimensionValueTxt'] = $valueTxt;
                } else {
                    $temp['dimensionTypeId'] = $value['dimensionType'];
                    $temp['dimensionTypeTxt'] = $value['dimensionName'];
                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                    $temp['dimensionValueTxt'] = $value['dimensionValue'];
                }
                $dimensionData[$piCode][$value['dimensionType']] = $temp;
            }
        }

        $uploadedAttachments = [];
        $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($pIID, 12);
        $companyName = $this->getSubdomain();
        $path = '/userfiles/' . md5($companyName) . "/attachments";

        foreach ($attachmentData as $value) {
            $temp = [];
            $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
            $temp['documentRealName'] = $value['documentRealName'];
            $temp['docLink'] = $path . "/" .$value['documentName'];
            $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
        }

        $returnDataSet = array(
            'piDetails' => $data,
            'locationProDetails' => $locationProducts,
            'inactiveItemFlag' => $inactiveItemFlag,
            'docRefData' => $docRefData,
            'errorMsg' => $errorMsg, 
            'originalTaxDetails' => $originalTaxDetails,
            'dimensionData' => $dimensionData,
            'uploadedAttachments' => $uploadedAttachments
            );

        return $returnDataSet;
    }

    public function getSelectedItemTaxDetails($products)
    {
        // get item wise tax details
        $taxDetails = $this->CommonTable('Inventory\Model\ProductTaxTable')->getProductTaxDetailsByProductIDList($products);
        $itemTaxDetails = [];
        foreach ($taxDetails as $key => $value) {
            $itemTaxDetails[$value['productID']][$value['taxID']] = $value;
            
        }
        return $itemTaxDetails; 
    }

    public function updatePiAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $data = $request->getPost();

        //get pi existing dataset for checking
        $dataSet = $this->getPiDataSet($data['efectedPiID']);

        $purchaseInvoiceData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataByID($data['efectedPiID']);

        // check whether the pi used to create debitNote
        $debitNote = $this->CommonTable("Inventory\Model\DebitNoteTable")->getDebitNotesByPvID($data['efectedPiID']);
        if ($debitNote->current() != NULL) {
            $this->status = false;
            $this->msg = 'This Purchase invoice has a debitNote.So you can not edit this Purchase invoice..!';
            return $this->JSONRespond();
        } 
        
        $locationId = $dataSet['piDetails']['locID']; // get location

        $updateFlag = false;
        if(sizeof(array_filter($dataSet['piDetails']['piProducts'])) != sizeof(array_filter($data['pr']))){
            $updateFlag = true;
        } else {
            $locationProMatchFlag = false;
            $itemCounter = 0;

            foreach ($dataSet['piDetails']['piProducts'] as $key => $existingvalue) {
                foreach (array_filter($data['pr']) as $key => $newValue) {
                    if($newValue['locationPID'] == $existingvalue['lPID']){
                        $locationProMatchFlag = true;
                        if($newValue['pQuantity'] == $existingvalue['piPQ'] && $newValue['pUnitPrice'] == $existingvalue['piPP'] &&
                            $newValue['pDiscount'] == $existingvalue['piPD'] && $newValue['pTotal'] == $existingvalue['piPT']){
                            $itemCounter++;
                        } else {
                            $updateFlag = true;
                        }
                    }
                }
            }

        }
        if(!$locationProMatchFlag){
            $updateFlag = true;
        }
        if(sizeof(array_filter($dataSet['piDetails']['piProducts'])) != $itemCounter){
            $updateFlag = true;
        }
        if (floatval($data['dC']) != floatval($purchaseInvoiceData['purchaseInvoiceDeliveryCharge'])) {
            $updateFlag = true;
        }
        $checkCopyGrnIds = [];
        foreach ($dataSet['piDetails']['piProducts'] as $ke => $existingval) {
            if ($existingval['copyDocType'] == "10") {
                $checkPiFromMultipleGrn = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPurchaseInvoiceDetailsByPurchaseInvoiceProductID($existingval['pIProductID'])->current();
                if ($checkPiFromMultipleGrn['purchaseInvoiceGrnID'] == null) {
                    $checkCopyGrnIds[] = $existingval['copyDocID'];
                }
            }
        }

        if ($updateFlag == false) {
            $this->beginTransaction();
            // need to cancel existing pi and update pi table as replace and add new records.
            $returnData = $this->piCancelForUpdate($data['efectedPiID'], $data['dimensionData'], $data['ignoreBudgetLimit']);
            if (!$returnData['status']) {
                $this->rollback();
                $this->status = false;
                $this->msg = $returnData['msg'];
                $this->data = $returnData['data'];
                return $this->JSONRespond();
            }

            $updated = $this->updatePurchaseInvoie($data, $dataSet,$data['efectedPiID']);
            if (!$updated['status']) {
                $this->rollback();
                $this->msg = $updated['msg'];
                $this->data = $updated['data'];
                $this->status = false;
                return $this->JSONRespond();
            } else {
                $changeArray = $this->getPurchaseInvoiceChageDetails($data);

                //set log details
                $previousData = json_encode($changeArray['previousData']);
                $newData = json_encode($changeArray['newData']);

                $this->setLogDetailsArray($previousData,$newData);
                $this->setLogMessage("Purchase Invoice ".$changeArray['previousData']['Purchase Invoice Code']." is updated.");
                $eventParameter = ['productIDs' => $this->productIds, 'locationIDs' => [$locationId]];
                $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);
                $this->commit();
                $this->data = $updated['data'];
                $this->msg = $updated['msg'];
                $this->status = true;
                return $this->JSONRespond();
            }

        } else {
            //update only purchaseInvoice table records
            $this->beginTransaction();

            $row = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPIsByPIID($data['efectedPiID'])->current();
            $previousData = [];
            $previousData['Purchase Invoice Code'] = $row['purchaseInvoiceCode'];
            $previousData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
            $previousData['Purchase Invoice Supplier Reference'] = $row['purchaseInvoiceSupplierReference'];
            $previousData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
            $previousData['Purchase Invoice Payment Due Date'] = $this->convertDateToUserFormat($row['purchaseInvoicePaymentDueDate']);
            $previousData['Purchase Invoice Date'] = $this->convertDateToUserFormat($row['purchaseInvoiceIssueDate']);
            $previousData['Purchase Invoice Delivery Charge'] = $row['purchaseInvoiceDeliveryCharge'];
            $previousData['Purchase Invoice Total'] = number_format($row['purchaseInvoiceTotal'],2);
            $previousData['Purchase Invoice Comment'] = $row['purchaseInvoiceComment'];


            $piData = array(
                'purchaseInvoiceSupplierReference' => $data['sR'],
                'purchaseInvoicePaymentDueDate' => $this->convertDateToStandardFormat($data['dD']),
                'purchaseInvoiceIssueDate' => $this->convertDateToStandardFormat($data['iD']),
                'purchaseInvoiceComment' => $data['cm'],
                'purchaseInvoiceDeliveryCharge' => (!empty($data['dC'])) ? $data['dC'] : 0,
                'paymentTermID' => $data['pT'],

            );
            $updatePiTable = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->updatePiDetails($piData, $data['efectedPiID']);
            if(!$updatePiTable){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_UPDATE_PI');
                return $this->JSONRespond();
            }

            if ($data['journalEntryData']) {
                // update journal Entry details
                $jEUpdates = $this->updateEditedPiJournalEntries($data, $dataSet, $data['efectedPiID']);

                if (!$jEUpdates['status']) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $jEUpdates['msg'];
                    $this->data = $jEUpdates['data'];
                    return $this->JSONRespond(); 
                }
                
            }

            if (is_null($data['journalEntryData'])) {
                //reverse journal entries
                if($this->useAccounting){
                    /**
                    * get journal entry details by given id and document id = 12
                    * because grn document id is 12
                    */
                    $jeData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('12', $data['efectedPiID']);
                    $jeNewData['journalEntryDate'] = $this->convertDateToStandardFormat($data['iD']);
                    $piDate = $jeNewData['journalEntryDate'];


                    $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();

                    $checkFiscalPeriod = false;
                    if(count($fiscalPeriod) > 0){
                        foreach ($fiscalPeriod as $key => $value) {
                            if($value['fiscalPeriodStatusID'] == 14){
                                if(($value['fiscalPeriodStartDate'] <= $piDate && $piDate <= $value['fiscalPeriodEndDate'])){
                                    $checkFiscalPeriod = true;
                                }
                            }
                        }
                    }
                    if (!$checkFiscalPeriod) {
                        $this->status = false;
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_DATE_IS_NOTIN_FISCALPERIOD_RANGE');
                        return $this->JSONRespond();
                    }

                    $jeUpdated = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntry($jeNewData, $jeData['journalEntryID']);
                    if(!$jeUpdated){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_UPDATE_PI');
                        return $this->JSONRespond();
                    }
                }
            }

            $documentReference = $data['documentReference'];
            $piID = $data['efectedPiID'];

            $this->CommonTable('Core\model\DocumentReferenceTable')->deleteDocumentReference(12, $piID);
            if (isset($documentReference)) {
                foreach ($documentReference as $key => $val) {
                    foreach ($val as $key1 => $value) {
                        $data = array(
                            'sourceDocumentTypeId' => 12,
                            'sourceDocumentId' => $piID,
                            'referenceDocumentTypeId' => $key,
                            'referenceDocumentId' => $value,
                        );
                        $documentReference = new DocumentReference();
                        $documentReference->exchangeArray($data);
                        $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                    }
                }
            }

            $changeArray = $this->getPurchaseInvoiceChageDetails($data);

            //set log details
            $piCode = $previousData['Purchase Invoice Code'];
            $previousData = json_encode($previousData);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("Purchase Invoice ". $piCode ." is updated.");

            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_UPDATE_PI');
            return $this->JSONRespond();
        }


    }

    public function getPurchaseInvoiceChageDetails($data)
    {
        $row = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPIsByPIID($data['efectedPiID'])->current();
        $previousData = [];
        $previousData['Purchase Invoice Code'] = $row['purchaseInvoiceCode'];
        $previousData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
        $previousData['Purchase Invoice Supplier Reference'] = $row['purchaseInvoiceSupplierReference'];
        $previousData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $previousData['Purchase Invoice Payment Due Date'] = $this->convertDateToUserFormat($row['purchaseInvoicePaymentDueDate']);
        $previousData['Purchase Invoice Date'] = $this->convertDateToUserFormat($row['purchaseInvoiceIssueDate']);
        $previousData['Purchase Invoice Delivery Charge'] = $row['purchaseInvoiceDeliveryCharge'];
        $previousData['Purchase Invoice Total'] = number_format($row['purchaseInvoiceTotal'],2);
        $previousData['Purchase Invoice Comment'] = $row['purchaseInvoiceComment'];

        $newData = [];
        $newData['Purchase Invoice Code'] = $data['piC'];
        $newData['Supplier'] = $row['supplierCode'] . '-' . $row['supplierName'];
        $newData['Purchase Invoice Supplier Reference'] = $data['sR'];
        $newData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $newData['Purchase Invoice Payment Due Date'] = $this->convertDateToUserFormat($data['dD']);
        $newData['Purchase Invoice Date'] = $this->convertDateToUserFormat($data['iD']);
        $newData['Purchase Invoice Delivery Charge'] = $data['dC'];
        $newData['Purchase Invoice Total'] = number_format($data['fT'],2);
        $newData['Purchase Invoice Comment'] = $data['cm'];

        return array('previousData' => $previousData, 'newData' => $newData);
    }

    /**
    * this function use to update journal entry records when only edit journal entry accounts in purchase invoice edit process
    * @param array newProductList
    * @param array existingProductList
    * @param int purchaseInvoiceID
    * return array
    **/
    public function updateEditedPiJournalEntries($newProductList,$existingProductList,$purchaseInvoiceID)
    {
        if($this->useAccounting){
            // need to reverse journal entries
            $updateJournalAccount = $this->journalEntryreverseByPI(
                    $purchaseInvoiceID,
                    $newProductList['piC'],
                    $newProductList['rL'],
                    [],
                    $newProductList['ignoreBudgetLimit']
                );

            if(!$updateJournalAccount['status']){
                return array(
                    'status' => false,
                    'data' => $updateJournalAccount['data'],
                    'msg' => ($updateJournalAccount['data'] == "BlockBudgetLimit" || $updateJournalAccount['data'] == "NotifyBudgetLimit") ? $updateJournalAccount['msg']: $this->getMessage('ERR_UPDATE_ACCOUNTS'),
                );
            }
            // update existing JE records
            $updateJE = $this->updatePreviouseJEAsDeleted($updateJournalAccount);
            
            // create new journal enties
            $jeResult = $this->saveJournalEntryDataForPI($purchaseInvoiceID, $newProductList['piC'], $newProductList['sID'], $newProductList['pr'], $newProductList['dC'], $newProductList['fT'], $newProductList['iD'], '', true, $newProductList['journalEntryData'], 4, $newProductList['dimensionData'], $newProductList['ignoreBudgetLimit']);
            if (!$jeResult['status']) {
                return array(
                    'status' => false,
                    'data' => $jeResult['data'],
                    'msg' => ($jeResult['data'] == "BlockBudgetLimit" || $jeResult['data'] == "NotifyBudgetLimit") ? $jeResult['msg']: $this->getMessage('ERR_UPDATE_ACCOUNTS'),
                );
            }
            return array(
                'status' => true,
            );  
            
        }
    }


    public function updatePreviouseJEAsDeleted($data)
    {
        $jeArray[0] = $data['oldJEID'];
        $jeArray[1] = $data['newJEID'];
        $getJEDetails = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryIDs($jeArray);
        foreach ($getJEDetails as $key => $value) {
            $updateJE = $this->updateDeleteInfoEntity($value['entityID']);
        } 
        return true;
    }

    public function piCancelForUpdate($pIID, $dimensionData = [], $ignoreBudgetLimit = true)
    {

        // $pIID = $request->getPost('pIID');

        // get purchase invoice details that related to the given purchase invoice id
        $openState = [3,6];
        $details = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByIDAndStatus($pIID, $openState);
        $grnFlag = false;
        $poFlag = false;
        $poID = null;
        $grnID = null;
        $basicPiDetails = $details;
        $defaultDataSet = iterator_to_array($basicPiDetails);
        if(empty($defaultDataSet)){
            $returnDataSet = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_CANNOT_DELETE_PI'),
                );
            return $returnDataSet;
        }


        //set grn flag and po flag
        if($defaultDataSet[0]['purchaseInvoiceGrnID'] != ''){
            $grnFlag = true;
            $grnID = $defaultDataSet[0]['purchaseInvoiceGrnID'];
        } else if ($defaultDataSet[0]['purchaseInvoicePoID'] != ''){
            $poFlag = true;
            $poID = $defaultDataSet[0]['purchaseInvoicePoID'];
        }

        $piProDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPurchaseInvoiceProductDetailsByPurchaseInvoiceID($pIID)->current();

        if ($piProDetails) {
            $grnFlag = true;
        }

        //if this pi come from grn then no need to reverse grn items. but need to check about new items that insert throuht the purchase invoice.

        if($grnFlag){
            $grnDataSet = $this->checkPiByOnlyGrn($pIID);

            $updateGrn = $this->updateGrnRelatedDetails($grnDataSet);
            if(!$updateGrn['status']){
                $returnDataSet = array(
                    'status' => false,
                    'msg' => $updateGrn['msg'],
                );
                return $returnDataSet;
            }

            //reverse existing items
            $reverseExistsItems = $this->reversePiProducts($pIID, $grnDataSet);
            if(!$reverseExistsItems['status']){
                $returnDataSet = array(
                    'status' => false,
                    'msg' => $reverseExistsItems['msg'],
                );
                return $returnDataSet;
            }

        } else {
            if($poFlag) {
                $poDataSet = $this->checkPIDataByPO($poID, $pIID);
                if(!$poDataSet['status']){
                    $returnDataSet = array(
                        'status' => false,
                        'msg' => $poDataSet['msg'],
                    );
                    return $returnDataSet;
                }
            }
            $createdDataSet = $this->createDataSetForReverse($pIID);
            $reverseItems = $this->reversePiProducts($pIID, $createdDataSet);

            if(!$reverseItems['status']){
                $returnDataSet = array(
                    'status' => false,
                    'msg' => $reverseItems['msg'],
                );
                return $returnDataSet;
            }
        }

        // for update juornal entry records first we need to check that use accounting
            if($this->useAccounting){
                $updateJournalAccount = $this->journalEntryreverseByPI(
                        $pIID,
                        $defaultDataSet[0]['purchaseInvoiceCode'],
                        $defaultDataSet[0]['purchaseInvoiceRetrieveLocation'],
                        $dimensionData,
                        $ignoreBudgetLimit
                    );

                if(!$updateJournalAccount['status']){
                    $returnDataSet = array(
                        'status' => false,
                        'data' => $updateJournalAccount['data'],
                        'msg' => ($updateJournalAccount['data'] == "BlockBudgetLimit" || $updateJournalAccount['data'] == "NotifyBudgetLimit") ? $updateJournalAccount['msg']: $this->getMessage('ERR_UPDATE_ACCOUNTS'),
                    );

                    return $returnDataSet;
                }
            }

        // update supplier outstanding

            $supplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($defaultDataSet[0]['purchaseInvoiceSupplierID']);

            $currentOutStanding = floatval($supplierDetails->supplierOutstandingBalance) - floatval($defaultDataSet[0]['purchaseInvoiceTotal']);

            //update supplier
            $data = array(
                'supplierOutstandingBalance' => $currentOutStanding,
                );

            $updateSupplier = $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplier($data, $defaultDataSet[0]['purchaseInvoiceSupplierID']);

            if(!$updateSupplier && floatval($defaultDataSet[0]['purchaseInvoiceTotal']) != 0){
                $returnDataSet = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_UPDATE_SUPPLIER_OUTSTANDING_PI'),
                );
                return $returnDataSet;

            }


        // update pv status as a replaced. add 10 as a status id
        $statusID = 10;
        $updatePI = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->updatePIStatusForCancel($pIID, $statusID);

        if(!$updatePI){
            $returnDataSet = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_UPDATE_PI_STATE'),
            );
            return $returnDataSet;

        }
        //update entity as deleted
        $updateEntity = $this->updateDeleteInfoEntity($defaultDataSet[0]['entityID']);
        $returnDataSet = array(
            'status' => true,
            'msg' => $this->getMessage('SUCC_PI_CANCELED'),
        );
        return $returnDataSet;
    }

    /////////////////////
    public function updatePurchaseInvoie($data, $dataSet, $canceledPiId)
    {

        $copyFromGrn = FALSE;
        $copyFromPo = FALSE;
        $copyingPoId = NULL;
        $copyingGrnId = NULL;
        $copiedDocumentType = null;
        $copiedDocumentID = null;

        if ($dataSet['piDetails']['purchaseInvoiceProductDocumentTypeID'] == 10) {
            $copyFromGrn = TRUE;
            $copyingGrnId = $dataSet['piDetails']['grnID'];
        }
        if ($dataSet['piDetails']['purchaseInvoiceProductDocumentTypeID'] == 9) {
            $copyFromPo = TRUE;
            $copyingPoId = $dataSet['piDetails']['poID'];
        }
        $supplierID = $data['sID'];
        $deliveryCharge = $data['dC'];
        $purchaseInvoiceCode = $data['piC'];
        $locationReferenceID = $data['lRID'];
        $documentReference = $data['documentReference'];
        $locationID = $data['rL'];
        $journalEntryAccountsData = $data['journalEntryData'];
        $ignoreBudgetLimit = $data['ignoreBudgetLimit'];
        $this->setLogMessage("Error occured when create ".$this->companyCurrencySymbol . $post['fT'] . ' amount purchase invoice - ' . $purchaseInvoiceCode);
        $entityID = $this->createEntity();
        $piData = array(
            'purchaseInvoiceCode' => $purchaseInvoiceCode,
            'purchaseInvoiceSupplierID' => $data['sID'],
            'purchaseInvoiceSupplierReference' => $data['sR'],
            'purchaseInvoiceRetrieveLocation' => $locationID,
            'purchaseInvoicePaymentDueDate' => $this->convertDateToStandardFormat($data['dD']),
            'purchaseInvoiceIssueDate' => $this->convertDateToStandardFormat($data['iD']),
            'purchaseInvoicePoID' => $copyingPoId,
            'purchaseInvoiceGrnID' => $copyingGrnId,
            'purchaseInvoiceComment' => $data['cm'],
            'purchaseInvoiceDeliveryCharge' => $data['dC'],
            'purchaseInvoiceShowTax' => $data['sT'],
            'purchaseInvoiceTotal' => $data['fT'],
            'paymentTermID' => $data['pT'],
            'purchaseInvoicePayedAmount' => $dataSet['piDetails']['piPayedAmo'],
            'entityID' => $entityID
        );
        $piM = new PurchaseInvoice();
        $piM->exchangeArray($piData);
        $piID = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->savePi($piM);
        $issueDate = $this->convertDateToStandardFormat($data['iD']);
        if(!$piID){
            $returnDataArray = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_PURINV_INV_CREATE'),
                );
            return $returnDataArray;

        }

        //if this pi had payments then re map that payments to the new purchaseInvoice.
        if($dataSet['piDetails']['piPayedAmo'] != 0){
            $newPaymentDetails = array(
                'invoiceID' => $piID,
                );
            $updatePayments = $this->CommonTable('SupplierInvoicePaymentsTable')->updateInvoicePaymentByInvoiceId($newPaymentDetails, $dataSet['piDetails']['piID']);
        }


        $supplierCurrentOutstndBalance = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($data['sID'])->supplierOutstandingBalance;
        $supplierNewOutstandingBalance = floatval($supplierCurrentOutstndBalance) + floatval($data['fT']);
        $supplierData = array(
            'supplierOutstandingBalance' => $supplierNewOutstandingBalance
        );
        $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplier($supplierData, $data['sID']);
        // $this->updateReferenceNumber($locationReferenceID);

        //save document Reference Data

        if($documentReference != ''){
            foreach ($documentReference as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $docRefdata = array(
                        'sourceDocumentTypeId' => 12,
                        'sourceDocumentId' => $piID,
                        'referenceDocumentTypeId' => $key,
                        'referenceDocumentId' => $value,
                        );
                    $documentReference = new DocumentReference();
                    $documentReference->exchangeArray($docRefdata);
                    $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                }
            }
        }

//if products were copied from GRN. No need to update Batch and series products
        if ($copyFromGrn) {

            $allProductTotal = 0;

            $editedProducts = array_filter($data['pr']);

            foreach ($editedProducts as $product) {
                $copiedDocumentType = null;
                $copiedDocumentID = null;
                $locationPID = $product['locationPID'];
                //get location product data for update average costing.
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                $grnProID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPurchaseInvoiceProductDetailsByPurchaseInvoiceProductID($product['pisubProID'])->current();

                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;
                $product['stockUpdate'] =  filter_var($product['stockUpdate'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                $grnProductQty = $this->CommonTable('Inventory\Model\GrnProductTable')
                    ->getgrnProductTotalQtyBylocationIDAndGrnID($grnProID['grnID'],$locationPID);

                $productBaseQty = $product['pQuantity'];
                $productType = $product['productType'];
                $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);
                $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                // $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);

                if ($product['dType'] == 'precentage') {
                    $productTotal = $product['pUnitPrice'] * $product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                } elseif ($product['dType'] == 'value') {
                    $productTotal = $product['pQuantity'] * (($product['pUnitPrice'] - $product['pDiscount']));
                } else {
                    $productTotal = $product['pUnitPrice'] * $product['pQuantity'];
                }
                
                $allProductTotal += $productTotal;

                //If product is a batch product
                $totalCopiedQty = 0;
                if (array_key_exists('bProducts', $product)) {
                    $batchProductCount = 0;
                    foreach ($product['bProducts'] as $bProduct) {
                        $totalCopiedQty += $bProduct['bQty'];
                        $batchProductCount++;

                        if($product['stockUpdate']){
                            $bPData = array(
                                'locationProductID' => $locationPID,
                                'productBatchCode' => $bProduct['bCode'],
                                'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                                'productBatchWarrantyPeriod' => $bProduct['warnty'],
                                'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                                'productBatchQuantity' => $bProduct['bQty'] * $productConversionRate
                                );

                            $batchProduct = new ProductBatch();
                            $batchProduct->exchangeArray($bPData);
                            $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                            $bProduct['bID']=$insertedBID;
                            if(!$insertedBID){
                                $returnDataArray = array(
                                    'status' => false,
                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                );
                                return $returnDataArray;

                            }
                        }
                        //if product is a batch and serial product
                        if (array_key_exists('sProducts', $product)) {
                            $batchSerialCount = 0;
                            foreach ($product['sProducts'] as $sProduct) {
                                if (isset($sProduct['sBCode'])) {
                                    if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                        if(!$product['stockUpdate']){
                                            $grnPData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($grnProductQty[0]['grnID'], $locationPID, $sProduct['sID'], $bProduct['bID'])->current();
                                            $grnProuctTotalQty = ($grnPData['grnProductTotalCopiedQuantity'] == '')? 0: $grnPData['grnProductTotalCopiedQuantity'];
                                            $newTotalQty = $grnProuctTotalQty + $productBaseQty;
                                            $grnProductID = $grnPData['grnProductID'];
                                            $data = array(
                                                'grnProductCopiedQuantity' => 1,
                                                'copied' => 1,
                                            );
                                            
                                            $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $grnProductID);

                                            if ($batchSerialCount == 0) {
                                                $sData = array(
                                                    'grnProductTotalCopiedQuantity' => $newTotalQty,
                                                    );
                                                $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($sData, $grnProductID);                                
                                            }
                                            $copiedDocumentType = 10;
                                            $copiedDocumentID = $grnPData['grnProductID'];


                                        }else{
                                            $sPData = array(
                                                'locationProductID' => $locationPID,
                                                'productBatchID' => $insertedBID,
                                                'productSerialCode' => $sProduct['sCode'],
                                                'productSerialExpireDate' => $this->convertDateToStandardFormat($sProduct['sEdate']),
                                                'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                                'productSerialWarrantyPeriodType' =>(int) $sProduct['sWarrantyType'],
                                            );
                                            $serialPr = new ProductSerial();
                                            $serialPr->exchangeArray($sPData);
                                            $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                            $sProduct['sID'] =$insertedSID;
                                            if(!$insertedSID){
                                                $returnDataArray = array(
                                                    'status' => false,
                                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                                );
                                                return $returnDataArray;

                                            }
                                            //save item in details
                                            $itemInData = array(
                                                'itemInDocumentType' => 'Payment Voucher',
                                                'itemInDocumentID' => $piID,
                                                'itemInLocationProductID' => $locationPID,
                                                'itemInBatchID' => $insertedBID,
                                                'itemInSerialID' => $insertedSID,
                                                'itemInQty' => 1,
                                                'itemInPrice' => $product['pUnitPrice'],
                                                'itemInDiscount' => $discountValue,
                                                'itemInDateAndTime' => $this->getGMTDateTime(),
                                                );
                                            $itemInModel = new ItemIn();
                                            $itemInModel->exchangeArray($itemInData);
                                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                            if(!$itemInID){
                                                $returnDataArray = array(
                                                    'status' => false,
                                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                                );
                                                return $returnDataArray;

                                            }
                                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                            $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                            $newPQtyData = array(
                                                'locationProductQuantity' => $newPQty
                                                );
                                            $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);

                                            if(!$status){
                                                $returnDataArray = array(
                                                    'status' => false,
                                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                                );
                                                return $returnDataArray;

                                            }
                                        }

                                        $piProductM = new PurchaseInvoiceProduct();
                                        $piProductData = array(
                                            'purchaseInvoiceID' => $piID,
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $bProduct['bID'],
                                            'productSerialID' => $sProduct['sID'],
                                            'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                                            'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                                            'purchaseInvoiceProductQuantity' => 1,
                                            'purchaseInvoiceProductTotalQty' => $productBaseQty,
                                            'purchaseInvoiceProductTotal' => $product['pTotal'],
                                            'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                                            'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                                            'rackID' => '',
                                            'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                                        );
                                        $piProductM->exchangeArray($piProductData);
                                        $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                                        if(!$insertedPiProductID){
                                            $returnDataArray = array(
                                                'status' => false,
                                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                            );
                                            return $returnDataArray;

                                        }
                                        if (array_key_exists('pTax', $product)) {
                                            if (array_key_exists('tL', $product['pTax'])) {
                                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                    $piPTaxesData = array(
                                                        'purchaseInvoiceID' => $piID,
                                                        'purchaseInvoiceProductID' => $insertedPiProductID,
                                                        'purchaseInvoiceTaxID' => $taxKey,
                                                        'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                                        'purchaseInvoiceTaxAmount' => $productTax['tA']
                                                    );
                                                    $piPTaxM = new PurchaseInvoiceProductTax();
                                                    $piPTaxM->exchangeArray($piPTaxesData);
                                                    $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                                }
                                            }
                                        }
                                    }
                                }
                                $batchSerialCount++;
                            }
                        } else {

                            if(!$product['stockUpdate']){
                                $grnPData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($grnProductQty[0]['grnID'], $locationPID, '', $bProduct['bID'])->current();
                                $grnProductID = $grnPData['grnProductID'];
                                $grnQty = ($grnPData['grnProductCopiedQuantity'] == '') ? 0: $grnPData['grnProductCopiedQuantity'];
                                $grnProuctTotalQty = ($grnPData['grnProductTotalCopiedQuantity'] == '')? 0: $grnPData['grnProductTotalCopiedQuantity'];
                                $newTotalQty = $grnProuctTotalQty + $productBaseQty;
                                $newQty = $grnQty + $bProduct['bQty'];
                                $copiedFlag = 0;
                                if($newQty == $grnPData['grnProductQuantity']){
                                    $copiedFlag = 1;
                                }

                                $data = array(
                                    'grnProductCopiedQuantity' => $newQty,
                                    'grnProductTotalCopiedQuantity' => $newTotalQty,
                                    'copied' => $copiedFlag,
                                );
                                $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $grnProductID);
                                $copiedDocumentType = 10;
                                $copiedDocumentID = $grnProductID;

                            }else{
                                 //save item in details
                                $itemInData = array(
                                    'itemInDocumentType' => 'Payment Voucher',
                                    'itemInDocumentID' => $piID,
                                    'itemInLocationProductID' => $locationPID,
                                    'itemInBatchID' => $insertedBID,
                                    'itemInSerialID' => NULL,
                                    'itemInQty' => $bProduct['bQty'] * $productConversionRate,
                                    'itemInPrice' => $product['pUnitPrice'],
                                    'itemInDiscount' => $discountValue,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                if(!$itemInID){
                                    $returnDataArray = array(
                                        'status' => false,
                                        'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                    );
                                    return $returnDataArray;

                                }
                                if ($batchProductCount == 1) {
                                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                                    $newPQtyData = array(
                                        'locationProductQuantity' => $newPQty
                                        );
                                    $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                    if(!$status){
                                        $returnDataArray = array(
                                            'status' => false,
                                            'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                        );
                                        return $returnDataArray;

                                    }
                                }
                            }

                            $piProductM = new PurchaseInvoiceProduct();
                            $piProductData = array(
                                'purchaseInvoiceID' => $piID,
                                'locationProductID' => $locationPID,
                                'productBatchID' => $bProduct['bID'],
                                'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                                'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                                'purchaseInvoiceProductQuantity' => $bProduct['bQty'] * $productConversionRate,
                                'purchaseInvoiceProductTotalQty' => $productBaseQty,
                                'purchaseInvoiceProductTotal' => $product['pTotal'],
                                'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                                'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                                'rackID' => '',
                                'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                            );

                            $piProductM->exchangeArray($piProductData);
                            $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                            if(!$insertedPiProductID){
                                $returnDataArray = array(
                                    'status' => false,
                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                );
                                return $returnDataArray;

                            }

                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $piPTaxesData = array(
                                            'purchaseInvoiceID' => $piID,
                                            'purchaseInvoiceProductID' => $insertedPiProductID,
                                            'purchaseInvoiceTaxID' => $taxKey,
                                            'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                            'purchaseInvoiceTaxAmount' => $productTax['tA']
                                        );
                                        $piPTaxM = new PurchaseInvoiceProductTax();
                                        $piPTaxM->exchangeArray($piPTaxesData);
                                        $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                    }
                                }
                            }
                        }
                    }

                } elseif (array_key_exists('sProducts', $product)) {

                    //If the product is a serial product
                    $serialCount = 0;
                    foreach ($product['sProducts'] as $sProduct) {
                        $totalCopiedQty++;
                        if(!$product['stockUpdate']){
                            $grnPData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($grnProductQty[0]['grnID'], $locationPID, $sProduct['sID'], '')->current();
                            $grnProductID = $grnPData['grnProductID'];
                            $grnProuctTotalQty = ($grnPData['grnProductTotalCopiedQuantity'] == '')? 0: $grnPData['grnProductTotalCopiedQuantity'];
                            $newTotalQty = $grnProuctTotalQty + $productBaseQty;
                            $data = array(
                                'grnProductCopiedQuantity' => 1,
                                'copied' => 1,
                                );
                            $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $grnProductID);

                            if ($serialCount == 0) {
                                $sData = array(
                                    'grnProductTotalCopiedQuantity' => $newTotalQty,
                                    );
                                $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($sData, $grnProductID);                                
                            }
                            $copiedDocumentType = 10;
                            $copiedDocumentID = $grnProductID;
                            $serialCount++;
                        }else{
                            $sPData = array(
                                'locationProductID' => $locationPID,
                                'productSerialCode' => $sProduct['sCode'],
                                'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                                'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                                );
                            $serialPr = new ProductSerial();
                            $serialPr->exchangeArray($sPData);
                            $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                            $sProduct['sID'] = $insertedSID;
                            if(!$insertedSID){
                                $returnDataArray = array(
                                    'status' => false,
                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                );
                                return $returnDataArray;

                            }
                         //save item in details
                            $itemInData = array(
                                'itemInDocumentType' => 'Payment Voucher',
                                'itemInDocumentID' => $piID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => NULL,
                                'itemInSerialID' => $insertedSID,
                                'itemInQty' => 1,
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if(!$itemInID){
                                $returnDataArray = array(
                                    'status' => false,
                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                );
                                return $returnDataArray;

                            }

                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                                );
                            $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                            if(!$status){
                                $returnDataArray = array(
                                    'status' => false,
                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                );
                                return $returnDataArray;

                            }
                        }

                        $piProductM = new PurchaseInvoiceProduct();
                        $piProductData = array(
                            'purchaseInvoiceID' => $piID,
                            'locationProductID' => $locationPID,
                            'productSerialID' => $sProduct['sID'],
                            'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                            'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                            'purchaseInvoiceProductQuantity' => 1,
                            'purchaseInvoiceProductTotalQty' => $productBaseQty,
                            'purchaseInvoiceProductTotal' => $product['pTotal'],
                            'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                            'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                            'rackID' => '',
                            'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                        );

                        $piProductM->exchangeArray($piProductData);
                        $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                        if(!$insertedPiProductID){
                            $returnDataArray = array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                            );
                            return $returnDataArray;

                        }
                        if (array_key_exists('pTax', $product)) {
                            if (array_key_exists('tL', $product['pTax'])) {
                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                    $piPTaxesData = array(
                                        'purchaseInvoiceID' => $piID,
                                        'purchaseInvoiceProductID' => $insertedPiProductID,
                                        'purchaseInvoiceTaxID' => $taxKey,
                                        'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                        'purchaseInvoiceTaxAmount' => $productTax['tA']
                                    );
                                    $piPTaxM = new PurchaseInvoiceProductTax();
                                    $piPTaxM->exchangeArray($piPTaxesData);
                                    $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                }
                            }
                        }
                    }
                } else {
                    $totalCopiedQty +=$productBaseQty;
                    if(!$product['stockUpdate']){
                        $grnPData = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductsByGrnIdLpidPsidAndPbid($grnProductQty[0]['grnID'], $locationPID, '', '')->current();
                        
                        $grnProductID = $grnPData['grnProductID'];
                        $grnQty = ($grnPData['grnProductCopiedQuantity'] == '') ? 0: $grnPData['grnProductCopiedQuantity'];
                        $grnProuctTotalQty = ($grnPData['grnProductTotalCopiedQuantity'] == '')? 0: $grnPData['grnProductTotalCopiedQuantity'];
                        $newQty = $grnQty + $productBaseQty;
                        $newTotalQty = $grnProuctTotalQty + $productBaseQty; 
                        $copiedFlag = 0;
                        if($newQty == $grnPData['grnProductQuantity']){
                            $copiedFlag = 1;
                        }

                        $data = array(
                            'grnProductCopiedQuantity' => $newQty,
                            'grnProductTotalCopiedQuantity' => $newTotalQty,
                            'copied' => $copiedFlag,
                            );
                        $this->CommonTable('Inventory\Model\GrnProductTable')->updateGrnProductsByGrnProductID($data, $grnProductID);
                        $copiedDocumentType = 10;
                        $copiedDocumentID = $grnProductID;

                    }else{
                        if ($productType != 2) {
                            $itemInData = array(
                                'itemInDocumentType' => 'Payment Voucher',
                                'itemInDocumentID' => $piID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => NULL,
                                'itemInSerialID' => NULL,
                                'itemInQty' => $productBaseQty,
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if(!$itemInID){
                                $returnDataArray = array(
                                    'status' => false,
                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                );
                                return $returnDataArray;

                            }
                        }

                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                        if ($productType == 2) {
                            $productBaseQty = 0;
                        }
                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                            );
                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        if(!$status){
                            $returnDataArray = array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                            );
                            return $returnDataArray;

                        }
                    }

                    //if the product is non serial and non batch
                    $piProductM = new PurchaseInvoiceProduct();
                    $piProductData = array(
                        'purchaseInvoiceID' => $piID,
                        'locationProductID' => $locationPID,
                        'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                        'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                        'purchaseInvoiceProductQuantity' => $productBaseQty,
                        'purchaseInvoiceProductTotalQty' => $productBaseQty,
                        'purchaseInvoiceProductTotal' => $product['pTotal'],
                        'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                        'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                        'purchaseInvoiceProductSelectedUomId' => $product['pUom']

                    );

                    $piProductM->exchangeArray($piProductData);
                    $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                    if(!$insertedPiProductID){
                        $returnDataArray = array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                        );
                        return $returnDataArray;

                    }

                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $piPTaxesData = array(
                                    'purchaseInvoiceID' => $piID,
                                    'purchaseInvoiceProductID' => $insertedPiProductID,
                                    'purchaseInvoiceTaxID' => $taxKey,
                                    'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                    'purchaseInvoiceTaxAmount' => $productTax['tA']
                                );
                                $piPTaxM = new PurchaseInvoiceProductTax();
                                $piPTaxM->exchangeArray($piPTaxesData);
                                $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                            }
                        }
                    }
                }
                if($product['stockUpdate']){
                    $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
                    $locationPrTotalQty = 0;
                    $locationPrTotalPrice = 0;
                    $newItemInIds = array();
                    if(count($itemInData) > 0){
                        foreach ($itemInData as $key => $value) {
                            if($value->itemInID > $locationProductLastItemInID){
                                $newItemInIds[] = $value->itemInID;
                                $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                if($remainQty > 0){
                                    $itemInPrice = $remainQty*$value->itemInPrice;
                                    $itemInDiscount = $remainQty*$value->itemInDiscount;
                                    $locationPrTotalQty += $remainQty;
                                    $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                                }
                                $locationProductLastItemInID = $value->itemInID;
                            }
                        }
                    }
                    $locationPrTotalQty += $locationProductQuantity;
                    $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                    if($locationPrTotalQty > 0){
                        $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                        $lpdata = array(
                            'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                            'locationProductLastItemInID' => $locationProductLastItemInID,
                        );

                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                        foreach ($newItemInIds as $inID) {
                            $intemInData = array(
                                'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                );
                            $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                        }
                    }
                }
                
                $this->checkAndUpdateGrnStatus($grnProductQty[0]['grnID']);
            }

            $jeResult = $this->saveJournalEntryDataForPI($piID, $purchaseInvoiceCode, $supplierID, $editedProducts, $deliveryCharge, $allProductTotal, $issueDate, $copyingGrnId, true, $journalEntryAccountsData, $flag = 4, $data['dimensionData'], $ignoreBudgetLimit);

            if($jeResult['status']){
                $this->setLogMessage($this->companyCurrencySymbol . $data['fT'] . ' amount purchase invoice successfully created - ' . $purchaseInvoiceCode);
                $this->flashMessenger()->addMessage($jeResult['msg']);
            }

            $returnDataArray = array(
                'status' => $jeResult['status'],
                'msg' => $jeResult['msg'],
                'data' =>  $jeResult['data']
            );
            return $returnDataArray;

        } else {
            $allProductTotal = 0;
            $editedNormalPro =  array_filter($data['pr']);
            foreach ($editedNormalPro as $product) {
                $copiedDocumentType = null;
                $copiedDocumentID = null;
                $locationPID = $product['locationPID'];
                //get location product data for update average costing.
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

                $product['stockUpdate'] =  filter_var($product['stockUpdate'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

                if(!$product['stockUpdate']){
                    $copiedDocumentType = 9;
                    $poProductDetails = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getPoProduct($copyingPoId, $locationPID);
                    $copiedDocumentID = $poProductDetails['purchaseOrderProductID'];

                }

                if ($copyFromPo == TRUE) {
                    $this->updatePoProductCopiedQty($locationPID, $copyingPoId, $product['pQuantity']);
                }
                
                $productBaseQty = $product['pQuantity'];
                $productType = $product['productType'];
                $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);
                $discountValue = $product['pUnitPrice'] * $product['pDiscount'] / 100;
                // $productTotal = $product['pUnitPrice']*$product['pQuantity'] * ((100 - $product['pDiscount']) / 100);

                if ($product['dType'] == 'precentage') {
                    $productTotal = $product['pUnitPrice'] * $product['pQuantity'] * ((100 - $product['pDiscount']) / 100);
                } elseif ($product['dType'] == 'value') {
                    $productTotal = $product['pQuantity'] * (($product['pUnitPrice'] - $product['pDiscount']));
                } else {
                    $productTotal = $product['pUnitPrice'] * $product['pQuantity'];
                }


                $allProductTotal+=$productTotal;

                //If product is a batch product
                if (array_key_exists('bProducts', $product)) {
                    $itemData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsForAvgCosting($locationPID);
                    $batchProductCount = 0;
                    foreach ($product['bProducts'] as $bProduct) {

                        $batchProductCount++;
                        $bPData = array(
                            'locationProductID' => $locationPID,
                            'productBatchCode' => $bProduct['bCode'],
                            'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                            'productBatchWarrantyPeriod' => $bProduct['warnty'],
                            'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                            'productBatchQuantity' => $bProduct['bQty'] * $productConversionRate
                        );

                        $batchProduct = new ProductBatch();
                        $batchProduct->exchangeArray($bPData);
                        $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                        if(!$insertedBID){
                            $returnDataArray = array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                        );
                        return $returnDataArray;

                        }
                        //if product is a batch and serial product
                        if (array_key_exists('sProducts', $product)) {
                            foreach ($product['sProducts'] as $sProduct) {
                                if (isset($sProduct['sBCode'])) {

                                    if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                        $sPData = array(
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialCode' => $sProduct['sCode'],
                                            'productSerialExpireDate' => $this->convertDateToStandardFormat($sProduct['sEdate']),
                                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                            'productSerialWarrantyPeriodType' =>(int) $sProduct['sWarrantyType'],
                                        );
                                        $serialPr = new ProductSerial();
                                        $serialPr->exchangeArray($sPData);
                                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                        if(!$insertedSID){
                                            $returnDataArray = array(
                                                'status' => false,
                                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                            );
                                            return $returnDataArray;

                                        }
                                        $piProductM = new PurchaseInvoiceProduct();
                                        $piProductData = array(
                                            'purchaseInvoiceID' => $piID,
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialID' => $insertedSID,
                                            'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                                            'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                                            'purchaseInvoiceProductQuantity' => $bProduct['bQty'],
                                            'purchaseInvoiceProductTotalQty' => $productBaseQty,
                                            'purchaseInvoiceProductTotal' => $product['pTotal'],
                                            'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                                            'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                                            'rackID' => '',
                                            'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                                        );
                                        $piProductM->exchangeArray($piProductData);
                                        $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                                        if(!$insertedPiProductID){
                                            $returnDataArray = array(
                                                'status' => false,
                                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                            );
                                            return $returnDataArray;
                                        }
                                        //save item in details
                                        $itemInData = array(
                                            'itemInDocumentType' => 'Payment Voucher',
                                            'itemInDocumentID' => $piID,
                                            'itemInLocationProductID' => $locationPID,
                                            'itemInBatchID' => $insertedBID,
                                            'itemInSerialID' => $insertedSID,
                                            'itemInQty' => 1,
                                            'itemInPrice' => $product['pUnitPrice'],
                                            'itemInDiscount' => $discountValue,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                        if(!$itemInID){
                                            $returnDataArray = array(
                                                'status' => false,
                                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                            );
                                            return $returnDataArray;

                                        }
                                        if (array_key_exists('pTax', $product)) {
                                            if (array_key_exists('tL', $product['pTax'])) {
                                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                    $piPTaxesData = array(
                                                        'purchaseInvoiceID' => $piID,
                                                        'purchaseInvoiceProductID' => $insertedPiProductID,
                                                        'purchaseInvoiceTaxID' => $taxKey,
                                                        'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                                        'purchaseInvoiceTaxAmount' => $productTax['tA']
                                                    );
                                                    $piPTaxM = new PurchaseInvoiceProductTax();
                                                    $piPTaxM->exchangeArray($piPTaxesData);
                                                    $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                                }
                                            }
                                        }
                                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                        $newPQtyData = array(
                                            'locationProductQuantity' => $newPQty
                                        );
                                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                        if(!$status){
                                            $returnDataArray = array(
                                                'status' => false,
                                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                            );
                                            return $returnDataArray;

                                        }
                                    }
                                }
                            }
                        } else {

                            $piProductM = new PurchaseInvoiceProduct();
                            $piProductData = array(
                                'purchaseInvoiceID' => $piID,
                                'locationProductID' => $locationPID,
                                'productBatchID' => $insertedBID,
                                'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                                'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                                'purchaseInvoiceProductQuantity' => $bProduct['bQty'] * $productConversionRate,
                                'purchaseInvoiceProductTotalQty' => $productBaseQty,
                                'purchaseInvoiceProductTotal' => $product['pTotal'],
                                'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                                'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                                'rackID' => '',
                                'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                            );

                            $piProductM->exchangeArray($piProductData);
                            $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                            if(!$insertedPiProductID){
                                $returnDataArray = array(
                                    'status' => false,
                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                );
                                return $returnDataArray;

                            }
                            //save item in details
                            $itemInData = array(
                                'itemInDocumentType' => 'Payment Voucher',
                                'itemInDocumentID' => $piID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => $insertedBID,
                                'itemInSerialID' => NULL,
                                'itemInQty' => $bProduct['bQty'] * $productConversionRate,
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if(!$itemInID){
                                $returnDataArray = array(
                                    'status' => false,
                                    'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                );
                                return $returnDataArray;

                            }
                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $piPTaxesData = array(
                                            'purchaseInvoiceID' => $piID,
                                            'purchaseInvoiceProductID' => $insertedPiProductID,
                                            'purchaseInvoiceTaxID' => $taxKey,
                                            'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                            'purchaseInvoiceTaxAmount' => $productTax['tA']
                                        );
                                        $piPTaxM = new PurchaseInvoiceProductTax();
                                        $piPTaxM->exchangeArray($piPTaxesData);
                                        $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                    }
                                }
                            }
                            if ($batchProductCount == 1) {
                                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                                $newPQtyData = array(
                                    'locationProductQuantity' => $newPQty
                                );
                                $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                if(!$status){
                                    $returnDataArray = array(
                                        'status' => false,
                                        'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                                    );
                                    return $returnDataArray;

                                }
                            }
                        }
                    }
                } elseif (array_key_exists('sProducts', $product)) {
                    //If the product is a serial product
                    $itemData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsForAvgCosting($locationPID);
                    foreach ($product['sProducts'] as $sProduct) {
                        $sPData = array('locationProductID' => $locationPID,
                            'productSerialCode' => $sProduct['sCode'],
                            'productSerialExpireDate' => ($sProduct['sEdate'] == '') ? $sProduct['sEdate'] : $this->convertDateToStandardFormat($sProduct['sEdate']),
                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                            'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                        );
                        $serialPr = new ProductSerial();
                        $serialPr->exchangeArray($sPData);
                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                        if(!$insertedSID){
                            $returnDataArray = array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                            );
                            return $returnDataArray;

                        }
                        $piProductM = new PurchaseInvoiceProduct();
                        $piProductData = array(
                            'purchaseInvoiceID' => $piID,
                            'locationProductID' => $locationPID,
                            'productSerialID' => $insertedSID,
                            'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                            'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                            'purchaseInvoiceProductQuantity' => 1,
                            'purchaseInvoiceProductTotalQty' => $productBaseQty,
                            'purchaseInvoiceProductTotal' => $product['pTotal'],
                            'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                            'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                            'rackID' => '',
                            'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                        );

                        $piProductM->exchangeArray($piProductData);
                        $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                        if(!$insertedPiProductID){
                            $returnDataArray = array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                            );
                            return $returnDataArray;

                        }
                        //save item in details
                        $itemInData = array(
                            'itemInDocumentType' => 'Payment Voucher',
                            'itemInDocumentID' => $piID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => $insertedSID,
                            'itemInQty' => 1,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            $returnDataArray = array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                            );
                            return $returnDataArray;

                        }
                        if (array_key_exists('pTax', $product)) {
                            if (array_key_exists('tL', $product['pTax'])) {
                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                    $piPTaxesData = array(
                                        'purchaseInvoiceID' => $piID,
                                        'purchaseInvoiceProductID' => $insertedPiProductID,
                                        'purchaseInvoiceTaxID' => $taxKey,
                                        'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                        'purchaseInvoiceTaxAmount' => $productTax['tA']
                                    );
                                    $piPTaxM = new PurchaseInvoiceProductTax();
                                    $piPTaxM->exchangeArray($piPTaxesData);
                                    $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                                }
                            }
                        }
                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                        );
                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        if(!$status){
                            $returnDataArray = array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                            );
                            return $returnDataArray;

                        }
                    }
                } else {
                    //if the product is non serial and non batch
                    $itemData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsForAvgCosting($locationPID);
                    $piProductM = new PurchaseInvoiceProduct();
                    $piProductData = array(
                        'purchaseInvoiceID' => $piID,                       
                        'locationProductID' => $locationPID,
                        'purchaseInvoiceProductPrice' => $product['pUnitPrice'],
                        'purchaseInvoiceProductDiscount' => $product['pDiscount'],
                        'purchaseInvoiceProductQuantity' => $productBaseQty,
                        'purchaseInvoiceProductTotalQty' => $productBaseQty,
                        'purchaseInvoiceProductTotal' => $product['pTotal'],
                        'purchaseInvoiceProductDocumentTypeID' => $copiedDocumentType,
                        'purchaseInvoiceProductDocumentID' => $copiedDocumentID,
                        'purchaseInvoiceProductSelectedUomId' => $product['pUom']
                    );

                    $piProductM->exchangeArray($piProductData);
                    $insertedPiProductID = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->savePurchaseInvoiceProduct($piProductM);
                    if(!$insertedPiProductID){
                        $returnDataArray = array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                        );
                        return $returnDataArray;

                    }

                    if ($productType != 2) {
                        $itemInData = array(
                            'itemInDocumentType' => 'Payment Voucher',
                            'itemInDocumentID' => $piID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => NULL,
                            'itemInQty' => $productBaseQty,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            $returnDataArray = array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                            );
                            return $returnDataArray;

                        }
                    }

                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $piPTaxesData = array(
                                    'purchaseInvoiceID' => $piID,
                                    'purchaseInvoiceProductID' => $insertedPiProductID,
                                    'purchaseInvoiceTaxID' => $taxKey,
                                    'purchaseInvoiceTaxPrecentage' => $productTax['tP'],
                                    'purchaseInvoiceTaxAmount' => $productTax['tA']
                                );
                                $piPTaxM = new PurchaseInvoiceProductTax();
                                $piPTaxM->exchangeArray($piPTaxesData);
                                $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTaxTable')->savePurchaseInvoiceProductTax($piPTaxM);
                            }
                        }
                    }
                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    if ($productType == 2) {
                        $productBaseQty = 0;
                    }


                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                    if(!$status){
                        $returnDataArray = array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_PURINV_CREATE'),
                        );
                        return $returnDataArray;

                    }

                }
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
                $locationPrTotalQty = 0;
                $locationPrTotalPrice = 0;
                $newItemInIds = array();
                if(count($itemInData) > 0){
                    foreach ($itemInData as $key => $value) {
                        if($value->itemInID > $locationProductLastItemInID){
                            $newItemInIds[] = $value->itemInID;
                            $remainQty = $value->itemInQty - $value->itemInSoldQty;
                            if($remainQty > 0){
                                $itemInPrice = $remainQty*$value->itemInPrice;
                                $itemInDiscount = $remainQty*$value->itemInDiscount;
                                $locationPrTotalQty += $remainQty;
                                $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                            }
                            $locationProductLastItemInID = $value->itemInID;
                        }
                    }
                }
                foreach ($itemData as $value) {
                    $availableStockQty = $value['totalAvilableStockQty'];
                    $availableStockValue = $value['totalAvilableStockValue'];
                }
                $itemInCanceledData = $this->CommonTable('Inventory\Model\ItemInTable')->getCanceledItemInProductDetails($canceledPiId, $locationPID)->current();
                
                $canceledItemInDate = $itemInCanceledData['itemInDateAndTime'];
                

                $locationPrTotalQty += $availableStockQty;
                $locationPrTotalPrice += $availableStockValue;
                    
                if($locationPrTotalQty > 0){

                    $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                    $lpdata = array(
                        'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                        'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                    foreach ($newItemInIds as $inID) {
                        $intemInData = array(
                            'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                            );
                        $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                    }

                    $outData = array(
                        'itemOutAverageCostingPrice' => $newAverageCostinPrice
                    );

                    $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutByArray($outData, $locationPID, $canceledItemInDate);

                    $itemOutIds = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDocumentIdsByCancelledDate($locationPID, $canceledItemInDate);

                    $itemOutDocumentIDs = [];
                    foreach ($itemOutIds as $value) {
                        
                        $journalEntryData = array(
                        'journalEntryAccountsDebitAmount' => $newAverageCostinPrice * $value['itemOutQty'],
                        'journalEntryAccountsCreditAmount' => $newAverageCostinPrice * $value['itemOutQty'],
                        );

                        $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryByDocumentId($journalEntryData, $value['journalEntryID']);   
                    }
                
                }
            }

            //      call product updated event
            $productIDs = array_map(function($element) {
                return $element['pID'];
            }, $post['pr']);

            // $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            // $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            if ($copyFromPo == TRUE) {
                $this->checkAndUpdatePoStatus($post['startByPoID']);
            }

            $jeResult = $this->saveJournalEntryDataForPI($piID, $purchaseInvoiceCode, $supplierID, $editedNormalPro, $deliveryCharge, $allProductTotal, $issueDate, '', true, $journalEntryAccountsData, $flag = 5, $data['dimensionData'], $ignoreBudgetLimit);

            if($jeResult['status']){
                $this->setLogMessage($this->companyCurrencySymbol . $data['fT'] . ' amount purchase invoice successfully created - ' . $purchaseInvoiceCode);
                $this->flashMessenger()->addMessage($jeResult['msg']);
            }

            $returnDataArray = array(
                'status' => $jeResult['status'],
                'msg' => $jeResult['msg'],
                'data' =>  $jeResult['data']
            );
            return $returnDataArray;
        }

    }

    private function calculateAvgCostAndOtherDetails($locationProductDetails, $productDataSet = null)
    {
        //get all iteminDetails
        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductIDInDecendingOrder($productDataSet['itemInLocationProductID'],$productDataSet['itemInID']);
        $initialState = 0;
        $locationProductQuantity = floatval($locationProductDetails['locationProductQuantity']);

        foreach ($itemInDetails as $key => $itemInSingleValue) {
            if ($initialState == 0) {
                //get sum of qty from item out for calculate costing
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
            } else {
                $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();

                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();

            }
            $initialState++;

            $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQty['totalQty'])) - floatval($itemInSingleValue['itemInQty']);

        }

        $nextInRecordFromCancelPi = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();

        $outDetailsQtyForPi = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($productDataSet['itemInLocationProductID'], $productDataSet['itemInDateAndTime'], $nextInRecordFromCancelPi['itemInDateAndTime'])->current();  


        $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQtyForPi['totalQty'])) - floatval($productDataSet['itemInQty']);

        $previousItemInFromCancelPi = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryPreviousRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();


        $outDetailsQtyForPreviousDoc = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($previousItemInFromCancelPi['itemInLocationProductID'], $previousItemInFromCancelPi['itemInDateAndTime'], $productDataSet['itemInDateAndTime'])->current();

        $locationProductQuantity = floatval($locationProductQuantity) + floatval($outDetailsQtyForPreviousDoc['totalQty']);

        if ($previousItemInFromCancelPi) {
            //get all iteminDetails after cancel
            $itemInDetailsAfterCancel = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductID($previousItemInFromCancelPi['itemInLocationProductID'],$previousItemInFromCancelPi['itemInID']);
            $avarageCosting = floatval($previousItemInFromCancelPi['itemInAverageCostingPrice']);
        } else {
            $itemInDetailsAfterCancel = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID']);
            $avarageCosting = 0;
        }
        $initialState = 0;
        $locationUpdateFlag = false;
        foreach ($itemInDetailsAfterCancel as $key => $itemInSingleValue) {
            $locationUpdateFlag = true;
            //get very Next record in itemIn Table
            $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();
            
            if($nextInRecord){
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();
                
            } else {
                if (sizeof($itemInDetails) != 0) {
                    $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
                }
            }
            
            if ($initialState == 0 && $previousItemInFromCancelPi) {
                $newAvgCosting = $avarageCosting;
            } else {
                $newAvgCosting = (($locationProductQuantity * $avarageCosting) + (floatval($itemInSingleValue['itemInQty']) * floatval($itemInSingleValue['itemInPrice'])))/(floatval($itemInSingleValue['itemInQty']) + $locationProductQuantity);
                $avarageCosting = $newAvgCosting;
                $locationProductQuantity = $locationProductQuantity + floatval($itemInSingleValue['itemInQty']);
            }
            //set data to update itemIn Table
            $itemInUpdatedDataSet = array(
                'itemInAverageCostingPrice' => $newAvgCosting,
            );

            //update item in table
            $itemInUpdateFlag = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($itemInUpdatedDataSet, $itemInSingleValue['itemInID']);
            if(!$itemInUpdateFlag){
                $returnCostingDetails = array(
                    'status' => false,
                    );
                return $returnCostingDetails;
            }
            
            $locationProductQuantity = $locationProductQuantity - floatval($outDetailsQty['totalQty']);
            //set data to update locationProduct
            $locationProductUpdatedData = array(
                'locationProductAverageCostingPrice' => $newAvgCosting
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $itemInSingleValue['itemInLocationProductID']);


            if($nextInRecord){
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime'],$nextInRecord['itemInDateAndTime']);
        
            } else {
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime']);

            }

           
            foreach ($itemOutDetails as $key => $itemOutSingleValue) {
                //set Data to update itemOut table
                $updateItemOutCValue = array(
                    'itemOutAverageCostingPrice' => $newAvgCosting,
                );

                //update costing value in itemOutRecords
                $updateItemOut = $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutTable($itemOutSingleValue['itemOutID'],$updateItemOutCValue);

                $itemOutIds = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDataByItemOutID($itemOutSingleValue['itemOutID'])->current();

                $journalEntryData = array(
                    'journalEntryAccountsDebitAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    'journalEntryAccountsCreditAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    );

                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryByDocumentId($journalEntryData, $itemOutIds['journalEntryID']);
                
                if(!$updateItemOut){
                    
                    $returnCalculateData = array(
                        'status' => false,
                    );
                
                    return $returnCalculateData;
                }
            }
            $initialState++;
        }

        if (!$locationUpdateFlag) {
            $locationProductUpdatedData = array(
                'locationProductAverageCostingPrice' => 0
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $productDataSet['itemInLocationProductID']);

        }

        $returnCostingDetails = array(
            'status' => true,
            );
        return $returnCostingDetails;
    }
    ////////////////////

    public function getAllRelatedDocumentDetailsByPiIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $purchaseInvoiceID = $request->getPost('purchaseInvoiceID');
            $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataWithTimeStampByID($purchaseInvoiceID);
            $poData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseOrderDataRelatedToPi($purchaseInvoiceID);
            $grnData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getGrnDetailsByPiId($purchaseInvoiceID);
            $grnRelatedPoData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPoDetailsRelatedToGrnByPiId($purchaseInvoiceID);
            $prDataRelatedToGrn = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPrDetailsRelatedToGrnByPiId($purchaseInvoiceID);
            $debitNoteDataByPi = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPiRelatedDebitNoteDataByPiId($purchaseInvoiceID);
            $piPaymentData = $this->CommonTable('SupplierPaymentsTable')->getPiPaymentDetailsByPiId($purchaseInvoiceID);
            $debitNotePaymentData = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentDetailsByPiId($purchaseInvoiceID);

            $dataExistsFlag = false;
            if ($piData) {
                $currentPiData = array(
                    'type' => 'PurchaseInvoice',
                    'documentID' => $piData['purchaseInvoiceID'],
                    'code' => $piData['purchaseInvoiceCode'],
                    'amount' => number_format($piData['purchaseInvoiceTotal'], 2),
                    'issuedDate' => $piData['purchaseInvoiceIssueDate'],
                    'created' => $piData['createdTimeStamp'],
                );
                $piDetails[] = $currentPiData;
                $dataExistsFlag = true;
            }
            if (isset($poData)) {
                foreach ($poData as $poDta) {
                    if (!is_null($poDta['purchaseOrderID'])) {
                        $purchaseOrderDta = array(
                            'type' => 'PurchaseOrder',
                            'documentID' => $poDta['purchaseOrderID'],
                            'code' => $poDta['purchaseOrderCode'],
                            'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                            'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                            'created' => $poDta['createdTimeStamp'],
                        );
                        $piDetails[] = $purchaseOrderDta;
                        if (isset($poDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
            }
            if (isset($grnData)) {
                foreach ($grnData as $grDta) {
                    if (!is_null($grDta['grnID'])) {
                        $grnDta = array(
                            'type' => 'Grn',
                            'documentID' => $grDta['grnID'],
                            'code' => $grDta['grnCode'],
                            'amount' => number_format($grDta['grnTotal'], 2),
                            'issuedDate' => $grDta['grnDate'],
                            'created' => $grDta['createdTimeStamp'],
                        );
                        $piDetails[] = $grnDta;
                        if (isset($grDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
            }
            if (isset($grnRelatedPoData)) {
                foreach ($grnRelatedPoData as $poDta) {
                    if (!is_null($poDta['purchaseOrderID'])) {
                        $purchaseOrderDta = array(
                            'type' => 'PurchaseOrder',
                            'documentID' => $poDta['purchaseOrderID'],
                            'code' => $poDta['purchaseOrderCode'],
                            'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                            'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                            'created' => $poDta['createdTimeStamp'],
                        );
                        $piDetails[] = $purchaseOrderDta;
                        if (isset($poDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
            }
            if (isset($prDataRelatedToGrn)) {
                foreach ($prDataRelatedToGrn as $prDta) {
                    if (!is_null($prDta['purchaseReturnID'])) {
                        $pReturnData = array(
                            'type' => 'PurchaseReturn',
                            'documentID' => $prDta['purchaseReturnID'],
                            'code' => $prDta['purchaseReturnCode'],
                            'amount' => number_format($prDta['purchaseReturnTotal'], 2),
                            'issuedDate' => $prDta['purchaseReturnDate'],
                            'created' => $prDta['createdTimeStamp'],
                        );
                        $piDetails[] = $pReturnData;
                        if (isset($prDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
            }
            if (isset($debitNoteDataByPi)) {
                foreach ($debitNoteDataByPi as $debitNoteDta) {
                    $dNData = array(
                        'type' => 'DebitNote',
                        'documentID' => $debitNoteDta['debitNoteID'],
                        'code' => $debitNoteDta['debitNoteCode'],
                        'amount' => number_format($debitNoteDta['debitNoteTotal'], 2),
                        'issuedDate' => $debitNoteDta['debitNoteDate'],
                        'created' => $debitNoteDta['createdTimeStamp'],
                    );
                    $piDetails[] = $dNData;
                    if (isset($debitNoteDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($piPaymentData)) {
                foreach ($piPaymentData as $piPayDta) {
                    $piPaymentData = array(
                        'type' => 'SupplierPayment',
                        'documentID' => $piPayDta['outgoingPaymentID'],
                        'code' => $piPayDta['outgoingPaymentCode'],
                        'amount' => number_format($piPayDta['outgoingPaymentAmount'], 2),
                        'issuedDate' => $piPayDta['outgoingPaymentDate'],
                        'created' => $piPayDta['createdTimeStamp'],
                    );
                    $piDetails[] = $piPaymentData;
                    if (isset($piPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($debitNotePaymentData)) {
                foreach ($debitNotePaymentData as $dNPayDta) {
                    $dNPaymentData = array(
                        'type' => 'DebitNotePayment',
                        'documentID' => $dNPayDta['debitNotePaymentID'],
                        'code' => $dNPayDta['debitNotePaymentCode'],
                        'amount' => number_format($dNPayDta['debitNotePaymentAmount'], 2),
                        'issuedDate' => $dNPayDta['debitNotePaymentDate'],
                        'created' => $dNPayDta['createdTimeStamp'],
                    );
                    $piDetails[] = $dNPaymentData;
                    if (isset($dNPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($piDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $piDetails);

            $documentDetails = array(
                'piDetails' => $piDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

    /**
     * This function is used to get grn details
     * @param JSON Requset
     * @return JSON Respond
     */
    public function getGrnDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $grnIDs = $request->getPost('grnIDs');

            if(!is_array($grnIDs)){
                $grnIdsArray = array($grnIDs);
            } else {
                $grnIdsArray = $grnIDs;
            }

            $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnDetailsByGrnIDs($grnIdsArray);
            $grnDetails = array();
            $grnID;
            $counter = 0;
            foreach ($grnData as $row) {
                $counter++;
                $tempG = array();
                $grnID = $row['grnID'];
                $tempG['gID'] = $row['grnID'];
                $tempG['gCd'] = $row['grnCode'];
                $tempG['gSID'] = $row['grnSupplierID'];
                $tempG['gSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempG['gSR'] = $row['grnSupplierReference'];
                $tempG['gRLID'] = $row['grnRetrieveLocation'];
                $tempG['gRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempG['gD'] = $this->convertDateToUserFormat($row['grnDate']);
                $tempG['gDC'] = $row['grnDeliveryCharge'];
                $tempG['gT'] = $row['grnTotal'];
                $tempG['gC'] = $row['grnComment'];
                $tempG['productType'] = $row['productTypeID'];
                $tempG['gSOB'] = $row['supplierOutstandingBalance'];
                $tempG['gSCB'] = $row['supplierCreditBalance'];
                $grnProducts = (isset($grnDetails[$row['grnID']]['gProducts'])) ? $grnDetails[$row['grnID']]['gProducts'] : array();

                /////////////////////
                if($counter == 1){
                //this condition use to set indexkey value for first time when run this main foreach loop
                    $indexKey = $row['grnProductID'];

                }
                if(isset($grnDetails[$row['grnID']]['gProducts']['uniqKey']) &&
                    $grnDetails[$row['grnID']]['gProducts']['uniqKey'] != $row['locationProductID']){
                    $indexKey = $row['grnProductID'];
                } else if(isset($grnDetails[$row['grnID']]['gProducts']['uniqKey']) &&
                    $grnDetails[$row['grnID']]['gProducts']['uniqKey'] == $row['locationProductID'] &&
                        $grnProducts[$indexKey]['gPP'] != $row['grnProductPrice']){
                    $indexKey = $row['grnProductID'];
                }

                /////////////////////

                if ($row['grnProductID'] != NULL) {
                    $productTaxes = (isset($grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['pT'])) ? $grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['pT'] : array();
                    $productBatches = (isset($grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['bP'])) ? $grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['bP'] : array();
                    $productSerials = (isset($grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['sP'])) ? $grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['sP'] : array();
                    $productUoms = (isset($grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['pUom'])) ? $grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['pUom'] : array();
                    $grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]] = array(
                            'gPC' => $row['productCode'],
                            'gPID' => $row['productID'],
                            'gPN' => $row['productName'],
                            'lPID' => $row['locationProductID'],
                            'gPP' => $row['grnProductPrice'],
                            'gPD' => $row['grnProductDiscount'],
                            'gPQ' => $row['grnProductTotalQty'] - $row['grnProductTotalCopiedQuantity'],
                            'gPT' => $row['grnProductTotal'],
                            'gProId' => $row['grnProductID'],
                            'gPUomID' => $row['grnProductUom'],
                            'grnDocId' => $row['grnID'],
                            'productType' => $row['productTypeID'],
                            'grnIndexKey' => $row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1],
                        );
                    if ($row['productBatchID'] != NULL) {
                        $productBatches[$row['productBatchID']] = array(
                        'bC' => $row['productBatchCode'],
                        'bED' => $this->convertDateToUserFormat($row['productBatchExpiryDate']),
                        'bW' => $row['productBatchWarrantyPeriod'],
                        'bMD' => $this->convertDateToUserFormat($row['productBatchManufactureDate']),
                        'bQ' => $row['grnProductQuantity']- $row['grnProductCopiedQuantity']); ///////////////////date-format
                    }
                    if ($row['productSerialID'] != NULL) {
                        $productSerials[$row['productSerialID']] = array(
                        'sC' => $row['productSerialCode'],
                        'sW' => $row['productSerialWarrantyPeriod'],
                        'sED' => $this->convertDateToUserFormat($row['productSerialExpireDate']),
                        'sBC' => $row['productBatchCode']); ///////////////////date-format
                    }
                    if ($row['grnTaxID'] != NULL) {
                        $productTaxes[$row['grnTaxID']] = array(
                            'pTN' => $row['taxName'],
                            'pTP' => $row['grnTaxPrecentage'],
                            'pTA' => $row['grnTaxAmount']);
                    }
                    if ($row['uomID'] != NULL) {
                        $productUoms[$row['uomID']] = array(
                            'gPUom' => $row['uomAbbr'],
                            'gPUomID' => $row['uomID'],
                            'gPUomCon' => $row['productUomConversion']);
                    }
                    $grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['pT'] = $productTaxes;
                    $grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['bP'] = $productBatches;
                    $grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['sP'] = $productSerials;
                    $grnProducts[$row['grnID'].$row['locationProductID'].explode('.',$row['grnProductPrice'])[0].explode('.',$row['grnProductPrice'])[1]]['pUom'] = $productUoms;
                }
                $tempG['gProducts'] = $grnProducts;
                $tempG['gProducts']['uniqKey'] = $row['locationProductID'];
                $grnDetails[$row['grnID']] = $tempG;
            }
            unset($grnDetails[$grnID]['gProducts']['uniqKey']);

            //get details of purchase invoice if any returns were made to the current GRN
            $prRelatedDetails = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnDetailsByGrnID(null,$grnIdsArray);
            $prID;
            $prDetails = array();
            foreach ($prRelatedDetails as $prData) {
                $tempPr = array();
                $prID = $prData['purchaseReturnID'];
                $tempPr['prID'] = $prData['purchaseReturnID'];
                $tempPr['prCd'] = $prData['purchaseReturnCode'];
                $tempPr['prSID'] = $prData['grnSupplierID'];
                $tempPr['prSName'] = $prData['supplierTitle'] . ' ' . $prData['supplierName'];
                $tempPr['prSR'] = $prData['grnSupplierReference'];
                $tempPr['prRLID'] = $prData['grnRetrieveLocation'];
                $tempPr['prRL'] = $prData['locationCode'] . ' - ' . $prData['locationName'];
                $tempPr['prD'] = $prData['purchaseReturnDate'];
                $tempPr['prT'] = $prData['purchaseReturnTotal'];
                $tempPr['prC'] = $prData['purchaseReturnComment'];
                $prProducts = (isset($prDetails[$prID]['prProducts'])) ? $prDetails[$prID]['prProducts'] : array();
                if ($prData['productID'] != NULL) {
                    $productTaxes = (isset($prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['pT'])) ?
                        $prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['pT'] : array();
                    $productBatches = (isset($prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['bP'])) ?
                        $prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['bP'] : array();
                    $productSerials = (isset($prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['sP'])) ?
                        $prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['sP'] : array();
                    $productUoms = (isset($prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['pUom'])) ?
                        $prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['pUom'] : array();

                    $prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]] = array(
                        'prPC' => $prData['productCode'],
                        'prPID' => $prData['productID'],
                        'prPN' => $prData['productName'],
                        'lPID' => $prData['purchaseReturnLocationProductID'],
                        'prPP' => $prData['purchaseReturnProductPrice'],
                        'prPD' => $prData['purchaseReturnProductDiscount'],
                        'prRPQ' => $prData['purchaseReturnProductReturnedQty'],
                        'prSRPQ' => $prData['purchaseReturnSubProductReturnedQty'],
                        'prPT' => $prData['purchaseReturnProductTotal']);
                    if ($prData['productBatchID'] != NULL) {
                        $productBatches[$prData['productBatchID']] = array(
                            'bC' => $prData['productBatchCode'],
                            'bED' => $prData['productBatchExpiryDate'],
                            'bW' => $prData['productBatchWarrantyPeriod'],
                            'bMD' => $prData['productBatchManufactureDate'],
                            'bRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                    }
                    if ($prData['productSerialID'] != NULL) {
                        $productSerials[$prData['productSerialID']] = array(
                            'sC' => $prData['productSerialCode'],
                            'sW' => $prData['productSerialWarrantyPeriod'],
                            'sED' => $prData['productSerialExpireDate'],
                            'sBC' => $prData['productBatchCode'],
                            'sRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                    }
                    if ($prData['purchaseReturnTaxID'] != NULL) {
                        $productTaxes[$prData['purchaseReturnTaxID']] = array(
                            'pTN' => $prData['taxName'],
                            'pTP' => $prData['purchaseReturnTaxPrecentage'],
                            'pTA' => $prData['purchaseReturnTaxAmount']);
                    }
                    if ($prData['uomID'] != NULL) {
                        $productUoms[$prData['uomID']] = array(
                            'pUom' => $prData['uomAbbr'],
                            'pUomID' => $prData['uomID'],
                            'pUomCon' => $prData['productUomConversion']);
                    }
                    $prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['pT'] = $productTaxes;
                    $prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['bP'] = $productBatches;
                    $prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['sP'] = $productSerials;
                    $prProducts[$prData['purchaseReturnGrnID'].$prData['purchaseReturnLocationProductID'].explode('.',$prData['purchaseReturnProductPrice'])[0].explode('.',$prData['purchaseReturnProductPrice'])[1]]['pUom'] = $productUoms;
                }
                $tempPr['prProducts'] = $prProducts;
                $prDetails[$prID] = $tempPr;
            }
            $totalProTaxes = array();
            foreach ($grnDetails as $key => $product) {
                foreach ($product['gProducts'] as $ke => $value) {
                    foreach ($value['pT'] as $tKey => $proTax) {
                        if (isset($totalProTaxes[$tKey])) {
                            $totalProTaxes[$tKey]['pTA'] = number_format((floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA'])), 2);
                        } else {
                            $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                        }
                    }
                }
            }

            foreach ($prDetails as $prIDKey => $prDataArray) {
                $prProductArray = isset($prDataArray['prProducts']) ? $prDataArray['prProducts'] : array();
                foreach ($grnDetails as $key => &$product) {
                    foreach ($product['gProducts'] as $ke => &$grnCheckProduct) {
                        //Update the product qtys of GRN if products were previously returned
                        $grnProductPurQty = $grnCheckProduct['gPQ'];
                        //check the current product was returned
                        if (array_key_exists($grnCheckProduct['grnIndexKey'], $prProductArray)) {
                            // $productKey = $grnCheckProduct['gPC'];
                            $productKey = $grnCheckProduct['grnIndexKey'];
                            $grnCheckProduct['gPQ'] = floatval($grnCheckProduct['gPQ']) - floatval($prProductArray[$productKey]['prRPQ']);

                            //if batch products available,update the returned qty details
                            if (!empty($grnCheckProduct['bP'])) {
                                foreach ($grnCheckProduct['bP'] as $batchKey => &$grnBatchPro) {
                                    if (array_key_exists($batchKey, $prProductArray[$productKey]['bP'])) {
                                        if ($grnBatchPro['bQ'] == $prProductArray[$productKey]['bP'][$batchKey]['bRQ']) {
                                            unset($grnCheckProduct['bP'][$batchKey]);
                                        } else {
                                            $grnBatchPro['bQ'] = floatval($grnBatchPro['bQ']) - floatval($prProductArray[$productKey]['bP'][$batchKey]['bRQ']);
                                        }
                                    }

                                }
                            }

                            //if serial products available,update the returned qty details
                            if (!empty($grnCheckProduct['sP'])) {
                                foreach ($grnCheckProduct['sP'] as $serialKey => &$serialPro) {
                                    if (array_key_exists($serialKey, $prProductArray[$productKey]['sP'])) {
                                        unset($grnCheckProduct['sP'][$serialKey]);
                                    }

                                }
                            }
                        }


                        //reset Total of the product
                        $grnCheckProduct['gPT'] = number_format((floatval($grnCheckProduct['gPT']) / floatval($grnProductPurQty)) * floatval($grnCheckProduct['gPQ']), 2, '.', '');

                        //reset the tax values of the product
                        foreach ($grnCheckProduct['pT'] as &$productTax) {
                            $productTax['pTA'] = number_format((floatval($productTax['pTA']) / floatval($grnProductPurQty)) * floatval($grnCheckProduct['gPQ']), 2, '.', '');
                        }

                        //if all items were returned, Unset the product from the list
                        if ($grnCheckProduct['gPQ'] == 0) {
                            unset($grnDetails[$key]['gProducts'][$productKey]);
                        }
                    }
                }
            }

            foreach ($grnDetails as $key => &$product) {
                foreach ($product['gProducts'] as &$grnCheckProduct) {

                    $grnProductPurQty = $grnCheckProduct['gPQ'];
                    if (!empty($grnCheckProduct['bP'])) {
                        foreach ($grnCheckProduct['bP'] as $batchKey => &$grnBatchPro) {
                                $batchSerialcurrentQty += $grnBatchPro['bQ'];
                        }
                        $grnCheckProduct['gPQ'] = $batchSerialcurrentQty;
                        $batchSerialcurrentQty = 0;
                    }
                    if (!empty($grnCheckProduct['sP'])) {
                        foreach ($grnCheckProduct['sP'] as $serialKey => &$serialPro) {
                            $batchSerialcurrentQty ++;
                        }
                        $grnCheckProduct['gPQ'] = $batchSerialcurrentQty;

                        $batchSerialcurrentQty = 0;

                    }

                    //reset Total of the product
                    $grnCheckProduct['gPT'] = number_format((floatval($grnCheckProduct['gPT']) / floatval($grnProductPurQty)) * floatval($grnCheckProduct['gPQ']), 2, '.', '');

                    //reset the tax values of the product
                    foreach ($grnCheckProduct['pT'] as &$productTax) {
                        $productTax['pTA'] = number_format((floatval($productTax['pTA']) / floatval($grnProductPurQty)) *
                            floatval($grnCheckProduct['gPQ']), 2, '.', '');
                    }
                }
            }

            $grnArray = [];
            foreach ($grnDetails as $key => $value) {
                $temp[$value['gSID']]['gID'] = $value['gID'];
                $temp[$value['gSID']]['gCd'] = $value['gCd'];
                $temp[$value['gSID']]['gSID'] = $value['gSID'];
                $temp[$value['gSID']]['gSN'] = $value['gSN'];
                $temp[$value['gSID']]['gSR'] = $value['gSR'];
                $temp[$value['gSID']]['gRLID'] = $value['gRLID'];
                $temp[$value['gSID']]['gRL'] = $value['gRL'];
                $temp[$value['gSID']]['gD'] = $value['gD'];
                $temp[$value['gSID']]['gDC'] = $value['gDC'];
                $temp[$value['gSID']]['gT'] = $value['gT'];
                $temp[$value['gSID']]['gC'] = $value['gC'];
                $temp[$value['gSID']]['productType'] = $value['productType'];
                $temp[$value['gSID']]['gSOB'] = $value['gSOB'];
                $temp[$value['gSID']]['gSCB'] = $value['gSCB'];

                foreach ($value['gProducts'] as $ke => $val) {
                    if ($ke != "uniqKey") {
                        $temp[$value['gSID']]['gProducts'][$ke] = $val;
                    }
                }
                $grnArray = $temp[$value['gSID']];
            }

            $locationProducts = Array();
            $inactiveItems = Array();
            $locationID = $grnDetails[$grnID]['gRLID'];
            foreach ($grnArray['gProducts'] as $gProducts) {
                $locationProducts[$gProducts['gPID']] = $this->getLocationProductDetails($locationID, $gProducts['gPID']);
                if ($locationProducts[$gProducts['gPID']] == null) {
                    $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($gProducts['gPID']);
                    $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                }
            }

            $inactiveItemFlag = false;
            $errorMsg = null;
            if (!empty($inactiveItems)) {
                $inactiveItemFlag = true;
                $errorItems = implode(",",$inactiveItems);        
                $errorMsg = $this->getMessage('ERR_GRN_ITM_INACTIVE_COPY', [$errorItems]);
            }

            $dimensionData = [];
            if (!is_array($request->getPost('grnIDs'))) {
                $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(10,$request->getPost('grnIDs'));
                
                $grnSingleData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnDetailsByGrnId($request->getPost('grnIDs'));

                foreach ($jEDimensionData as $value) {
                    if (!is_null($value['journalEntryID'])) {
                        $temp = [];
                        if($value['dimensionType'] == "job" || $value['dimensionType'] == "project") {
                            if ($value['dimensionType'] == "job") {
                                $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($value['dimensionValueID']);
                                $valueTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                                $dimensionTxt = "Job";
                            } else if ($value['dimensionType'] == "project") {
                                $proData = $this->CommonTable('Jobs\Model\ProjectTable')->getProjectByProjectId($value['dimensionValueID']);
                                $valueTxt = $proData->projectCode.'-'.$proData->projectName;
                                $dimensionTxt = "Project";
                            }

                            $temp['dimensionTypeId'] = $value['dimensionType'];
                            $temp['dimensionTypeTxt'] = $dimensionTxt;
                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                            $temp['dimensionValueTxt'] = $valueTxt;
                        } else {
                            $temp['dimensionTypeId'] = $value['dimensionType'];
                            $temp['dimensionTypeTxt'] = $value['dimensionName'];
                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                            $temp['dimensionValueTxt'] = $value['dimensionValue'];
                        }
                        $dimensionData[$grnSingleData->grnCode][$value['dimensionType']] = $temp;
                    }
                }
            }


            $sourceDocumentTypeId=10;
            $sourceDocumentId=$grnArray["gID"];
            $docrefData=$this->getDocumentReferenceBySourceData($sourceDocumentTypeId, $sourceDocumentId);
            $grnInfo = array(
                'grnData' => $grnArray,
                'grnSubTotal' => $grnSubTotal,
                'pTotalTax' => $totalProTaxes,
                'locationProducts' => $locationProducts,
                'docRefData'=>$docrefData,
                'errorMsg'=>$errorMsg,
                'inactiveItemFlag'=>$inactiveItemFlag,
                'dimensionData'=>$dimensionData,
            );
            $this->setLogMessage("Retrive ".$tempG['gCd']." GRN details.");
            $this->data = $grnInfo;
            return $this->JSONRespond();
        }
    }

    public function savePiByLandedCost($comingData, $savedID, $referenceCode)
    {
        // get all cost types
        $costType = $this->CommonTable('Inventory\Model\CompoundCostTypeTable')->getAllCostTypes();
        $costTypeArray = [];
        foreach ($costType as $key => $costType) {
            $costTypeArray[$costType['compoundCostTypeID']] = [
                'code' => $costType['compoundCostTypeCode'],
                'name' => $costType['compoundCostTypeName']
            ];
            
        }
        $this->beginTransaction();
        $dateFormat = $this->getUserDateFormat();
        $todayDateTime = $this->getUserDateTime();
        $todayDate = date('Y-m-d', strtotime($todayDateTime));

        // get suppliers one by one
        foreach (array_filter($comingData['costSuppliers']) as $key => $costSupp) {
            $costItem = [];
            $costItemTotal = 0;
            foreach ($comingData['supplierCostSet'] as $key => $supCostSet) {
                if ($costSupp == $supCostSet['supID']) {
                    // then get this product by productCode
                    $requestedProCode = 'NONINVENT_'.$costTypeArray[$supCostSet['CostID']]['code'];
                    $productDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductAndUomByProductCodeAndLocationId($requestedProCode, $comingData['location']);

                    if ($productDetails == '') {
                        $productDetails = $this->insertCompundCostItems($costTypeArray[$supCostSet['CostID']],$comingData['location']);
                    }
                    
                    // set item details to the array
                    $costItem[] = [
                        'locationPID' => $productDetails['locationProductID'],
                        'pID' => $productDetails['productID'],
                        'pCode' => $productDetails['productCode'],
                        'pName' =>$productDetails['productName'],
                        'pQuantity' => 1,
                        'pDiscount' => 0,
                        'pUnitPrice' => floatval($supCostSet['Cost']),
                        'pUom' => $productDetails['uomID'],
                        'pTotal' => floatval($supCostSet['Cost']),
                        'pTax' => [],
                        'productType' => $productDetails['productTypeID'], 
                        'stockUpdate' => true,
                        'productindexID' => null,
                        'pisubProID' => null,
                        'grnFlag' => false,
                        'poFlag' => false,
                        'grnDocId' => null 
                    ];

                    $costItemTotal += floatval($supCostSet['Cost']); 
                }
            }
            // get PI Reference number
            $refData = $this->getReferenceNoForLocation(14, $comingData['location']);
            $rid = $refData["refNo"];
            $locationReferenceID = $refData["locRefID"];
            // set pi data set
            $piDataSet = [
                'sID' => $costSupp,
                'piC' => $rid,
                'dD' => $todayDate,
                'iD' => $todayDate,
                'sR' => null,
                'rL' => $comingData['location'],
                'cm' => 'Document Genarated by landing cost # '.$referenceCode,
                'pr' => $costItem,
                'dC' => null,
                'fT' => $costItemTotal,
                'sT' => 0,
                'lRID' => $locationReferenceID,
                'pT' => 1,
                'multipleGrnCopyFlag' => false,
            ];

            // save PI
            $result = $this->storePi($piDataSet);
            if (!$result['status']) {
                $this->rollback();
                return false;
            }
            $compCostPiData = [
                'compoundCostTemplateID' => $savedID,
                'purchaseInvoiceID' => $result['data']['piID']
            ];
            $costPI = new CompoundCostPurchaseInvoice();
            $costPI->exchangeArray($compCostPiData);
            $saveCostPI = $this->CommonTable('Inventory\Model\CompoundCostPurchaseInvoiceTable')->saveCompoundCostPurchaseInvoice($costPI);
        
        }

        $this->commit();
        return true;
    }

    public function insertCompundCostItems($productDetails, $locationID, $newCostTypeCreateFlag = false)
    {
        $newCreatedProCode = 'NONINVENT_'.$productDetails['code'];
        $newCreatedProName = 'NONINVENT_'.$productDetails['name'];

        // get uom details
        $uoms = $this->CommonTable('Settings\Model\UomTable')->fetchAll();
        $uomArray = [];
        $uom;
        foreach ($uoms as $key => $uomValue) {
            if ($uomValue->uomAbbr == 'Unit') {
                $uomArray[$uomValue->uomID] = [
                    'uomConversion' => 1,
                    'baseUom' => true,
                    'displayUom' =>  true,
                ];
                $uom = $uomValue->uomID;
            }
            
        }

        // get gl accounts details
        if (!$newCostTypeCreateFlag) {
            $glAccounts = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll()->current();
            $productSalesAccountID = $glAccounts->glAccountSetupItemDefaultSalesAccountID;
            $productInventoryAccountID = $glAccounts->glAccountSetupItemDefaultInventoryAccountID;
            $productCOGSAccountID = $glAccounts->glAccountSetupItemDefaultCOGSAccountID;
            $productAdjusmentAccountID = $glAccounts->glAccountSetupItemDefaultAdjusmentAccountID;
        } else {
            $productSalesAccountID = $productDetails['productSalesAccountID'];
            $productInventoryAccountID = $productDetails['productInventoryAccountID'];
            $productCOGSAccountID = $productDetails['productCOGSAccountID'];
            $productAdjusmentAccountID = $productDetails['productAdjusmentAccountID'];
        }
        $productSet = [
            'productCode' => $newCreatedProCode, 
            'productBarcode' => $newCreatedProCode,
            'productName' => $newCreatedProName,
            'productTypeID' => 2,
            'productDescription' => null,
            'productDiscountEligible' => 1,
            'productDiscountPercentage' => 0,
            'productDiscountValue' => null,
            'productPurchaseDiscountEligible' => 1,
            'productPurchaseDiscountPercentage' => 0,
            'productPurchaseDiscountValue' => null,
            'productDefaultOpeningQuantity' => 0,
            'productDefaultSellingPrice' => 0,
            'productDefaultPurchasePrice' => 0,
            'productDefaultMinimumInventoryLevel' => 0,
            'productDefaultReorderLevel' => 0,
            'productTaxEligible' => 1,
            'productImageURL' => null,
            'productState' => 1,
            'categoryID' => 1,
            'productGlobalProduct' => 1,
            'batchProduct' => 0,
            'serialProduct' => 0,
            'hsCode' => null,
            'customDutyPercentage' => null,
            'customDutyValue' => null,
            'rackID' => null,
            'itemDetail' => null,
            'productSalesAccountID' => $productSalesAccountID,  
            'productInventoryAccountID' => $productInventoryAccountID,
            'productCOGSAccountID' => $productCOGSAccountID,
            'productAdjusmentAccountID' => $productAdjusmentAccountID,
            'handeling' => [
                0 => 'productHandelingManufactureProduct',
            ],
            'displayUoms' => $uom,
            'uom' => $uomArray,
            
        ];
        
        // save product details
        $pI = $this->getServiceLocator()->get("ProductAPIController");
        $productStatus =  $pI->storeProduct($productSet);
        if ($productStatus['status']) {
            return $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductAndUomByProductCodeAndLocationId($newCreatedProCode, $locationID);
        } else {
            return false;
        }
        
    }


}