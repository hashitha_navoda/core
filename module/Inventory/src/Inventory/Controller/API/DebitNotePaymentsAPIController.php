<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file debit note payments API related controller functions
 */

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Model\DebitNotePayment;
use Inventory\Model\DebitNotePaymentDetails;
use Inventory\Model\Supplier;
use Inventory\Model\DebitNote;
use Inventory\Model\DebitNotePaymentMethodNumbers;
use Zend\View\Model\JsonModel;

/**
 * This is the debit note Payments Api controller class
 */
class DebitNotePaymentsAPIController extends CoreController
{

    protected $user_session;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    function getSupplierRelatedDebitNotesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $flag = 0;
            $supplierID = $request->getPost('supplierID');
            $supplierCode = $request->getPost('supplierCode');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteBySupplierIDAndLocationID($supplierID, $locationID, $groupFlag = true);
            $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
            $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
            foreach ($paymentMethos as $t) {
                $payMethod[$t['paymentMethodID']] = $t['paymentMethodName'];
            }

            foreach ($debitNoteData as $dn) {
                $dNote = (object) $dn;
                if ($dNote->statusID == 3 && $dNote->debitNotePaymentEligible == 1) {
                    $flag = 1;
                }
                $purchaseInvoiceID = $dn['purchaseInvoiceID'];
                $purchaseInvoiceData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataByID($purchaseInvoiceID);
                if (floatval($purchaseInvoiceData['purchaseInvoicePayedAmount']) != 0) {
                    $debitNoteSettledAmount = is_null($dNote->debitNoteSettled) ? 0 : ($dNote->debitNoteSettled);
                    if ($dNote->debitNoteTotal != $debitNoteSettledAmount) {
                        $debitNotes[] = $dNote;
                    }
                }
            }

            $debitNotePaymenteView = new ViewModel(array(
                'debitNotes' => $debitNotes,
                'paymentMethod' => $payMethod,
                    )
            );
            $debitNotePaymenteView->setTerminal(true);
            $debitNotePaymenteView->setTemplate('inventory/debit-note-payments-api/addDebitNotePaymentList');
            if ($flag == 1) {
                $this->setLogMessage("Retrive supplier ".$supplierCode." related debit notes.");
                $this->status = true;
                $this->data = array('supplier' => $supplier,'eligibleDebitNoteCount' => count($debitNotes));
                $this->html = $debitNotePaymenteView;
                return $this->JSONRespondHtml();
            } else {
                $this->setLogMessage("Not any debite notes related to supplier ".$supplierCode.".");
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DNPAY_NO_CN_FOUND');
                return $this->JSONRespond();
            }
        }
    }

    function getDebitNoteDetailsByDebitNoteIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $debitNoteID = $request->getPost('debitNoteID');
            $debitNoteCode = $request->getPost('debitNoteCode');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteID($debitNoteID);
            foreach ($debitNoteData as $dn) {
                $dNote = (object) $dn;
                $supplierID = $dNote->supplierID;
                $debitNotes[] = $dNote;

                $purchaseInvoiceID = $dn['purchaseInvoiceID'];
                $purchaseInvoiceData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataByID($purchaseInvoiceID);
                if (floatval($purchaseInvoiceData['purchaseInvoicePayedAmount']) == 0) {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_DNPAYMENT_CANNOT_DO_BEFORE_PI_PAYMENT');
                    return $this->JSONRespond();
                }

                if (floatval($dNote->debitNotePaymentAmount) == floatval($dNote->debitNoteSettled)) {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_DNPAYMENT_PAID');
                    return $this->JSONRespond();
                }
            }
            $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
            $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
            foreach ($paymentMethos as $t) {
                $payMethod[$t['paymentMethodID']] = $t['paymentMethodName'];
            }

            $debitNotePaymenteView = new ViewModel(array(
                'debitNotes' => $debitNotes,
                'paymentMethod' => $payMethod,
                    )
            );
            $debitNotePaymenteView->setTerminal(true);
            $debitNotePaymenteView->setTemplate('inventory/debit-note-payments-api/addDebitNotePaymentList');
            $this->status = true;
            $this->data = array('supplier' => $supplier);
            $this->html = $debitNotePaymenteView;
            $this->setLogMessage("Retrive debit note ".$debitNoteCode." Details.");
            return $this->JSONRespondHtml();
        }
    }

    function addDebitNotePaymentAction() 
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierID = $request->getPost('supplierID');
            $supplierCredit = $request->getPost('supplierCredit');
            $dnPaymentData = $request->getPost('debitNotePaymentsData');
            $debitNotepaymentCode = $request->getPost('debitNotepaymentCode');
            $debitNotePaymentDate = $this->convertDateToStandardFormat($request->getPost('date'));
            $paymentTerm = $request->getPost('paymentTerm');
            $debitNotePaymentAmount = $request->getPost('amount');
            $debitNotePaymentDiscount = $request->getPost('discount');
            $debitNotePaymentMemo = $request->getPost('memo');
            $locationID = $request->getPost('locationID');
            $dimensionData = $request->getPost('dimensionData');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $paymentdetails = json_decode($request->getPost('PaymentMethodDetails'));
            $this->beginTransaction();
            $result = $this->getReferenceNoForLocation('18', $locationID);
            $locationReferenceID = $result['locRefID'];
            // check if a debit note payment from the same debit note payment code exists if exist add next number to debit note payment code
            while ($debitNotepaymentCode) {
                if ($this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentCode($debitNotepaymentCode)) {
                    if ($locationReferenceID) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $debitNotepaymentCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $this->setLogMessage("Error occured when create debit note payment ".$debitNotepaymentCode." .");
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DNPAY_CODE_EXISTS');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }

            $this->setLogMessage("Error occured when create debit note payment ".$debitNotepaymentCode." .");

            $entityID = $this->createEntity();

            $debitNotePaymentData = array(
                'debitNotePaymentCode' => $debitNotepaymentCode,
                'debitNotePaymentDate' => $debitNotePaymentDate,
                'paymentTermID' => $paymentTerm,
                'debitNotePaymentAmount' => $debitNotePaymentAmount,
                'supplierID' => $supplierID,
                'debitNotePaymentDiscount' => $debitNotePaymentDiscount,
                'debitNotePaymentMemo' => $debitNotePaymentMemo,
                'locationID' => $locationID,
                'statusID' => 4,
                'entityID' => $entityID,
            );

            $debitNotePayment = new DebitNotePayment;
            $debitNotePayment->exchangeArray($debitNotePaymentData);
            $debitNotePaymentID = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->saveDebitNotePayment($debitNotePayment);

            $supplierData = array(
                'supplierCreditBalance' => $supplierCredit,
                'supplierID' => $supplierID,
            );
            $supplierDetails = new Supplier;
            $supplierDetails->exchangeArray($supplierData);
            $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplierCredit($supplierDetails);
            
            $supData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
            $paymentMethodAmounts = array();
            $totalPaymentMethodsAmount = 0;
            foreach ($dnPaymentData as $key => $val) {
                $value = (object) $val;
                if(isset($paymentMethodAmounts[$value->paymentMethod])){
                    $paymentMethodAmounts[$value->paymentMethod] += $value->settledAmount;
                }else{
                    $paymentMethodAmounts[$value->paymentMethod] = $value->settledAmount;
                }

                $totalPaymentMethodsAmount +=$value->settledAmount;

                $debitNotePaymentDetailsData = array(
                    'debitNoteID' => $value->debitNoteID,
                    'debitNotePaymentID' => $debitNotePaymentID,
                    'debitNotePaymentDetailsAmount' => $value->settledAmount,
                    'paymentMethodID' => $value->paymentMethod,
                );
                $debitNotePaymentDetails = new DebitNotePaymentDetails;
                $debitNotePaymentDetails->exchangeArray($debitNotePaymentDetailsData);
                $this->CommonTable('Inventory\Model\DebitNotePaymentDetailsTable')->saveDebitNotePaymentDetails($debitNotePaymentDetails);

                $debitNoteData = (object) $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteID($value->debitNoteID)->current();
                $newDebitNoteSettleAmount = $debitNoteData->debitNoteSettledAmount;
                if ($newDebitNoteSettleAmount == $debitNoteData->debitNotePaymentAmount) {
                    $statusID = 4;
                } else {
                    $statusID = 3;
                }

                $debitNoteUpdateData = array(
                    'debitNoteSettledAmount' => $newDebitNoteSettleAmount,
                    'statusID' => $statusID,
                    'debitNoteID' => $value->debitNoteID,
                );

                $debitNoteUpdate = new DebitNote;
                $debitNoteUpdate->exchangeArray($debitNoteUpdateData);
                $this->CommonTable('Inventory\Model\DebitNoteTable')->updateDebitNoteSettleAmountAndStatusID($debitNoteUpdate);

                if ($value->paymentMethod == 1) {
                    $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($locationID)->current();
                    $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] + $value->settledAmount;
                    $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($locationID, $newLocationCashInHand);
                }
            }

            $this->updateReferenceNumber($locationReferenceID);
            
            if ($this->useAccounting == 1) {
                $accountPayments = array();
                    //check whether supplier Accounts Payable Accounts is set or not
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                if(empty($supplierData->supplierPayableAccountID)){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PURINV_SUPPLIER_PAYABLE_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                    return $this->JSONRespond();
                }else if(!empty($debitNotePaymentDiscount) && empty($supplierData->supplierPurchaseDiscountAccountID)){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PURINV_SUPPLIER_PURCHASE_DISCOUNT_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                    return $this->JSONRespond();
                }

                foreach ($paymentdetails as $pmkey => $pmvalue) {
                    if($pmvalue->paymentMethodFinanceAccountID != ''){
                        //set gl accounts for the journal entry
                        $paymethodDiscount = 0;
                        if(!($debitNotePaymentDiscount == 0 || $debitNotePaymentDiscount == "")){
                            $paymethodDiscount = $debitNotePaymentDiscount*($paymentMethodAmounts[$pmvalue->paymentMethodID]/$totalPaymentMethodsAmount); 
                        }
                        if(isset($accountPayments[$pmvalue->paymentMethodFinanceAccountID]['total'])){
                            $accountPayments[$pmvalue->paymentMethodFinanceAccountID]['total'] += $paymentMethodAmounts[$pmvalue->paymentMethodID] - $paymethodDiscount;
                        }else{
                            $accountPayments[$pmvalue->paymentMethodFinanceAccountID]['total'] = $paymentMethodAmounts[$pmvalue->paymentMethodID] - $paymethodDiscount;
                            $accountPayments[$pmvalue->paymentMethodFinanceAccountID]['accountID'] = $pmvalue->paymentMethodFinanceAccountID;
                        }
                    }else{
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PUR_PAY_PLEASE_SELECT_ACCOUNT_ID_FOR_PAY_METHODS', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                        return $this->JSONRespond();       
                    }
                }

                //create data array for the journal entry save.
                $i=0;
                $journalEntryAccounts = array();

                foreach ($accountPayments as $key => $value) {
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['total'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Debit Note Payments '.$debitNotepaymentCode;
                    $i++;
                }

                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $supplierData->supplierPayableAccountID;
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $totalPaymentMethodsAmount;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Debit Note Payments '.$debitNotepaymentCode;
                $i++;

                if(!empty($debitNotePaymentDiscount)){
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $supplierData->supplierPurchaseDiscountAccountID;
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $debitNotePaymentDiscount;
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Debit Note Payments '.$debitNotepaymentCode.".";
                }

                //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $debitNotePaymentDate,
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create Debit Note Payment '.$debitNotepaymentCode.".",
                    'documentTypeID' => 18,
                    'journalEntryDocumentID' => $debitNotePaymentID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                );

                $resultData = $this->saveJournalEntry($journalEntryData);
                if($resultData['status']){
                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($debitNotePaymentID,18, 3);
                    if(!$jEDocStatusUpdate['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $jEDocStatusUpdate['msg'];
                        return $this->JSONRespond();
                    }

                    if (!empty($dimensionData)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$debitNotepaymentCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $saveRes['msg'];
                            $this->data = $saveRes['data'];
                            return $this->JSONRespond();
                        }   
                    } else {
                        foreach ($dnPaymentData as $key => $value) {
                            $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(13,$key);
                            $dimensionData = [];
                            foreach ($jEDimensionData as $value) {
                                if (!is_null($value['journalEntryID'])) {
                                    $temp = [];
                                    $temp['dimensionTypeId'] = $value['dimensionType'];
                                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                                    $dimensionData[$debitNotepaymentCode][] = $temp;
                                }
                            }
                            if (!empty($dimensionData)) {
                                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$debitNotepaymentCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                if(!$saveRes['status']){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg =$saveRes['msg'];
                                    $this->data =$saveRes['data'];
                                    return $this->JSONRespond();
                                }   
                            }
                        }
                    } 
                    $this->setLogMessage($this->companyCurrencySymbol . $debitNotePaymentAmount . ' amount debit Note payment -' . $debitNotepaymentCode.' successfully created  againts supplier '.$supData->supplierCode.'.');
                    $this->commit();
                    $this->status = true;
                    $this->msg = $this->getMessage('SUC_DNPAY_DETAILS_SUC_ADDED');
                    $this->data = array('debitNotePaymentID' => $debitNotePaymentID, 'debitNotePaymentCode' => $debitNotepaymentCode);
                }else{
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $resultData['msg'];
                    $this->data = $resultData['data'];
                }
            }else {
                $this->setLogMessage($this->companyCurrencySymbol . $debitNotePaymentAmount . ' amount debit Note payment -' . $debitNotepaymentCode.' successfully created  againts supplier '.$supData->supplierCode.'.');
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUC_DNPAY_DETAILS_SUC_ADDED');
                $this->data = array('debitNotePaymentID' => $debitNotePaymentID, 'debitNotePaymentCode' => $debitNotepaymentCode);
            }
            
            return $this->JSONRespond();
        }
    }

    public function addDebitNotePaymentMethodNumbersAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $debitNotePaymentID = $request->getPost('debitNotePaymentID');
            $debitNotePaymentMethodNumbersDetails = json_decode($request->getPost('debitNotePaymentMethodDetails'));
            
            foreach ($debitNotePaymentMethodNumbersDetails as $key => $value) {
                    $referenceNumber = (isset($value->paymentMethodReferenceNumber)) ? $value->paymentMethodReferenceNumber: '';
                    $debitNotePaymentMethodBank = (isset($value->paymentMethodBank)) ? $value->paymentMethodBank: '';
                    $debitNotePaymentMethodCardID = (isset($value->paymentMethodCardID)) ? $value->paymentMethodCardID: '';
                    $data = array(
                        'debitNotePaymentID' => $debitNotePaymentID,
                        'paymentMethodID' => $value->paymentMethodID,
                        'debitNotePaymentMethodReferenceNumber' => $referenceNumber,
                        'debitNotePaymentMethodBank' => $debitNotePaymentMethodBank,
                        'debitNotePaymentMethodCardID' => $debitNotePaymentMethodCardID,
                    );
                $DebitNotePaymentMethodsNumbers = new DebitNotePaymentMethodNumbers();
                $DebitNotePaymentMethodsNumbers->exchangeArray($data);
                $valuess = $this->CommonTable('Inventory\Model\DebitNotePaymentMethodNumbersTable')->saveDebitNotePaymentMethodsNumbers($DebitNotePaymentMethodsNumbers);
            }

            $this->status = true;
            $this->msg = $this->getMessage('SUC_DNPAY_METHOD_NUMBERS_SUC_ADDED');
            $this->data = $debitNotePaymentID;
            $this->setLogMessage('Debit note payment method numbers detais sucssesfully saved.');
        } else {
            $this->setLogMessage("Debit note add payment method numbers request is not a post request.");
            $this->status = false;
        }
        return $this->JSONRespond();
    }

    public function retriveSupplierDebitNotePaymentsAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $sup_id = $request->getPost('supplierID');
            $sup_code = $request->getPost('supplierCode');
            $debitNotePayments = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentsBySupplierIDAndLocationID($sup_id, $locationID);

            foreach ($debitNotePayments as $t) {
                $debitNotePaymentsArray[$t['debitNotePaymentID']] = (object) $t;
            }
            $supplierDebitNotePayments = new ViewModel(
                    array(
                'debitNotePayments' => $debitNotePaymentsArray,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'statuses' => $this->getStatusesList()
            ));
            $supplierDebitNotePayments->setTerminal(TRUE);
            $supplierDebitNotePayments->setTemplate('inventory/debit-note-payments/debit-note-payment-list');
            $this->setLogMessage("Retrive debit note payments for list view  by supplier ".$sup_code." .");
            return $supplierDebitNotePayments;
        }
    }

    public function retriveDebitNotePaymentByDebitNotePaymentIDAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $debitNotePaymentID = $request->getPost('debitNotePaymentID');
            $debitNotePaymentCode = $request->getPost('debitNotePaymentCode');
            $debitNotePayments = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentID($debitNotePaymentID, true);
            if ($debitNotePayments->count() == 0) {
                $er = 'notFindDebitNotePayment';
                return new JsonModel(array($er));
            } else {
                foreach ($debitNotePayments as $t) {
                    $debitNotePaymentsArray[$t['debitNotePaymentID']] = (object) $t;
                }
                $debitNotePaymentsData = new ViewModel(
                        array(
                    'debitNotePayments' => $debitNotePaymentsArray,
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,
                    'statuses' => $this->getStatusesList()
                ));
                $debitNotePaymentsData->setTerminal(TRUE);
                $debitNotePaymentsData->setTemplate('inventory/debit-note-payments/debit-note-payment-list');
                $this->setLogMessage("Retrive debit note payment ".$debitNotePaymentCode." for list view.");
                return $debitNotePaymentsData;
            }
        }
    }

    public function retriveDebitNotePaymentsByDatefilterAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $fromdate = $request->getPost('fromdate');
            $todate = $request->getPost('todate');
            $supplierID = $request->getPost('supplierID');
            $getpayment = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentsByDate($fromdate, $todate, $supplierID, $locationID);

            foreach ($getpayment as $t) {
                $debitNotePaymentsArray[$t['debitNotePaymentID']] = (object) $t;
            }
            $debitNotePaymentsData = new ViewModel(array(
                'debitNotePayments' => $debitNotePaymentsArray,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'statuses' => $this->getStatusesList()
                    )
            );
            $debitNotePaymentsData->setTerminal(TRUE);
            $debitNotePaymentsData->setTemplate('inventory/debit-note-payments/debit-note-payment-list');
            $this->setLogMessage("Retrive debit note payments for list view by date filer.");
            return $debitNotePaymentsData;
        }
    }

    public function deleteDebitNotePaymentAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $debitNotePaymentID = $request->getPost('debitNotePaymentID');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $username = trim($request->getPost('username'));
            $password = trim($request->getPost('password'));
            $debitNotePaymentCancelMessage = $request->getPost('debitNotePaymentCancelMessage');
            $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
            $debitNotePaymentdata = (object) $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentID($debitNotePaymentID)->current();
            $this->setLogMessage("Error occured when delete debit note payment ".$debitNotePaymentdata->debitNotePaymentCode);
            if ($user) {
                if ($user->roleID == '1') {
                    $passwordData = explode(':', $user->userPassword);
                    $storedPassword = $passwordData[0];
                    $checkPassword = md5($password . $passwordData[1]);

                    if ($storedPassword == $checkPassword) {
                        $this->beginTransaction();

                        $supplierAddCreditBalance = 0.00;
                        $debitNotePaymentDetailsdata = $this->CommonTable('Inventory\Model\DebitNotePaymentDetailsTable')->getDebitNotePaymentDetailsByDebitNotePaymentID($debitNotePaymentID);
                        foreach ($debitNotePaymentDetailsdata as $data) {
                            $debitNoteNewSettledAmount = 0.00;
                            $data = (object) $data;
                            $supplierAddCreditBalance += $data->debitNotePaymentDetailsAmount;
                            $debitNoteID = $data->debitNoteID;
                            $debitNoteDetails = (object) $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteID($debitNoteID)->current();
                            $debitNoteNewSettledAmount = $debitNoteDetails->debitNoteSettledAmount - $data->debitNotePaymentDetailsAmount;
                            $debitNoteData = array(
                                'debitNoteID' => $debitNoteID,
                                'statusID' => 3,
                                'debitNoteSettledAmount' => $debitNoteNewSettledAmount,
                            );
                            $debitNote = new DebitNote;
                            $debitNote->exchangeArray($debitNoteData);
                            $this->CommonTable('Inventory\Model\DebitNoteTable')->updateDebitNoteSettleAmountAndStatusID($debitNote);
                            if ($data->paymentMethodID == 1) {
                                $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($debitNotePaymentdata->locationID)->current();
                                $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] - $data->debitNotePaymentDetailsAmount;
                                $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($debitNotePaymentdata->locationID, $newLocationCashInHand);
                            }
                        }
                        $supplierID = $debitNotePaymentdata->supplierID;
                        $supplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                        $supplierNewCurrentCredit = $supplierDetails->supplierCreditBalance + $supplierAddCreditBalance;

                        $supplierData = array(
                            'supplierCreditBalance' => $supplierNewCurrentCredit,
                            'supplierID' => $supplierID,
                        );
                        $supplierUpdateData = new Supplier;
                        $supplierUpdateData->exchangeArray($supplierData);
                        $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplierCredit($supplierUpdateData);

                        $entityID = $debitNotePaymentdata->entityID;
                        $updateResult = $this->updateDeleteInfoEntity($entityID);
                        $this->_updatePaymentStatus($debitNotePaymentID, $debitNotePaymentCancelMessage);
                        if ($updateResult) {
                            if ($this->useAccounting == 1) {
                                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('18', $debitNotePaymentID);

                                $journalEntryID = $journalEntryData['journalEntryID'];
                                if(!empty($journalEntryID)){
                                    $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);                   
                                    $i=0;
                                    $journalEntryAccounts = array();
                                    foreach ($jEAccounts as $key => $value) {
                                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
                                        $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
                                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
                                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
                                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Debit Note Payment Delete '.$debitNotePaymentdata->debitNotePaymentCode.'.';
                                        $i++;
                                    }

                    //get journal entry reference number.
                                    $jeresult = $this->getReferenceNoForLocation('30', $debitNotePaymentdata->locationID);
                                    $jelocationReferenceID = $jeresult['locRefID'];
                                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                                    $journalEntryData = array(
                                        'journalEntryAccounts' => $journalEntryAccounts,
                                        'journalEntryDate' => $this->convertDateToStandardFormat($this->getUserDateTime()),
                                        'journalEntryCode' => $JournalEntryCode,
                                        'journalEntryTypeID' => '',
                                        'journalEntryIsReverse' => 0,
                                        'journalEntryComment' => 'Journal Entry is posted when delete debit note payment '.$debitNotePaymentdata->debitNotePaymentCode.'.',
                                        'documentTypeID' => 18,
                                        'journalEntryDocumentID' => $debitNotePaymentID,
                                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                                        );

                                    $resultData = $this->saveJournalEntry($journalEntryData);

                                    if(!$resultData['status']){
                                        $this->rollback();
                                        $this->setLogMessage("Error Occred when delete Debit note payment ".$debitNotePaymentdata->debitNotePaymentCode.".");
                                        $this->status = false;
                                        $this->data = $resultData['data'];
                                        $this->msg = $resultData['msg'];
                                        return $this->JSONRespond();
                                    }

                                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($debitNotePaymentID,18, 5);
                                    if(!$jEDocStatusUpdate['status']){
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $jEDocStatusUpdate['msg'];
                                        return $this->JSONRespond();
                                    }

                                    $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(18,$debitNotePaymentID);
                                    $dimensionData = [];
                                    foreach ($jEDimensionData as $value) {
                                        if (!is_null($value['journalEntryID'])) {
                                            $temp = [];
                                            $temp['dimensionTypeId'] = $value['dimensionType'];
                                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                                            $dimensionData[$debitNotePaymentdata->debitNotePaymentCode][] = $temp;
                                        }
                                    }
                                    if (!empty($dimensionData)) {
                                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$debitNotePaymentdata->debitNotePaymentCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                        if(!$saveRes['status']){
                                            $this->rollback();
                                            $this->status = false;
                                            $this->msg = $saveRes['msg'];
                                            $this->data = $saveRes['data'];
                                            return $this->JSONRespond();
                                        }   
                                    }
                                }
                            }

                            $this->commit();
                            $this->setLogMessage("Debit note payment ".$debitNotePaymentdata->debitNotePaymentCode." deleted successfully.");
                            $this->status = true;
                            $this->data = true;
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->data = false;
                        }
                    } else {
                        $this->status = false;
                        $this->data = 'pass';
                    }
                } else {
                    $this->status = false;
                    $this->data = 'admin';
                }
            } else {
                $this->status = false;
                $this->data = 'user';
            }
        } else {
            $this->setLogMessage("Debit Note Payment delete request is not a post request");
            $this->status = false;
            $this->data = false;
        }
        return $this->JSONRespond();
    }

    private function _updatePaymentStatus($paymentID, $debitNotePaymentCancelMessage)
    {
        $cancelStatusID = $this->getStatusID('cancelled');
        $data = array(
            'statusID' => $cancelStatusID,
            'debitNotePaymentCancelMessage' => $debitNotePaymentCancelMessage
        );
        $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->update($data, $paymentID);
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Debit Note Payment';
        if ($request->isPost()) {
            $documentID = $request->getPost('documentID');
            $email = $request->getPost('to_email');
            $debitNotePayData = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentID($documentID)->current();
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_DNPAY_SENT_EMAIL');
            $this->setLogMessage($debitNotePayData['debitNotePaymentCode']." debit note payment document email to ".$email.'.');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_DNPAY_SENT_EMAIL');
        $this->setLogMessage("Error occured when email the debit note payment document.");
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search Debit note Payments For Dropdown
     * @return type
     */
    public function searchDebitNotePaymentForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $debitnotePayments = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentsByLocatioIDWithDeleted($locationID, $searchKey);

            $$debitnotePaymentList = array();
            foreach ($debitnotePayments as $debitnotePayment) {
                $temp['value'] = $debitnotePayment['debitNotePaymentID'];
                $temp['text'] = $debitnotePayment['debitNotePaymentCode'];
                $debitnotePaymentList[] = $temp;
            }

            $this->data = array('list' => $debitnotePaymentList);
            $this->setLogMessage("Retrive debit note payments list for dropdown.");
            return $this->JSONRespond();
        }
    }

    public function getAllRelatedDocumentDetailsByDebitNotePaymentIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $debitNotePaymentID = $request->getPost('debitNotePaymentID');

            $debitNotePaymentData = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentID($debitNotePaymentID)->current();

            if (isset($debitNotePaymentData)) {
                $dNPaymentData = array(
                    'type' => 'DebitNotePayment',
                    'documentID' => $debitNotePaymentData['debitNotePaymentID'],
                    'code' => $debitNotePaymentData['debitNotePaymentCode'],
                    'amount' => number_format($debitNotePaymentData['debitNotePaymentAmount'], 2),
                    'issuedDate' => $debitNotePaymentData['debitNotePaymentDate'],
                    'created' => $debitNotePaymentData['createdTimeStamp'],
                );
                $historyData[] = $dNPaymentData;
                if (isset($debitNotePaymentData)) {
                    $dataExistsFlag = true;
                }

            }

            $debitNotePaymentRelatedDebitNoteDetails = $this->CommonTable('Inventory\Model\DebitNotePaymentDetailsTable')->getDebitNotePaymentDetailsByDebitNotePaymentID($debitNotePaymentID);

            foreach ($debitNotePaymentRelatedDebitNoteDetails as $key => $dnp) {

                $debitNoteID = $dnp['debitNoteID'];
                $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteDetailsByDebitNoteId($debitNoteID);
                $piData = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataByDebitNoteId($debitNoteID);
                $piRelatedPoData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPiRelatedPODetailsByDebitNoteId($debitNoteID);
                $piRelatedGrnData = $this->CommonTable('Inventory\Model\GrnTable')->getPiRelatedGrnDetailsByDebitNoteId($debitNoteID);
                $grnRelatedPoData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getGrnRelatedPODetailsByDebitNoteId($debitNoteID);
                $grnRelatedPrData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPRDetailsByDebitNoteId($debitNoteID);
                $piPaymentData = $this->CommonTable('SupplierPaymentsTable')->getPiPaymentDetailsByPrDebitNoteId($debitNoteID);

                $dataExistsFlag = false;
                if ($debitNoteData) {
                    $debitNoteData = array(
                        'type' => 'DebitNote',
                        'documentID' => $debitNoteData['debitNoteID'],
                        'code' => $debitNoteData['debitNoteCode'],
                        'amount' => number_format($debitNoteData['debitNoteTotal'], 2),
                        'issuedDate' => $debitNoteData['debitNoteDate'],
                        'created' => $debitNoteData['createdTimeStamp'],
                    );
                    $historyData[] = $debitNoteData;
                    $dataExistsFlag = true;
                }
                if (isset($piData)) {
                    foreach ($piData as $piDirectDta) {
                        $piDeta = array(
                            'type' => 'PurchaseInvoice',
                            'documentID' => $piDirectDta['purchaseInvoiceID'],
                            'code' => $piDirectDta['purchaseInvoiceCode'],
                            'amount' => number_format($piDirectDta['purchaseInvoiceTotal'], 2),
                            'issuedDate' => $piDirectDta['purchaseInvoiceIssueDate'],
                            'created' => $piDirectDta['createdTimeStamp'],
                        );
                        $historyData[] = $piDeta;
                        if (isset($piDirectDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($piRelatedPoData)) {
                    foreach ($piRelatedPoData as $poDta) {
                        $pOrderData = array(
                            'type' => 'PurchaseOrder',
                            'documentID' => $poDta['purchaseOrderID'],
                            'code' => $poDta['purchaseOrderCode'],
                            'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                            'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                            'created' => $poDta['createdTimeStamp'],
                        );
                        $historyData[] = $pOrderData;
                        if (isset($prDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                if (isset($grnRelatedPoData)) {
                    foreach ($grnRelatedPoData as $poDta) {
                        $pOrderData = array(
                            'type' => 'PurchaseOrder',
                            'documentID' => $poDta['purchaseOrderID'],
                            'code' => $poDta['purchaseOrderCode'],
                            'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                            'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                            'created' => $poDta['createdTimeStamp'],
                        );
                        $historyData[] = $pOrderData;
                        if (isset($prDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                if (isset($piRelatedGrnData)) {
                    foreach ($piRelatedGrnData as $grDta) {
                        $grnDta = array(
                            'type' => 'Grn',
                            'documentID' => $grDta['grnID'],
                            'code' => $grDta['grnCode'],
                            'amount' => number_format($grDta['grnTotal'], 2),
                            'issuedDate' => $grDta['grnDate'],
                            'created' => $grDta['createdTimeStamp'],
                        );
                        $historyData[] = $grnDta;
                        if (isset($grDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                
                
                if (isset($grnRelatedPrData)) {
                    foreach ($grnRelatedPrData as $prData) {
                        $prDta = array(
                            'type' => 'purchaseReturn',
                            'documentID' => $prData['purchaseReturnID'],
                            'code' => $prData['purchaseReturnCode'],
                            'amount' => number_format($prData['purchaseReturnTotal'], 2),
                            'issuedDate' => $prData['purchaseReturnDate'],
                            'created' => $prData['createdTimeStamp'],
                        );
                        $historyData[] = $prDta;
                        if (isset($prData)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                
                if (isset($piPaymentData)) {
                    foreach ($piPaymentData as $piPayDta) {
                        $piPaymentData = array(
                            'type' => 'SupplierPayment',
                            'documentID' => $piPayDta['outgoingPaymentID'],
                            'code' => $piPayDta['outgoingPaymentCode'],
                            'amount' => number_format($piPayDta['outgoingPaymentAmount'], 2),
                            'issuedDate' => $piPayDta['outgoingPaymentDate'],
                            'created' => $piPayDta['createdTimeStamp'],
                        );
                        $historyData[] = $piPaymentData;
                        if (isset($piPayDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

               
            }
 
            $sortData = Array();
            foreach ($historyData as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $historyData);

            $documentDetails = array(
                'historyData' => $historyData
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }


}

/////////////////// END OF Product CONTROLLER \\\\\\\\\\\\\\\\\\\\



