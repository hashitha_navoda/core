<?php

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Inventory\Model\PurchaseReturn;
use Inventory\Model\PurchaseReturnProduct;
use Inventory\Model\PurchaseReturnProductTax;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemOut;

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */
class PurchaseReturnsController extends CoreController
{

    protected $paginator;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This is the purchase returns create function
     */
    public function getPrNoForLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $locationDetails = $this->CommonTable('Core\Model\LocationTable')->getLocation($locationID);
            $refData = $this->getReferenceNoForLocation(15, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];
            if ($rid == '' || $rid == NULL) {
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_PURRETURN_REFLOC');
                } else {
                    $this->msg = $this->getMessage('ERR_PURRETURN_REFMAX');
                }
                $this->data = FALSE;
                $this->status = false;
            } else {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $rid,
                    'locRefID' => $lrefID,
                );
            }
            $this->setLogMessage("Retrive purchase return reference for location ".$locationDetails->locationName);
            return $this->JSONRespond();
        }
    }

    public function savePurchaseReturnAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $statusID = $this->getStatusID('open');
            $entityID = $this->createEntity();
            $prCode = $post['prC'];
            $locationReferenceID = $post['lRID'];
            $locationID = $post['prL'];
            $supplierID = $post['sID'];
            $grnCode = explode('-', $post['gCode'])[0];
            $grnStatus = $this->CommonTable('Inventory\Model\GrnTable')->checkGrnnByCode(null, $post['gID'])->current();
            
            $this->beginTransaction();
            if (!$grnStatus) {
                $this->setLogMessage("Error occured when create purchase return ".$prCode.'.');
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_GRN_CODE_CLOSE_OR_EDIT');
                return $this->JSONRespond();
            }
            while ($prCode) {
                if ($this->CommonTable('Inventory\Model\PurchaseReturnTable')->checkPrByCode($prCode)->current()) {
                    if ($locationReferenceID) {
                        $newPrCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newPrCode == $prCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $prCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $prCode = $newPrCode;
                        }
                    } else {
                        $this->setLogMessage("Error occured when create purchase return ".$prCode.'.');
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PURRETURN_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }

            $this->setLogMessage("Error occured when create purchase return ".$prCode.'.');

            $prData = array(
                'purchaseReturnCode' => $prCode,
                'purchaseReturnGrnID' => $post['gID'],
                'purchaseReturnDate' => $this->convertDateToStandardFormat($post['prD']),
                'purchaseReturnComment' => $post['cm'],
                'purchaseReturnShowTax' => $post['sT'],
                'purchaseReturnTotal' => $post['fT'],
                'purchaseReturnStatus' => $statusID,
                'purchaseReturnEntityID' => $entityID,
                'prSupplierID' => $supplierID,
                'prLocationID' => $locationID,
            );

            $prM = new PurchaseReturn();
            $prM->exchangeArray($prData);
            $insertedPrID = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->savePr($prM);
            //check whether the purchase retun save or not
            if(!$insertedPrID){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                return $this->JSONRespond();
            }

            $normalPTot = [];
            foreach ($post['prRP'] as $returnProduct) {
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($returnProduct['locationPID']);
                $productData = $this->CommonTable('Inventory\Model\ProductTable')->getProduct($returnProduct['pID']);
                $averageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $discountValue = $returnProduct['pUnitPrice'] * $returnProduct['pDiscount'] / 100;
                if (array_key_exists('bProducts', $returnProduct) && array_key_exists('sProducts', $returnProduct)) {
                    //Create Purchase Return Product for products that have both batch and serial
                    foreach ($returnProduct['sProducts'] as $serialProduct) {
                        $thisSerialBatchCode = $serialProduct['sBCode'];
                        $thisSerialBatchID = $returnProduct['bProducts'][$thisSerialBatchCode]['bID'];
                        $subProductQty = $returnProduct['bProducts'][$thisSerialBatchCode]['bRQty'];
                        $prProductData = array(
                            'purchaseReturnID' => $insertedPrID,
                            'purchaseReturnLocationProductID' => $returnProduct['locationPID'],
                            'purchaseReturnProductBatchID' => $thisSerialBatchID,
                            'purchaseReturnProductSerialID' => $serialProduct['sID'],
                            'purchaseReturnProductPrice' => $returnProduct['pUnitPrice'],
                            'purchaseReturnProductDiscount' => $returnProduct['pDiscount'],
                            'purchaseReturnProductPurchasedQty' => $returnProduct['pQuantity'],
                            'purchaseReturnProductReturnedQty' => $returnProduct['pReturnQuantity'],
                            'purchaseReturnSubProductReturnedQty' => $subProductQty,
                            'purchaseReturnProductTotal' => $returnProduct['pTotal'],
                            'purchaseReturnProductGrnProductID' => $returnProduct['thisItemGrnProID'],
                        );
                        $prProductM = new PurchaseReturnProduct();
                        $prProductM->exchangeArray($prProductData);
                        $insertedPrProID = $this->CommonTable('Inventory\Model\PurchaseReturnProductTable')->savePurchaseReturnProduct($prProductM);
                        
                        //check wheather the Purchase return product data is save or not
                        if(!$insertedPrProID){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                            return $this->JSONRespond();
                        }

                        //update batch and serial products
                        $serialUpdateData = array(
                            'productSerialReturned' => '1'
                        );
                        $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialUpdateData, $serialProduct['sID']);
                        $this->updateBatchProductQuantity($thisSerialBatchID, 1);
                        ////Add items to item out table ,Serial and batch/////
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($returnProduct['locationPID'], $thisSerialBatchID, $serialProduct['sID']);
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Purchase Return',
                            'itemOutDocumentID' => $insertedPrID,
                            'itemOutLocationProductID' => $returnProduct['locationPID'],
                            'itemOutBatchID' => $thisSerialBatchID,
                            'itemOutSerialID' => $serialProduct['sID'],
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => 1,
                            'itemOutPrice' => $returnProduct['pUnitPrice'],
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDiscount' => $discountValue,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );
                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $updateQty = $itemInDetails['itemInSoldQty'] + 1;
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        ///////////
                        //save tax for current return product
                        if (array_key_exists('pTax', $returnProduct)) {
                            foreach ($returnProduct['pTax'] as $taxKey => $productTax) {
                                $prProTaxData = array(
                                    'purchaseReturnID' => $insertedPrID,
                                    'purchaseReturnProductID' => $insertedPrProID,
                                    'purchaseReturnTaxID' => $taxKey,
                                    'purchaseReturnTaxPrecentage' => $productTax['pTP'],
                                    'purchaseReturnTaxAmount' => $productTax['pTA'],
                                );
                                $prPTaxM = new PurchaseReturnProductTax();
                                $prPTaxM->exchangeArray($prProTaxData);
                                $this->CommonTable('Inventory\Model\PurchaseReturnProductTaxTable')->savePurchaseReturnProductTax($prPTaxM);
                            }
                        }
                    }
                } else if (array_key_exists('bProducts', $returnProduct)) {
                    //Create Purchase Return Product for products that have batch products only
                    foreach ($returnProduct['bProducts'] as $batchProduct) {
                        $prProductData = array(
                            'purchaseReturnID' => $insertedPrID,
                            'purchaseReturnLocationProductID' => $returnProduct['locationPID'],
                            'purchaseReturnProductBatchID' => $batchProduct['bID'],
                            'purchaseReturnProductPrice' => $returnProduct['pUnitPrice'],
                            'purchaseReturnProductDiscount' => $returnProduct['pDiscount'],
                            'purchaseReturnProductPurchasedQty' => $returnProduct['pQuantity'],
                            'purchaseReturnProductReturnedQty' => $returnProduct['pReturnQuantity'],
                            'purchaseReturnSubProductReturnedQty' => $batchProduct['bRQty'],
                            'purchaseReturnProductTotal' => $returnProduct['pTotal'],
                            'purchaseReturnProductGrnProductID' => $returnProduct['thisItemGrnProID'],
                        );
                        $prProductM = new PurchaseReturnProduct();
                        $prProductM->exchangeArray($prProductData);
                        $insertedPrProID = $this->CommonTable('Inventory\Model\PurchaseReturnProductTable')->savePurchaseReturnProduct($prProductM);
                        
                        //check wheather the Purchase return product data is save or not
                        if(!$insertedPrProID){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                            return $this->JSONRespond();
                        }

                        //update batch products
                        $this->updateBatchProductQuantity($batchProduct['bID'], $batchProduct['bRQty']);
                        ////add details for item out batch products/////
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($returnProduct['locationPID'], $batchProduct['bID']);
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Purchase Return',
                            'itemOutDocumentID' => $insertedPrID,
                            'itemOutLocationProductID' => $returnProduct['locationPID'],
                            'itemOutBatchID' => $batchProduct['bID'],
                            'itemOutSerialID' => NULL,
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => $batchProduct['bRQty'],
                            'itemOutPrice' => $returnProduct['pUnitPrice'],
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDiscount' => $discountValue,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );
                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);

                        $remainQty = $itemInDetails['itemInQty'] - $itemInDetails['itemInSoldQty'];


                        if ($remainQty < $batchProduct['bRQty']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_RTRN_NOT_ENOUGH_QTY',array($productData->productCode.' - '.$productData->productName));;
                            return $this->JSONRespond();
                        }


                        $updateQty = $itemInDetails['itemInSoldQty'] + $batchProduct['bRQty'];
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        ////////////////
                        //save tax for current return product
                        if (array_key_exists('pTax', $returnProduct)) {
                            foreach ($returnProduct['pTax'] as $taxKey => $productTax) {
                                $prProTaxData = array(
                                    'purchaseReturnID' => $insertedPrID,
                                    'purchaseReturnProductID' => $insertedPrProID,
                                    'purchaseReturnTaxID' => $taxKey,
                                    'purchaseReturnTaxPrecentage' => $productTax['pTP'],
                                    'purchaseReturnTaxAmount' => $productTax['pTA'],
                                );
                                $prPTaxM = new PurchaseReturnProductTax();
                                $prPTaxM->exchangeArray($prProTaxData);
                                $this->CommonTable('Inventory\Model\PurchaseReturnProductTaxTable')->savePurchaseReturnProductTax($prPTaxM);
                            }
                        }
                    }
                } else if (array_key_exists('sProducts', $returnProduct)) {
                    //Create Purchase Return Product for products that have serial products only
                    foreach ($returnProduct['sProducts'] as $serialProduct) {
                        $prProductData = array(
                            'purchaseReturnID' => $insertedPrID,
                            'purchaseReturnLocationProductID' => $returnProduct['locationPID'],
                            'purchaseReturnProductSerialID' => $serialProduct['sID'],
                            'purchaseReturnProductPrice' => $returnProduct['pUnitPrice'],
                            'purchaseReturnProductDiscount' => $returnProduct['pDiscount'],
                            'purchaseReturnProductPurchasedQty' => $returnProduct['pQuantity'],
                            'purchaseReturnProductReturnedQty' => $returnProduct['pReturnQuantity'],
                            'purchaseReturnSubProductReturnedQty' => '1',
                            'purchaseReturnProductTotal' => $returnProduct['pTotal'],
                            'purchaseReturnProductGrnProductID' => $serialProduct['sGrnProId'],
                        );
                        $prProductM = new PurchaseReturnProduct();
                        $prProductM->exchangeArray($prProductData);
                        $insertedPrProID = $this->CommonTable('Inventory\Model\PurchaseReturnProductTable')->savePurchaseReturnProduct($prProductM);
                        
                        //check wheather the Purchase return product data is save or not
                        if(!$insertedPrProID){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                            return $this->JSONRespond();
                        }

                        //update Serial Products
                        $serialUpdateData = array(
                            'productSerialReturned' => '1'
                        );
                        $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialUpdateData, $serialProduct['sID']);
                        //////add details for item out table serieal products only///////////
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($returnProduct['locationPID'], $serialProduct['sID']);
                        if (empty($itemInDetails)) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_ITM_ALRDY_RTRN',array($productData->productCode.' - '.$productData->productName));;
                            return $this->JSONRespond();
                        }
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Purchase Return',
                            'itemOutDocumentID' => $insertedPrID,
                            'itemOutLocationProductID' => $returnProduct['locationPID'],
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => $serialProduct['sID'],
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => 1,
                            'itemOutPrice' => $returnProduct['pUnitPrice'],
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDiscount' => $discountValue,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );

                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);


                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);
                        ////////////////////////
                        //save tax for current return product
                        if (array_key_exists('pTax', $returnProduct)) {
                            foreach ($returnProduct['pTax'] as $taxKey => $productTax) {
                                $prProTaxData = array(
                                    'purchaseReturnID' => $insertedPrID,
                                    'purchaseReturnProductID' => $insertedPrProID,
                                    'purchaseReturnTaxID' => $taxKey,
                                    'purchaseReturnTaxPrecentage' => $productTax['pTP'],
                                    'purchaseReturnTaxAmount' => $productTax['pTA'],
                                );
                                $prPTaxM = new PurchaseReturnProductTax();
                                $prPTaxM->exchangeArray($prProTaxData);
                                $this->CommonTable('Inventory\Model\PurchaseReturnProductTaxTable')->savePurchaseReturnProductTax($prPTaxM);
                            }
                        }
                    }
                } else {
                    //Create Purchase Return Product for products that don't have either batch or serial products
                    $prProductData = array(
                        'purchaseReturnID' => $insertedPrID,
                        'purchaseReturnLocationProductID' => $returnProduct['locationPID'],
                        'purchaseReturnProductPrice' => $returnProduct['pUnitPrice'],
                        'purchaseReturnProductDiscount' => $returnProduct['pDiscount'],
                        'purchaseReturnProductPurchasedQty' => $returnProduct['pQuantity'],
                        'purchaseReturnProductReturnedQty' => $returnProduct['pReturnQuantity'],
                        'purchaseReturnProductTotal' => $returnProduct['pTotal'],
                        'purchaseReturnProductGrnProductID' => $returnProduct['thisItemGrnProID'],
                    );
                    $prProductM = new PurchaseReturnProduct();
                    $prProductM->exchangeArray($prProductData);
                    $insertedPrProID = $this->CommonTable('Inventory\Model\PurchaseReturnProductTable')->savePurchaseReturnProduct($prProductM);
                    
                    //check wheather the Purchase return product data is save or not
                    if(!$insertedPrProID){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                        return $this->JSONRespond();
                    }


                    //////add details to item out table non batch and non serial product//////
                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetailsByDocument($returnProduct['locationPID'], 'Goods Received Note', $post['gID']);
                    ////check whether items were sold or enough to return

                    $sellingQty = $returnProduct['pReturnQuantity'];
                    if ($itemInDetails['itemInRemainingQty'] != 0) {

                        $updatedQty = 0;

                        if ($itemInDetails['itemInRemainingQty'] == $returnProduct['pReturnQuantity']) {
                            $updatedQty = $returnProduct['pReturnQuantity'];
                        } elseif ($itemInDetails['itemInRemainingQty'] < $returnProduct['pReturnQuantity']) {
                            $updatedQty = $itemInDetails['itemInRemainingQty'];
                        } elseif ($itemInDetails['itemInRemainingQty'] > $returnProduct['pReturnQuantity']) {
                            $updatedQty = $returnProduct['pReturnQuantity'];
                        }
                   
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Purchase Return',
                            'itemOutDocumentID' => $insertedPrID,
                            'itemOutLocationProductID' => $returnProduct['locationPID'],
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => NULL,
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => $updatedQty,
                            'itemOutPrice' => $returnProduct['pUnitPrice'],
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDiscount' => $discountValue,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );
                        $updateQty = $itemInDetails['itemInSoldQty'] + $updatedQty;
                        $normalPTot[$returnProduct['pID']]['itemTot'] += $updatedQty * floatval($itemInDetails['itemInPrice']);
                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        if ($itemInDetails['itemInRemainingQty'] == $returnProduct['pReturnQuantity']) {
                            $sellingQty = 0;
                        } else {

                            $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                        }

                    }

                    if ($sellingQty > 0) {

                        while ($sellingQty != 0) {
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($returnProduct['locationPID']);

                            if (!empty($itemInDetails)) {

                                if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                                    $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                                    $itemOutM = new ItemOut();
                                    $itemOutData = array(
                                        'itemOutDocumentType' => 'Purchase Return',
                                        'itemOutDocumentID' => $insertedPrID,
                                        'itemOutLocationProductID' => $returnProduct['locationPID'],
                                        'itemOutBatchID' => NULL,
                                        'itemOutSerialID' => NULL,
                                        'itemOutItemInID' => $itemInDetails['itemInID'],
                                        'itemOutQty' => $sellingQty,
                                        'itemOutPrice' => $itemInDetails['itemInPrice'],
                                        'itemOutAverageCostingPrice' => $averageCostingPrice,
                                        'itemOutDiscount' => $itemInDetails['itemInDiscount'],
                                        'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                    $itemOutM->exchangeArray($itemOutData);
                                    $normalPTot[$returnProduct['pID']]['itemTot'] += $sellingQty * floatval($itemInDetails['itemInPrice']);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    $sellingQty = 0;
                                    break;
                                } else {
                                    $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                                    $itemOutM = new ItemOut();
                                    $itemOutData = array(
                                        'itemOutDocumentType' => 'Purchase Return',
                                        'itemOutDocumentID' => $insertedPrID,
                                        'itemOutLocationProductID' => $returnProduct['locationPID'],
                                        'itemOutBatchID' => NULL,
                                        'itemOutSerialID' => NULL,
                                        'itemOutItemInID' => $itemInDetails['itemInID'],
                                        'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                        'itemOutPrice' => $itemInDetails['itemInPrice'],
                                        'itemOutAverageCostingPrice' => $averageCostingPrice,
                                        'itemOutDiscount' => $itemInDetails['itemInDiscount'],
                                        'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                    $itemOutM->exchangeArray($itemOutData);
                                    $normalPTot[$returnProduct['pID']]['itemTot'] += $itemInDetails['itemInRemainingQty'] * floatval($itemInDetails['itemInPrice']);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                                }
                            } else {
                                if ($sellingQty != 0) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_RTRN_NOT_ENOUGH_QTY_PRO',array($productData->productCode.' - '.$productData->productName));;
                                    return $this->JSONRespond();
                                }
                            }
                        }

                    }
                           
                    //save tax for current return product
                    if (array_key_exists('pTax', $returnProduct)) {
                        foreach ($returnProduct['pTax'] as $taxKey => $productTax) {
                            $prProTaxData = array(
                                'purchaseReturnID' => $insertedPrID,
                                'purchaseReturnProductID' => $insertedPrProID,
                                'purchaseReturnTaxID' => $taxKey,
                                'purchaseReturnTaxPrecentage' => $productTax['pTP'],
                                'purchaseReturnTaxAmount' => $productTax['pTA'],
                            );
                            $prPTaxM = new PurchaseReturnProductTax();
                            $prPTaxM->exchangeArray($prProTaxData);
                            $this->CommonTable('Inventory\Model\PurchaseReturnProductTaxTable')->savePurchaseReturnProductTax($prPTaxM);
                        }
                    }
                }

                if ($returnProduct['productType'] == 2) {
                    $returnProduct['pReturnQuantity'] = 0;
                }
                $this->updateLocationProductQty($returnProduct['locationPID'], $returnProduct['pReturnQuantity']);
            }
            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }

            // call product updated event
            $productIDs = array_map(function($element) {
                return $element['pID'];
            }, $post['prRP']);

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            //check and update grn status $post['gID']
            $this->checkAndUpdateGrnStatus($post['gID']);

            if ($this->useAccounting == 1) {

                //check whether supplier GRN Clearing account is set or not
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                if(empty($supplierData->supplierGrnClearingAccountID)){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_GRN_SUPPLIER_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                    return $this->JSONRespond();
                }
                $GRNClearingAccountID = $supplierData->supplierGrnClearingAccountID;

                $accountProduct = array();
                foreach ($post['prRP'] as $product) {
                    //check whether Product Inventory Gl account id set or not
                    $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($product['pID']);
                    if(empty($pData['productInventoryAccountID'])){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                        return $this->JSONRespond();
                    }

                    //calculate product total without tax potion.
                    if (!array_key_exists('bProducts', $product) && !array_key_exists('sProducts', $product)) {
                        $productTotal = $normalPTot[$product['pID']]['itemTot'] * ((100 - $product['pDiscount']) / 100);
                    } else {
                        $productTotal = $product['pUnitPrice']*$product['pReturnQuantity'] * ((100 - $product['pDiscount']) / 100);
                    }
                    
                    //set gl accounts for the journal entry
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['total'])){
                        $accountProduct[$pData['productInventoryAccountID']]['total'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['total'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
                    }
                }

                //create data array for the journal entry save.
                $i=0;
                $journalEntryAccounts = array();
                $totalValueForSupplierAccount = 0;

                foreach ($accountProduct as $key => $value) {
                    $totalValueForSupplierAccount+=$value['total'];
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['total'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Return '.$prCode.".";
                    $i++;
                }

                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $GRNClearingAccountID;
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $totalValueForSupplierAccount;
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Purchase Return '.$prCode;

                //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' =>$this->convertDateToStandardFormat($post['prD']),
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create Purchase Return '.$prCode.".",
                    'documentTypeID' => 11,
                    'journalEntryDocumentID' => $insertedPrID,
                    'ignoreBudgetLimit' => $post['ignoreBudgetLimit'],
                );

                $resultData = $this->saveJournalEntry($journalEntryData);
                if($resultData['status']){
                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($insertedPrID,11, 3);
                    if(!$jEDocStatusUpdate['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg =$jEDocStatusUpdate['msg'];
                        return $this->JSONRespond();
                    }

                    $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(10,$post['gID']);
                    $dimensionData = [];
                    foreach ($jEDimensionData as $value) {
                        if (!is_null($value['journalEntryID'])) {
                            $temp = [];
                            $temp['dimensionTypeId'] = $value['dimensionType'];
                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                            $dimensionData[$prCode][] = $temp;
                        }
                    }
                    if (!empty($dimensionData)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$prCode], $resultData['data']['journalEntryID'], $post['ignoreBudgetLimit'], $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->msg =$saveRes['msg'];
                            $this->data =$saveRes['data'];
                            return $this->JSONRespond();
                        }   
                    }

                    $this->commit();
                    $this->status = true;
                    $this->data = array('prID' => $insertedPrID);
                    $this->msg = $this->getMessage('SUCC_PURRETURN_CREATE', array($prCode));
                    $this->setLogMessage($this->companyCurrencySymbol . $post['fT'] . ' amount purchase Return successfully created - ' . $prCode.' againts GRN -'.$grnCode.' .');
            
                }else{
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $resultData['msg'];
                    $this->data = $resultData['data'];
                }
            }else{
                $this->commit();
                $this->status = true;
                $this->data = array('prID' => $insertedPrID);
                $this->msg = $this->getMessage('SUCC_PURRETURN_CREATE', array($prCode));
                $this->setLogMessage($this->companyCurrencySymbol . $post['fT'] . ' amount purchase Return successfully created - ' . $prCode.' againts GRN -'.$grnCode.' .');
            }

            return $this->JSONRespond();
        }
    }


    public function saveDirectPurchaseReturnAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $statusID = $this->getStatusID('open');
            $entityID = $this->createEntity();
            $prCode = $post['prC'];
            $locationReferenceID = $post['lRID'];
            $locationID = $post['prL'];
            $supplierID = $post['sID'];
            $dimensionData = $post['dimensionData'];
            $ignoreBudgetLimit = $post['ignoreBudgetLimit'];
            
            $this->beginTransaction();
            
            while ($prCode) {
                if ($this->CommonTable('Inventory\Model\PurchaseReturnTable')->checkPrByCode($prCode)->current()) {
                    if ($locationReferenceID) {
                        $newPrCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newPrCode == $prCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $prCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $prCode = $newPrCode;
                        }
                    } else {
                        $this->setLogMessage("Error occured when create direct purchase return ".$prCode.'.');
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PURRETURN_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }

            $this->setLogMessage("Error occured when create purchase return ".$prCode.'.');

            $prData = array(
                'purchaseReturnCode' => $prCode,
                'purchaseReturnGrnID' => NULL,
                'purchaseReturnDate' => $this->convertDateToStandardFormat($post['prD']),
                'purchaseReturnComment' => $post['cm'],
                'purchaseReturnShowTax' => $post['sT'],
                'purchaseReturnTotal' => $post['fT'],
                'purchaseReturnStatus' => $statusID,
                'purchaseReturnEntityID' => $entityID,
                'directReturnFlag' => true,
                'prSupplierID' => $supplierID,
                'prLocationID' => $locationID,
            );

            $prM = new PurchaseReturn();
            $prM->exchangeArray($prData);
            $insertedPrID = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->savePr($prM);
            //check whether the purchase retun save or not
            if(!$insertedPrID){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                return $this->JSONRespond();
            }

            $products = Array();
            foreach ($post['prRP'] as $key1 => $value1) {
                $products[$key1] = $value1;
            }
            foreach ($post['prRPSub'] as $key2 => $value2) {
                $i=0;
                foreach ($value2 as $ke => $val) {
                    if (!is_null($val['serialID'])) {
                        $products[$key2]['sProducts'][$val['serialID']]['sID'] = $val['serialID'];
                        $products[$key2]['sProducts'][$val['serialID']]['bID'] = $val['batchID'];
                        $products[$key2]['sProducts'][$val['serialID']]['sRQty'] = 1;
                        if (!is_null($val['batchID'])) {
                            $i++;
                            $products[$key2]['bProducts'][$val['batchID']]['bID'] = $val['batchID'];
                            $products[$key2]['bProducts'][$val['batchID']]['bRQty'] = $i;
                        }
                    } else {
                        $products[$key2]['bProducts'][$val['batchID']]['bID'] = $val['batchID'];
                        $products[$key2]['bProducts'][$val['batchID']]['bRQty'] = $val['qtyByBase'];
                    }
                }
            }

            foreach ($products as $returnProduct) {
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($returnProduct['locationProductID']);
                $averageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $discountValue = $returnProduct['productPrice'] * $returnProduct['productDiscount'] / 100;
                if (array_key_exists('bProducts', $returnProduct) && array_key_exists('sProducts', $returnProduct)) {
                    //Create Purchase Return Product for products that have both batch and serial
                    foreach ($returnProduct['sProducts'] as $serialProduct) {
                        $thisSerialBatchCode = $serialProduct['bID'];
                        $thisSerialBatchID = $returnProduct['bProducts'][$thisSerialBatchCode]['bID'];
                        $subProductQty = $returnProduct['bProducts'][$thisSerialBatchCode]['bRQty'];
                        $prProductData = array(
                            'purchaseReturnID' => $insertedPrID,
                            'purchaseReturnLocationProductID' => $returnProduct['locationProductID'],
                            'purchaseReturnProductBatchID' => $thisSerialBatchID,
                            'purchaseReturnProductSerialID' => $serialProduct['sID'],
                            'purchaseReturnProductPrice' => $returnProduct['productPrice'],
                            'purchaseReturnProductDiscount' => $returnProduct['productDiscount'],
                            'purchaseReturnProductPurchasedQty' => 0,
                            'purchaseReturnProductReturnedQty' => $returnProduct['deliverQuantity']['qty'],
                            'purchaseReturnSubProductReturnedQty' => $subProductQty,
                            'purchaseReturnProductTotal' => $returnProduct['productTotal'],
                            'purchaseReturnProductGrnProductID' => NULL,
                        );
                        $prProductM = new PurchaseReturnProduct();
                        $prProductM->exchangeArray($prProductData);
                        $insertedPrProID = $this->CommonTable('Inventory\Model\PurchaseReturnProductTable')->savePurchaseReturnProduct($prProductM);
                        
                        //check wheather the Purchase return product data is save or not
                        if(!$insertedPrProID){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                            return $this->JSONRespond();
                        }

                        //update batch and serial products
                        $serialUpdateData = array(
                            'productSerialReturned' => '1'
                        );
                        $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialUpdateData, $serialProduct['sID']);
                        $this->updateBatchProductQuantity($thisSerialBatchID, 1);
                        ////Add items to item out table ,Serial and batch/////
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($returnProduct['locationProductID'], $thisSerialBatchID, $serialProduct['sID']);
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Direct Purchase Return',
                            'itemOutDocumentID' => $insertedPrID,
                            'itemOutLocationProductID' => $returnProduct['locationProductID'],
                            'itemOutBatchID' => $thisSerialBatchID,
                            'itemOutSerialID' => $serialProduct['sID'],
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => 1,
                            'itemOutPrice' => $returnProduct['productPrice'],
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDiscount' => $discountValue,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );
                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $updateQty = $itemInDetails['itemInSoldQty'] + 1;
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                        //save tax for current return product
                        if (array_key_exists('pTax', $returnProduct)) {
                            foreach ($returnProduct['pTax']['tL'] as $taxKey => $productTax) {
                                $prProTaxData = array(
                                    'purchaseReturnID' => $insertedPrID,
                                    'purchaseReturnProductID' => $insertedPrProID,
                                    'purchaseReturnTaxID' => $taxKey,
                                    'purchaseReturnTaxPrecentage' => $productTax['tP'],
                                    'purchaseReturnTaxAmount' => $productTax['tA'],
                                );
                                $prPTaxM = new PurchaseReturnProductTax();
                                $prPTaxM->exchangeArray($prProTaxData);
                                $this->CommonTable('Inventory\Model\PurchaseReturnProductTaxTable')->savePurchaseReturnProductTax($prPTaxM);
                            }
                        }
                    }
                } else if (array_key_exists('bProducts', $returnProduct)) {
                    //Create Purchase Return Product for products that have batch products only
                    foreach ($returnProduct['bProducts'] as $batchProduct) {
                        $prProductData = array(
                            'purchaseReturnID' => $insertedPrID,
                            'purchaseReturnLocationProductID' => $returnProduct['locationProductID'],
                            'purchaseReturnProductBatchID' => $batchProduct['bID'],
                            'purchaseReturnProductPrice' => $returnProduct['productPrice'],
                            'purchaseReturnProductDiscount' => $returnProduct['productDiscount'],
                            'purchaseReturnProductPurchasedQty' => 0,
                            'purchaseReturnProductReturnedQty' => $returnProduct['deliverQuantity']['qty'],
                            'purchaseReturnSubProductReturnedQty' => $batchProduct['bRQty'],
                            'purchaseReturnProductTotal' => $returnProduct['productTotal'],
                            'purchaseReturnProductGrnProductID' => NULL,
                        );
                        $prProductM = new PurchaseReturnProduct();
                        $prProductM->exchangeArray($prProductData);
                        $insertedPrProID = $this->CommonTable('Inventory\Model\PurchaseReturnProductTable')->savePurchaseReturnProduct($prProductM);
                        
                        //check wheather the Purchase return product data is save or not
                        if(!$insertedPrProID){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                            return $this->JSONRespond();
                        }

                        //update batch products
                        $this->updateBatchProductQuantity($batchProduct['bID'], $batchProduct['bRQty']);
                        ////add details for item out batch products/////
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($returnProduct['locationProductID'], $batchProduct['bID']);
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Direct Purchase Return',
                            'itemOutDocumentID' => $insertedPrID,
                            'itemOutLocationProductID' => $returnProduct['locationProductID'],
                            'itemOutBatchID' => $batchProduct['bID'],
                            'itemOutSerialID' => NULL,
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => $batchProduct['bRQty'],
                            'itemOutPrice' => $returnProduct['productPrice'],
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDiscount' => $discountValue,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );
                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $updateQty = $itemInDetails['itemInSoldQty'] + $batchProduct['bRQty'];
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                        //save tax for current return product
                        if (array_key_exists('pTax', $returnProduct)) {
                            foreach ($returnProduct['pTax']['tL'] as $taxKey => $productTax) {
                                $prProTaxData = array(
                                    'purchaseReturnID' => $insertedPrID,
                                    'purchaseReturnProductID' => $insertedPrProID,
                                    'purchaseReturnTaxID' => $taxKey,
                                    'purchaseReturnTaxPrecentage' => $productTax['tP'],
                                    'purchaseReturnTaxAmount' => $productTax['tA'],
                                );
                                $prPTaxM = new PurchaseReturnProductTax();
                                $prPTaxM->exchangeArray($prProTaxData);
                                $this->CommonTable('Inventory\Model\PurchaseReturnProductTaxTable')->savePurchaseReturnProductTax($prPTaxM);
                            }
                        }
                    }
                } else if (array_key_exists('sProducts', $returnProduct)) {
                    //Create Purchase Return Product for products that have serial products only
                    foreach ($returnProduct['sProducts'] as $serialProduct) {
                        $prProductData = array(
                            'purchaseReturnID' => $insertedPrID,
                            'purchaseReturnLocationProductID' => $returnProduct['locationProductID'],
                            'purchaseReturnProductSerialID' => $serialProduct['sID'],
                            'purchaseReturnProductPrice' => $returnProduct['productPrice'],
                            'purchaseReturnProductDiscount' => $returnProduct['productDiscount'],
                            'purchaseReturnProductPurchasedQty' => 0,
                            'purchaseReturnProductReturnedQty' => $returnProduct['deliverQuantity']['qty'],
                            'purchaseReturnSubProductReturnedQty' => '1',
                            'purchaseReturnProductTotal' => $returnProduct['productTotal'],
                            'purchaseReturnProductGrnProductID' => NULL,
                        );
                        $prProductM = new PurchaseReturnProduct();
                        $prProductM->exchangeArray($prProductData);
                        $insertedPrProID = $this->CommonTable('Inventory\Model\PurchaseReturnProductTable')->savePurchaseReturnProduct($prProductM);
                        
                        //check wheather the Purchase return product data is save or not
                        if(!$insertedPrProID){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                            return $this->JSONRespond();
                        }

                        //update Serial Products
                        $serialUpdateData = array(
                            'productSerialReturned' => '1'
                        );
                        $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialUpdateData, $serialProduct['sID']);
                        //////add details for item out table serieal products only///////////
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($returnProduct['locationProductID'], $serialProduct['sID']);
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Direct Purchase Return',
                            'itemOutDocumentID' => $insertedPrID,
                            'itemOutLocationProductID' => $returnProduct['locationProductID'],
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => $serialProduct['sID'],
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => 1,
                            'itemOutPrice' => $returnProduct['productPrice'],
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDiscount' => $discountValue,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );

                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);

                        //save tax for current return product
                        if (array_key_exists('pTax', $returnProduct)) {
                            foreach ($returnProduct['pTax']['tL'] as $taxKey => $productTax) {
                                $prProTaxData = array(
                                    'purchaseReturnID' => $insertedPrID,
                                    'purchaseReturnProductID' => $insertedPrProID,
                                    'purchaseReturnTaxID' => $taxKey,
                                    'purchaseReturnTaxPrecentage' => $productTax['tP'],
                                    'purchaseReturnTaxAmount' => $productTax['tA'],
                                );
                                $prPTaxM = new PurchaseReturnProductTax();
                                $prPTaxM->exchangeArray($prProTaxData);
                                $this->CommonTable('Inventory\Model\PurchaseReturnProductTaxTable')->savePurchaseReturnProductTax($prPTaxM);
                            }
                        }
                    }
                } else {
                    //Create Purchase Return Product for products that don't have either batch or serial products
                    $prProductData = array(
                        'purchaseReturnID' => $insertedPrID,
                        'purchaseReturnLocationProductID' => $returnProduct['locationProductID'],
                        'purchaseReturnProductPrice' => $returnProduct['productPrice'],
                        'purchaseReturnProductDiscount' => $returnProduct['productDiscount'],
                        'purchaseReturnProductPurchasedQty' => 0,
                        'purchaseReturnProductReturnedQty' => $returnProduct['deliverQuantity']['qty'],
                        'purchaseReturnProductTotal' => $returnProduct['productTotal'],
                        'purchaseReturnProductGrnProductID' => NULL,
                    );
                    $prProductM = new PurchaseReturnProduct();
                    $prProductM->exchangeArray($prProductData);
                    $insertedPrProID = $this->CommonTable('Inventory\Model\PurchaseReturnProductTable')->savePurchaseReturnProduct($prProductM);
                    
                    //check wheather the Purchase return product data is save or not
                    if(!$insertedPrProID){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_OCCURED_PURRETURN_CREATE');
                        return $this->JSONRespond();
                    }

                    
                    $sellingQty = $returnProduct['deliverQuantity']['qty'];
                    while ($sellingQty != 0) {
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($returnProduct['locationProductID']);
                        if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                            $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Direct Purchase Return',
                                'itemOutDocumentID' => $insertedPrID,
                                'itemOutLocationProductID' => $returnProduct['locationProductID'],
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $sellingQty,
                                'itemOutPrice' => $itemInDetails['itemInPrice'],
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDiscount' => $itemInDetails['itemInDiscount'],
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                            $sellingQty = 0;
                            break;
                        } else {
                            $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Direct Purchase Return',
                                'itemOutDocumentID' => $insertedPrID,
                                'itemOutLocationProductID' => $returnProduct['locationProductID'],
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                'itemOutPrice' => $itemInDetails['itemInPrice'],
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDiscount' => $itemInDetails['itemInDiscount'],
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                            $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                        }
                    }
                    
                    //save tax for current return product
                    if (array_key_exists('pTax', $returnProduct)) {
                        foreach ($returnProduct['pTax']['tL'] as $taxKey => $productTax) {
                            $prProTaxData = array(
                                'purchaseReturnID' => $insertedPrID,
                                'purchaseReturnProductID' => $insertedPrProID,
                                'purchaseReturnTaxID' => $taxKey,
                                'purchaseReturnTaxPrecentage' => $productTax['tP'],
                                'purchaseReturnTaxAmount' => $productTax['tA'],
                            );
                            $prPTaxM = new PurchaseReturnProductTax();
                            $prPTaxM->exchangeArray($prProTaxData);
                            $this->CommonTable('Inventory\Model\PurchaseReturnProductTaxTable')->savePurchaseReturnProductTax($prPTaxM);
                        }
                    }
                }

                if ($returnProduct['productType'] == 2) {
                    $returnProduct['deliverQuantity']['qty'] = 0;
                }
                $this->updateLocationProductQty($returnProduct['locationProductID'], $returnProduct['deliverQuantity']['qty']);
            }

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }

            // call product updated event
            $productIDs = array_map(function($element) {
                return $element['productID'];
            }, $post['prRP']);

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            if ($this->useAccounting == 1) {

                //check whether supplier GRN Clearing account is set or not
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
                if(empty($supplierData->supplierGrnClearingAccountID)){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_GRN_SUPPLIER_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                    return $this->JSONRespond();
                }
                $GRNClearingAccountID = $supplierData->supplierGrnClearingAccountID;

                $accountProduct = array();
                foreach ($post['prRP'] as $product) {
                    //check whether Product Inventory Gl account id set or not
                    $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($product['productID']);
                    if(empty($pData['productInventoryAccountID'])){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                        return $this->JSONRespond();
                    }

                    //calculate product total without tax potion.
                    $productTotal = $product['productPrice']*$product['deliverQuantity']['qty'] * ((100 - $product['productDiscount']) / 100);
                    
                    //set gl accounts for the journal entry
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['total'])){
                        $accountProduct[$pData['productInventoryAccountID']]['total'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['total'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['inventoryAccountID'] = $pData['productInventoryAccountID'];
                    }
                }

                //create data array for the journal entry save.
                $i=0;
                $journalEntryAccounts = array();
                $totalValueForSupplierAccount = 0;

                foreach ($accountProduct as $key => $value) {
                    $totalValueForSupplierAccount+=$value['total'];
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['inventoryAccountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = 0.00;
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['total'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Direct Purchase Return '.$prCode.".";
                    $i++;
                }

                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $GRNClearingAccountID;
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $totalValueForSupplierAccount;
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = 0.00;
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Direct Purchase Return '.$prCode;

                //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' =>$this->convertDateToStandardFormat($post['prD']),
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create Direct Purchase Return '.$prCode.".",
                    'documentTypeID' => 11,
                    'journalEntryDocumentID' => $insertedPrID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                );

                $resultData = $this->saveJournalEntry($journalEntryData);
                if($resultData['status']){
                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($insertedPrID,11, 3);
                    if(!$jEDocStatusUpdate['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg =$jEDocStatusUpdate['msg'];
                        return $this->JSONRespond();
                    }

                    if (!empty($dimensionData)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$prCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->msg =$saveRes['msg'];
                            $this->data =$saveRes['data'];
                            return $this->JSONRespond();
                        }   
                    }

                    $this->commit();
                    $this->status = true;
                    $this->data = array('prID' => $insertedPrID);
                    $this->msg = $this->getMessage('SUCC_PURRETURN_CREATE', array($prCode));
                    $this->setLogMessage($this->companyCurrencySymbol . $post['fT'] . ' amount purchase Return successfully created - ' . $prCode.' againts GRN -'.$grnCode.' .');
            
                }else{
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $resultData['msg'];
                    $this->data = $resultData['data'];
                }
            }else{
                $this->commit();
                $this->status = true;
                $this->data = array('prID' => $insertedPrID);
                $this->msg = $this->getMessage('SUCC_PURRETURN_CREATE', array($prCode));
                $this->setLogMessage($this->companyCurrencySymbol . $post['fT'] . ' amount Direct Purchase Return successfully created - ' . $prCode.'.');
            }

            return $this->JSONRespond();
        }
    }
    public function checkAndUpdateGrnStatus($grnID)
    {

        $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnID($grnID, TRUE);
        //get details of purchase return if any returns were made to the current GRN
        $prRelatedDetails = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnDetailsByGrnID($grnID);
        $prID;
        $prDetails = array();
        foreach ($prRelatedDetails as $prData) {
            $tempPr = array();
            $prID = $prData['purchaseReturnID'];
            $tempPr['prID'] = $prData['purchaseReturnID'];
            $tempPr['prCd'] = $prData['purchaseReturnCode'];
            $tempPr['prSID'] = $prData['grnSupplierID'];
            $tempPr['prSName'] = $prData['supplierTitle'] . ' ' . $prData['supplierName'];
            $tempPr['prSR'] = $prData['grnSupplierReference'];
            $tempPr['prRLID'] = $prData['grnRetrieveLocation'];
            $tempPr['prRL'] = $prData['locationCode'] . ' - ' . $prData['locationName'];
            $tempPr['prD'] = $prData['purchaseReturnDate'];
            $tempPr['prT'] = $prData['purchaseReturnTotal'];
            $tempPr['prC'] = $prData['purchaseReturnComment'];
            $prProducts = (isset($prDetails[$prID]['prProducts'])) ? $prDetails[$prID]['prProducts'] : array();
            if ($prData['productID'] != NULL) {
                $productTaxes = (isset($prProducts[$prData['purchaseReturnProductGrnProductID']]['pT'])) ? $prProducts[$prData['purchaseReturnProductGrnProductID']]['pT'] : array();
                $productBatches = (isset($prProducts[$prData['purchaseReturnProductGrnProductID']]['bP'])) ? $prProducts[$prData['purchaseReturnProductGrnProductID']]['bP'] : array();
                $productSerials = (isset($prProducts[$prData['purchaseReturnProductGrnProductID']]['sP'])) ? $prProducts[$prData['purchaseReturnProductGrnProductID']]['sP'] : array();
                $productUoms = (isset($prProducts[$prData['purchaseReturnProductGrnProductID']]['pUom'])) ? $prProducts[$prData['purchaseReturnProductGrnProductID']]['pUom'] : array();

                $prProducts[$prData['purchaseReturnProductGrnProductID']] = array('prPC' => $prData['productCode'], 'prPID' => $prData['productID'], 'prPN' => $prData['productName'], 'lPID' => $prData['purchaseReturnLocationProductID'], 'prPP' => $prData['purchaseReturnProductPrice'], 'prPD' => $prData['purchaseReturnProductDiscount'], 'prRPQ' => $prData['purchaseReturnProductReturnedQty'], 'prSRPQ' => $prData['purchaseReturnSubProductReturnedQty'], 'prPT' => $prData['purchaseReturnProductTotal'], 'prGPId' => $prData['purchaseReturnProductGrnProductID']);
                if ($prData['productBatchID'] != NULL) {
                    $productBatches[$prData['productBatchID']] = array('bC' => $prData['productBatchCode'], 'bED' => $prData['productBatchExpiryDate'], 'bW' => $prData['productBatchWarrantyPeriod'], 'bMD' => $prData['productBatchManufactureDate'], 'bRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                }
                if ($prData['productSerialID'] != NULL) {
                    $productSerials[$prData['productSerialID']] = array('sC' => $prData['productSerialCode'], 'sW' => $prData['productSerialWarrantyPeriod'], 'sED' => $prData['productSerialExpireDate'], 'sBC' => $prData['productBatchCode'], 'sRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                }
                if ($prData['purchaseReturnTaxID'] != NULL) {
                    $productTaxes[$prData['purchaseReturnTaxID']] = array('pTN' => $prData['taxName'], 'pTP' => $prData['purchaseReturnTaxPrecentage'], 'pTA' => $prData['purchaseReturnTaxAmount']);
                }
                if ($prData['uomID'] != NULL) {
                    $productUoms[$prData['uomID']] = array('pUom' => $prData['uomAbbr'], 'pUomID' => $prData['uomID'], 'pUomCon' => $prData['productUomConversion']);
                }
                $prProducts[$prData['purchaseReturnProductGrnProductID']]['pT'] = $productTaxes;
                $prProducts[$prData['purchaseReturnProductGrnProductID']]['bP'] = $productBatches;
                $prProducts[$prData['purchaseReturnProductGrnProductID']]['sP'] = $productSerials;
                $prProducts[$prData['purchaseReturnProductGrnProductID']]['pUom'] = $productUoms;
            }
            $tempPr['prProducts'] = $prProducts;
            $prDetails[$prID] = $tempPr;
        }
        //get details of the GRN
        $grnDetails = array();
        while ($row = $grnData->current()) {
            $tempG = array();
            $tempG['gID'] = $row['grnID'];
            $tempG['gCd'] = $row['grnCode'];
            $tempG['gSID'] = $row['grnSupplierID'];
            $tempG['gSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
            $tempG['gSR'] = $row['grnSupplierReference'];
            $tempG['gRLID'] = $row['grnRetrieveLocation'];
            $tempG['gRL'] = $row['locationCode'] . '-' . $row['locationName'];
            $tempG['gD'] = $row['grnDate'];
            $tempG['gDC'] = $row['grnDeliveryCharge'];
            $tempG['gT'] = $row['grnTotal'];
            $tempG['gC'] = $row['grnComment'];
            $grnProducts = (isset($grnDetails[$row['grnID']]['gProducts'])) ? $grnDetails[$row['grnID']]['gProducts'] : array();
            if ($row['grnProductID'] != NULL) {
                $productTaxes = (isset($grnProducts[$row['grnProductID']]['pT'])) ? $grnProducts[$row['grnProductID']]['pT'] : array();
                $productBatches = (isset($grnProducts[$row['grnProductID']]['bP'])) ? $grnProducts[$row['grnProductID']]['bP'] : array();
                $productSerials = (isset($grnProducts[$row['grnProductID']]['sP'])) ? $grnProducts[$row['grnProductID']]['sP'] : array();
                $productUoms = (isset($grnProducts[$row['grnProductID']]['pUom'])) ? $grnProducts[$row['grnProductID']]['pUom'] : array();

                if (is_null($row['productBatchID']) && is_null($row['productSerialID'])) {
                    $grnProductCopiedQuantity = (is_null($row['grnProductTotalCopiedQuantity'])) ? 0 : $row['grnProductTotalCopiedQuantity'];
                } else {
                    $grnProductCopiedQuantity = (is_null($row['grnProductCopiedQuantity'])) ? 0 : $row['grnProductCopiedQuantity'];
                }
                $grnProducts[$row['grnProductID']] = array('gPC' => $row['productCode'], 'gPID' => $row['productID'], 'gPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'gPP' => $row['grnProductPrice'], 'gPD' => $row['grnProductDiscount'], 'gPQ' => floatval($row['grnProductQuantity']) - floatval($grnProductCopiedQuantity), 'gPT' => $row['grnProductTotal'], 'productType' => $row['productTypeID'], 'grnPId' => $row['grnProductID']);
                if ($row['productBatchID'] != NULL) {
                    $productBatches[$row['productBatchID']] = array('bC' => $row['productBatchCode'], 'bED' => $row['productBatchExpiryDate'], 'bW' => $row['productBatchWarrantyPeriod'], 'bMD' => $row['productBatchManufactureDate'], 'bQ' => $row['grnProductQuantity']);
                }
                if ($row['productSerialID'] != NULL) {
                    $productSerials[$row['productSerialID']] = array('sC' => $row['productSerialCode'], 'sW' => $row['productSerialWarrantyPeriod'], 'sED' => $row['productSerialExpireDate'], 'sBC' => $row['productBatchCode']);
                }
                if ($row['grnTaxID'] != NULL) {
                    $productTaxes[$row['grnTaxID']] = array('pTN' => $row['taxName'], 'pTP' => $row['grnTaxPrecentage'], 'pTA' => $row['grnTaxAmount']);
                }
                if ($row['uomID'] != NULL) {
                    $productUoms[$row['uomID']] = array('gPUom' => $row['uomAbbr'], 'gPUomID' => $row['uomID'], 'gPUomCon' => $row['productUomConversion']);
                }
                $grnProducts[$row['grnProductID']]['pT'] = $productTaxes;
                $grnProducts[$row['grnProductID']]['bP'] = $productBatches;
                $grnProducts[$row['grnProductID']]['sP'] = $productSerials;
                $grnProducts[$row['grnProductID']]['pUom'] = $productUoms;
            }
            $tempG['gProducts'] = $grnProducts;
            $grnDetails[$row['grnID']] = $tempG;
        }

        foreach ($prDetails as $prIDKey => $prDataArray) {
            $prProductArray = isset($prDataArray['prProducts']) ? $prDataArray['prProducts'] : array();
            //Update the product qtys of GRN if products were previously returned
            foreach ($grnDetails[$grnID]['gProducts'] as &$grnCheckProduct) {
                $grnProductPurQty = $grnCheckProduct['gPQ'];
                //check the current product was returned
                if (array_key_exists($grnCheckProduct['grnPId'], $prProductArray)) {
                    $productKey = $grnCheckProduct['grnPId'];
                    $grnCheckProduct['gPQ'] = floatval($grnCheckProduct['gPQ']) - floatval($prProductArray[$productKey]['prRPQ']);

                    //if batch products available,update the returned qty details
                    if (!empty($grnCheckProduct['bP'])) {
                        foreach ($grnCheckProduct['bP'] as $batchKey => &$grnBatchPro) {
                            if (array_key_exists($batchKey, $prProductArray[$productKey]['bP'])) {
                                if ($grnBatchPro['bQ'] == $prProductArray[$productKey]['bP'][$batchKey]['bRQ']) {
                                    unset($grnCheckProduct['bP'][$batchKey]);
                                } else {
                                    $grnBatchPro['bQ'] = floatval($grnBatchPro['bQ']) - floatval($prProductArray[$productKey]['bP'][$batchKey]['bRQ']);
                                }
                            }
                        }
                    }
                    //if serial products available,update the returned qty details
                    if (!empty($grnCheckProduct['sP'])) {
                        foreach ($grnCheckProduct['sP'] as $serialKey => &$serialPro) {
                            if (array_key_exists($serialKey, $prProductArray[$productKey]['sP'])) {
                                unset($grnCheckProduct['sP'][$serialKey]);
                            }
                        }
                    }
                }


                //if all items were returned, Unset the product from the list
                if ($grnCheckProduct['gPQ'] <= 0) {
                    unset($grnDetails[$grnID]['gProducts'][$productKey]);
                }
            }
        }

        if (empty($grnDetails[$grnID]['gProducts'])) {
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Inventory\Model\GrnTable')->updateGrnStatus($grnID, $closeStatusID);
        }
    }

    //Update returned Batch Quantities
    public function updateBatchProductQuantity($batchID, $returnQty)
    {

        $batchData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchID);
        $batchOldQty = $batchData->productBatchQuantity;
        $batchNewQty = $batchOldQty - floatval($returnQty);
        $batchUpdateData = array(
            'productBatchQuantity' => $batchNewQty
        );
        $this->CommonTable('Inventory\Model\ProductBatchTable')->updateBatchByID($batchID, $batchUpdateData);
    }

    //Update returned location Products
    public function updateLocationProductQty($locationPID, $returnedQty)
    {
        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
        $newPQty = floatval($currentPQty->locationProductQuantity) - floatval($returnedQty);
        $newPQtyData = array(
            'locationProductQuantity' => $newPQty
        );
        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
    }

    public function getPurchaseReturnsForSearchAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $statuses = $globaldata['statuses'];
            if ($request->getPost('searchKey') != '') {
                $prList = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnsforSearch($request->getPost('searchKey'));
                $prListView = new ViewModel(array('prList' => $prList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
            } else {
                $this->paginator = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturns(true);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $prListView = new ViewModel(array('prList' => $this->paginator, 'statuses' => $statuses, 'dateFormat' => $dateFormat));
            }

            $prListView->setTemplate('inventory/purchase-returns/pr-list');
            $this->html = $prListView;
            return $this->JSONRespondHtml();
        }
    }

    public function sendPrEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Purchase Return';

        if ($request->isPost()) {
            $documentID = $request->getPost('documentID');
            $email = $request->getPost('to_email');
            $prData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnById($documentID)->current();
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_PAY_EMAIL');
            $this->setLogMessage($prData['purchaseReturnCode']." purchase return document email to ".$email.'.');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_GRN_EMAIL', array($documentType));
        $this->setLogMessage("Error occured when email the purchase return document.");
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * search purchase returns for dropdown
     * @return Viewmodel Using in dropdown
     */
    public function searchPurchaseReturnsForDropdownByPRCodeAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchPRKey = $searchrequest->getPost('searchKey');
            $PRs = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->searchPurchaseReturnsFroDropdown($searchPRKey);

            $PRList = array();
            foreach ($PRs as $PR) {
                $temp['value'] = $PR['purchaseReturnID'];
                $temp['text'] = $PR['purchaseReturnCode'];
                $PRList[] = $temp;
            }

            $this->data = array('list' => $PRList);
            $this->setLogMessage("Retrive purchase return list for dropdown.");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Get Purchasing Returns list for view page
     * @return view model
     */
    public function getPurchaseReturnsForViewAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $statuses = $globaldata['statuses'];
            $PRID = $request->getPost('PRID');
            $PRCode = $request->getPost('PRCode');
            $supplierID = $request->getPost('supplierID');
            $supplierCode = $request->getPost('supplierCode');

            if ($PRID) {
                $prList = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnsBySearch($PRID);
                $prListView = new ViewModel(array('prList' => $prList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
                $this->setLogMessage("Retrive purchase return ".$PRCode." for list view.");
            } else if ($supplierID) {
                $prList = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnsBySearch(null, $supplierID);
                $prListView = new ViewModel(array('prList' => $prList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
                $this->setLogMessage("Retrive purchase returns for list view by supplier ".$supplierCode.'.');
            } else {
                $userActiveLocation = $this->user_session->userActiveLocation;
                $this->paginator = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturns(true, $userActiveLocation['locationID']);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $prListView = new ViewModel(array('prList' => $this->paginator, 'statuses' => $statuses, 'dateFormat' => $dateFormat));
                $this->setLogMessage("Retrive purchase returns for list view.");
            }

            $prListView->setTemplate('inventory/purchase-returns/pr-list');
            $this->html = $prListView;
            return $this->JSONRespondHtml();
        }
    }

    public function getAllRelatedDocumentDetailsByPrIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $purchaseReturnID = $request->getPost('purchaseReturnID');
            $prData = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPRDetailsByPrId($purchaseReturnID);
            $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnDetailsByPrId($purchaseReturnID);
            $poData = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPODetailsByPrId($purchaseReturnID);
            $piDataByPo = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataByPrId($purchaseReturnID);
            $piDataByGrn = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBasicDataRelatedToGrnByPrId($purchaseReturnID);
            $debitNoteDataByPo = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPoRelatedDebitNoteDataByPrId($purchaseReturnID);
            $debitNoteDataByGrn = $this->CommonTable('Inventory\Model\DebitNoteTable')->getGrnRelatedDebitNoteDataByPrId($purchaseReturnID);
            $piPaymentByPo = $this->CommonTable('SupplierPaymentsTable')->getPiPaymentDetailsThroughPoByPrId($purchaseReturnID);
            $piPaymentDataByGrn = $this->CommonTable('SupplierPaymentsTable')->getPiPaymentDetailsThroughGrnByPrId($purchaseReturnID);
            $debitNotePaymentDataByPo = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentDetailsThroughPOByPrId($purchaseReturnID);
            $debitNotePaymentDataByGrn = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentDetailsThroughGrnByPrId($purchaseReturnID);

            $dataExistsFlag = false;
            if ($prData) {
                $prData = array(
                    'type' => 'PurchaseReturn',
                    'documentID' => $prData['purchaseReturnID'],
                    'code' => $prData['purchaseReturnCode'],
                    'amount' => number_format($prData['purchaseReturnTotal'], 2),
                    'issuedDate' => $prData['purchaseReturnDate'],
                    'created' => $prData['createdTimeStamp'],
                );
                $prDetails[] = $prData;
                $dataExistsFlag = true;
            }
            if (isset($grnData)) {
                foreach ($grnData as $grDta) {
                    $grnDta = array(
                        'type' => 'Grn',
                        'documentID' => $grDta['grnID'],
                        'code' => $grDta['grnCode'],
                        'amount' => number_format($grDta['grnTotal'], 2),
                        'issuedDate' => $grDta['grnDate'],
                        'created' => $grDta['createdTimeStamp'],
                    );
                    $prDetails[] = $grnDta;
                    if (isset($grDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($poData)) {
                foreach ($poData as $poDta) {
                    $pOrderData = array(
                        'type' => 'PurchaseOrder',
                        'documentID' => $poDta['purchaseOrderID'],
                        'code' => $poDta['purchaseOrderCode'],
                        'amount' => number_format($poDta['purchaseOrderTotal'], 2),
                        'issuedDate' => $poDta['purchaseOrderExpDelDate'],
                        'created' => $poDta['createdTimeStamp'],
                    );
                    $prDetails[] = $pOrderData;
                    if (isset($prDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($piDataByPo)) {
                foreach ($piDataByPo as $piDirectDta) {
                    $piDeta = array(
                        'type' => 'PurchaseInvoice',
                        'documentID' => $piDirectDta['purchaseInvoiceID'],
                        'code' => $piDirectDta['purchaseInvoiceCode'],
                        'amount' => number_format($piDirectDta['purchaseInvoiceTotal'], 2),
                        'issuedDate' => $piDirectDta['purchaseInvoiceIssueDate'],
                        'created' => $piDirectDta['createdTimeStamp'],
                    );
                    $prDetails[] = $piDeta;
                    if (isset($piDirectDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($piDataByGrn)) {
                foreach ($piDataByGrn as $piData) {
                    $piDta = array(
                        'type' => 'PurchaseInvoice',
                        'documentID' => $piData['purchaseInvoiceID'],
                        'code' => $piData['purchaseInvoiceCode'],
                        'amount' => number_format($piData['purchaseInvoiceTotal'], 2),
                        'issuedDate' => $piData['purchaseInvoiceIssueDate'],
                        'created' => $piData['createdTimeStamp'],
                    );
                    $prDetails[] = $piDta;
                    if (isset($piData)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($debitNoteDataByPo)) {
                foreach ($debitNoteDataByPo as $debitNoteDta) {
                    $dNData = array(
                        'type' => 'DebitNote',
                        'documentID' => $debitNoteDta['debitNoteID'],
                        'code' => $debitNoteDta['debitNoteCode'],
                        'amount' => number_format($debitNoteDta['debitNoteTotal'], 2),
                        'issuedDate' => $debitNoteDta['debitNoteDate'],
                        'created' => $debitNoteDta['createdTimeStamp'],
                    );
                    $prDetails[] = $dNData;
                    if (isset($debitNoteDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($debitNoteDataByGrn)) {
                foreach ($debitNoteDataByGrn as $debitNoteDta) {
                    $dNData = array(
                        'type' => 'DebitNote',
                        'documentID' => $debitNoteDta['debitNoteID'],
                        'code' => $debitNoteDta['debitNoteCode'],
                        'amount' => number_format($debitNoteDta['debitNoteTotal'], 2),
                        'issuedDate' => $debitNoteDta['debitNoteDate'],
                        'created' => $debitNoteDta['createdTimeStamp'],
                    );
                    $prDetails[] = $dNData;
                    if (isset($debitNoteDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($piPaymentByPo)) {
                foreach ($piPaymentByPo as $piPayDta) {
                    $piPaymentData = array(
                        'type' => 'SupplierPayment',
                        'documentID' => $piPayDta['outgoingPaymentID'],
                        'code' => $piPayDta['outgoingPaymentCode'],
                        'amount' => number_format($piPayDta['outgoingPaymentAmount'], 2),
                        'issuedDate' => $piPayDta['outgoingPaymentDate'],
                        'created' => $piPayDta['createdTimeStamp'],
                    );
                    $prDetails[] = $piPaymentData;
                    if (isset($piPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($piPaymentDataByGrn)) {
                foreach ($piPaymentDataByGrn as $piPayDta) {
                    $piPaymentData = array(
                        'type' => 'SupplierPayment',
                        'documentID' => $piPayDta['outgoingPaymentID'],
                        'code' => $piPayDta['outgoingPaymentCode'],
                        'amount' => number_format($piPayDta['outgoingPaymentAmount'], 2),
                        'issuedDate' => $piPayDta['outgoingPaymentDate'],
                        'created' => $piPayDta['createdTimeStamp'],
                    );
                    $prDetails[] = $piPaymentData;
                    if (isset($piPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($debitNotePaymentDataByPo)) {
                foreach ($debitNotePaymentDataByPo as $dNPayDta) {
                    $dNPaymentData = array(
                        'type' => 'DebitNotePayment',
                        'documentID' => $dNPayDta['debitNotePaymentID'],
                        'code' => $dNPayDta['debitNotePaymentCode'],
                        'amount' => number_format($dNPayDta['debitNotePaymentAmount'], 2),
                        'issuedDate' => $dNPayDta['debitNotePaymentDate'],
                        'created' => $dNPayDta['createdTimeStamp'],
                    );
                    $prDetails[] = $dNPaymentData;
                    if (isset($dNPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($debitNotePaymentDataByGrn)) {
                foreach ($debitNotePaymentDataByGrn as $dNPayDta) {
                    $dNPaymentData = array(
                        'type' => 'DebitNotePayment',
                        'documentID' => $dNPayDta['debitNotePaymentID'],
                        'code' => $dNPayDta['debitNotePaymentCode'],
                        'amount' => number_format($dNPayDta['debitNotePaymentAmount'], 2),
                        'issuedDate' => $dNPayDta['debitNotePaymentDate'],
                        'created' => $dNPayDta['createdTimeStamp'],
                    );
                    $prDetails[] = $dNPaymentData;
                    if (isset($dNPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($prDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $prDetails);

            $documentDetails = array(
                'prDetails' => $prDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

    public function getAllRelatedDocumentInDetailsForPrByPrIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $purchaseReturnID = $request->getPost('purchaseReturnID');
            $getReferenceData = $this->CommonTable('Core\model\DocumentReferenceTable')->getDocumentReferenceBySourceDocIDAndRefDocDetails(10,$purchaseReturnID,11);

            if (!empty($getReferenceData)) {
                $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnInForPrDetailsByGrnIds($getReferenceData);
            }
            
            $dataExistsFlag = false;

            if (isset($grnData)) {
                foreach ($grnData as $grDta) {
                    $grnDta = array(
                        'type' => 'Grn',
                        'documentID' => $grDta['grnID'],
                        'code' => $grDta['grnCode'],
                        'amount' => number_format($grDta['grnTotal'], 2),
                        'issuedDate' => $grDta['grnDate'],
                        'created' => $grDta['createdTimeStamp'],
                    );
                    $prDetails[] = $grnDta;
                    if (isset($grDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($prDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $prDetails);

            $documentDetails = array(
                'prDetails' => $prDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

}

?>
