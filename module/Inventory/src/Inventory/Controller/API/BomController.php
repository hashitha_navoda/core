<?php

/** this controller use to create bom items assemble bom items and disassemble bom items
 * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
 */

namespace Inventory\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Inventory\Model\Product;
use Inventory\Model\ProductUom;
use Inventory\Model\ProductHandeling;
use Inventory\Model\LocationProduct;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemOut;
use Inventory\Model\Bom;
use Inventory\Model\BomSubItem;
use Inventory\Model\BomAssemblyDisassembly;
use Zend\View\Model\ViewModel;

class BomController extends CoreController
{

    public function addBomAction()
    {
        $savedBomSubItem = false;
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $bomCode = $request->getPost('bomReference');
            
            if ($this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($bomCode)) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_ITEM_EXIST');
                return $this->JSONRespond();
            }
            
            $this->beginTransaction();
            $bomDataArray = array(
                'bomCode' => $bomCode,
                'bomDiscription' => $request->getPost('discription'),
                'bomTotalCost' => $request->getPost('totalValue'),
                'isJEPostSubItemWise' => $request->getPost('jeState'),
            );
            $bom = new Bom();
            $bom->exchangeArray($bomDataArray);
            //save parent item in to the bom table
            $savedBomID = $this->CommonTable('Inventory\Model\BomTable')->saveBomDetails($bom);

            $nonInventoryCount = 0;
            $inventoryCount = 0;
            $subItemCount = sizeof($request->getPost('subItemDetails'));

            foreach ($request->getPost('subItemDetails') as $key => $value) {
                $pData = $this->CommonTable('Inventory/Model/ProductTable')->getProduct($value['itemID']);

                if($pData->productTypeID == 1){
                    $inventoryCount ++;
                } elseif ($pData->productTypeID == 2) {
                    $nonInventoryCount ++;
                }   
            }

            $productType = null;

            if ($subItemCount == $inventoryCount && !$request->getPost('jeState')) {
                $productType = 1;
            } elseif ($subItemCount == $nonInventoryCount) {
                $productType = 2;
            } elseif ($nonInventoryCount > 0 && $inventoryCount > 0 && !$request->getPost('jeState')) {
               $productType = 1;
            } else {
                $productType = 2;
            }

            if ($savedBomID) {
                //save sub items in to the bom sub table that related to the parent Item
                $bomSubItemDetails = $request->getPost('subItemDetails');
                if(!empty($bomSubItemDetails)){
                    foreach ($bomSubItemDetails as $bomSubData) {
                        $bomSubItemArray = array(
                            'bomID' => $savedBomID,
                            'productID' => $bomSubData['itemID'],
                            'bomSubItemQuantity' => $bomSubData['quontity'],
                            'bomSubItemUnitPrice' => $bomSubData['unitPrice'],
                            'bomSubItemCost' => $bomSubData['totatPrice'],
                        );
                        $bomSubItem = new BomSubItem();
                        $bomSubItem->exchangeArray($bomSubItemArray);
                        $savedBomSubItem = $this->CommonTable('Inventory/Model/BomSubItemTable')->saveBomSubItem($bomSubItem);
                    }
                } else {
                    $this->rollback();
                    $this->msg = $this->getMessage('ERROR_BOM_SUB_ITEMS_NOT_EXIST');
                    return $this->JSONRespond();
                }                
            }
            if ($savedBomSubItem) {
                //save to the product table(newly created parent item)
                $productDefaultAccounts = $this->getProductDefaultAccounts();

                $post = [
                    'productCode' => $bomCode,
                    'productBarcode' => $bomCode,
                    'productName' => $request->getPost('bomParentName'),
                    'categoryID' => $request->getPost('bomCategory'),
                    'productTypeID' => $productType, //All Bom products should be an inventory product.
                    'productDescription' => $request->getPost('discription'),
                    'productDiscountEligible' => 0,
                    'productDiscountValue' => 0,
                    'productState' => 1,
                    'productGlobalProduct' => filter_var($request->getPost('productGlobalProduct'), FILTER_VALIDATE_BOOLEAN),
                    'batchProduct' => 0,
                    'serialProduct' => 0,
                    'productTaxEligible' => 0,
                    'productSalesAccountID' => $productDefaultAccounts['product_accounts']['productSales'],
                    'productInventoryAccountID' => $productDefaultAccounts['product_accounts']['productInventory'],
                    'productCOGSAccountID' => $productDefaultAccounts['product_accounts']['productCOGSA'],
                    'productAdjusmentAccountID' => $productDefaultAccounts['product_accounts']['productAdjusment'],
                    'entityID' => $this->createEntity(),
                ];

                // check if a Product from the same Product Code exists
                if ($this->CommonTable('Inventory\Model\ProductTable')->getProductByCode($post['productCode'])) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PRDCTAPI_ITEM_EXIST');
                    return $this->JSONRespond();
                }
                $product = new Product();
                $product->exchangeArray($post);
                $addedID = $this->CommonTable('Inventory\Model\ProductTable')->saveProduct($product);

                if ($addedID) {
                    //add handeling types
                    $data = array(
                        'productID' => $addedID,
                        'productHandelingManufactureProduct' => 1,
                        'productHandelingPurchaseProduct' => 1,
                        'productHandelingSalesProduct' => 1,
                        'productHandelingConsumables' => 1,
                        'productHandelingFixedAssets' => 1,
                        'productHandelingBomItem' => 1,
                    );
                    $productHandeling = new ProductHandeling();
                    $productHandeling->exchangeArray($data);


                    if (!$this->CommonTable('Inventory\Model\ProductHandelingTable')->saveProductHandeling($productHandeling)) {
                        return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMHANDLE'));
                    }
                    //add locations
                    $locations = array();
                    // if location type is global, get all locations
                    if ($request->getPost('productGlobalProduct') == 1) {
                        $locationsArr = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
                        foreach ($locationsArr as $l) {
                            $l = (object) $l;
                            $locations[] = $l->locationID;
                        }
                    } else {
                        $locations = (is_array($request->getPost('locations'))) ? $request->getPost('locations') : array();
                    }
                    foreach ($locations as $locationID) {
                        $entityID = $this->createEntity();

                        if ($subItemCount == $nonInventoryCount) {
                            $data = array(
                                'productID' => $addedID,
                                'locationID' => $locationID,
                                'defaultSellingPrice' => $request->getPost('totalValue'),
                                'entityID' => $entityID,
                            );
                        } else {
                            $data = array(
                                'productID' => $addedID,
                                'locationID' => $locationID,
                                'entityID' => $entityID,
                            );
                        }

                        $locationIDs[] = $locationID;


                        $locationProduct = new LocationProduct();
                        $locationProduct->exchangeArray($data);

                        if (!$this->CommonTable('Inventory\Model\LocationProductTable')->saveLocationProduct($locationProduct)) {
                            return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMLOC'));
                        }
                    }

                    //add uoms
                    $uom = (is_array($request->getPost('uomDetails'))) ? $request->getPost('uomDetails') : array();
                    foreach ($uom as $uomValue) {
                        $uomValue = (object) $uomValue;
                        $data = array(
                            'productID' => $addedID,
                            'uomID' => $uomValue->uomID,
                            'productUomConversion' => $uomValue->coRate,
                            'productUomDisplay' => (array_key_exists('display', $uomValue)) ? 1 : 0,
                            'productUomBase' => (array_key_exists('base', $uomValue)) ? 1 : 0,
                        );
                        $productUom = new ProductUom();
                        $productUom->exchangeArray($data);
                        if (!$this->CommonTable('Inventory\Model\ProductUomTable')->saveProductUom($productUom)) {
                            return $this->_dependingQueryError($this->getMessage('ERR_PRDCTAPI_ITEMUOM'));
                        }
                    }


                    //link bom table and product table
                    $updateBomData = array(
                        'productID' => $addedID,
                        'bomID' => $savedBomID,
                        'bomItemType' => $productType,
                    );

                    $bom->exchangeArray($updateBomData);
                    $updated = $this->CommonTable('Inventory\Model\BomTable')->updateBomDetails($bom);
                }
            }


            if ($savedBomSubItem && $updated) {
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCCES_BOM_SAVE');
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERROR_BOM_SAVE');
            }
            return $this->JSONRespond();
        }
    }

    public function getBomSubItemDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $bomID = $request->getPost('bomID');
            $bomSubItemDetails = $this->CommonTable("Inventory\Model\BomSubItemTable")->getBomSubDataByBomID($bomID);
            foreach ($bomSubItemDetails as $value) {
                $dataArray[] = $value;
            }
            if (count($bomSubItemDetails) > 0) {
                $this->status = true;
                $this->data = $dataArray;
            }
            return $this->JSONRespond();
        }
    }

    public function updateBomAction()
    {
        $savedBomSubItem = false;
        $request = $this->getRequest();

        if ($request->isPost()) {
            $this->beginTransaction();
            
            try {                
                $bomDataArray = array(
                    'bomID' => $request->getPost('bomID'),
                    'bomCode' => $request->getPost('bomReference'),
                    'bomDiscription' => $request->getPost('discription'),
                    'bomTotalCost' => $request->getPost('totalValue'),
                );
                $bom = new Bom();
                $bom->exchangeArray($bomDataArray);

                $updateBomTable = $this->CommonTable('Inventory\Model\BomTable')->updateBomDetailsByBomID($bom);
                $bomData = $this->CommonTable('Inventory\Model\BomTable')->getBomDataByBomID($request->getPost('bomID'))->current();
                if ($updateBomTable) {

                    $deleteSubProducts = $this->CommonTable('Inventory\Model\BomSubItemTable')->deleteBomSubDataByBomID($request->getPost('bomID'));
                    $bomSubItemDetails = $request->getPost('subItemDetails');

                    foreach ($bomSubItemDetails as $bomSubData) {
                        $bomSubItemArray = array(
                            'bomID' => $request->getPost('bomID'),
                            'productID' => $bomSubData['itemID'],
                            'bomSubItemQuantity' => $bomSubData['quontity'],
                            'bomSubItemUnitPrice' => $bomSubData['unitPrice'],
                            'bomSubItemCost' => $bomSubData['totatPrice'],
                        );
                        $bomSubItem = new BomSubItem();
                        $bomSubItem->exchangeArray($bomSubItemArray);
                        $savedBomSubItem = $this->CommonTable('Inventory/Model/BomSubItemTable')->saveBomSubItem($bomSubItem);
                    }
                }
                if ($savedBomSubItem) {
                    //update product table
                    $productArr = [
                        'productName' => $request->getPost('bomParentName'),
                        'categoryID' => $request->getPost('bomCategory'),
                        'productDescription' => $request->getPost('discription')
                    ];
                    $this->CommonTable('Inventory\Model\ProductTable')->updateProductByArray($productArr,$bomData['productID']);
                }

                if ($savedBomSubItem) {
                    $this->commit();
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCCES_BOM_UPDATE');
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERROR_BOM_UPDATE');
                }
                return $this->JSONRespond();
                
            } catch (Exception $e) {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERROR_BOM_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

    public function searchBomItemsForDropdownAction()
    {
        $searchrequest = $this->getRequest();

        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            $isAdList = (isset($_GET['adList'])) ? $_GET['adList'] : "null";



            $bomDetails = $this->CommonTable('Inventory\Model\BomTable')->searchBomDetailsForDropDown($locationID, $searchKey, $isAdList);

            $bomDataList = array();
            foreach ($bomDetails as $bomData) {
                $temp['value'] = $bomData['bomID'];
                $temp['text'] = $bomData['productName'] . '-' . $bomData['bomCode'];
                $bomDataList[] = $temp;
            }

            $this->data = array('list' => $bomDataList);
            return $this->JSONRespond();
        }
    }

    public function searchSubItemBaseBomItemsForDropdownAction()
    {
        $searchrequest = $this->getRequest();

        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $bomDetails = $this->CommonTable('Inventory\Model\BomTable')->searchSubItemBaseBomDetailsForDropDown($searchKey);

            $bomDataList = array();
            foreach ($bomDetails as $bomData) {
                $temp['value'] = $bomData['bomID'];
                $temp['text'] = $bomData['productName'] . '-' . $bomData['bomCode'];
                $bomDataList[] = $temp;
            }

            $this->data = array('list' => $bomDataList);
            return $this->JSONRespond();
        }
    }

    public function getBomSubItemDetailsForAssembleAction()
    {
        $request = $this->getRequest();
        $minimumProductQuantity = null;

        if ($request->isPost()) {
            $bomID = $request->getPost('bomID');
            $locationID = $request->getPost('locationID');
            $type = $request->getPost('type');

            if ($type == 'dissembly') {
                $bomDetails = $this->CommonTable('Inventory\Model\BomTable')->getBomDataByBomID($bomID)->current();
                $bomItemLocationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($bomDetails['productID'], $locationID);
                $bomParentItemDetails = $this->CommonTable('Inventory\Model\BomTable')->getBomDataAndProductDataByBomIDAndLocationID($bomID, $locationID)->current();
            }
            $bomSubItemDetails = $this->CommonTable("Inventory\Model\BomSubItemTable")->getBomSubDataAndProductDataByBomIDAndLocationID($bomID, $locationID);

            $productTypeArr = [];
            foreach ($bomSubItemDetails as $value) {
                $value['bomSubItemQuantity'] = $value['bomSubItemQuantity'] / $value['productUomConversion'];
                $value['locationProductQuantity'] = $value['locationProductQuantity'] / $value['productUomConversion'];
                if ($type == 'assembly') {
                    $productTypeArr[] = $value['productTypeID'];
                    if($value['productTypeID'] == '1'){//for inventory product
                        if ($minimumProductQuantity == null) {
                            $minimumProductQuantity = $value['locationProductQuantity'];
                            $minimumItemCreate = intval($value['locationProductQuantity'] / $value['bomSubItemQuantity']);
                        } else if ($minimumProductQuantity > $value['locationProductQuantity']) {
                            $minimumProductQuantity = $value['locationProductQuantity'];
                            $minimumItemCreate = intval($value['locationProductQuantity'] / $value['bomSubItemQuantity']);
                        }
                    }
                }
                $dataArray[] = $value;
            }
            
            //check wheter all the products non-inventory
            if(!in_array("1", $productTypeArr)){
                $minimumItemCreate = null;
            }

            if ($type == 'assembly') {
                $minimumAssemblyOrDissambly = $this->calculateMinimumBomCreation($dataArray, $minimumItemCreate);
            } else {
                $minimumAssemblyOrDissambly = ($bomItemLocationProductDetails->locationProductQuantity) ? ($bomItemLocationProductDetails->locationProductQuantity) / $bomParentItemDetails['productUomConversion'] : 0;
            }

            $resultArray = array(
                'dataArray' => $dataArray,
                'minimumAssemblyOrDissambly' => $minimumAssemblyOrDissambly,
            );
            if (count($bomSubItemDetails) > 0) {
                $this->status = true;
                $this->data = $resultArray;
            }
            return $this->JSONRespond();
        }
    }

    public function saveBomADAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $saveData = $request->getPost();
            $locationID = $request->getPost('locationID');
            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            if($FIFOCostingFlag == 0 && $averageCostingFlag == 0){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE');
                return $this->JSONRespond();
            }

            $this->beginTransaction();
            //save bomAssemblyDisassembly details
            if ($saveData['adType'] == 'assembly') {
                $aDType = 'assembly';
                $changedQuantity = $saveData['createQuantity'];
            } else {
                $aDType = 'disassembly';
                $changedQuantity = $saveData['disamblyQuantity'];
            }

            $bomADData = array(
                'bomAssemblyDisassemblyType' => $aDType,
                'bomAssemblyDisassemblyReference' => $saveData['adRef'],
                'bomID' => $saveData['parentID'],
                'bomAssemblyDisassemblyParentQuantity' => $changedQuantity,
                'bomAssemblyDisassemblyComment' => $saveData['comment']
            );
            $bomAD = new BomAssemblyDisassembly();
            $bomAD->exchangeArray($bomADData);
            $savedBomADId = $this->CommonTable('Inventory\Model\BomAssemblyDisassemblyTable')->saveBomAssemblyDisassemblyDetails($bomAD);

            //get product id and existing location product details that related to the given parent bomID and locationID
            $relatedData = $this->CommonTable('Inventory\Model\BomTable')->getBomDataAndProductDataByBomIDAndLocationID($saveData['parentID'], $locationID)->current();

            //if this is assembly request then update parent item in itemIn Table and locationProduct table.
            //also update child items in itemIn,itemOut and LocationProductTable...

            $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($relatedData['locationProductID']);
            $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
            $locationProductQuantity = $locationProductData->locationProductQuantity;
            $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

            if ($aDType == 'assembly') {

                $newPQty = floatval($relatedData['locationProductQuantity']) + ($changedQuantity * $relatedData['productUomConversion']);
                $newPQtyData = array(
                    'locationProductQuantity' => $newPQty
                );

                //Update parent Item in location Product Quantity
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $relatedData['locationProductID']);


                //first add new record about parent item in to the ItemIn Table
                $itemInDatas = array(
                    'itemInDocumentType' => 'Bom Assembly Disassembly',
                    'itemInDocumentID' => $savedBomADId,
                    'itemInLocationProductID' => $relatedData['locationProductID'],
                    'itemInQty' => $changedQuantity * $relatedData['productUomConversion'],
                    'itemInPrice' => $relatedData['bomTotalCost'],
                    'itemInDateAndTime' => $this->getGMTDateTime(),
                );
                $itemInModel = new ItemIn();
                $itemInModel->exchangeArray($itemInDatas);
                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($relatedData['locationProductID']);
                $locationPrTotalQty = 0;
                $locationPrTotalPrice = 0;
                $newItemInIds = array();
                if(count($itemInData) > 0){
                    foreach ($itemInData as $key => $value) {
                        if($value->itemInID > $locationProductLastItemInID){
                            $newItemInIds[] = $value->itemInID; 
                            $remainQty = $value->itemInQty - $value->itemInSoldQty;
                            if($remainQty > 0){
                                $itemInPrice = $remainQty*$value->itemInPrice;
                                $itemInDiscount = $remainQty*$value->itemInDiscount;
                                $locationPrTotalQty += $remainQty;
                                $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                            }
                            $locationProductLastItemInID = $value->itemInID; 
                        }
                    }
                }
                $locationPrTotalQty += $locationProductQuantity;
                $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                if($locationPrTotalQty > 0){
                    $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                    $lpdata = array(
                        'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                        'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $relatedData['locationProductID']);
                
                    foreach ($newItemInIds as $inID) {
                        $intemInData = array(
                            'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                        );
                        $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                    }
                }


                //update child items in ItemIn Table and ItemOut Table
                foreach ($saveData['subData'] as $value) {
                    if($value['productTypeID'] == '1'){
                        $lProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($value['locationProductID']);
                        $lProductAverageCostingPrice = $lProductData->locationProductAverageCostingPrice;
                        $rquiredQuanty = ($value['bomSubItemQuantity'] * $value['productUomConversion'] * ($changedQuantity * $relatedData['productUomConversion']));
                        while ($rquiredQuanty != 0) {

                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($value['locationProductID']);
                            
                            $unitPrice = $itemInDetails['itemInPrice'];
                            $unitDiscount = $itemInDetails['itemInDiscount'];
                            if($averageCostingFlag == 1){
                                $unitPrice = $lProductAverageCostingPrice;
                                $unitDiscount = 0;
                            }

                            if (!$itemInDetails) {
                                break;
                            }
                            if ($itemInDetails['itemInRemainingQty'] > $rquiredQuanty) {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $rquiredQuanty;
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Bom Assembly Disassembly',
                                    'itemOutDocumentID' => $savedBomADId,
                                    'itemOutLocationProductID' => $value['locationProductID'],
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $rquiredQuanty,
                                    'itemOutPrice' => $unitPrice, 
                                    'itemOutAverageCostingPrice' =>$lProductAverageCostingPrice,
                                    'itemOutDiscount' => $unitDiscount, 
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                $rquiredQuanty = 0;
                                break;
                            } else {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Bom Assembly Disassembly',
                                    'itemOutDocumentID' => $savedBomADId,
                                    'itemOutLocationProductID' => $value['locationProductID'],
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                    'itemOutPrice' => $unitPrice, 
                                    'itemOutAverageCostingPrice' => $lProductAverageCostingPrice,
                                    'itemOutDiscount' => $unitDiscount, 
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                $rquiredQuanty = $rquiredQuanty - $itemInDetails['itemInRemainingQty'];
                            }
                        }

                        $avbQty = floatval($value['locationProductQuantity'] * $value['productUomConversion']);
                        $usedQty = $value['bomSubItemQuantity'] * $value['productUomConversion'] * ($changedQuantity * $relatedData['productUomConversion']);
                        $newPQty = $avbQty - $usedQty;

                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                        );

                        //Update location Product Quantity for child items
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $value['locationProductID']);

                        $productData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($value['productID']);
                        if($productData['batchProduct'] == 1 && $productData['serialProduct'] == 1) {
                            $locationProductID = $lProductData->locationProductID;
                            $productSerialID = $this->CommonTable('Inventory\Model\ProductSerialTable')->getMultiAvailableProductSerialID($usedQty, $locationProductID);
                            if($productSerialID != 0) {
                                $result = $this->CommonTable('Inventory\Model\ProductSerialTable')->setMultiProductSerialAsSold(array('productSerialSold' => '1'), $productSerialID['productSerialIDs']);
                            }
                            $productBatchIDs = array_count_values($productSerialID['productBatchIDs']);
                            foreach ($productBatchIDs as $key => $value) {
                                $productBatch = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($key);
                                $newQty = $productBatch->productBatchQuantity - $value;
                                $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($key, array('productBatchQuantity' => $newQty));
                            }

                        }else if($productData['batchProduct'] == 1) {
                            $locationProductID = $lProductData->locationProductID;
                            $dipbt = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProductsByLocProductId($locationProductID);
                            $qtyInProductBatchTable = 0;
                            foreach ($dipbt as $value) {
                                $qtyInProductBatchTable += $value['productBatchQuantity'];
                                $dataInProductBatchTable[] = $value;
                            }
                            if($qtyInProductBatchTable >= $usedQty) {
                                foreach ($dataInProductBatchTable as $value) {
                                    if($usedQty == 0) {
                                        break;
                                    } else if($usedQty <= $value['productBatchQuantity']) {
                                        $rmdQty = $value['productBatchQuantity'] - $usedQty;
                                        $usedQty = 0;
                                    } else if($usedQty > $value['productBatchQuantity']) {
                                        $rmdQty = 0;
                                        $usedQty = $usedQty - $value['productBatchQuantity'];
                                    }
                                    $this->CommonTable('Inventory\Model\ProductBatchTable')->updateBatchByID($value['productBatchID'], array('productBatchQuantity' => $rmdQty));
                                }
                            }
                        }else if($productData['serialProduct'] == 1) {
                            $locationProductID = $lProductData->locationProductID;
                            $productSerialID = $this->CommonTable('Inventory\Model\ProductSerialTable')->getMultiAvailableProductSerialID($usedQty, $locationProductID);
                            if($productSerialID != 0) {
                                $result = $this->CommonTable('Inventory\Model\ProductSerialTable')->setMultiProductSerialAsSold(array('productSerialSold' => '1'), $productSerialID['productSerialIDs']);
                            }
                        }
                    }
                }
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCCESS_ASSEMBLY_ITEM');
            } else if ($saveData['adType'] == 'dissembly') {

                // update bom parent item in ItemIn and ItemOut tables
                $requQuantity = $changedQuantity;

                while ($changedQuantity != 0) {

                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($relatedData['locationProductID']);
                    
                    $unitPrice = $itemInDetails['itemInPrice'];
                    $unitDiscount = $itemInDetails['itemInDiscount'];
                    if($averageCostingFlag == 1){
                        $unitPrice = $locationProductAverageCostingPrice;
                        $unitDiscount = 0;
                    }

                    if (!$itemInDetails) {
                        break;
                    }
                    if ($itemInDetails['itemInRemainingQty'] > $changedQuantity) {
                        $updateQty = $itemInDetails['itemInSoldQty'] + $changedQuantity;
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Bom Assembly Disassembly',
                            'itemOutDocumentID' => $savedBomADId,
                            'itemOutLocationProductID' => $relatedData['locationProductID'],
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => NULL,
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => $changedQuantity,
                            'itemOutPrice' => $unitPrice, 
                            'itemOutAverageCostingPrice' => $locationProductAverageCostingPrice,
                            'itemOutDiscount' => $unitDiscount, 
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );

                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        $changedQuantity = 0;
                        break;
                    } else {
                        $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Bom Assembly Disassembly',
                            'itemOutDocumentID' => $savedBomADId,
                            'itemOutLocationProductID' => $relatedData['locationProductID'],
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => NULL,
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                            'itemOutPrice' => $unitPrice, 
                            'itemOutAverageCostingPrice' => $locationProductAverageCostingPrice,
                            'itemOutDiscount' => $unitDiscount, 
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );

                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                        $changedQuantity = $changedQuantity - $itemInDetails['itemInRemainingQty'];
                    }
                }

                $newPQty = floatval($relatedData['locationProductQuantity']) - ($requQuantity * $relatedData['productUomConversion']);
                $newPQtyData = array(
                    'locationProductQuantity' => $newPQty
                );

                //Update parent Item in location Product Quantity
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $relatedData['locationProductID']);
                foreach ($saveData['subData'] as $value) {
                    if($value['productTypeID'] == '1'){
                        $lproductID = $value['locationProductID'];
                        $lProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($value['locationProductID']);
                        $lProductAverageCostingPrice = $lProductData->locationProductAverageCostingPrice;
                        $lProductQuantity = $lProductData->locationProductQuantity;
                        $lProductLastItemInID = $lProductData->locationProductLastItemInID;

                        $newPQty = floatval($value['locationProductQuantity']* $value['productUomConversion']) + ($value['bomSubItemQuantity'] * $value['productUomConversion'] * $requQuantity);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                        );

                        //Update child Item in location Product Quantity
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $value['locationProductID']);

                        //add new record about child item in to the ItemIn Table
                        $itemInData = array(
                            'itemInDocumentType' => 'Bom Assembly Disassembly',
                            'itemInDocumentID' => $savedBomADId,
                            'itemInLocationProductID' => $value['locationProductID'],
                            'itemInQty' => $value['bomSubItemQuantity'] * $value['productUomConversion'] * $requQuantity,
                            'itemInPrice' => $value['defaultPurchasePrice'],
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                        $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($value['locationProductID']);
                        $locationPrTotalQty = 0;
                        $locationPrTotalPrice = 0;
                        $newItemInIds = array();
                        if(count($itemInData) > 0){
                            foreach ($itemInData as $key => $value) {
                                if($value->itemInID > $lProductLastItemInID){
                                    $newItemInIds[] = $value->itemInID; 
                                    $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                    if($remainQty > 0){
                                        $itemInPrice = $remainQty*$value->itemInPrice;
                                        $itemInDiscount = $remainQty*$value->itemInDiscount;
                                        $locationPrTotalQty += $remainQty;
                                        $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                                    }
                                    $lProductLastItemInID = $value->itemInID; 
                                }
                            }
                        }
                        $locationPrTotalQty += $lProductQuantity;
                        $locationPrTotalPrice += $lProductQuantity*$lProductAverageCostingPrice;

                        if($locationPrTotalQty > 0){
                            $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                            $lpdata = array(
                                'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                                'locationProductLastItemInID' => $lProductLastItemInID,
                                );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $lproductID);

                            foreach ($newItemInIds as $inID) {
                                $intemInData = array(
                                    'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                    );
                                $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                            }
                        }
                    }
                }
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCCESS_DISASSEMBLY_ITEM');
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERROR_AD_ITEM');
            }

            return $this->JSONRespond();
        }
    }

    public function calculateMinimumBomCreation($dataArray, $minimumItemCreate)
    {
        if(!is_null($minimumItemCreate)){
            foreach ($dataArray as $value) {
                if($value['productTypeID'] == '1'){
                    if ($value['bomSubItemQuantity'] * $minimumItemCreate > $value['locationProductQuantity']) {
                        $minimumItemCreate = intval($value['locationProductQuantity'] / $value['bomSubItemQuantity']);
                    }
                }
            }
        }
        return $minimumItemCreate;
    }

    public function getUomConversionValueAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $uomValue = $request->getPost('uomValue');
            $productID = $request->getPost('productID');
            for ($i = 1; $i < sizeof($uomValue) + 1; $i++) {
                $conversionFactor = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($productID, $uomValue[$i]);
                $proConFactor[$i] = $conversionFactor;
            }
            $this->data = $proConFactor;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getBomBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginatedflag = false;
            $userDateFormat = $this->getUserDateFormat();
            $searchKey = $request->getPost('searchKey');
            if (!empty($searchKey)) {
                $bomList = $this->CommonTable('Inventory\Model\BomTable')->bomSearchByKey($searchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $this->bomList = $this->CommonTable('Inventory\Model\BomTable')->fetchAll(true);
                $this->bomList->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->bomList->setItemCountPerPage(10);
                $bomList = $this->bomList;
                $paginatedflag = true;
            }
            $bomListView = new ViewModel(array(
                'bomList' => $bomList,
                'paginated' => $paginatedflag,
                'status' => $this->getStatusesList(),
                'userDateFormat' => $userDateFormat,
            ));

            $bomListView->setTemplate('inventory/bom/list-search');
            $bomListView->setTerminal(true);

            $this->html = $bomListView;
            return $this->JSONRespondHtml();
        }
    }

    public function getBomAdBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginatedflag = false;
            $userDateFormat = $this->getUserDateFormat();
            $searchKey = $request->getPost('searchKey');
            if (!empty($searchKey)) {
                $bomADList = $this->CommonTable('Inventory\Model\BomAssemblyDisassemblyTable')->bomAdSearchByKey($searchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $this->bomADList = $this->CommonTable('Inventory\Model\BomAssemblyDisassemblyTable')->fetchAll(true);
                $this->bomADList->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->bomADList->setItemCountPerPage(10);
                $bomADList = $this->bomADList;
                $paginatedflag = true;
            }
            $bomADListView = new ViewModel(array(
                'bomADList' => $bomADList,
                'paginated' => $paginatedflag,
                'status' => $this->getStatusesList(),
                'userDateFormat' => $userDateFormat,
            ));

            $bomADListView->setTemplate('inventory/bom/ad-list-search');
            $bomADListView->setTerminal(true);

            $this->html = $bomADListView;
            return $this->JSONRespondHtml();
        }
    }

    function checkSelectedIdUsageAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productID = $request->getPost('productID');
            $bomDetails = $this->CommonTable('Inventory\Model\BomAssemblyDisassemblyTable')->getBomADDetailsByBomID($productID);
            if (count($bomDetails) > 0) {
                $this->status = false;
                $this->msg = $this->getMessage("ERROR_USED_IN_AD_OPARATION");
                return $this->JSONRespond();
            }
            $this->status = true;
            return $this->JSONRespond();
        }
    }

}
