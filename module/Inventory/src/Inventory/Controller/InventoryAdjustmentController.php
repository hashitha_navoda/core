<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains Inventory Adjustment related controller functions
 */

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Inventory\Form\InventoryAdjustmentForm;
use Zend\I18n\Translator\Translator;
use Zend\Session\Container;
use Inventory\Form\AdjustmentItemImportForm;

class InventoryAdjustmentController extends CoreController
{

    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'inv_adjustment_upper_menu';
    protected $downMenus = '';
    protected $paginator;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'inventory_side_menu'.$this->packageID;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Adjustment', 'Create Adjustment', 'INVENTORY');
        $inveAdjustForm = new InventoryAdjustmentForm(array(
            'name' => 'inventoryAdjustForm',
            'id' => 'inveNo'
        ));
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'location',
            'uomByID',
            'uomByProductID',
            'uomByAbbr'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory_adjustment/inventory-adjustment.js');

        $goodIssueTypes = $this->CommonTable('Inventory\Model\GoodsIssueTypeTable')->fetchAll();

        $goodIssueType = array();
        foreach ($goodIssueTypes as $key => $g) {
            $goodIssueType[$key] = $g;
        }

        $userLocations = $this->userLocations();
        $dateFormat = $this->getUserDateFormat();
        $index = new ViewModel(array(
            'inventoryAdjustForm' => $inveAdjustForm,
            'goodsIssueTypes' => $goodIssueType,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'userLocations' => $userLocations,
            'userActiveLocID' => $this->user_session->userActiveLocation['locationID'],
            'dateFormat' => $dateFormat
        ));

        $negativeAdjustmentModal = new ViewModel();
        $negativeAdjustmentModal->setTemplate('/inventory/inventory-adjustment/add-negative-adjustment-products');
        $index->addChild($negativeAdjustmentModal, 'addNegativeAdjustmentProducts');
        $autoFillView = new ViewModel(array('dateFormat' => $dateFormat));
        $autoFillView->setTemplate('inventory/serialNumberAutoFill');
        $positiveAdjustmentModal = new ViewModel(array('dateFormat' => $dateFormat));
        $positiveAdjustmentModal->setTemplate('/inventory/inventory-adjustment/add-positive-adjustment-products');
        $positiveAdjustmentModal->addChild($autoFillView, 'SerialNumberAutoFill');
        $index->addChild($positiveAdjustmentModal, 'addPositiveAdjustmentProducts');

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $index->addChild($dimensionAddView, 'dimensionAddView');
        
        $uploadForm = new AdjustmentItemImportForm();

        $adjustmentUploadView = new ViewModel(array('uploadForm' => $uploadForm));
        $adjustmentUploadView->setTemplate('inventory/inventory-adjustment/adjustment-product-upload');
        $index->addChild($adjustmentUploadView, 'adjustmentUploadView');

        $adjustmentItemImpotView = new ViewModel();
        $adjustmentItemImpotView->setTemplate('inventory/inventory-adjustment/adjustment-item-import');
        $index->addChild($adjustmentItemImpotView, 'adjustmentItemImpotView');

        $adjustmentItemUomUploadView = new ViewModel();
        $adjustmentItemUomUploadView->setTemplate('inventory/inventory-adjustment/adjustment-item-upload-uom-map');
        $index->addChild($adjustmentItemUomUploadView, 'adjustmentItemUomUploadView');

        $refNotSet = new ViewModel(array(
            'title' => 'Reference Number not set',
            'msg' => $this->getMessage('REQ_OUTPAY_ADD'),
            'controller' => 'company',
            'action' => 'reference'
        ));
        $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
        $index->addChild($refNotSet, 'refNotSet');

        return $index;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return \Zend\View\Model\ViewModel
     */
    public function viewAction()
    {
        $this->getSideAndUpperMenus('Adjustment', 'View Adjustments', 'INVENTORY');
        $this->getPaginatedGoodsIssue();
        $searchHeaderView = new ViewModel();
        $searchHeaderView->setTemplate('inventory/inventory-adjustment/search-header');
        $dateFormat = $this->getUserDateFormat();

        $adjustmentListView = new ViewModel(array('adjustment' => $this->paginator, 'companyCurrencySymbol' => $this->companyCurrencySymbol, 'statuses' => $this->getStatusesList(), 'dateFormat' => $dateFormat));
        $adjustmentListView->setTemplate('inventory/inventory-adjustment/adjustment-list');

        $mainViewModel = new ViewModel();
        $mainViewModel->addChild($searchHeaderView, 'adjustmentSearchHeader');
        $mainViewModel->addChild($adjustmentListView, 'adjustmentList');

        $modal = new ViewModel();
        $modal->setTemplate('/inventory/inventory-adjustment/load-edit-adjustment');
        $mainViewModel->addChild($modal, 'load_edit_adjustment');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $mainViewModel->addChild($attachmentsView, 'attachmentsView');

        $this->getViewHelper('HeadScript')->prependFile('/js/inventory_adjustment/view.js');

        return $mainViewModel;
    }

    public function adjustmentViewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $this->getSideAndUpperMenus('Adjustment', 'View Adjustments', 'INVENTORY');
            $adjustmentData = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getGoodIssueByID($paramIn);

            $adjustmentDetails = array();
            $adjusmentID = NULL;
            if ($adjustmentData != null) {
                foreach ($adjustmentData as $value) {
                    $adjusmentID = $value['goodsIssueID'];
                    $tempA = array();

                    $tempA['aID'] = $value['goodsIssueCode'];
                    $tempA['aL'] = $value['locationCode'] . '-' . $value['locationName'];
                    $tempA['aD'] = $this->convertDateToUserFormat($value['goodsIssueDate']);
                    $tempA['aR'] = $value['goodsIssueReason'];
                    $tempA['aT'] = $value['goodsIssueTypeName'];
                    $tempA['aTID'] = $value['goodsIssueTypeID'];
                    $tempA['aCmnt'] = $value['goodsIssueComment'];

                    $adjustmentProducts = (isset($adjustmentDetails[$value['goodsIssueID']]['aProducts'])) ? $adjustmentDetails[$value['goodsIssueID']]['aProducts'] : array();

                    if ($value['productID'] != NULL) {

                        $productBatches = (isset($adjustmentProducts[$value['productCode']]['bP'])) ? $adjustmentProducts[$value['productCode']]['bP'] : array();
                        $productSerials = (isset($adjustmentProducts[$value['productCode']]['sP'])) ? $adjustmentProducts[$value['productCode']]['sP'] : array();
                        $productProduct = (isset($adjustmentProducts[$value['productCode']]['pP'])) ? $adjustmentProducts[$value['productCode']]['pP'] : array();

                        $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);

                        $unitPrice = $this->getProductUnitPriceViaDisplayUom($value['unitOfPrice'], $productUom);
                        $productQtyDetails = $this->getProductQuantityViaDisplayUom($value['goodsIssueProductQuantity'], $productUom);
                        $productQtyDetails['quantity'] = ($productQtyDetails['quantity'] == 0) ? '-' : $productQtyDetails['quantity'];

                        $adjustmentProducts[$value['productCode']] = array(
                            'aPC' => $value['productCode'],
                            'aPN' => $value['productName'],
                            'aPQ' => $productQtyDetails['quantity'],
                            'aPUom' => $productQtyDetails['uomAbbr'],
                            'aP' => $unitPrice,
                            'uCR' => $productQtyDetails['productUomConversion'],
                            'aPUomDecimal' => $productQtyDetails['uomDecimalPlace']
                        );

                        if ($value['productBatchID'] != NULL && $value['productSerialID'] == NULL) {
                            $productBatches[$value['productBatchID']] = array('bC' => $value['productBatchCode'], 'bED' => $this->convertDateToUserFormat($value['productBatchExpiryDate']), 'bW' => $value['productBatchWarrantyPeriod'], 'bMD' => $this->convertDateToUserFormat($value['productBatchManufactureDate']), 'bQ' => $productQtyDetails['quantity']);
                        }

                        if ($value['productSerialID']) {
                            $productSerials[$value['productSerialID']] = array('sC' => $value['productSerialCode'], 'bC' => $value['productBatchCode'], 'sW' => $value['productSerialWarrantyPeriod'], 'sED' => $this->convertDateToUserFormat($value['productSerialExpireDate']));
                        }

                        if ($value['productBatchID'] == NULL && $value['productSerialID'] == NULL) {
                            $productProduct[$value['locationProductID']] = array('pC' => $value['productCode']);
                        }

                        $adjustmentProducts[$value['productCode']]['bP'] = $productBatches;
                        $adjustmentProducts[$value['productCode']]['sP'] = $productSerials;
                        $adjustmentProducts[$value['productCode']]['pP'] = $productProduct;
                    }

                    $tempA['aProducts'] = $adjustmentProducts;
                    $adjustmentDetails[$value['goodsIssueID']] = $tempA;
                }
            } else {
                $this->msg = $this->getMessage('ERR_INVENADJ_NODATA');
                $this->status = FALSE;

                return $this->JSONRespond();
            }

            $adjusmentForm = new InventoryAdjustmentForm();

            $adjusmentForm->get('inve_no')->setValue($adjustmentDetails[$adjusmentID]['aID']);
            $adjusmentForm->get('inve_no')->setAttribute('disabled', TRUE);
            $adjusmentForm->get('adjustment_type')->setValue($adjustmentDetails[$adjusmentID]['aT']);
            $adjusmentForm->get('adjustment_type')->setAttribute('disabled', TRUE);
            $adjusmentForm->get('issue_date')->setValue($adjustmentDetails[$adjusmentID]['aD']);
            $adjusmentForm->get('issue_date')->setAttribute('disabled', TRUE);
            $adjusmentForm->get('location_name')->setValue($adjustmentDetails[$adjusmentID]['aL']);
            $adjusmentForm->get('location_name')->setAttribute('disabled', TRUE);
            $adjusmentForm->get('reason')->setValue($adjustmentDetails[$adjusmentID]['aR']);
            $adjusmentForm->get('reason')->setAttribute('disabled', TRUE);
            $adjusmentForm->get('cmnt')->setValue($adjustmentDetails[$adjusmentID]['aCmnt']);
            $adjusmentForm->get('cmnt')->setAttribute('disabled', TRUE);
            $adjustmentView = new ViewModel(array(
                'adjusmentForm' => $adjusmentForm,
                'adjusmentData' => $adjustmentDetails[$adjusmentID],
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                    )
            );
            $adjustmentView->setTemplate('inventory/inventory-adjustment/adjustment-view');
            return $adjustmentView;
        }
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * set paginator
     */
    protected function getPaginatedGoodsIssue()
    {
        $locationId = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Inventory\Model\GoodsIssueTable')->fetchAll(true, $locationId);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @param type $helperName
     * @return type
     * Helper for vars js file
     */
    protected function getViewHelper($helperName)
    {
        return $this->getServiceLocator()->get('viewhelpermanager')->get($helperName);
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     *
     * use to get adjustment print view.
     */
    public function previewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $data = $this->getDataForDocument($paramIn);
            $path = "/inventory-adjustment/document/"; //.$paramIn;
            $translator = new Translator();
            $createNew = $translator->translate('New Adjustment');
            $createPath = "/inventory-adjustment";
            $data["email"] = array(
                "to" => '',
                "subject" => " ",
                "body" => <<<EMAILBODY

A Adjustment has been generated and is attached herewith. <br /><br />

<strong>Adjustment No:</strong> {$data['adjustmentCode']} <br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
            );

            $journalEntryValues = [];
            if($this->useAccounting == 1){
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('16',$paramIn);
                $journalEntryID = $journalEntryData['journalEntryID'];
                $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
                $journalEntryValues = $jeResult['JEDATA']; 
            }

            $JEntry = new ViewModel(
                array(
                 'journalEntry' => $journalEntryValues,
                 'closeHidden' => true 
                 )
                );
            $JEntry->setTemplate('accounting/journal-entries/view-modal');

            $documentType = 'Inventory Adjustment';
            $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paramIn, $createPath);
            $preview->addChild($JEntry, 'JEntry');

            $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
            $additionalButton->setTemplate('additionalButton');
            $preview->addChild($additionalButton, 'additionalButton');

            return $preview;
        }
    }

    public function documentPdfAction()
    {
        $adjustmentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Inventory Adjustment';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/inventory/inventory-adjustment/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($adjustmentID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($adjustmentID, $documentType, $documentData, $templateID);

        return;
    }

    public function documentAction()
    {
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $inventoryAdjustmentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Inventory Adjustment';
        echo $this->generateDocument($inventoryAdjustmentID, $documentType, $templateID);
        exit;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * get data for adjustment print view page
     */
    public function getDataForDocument($inventoryAdjustmentID)
    {
        if (!empty($this->_adjustmentViewData)) {
            return $this->_adjustmentViewData;
        }
        $inventoryAdjustmentDetails = (object) $this->CommonTable('Inventory\Model\GoodsIssueTable')->getDataToPreview($inventoryAdjustmentID);
        $inventoryAdjustmentData = [];
        $itemArray = array();
       
        foreach ($inventoryAdjustmentDetails as $t) {
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($t['productID']);
                $productQtyDetails = $this->getProductQuantityViaDisplayUom($t['goodsIssueProductQuantity'], $productUom);
                $qty = ($productQtyDetails['quantity'] == 0) ? 0 : $productQtyDetails['quantity'];
            if (!in_array($t['productID'], $itemArray)) {
                $itemArray[] = $t['productID'];

                $t['unitOfPrice'] = $this->getProductUnitPriceViaDisplayUom($t['unitOfPrice'], $productUom);
                $t['uomAbbr'] = $productQtyDetails['uomAbbr'];
                $inventoryAdjustmentData['inventdata'][$t['productID']] = $t;
                $inventoryAdjustmentData['inventdata'][$t['productID']]['sumProductQty'] = floatval($qty);
               
            } else {
                $inventoryAdjustmentData['inventdata'][$t['productID']]['sumProductQty'] += floatval($qty);
            }

            if ($t['goodsIssueTypeID'] == '2') {
                $adjustmentType = 'Positive';
            } else {
                $adjustmentType = 'Negative';
            }
            
            $inventoryAdjustmentData['adjustmentCode'] = $t['goodsIssueCode'];
            $inventoryAdjustmentData['adjustmentDate'] = $t['goodsIssueDate'];
            $inventoryAdjustmentData['adjustmentType'] = $adjustmentType;
            $inventoryAdjustmentData['adjustmentComment'] = $t['goodsIssueComment'];
            $inventoryAdjustmentData['adjustmentReson'] = $t['goodsIssueReason'];
            $inventoryAdjustmentData['userUsername'] = $t['userUsername'];
            $inventoryAdjustmentData['createdTimeStamp'] = $this->getUserDateTime($t['createdTimeStamp']);
        }
       
        $basicData = $this->getDataForDocumentView();
        $data = array_merge($basicData, $inventoryAdjustmentData);

        // set variables
        $data['table'] = array(
            'col_size' => array(10, 90, 290, 50, 100, 100, 100),
            'col_allign' => array("left", "left", "left", "right", "right", "right"),
            'headers' => array(
                _("Item Code") => 2,
                _("Item Name") => 1,
                _("qty") => 1,
                _("price</br>" . "(" . $data['currencySymbol'] . ")") => 1,
                _("total</br>" . "(" . $data['currencySymbol'] . ")") => 1
            ),
            'records' => $records,
        );
        return $this->_adjustmentViewData = $data;
    }

//set adjustment print view template.
    public function getDocumentDataTable($inventoryAdjustmentID, $documentSize = 'A4')
    {
        $data_table_vars = $this->getDataForDocument($inventoryAdjustmentID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/inventory/inventory-adjustment/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }
}
