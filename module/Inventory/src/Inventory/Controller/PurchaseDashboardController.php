<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class PurchaseDashboardController extends CoreController
{
	protected $sideMenus = 'purchasing_side_menu';
    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'purchasing_side_menu'.$this->packageID;
        }
    }


    public function indexAction()
    {
        $this->getSideAndUpperMenus('Purchase Dashboard', null, 'PURCHASING');
        
        $viewmodel = new ViewModel(array(
            // 'finalProductArray' => $finalProductArray,
        ));

        $this->getViewHelper('HeadLink')->prependStylesheet('/css/purchasing-dashboard.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/purchasing-dashboard.js');

        return $viewmodel;
    }
}