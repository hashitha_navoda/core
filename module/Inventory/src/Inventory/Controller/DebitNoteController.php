<?php

/**
 * @author ashan madushka <ashan@thinkcube.com>
 * This file contains debit note Process related controller functions
 */

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Inventory\Form\DebitNoteForm;

class DebitNoteController extends CoreController
{

    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'debit_note_upper_menu';
    protected $userID;
    protected $user_session;
    protected $company;
    protected $useAccounting;
    private $_debitNoteViewData;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'purchasing_side_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Debit Note', 'Create Debit Note', 'PURCHASING');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(11, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $paymentterm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        while ($t = $paymentterm->current()) {
            $payterm[$t->paymentTermID] = $t->paymentTermName;
        }
        $debitNoteForm = new DebitNoteForm();
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);
        $this->getViewHelper('HeadScript')->prependFile('/js/Dn/debitNote.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/calculations.js');
        $debitNoteForm->get('supplier')->setAttribute('disabled', true);
        $debitNoteAddView = new ViewModel(
                array(
            'paymentTerm' => $payterm,
            'debitNoteForm' => $debitNoteForm,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'referenceNumber' => $rid,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
                )
        );
        $debitNoteProductsView = new ViewModel();
        $debitNoteProductsView->setTemplate('inventory/debit-note/debit-note-add-products');
        $debitNoteAddView->addChild($debitNoteProductsView, 'debitNoteAddProducts');

        $directDebitNoteProductsView = new ViewModel();
        $directDebitNoteProductsView->setTemplate('inventory/debit-note/direct-debit-note-add-sub-products');
        $debitNoteAddView->addChild($directDebitNoteProductsView, 'directDebitProducts');

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $debitNoteAddView->addChild($dimensionAddView, 'dimensionAddView');
        
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Debit Note Reference Number not set';
                $msg = $this->getMessage('ERR_DNREF_NOT_SET');
            } else {
                $title = 'Debit Note Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DNREF_REACH_MAX_LIMIT');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $debitNoteAddView->addChild($refNotSet, 'refNotSet');
        }
        $this->setLogMessage("Debit note create page accessed.");
        return $debitNoteAddView;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Debit Note', 'View Debit Note', 'PURCHASING');
        $this->getPaginatedDebitNotes();
        $debitNoteView = new ViewModel(array(
            'debitNote' => $this->paginator,
            'statuses' => $this->getStatusesList()
                )
        );
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/debit-note/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('inventory/debit-note/document-view');
        $debitNoteView->addChild($docHistoryView, 'docHistoryView');
        $debitNoteView->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $debitNoteView->addChild($attachmentsView, 'attachmentsView');

        $this->setLogMessage("Debit note list page accessed.");
        return $debitNoteView;
    }

    private function getPaginatedDebitNotes()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Inventory\Model\DebitNoteTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function viewDebitNoteReceiptAction()
    {
        return $this->documentPreviewAction();
    }

    public function documentPreviewAction()
    {
        $debitNoteID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($debitNoteID);
        $debitNoteCode = $data['debitNoteCode'];
        $data['debitNoteID'] = $data['debitNoteID'];
        $data['total'] = number_format($data['total'], 2);
        $path = "/debit-note/document/"; //.$returnID;
        $createNew = "New Debit Note";
        $createPath = "/debit-note/create";

        $data["email"] = array(
            "to" => $data['supplierEmail'],
            "subject" => "Debit Note from " . $data['companyName'],
            "body" => <<<EMAILBODY

Dear {$data['supplierName']}, <br /><br />

Thank you for your inquiry. <br /><br />

A Debit Note has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>DN Number:</strong> {$data['debitNoteCode']} <br />
<strong>DN Amount:</strong> {$data['currencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $journalEntryValues = [];
        if($this->useAccounting == 1){
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('13',$debitNoteID);
            $journalEntryID = $journalEntryData['journalEntryID'];
            $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryValues = $jeResult['JEDATA']; 
        }

        $JEntry = new ViewModel(
            array(
               'journalEntry' => $journalEntryValues,
               'closeHidden' => true 
               )
            );
        $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $documentType = 'Debit Note';
        $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $debitNoteID, $createPath);
        $preview->addChild($JEntry, 'JEntry');

        $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
        $additionalButton->setTemplate('additionalButton');
        $preview->addChild($additionalButton, 'additionalButton');

        $this->setLogMessage("Preview debit note ".$debitNoteCode.' .');
        return $preview;
    }

    public function documentPdfAction()
    {
        $dnID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Debit Note';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/inventory/debit-note/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($dnID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($dnID, $documentType, $documentData, $templateID);

        return;
    }

    public function getDataForDocument($debitNoteID)
    {

        if (!empty($this->_debitNoteViewData)) {
            return $this->_debitNoteViewData;
        }

        $data = $this->getDataForDocumentView();

        $debitNoteDetails = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteID($debitNoteID)->current();
        $supplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($debitNoteDetails['supplierID']);
        $debitNoteDetails['createdTimeStamp'] = $this->getUserDateTime($debitNoteDetails['createdTimeStamp']);
        $data = array_merge($data, $debitNoteDetails, (array) $supplierDetails);

        // to be used by below functions - convert to object
        $debitNoteDetails = (object) $debitNoteDetails;

        $debitNoteProductDetails = $this->CommonTable('Inventory\Model\DebitNoteProductTable')->getAllDebitNoteProductDetailsByDebitNoteID($debitNoteID);

        $txTypes = array();
        $sub_total = 0;
        foreach ($debitNoteProductDetails as $product) {

            $product = (object) $product;

            $total_itm_tx = 0;
            $productsTax = $this->CommonTable('Inventory\Model\DebitNoteProductTaxTable')->getDebitNoteProductTaxByDebitNoteProductID($product->debitNoteProductID);
            foreach ($productsTax as $productTax) {
                if (!isset($txTypes[$productTax['taxName']])) {
                    $txTypes[$productTax['taxName']] = $productTax['debitNoteProductTaxAmount'];
                } else {
                    $txTypes[$productTax['taxName']] += $productTax['debitNoteProductTaxAmount'];
                }
                $total_itm_tx += $productTax['debitNoteProductTaxAmount'];
            }

            $itemDiscount = ($product->debitNoteProductDiscountType == 'precentage') ? $product->debitNoteProductPrice * $product->debitNoteProductDiscount / 100 : $product->debitNoteProductDiscount;
            $item_tax_string = ($total_itm_tx > 0) ? "Tax " . number_format($total_itm_tx, 2) : "";
            $item_disc_string = "";

            if ($itemDiscount > 0) {
                if ($product->debitNoteProductDiscountType == 'precentage') {
                    $item_disc_string = "Disc: " . number_format($product->debitNoteProductDiscount, 2) . "%";
                } else {
                    $item_disc_string = "Disc: " . $data['currencySymbol'] . number_format($itemDiscount, 2) . "";
                }
            }
            $sub_line = "";
            if ($total_itm_tx > 0) {
                if ($item_tax_string != "" && $item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                } else if ($item_tax_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ")</small>";
                } else if ($item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                }
                $unit = $product->debitNoteProductPrice;
            } else {
                $unit = $product->debitNoteProductPrice + ($total_itm_tx / $product->debitNoteProductQuantity);
                $sub_line = ($product->debitNoteProductDiscount > 0) ? "<br><small>(" . $item_disc_string . ")</small>" : "";
            }

            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID);

            $unitPrice = $this->getProductUnitPriceViaDisplayUom($unit, $productUom);
            $thisqty = $this->getProductQuantityViaDisplayUom($product->debitNoteProductQuantity, $productUom);
            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

            $records[] = array(
                $product->productCode,
                $product->productName . $sub_line,
                $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'],
                number_format($unitPrice, 2),
                number_format($product->debitNoteProductTotal, 2),
            );
            $sub_total+= $product->debitNoteProductTotal;
        }
        foreach ($txTypes as $t_name => $t_val) {
            $tx_line.=($tx_line == null) ? $t_name . ":&nbsp; " . $this->companyCurrencySymbol . " " . number_format($t_val, 2) :
                    ",&nbsp;" . $t_name . ":&nbsp; " . $this->companyCurrencySymbol . " " . number_format($t_val, 2);
        }

        $data['type'] = _("Debit Note");
        $data['sup_name'] = '';
        $data['state'] = $debitNoteDetails->statusID;
        $data['doc_data'] = array(
            _("Date") => $debitNoteDetails->debitNoteDate,
            _("Valid till") => '',
            _("Payment terms") => '',
            _("Delivery Note No.") => '',
            _("Debit Note No") => $debitNoteDetails->debitNoteCode,
        );
        $data['sup_data'] = array(
            _("address") => '',
        );
        $data['table'] = array(
            'col_size' => array(10, 90, 290, 50, 100, 100),
            'col_allign' => array("left", "left", "left", "right", "right"),
            'headers' => array(
                _("product code") => 2,
                _("product") => 1,
                _("return qty") => 1,
                _("price") => 1,
                _("total") => 1
            ),
            'records' => $records,
        );
        $data['comment'] = $debitNoteDetails->debitNoteComment;
        $data['sub_total'] = $sub_total;
        $data['total'] = $debitNoteDetails->debitNoteTotal;
        $data['discount'] = '';
        $data['discountStatement'] = '';
        $data['show_tax'] = true;
        $data['tax_record'] = $tx_line;
        $data['supplierName'] = $data['supplierCode'] . ' ' . $data['supplierName'];

        return $this->_debitNoteViewData = $data;
    }

    public function documentAction()
    {
        $documentType = 'Debit Note';
        $debitNoteID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        echo $this->generateDocument($debitNoteID, $documentType, $templateID);
        exit;
    }

    public function getDocumentDataTable($debitNoteID, $documentSize = 'A4')
    {
        $data_table_vars = $this->getDataForDocument($debitNoteID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/inventory/debit-note/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Get all Debit note list to view page by ajax
     * @return boolean|\Zend\View\Model\ViewModel
     */
    public function getDebitNoteListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->getPaginatedDebitNotes();
            $debitNoteView = new ViewModel(array(
                'debitNote' => $this->paginator,
                'statuses' => $this->getStatusesList(),
                'paginated' => true
                    )
            );
            $debitNoteView->setTerminal(TRUE);
            $debitNoteView->setTemplate('inventory/debit-note/debit-note-list');
            return $debitNoteView;
        }
        return false;
    }

}
