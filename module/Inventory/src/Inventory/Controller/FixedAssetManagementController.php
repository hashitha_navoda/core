<?php
namespace Inventory\Controller;

use Core\Controller\CoreController;
use Inventory\Form\GiftCardForm;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class FixedAssetManagementController extends CoreController
{

    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'fixed_asset_upper_menu';
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }


    public function listFixedAssetAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $this->getSideAndUpperMenus('Fixed Asset Management', 'View Fixed Assets', 'INVENTORY');

        $this->getPaginatedProducts($locationID, true);
        $userLocation = $this->user_session->userActiveLocation['locationID'];

        $proIds = [];
        $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown([$userLocation], $productSearchKey = NULL, $isSearch = FALSE, false, null, true);

        foreach ($productData as $value) {
            $proIds[$value['productID']] = $value['productID'];
        }

        if (!empty($proIds)) {
            $globalStockValueData = $this->getService('StockInHandReportService')->getProductsWithGRNCosting($proIds, [$userLocation], false, []);
            $depreciation = $this->CommonTable('Inventory/Model/DepreciationTable')->getTotalDepreciationByProductIDArray($proIds);
        } else {
            $globalStockValueData = 0;
            $depreciation = 0;
        }


        $depreciationData=[];
        foreach ($depreciation as $value) {
            $depreciationData[$value['productID']] = ($value['totalDepreciation'] == null) ? 0 : $value['totalDepreciation'];
        }

        $productListView = new ViewModel(array(
            'cSymbol' => $this->companyCurrencySymbol,
            'products' => $this->paginator,
            'globalStockValueData' => $globalStockValueData,
            'depreciationData' => $depreciationData,
            'userLocation' => $userLocation,
            'paginated' => true
        ));

        $productListView->setTemplate('inventory/product/fixed-asset-list-table');
        $mainViewModel = new ViewModel();
        $mainViewModel->addChild($productListView, 'productList');

        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/script.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['categories']);
        $this->getViewHelper('HeadScript')->prependFile('/js/product.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.min.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        $this->setLogMessage("Add product view accessed");
        return $mainViewModel;
    }

    public function depreciateAction()
    {
        $this->getSideAndUpperMenus('Fixed Asset Management', 'View Fixed Assets', 'INVENTORY');

        $productID = $this->params()->fromRoute('param1');
        $productsArr = $this->CommonTable('Inventory/Model/ProductTable')->getProductByProductID($productID);

        $userLocation = $this->user_session->userActiveLocation['locationID'];

        $globalStockValueData = $this->getService('StockInHandReportService')->getProductsWithGRNCosting([$productID], [$userLocation], false, []);

        $productValue = $globalStockValueData[$productID]['pD'][$userLocation]['TotalValue'] / $globalStockValueData[$productID]['pD'][$userLocation]['aQty'];

        $productValue = ($productValue == false) ? 0 : $productValue;

        $depreciation = $this->CommonTable('Inventory/Model/DepreciationTable')->getTotalDepreciationByProductID($productID)->current();
        $depreciationValue = ($depreciation['totalDepreciation'] == null) ? 0 : $depreciation['totalDepreciation'];

        $financeAccountsArray = $this->getFinanceAccounts();

        $productDepereciationAccountName = $financeAccountsArray[$productsArr['productDepreciationAccountID']]['financeAccountsCode']."_".$financeAccountsArray[$productsArr['productDepreciationAccountID']]['financeAccountsName'];

        $newAssetValue = floatval($productValue) - floatval($depreciationValue);
        $userdateFormat = $this->getUserDateFormat();
        $form = new GiftCardForm($data);
        $view = new ViewModel(array(
            'form' => $form,
            'useAccounting' => $this->useAccounting,
            'productName' => $productsArr['productName'],
            'productValue' => $productValue,
            'productID' => $productID,
            'depreciationValue' => $depreciationValue,
            'newAssetValue' => $newAssetValue,
            'userdateFormat' => $userdateFormat,
            'productDepereciationAccountName' => $productDepereciationAccountName,
            'productDepreciationAccountID' => $productsArr['productDepreciationAccountID'],
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/depreciation.js');
        return $view;
    }


    public function disposeAction()
    {
        $this->getSideAndUpperMenus('Fixed Asset Management', 'View Fixed Assets', 'INVENTORY');
       
        $productID = $this->params()->fromRoute('param1');
        $productsArr = $this->CommonTable('Inventory/Model/ProductTable')->getProductByProductID($productID);

        $userLocation = $this->user_session->userActiveLocation['locationID'];

        $globalStockValueData = $this->getService('StockInHandReportService')->getProductsWithGRNCosting([$productID], [$userLocation], false, []);

        $productValue = $globalStockValueData[$productID]['pD'][$userLocation]['TotalValue'] / $globalStockValueData[$productID]['pD'][$userLocation]['aQty'];

        $productValue = ($productValue == false) ? 0 : $productValue;

        $depreciation = $this->CommonTable('Inventory/Model/DepreciationTable')->getTotalDepreciationByProductID($productID)->current();
        $depreciationValue = ($depreciation['totalDepreciation'] == null) ? 0 : $depreciation['totalDepreciation'];

        $financeAccountsArray = $this->getFinanceAccounts();

        $productDepereciationAccountName = $financeAccountsArray[$productsArr['productDepreciationAccountID']]['financeAccountsCode']."_".$financeAccountsArray[$productsArr['productDepreciationAccountID']]['financeAccountsName'];

        $newAssetValue = floatval($productValue) - floatval($depreciationValue);
        $userdateFormat = $this->getUserDateFormat();
        $form = new GiftCardForm($data);
        $view = new ViewModel(array(
            'form' => $form,
            'useAccounting' => $this->useAccounting,
            'productName' => $productsArr['productName'],
            'productValue' => $productValue,
            'productID' => $productID,
            'depreciationValue' => $depreciationValue,
            'newAssetValue' => $newAssetValue,
            'userdateFormat' => $userdateFormat,
            'productDepereciationAccountName' => $productDepereciationAccountName,
            'productDepreciationAccountID' => $productsArr['productDepreciationAccountID'],
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/disposal.js');
        return $view;
    }

    public function getPaginatedProducts($locationID, $fixedAssetFlag = false)
    {
        $this->paginator = $this->CommonTable('Inventory/Model/ProductTable')->fetchAll(true, $locationID, $fixedAssetFlag);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }
}