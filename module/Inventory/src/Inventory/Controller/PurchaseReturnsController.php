<?php

namespace Inventory\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Zend\I18n\Translator\Translator;

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */
class PurchaseReturnsController extends CoreController
{

    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'purchaseReturns_upper_menu';
    protected $_prViewDetails;
    protected $paginator;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'purchasing_side_menu'.$this->packageID;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This is the purchase returns create function
     */
    public function createAction()
    {
        $this->getSideAndUpperMenus('Returns', 'Create PR', 'PURCHASING');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        while ($row = $currencyResultSet->current()) {
            $currencies[$row->currencyID] = $row->currencyName;
        }
        $refData = $this->getReferenceNoForLocation(15, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];
        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        while ($row = $paymentTermsResultSet->current()) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }
        $prForm = new GrnForm();
        $prForm->setAttribute('id', "prForm");
        $prForm->get('discount')->setAttribute('id', 'prDiscount');
        $prForm->get('supplier')->setAttribute('disabled', true);
        $prForm->get('retrieveLocation')->setAttribute('disabled', true);
        $prForm->get('supplierReference')->setAttribute('disabled', true);
        $prForm->get('grnNo')->setAttribute('id', 'prNo')->setAttribute('name', 'prNo');
        $prForm->get('grnSaveButton')->setAttribute('id', 'prSave')->setAttribute('name', 'prSaveButton');
        $prForm->get('grnCancelButton')->setAttribute('id', 'prCancel')->setAttribute('name', 'prCancelButton');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['uomByID', 'taxes']);
        $this->getViewHelper('HeadScript')->prependFile('/js/pr/pr.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/calculations.js');
        $dateFormat = $this->getUserDateFormat();

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        
        $prCreateView = new ViewModel(
                array(
            'prForm' => $prForm,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'dateFormat' => $dateFormat
                )
        );
        $invoiceProductsView = new ViewModel();
        $invoiceProductsView->setTemplate('invoice/invoice/invoice-note-add-products');
        $prCreateView->addChild($invoiceProductsView, 'invoiceProducts');

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $prCreateView->addChild($dimensionAddView, 'dimensionAddView');

        $this->setLogMessage("Purchase return create page accessed.");
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'PurchaseReturns Reference Number not set';
                $msg = $this->getMessage('REQ_OUTPAY_ADD');
            } else {
                $title = 'PurchaseReturns Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('REQ_OUTPAY_CHANGE');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $prCreateView->addChild($refNotSet, 'refNotSet');
            return $prCreateView;
        } else {
            return $prCreateView;
        }
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Returns', 'View PRs', 'PURCHASING');
        $userActiveLocation = $this->user_session->userActiveLocation;
        $prList = $this->getPaginatedPurchaseReturns($userActiveLocation['locationID']);
        $this->getViewHelper('HeadScript')->prependFile('/js/pr/viewPr.js');
        $dateFormat = $this->getUserDateFormat();
        $prViewList = new ViewModel(
                array(
            'prList' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat
        ));
        $prViewList->setTemplate('inventory/purchase-returns/pr-list');
        $prView = new ViewModel();
        $prView->addChild($prViewList, 'prList');
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/purchase-returns/doc-history');
        $retunItemInGrnView = new ViewModel();
        $retunItemInGrnView->setTemplate('inventory/purchase-returns/related-in-grn');
        $documentView = new ViewModel();
        $documentView->setTemplate('inventory/purchase-returns/document-view');
        $prView->addChild($docHistoryView, 'docHistoryView');
        $prView->addChild($retunItemInGrnView, 'retunItemInGrnView');
        $prView->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $prView->addChild($attachmentsView, 'attachmentsView');
        
        $this->setLogMessage("Purchase return list page accessed.");
        return $prView;
    }

    public function previewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $data = $this->_getDataForPrView($paramIn);
            $data['prCode'] = $data['prCd'];
            $data['total'] = number_format($data['prT'], 2);
            $path = "/pr/document/"; //.$paramIn;
            $translator = new Translator();
            $createNew = $translator->translate('New Purchase Return');
            $createPath = "/pr/create";

            $data["email"] = array(
                "to" => $data['prSEmail'],
                "subject" => "Purchase Return from " . $data['companyName'],
                "body" => <<<EMAILBODY

Dear {$data['prSName']}, <br /><br />

Thank you for your inquiry. <br /><br />

An Purchase Return has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>PR No:</strong> {$data['prCd']} <br />
<strong>PR Amount:</strong> {$data['currencySymbol']}{$data['prT']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
            );

            $journalEntryValues = [];
            if($this->useAccounting == 1){
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('11',$paramIn);
                $journalEntryID = $journalEntryData['journalEntryID'];
                $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
                $journalEntryValues = $jeResult['JEDATA']; 
            }

            $JEntry = new ViewModel(
                array(
                 'journalEntry' => $journalEntryValues,
                 'closeHidden' => true 
                 )
                );
            $JEntry->setTemplate('accounting/journal-entries/view-modal');

            $documentType = 'Purchase Return';
            $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paramIn, $createPath);
            $preview->addChild($JEntry, 'JEntry');

            $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
            $additionalButton->setTemplate('additionalButton');
            $preview->addChild($additionalButton, 'additionalButton');

            $this->setLogMessage("Preview purchase return ".$data['prCode'].' .');
            return $preview;
        }
    }

    public function documentAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = 'Purchase Return';
        $prID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $prID, $templateID);
//        if ($document = $tpl->getDocumentFile($filename, $documentType)) {
//            echo $document;
//            exit;
//        }
        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForPrView($prID);
        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);
        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($prID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                $data['data_table_indetail'] = $this->_getDocumentDataTable($prID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])) . '-indetail'));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    private function _getDocumentDataTable($prID, $documentSize = 'A4')
    {

        $data_table_vars = $this->_getDataForPrView($prID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/inventory/purchase-returns/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    private function _getDataForPrView($prID)
    {

        if (!empty($this->_prViewDetails)) {
            return $this->_prViewDetails;
        }

        $data = $this->getDataForDocumentView();

        $prRelatedDetails = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnDetailsByPrID($prID);
        $prDetails = array();
       
        foreach ($prRelatedDetails as $prData) {
            if ($prData['directReturnFlag']){
                $supplierTitle = $prData['prSupplierTitle'];
                $supplierName = $prData['prSupplierName'];
                $locationCode = $prData['pLocationCode'];
                $retriveLocationID = $prData['prLocationID'];
                $locationName = $prData['pLocationName'];
            } else {
                $supplierTitle = $prData['gSupplierTitle'];
                $supplierName = $prData['gSupplierName'];
                $locationCode = $prData['gLocationCode'];
                $retriveLocationID = $prData['grnRetrieveLocation'];
                $locationName = $prData['gLocationName'];
            }
            $tempPr = array();
            $prID = $prData['purchaseReturnID'];
            $tempPr['prID'] = $prData['purchaseReturnID'];
            $tempPr['prCd'] = $prData['purchaseReturnCode'];
            $tempPr['prGRNCd'] = $prData['grnCode'];
            $tempPr['prSID'] = $prData['grnSupplierID'];
            $tempPr['prSEmail'] = $prData['supplierEmail'];
            $tempPr['prSAddress'] = $prData['supplierAddress'];
            $tempPr['prSName'] = $supplierTitle . ' ' . $supplierName;
            $tempPr['prSR'] = $prData['grnSupplierReference'];
            $tempPr['prRLID'] = $retriveLocationID;
            $tempPr['pRL'] = $locationCode . '-' . $locationName;
            $tempPr['prD'] = $this->convertDateToUserFormat($prData['purchaseReturnDate']);
            $tempPr['prT'] = $prData['purchaseReturnTotal'];
            $tempPr['prC'] = $prData['purchaseReturnComment'];
            $tempPr['prSTax'] = $prData['purchaseReturnShowTax'];
            $tempPr['userUsername'] = $prData['userUsername'];
            $tempPr['createdTimeStamp'] = $this->getUserDateTime($prData['createdTimeStamp']);
            $prProducts = (isset($prDetails[$prID]['prProducts'])) ? $prDetails[$prID]['prProducts'] : array();
            if ($prData['productID'] != NULL) {
                $productTaxes = (isset($prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['pT'])) ? 
                    $prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['pT'] : array();
                $productBatches = (isset($prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['bP'])) ? 
                    $prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['bP'] : array();
                $productSerials = (isset($prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['sP'])) ? 
                    $prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['sP'] : array();
                $productUoms = (isset($prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['pUom'])) ? 
                    $prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['pUom'] : array();

                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($prData['productID']);

                $unitPrice = $this->getProductUnitPriceViaDisplayUom($prData['purchaseReturnProductPrice'], $productUom);
                $thisqty = $this->getProductQuantityViaDisplayUom($prData['purchaseReturnProductReturnedQty'], $productUom);
                $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                $prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']] = array(
                    'prPC' => $prData['productCode'], 
                    'prPID' => $prData['productID'], 
                    'prPN' => $prData['productName'], 
                    'lPID' => $prData['purchaseReturnLocationProductID'], 
                    'prPP' => $unitPrice, 
                    'prPD' => $prData['purchaseReturnProductDiscount'], 
                    'prRPQ' => $thisqty['quantity'], 
                    'prSRPQ' => $prData['purchaseReturnSubProductReturnedQty'], 
                    'prPT' => $prData['purchaseReturnProductTotal'], 
                    'prUom' => $thisqty['uomAbbr']);
                
                if ($prData['productBatchID'] != NULL) {
                    $productBatches[$prData['productBatchID']] = array(
                        'bC' => $prData['productBatchCode'], 
                        'bED' => $prData['productBatchExpiryDate'], 
                        'bW' => $prData['productBatchWarrantyPeriod'], 
                        'bMD' => $prData['productBatchManufactureDate'], 
                        'bRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                }
                
                if ($prData['productSerialID'] != NULL) {
                    $productSerials[$prData['productSerialID']] = array(
                        'sC' => $prData['productSerialCode'], 
                        'sW' => $prData['productSerialWarrantyPeriod'], 
                        'sED' => $prData['productSerialExpireDate'], 
                        'sBC' => $prData['productBatchCode'], 
                        'sRQ' => $prData['purchaseReturnSubProductReturnedQty']);
                }
                if ($prData['purchaseReturnTaxID'] != NULL) {
                    $productTaxes[$prData['purchaseReturnTaxID']] = array(
                        'pTN' => $prData['taxName'], 
                        'pTP' => $prData['purchaseReturnTaxPrecentage'], 
                        'pTA' => $prData['purchaseReturnTaxAmount']);
                }
                if ($prData['uomID'] != NULL) {
                    $productUoms[$prData['uomID']] = array(
                        'pUom' => $prData['uomAbbr'], 
                        'pUomID' => $prData['uomID'], 
                        'pUomCon' => $prData['productUomConversion'], 
                        'pUomDecimal' => $prData['uomDecimalPlace']);
                }
                $prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['pT'] = $productTaxes;
                $prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['bP'] = $productBatches;
                $prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['sP'] = $productSerials;
                $prProducts[$prData['productCode'].$prData['purchaseReturnProductPrice']]['pUom'] = $productUoms;
            }
            $tempPr['prProducts'] = $prProducts;
            $prDetails[$prID] = $tempPr;
        }
        $totalProTaxes = array();
        foreach ($prDetails[$prID]['prProducts'] as $product) {
            foreach ($product['pT'] as $tKey => $proTax) {
                if (isset($totalProTaxes[$tKey])) {
                    $totalProTaxes[$tKey]['pTA'] = number_format((floatval($totalProTaxes[$tKey]['pTA']) + floatval($proTax['pTA'])), 2);
                } else {
                    $totalProTaxes[$tKey] = array('pTN' => $proTax['pTN'], 'pTP' => $proTax['pTP'], 'pTA' => $proTax['pTA']);
                }
            }
        }
        $prDetails[$prID]['prTotalTax'] = $totalProTaxes;
//
        $data = array_merge($data, $prDetails[$prID]);
        $data['today_date'] = gmdate('Y-m-d');
        $data['currencySymbol'] = $this->companyCurrencySymbol;
        return $this->_prViewDetails = $data;
    }

    private function getPaginatedPurchaseReturns($userActiveLocationID = NULL)
    {
        $this->paginator = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturns(true, $userActiveLocationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    public function documentPdfAction()
    {
        $prID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Purchase Return';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/inventory/purchase-returns/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->_getDataForPrView($prID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($prID, $documentType, $documentData, $templateID);

        return;
    }

}

?>
