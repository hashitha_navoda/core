<?php

namespace Inventory\Service;

use Core\Contracts\SearchableFromCode;
use Core\Service\BaseService;
use Illuminate\Support\Collection;
use Illuminate\Validation\Factory as Validator;

class InventoryMaterialRequisitionService extends BaseService
{

	public function getDataForRequisition($data)
	{
		// get current stock details 
        $minInventoryLevel = $this->getModel('LocationProductTable')->getLocationProductMinInvntryLevelByLocationIDAndProductID($data['location'], $data['product']);

        $mainDataSet = [];
        foreach ($minInventoryLevel as $key => $minValues) {
        	$mainDataSet[$minValues['productID']] = [
        		'minQty' => isset($minValues['minTotal']) ? $minValues['minTotal'] : 0,
        		'availableQty' => isset($minValues['avlbeQty'])? $minValues['avlbeQty'] : 0,
        		'proName' => $minValues['productName'],
        		'proCode' => $minValues['productCode'],
        		'proID' => $minValues['productID'],
        		'proCatID' => $minValues['categoryID'],
        		'proCat' => $minValues['categoryName'],
        		'poAddQty' => 0,
        		'neededQty' => 0,
        		'soQty' => 0,
        		'jobQty' => 0,
        		'poQty' => 0
        	];
	        if ($data['reOdrFlag'] == 'true') {
	        	$mainDataSet[$minValues['productID']]['neededQty'] += ($mainDataSet[$minValues['productID']]['minQty'] - $mainDataSet[$minValues['productID']]['availableQty'] < 0) ? 0 : $mainDataSet[$minValues['productID']]['minQty'] - $mainDataSet[$minValues['productID']]['availableQty']; 
	        }
        }
        
        if ($data['soFlag'] == 'true') {
			// get sales order details
			$salesOrderDetails = $this->getModel('SalesOrderProductTable')->getSalesOrderProductQtyByProductID($data['fromDate'], $data['toDate'], $data['product'], $data['location']);
        	
	        foreach ($salesOrderDetails as $key => $proDetails) {
	            $mainDataSet[$proDetails['productID']]['soQty'] = ($proDetails['totalQty']- $proDetails['totalSold'] < 0) 
	                    ? 0 : floatval($proDetails['totalQty']) - floatval($proDetails['totalSold']);

	            $mainDataSet[$proDetails['productID']]['neededQty'] += ($mainDataSet[$proDetails['productID']]['availableQty'] + $proDetails['totalSold'] - $proDetails['totalQty'] > 0) 
	                    ? 0 : $proDetails['totalQty'] - ($mainDataSet[$proDetails['productID']]['availableQty'] + $proDetails['totalSold']);
	        }
        }

        if ($data['jobReqFlag'] == 'true') {
	        // get job related Details
	        $jobProductDetails = $this->getModel('JobProductTable')->jobProductsQtyForInventoryMarerialRequisition($data['fromDate'],$data['toDate'], $data['location'], $data['product']);
	        
	        foreach ($jobProductDetails as $key => $jobValue) {
	        	$mainDataSet[$jobValue['productID']]['jobQty'] = ($jobValue['reqQty']- $jobValue['issQty'] < 0) ? 0.00 : $jobValue['reqQty']- $jobValue['issQty'];

	            $mainDataSet[$jobValue['productID']]['neededQty'] += ( $mainDataSet[$jobValue['productID']]['availableQty'] + $jobValue['issQty'] - $jobValue['reqQty'] > 0) ? 0 : $jobValue['reqQty']- ( $jobValue['issQty'] + $mainDataSet[$jobValue['productID']]['availableQty']);
	        }
        	
        }
        
        if ($data['poFlag'] == 'true') {

	        // get purchase order detials 
	        $purchaseOrderDetails = $this->getModel('PurchaseOrderProductTable')->getPurchaseOrderProductQtyByProductID($data['fromDate'],$data['toDate'], null, $data['product'], $data['location']);
        	
	        foreach ($purchaseOrderDetails as $result) {
	            $mainDataSet[$result['productID']]['poQty'] = ($result['totalQty']- $result['totalAssign'] < 0) 
	                    ? 0 : floatval($result['totalQty'])- floatval($result['totalAssign']); 
	            
	            if ($mainDataSet[$result['productID']]['neededQty'] + $result['totalAssign'] - $result['totalQty'] < 0) {
	            	$mainDataSet[$result['productID']]['neededQty'] = 0;	
	            } else {
	            	$mainDataSet[$result['productID']]['neededQty'] = $mainDataSet[$result['productID']]['neededQty'] + $result['totalAssign'] - $result['totalQty'];	

	            }
	            
	        }
	        
	        // get additional purchase order details 
	        $additionalPurchaseOrderDetails = $this->getModel('PurchaseOrderProductTable')->getPurchaseOrderProductQtyByProductID($data['fromDate'],$data['toDate'], 1, $data['product'], $data['location']);
	        
	        foreach ($additionalPurchaseOrderDetails as $key => $additionalValue) {
	            $mainDataSet[$additionalValue['productID']]['poAddQty'] = ($additionalValue['totalQty']- $additionalValue['totalAssign'] < 0) ? 0.00 : $additionalValue['totalQty']- $additionalValue['totalAssign'];
	        }
        }


        foreach ($mainDataSet as $key => $dat) {
	        $productSupplierData = $this->getModel('ProductSupplierTable')->getProductSupplierByProductID($dat['proID'])->current();

	        if (!$productSupplierData) {
	        	$mainDataSet[$key]['potentialSupID'] = null;
	        	$mainDataSet[$key]['potentialSupplier'] = 'Other';
	        } else {
	        	$mainDataSet[$key]['potentialSupID'] = $productSupplierData['supplierID'];
	        	$mainDataSet[$key]['potentialSupplier'] = $productSupplierData['supplierName'].' - '.$productSupplierData['supplierCode'];
	        }
        }

        if ($data['filterBySup'] == "true") {
        	$temp = [];
        	$temp1 = [];
        	foreach ($mainDataSet as $key => $ss) {
        		$temp[$ss['potentialSupplier']][] = $ss;
        	}
        	$mainDataSet = $temp;

        	if ($data['filterByCat'] == "true") {
        		foreach ($mainDataSet as $key => $val) {
        			foreach ($val as $key => $vv) {
        				$temp1[$vv['potentialSupplier']][$vv['proCat']][] = $vv;
        			}
        		}
        		$mainDataSet = $temp1;
        	}
        } elseif ($data['filterByCat'] == "true") {
        	$temp = [];
        	foreach ($mainDataSet as $key => $cc) {
        		$temp[$cc['proCat']][] = $cc;
        	}
        	$mainDataSet = $temp;
        }
        
        return $this->returnSuccess($mainDataSet);        
       
	}

}