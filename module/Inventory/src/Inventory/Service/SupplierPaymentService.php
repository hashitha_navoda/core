<?php

namespace Inventory\Service;

use Core\Contracts\SearchableFromCode;
use Core\Service\BaseService;
use Inventory\Model\SupplierPayments;
use Illuminate\Support\Collection;
use Accounting\Model\JournalEntryAccounts;
use Expenses\Model\PaymentVoucher;
use Illuminate\Validation\Factory as Validator;


class SupplierPaymentService extends BaseService
{
    /**
    * this function use to calculate and reset supplier payment currency gain and loss
    * @param int $paymentID
    * @param boolean $useGainFlag
    * retrun array
    **/
    public function currncyGainAndLostHandle($paymentID, $useGainFlag = false)
    {
        /**************************************************************************************************************** 
        ** in this case assume that invoice total is 150 and payment amount is 100. if user need to close that invoice **
        ** mean currency gain. also invoice total is 100 and payment amount is 150 then it has currency lost. ***********
        ****************************************************************************************************************/
        
        if ($useGainFlag == 'false') {
            return $this->returnSuccess();
        }
        
        $supplierReduceOutstanding = 0;
        $supplierReduceCredit = 0;
        $documentType = '';
        $supplierID = null;
        $suppAccount = null;
  
        // get all purchase Invoices and payment vouches that related to the given payment ID
        $paymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentWithInvoiceDetailByPaymentID($paymentID);
        foreach ($paymentDetails as $key => $singlePayValue) {
            
            // store total payment amout of this payment record
            $thisTypeCashAmount = $singlePayValue['outgoingInvoiceCashAmount'];
            $thisTypeCreditAmount = $singlePayValue['outgoingInvoiceCreditAmount'];
            $currentPaymentInvoiceID = $singlePayValue['outgoingPaymentInvoiceID'];
            $supplierID = $singlePayValue['supplierID'];

            if ($singlePayValue['invoiceID'] != 0) {
                $documentType = 'purchaseInvoice';
                $documentID = $singlePayValue['invoiceID'];
                
            } else if ($singlePayValue['paymentVoucherId'] != 0) {
                
                // payment voucher implementation
                $documentType = 'paymentVoucher';
                $documentID = $singlePayValue['paymentVoucherId'];
            }

            // get previous purchase Invoice/ payment voucher paid amounts
            $purchaseInvoicePaymentVoucherDetailSet = $this->purchaseInvoiceUpdateDataReset($documentID, $documentType, $currentPaymentInvoiceID);
            $suppAccount = $purchaseInvoicePaymentVoucherDetailSet['supGlAcc'];

            // calculate currency gain and loss
            $supplierDataSet = $this->currencyGainAndLoss($purchaseInvoicePaymentVoucherDetailSet['hasToPayAmount'],$thisTypeCashAmount,$thisTypeCreditAmount,$currentPaymentInvoiceID,$documentID, $documentType);

            $supplierReduceOutstanding += floatval($supplierDataSet['outstanding']);
            $supplierReduceCredit += floatval($supplierDataSet['credit']);

            // update purchase Invoice/payment voucher paid amount and outgoing Invoice payment table
            $updatePayments = $this->updateMainDocumentsAndPaymentDocuments($supplierDataSet, $purchaseInvoicePaymentVoucherDetailSet['totalInvoiceAmount'], $documentType, $documentID, $currentPaymentInvoiceID, $supplierDataSet);

            if (!$updatePayments['status']) {
                return $updatePayments;
            }

        }

        // payment voucher without supplier, no need to update supplier 

        if ($supplierID != null) {
            // update supplier details
            $suplierUpdate = $this->updateSupplierDetails($supplierID, $supplierReduceOutstanding, $supplierReduceCredit);
            
            if (!$suplierUpdate) {
                return $this->returnError('ERR_SUPPL_UPDATE');
            }
            
        }
        

        // set data for journal entries
        $journalEntryDataSet = $this->valueSetForJournalEntry($paymentID,$supplierReduceOutstanding, $supplierReduceCredit, $supplierID, $suppAccount);

        // update journal entry
        $updateJournalEntry = $this->resetJournalEntry($journalEntryDataSet,$paymentID);
                
        return $updateJournalEntry;
    }

    /**
    * this function use to get all previous payment details that related to the given purchase invoice id or payment vaucher ID
    * @param int $documentID -- purchase invoice ID or payment voucher id
    * @param string $documentType
    * @param int $currentOutgoingInvoiceID -- currently saved outgoingInvoicePaymentID
    * return array
    **/
    public function purchaseInvoiceUpdateDataReset($documentID, $documentType, $currentOutgoingInvoiceID)
    {
        // this is purchase invoice record
        // get previous payment related to that purchase invoice
        $purchasePaymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentDetailsByPIIdOrPVId($documentID, $documentType);
        $totalInvoiceAmount = 0;
        $totalCashPaidAmount = 0;
        $totalCreditPaidAmount = 0;
        foreach ($purchasePaymentDetails as $key => $singlePIDetails) {
            if($documentType == 'purchaseInvoice') {
                $totalInvoiceAmount = floatval($singlePIDetails['purchaseInvoiceTotal']);
                $supGlAcc = null;
            } else if ($documentType == 'paymentVoucher') {
                $totalInvoiceAmount = floatval($singlePIDetails['paymentVoucherTotal']);
                $supGlAcc = $singlePIDetails['paymentVoucherAccountID'];
            }
            if ($singlePIDetails['outgoingPaymentInvoiceID'] != $currentOutgoingInvoiceID) {
                // get all payment method values without credit payments
                $totalCashPaidAmount += floatval($singlePIDetails['outgoingInvoiceCashAmount']);
                // get all credit payments 
                $totalCreditPaidAmount += floatval($singlePIDetails['outgoingInvoiceCreditAmount']);
            }
        }
        // calculate previous payments total paid values
        $totalPreviousPaidAmount = $totalCashPaidAmount + $totalCreditPaidAmount;
        // remaining invoice amount
        $hasToPayAmount = floatval($totalInvoiceAmount) - floatval($totalPreviousPaidAmount);
            
        return [
            'hasToPayAmount' => $hasToPayAmount,
            'totalInvoiceAmount' => $totalInvoiceAmount,
            'supGlAcc' => $supGlAcc
        ];
    }
    

    /**
    * this function use to calculate currancy gain and lost and supplier credit and outstanding balances.
    * @param float $hasToPayAmount
    * @param float $currentPaymentCashAmount
    * @param float $currentPayementCreditAmount
    * 
    * return array
    **/
    public function currencyGainAndLoss($hasToPayAmount,$currentPaymentCashAmount,$currentPayementCreditAmount)
    {
        // check currency gain and loss
        if (floatval($hasToPayAmount) > floatval($currentPaymentCashAmount) + floatval($currentPayementCreditAmount)) {
            // gain
            $supplierReduceOutstanding = floatval($hasToPayAmount) - (floatval($currentPaymentCashAmount) + floatval($currentPayementCreditAmount));
            $supplierReduceCredit = 0;
            $type = 'gain';
        } else if((floatval($currentPaymentCashAmount) + floatval($currentPayementCreditAmount)) > floatval($hasToPayAmount)){
            // loss
            $supplierReduceCredit = (floatval($currentPaymentCashAmount) + floatval($currentPayementCreditAmount)) - floatval($hasToPayAmount);
            $supplierReduceOutstanding = 0;
            $type = 'loss';
        }
        return [
            'outstanding' => $supplierReduceOutstanding,
            'credit' =>  $supplierReduceCredit,
            'type' => $type,
        ];

    }

    /** 
    * this function use to update purchase invoice/ payment voucher paid amount and outgoingPaymentInvoice table 
    * currency gain and loss value
    * @param array $currencyGainAndLossData
    * @param float $totalInvoiceAmount
    * @param string $documentType
    * @param int $documentID
    * @param int $currentPaymentInvoiceID -- this is outgoingPaymentInvoice table primary key
    * return boolean
    **/
    public function updateMainDocumentsAndPaymentDocuments($currencyGainAndLossData, $totalInvoiceAmount, $documentType, $documentID, $currentPaymentInvoiceID)
    {
        $currencyGainOrLossAmount = 0;
        if ($currencyGainAndLossData['type'] == 'gain') 
        {
            // need to update PI or PV paid amount as total amount and status need to be closed.
            if ($documentType == 'purchaseInvoice') 
            {
                // set data for update
                $purchaseInvoiceUpdatedDataSet = [
                    'purchaseInvoicePayedAmount' => floatval($totalInvoiceAmount),
                    'status' => '4'
                ];
                // update purchase Invoice table
                $updatePurchaseInvoice = $this->getModel('PurchaseInvoiceTable')->updatePiDetails($purchaseInvoiceUpdatedDataSet, $documentID);

                if (!$updatePurchaseInvoice) {
                    return $this->returnError('ERR_UPDATE_PI');
                }

            } else {
                // set data for update
                $paymentVoucherUpdatedDataSet = [
                    'paymentVoucherPaidAmount' => floatval($totalInvoiceAmount),
                    'paymentVoucherStatus' => '4',
                    'paymentVoucherID' => $documentID, 
                ];
                
                $pVObject = new PaymentVoucher();
                $pVObject->exchangeArray($paymentVoucherUpdatedDataSet);

                // update payment voucher table
                $updatePaymentVoucher = $this->getModel('PaymentVoucherTable')->updatePaymentOfPaymentVoucher($pVObject);

                if (!$updatePaymentVoucher) {
                    return $this->returnError('ERR_UPDATE_PV');
                }                
            }
            $currencyGainOrLossAmount = $currencyGainAndLossData['outstanding'];
        } else {
            $currencyGainOrLossAmount = $currencyGainAndLossData['credit'];

        }

        // set outgoingPaymentInvoice updated data set
        $data = [
            'outgoingPaymentInvoiceCurrencyGainAndLossType' => $currencyGainAndLossData['type'],
            'outgoingPaymentInvoiceGainAndLossAmount' => $currencyGainOrLossAmount,
        ];
        
        // update
        $updateOutgoingPaymentInvoiceTable = $this->getModel('SupplierInvoicePaymentsTable')->updateInvoicePaymentByInvoicePaymentID($data, $currentPaymentInvoiceID);

        if (!$updateOutgoingPaymentInvoiceTable) {
            return $this->returnError('ERR_PAYMENT_INVOICE_UPDATE');
        }
        
        return $this->returnSuccess();
    }

    /**
    * this function use to update supplier outstanding or credit values
    * @param int $supplierID
    * @param float $reduceOutstanding
    * @param float $reduseCredit
    * return boolean
    **/
    public function updateSupplierDetails($supplierID, $reduceOutstanding, $reduseCredit)
    {
        // get supplier details by given supplier ID
        $supplierDetails = $this->getModel('SupplierTable')->getSupplierFromID($supplierID);
        
        if (round(floatval($reduceOutstanding), 4) > round(floatval($reduseCredit), 4)) {
            // that's mean gain. so need to reduce outstanding
            
            // before do it need to check current outstanding value
            if (round(floatval($reduceOutstanding), 4) > floatval($supplierDetails->supplierOutstandingBalance)) {
                
                // exixsting outstanding value less than requested reduce outstanding value
                $supplierUpdateData = [
                    'supplierOutstandingBalance' => 0,
                    'supplierCreditBalance' => (floatval($supplierDetails->supplierCreditBalance)- round(floatval($reduseCredit), 4)) + (round(floatval($reduceOutstanding), 4) - floatval($supplierDetails->supplierOutstandingBalance))
                ];
            } else {
                $supplierUpdateData = [
                    'supplierOutstandingBalance' => floatval($supplierDetails->supplierOutstandingBalance) - round(floatval($reduceOutstanding), 4),
                    'supplierCreditBalance'=> floatval($supplierDetails->supplierCreditBalance) - round(floatval($reduseCredit), 4)
                ];
            }
        } else {
            // that's mean currency loss. so need to reduce credit value
            // check credit value level with requested reduce credit value

            if (round($reduseCredit, 4) > round($supplierDetails->supplierCreditBalance, 4)) {

                // existing credit value less than requested reduce credit value
                $supplierUpdateData = [
                    'supplierCreditBalance' => 0,
                    'supplierOutstandingBalance' => (floatval($supplierDetails->supplierOutstandingBalance)- floatval($reduceOutstanding)) + (floatval($reduseCredit) + floatval($supplierDetails->supplierCreditBalance)),
                ];   
            } else {
                $supplierUpdateData = [
                    'supplierCreditBalance' => floatval($supplierDetails->supplierCreditBalance) - round(floatval($reduseCredit), 4),
                    'supplierOutstandingBalance' => floatval($supplierDetails->supplierOutstandingBalance)- round(floatval($reduceOutstanding), 4) 
                 ];
            }

        }
        

        $updateSupplier = $this->getModel('SupplierTable')->updateSupplierByCurrencyGainAndLoss($supplierUpdateData, $supplierID);
        return $updateSupplier; 
    }

    /**
    * this function use to recalculate journal entry details for given payment ID
    * @param int $paymentID
    * @param float $supplierReduceOutstanding
    * @param float $supplierReduceCredit
    * @param int $supplierID
    * return 
    **/
    public function valueSetForJournalEntry($paymentID, $supplierReduceOutstanding, $supplierReduceCredit,$supplierID = null, $suppAccID = null)
    {
        $paymentMethods = [];
        
        // get payment Method details
        $paymentMethodDetails = $this->getModel('PaymentMethodTable')->fetchAll();
        foreach ($paymentMethodDetails as $key => $value) {
            $paymentMethods[$value['paymentMethodID']] = $value;
        }

        // get supplier Accounts details
        $supplierAccDetails = $this->getModel('SupplierTable')->getSupplierFromID($supplierID);

        // get gl accounts details
        $glAccDetails = $this->getModel('GlAccountSetupTable')->fetchAll()->current();
            
        $newlyCreatedAccounts = [];
        $currencyLossValue = 0;

        // get current Payment details
        $paymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentWithInvoiceDetailByPaymentID($paymentID);
        foreach ($paymentDetails as $key => $singlePayValue) {
            if ($singlePayValue['outgoingPaymentInvoiceCurrencyGainAndLossType'] == 'gain') {
                              
                // add gain value to the supplier payble accounts debit side
                // set supplier side finance account ID
                if ($supplierID != null ){
                    $supSideID = $supplierAccDetails->supplierPayableAccountID;
                } else {
                    $supSideID = $suppAccID;
                }
                if ($newlyCreatedAccounts[$supSideID]['debit']) {
                    $newlyCreatedAccounts[$supSideID]['debit'] += floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']);
                } else {
                    $newlyCreatedAccounts[$supSideID]['debit'] = floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']);

                }

                // add gain value to the currency gain and loss account
                if ($newlyCreatedAccounts[$glAccDetails->glAccountSetupGeneralExchangeVarianceAccountID]['credit']) {
                    $newlyCreatedAccounts[$glAccDetails->glAccountSetupGeneralExchangeVarianceAccountID]['credit'] += floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']); 
                } else {
                    $newlyCreatedAccounts[$glAccDetails->glAccountSetupGeneralExchangeVarianceAccountID]['credit'] = floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']);
                }

            } else {
                
                // add loss value to the payment method account
                if ($newlyCreatedAccounts[$paymentMethods[$singlePayValue['paymentMethodID']]['paymentMethodPurchaseFinanceAccountID']]['credit']) {
                    $newlyCreatedAccounts[$paymentMethods[$singlePayValue['paymentMethodID']]['paymentMethodPurchaseFinanceAccountID']]['credit'] += floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']);
                } else {
                    $newlyCreatedAccounts[$paymentMethods[$singlePayValue['paymentMethodID']]['paymentMethodPurchaseFinanceAccountID']]['credit'] = floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']);

                }                
                
                // add loss value to the currency gain and loss account
                if ($newlyCreatedAccounts[$glAccDetails->glAccountSetupGeneralExchangeVarianceAccountID]['debit']) {
                    $newlyCreatedAccounts[$glAccDetails->glAccountSetupGeneralExchangeVarianceAccountID]['debit'] += floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']); 
                } else {
                    $newlyCreatedAccounts[$glAccDetails->glAccountSetupGeneralExchangeVarianceAccountID]['debit'] = floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']);
                }
                $currencyLossValue += floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']); 

            }

        }
        return [
            'newlyCreatedAccounts' => $newlyCreatedAccounts,
            'currencyLoss' => $currencyLossValue,
            'supplierAccDetails' => $supplierAccDetails,
            'gainAndLossAcc' => $glAccDetails->glAccountSetupGeneralExchangeVarianceAccountID,
            'suppAccID' => $suppAccID
        ];
    }

    /**
    * this function use to update journal entry
    * @param array $accountDetails
    * @param int $paymentID
    * return mix
    **/
    public function resetJournalEntry($accountDetails, $paymentID)
    {
        // get payment related jE
        $documentTypeId = 14; // 14 is purchase payment type
        $journalEntryDetals = $this->getModel('JournalEntryTable')
            ->getJournalEntryDataWithAccountDetailsByDocumentTypeIDAndDocumentID($documentTypeId, $paymentID);
        
        // set journal entry details to the array
        $jEDetails = [];
        $journaEntryID = null;
        $jEAccMemo = null;
        foreach ($journalEntryDetals as $key => $jEValue) {
            $jEDetails[$jEValue['financeAccountsID']] = $jEValue;
            $journaEntryID = $jEValue['journalEntryID'];
            $jEAccMemo = $jEValue['journalEntryAccountsMemo'];
        }
        // check new account details
        
        foreach ($accountDetails['newlyCreatedAccounts'] as $key => $newAccValue) {

            if ($jEDetails[$key]) {
                
                $jEAccID = $jEDetails[$key]['journalEntryAccountsID'];
                $financeAccID = $jEDetails[$key]['financeAccountsID'];
                $docType = null;
                // need to update existing journal entries
                if($newAccValue['debit']) {
                   
                    // update debit values
                    $docType = 'debit';
                    $existingAmo = floatval($jEDetails[$key]['journalEntryAccountsDebitAmount']);
                    $newAmo = floatval($newAccValue['debit']);

                } else if ($newAccValue['credit']) {
                   
                    // update credit values
                    $docType = 'credit';
                    $existingAmo = floatval($jEDetails[$key]['journalEntryAccountsCreditAmount']);
                    $newAmo = floatval($newAccValue['credit']);
                }
                
                // update journal entry accounts
                if ($docType != null) {
                    if (!($docType == 'credit' && round($accountDetails['currencyLoss'], 4) > 0)) {
                        // this condition becase do not need to update credit value of existing journal entry when currency loss
                        $updateJERecord = $this->updateJERecords($jEAccID, $existingAmo, $newAmo, $docType, $financeAccID);
                        if (!$updateJERecord['status']) {
                            return $updateJERecord;
                        }
                        
                    } 
                    
                }
                
            
            } else {
                // create new jEAccountRecord
                if ($key == $accountDetails['gainAndLossAcc']) {
                    if($newAccValue['debit']) {
                        $creditAmount = 0;
                        if($newAccValue['credit']) {
                            $creditAmount += $newAccValue['credit'];
                        }
                        $crdAmo = $creditAmount;
                        $debAmo = $newAccValue['debit'];
                        
                    } else if ($newAccValue['credit']) {
                        $debitAmount = 0;
                        if($newAccValue['debit']) {
                            $debitAmount += $newAccValue['debit'];
                        }
                        $crdAmo = $newAccValue['credit'];
                        $debAmo = $debitAmount;
                            

                    }
                    $newCreatedJEAccData = array(
                        'journalEntryID' => $journaEntryID,
                        'financeAccountsID' => $key,
                        'financeGroupsID' => 0,
                        'journalEntryAccountsDebitAmount' => floatval($debAmo),
                        'journalEntryAccountsCreditAmount' => floatval($crdAmo),
                        'journalEntryAccountsMemo' => $jEAccMemo,
                        'yearEndCloseFlag' => 0,
                    );
                    $jEAcc = new JournalEntryAccounts();
                    $jEAcc->exchangeArray($newCreatedJEAccData);
                   
                    // save jE Account
                    $saveJEAccount = $this->getModel('JournalEntryAccountsTable')->saveJournalEntryAccounts($jEAcc);

                    if (!$saveJEAccount) {
                        return $this->returnError('ERR_JE_ACC_SAVE');
                    }                        
                }

            }
        }
        
        // handle customer advanced payment account
        if ($accountDetails['currencyLoss'] > 0 ) {
            $supplerAdvanced = $this->updateSupplierAdvancedPaymentAcc($journaEntryID, $accountDetails['currencyLoss'], $accountDetails['supplierAccDetails']->supplierAdvancePaymentAccountID);

            return $supplerAdvanced;
        }
        
        return $this->returnSuccess();

    }

    public function updateSupplierAdvancedPaymentAcc($journalEntryID, $lossValue, $supplierAdvancePayAcc)
    {
        // get advanced payment related jE details
        $jEDetails = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryIDAndAccountID($journalEntryID, $supplierAdvancePayAcc)->current();
        $remAccountDebitAmo = floatval($jEDetails['financeAccountsDebitAmount']) - floatval($lossValue);
        // update finance account
        $updatedDetails = [
            'financeAccountsDebitAmount' => $remAccountDebitAmo,
        ];
        
        if (floatval($jEDetails['journalEntryAccountsDebitAmount']) == floatval($lossValue)) {
                            
            // update finance account
            $updateAcc = $this->updateFinanceAndJETables($updatedDetails, [], $jEDetails['financeAccountsID'], null);

            if(!$updateAcc['status']) {
                return $updateAcc; 
            }
            // delete jE
            $delete = $this->getModel('JournalEntryAccountsTable')->deleteJournalEntryAccoutsByID($jEDetails['journalEntryAccountsID']);
            
            if (!$delete) {
                return $this->returnError('ERR_DELETE_JE');
            }
            
        } else if (floatval($jEDetails['journalEntryAccountsDebitAmount']) > floatval($lossValue)) {
            $remJEDebitAmo = floatval($jEDetails['journalEntryAccountsDebitAmount']) - floatval($lossValue);

            // update journal entry data
            $jEUpdateData = [
                'journalEntryAccountsDebitAmount' => $remJEDebitAmo 
            ];

            // update finance and jE accounts
            $updateAcc = $this->updateFinanceAndJETables($updatedDetails, $jEUpdateData, $jEDetails['financeAccountsID'], $jEDetails['journalEntryAccountsID']);

            if(!$updateAcc['status']) {
                return $updateAcc; 
            }
        }

        return $this->returnSuccess();
    }

    /**
    * this function use to update finance and journal entry accounts
    * @param array $updatedAccDetails
    * @param array $updatedJEDetails
    * @param int $financeAccID
    * @param int $jEAccID
    * return array
    **/
    public function updateFinanceAndJETables($updatedAccDetails = [], $updatedJEDetails = [], $financeAccID = null, $jEAccID = null)
    {
        // update finance Account
        if ($financeAccID != null && $updatedAccDetails != null) {
            $updateFinanceAcc = $this->getModel('FinanceAccountsTable')->updateFinanceAccounts($updatedAccDetails, $financeAccID);
            
            if (!$updateFinanceAcc) {
                return $this->returnError('ERR_FINANCE_ACC_UPDATE');
            }
            
        }
        // update journal Entry Account
        if ($jEAccID != null && $updatedJEDetails != null) {
            $updateJEAcc = $this->getModel('JournalEntryAccountsTable')->updateJournalEntryAccounts($updatedJEDetails, $jEAccID);
            
            if(!$updateJEAcc) {
                return $this->returnError('ERR_JE_ACC_UPDATE');
            }
        }

        return $this->returnSuccess();
    }


    /**
    * update jEAccout detials
    * @param int $journalEntryAccountID
    * @param float $existingValue
    * @param float $newValue
    * @param string $type
    * @param int $accountID
    * return mix
    **/
    public function updateJERecords($journalEntryAccountID,$existingValue,$newValue,$type, $accountID)
    {
        // get account values
        $financeAccDetails = $this->getModel('FinanceAccountsTable')->getAccountsDataByAccountsID($accountID);
        
        if ($type == 'debit') {
            $newDebitValueForJournalEntry = floatval($existingValue) + floatval($newValue);
            $newDebitValueForFinanceAcc = floatval($newValue) + floatval($financeAccDetails['financeAccountsDebitAmount']);

            // update finance account
            $updatedDetails = [
                'financeAccountsDebitAmount' => $newDebitValueForFinanceAcc,
            ];

            // update journal entry data
            $jEUpdateData = [
                'journalEntryAccountsDebitAmount' => $newDebitValueForJournalEntry 
            ];
            
        } else if ($type == 'credit') {

            $newCreditValueForJournalEntry = floatval($existingValue) + floatval($newValue);
            $newCreditValueForFinanceAcc = floatval($newValue) + floatval($financeAccDetails['financeAccountsCreditAmount']);

            // update finance account
            $updatedDetails = [
                'financeAccountsCreditAmount' => $newCreditValueForFinanceAcc,
            ];

            // update journal entry data
            $jEUpdateData = [
                'journalEntryAccountsCreditAmount' => $newCreditValueForJournalEntry 
            ];

        }
       
        // update
        $updateAcc = $this->updateFinanceAndJETables($updatedDetails, $jEUpdateData, $accountID, $journalEntryAccountID);

        return $updateAcc; 
        
    }

    /** 
    * this function use to handle payment delete currency gain and loss 
    * @param int $paymentID
    * return array
    **/
    public function handleCurrencyGainAndLossForDeletePayment($paymentID)
    {
        $suppleirAddingOutstanding = 0;
        $supplierAddingCredits = 0;
        $currentPaymentInvoiceID = null;
        $supplierID = null;
        
        // get payment details
        $paymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentWithInvoiceDetailByPaymentID($paymentID);
        foreach ($paymentDetails as $key => $singlePayValue) {
            // first need to check this has gain or loss
            if ($singlePayValue['outgoingPaymentInvoiceCurrencyGainAndLossType'] != null && $singlePayValue['outgoingPaymentInvoiceGainAndLossAmount'] != null) {

                $currentPaymentInvoiceID = $singlePayValue['outgoingPaymentInvoiceID'];
                $supplierID = $singlePayValue['supplierID'];
                
                // check purchase invoice or payment voucher
                if ($singlePayValue['invoiceID'] != 0) {
                    
                    // that's mean purchase invoice
                    $documentID = $singlePayValue['invoiceID'];
                    $documentType = 'purchaseInvoice';

                } else if ($singlePayValue['paymentVoucherId'] != 0) {
                
                    // payment voucher implementation
                    $documentType = 'paymentVoucher';
                    $documentID = $singlePayValue['paymentVoucherId'];
                }
                
                // get purchase invoice/payment voucher remainig amounts
                $purchaseInvoiceRemainingDetails = $this->purchaseInvoiceUpdateDataReset($documentID, $documentType, $currentPaymentInvoiceID);
                $documentUpdate['status'] = true;
                
                if ($purchaseInvoiceRemainingDetails['totalInvoiceAmount'] == 0) {
                    // that's mean this invoice/ payment voucher has only one payment and its already deleted.
                    // then need to get payment voucher/ purchase invoice details manually
                    $documentDetails = $this->getPurchaseInvoiceAndPaymentVoucherDetails($documentType, $documentID);
                    if ($singlePayValue['outgoingPaymentInvoiceCurrencyGainAndLossType'] == 'gain') {
                        $calRemPaidAmo = floatval($documentDetails['currentlyPaidAmount']) - floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']);
                        // update document remainig amount and status
                        $documentUpdate = $this->updatePurchaseInvoiceAndPaymentVoucherRecords($documentType, $calRemPaidAmo, $documentID);
                    }               
                } else {
                    // update document remainig amount and status
                    $documentUpdate = $this->updatePurchaseInvoiceAndPaymentVoucherRecords($documentType, $purchaseInvoiceRemainingDetails['totalInvoiceAmount'] - $purchaseInvoiceRemainingDetails['hasToPayAmount'], $documentID);

                }

                if (!$documentUpdate['status']) {
                    return $documentUpdate;
                }

                // set supplier values
                if ($singlePayValue['outgoingPaymentInvoiceCurrencyGainAndLossType'] == 'loss' && $singlePayValue['supplierID'] != null) {
                    $supplierAddingCredits += floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']);
                
                } else if ($singlePayValue['outgoingPaymentInvoiceCurrencyGainAndLossType'] == 'gain' && $singlePayValue['supplierID'] != null) {
                    $suppleirAddingOutstanding += floatval($singlePayValue['outgoingPaymentInvoiceGainAndLossAmount']);
                }
            }
        } 
        
        if ($supplierID != null) {
            // updadte supplier
            $updateSupplier = $this->resetAndUpdateSupplierByPaymentDelete($supplierID, $supplierAddingCredits, $suppleirAddingOutstanding);
            if (!$updateSupplier) {
                return $this->returnError('ERR_SUPPL_UPDATE');
            }
        }

        return $this->returnSuccess();

    }

    /**
    * this function use to get purchase invoice and payment voucher details for given ids
    * @param string $documentType
    * @param int $documentID
    * return array
    **/
    public function getPurchaseInvoiceAndPaymentVoucherDetails($documentType, $documentID)
    {
        if ($documentType == 'purchaseInvoice') {
            // get purchase Invoice detials
            $details = $this->getModel('PurchaseInvoiceTable')->getPurchaseVoucherByID($documentID);
            $documentTotalAmount = $details['purchaseInvoiceTotal'];
            $documentCurrentlyPaidAmount = $details['purchaseInvoicePayedAmount'];


        } else if ($documentType == 'paymentVoucher') {
            
            // get payment voucher details
            $pVDetails = $this->getModel('PaymentVoucherTable')->getPaymentVoucherByPaymentVoucherID($documentID);
            $documentTotalAmount = $pVDetails->paymentVoucherTotal;
            $documentCurrentlyPaidAmount = $pVDetails->paymentVoucherPaidAmount;
        }
        return [
            'totalInvoiceAmount' => $documentTotalAmount,
            'currentlyPaidAmount' => $documentCurrentlyPaidAmount,
        ];
        
    }

    /**
    * this function use to update purchase invoice and payment voucher tables
    * @param String $documentType
    * @param float $totalInvoiceAmount
    * @param int $documentID
    * return array
    **/
    public function updatePurchaseInvoiceAndPaymentVoucherRecords($documentType, $totalInvoiceAmount, $documentID)
    {
        
         if ($documentType == 'purchaseInvoice') 
            {
                // set data for update
                $purchaseInvoiceUpdatedDataSet = [
                    'purchaseInvoicePayedAmount' => floatval($totalInvoiceAmount),
                    'status' => '3'
                ];
                // update purchase Invoice table
                $updatePurchaseInvoice = $this->getModel('PurchaseInvoiceTable')->updatePiDetails($purchaseInvoiceUpdatedDataSet, $documentID);

                if (!$updatePurchaseInvoice) {
                    return $this->returnError('ERR_UPDATE_PI');
                }

            } else {
                // set data for update
                $paymentVoucherUpdatedDataSet = [
                    'paymentVoucherPaidAmount' => floatval($totalInvoiceAmount),
                    'paymentVoucherStatus' => '3',
                    'paymentVoucherID' => $documentID, 
                ];
                $updatePV = new PaymentVoucher();
                $updatePV->exchangeArray($paymentVoucherUpdatedDataSet);
                // update payment voucher table
                $updatePaymentVoucher = $this->getModel('PaymentVoucherTable')->updatePaymentOfPaymentVoucher($updatePV);

                if (!$updatePaymentVoucher) {
                    return $this->returnError('ERR_UPDATE_PV');
                }                
            }

            return $this->returnSuccess();
    }
    
    /**
    * this function use to update supplier outstanding value and supplier credit values
    * @param $supplierID
    * @param $creditValue
    * @param $outstandingValue
    * return boolean
    **/
    public function resetAndUpdateSupplierByPaymentDelete($supplierID, $creditValue, $outstandingValue)
    {

         // get supplier details by given supplier ID
        $supplierDetails = $this->getModel('SupplierTable')->getSupplierFromID($supplierID);

        $supplierUpdateData = [
            'supplierCreditBalance' => floatval($supplierDetails->supplierCreditBalance) + floatval($creditValue),
            'supplierOutstandingBalance' => floatval($supplierDetails->supplierOutstandingBalance) + floatval($outstandingValue) 
        ];

        $updateSupplier = $this->getModel('SupplierTable')->updateSupplier($supplierUpdateData, $supplierID);
        return $updateSupplier; 
    }

}