<?php

namespace Inventory\Form;

use Zend\Form\Form;

class DebitNoteForm extends Form
{

    public function __construct()
    {

        $name = 'returnForm';
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $name);

        $this->add(array(
            'name' => 'debitNoteNo',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'debitNoteNo',
            ),
        ));

        $this->add(array(
            'name' => 'supplier',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'supplier',
            ),
        ));

        $this->add(array(
            'name' => 'currentLocation',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'currentLocation',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'debitNoteNoDate',
            'type' => 'date',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'debitNoteNoDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));

        $this->add(array(
            'name' => 'itemCode',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemCode',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'itemName',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemName',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'itemDescription',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemDescription'
            ),
        ));

        $this->add(array(
            'name' => 'invoicedQuantity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'invoicedQuantity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'debitNoteQuanity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'debitNoteQuanity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'unitPrice',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'unitPrice',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'discount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'discount',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'deliveryCharge',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'deliveryCharge',
            ),
        ));

        $this->add(array(
            'name' => 'deliveryChargeEnable',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'deliveryChargeEnable',
            ),
        ));

        $this->add(array(
            'name' => 'showTax',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'showTax',
            ),
        ));

        $this->add(array(
            'name' => 'comment',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'comment',
            ),
        ));

        $this->add(array(
            'name' => 'Address',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'Address',
            ),
        ));

        $this->add(array(
            'name' => 'debitNoteSaveButton',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'debitNoteSaveButton',
                'class' => 'btn btn-primary'
            ),
        ));
        $this->add(array(
            'name' => 'debitNoteCancelButton',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Cancel'),
                'id' => 'debitNoteCancelButton',
                'class' => 'btn btn-default'
            ),
        ));
        $this->add(array(
            'name' => 'availableQuantity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'availableQuantity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'deliverQuanity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'deliverQuanity',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'debitDiscount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'debitDiscount',
                'autocomplete' => 'off'
            ),
        ));
    }

}

?>
