<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains products related Form fields
 */

namespace Inventory\Form;

use Zend\Form\Form;

class ProductCategoryForm extends Form
{

    public function __construct($data = NULL)
    {
        parent::__construct('productCategory');
        $this->setAttribute('method', 'post');
        $this->setAttribute('onsubmit', 'validate_form();');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'categoryID',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'categoryName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'categoryName',
                'class' => 'form-control',
                'style' => '',
                'placeholder' => "eg : HP"
            ),
        ));
        $this->add(array(
            'name' => 'categoryParentID',
            'type' => 'Hidden',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'categoryParentID',
                'class' => 'form-control',
                'style' => '',
                'placeholder' => "eg : HP"
            ),
        ));
        $this->add(array(
            'name' => 'categoryLevel',
            'type' => 'Hidden',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'categoryLevel',
                'class' => 'form-control',
                'style' => '',
                'placeholder' => "eg : HP"
            ),
        ));
//        $this->add(array(
//            'name' => 'autoGenarate',
//            'type' => 'Zend\Form\Element\Checkbox',
//            'options' => array(
//                'use_hidden_element' => true,
//                'checked_value' => '1',
//                'unchecked_value' => '0'
//            ),
//            'attributes' => array(
//                'id' => 'autoGenarate',
//            ),
//        ));
        $this->add(array(
            'name' => 'categoryPrefix',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'categoryPrefix',
                'class' => 'form-control',
                'style' => '',
                'placeholder' => "eg : probook"
            ),
        ));
        $this->add(array(
            'name' => 'categoryState',
            'type' => 'Hidden',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'categoryState',
                'class' => 'form-control',
                'style' => '',
                'placeholder' => "eg : probook"
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Add Category',
                'id' => 'product-category-button',
                'class' => 'btn btn-primary',
            ),
        ));
    }

}

