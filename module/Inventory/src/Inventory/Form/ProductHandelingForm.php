<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * This file contains products handeling related Form fields
 */

namespace Inventory\Form;

use Zend\Form\Form;

class ProductHandelingForm extends Form
{

    public function __construct()
    {
        parent::__construct('producthandeling');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'productHandelingManufactureProduct',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'productHandelingManufactureProduct',
                'class' => 'handeling'
            ),
        ));

        $this->add(array(
            'name' => 'productHandelingPurchaseProduct',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'productHandelingPurchaseProduct',
                'class' => 'handeling'
            ),
        ));

        $this->add(array(
            'name' => 'productHandelingSalesProduct',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'productHandelingSalesProduct',
                'class' => 'handeling'
            ),
        ));

        $this->add(array(
            'name' => 'productHandelingConsumables',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'productHandelingConsumables',
                'class' => 'handeling'
            ),
        ));

        $this->add(array(
            'name' => 'productHandelingFixedAssets',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'productHandelingFixedAssets',
                'class' => 'handeling'
            ),
        ));
    }

    public function populate($data)
    {
        foreach ($data as $field => $value) {
            if ($this->has($field)) {
                $this->get($field)->setValue($value);
            }
        }

        return $this;
    }

}
