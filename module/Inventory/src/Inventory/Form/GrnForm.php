<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is Grn Form
 */

namespace Inventory\Form;

use Zend\Form\Form;

class GrnForm extends Form
{

    public function __construct($data = NULL)
    {

        $name = 'grnForm';
        $terms = isset($data['paymentTerms']) ? $data['paymentTerms'] : array();
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $name);

        $this->add(array(
            'name' => 'grnNo',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'grnNo',
                'disabled' => 'disabled'
            ),
        ));

        $this->add(array(
            'name' => 'supplier',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'supplier',
            ),
        ));

        $this->add(array(
            'name' => 'supplierReference',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'supplierReference',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'retrieveLocation',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'retrieveLocation',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'deliveryDate',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'deliveryDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));

        $this->add(array(
            'name' => 'purchaseOrderDate',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'purchaseOrderDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));

        $this->add(array(
            'name' => 'itemCode',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemCode',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'itemName',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemName',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'itemDescription',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemDescription'
            ),
        ));

        $this->add(array(
            'name' => 'qty',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'qty',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'unitPrice',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'unitPrice',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'discount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'discount',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'deliveryCharge',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'deliveryCharge',
                'maxlength' => 10,
            ),
        ));

        $this->add(array(
            'name' => 'deliveryChargeEnable',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'deliveryChargeEnable',
            ),
        ));

        $this->add(array(
            'name' => 'showTax',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'showTax',
            ),
        ));

        $this->add(array(
            'name' => 'comment',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'comment',
            ),
        ));
        $this->add(array(
            'name' => 'grnSaveButton',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'grnSave',
                'class' => 'btn btn-primary'
            ),
        ));
        $this->add(array(
            'name' => 'grnCancelButton',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Cancel'),
                'id' => 'grnCancel',
                'class' => 'btn btn-default'
            ),
        ));
        $this->add(array(
            'name' => 'grnEditButton',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Edit'),
                'id' => 'grnEdit',
                'class' => 'btn btn-primary hidden'
            ),
        ));
        $this->add(array(
            'name' => 'paymentTerm',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'paymentTerm'
            ),
            'options' => array(
                'value_options' => $terms,
            )
        ));
        $this->add(array(
            'name' => 'nonItemDescription',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'nonItemDescription',
                'placeholder' => 'Non item Description',
            ),
        ));
        $this->add(array(
            'name' => 'expenseType',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'expenseType',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'poDraft',
            'type' => 'hidden',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'poDraft',
            ),
        ));

        $this->add(array(
            'name' => 'availableQuantity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'availableQuantity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'deliverQuanity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'deliverQuanity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'total_discount_rate',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding piTotalDiscountRate',
                'id' => 'total_discount_rate',
                'autocomplete' => 'off'
            ),
        ));
    }

}

?>
