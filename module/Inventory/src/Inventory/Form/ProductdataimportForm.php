<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * this file contains Product import form
 */

namespace Inventory\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class ProductdataimportForm extends Form
{

    public function __construct($name = null)
    {
        parent::__construct('Productdataimport');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'productfileupload',
            'attributes' => array(
                'type' => 'file',
                'value' => 'browse',
                'id' => 'productfileupload',
            ),
        ));
        $this->add(array(
            'name' => 'productcharacterencoding',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'productcharacterencoding'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'UTF-8',
                    '1' => 'ISO 8859-1'
                ),
            )
        ));
        $this->add(array(
            'name' => 'productdelimiter',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'productdelimiter'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => ', (Comma)',
                    '1' => '; (semi-colon)',
                    '2' => '. (full stop)',
                ),
            )
        ));
        $this->add(array(
            'name' => 'productheader',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => TRUE,
                'checked_value' => '1',
                'unchecked_value' => '0',
            ),
            'attributes' => array(
                'checked' => TRUE,
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Next',
                'id' => 'productuploadfile',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}

