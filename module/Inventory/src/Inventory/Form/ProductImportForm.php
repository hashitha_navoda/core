<?php
namespace Inventory\Form;

use Zend\Form\Form;
use Zend\I18n\Translator\Translator;

class ProductImportForm extends Form
{
    private $translator;

    public function __construct($name = null)
    {
        parent::__construct('ProductImport');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->translator = new Translator();
        
        $this->addElements();
        $this->addInputFilter();
    }
    
    private function addElements() {
        
        $this->add(array(
            'name' => 'productFile',
            'attributes' => array(
                'type' => 'file',
                'value' => 'browse',
                'id' => 'productFile',
            ),
        ));
        
        $this->add(array(
            'name' => 'productFileType',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control input-sm',
                'id' => 'productFileType'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'csv'
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'productCharacterEncoding',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control input-sm',
                'id' => 'productCharacterEncoding'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'UTF-8',
                    '1' => 'ISO 8859-1'
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'productDelimiter',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control input-sm',
                'id' => 'productDelimiter'
            ),
            'options' => array(
                'value_options' => array(
                    ',' => ', (comma)',
                    ';' => '; (semi-colon)'
                ),
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'productUniqueField',            
            'attributes' => array(
                'class' => 'form-control input-sm',
                'id' => 'productUniqueField',
                'disabled' => true,
            ),
            'options' => array(                
                'options' => array(
                    '0' => 'Item Code'
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'productHandleType',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control input-sm',
                'id' => 'productHandleType'
            ),
            'options' => array(
                'value_options' => array(
                    'skip' => $this->translator->translate('Skip'),
                    'merge' => $this->translator->translate('Merge')
                ),
            )
        ));
        
        
        $this->add(array(
            'name' => 'productHeader',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => TRUE,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'productHeader',
                'checked' => TRUE
            ),
        ));
        
        $this->add(array(
            'name' => 'productUploadBtn',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Next',
                'id' => 'productUploadBtn',
                'class' => 'btn btn-default'
            ),
        ));
        
        $this->add(array(
            'name' => 'productUploadCancelBtn',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'productUploadCancelBtn',
                'class' => 'btn btn-default'
            ),
        ));
    }
    
    private function addInputFilter() {
        $inputFilter = new \Zend\InputFilter\InputFilter();
        
        $factory = new \Zend\InputFilter\Factory();

        $inputFilter->add($factory->createInput(array(
            'name' => 'productUniqueField',
            'required' => false
        )));

        // File Input
        $fileInput = new \Zend\InputFilter\FileInput('productFile');
        $fileInput->setRequired(true);
        $fileInput->getValidatorChain()
                ->attach(new \Zend\Validator\File\Extension('csv'))
                ->attach(new \Zend\Validator\File\Size([
                    'max'=>  min(
                        $this->convertToBytes(ini_get('post_max_size')), 
                        $this->convertToBytes(ini_get('upload_max_filesize')),
                        $this->convertToBytes(ini_get('memory_limit'))
                    )]));        
        $inputFilter->add($fileInput);        
        
        $this->setInputFilter($inputFilter);
    }
    
    private function convertToBytes( $value) {
        if ( is_numeric( $value ) ) {
            return $value;
        } else {
            $value_length = strlen( $value );
            $qty = substr( $value, 0, $value_length - 1 );
            $unit = strtolower( substr( $value, $value_length - 1 ) );
            switch ( $unit ) {
                case 'k':
                    $qty *= 1024;
                    break;
                case 'm':
                    $qty *= 1048576;
                    break;
                case 'g':
                    $qty *= 1073741824;
                    break;
            }
            return $qty;
        }
    }
    
    

}

