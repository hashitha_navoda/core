<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains products related Form fields
 */

namespace Inventory\Form;

use Zend\Form\Form;

class ProductForm extends Form
{

    public function __construct($data = NULL)
    {
        parent::__construct('product');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'productID',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'productCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productCode',
                'class' => 'form-control',
                'placeholder' => _('Eg: LP10'),
                'value' => $data['referenceNo']
            ),
        ));
        $this->add(array(
            'name' => 'productBarcode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productBarcode',
                'class' => 'form-control',
                'placeholder' => _('Enter barcode')
            ),
        ));
        $this->add(array(
            'name' => 'productName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productName',
                'class' => 'form-control',
                'placeholder' => _('Eg: Laptop')
            ),
        ));
        $this->add(array(
            'name' => 'productDescription',
            'type' => 'textarea',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDescription',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'categoryID',
            'type' => 'select',
            'options' => array(
                'value_options' => array(),
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'id' => 'categoryID',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'productTypeID',
            'type' => 'select',
            'options' => array(
                'disable_inarray_validator' => true,
                'value_options' => $data['productType'],
            ),
            'attributes' => array(
                'id' => 'productTypeID',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'productDiscountEligible',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'productDiscountEligible',
            ),
        ));
        $this->add(array(
            'name' => 'discountSchemeEligible',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'discountSchemeEligible',
            ),
        ));
        $this->add(array(
            'name' => 'discountSchemeID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'discountSchemeID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an discount scheme',
            )
        ));
        $this->add(array(
            'name' => 'productDiscountPercentage',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDiscountPercentage',
                'class' => 'form-control',
                'placeholder' => _('eg : 10')
            ),
        ));
        $this->add(array(
            'name' => 'productDiscountValue',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDiscountValue',
                'class' => 'form-control',
                'placeholder' => _('eg : 100')
            ),
        ));
        $this->add(array(
            'name' => 'productPurchaseDiscountEligible',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'productPurchaseDiscountEligible',
            ),
        ));
        $this->add(array(
            'name' => 'productPurchaseDiscountPercentage',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productPurchaseDiscountPercentage',
                'class' => 'form-control',
                'placeholder' => _('eg : 10')
            ),
        ));
        $this->add(array(
            'name' => 'productPurchaseDiscountValue',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productPurchaseDiscountValue',
                'class' => 'form-control',
                'placeholder' => _('eg : 100')
            ),
        ));
        
        $this->add(array(
            'name' => 'rackId',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'rackId',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'itemDetail',
            'type' => 'textarea',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'itemDetail',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'productState',
            'type' => 'select',
            'options' => array(
                'value_options' => array(
                    1 => "Active",
                    0 => "Inactive",
                ),
            ),
            'attributes' => array(
                'id' => 'productState',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'productTaxEligible',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'productTaxEligible',
            ),
        ));

        if (isset($data['tax'])) {
            foreach ($data['tax'] as $id => $name) {
                $this->add(array(
                    'name' => 'tax' . $id,
                    'type' => 'Zend\Form\Element\Checkbox',
                    'options' => array(
                        'use_hidden_element' => true,
                        'checked_value' => $id,
                        'unchecked_value' => 'bad'
                    ),
                    'attributes' => array(
                        'id' => 'tax' . $id,
                        'data-taxid' => $id,
                        'class' => 'taxes',
                    ),
                ));
            }
        }

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'productGlobalProduct',
            'options' => array(
                'value_options' => array(
                    '0' => array(
                        'label' => 'Global Item',
                        'value' => 0,
                        'attributes' => array(
                        )
                    ),
                    '1' => array(
                        'label' => 'Global Item',
                        'value' => 1,
                        'attributes' => array(
                        )
                    )
                )
            )
        ));

        if (isset($data['location'])) {
            foreach ($data['location'] as $id => $name) {
                $this->add(array(
                    'name' => 'location' . $id,
                    'type' => 'Zend\Form\Element\Checkbox',
                    'options' => array(
                        'use_hidden_element' => true,
                        'checked_value' => $id,
                        'unchecked_value' => 'bad'
                    ),
                    'attributes' => array(
                        'id' => 'locationSel' . $id,
                        'class' => 'locationCheck',
                        'data-locationid' => $id,
                    ),
                ));
            }
        }

        $this->add(array(
            'name' => 'potentialSuppliers',
            'type' => 'select',
            'options' => array(
                'empty_option' => '',
                'value_options' => $data['supplier'],
                'title' => _('Select supplier'),
            ),
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'potentialSuppliers',
            ),
        ));

        $this->add(array(
            'name' => 'batchProduct',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'disable_inarray_validator' => true,
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'batchProduct',
            ),
        ));
        $this->add(array(
            'name' => 'serialProduct',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'disable_inarray_validator' => true,
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
            'attributes' => array(
                'id' => 'serialProduct',
            ),
        ));
        $this->add(array(
            'name' => 'hsCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'hsCode',
                'class' => 'form-control',
                'placeholder' => _('Eg: 0104.10.90')
            ),
        ));
        $this->add(array(
            'name' => 'customDutyPercentage',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'customDutyPercentage',
                'class' => 'form-control',
                'placeholder' => _('Eg: 10')
            ),
        ));
        $this->add(array(
            'name' => 'customDutyValue',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'customDutyValue',
                'class' => 'form-control',
                'placeholder' => _('Eg: 100')
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'AddProduct',
                'id' => 'add-product-button',
                'class' => 'btn btn-primary',
//                'data-dismiss' => "modal",
            ),
        ));
        $this->add(array(
            'name' => 'productDefaultOpeningQuantity',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDefaultOpeningQuantity',
                'class' => 'form-control',
                'placeholder' => _('eg : 100')
            ),
        ));
        $this->add(array(
            'name' => 'productDefaultSellingPrice',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDefaultSellingPrice',
                'class' => 'form-control productDefaultSellingPrice',
                'placeholder' => _('eg : 100')
            ),
        ));
        $this->add(array(
            'name' => 'productDefaultReorderLevel',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDefaultReorderLevel',
                'class' => 'form-control',
                'placeholder' => _('eg : 100')
            ),
        ));
        $this->add(array(
            'name' => 'productDefaultMinimumInventoryLevel',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDefaultMinimumInventoryLevel',
                'class' => 'form-control',
                'placeholder' => _('eg : 100')
            ),
        ));
                $this->add(array(
            'name' => 'productDefaultMaximunInventoryLevel',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDefaultMaximunInventoryLevel',
                'class' => 'form-control',
                'placeholder' => _('eg : 100')
            ),
        ));
        $this->add(array(
            'name' => 'productDefaultPurchasePrice',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDefaultPurchasePrice',
                'class' => 'form-control productDefaultPurchasePrice',
                'placeholder' => _('eg : 100')
            ),
        ));
        $this->add(array(
            'name' => 'productSalesAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'productSalesAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));
        $this->add(array(
            'name' => 'productInventoryAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'productInventoryAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));
        $this->add(array(
            'name' => 'productCOGSAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'productCOGSAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));
        $this->add(array(
            'name' => 'productAdjusmentAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'productAdjusmentAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));
        $this->add(array(
            'name' => 'productDeperciationAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'productDeperciationAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));
        
    }

    public function populate($data)
    {
        foreach ($data as $field => $value) {
            if ($this->has($field)) {
                $this->get($field)->setValue($value);
            }
        }

        return $this;
    }

}
