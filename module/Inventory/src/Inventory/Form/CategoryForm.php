<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Inventory\Form;

use Zend\Form\Form;

class CategoryForm extends Form
{

    public function __construct($categories = NULL)
    {
        parent::__construct('category');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'categoryID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'categoryName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'categoryName',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: Car')
            ),
        ));

        $this->add(array(
            'name' => 'categoryParentID',
            'type' => 'select',
            'options' => array(
                'disable_inarray_validator' => true,
                'empty_option' => 'Select a Category',
                'value_options' => $categories
            ),
            'attributes' => array(
                'id' => 'categoryParentID',
                'class' => 'form-control',
                'data-live-search' => 'true',
            ),
        ));

        $this->add(array(
            'name' => 'categoryParentIDHelper',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'categoryParentIDHelper',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: Automobile')
            ),
        ));

        $this->add(array(
            'name' => 'categoryPrefix',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'categoryPrefix',
                'class' => 'form-control',
                'placeholder' => _('Eg: AU')
            ),
        ));

        $this->add(array(
            'name' => 'categoryState',
            'type' => 'select',
            'options' => array(
                'value_options' => array(
                    1 => 'Active',
                    0 => 'Inactive'
                )
            ),
            'attributes' => array(
                'id' => 'categoryState',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Add Category',
                'id' => 'add-uom-button',
                'class' => 'btn btn-primary',
                'data-dismiss' => "modal",
            ),
        ));
    }

    public function populate($data)
    {
        foreach ($data as $field => $value) {
            if ($this->has($field)) {
                $this->get($field)->setValue($value);
            }
        }

        return $this;
    }

}
