<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Inventory\Form;

use Zend\Form\Form;

class TransferForm extends Form
{

    public function __construct()
    {
        parent::__construct('transfer');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'transferCode',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'id' => 'transferCode',
                'class' => 'form-control',
                'placeholder' => _('Eg: 1000')
            ),
        ));

        $this->add(array(
            'name' => 'locationOut',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'id' => 'locationOut',
                'class' => 'form-control',
                'placeholder' => _('Location Code or Name'),
                'autocomplete' => false
            ),
        ));

        $this->add(array(
            'name' => 'locationIn',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'id' => 'locationIn',
                'class' => 'form-control',
                'placeholder' => _('Location Code or Name'),
                'autocomplete' => false
            ),
        ));
        $this->add(array(
            'name' => 'date',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'date',
                'class' => 'form-control transparent pointer_cursor',
                'placeholder' =>_('yyyy-mm-dd'),
                'readonly' => true
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'type' => 'TextArea',
            'options' => array(),
            'attributes' => array(
                'id' => 'description',
                'class' => 'form-control',
                'rows' => '3',
                'placeholder' => _('Reason for transferring')
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Save Transfer',
                'id' => 'save-transfer',
                'class' => 'btn btn-primary',
            ),
        ));
    }

}
