<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * this file contains supplier Form Elements
 */

namespace Inventory\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class SupplierForm extends Form
{

    public function __construct($data)
    {
        parent::__construct('Supplier');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'supplierID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'supplierID'
            ),
        ));

        $this->add(array(
            'name' => 'supplierTitle',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'supplierTitle'
            ),
            'options' => array(
                'value_options' => isset($data['supplierTitle']) ? $data['supplierTitle'] : NULL,
            ),
        ));

        $this->add(array(
            'name' => 'supplierName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('John Smith'),
                'id' => 'supplierName',
            ),
        ));
        $this->add(array(
            'name' => 'supplierCode',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('jsmith123'),
                'id' => 'supplierCode',
            ),
        ));

        $this->add(array(
            'name' => 'supplierCategory[]',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'supplierCategory',
                'data-selected-text-format' => "count>2",
                'multiple' => 'multiple',
                'data-live-search' => "true",
                'data-size' => '5',
                'title' => ' ',
            ),
            'options' => array(
//                'empty_option' => ' ',
                'value_options' => isset($data['supplierCategory']) ? $data['supplierCategory'] : NULL,
            ),
        ));

        $this->add(array(
            'name' => 'supplierCurrency',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'supplierCurrency',
                'value' => $data['currencyCountry'],
            ),
            'options' => array(
                'value_options' => isset($data['currencies']) ? $data['currencies'] : NULL,
            ),
        ));


        $this->add(array(
            'name' => 'supplierTelephoneNumber',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'supplierTelephoneNumber',
//                'placeholder' => _('0723000000')
            ),
        ));
        $this->add(array(
            'name' => 'supplierEmail',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('jsmith@example.com'),
                'id' => 'supplierEmail'
            ),
        ));
        $this->add(array(
            'name' => 'supplierAddress',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control resize_none',
                'placeholder' => _('Enter current Address'),
                'id' => 'supplierAddress',
                'rows' => '4'
            ),
        ));
        $this->add(array(
            'name' => 'supplierVatNumber',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'supplierVatNumber',
                'placeholder' => _('10')
            ),
        ));
        $this->add(array(
            'name' => 'supplierPaymentTerm',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'supplierPaymentTerm'
            ),
            'options' => array(
                'value_options' => isset($data['paymentTerms']) ? $data['paymentTerms'] : NULL,
            )
        ));
        $this->add(array(
            'name' => 'supplierCreditLimit',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'supplierCreditLimit',
                'placeholder' => _('1000')
            ),
        ));
        $this->add(array(
            'name' => 'supplierOutstandingBalance',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control customerAdd',
                'id' => 'supplierOutstandingBalance',
                'placeholder' => _('1000'),
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'supplierCreditBalance',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control customerAdd',
                'id' => 'supplierCreditBalance',
                'placeholder' => _('1000'),
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'supplierOther',
            'type' => '\Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control resize_none',
                'placeholder' => _('Enter Further Details'),
                'id' => 'supplierOther',
                'rows' => '4'
            ),
        ));

         $this->add(array(
            'name' => 'supplierPayableAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'supplierPayableAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));

        $this->add(array(
            'name' => 'supplierPurchaseDiscountAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'supplierPurchaseDiscountAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));

        $this->add(array(
            'name' => 'supplierGrnClearingAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'supplierGrnClearingAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));

        $this->add(array(
            'name' => 'supplierAdvancePaymentAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'supplierAdvancePaymentAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));

        $this->add(array(
            'name' => 'supplierFormButton',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Add Supplier'),
                'id' => 'supplierSave',
                'class' => 'btn btn-primary add_suppliar_button'
            ),
        ));

        $this->add(array(
            'name' => 'supplierFormCancel',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Reset'),
                'id' => 'clearbutton',
                'class' => 'btn btn-default clear_button'
            ),
        ));
    }

}
