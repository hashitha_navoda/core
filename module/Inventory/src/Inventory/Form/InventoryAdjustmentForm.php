<?php

/**
 * @author Sandun<sandun@thinkcube.com>
 * This file contains Inventory Adjustment Form. This is the same file use for the Quotaion Form
 */

namespace Inventory\Form;

use Zend\Form\Form;

class InventoryAdjustmentForm extends Form
{

    public function __construct($data = NULL)
    {

        $name = isset($data['name']) ? $data['name'] : 'adjustment';
        $id = isset($data['id']) ? $data['id'] : 'inv_no';
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $name);
        $this->setAttribute("role", "form");
        $this->add(array(
            'name' => 'inve_no',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'inve_no',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => $id,
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => $id,
            ),
        ));

        $this->add(array(
            'name' => 'location_name',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'location_name'
            ),
        ));
        $this->add(array(
            'name' => 'adjustment_type',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'adjustment_type'
            ),
        ));
        $this->add(array(
            'name' => 'reason',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'reason'
            ),
        ));

        $this->add(array(
            'name' => 'issue_date',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'issue_date',
            ),
        ));

        $this->add(array(
            'name' => 'itemCode',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemCode',
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'item',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'item'
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'availableQuantity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'availableQuantity',
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'qty',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'qty',
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'unitPrice',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'unitPrice'
            //'required' => 'true',
            ),
        ));

        $this->add(array(
            'name' => 'cmnt',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'cmnt',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}

?>
