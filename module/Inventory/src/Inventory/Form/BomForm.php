<?php

namespace Inventory\Form;

use Zend\Form\Form;

class BomForm extends Form
{

    public function __construct($data = NULL)
    {
        parent::__construct('bom');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'BOMID',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'BomRef',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'BomRef',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'BOMName',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'BOMName',
                'class' => 'form-control',
            ),
        ));
        
        
        $this->add(array(
            'name' => 'productDescription',
            'type' => 'textarea',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'productDescription',
                'class' => 'form-control',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'productGlobalProduct',
            'options' => array(
                'value_options' => array(
                    '0' => array(
                        'label' => 'Global Item',
                        'value' => 0,
                        'attributes' => array(
                        )
                    ),
                    '1' => array(
                        'label' => 'Global Item',
                        'value' => 1,
                        'attributes' => array(
                        )
                    )
                )
            )
        ));

        if (isset($data['location'])) {
            foreach ($data['location'] as $id => $name) {
                $this->add(array(
                    'name' => 'location' . $id,
                    'type' => 'Zend\Form\Element\Checkbox',
                    'options' => array(
                        'use_hidden_element' => true,
                        'checked_value' => $id,
                        'unchecked_value' => 'bad'
                    ),
                    'attributes' => array(
                        'id' => 'locationSel' . $id,
                        'class' => 'locationCheck',
                        'data-locationid' => $id,
                    ),
                ));
            }
        }
    }

}
