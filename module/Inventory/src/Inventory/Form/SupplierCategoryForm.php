<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 * This Form for add or edit supplier category
 */

namespace Inventory\Form;

use Zend\Form\Form;

class SupplierCategoryForm extends Form
{

    public function __construct()
    {

        parent::__construct();
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");


        $this->add(array(
            'name' => 'supplierCategoryID',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'supplierCategoryID',
            ),
        ));

        $this->add(array(
            'name' => 'supplierCategoryName',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'supplierCategoryName',
                'placeholder' => _('eg : Agriculture')
            ),
        ));
    }

}
