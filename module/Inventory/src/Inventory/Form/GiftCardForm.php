<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 */

namespace Inventory\Form;

use Zend\Form\Form;

class GiftCardForm extends Form
{

    public function __construct($data = NULL)
    {
        parent::__construct('giftCard');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'giftCardId',
            'type' => 'Hidden',
            'attributes' => array(
                'id' => 'giftCardId',
            ),
        ));

        $this->add(array(
            'name' => 'giftCardName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'giftCardName',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: Card')
            ),
        ));

        $this->add(array(
            'name' => 'giftCardPrice',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'giftCardPrice',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: 1000')
            ),
        ));

        $this->add(array(
            'name' => 'giftCardDiscountPrecentage',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'giftCardDiscountPrecentage',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: 10')
            ),
        ));

        $this->add(array(
            'name' => 'giftCardDurationDays',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'giftCardDurationDays',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: 30')
            ),
        ));

        $this->add(array(
            'name' => 'giftCardPrefix',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'giftCardPrefix',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: GCRD')
            ),
        ));

        $this->add(array(
            'name' => 'startValue',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'startValue',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: 1')
            ),
        ));

        $this->add(array(
            'name' => 'endValue',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'endValue',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: 100')
            ),
        ));

        $this->add(array(
            'name' => 'locationName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'locationName',
                'autocomplete' => false,
                'class' => 'form-control',
                'disabled' => TRUE,
                'value' => $data['location']
            ),
        ));

        $this->add(array(
            'name' => 'giftCardGlAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'giftCardGlAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

    }

}
